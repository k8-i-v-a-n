/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

/*
 * NOTICE!!!
 *
 * This file contains SPOILERS, which might ruin your IVAN experience
 * totally. Also, editing anything can DESTROY GAME BALANCE or CAUSE
 * OBSCURE BUGS if you don't know what you're doing. So from here on,
 * proceed at your own risk!
 */

VAR CONSOLE_ENABLED = 0;

VAR LOADER_DEBUG = 0;
VAR POI_PLACEMENT_DEBUG = 0;

VAR BONE_DEBUG = 0;
// active only if bone debug active
VAR DEBUG_ALWAYS_CREATE_BONES = 0;
VAR DEBUG_ALWAYS_LOAD_BONES = 0;

VAR DEBUG_SHOW_STETHOSCOPE_COUNTERS = 0;

VAR DEBUG_MONSTER_SPAWN = 0;
VAR DEBUG_ITEM_SPAWN = 0;

//VAR START_CHEAT_WEAPON = 0;

VAR DEBUG_SOUND = 0;

// number of times player ghost can be buffed before giving up
VAR MAX_GHOST_CREATION_BUFFS = 28;

// turns for full danger map refresh.
// special values:
//   <0: one monster at a time (original game)
//    0: all monster configs at a time
//VAR DANGERMAP_REFRESH_RATE = 220;
//VAR DANGERMAP_REFRESH_RATE = 125;
//VAR DANGERMAP_REFRESH_RATE = 180;
VAR DANGERMAP_REFRESH_RATE = 140;

// keep updating the list in worldmap? original game doesn't do that.
VAR DANGERMAP_UPDATE_IN_WORLDMAP = 1;

VAR DEBUG_DANGER_MAP_UPDATES = 0;


// slows down `protosystem::BalancedCreateItem()` but makes it produce more accurate results.
// was `100`.
// if item creator failed for this number of iterations, it will bump the price.
// usually it either doesn't fail, or fails a lot. this is how very pricy items
// are spawned sometimes. most failures seems to be in creating god items.
// it is quite hard to decide on the limit for this. dunno.
VAR BALANCED_CREATE_ITEM_ITERATIONS = 140;
VAR BALANCED_CREATE_GOD_ITEM_ITERATIONS = 150;
/* was 25 */
VAR BALANCED_ITEM_TRY_LIMIT = 25;
VAR BALANCED_GOD_ITEM_TRY_LIMIT = 25;

// number of times we repeat the whole process if it is failed.
// there is no randomness in monster selection, we list them all.
// the randomness comes next. at least that's how i read the code.
// note that there is no difficulty rise in repeats. but the call
// to `game::GetMinDifficulty()` returns slightly randomized results,
// so the list will not be exactly the same each time.
// also, when this limit is reached, we simply collect all monsters,
// and spawn a random one without further checking.
// was `100`.
VAR BALANCED_MONSTER_SELECT_LIMIT = 150;
// this is the second part of the generator. here, we choose a random
// monster from our selection list, and check other conditions,
// like frequency and danger level. this may fail, and then we'll
// repeat the whole process (starting from monster selection) again.
// was `25`.
VAR BALANCED_MONSTER_TRY_LIMIT = 25;

/* was 25; used to find a suitable monster for polymorphing */
VAR POLY_MONSTER_TRY_LIMIT = 69;


// add new monster after `game::Run()` is called this number of times
// note that the more times the game tried to generate a new monster,
// the less generation chance is (this is controlled in `level::GenerateMonsters()`)
VAR MONSTER_GEN_DELAY = 10;


// >= this
VAR REFUSE_ZOMBIE_EATING_INT = 15;

/*k8: this now can be controlled from here; 'cause why not? */
#define OVER_FED_LEVEL 175000
#define BLOATED_LEVEL 150000
#define SATIATED_LEVEL 100000
#define NOT_HUNGER_LEVEL 30000
#define HUNGER_LEVEL 20000
#define VERY_HUNGER_LEVEL 10000

#define OVER_LOADED 0
#define STRESSED 1
#define BURDENED 2
#define UNBURDENED 3

#define STARVING 0
#define VERY_HUNGRY 1
#define HUNGRY 2
#define NOT_HUNGRY 3
#define SATIATED 4
#define BLOATED 5
#define OVER_FED 6


/* Numerical defines for other script files */
#bitenum {
  NONE = 0, // this will not force-change index
  MIRROR,
  FLIP,
  ROTATE,
};

#define RED    63488
#define GREEN  2016
#define BLUE   10943

#define YELLOW  65504
#define PINK    61470

#define WHITE       65535
#define LIGHT_GRAY  46518
#define DARK_GRAY   21130
#define BLACK       0

#define TRANSPARENT_COLOR  63519


//WARNING! we run ouf of effect bits!
#bitenum #ItemEffects {
  POLYMORPHED,
  HASTE,
  SLOW,
  POLYMORPH_CONTROL,
  LIFE_SAVED,
  LYCANTHROPY,
  INVISIBLE,
  INFRA_VISION,
  ESP,
  POISONED,
  TELEPORT,
  POLYMORPH,
  TELEPORT_CONTROL,
  PANIC,
  CONFUSED,
  PARASITE_TAPE_WORM,
  SEARCHING,
  GAS_IMMUNITY,
  LEVITATION,
  LEPROSY,
  HICCUPS,
  VAMPIRISM,
  SWIMMING,
  DETECTING,
  ETHEREAL_MOVING,
  FEARLESS,
  POLYMORPH_LOCK,
  REGENERATION,
  DISEASE_IMMUNITY,
  TELEPORT_LOCK,
  // new from comm. fork
  FASTING,
  PARASITE_MIND_WORM,
};


#define THROW_ITEM_TYPES 5
/*ThrowFlags */
#bitenum #ThrowFlags {
  THROW_BONE,
  THROW_POTION,
  THROW_AXE,
  THROW_GAS_GRENADE,
  THROW_WAND,
  THROW_DAGGER,
};


#bitenum #BodyParts {
  HEAD,
  TORSO,
  RIGHT_ARM,
  LEFT_ARM,
  GROIN,
  RIGHT_LEG,
  LEFT_LEG,
  OTHER,
};

#define ARMS  (RIGHT_ARM | LEFT_ARM)
#define LEGS  (RIGHT_LEG | LEFT_LEG)
#define ALL   255


#bitenum #DamageType {
  PHYSICAL_DAMAGE,
  SOUND,
  ACID,
  FIRE,
  ELECTRICITY,
  ENERGY,
  POISON,
  DRAIN,
  MUSTARD_GAS_DAMAGE,
  PSI, // psi damage from mind worm
}

#define THROW 32768


#enum #Sex {
  UNDEFINED,
  MALE,
  FEMALE,
  TRANSSEXUAL,
};


#enum #BodyPartIndex {
  TORSO_INDEX,
  HEAD_INDEX,
  RIGHT_ARM_INDEX,
  LEFT_ARM_INDEX,
  GROIN_INDEX,
  RIGHT_LEG_INDEX,
  LEFT_LEG_INDEX,
};


#enum #Alignments {
  ALPP,
  ALP,
  AL,
  ALM,
  ANP,
  AN,
  ANM,
  ACP,
  AC,
  ACM,
  ACMM,
};

/*
#define UNARTICLED    0
#define PLURAL        1
#define ARTICLE_BIT   2
#define DEFINITE      2
#define INDEFINE_BIT  4
#define INDEFINITE    6
#define STRIPPED 8
*/


#define ANY_CATEGORY 2147483647
#bitenum #ItemCategory {
  HELMET,
  AMULET,
  CLOAK,
  BODY_ARMOR,
  WEAPON,
  SHIELD,
  RING,
  GAUNTLET,
  BELT,
  BOOT,
  FOOD,
  POTION,
  SCROLL,
  BOOK,
  WAND,
  TOOL,
  VALUABLE,
  MISC,
};


#enum #DeedType {
  = 1, /* first */
  GOOD, /* it will be 1 */
  NEUTRAL,
  EVIL,
  TOPPLED,
};


#bitenum #ConsumeType {
  CT_FRUIT,
  CT_MEAT,
  CT_METAL,
  CT_MINERAL,
  CT_LIQUID,
  CT_BONE,
  CT_PROCESSED,
  //CT_MISC_ORGANIC, // old; replaced with "MISC_PLANT" and "MISC_ANIMAL"
  CT_MISC_PLANT,
  CT_MISC_ANIMAL,
  CT_PLASTIC,
  CT_GAS,
  /*NEW!*/
  CT_MAGIC,
};


#enum #Direction {
  LEFT,
  DOWN,
  UP,
  RIGHT,
  CENTER,
};


/*
#define HOSTILE   1
#define UNCARING  2
#define FRIEND    4
*/
#bitenum #Attitude {
  HOSTILE,
  UNCARING,
  FRIEND,
  PROTECTIVE, /* will became hostile if the other team is hostile; special flag, not an attitude! */
};

//#define MARTIAL_SKILL_CATEGORIES  3
//#define WEAPON_SKILL_CATEGORIES  11

// WARNING! keep this in sync with `CWeaponSkillName` in "src/game/wskill.cpp"!
// also, see `WEAPON_SKILL_CATEGORIES` in "src/game/ivandef.h".
// also, first 3 should be "martial skills" (`MARTIAL_SKILL_CATEGORIES` in "src/game/ivandef.h").
#enum #WeaponSkills {
  UNARMED,
  KICK,
  BITE,
  UNCATEGORIZED,
  SMALL_SWORDS,
  LARGE_SWORDS,
  BLUNT_WEAPONS,
  AXES,
  POLE_ARMS,
  WHIPS,
  SHIELDS,
};

#define MAX_WEAPON_SKILL  SHIELDS


#define LOCKED  1

#enum #CharEffects {
  EFFECT_NOTHING,
  EFFECT_POISON,
  EFFECT_DARKNESS,
  EFFECT_OMMEL_URINE,
  EFFECT_PEPSI,
  EFFECT_KOBOLD_FLESH,
  EFFECT_HEAL,
  EFFECT_LYCANTHROPY,
  EFFECT_SCHOOL_FOOD,
  EFFECT_ANTIDOTE,
  EFFECT_CONFUSE,
  EFFECT_POLYMORPH,
  EFFECT_ESP,
  EFFECT_SKUNK_SMELL,
  EFFECT_MAGIC_MUSHROOM,
  EFFECT_TRAIN_PERCEPTION,
  EFFECT_HOLY_BANANA,
  EFFECT_EVIL_WONDER_STAFF_VAPOUR,
  EFFECT_GOOD_WONDER_STAFF_VAPOUR,
  EFFECT_PEA_SOUP,
  EFFECT_BLACK_UNICORN_FLESH,
  EFFECT_GRAY_UNICORN_FLESH,
  EFFECT_WHITE_UNICORN_FLESH,
  EFFECT_TELEPORT_CONTROL,
  EFFECT_MUSHROOM,
  EFFECT_OMMEL_CERUMEN,
  EFFECT_OMMEL_SWEAT,
  EFFECT_OMMEL_TEARS,
  EFFECT_OMMEL_SNOT,
  EFFECT_OMMEL_BONE,
  EFFECT_MUSTARD_GAS,
  EFFECT_MUSTARD_GAS_LIQUID,
  EFFECT_PANIC,
  EFFECT_TELEPORT,
  EFFECT_VAMPIRISM,
  EFFECT_DETECTING,
  EFFECT_HOLY_MANGO,
  // more effects from comm. fork
  EFFECT_TRAIN_WISDOM,
  EFFECT_OMMEL_BLOOD,
  EFFECT_PANACEA,
  EFFECT_LAUGH, // hiccups
  EFFECT_POLYJUICE,
  EFFECT_PUKE, // does nothing for now
  EFFECT_SICKNESS,
  EFFECT_PHASE,
  EFFECT_ACID_GAS,
  EFFECT_REGENERATION,
};


/* CEM = Consume End Message */
#enum #CEM {
  CEM_NOTHING,
  CEM_SCHOOL_FOOD,
  CEM_BONE,
  CEM_FROG_FLESH,
  CEM_OMMEL,
  CEM_PEPSI,
  CEM_KOBOLD_FLESH,
  CEM_HEALING_LIQUID,
  CEM_ANTIDOTE,
  CEM_ESP,
  CEM_HOLY_BANANA,
  CEM_PEA_SOUP,
  CEM_BLACK_UNICORN_FLESH,
  CEM_GRAY_UNICORN_FLESH,
  CEM_WHITE_UNICORN_FLESH,
  CEM_OMMEL_BONE,
  CEM_LIQUID_HORROR,
  CEM_HOLY_MANGO,
  CEM_COCA_COLA,
};


/* HM = Hit Message */
#enum #HM {
  HM_NOTHING,
  HM_SCHOOL_FOOD,
  HM_FROG_FLESH,
  HM_OMMEL,
  HM_PEPSI,
  HM_KOBOLD_FLESH,
  HM_HEALING_LIQUID,
  HM_ANTIDOTE,
  HM_CONFUSE,
  HM_HOLY_BANANA,
  HM_HOLY_MANGO,
};


/*#define SOLID_ID  4096*/
#enum #MaterialConfig {
  SOLID_ID = (1 << 12),
  VALPURIUM,
  GRAVEL,
  MORAINE,
  OCTIRON,
  GLASS,
  PARCHMENT,
  CLOTH,
  MITHRIL,
  MARBLE,
  GOLD,
  GRASS,
  LEATHER,
  LEAF,
  FABRIC,
  PALM_LEAF,
  SULFUR,
  UNICORN_HORN,
  DIAMOND,
  SILVER,
  SAPPHIRE,
  RUBY,
  BRONZE,
  COPPER,
  TIN,
  SPIDER_SILK,
  KEVLAR,
  OMMEL_HAIR,
  HARDENED_LEATHER,
  TROLL_HIDE,
  NYMPH_HAIR,
  ANGEL_HAIR,
  PHOENIX_FEATHER,
  GOLDEN_EAGLE_FEATHER,
  DRAGON_HIDE,
  ARCANITE,
  ILLITHIUM,
  BALSA_WOOD,
  PINE_WOOD,
  FIR_WOOD,
  BIRCH_WOOD,
  OAK_WOOD,
  TEAK_WOOD,
  EBONY_WOOD,
  BLUE_CRYSTAL,
  PURPLE_CRYSTAL,
  GREEN_CRYSTAL,
  SAND_STONE,
  LIME_STONE,
  CALCITE,
  OBSIDIAN,
  GNEISS,
  SLATE,
  GRANITE,
  BASALT,
  MILKY_QUARTZ,
  FLINT,
  QUARTZITE,
  AMETHYST,
  CITRINE,
  ROSE_QUARTZ,
  JASPER,
  ROCK_CRYSTAL,
  DARK_GRASS,
  LEAD,
  BLACK_GRANITE,
  BLACK_LEATHER,

  ICE,
  RED_ICE,
  JOTUN_ICE,
  PRIMORDIAL_ICE,

  FLAWLESS_DIAMOND,
  EMERALD,
  EXTRA_HARD_BASALT,
  SUN_CRYSTAL,
  BLACK_DIAMOND,
  PSYPHER,

  //WEAK_GLASS, not defined, not used

  DEAD_GRASS,
  KAURI_WOOD,
  RATA_WOOD,
  SIDGURE_WOOD,
  PETRIFIED_WOOD,
  NEPHRITE,

  HESSIAN_CLOTH, //k8 exclusive

  // new in comm. fork
  BLACK_JADE,
  RED_JADE,
  GREEN_JADE,
  BLUE_JADE,
  WHITE_JADE,

  LINEN_CLOTH,
  HEMP_CLOTH,
  WOOL_CLOTH,
  FELT,
  ELF_CLOTH,
  TROLL_WOOL,

  LAPIS_LAZULI,
  AMBER,
  OPAL,
  ONYX,
  SLADE,

  NACRE,
  PEARL,
  CORAL,
  DEEP_CORAL,

  FUNGI_WOOD,
  SPRUCE_WOOD,
  SYCAMORE_WOOD,
  ELM_WOOD,
  ASH_WOOD,
  CYPRESS_WOOD,
  HOLLY_WOOD,
  YEW_WOOD,
  BEECH_WOOD,
  BAMBOO_WOOD,
  MAHOGANY_WOOD,

  VERDIGRIS,
  DEEP_BRONZE,
  BRASS,
  HEPATIZON,

  GALVORN,
  STAR_METAL,
  PLATINUM,
  ORICHALCUM,

  MOON_SILVER,
  FAIRY_STEEL,

  DARK_GOLD,
  DARK_MATTER,

  PETRIFIED_DARK,
  ELECTRUM,
  ALUMINIUM,
  NICKEL,
  COBALT,
  TUNGSTEN,
  IRIDIUM,
  PALLADIUM,
  CHROME,
  TITANITE,

  GOSSAMER,
  SPIRIT_CLOTH,
  HUMAN_SKIN,
  WOLF_SKIN,
  BEAR_SKIN,
  SNAKE_SKIN,
  CROCODILE_SKIN,
  BASILISK_SKIN,
  NAGA_SKIN,
  WYVERN_HIDE,
  HYDRA_HIDE,
  BOILED_LEATHER,
  QUILTED_LEATHER,
  STUDDED_LEATHER,
  IMP_HIDE,
  OUROBOROS_HIDE,

  OSTRICH_FEATHER,
  HARPY_FEATHER,
  GRIFFON_FEATHER,
  STYMPHALIAN_FEATHER,

  SEA_WEED,
  FISH_SCALE,
  MERMAID_HAIR,
  SELKIE_SKIN,
  SEA_SERPENT_SCALE,
  KRAKEN_HIDE,
  LEVIATHAN_HIDE,

  DREAM_CLOTH,
  RAINBOW_CLOTH,
  SHADOW_CLOTH,
  NYLON,
  ELASTI_CLOTH,
  ETEX_CLOTH,
  SMART_CLOTH,
  FLEXI_CLOTH,

  SYNTH_CLOTH,
  NANO_FIBER,
  LATEX,
  RUBBER,
  PLASTIC,
  HARDENED_PLASTIC,
  STAINLESS_STEEL,
  PLASTIC_STEEL,
  DURALLOY,
  VITRELLOY,
  CHAR_COAL,
  COAL,
  JET,
  CARBIDE,
  FOLDED_CARBIDE,
  FULLERITE,
  URANIUM,
  SOLARIUM,
  NEUTRONIUM,
  MAGIC_CRYSTAL,
  OCCULTUM,
  RED_SAND_STONE,
  PEARL_GLASS,
  MYCELIUM,
  WITCH_BARK,
  DARK_PETAL,
  STEEL_LEAF,
  CHLOROPHYTE,
  PAPYRUS,
  VELLUM,
  PAPER_BOARD,
  HUMAN_NAIL,
  CHITINE,
  BUFFALO_HORN,
  MOOSE_ANTLER,
  TURTLE_SHELL,
  SEA_SHELL,
  DRAGON_BONE,
  WAX,
  COBBLE_STONE,
  SHALE,
  JACINTH,
  AGATE,
  TOURMALINE,
  CHALCEDONY,
  CHALK,
  HALCYON,
  NETHER_QUARTZ,
  TOPAZ,
  JADEITE,
  MALACHITE,
  CONCRETE,
  HARDENED_CONCRETE,
  SUPER_CONCRETE,
  BRIM_STONE,
  CLAY,
  HARDENED_CLAY,
  PORCELAIN,
  ROCK_SALT,
  AEGI_SALT,
  ALABASTER,
  HEMATITE,
  PYRITE,
  HARDENED_ASH,
  CRYSTEEL,
  FLAWLESS_CRYSTEEL,
};


/*#define ORGANIC_ID (4096 * 2)*/
#enum #OrganicConfig {
  ORGANIC_ID = (2 << 12),
  BANANA_FLESH,
  SCHOOL_FOOD,
  BANANA_PEEL,
  KIWI_FLESH,
  PINEAPPLE_FLESH,
  PLANT_FIBER,
  MUTANT_PLANT_FIBER,
  BONE,
  BREAD, // replaced by two kinds of bread in comm. fork, i believe
  HOLY_BANANA_FLESH,
  CARROT_FLESH,
  OMMEL_CERUMEN,
  OMMEL_BONE,
  OMMEL_TOOTH,
  RYE_BREAD,
  MANGO_FLESH,
  HOLY_MANGO_FLESH, // k8new

  // new in comm. fork
  DARK_BREAD,
  FLAT_BREAD,

  KELP,
  PLANKTON,
  BANANA_STOLLEN,
  CHEESE,
  CHOCOLATE,
  WHALE_BONE,
  WRAITH_BONE,
  SHARK_TOOTH,
  TROLL_TUSK,
  MAMMOTH_TUSK,
  RESIDUUM,
};


/*#define GAS_ID (4096 * 3)*/
//#define GAS_ID  (3 << 12)
#enum #GasConfig {
  GAS_ID = (3 << 12),
  AIR,
  MAGICAL_AIR, //k8 exclusive
  SMOKE,
  SKUNK_SMELL,
  //GHOST, // this is prolly "ECTOPLASM" in comm. fork
  MAGIC_VAPOUR,
  EVIL_WONDER_STAFF_VAPOUR,
  GOOD_WONDER_STAFF_VAPOUR,
  FART,
  MUSTARD_GAS,

  //k8: i don't remember if this is mine or not
  VACUUM_BLADE_AIR,

  // new in comm. fork
  VACUUM,
  ETHER,
  //ECTOPLASM, // was "GHOST"; now liquid
  SLEEPING_GAS,
  TELEPORT_GAS,
  LAUGHING_GAS,
  ACID_GAS,
  //FIRE_GAS, -- i have no fire system
};

#define FIRST_GAS  AIR
#define LAST_GAS   ACID_GAS


/*#define LIQUID_ID (4096 * 4)*/
#enum #LiquidConfig {
  LIQUID_ID = (4 << 12),
  OMMEL_URINE,
  PEPSI,
  WATER,
  HEALING_LIQUID,
  BLOOD,
  BROWN_SLIME,
  POISON_LIQUID,
  VALDEMAR,
  ANTIDOTE_LIQUID,
  VODKA,

  TROLL_BLOOD,
  DARK_FROG_BLOOD,
  SPIDER_BLOOD,
  VOMIT,
  ACIDOUS_BLOOD,
  SULPHURIC_ACID,
  DOG_DROOL,
  PEA_SOUP,
  OMMEL_SWEAT,
  OMMEL_TEARS,
  OMMEL_SNOT,
  SWEAT,
  GLOWING_BLOOD,
  YELLOW_SLIME, //k8 exclusive
  SICK_BLOOD,
  MUSTARD_GAS_LIQUID,
  OMMEL_VOMIT, //k8 exclusive
  LIQUID_HORROR, //k8 exclusive; this might be "LIQUID_FEAR" in comm. fork
  LIQUID_DARKNESS,
  LIGHT_FROG_BLOOD,
  VINEGAR,
  OMMEL_BLOOD,
  CURDLED_OMMEL_BLOOD, //k8 exclusive, for witch quest

  // comm. fork, already added
  CIDER,
  WHITE_WINE,
  RED_WINE,
  BEER,
  ELF_ALE,
  DWARF_BEER,

  CHICKEN_SOUP,

  // moved from gases
  ECTOPLASM, // was "GHOST"; now liquid

  // new in comm. fork
  CURE_ALL_LIQUID,
  MAGIC_LIQUID,
  TELEPORT_FLUID,
  POLYMORPHINE,
  PORRIDGE,
  CUSTARD,
  MILK,
  HONEY,
  COCA_COLA,
  INK,
  BRINE,
  GREEN_SLIME,
  ASPHALT,
  GREEN_BLOOD,
  BLUE_BLOOD,
  BLACK_BLOOD,
  PLANT_SAP,
  MERCURY, // this is technically metal
  QUICK_SILVER, // this is technically metal
  MUD,
  QUICK_SAND,
  LAVA,
  NAPALM,
};

#define FIRST_LIQUID  OMMEL_URINE
#define LAST_LIQUID   NAPALM


/*#define FLESH_ID (4096 * 5)*/
#enum #FleshConfig {
  FLESH_ID = (5 << 12),
  GOBLINOID_FLESH,
  PORK,
  BEEF,
  FROG_FLESH,
  ELPURI_FLESH,
  HUMAN_FLESH,
  DOLPHIN_FLESH,
  BEAR_FLESH,
  WOLF_FLESH,
  DOG_FLESH,
  ENNER_BEAST_FLESH,
  SPIDER_FLESH,
  JACKAL_FLESH,
  MUTANT_ASS_FLESH,
  BAT_FLESH,
  WEREWOLF_FLESH, // called "WERE_WOLF_FLESH" in comm. fork
  KOBOLD_FLESH,
  GIBBERLING_FLESH,
  CAT_FLESH,
  RAT_FLESH,
  ANGEL_FLESH,
  DWARF_FLESH,
  DAEMON_FLESH,
  MAMMOTH_FLESH,
  BLACK_UNICORN_FLESH,
  GRAY_UNICORN_FLESH,
  WHITE_UNICORN_FLESH,
  LION_FLESH,
  BUFFALO_FLESH,
  SNAKE_FLESH,
  ORC_FLESH,
  OSTRICH_FLESH,
  CHAMELEON_FLESH,
  FLOATING_EYE_FLESH,
  MUSHROOM_FLESH,
  MOOSE_FLESH,
  MAGPIE_FLESH,
  SKUNK_FLESH,
  HEDGEHOG_FLESH,
  MUTANT_BUNNY_FLESH,
  HATTIFATTENER_FLESH,
  BLINK_DOG_FLESH,
  MAGIC_MUSHROOM_FLESH,
  SICK_SPIDER_FLESH,
  MIND_WORM_FLESH,
  MUTANT_HEDGEHOG_FLESH, //k8 exclusive
  EAGLE_FLESH, //k8 exclusive
  KABOUTER_FLESH, //k8 exclusive
  ULDRA_FLESH, //k8 exclusive
  OKAPI_FLESH, //k8 exclusive
  VAMPIRE_FLESH,
  MOUSE_FLESH, //k8 exclusive
  FOX_FLESH, //k8 exclusive
  THUNDER_BIRD_FLESH, //k8 exclusive

  ELF_FLESH,

  TUNA,
  SARDINE,
  SMOKED_SARDINE,

  VENISON,
  MYSTERY_MEAT,
  SNAKE_JERKY,
};


/*#define POWDER_ID (4096 * 6)*/
#enum #PowderConfig {
  POWDER_ID = (6 << 12),
  GUN_POWDER,
  SNOW,
  SAND,
  ASH,

  // new in comm. fork
  SALT,
  RED_SAND,
  BLACK_SAND,
  DIRT,
  SOIL,
  SOOT,
};


/*#define IRON_ALLOY_ID (4096 * 7)*/
#enum #IronConfig {
  IRON_ALLOY_ID = (7 << 12),
  IRON,
  STEEL,
  METEORIC_STEEL,
  DWARF_STEEL,
  ADAMANT,
  DAMASCUS_STEEL, //k8 exclusive

  // new in comm. fork
  //PIG_IRON, -- i removed it
  UKKU_STEEL,
  BLACK_IRON,
  SOUL_STEEL,
  BLOOD_IRON,
  BLOOD_STEEL,
  WHITE_STEEL,
  UR_STEEL,
};

// universal replacement, lol
#define PIG_IRON  BRONZE


#enum #AttackType {
  UNARMED_ATTACK,
  WEAPON_ATTACK,
  KICK_ATTACK,
  BITE_ATTACK,
  THROW_ATTACK, //k8 exclusive
};


#define USE_ARMS  1
#define USE_LEGS  2
#define USE_HEAD  4

//#define ATTRIBUTES      11
//#define BASE_ATTRIBUTES  7

/*
#enum {
  ENDURANCE,
  PERCEPTION,
  INTELLIGENCE,
  WISDOM,
  WILL_POWER,
  CHARISMA,
  MANA,
  ARM_STRENGTH,
  LEG_STRENGTH,
  DEXTERITY,
  AGILITY,
};
*/


/*
#enum {
  HELMET_INDEX,
  AMULET_INDEX,
  CLOAK_INDEX,
  BODY_ARMOR_INDEX,
  BELT_INDEX,
  RIGHT_WIELDED_INDEX,
  LEFT_WIELDED_INDEX,
  RIGHT_RING_INDEX,
  LEFT_RING_INDEX,
  RIGHT_GAUNTLET_INDEX,
  LEFT_GAUNTLET_INDEX,
  RIGHT_BOOT_INDEX,
  LEFT_BOOT_INDEX,
};
*/


#define SUPER    64
#define BROKEN   128
#define WINDOW  1024

#enum #MeeleeConfig {
  LONG_SWORD = 1,
  TWO_HANDED_SWORD,
  TWO_HANDED_SCIMITAR,
  SPEAR,
  AXE,
  HALBERD,
  MACE,
  WAR_HAMMER,
  SICKLE,
  DAGGER,
  SHORT_SWORD,
  BASTARD_SWORD,
  BATTLE_AXE,
  SCYTHE,
  QUARTER_STAFF,
  HAMMER,
  KNIGHT_SWORD, // k8 exclusive
  KATANA,
  SPETUM, // k8 exclusive
  TIP_SWORD, // k8 exclusive
  KNUCKLE, // k8 exclusive
  RAPIER, // k8 exclusive
  GREAT_AXE, // k8 exclusive
  GRAND_STOLLEN_KNIFE,
  LOST_RUBY_FLAMING_SWORD,
  // new in comm. fork
  ROLLING_PIN,
  FRYING_PAN,
  //CLAW, -- ??? i have claws, but they are in Zone69 mod
  MEAT_CLEAVER,
  RUNE_SWORD,
};


// new in comm. fork
#enum #MageStaffConfig {
  ROYAL_STAFF = 1,
};


#enum {
  GOROVITS_HAMMER = 1,
  GOROVITS_SICKLE,
  GOROVITS_SCIMITAR,
};


// something in comm. fork
#enum #SkullConfig {
  PUPPY_SKULL = 1,
};


#enum #ArmorConfig {
  CHAIN_MAIL = 1,
  PLATE_MAIL,
  ARMOR_OF_GREAT_HEALTH,
  DRAGON_CUIRASS, //k8 exclusive
  OMMEL_CUIRASS,
  ARMOR_OF_THE_OMMEL, //k8 exclusive
  FILTHY_TUNIC, //k8 exclusive
};


#enum #PetrusNutConfig {
  CHEAP = 1,
  EXPENSIVE,
};


#enum #WandConfig {
  WAND_OF_POLYMORPH = 1,
  WAND_OF_STRIKING,
  WAND_OF_FIRE_BALLS,
  WAND_OF_TELEPORTATION,
  WAND_OF_HASTE,
  WAND_OF_SLOW,
  WAND_OF_RESURRECTION,
  WAND_OF_DOOR_CREATION,
  WAND_OF_INVISIBILITY,
  WAND_OF_CLONING,
  WAND_OF_LIGHTNING,
  WAND_OF_ACID_RAIN,
  WAND_OF_MIRRORING,
  WAND_OF_NECROMANCY,
  // new in comm. fork
  WAND_OF_WEBBING,
  WAND_OF_ALCHEMY,
  WAND_OF_SOFTEN_MATERIAL,
};


#enum #WhipConfig {
  RUNED_WHIP = 1,
  CHAIN_WHIP, //k8 exclusive
};


#define BIG_MINE 1


#enum {
  PHOENIX_SHIELD = 1,
  AEGIS_SHIELD,
  // new in comm. fork
  SHIELD_OF_FIRE_RESISTANCE,
  SHIELD_OF_ELECTRICITY_RESISTANCE,
  SHIELD_OF_MAGIC_RESISTANCE,
}


#enum #CloakConfig {
  CLOAK_OF_INVISIBILITY = 1,
  CLOAK_OF_FIRE_RESISTANCE,
  CLOAK_OF_ELECTRICITY_RESISTANCE,
  CLOAK_OF_ACID_RESISTANCE,
  CLOAK_OF_WEREWOLF,
  CLOAK_OF_SHADOWS,
  CLOAK_OF_ORCS,
  CLOAK_OF_FLYING,
  CLOAK_OF_QUICKNESS,
  CLOAK_OF_PROTECTION,
  CLOAK_OF_VAMPIRE,
};


#enum #BootConfig {
  BOOT_OF_STRENGTH = 1,
  BOOT_OF_AGILITY,
  BOOT_OF_KICKING,
  // new in comm. fork
  BOOT_OF_DISPLACEMENT,
};


#enum #GauntletConfig {
  GAUNTLET_OF_STRENGTH = 1,
  GAUNTLET_OF_DEXTERITY,
};


#enum #RingConfig {
  RING_OF_FIRE_RESISTANCE = 1,
  RING_OF_POLYMORPH_CONTROL,
  RING_OF_INFRA_VISION,
  RING_OF_TELEPORTATION,
  RING_OF_TELEPORT_CONTROL,
  RING_OF_POLYMORPH,
  RING_OF_POISON_RESISTANCE,
  RING_OF_INVISIBILITY,
  RING_OF_ELECTRICITY_RESISTANCE,
  RING_OF_SEARCHING,
  RING_OF_ACID_RESISTANCE,
  //RING_OF_THIEVES, // moved to separate item
  RING_OF_LIGHT,
  RING_OF_MAGIC_RESISTANCE,
  RING_OF_DETECTION,
  RING_OF_POLYMORPH_LOCK,
  RING_OF_WORM,
  // new in comm. fork
  RING_OF_BRAVERY,
  RING_OF_SPEED,
};


#enum #AmuletConfig {
  AMULET_OF_LIFE_SAVING = 1,
  AMULET_OF_ESP,
  AMULET_OF_WARDING,
  AMULET_OF_VANITY,
  AMULET_OF_UNBREATHING,
  AMULET_OF_SPEED,
  AMULET_OF_PHASING,
  AMULET_OF_DISEASE_IMMUNITY,
  AMULET_OF_DIMENSION_ANCHOR,
  // new in comm. fork
  AMULET_OF_FASTING,
  // cheating item
  AMULET_OF_ESP_2,
  AMULET_OF_EVERYTHING,
};


#enum #HelmetConfig {
  FULL_HELMET = 1,
  HELM_OF_PERCEPTION,
  HELM_OF_UNDERSTANDING,
  HELM_OF_BRILLIANCE,
  HELM_OF_ATTRACTIVITY,
  GOROVITS_FAMILY_GAS_MASK,
  HELM_OF_TELEPATHY,
  // new in comm. fork
  HELM_OF_WILLPOWER,
  HELM_OF_TELEPORTATION,
  HELM_OF_ENLIGHTENMENT,
  HELM_OF_MANA,
  MASK,
  WAR_MASK,
};


#enum #BeltConfig {
  BELT_OF_CARRYING = 1,
  BELT_OF_LEVITATION,
  BELT_OF_PROTECTION,
  BELT_OF_GIANT_STRENGTH,
  BELT_OF_REGENERATION,
  // new in comm. fork
  // dunno, seems to be a separate object in k8ivan
  //BELT_OF_THIEF,
};


#enum #ChestConfig {
  SMALL_CHEST = 1,
  CHEST,
  LARGE_CHEST,
  STRONG_BOX,
  MAGIC_CHEST, //k8 exclusive
  EMPTY_STRONG_BOX, // can be bought from the smith
};


#enum #HornConfig {
  BRAVERY = 1,
  FEAR,
  CONFUSION,
  // new in comm. fork
  HEALING,
  PLENTY,
};


#enum #TrunketConfig {
  POTTED_CACTUS = 1,
  POTTED_PLANT,
  SMALL_CLOCK,
  LARGE_CLOCK,
};


#enum #FishConfig {
  DEAD_FISH = 1,
  SMOKED_FISH,
  BONE_FISH,
};


#enum #FarmerConfig {
  IMPRISONED_FARMER = 1,
  CULTIST,
  // new in comm. fork
  CRAZED_FARMER,
};


#enum #GuardConfig {
  ROOKIE = 1,
  VETERAN,
  EUNUCH,
  PATROL,
  SHOP,
  ELITE,
  MASTER,
  GRAND_MASTER,
  TOMB_ENTRY,
  TOMB_ENTRY_MASTER,
  HONOR,
  EMISSARY,
  TRAINEE,
  // k8 exclusive
  MONDEDR_GUARD,
  DWARVEN_GUARD,
  SENTINEL,
  FOREST_SHOP,
  // new in comm. fork
  ROOKIE_FEMALE,
  VETERAN_FEMALE,
  ELITE_FEMALE,
  CASTLE,
  ROYAL,
  REBEL,
};

// same as PATROL (4)
#define TEMPLAR  PATROL
// same as SHOP (5)
#define GRAVE_KEEPER SHOP


#define ENQUIOX 128


#enum #FrogConfig {
  DARK = 1,
  GREATER_DARK,
  GIANT_DARK,
  LIGHT,
  GREATER_LIGHT,
  GIANT_LIGHT,
};


#enum #SkeletonConfig {
  WARRIOR = 1,
  WAR_LORD,
};


#enum #GoblinConfig {
  BERSERKER = 1,
  BUTCHER,
  PRINCE,
  KING,
  JAILER, //k8 exclusive
  PRISON_WARDEN, //k8 exclusive
  // new in comm. fork
  MONK,
  WARLOCK,
};


#enum #MommoConfig {
  CONICAL = 1,
  FLAT,
  // new in comm. fork
  MAGMA,
  BLOAT,
};


#enum #DogConfig {
  SKELETON_DOG = 1,
};


#enum #SpiderConfig {
  LARGE = 1,
  GIANT,
  ARANEA,
  // new in comm. fork
  PHASE,
  GIANT_GOLD,
};


#enum #HunterConfig {
  IMPRISONED_HUNTER = 1,
};


#enum #BearConfig {
  BLACK_BEAR = 1,
  GRIZZLY_BEAR,
  CAVE_BEAR,
  POLAR_BEAR,
  PANDA_BEAR,
  MUTANT_BEAR,
};


#enum #FemaleSlaveConfig {
  IMPRISONED_FEMALE = 1,
  /* 2 reserved for ATTNAM */
  /* 3 reserved for NEW_ATTNAM */
  JESTER = 4,
};


#enum #ZombieConfig {
  ZOMBIE_OF_KHAZ_ZADM = 1,
  IMPRISONED_ZOMBIE,
};


#enum #MistressConfig {
  TORTURING_CHIEF = 1,
  WHIP_CHAMPION,
  WAR_LADY,
  QUEEN,
};


#enum #KoboldConfig {
  CHIEFTAIN = 1,
  LORD,
  PATRIARCH,
};


// new in comm. fork
#define DRUID 1
#define SLAYER 1


// new in comm. fork -- seems to be kobolds; with `WARRIOR` instead of `CHIEFTAIN`
/* WARRIOR already defined. */
#define HUNTER 2
//!!#define PATRIARCH 3
#define ASSASSIN 4
#define MASTER_ASSASSIN 5


// new in comm. fork
#enum #SnakeConfig {
  RED_SNAKE = 1,
  GREEN_SNAKE,
  BLUE_SNAKE,
};


// new in comm. fork
#enum #RebelConfig {
  REBEL_SOLDIER = 1,
  REBEL_LIEUTENANT,
};


#enum #MushroomConfig {
  AMBULATORY = 1,
};


#enum #CarnivorousPlantConfig {
  GREATER = 1,
  GIANTIC,
  SHAMBLING,
  LILY,
};


#enum #OrcConfig {
  SLAUGHTERER = 1,
  SQUAD_LEADER,
  OFFICER,
  GENERAL,
  MARSHAL,
  REPRESENTATIVE,
};


#enum #ImperialistConfig {
  MASTER_TORTURER = 1,
  HOARD_MASTER,
  // new in comm. fork
  VICE_ROY,
};


#enum #MageConfig {
  APPRENTICE = 1,
  BATTLE_MAGE,
  ELDER,
  ARCH_MAGE,
};


#enum #ForestmanConfig {
  ROVER = 1,
  BAND_LEADER,
};


#enum #MouseConfig {
  FIELD_MOUSE = 1,
  LABORATORY_MOUSE,
};


#enum #PigConfig {
  THIN_PIG = 1,
};


#enum #OxConfig {
  STARVED_OX = 1,
};


#enum #HatifattenerConfig {
  FLOATIE = 1,
};


#enum #HedgehogConfig {
  SONIC = 1,
};


/* Least significant bit defines sex */
#enum #BunnyConfig {
  BABY_MALE = 2,
  BABY_FEMALE,
  ADULT_MALE,
  ADULT_FEMALE,
};


#enum #NecromancerConfig {
  APPRENTICE_NECROMANCER = 1,
  MASTER_NECROMANCER,
  IMPRISONED_NECROMANCER = 9,
};


#enum #TouristConfig {
  HUSBAND = 1,
  WIFE,
  CHILD,
};


#enum #ChildConfig {
  BOY = 1,
  GIRL,
};


#enum #SirenConfig {
  LIGHT_ASIAN_SIREN = 1,
  DARK_ASIAN_SIREN,
  CAUCASIAN_SIREN,
  DARK_SIREN,
  GREEN_SIREN,
  // more
  BLUE_SIREN,
  RED_SIREN,
  PINK_SIREN,
  HISPANIC_SIREN,
  AMBASSADOR_SIREN,
};


#enum #MindwormConfig {
  HATCHLING = 1,
  BOIL,
};


#enum #PunisherConfig {
  LAW_STUDENT = 1,
};


#enum #SolidTerrainConfig {
  PARQUET = 1,
  FLOOR,
  GROUND,
  GRASS_TERRAIN,
  LANDING_SITE,
  SNOW_TERRAIN,
  DARK_GRASS_TERRAIN,
  SAND_TERRAIN,
  DEAD_GRASS_TERRAIN,
};


#enum #LiquidTerrainConfig {
  POOL = 1,
  UNDERGROUND_LAKE,
  SEA,
};


#enum #WallConfig {
  BRICK_FINE = 1,
  BRICK_PROPAGANDA,
  BRICK_OLD,
  BRICK_PRIMITIVE,
  BRICK_PRIMITIVE_PROPAGANDA,
  STONE_WALL,
  ICE_WALL,
  BROKEN_WALL,
  TENT_WALL,
};


#enum #DecorationConfig {
  PINE = 1,
  FIR,
  HOLY_TREE,
  CARPET,
  COUCH,
  DOUBLE_BED,
  POOL_BORDER,
  POOL_CORNER,
  PALM,
  SNOW_PINE,
  SNOW_FIR,
  ANVIL,
  SHARD,
  CACTUS,
  OAK,
  BIRCH,
  TEAK,
  DWARF_BIRCH,

  ARM_CHAIR,
  BANANA_TREE,
  BENCH,
  CHAIR,
  CHEAP_BED,
  DEAD_TREE,
  DESK,
  EXPENSIVE_BED,
  FORGE,
  FURNACE,
  OVEN,
  PEDESTAL,
  PLAIN_BED,
  SHACKLES,
  STRANGE_TREE,
  TABLE,
  TORTURE_RACK,
  WELL,
  WOODEN_HORSE,
  WORK_BENCH,
  TAILORING_BENCH,
};


#define SNOW_BOULDER  4

#define STAIRS_UP              100
#define STAIRS_DOWN            200
#define OREE_LAIR_ENTRY        300
#define OREE_LAIR_EXIT         400
#define SUMO_ARENA_ENTRY       700
#define SUMO_ARENA_EXIT        800
#define KHARAZ_ARAD_ENTRY      900
#define KHARAZ_ARAD_EXIT      1000
#define WAYPOINT_DEEPER       1100
#define WAYPOINT_SHALLOWER    1200
#define XINROCH_TOMB_ENTRANCE 1300
#define XINROCH_TOMB_EXIT     1400
#define FOUNTAIN             65535


#enum #TerrainContainerConfig {
  BOOK_CASE = 1,
  CHEST_OF_DRAWERS,
  SHELF,
};


#enum #BarwallConfig {
  BROKEN_BARWALL = 1,
};


#enum #DoorConfig {
  BARDOOR = 1,
  SECRET_DOOR,
  // new in comm. fork
  CURTAIN,
};


#define WORLD_MAP  255

#define DEFAULT_TEAM  255


/* Hard-coded teams */
// just add your own to enum
#enum #Teams {
  PLAYER_TEAM,
  MONSTER_TEAM,
  ATTNAM_TEAM,
  SUMO_TEAM,
  VALPURUS_ANGEL_TEAM,
  GC_SHOPKEEPER_TEAM,
  IVAN_TEAM,
  NEW_ATTNAM_TEAM,
  COLONIST_TEAM,
  TOURIST_GUIDE_TEAM,
  TOURIST_TEAM,
  BETRAYED_TEAM,
  MONDEDR_TEAM,
  KHARAZ_ARAD_TEAM,
  FORESTMAN_TEAM,
  SOLICITUS_TEAM,
  MORBE_TEAM,
  XINROCH_TOMB_ENTRY_TEAM,
  XINROCH_TOMB_NECRO_TEAM,
  XINROCH_TOMB_KAMIKAZE_DWARF_TEAM,
  PRISONER_TEAM,
  // new in comm. fork
  TERRA_TEAM,
  ASLONA_TEAM,
  REBEL_TEAM,
  // two new default teams in community fork
  ANGEL_TEAM, //4
  GUILD_TEAM, //5
  // unused team for console
  INDIFFERENT_TEAM,
  //
  NO_TEAM := 65535,
};


#define NOT_WALKABLE 1
#define HAS_CHARACTER 2
#define IN_ROOM 4
#define NOT_IN_ROOM 8
#define ATTACHABLE (16|NOT_IN_ROOM) /* overrides IN_ROOM */
#define HAS_NO_OTERRAIN 32

// dungeon indicies; just add your own to enum
// also, world points of interest
#enum #Dungeons {
  RANDOM,
  ELPURI_CAVE,
  ATTNAM,
  NEW_ATTNAM,
  UNDER_WATER_TUNNEL,
  MONDEDR,
  MUNTUO,
  DRAGON_TOWER,
  DARK_FOREST,
  XINROCH_TOMB,

  // comm. fork conent, included into the default module now
  ASLONA_CASTLE,
  REBEL_CAMP,
  GOBLIN_FORT,
  FUNGAL_CAVE,
  PYRAMID,
  BLACK_MARKET,
  //IRINOX, -- this is WIP in comm. fork

  // POIs without special dungeons
  KHARAZ_ARAD_SHOP := 127,
  UNDER_WATER_TUNNEL_EXIT := 128,
};


/* dungeon tags */
#define ALL_DUNGEONS  32767

#define VESANA_LEVEL 2
#define CRYSTAL_LEVEL 3
#define SPIDER_LEVEL 4
#define ENNER_BEAST_LEVEL 4
#define ZOMBIE_LEVEL 5
/* */
#define CITY_LEVEL 6
#define FUSANGA_K8_LEVEL 7
#define IVAN_LEVEL 9
#define DARK_LEVEL 10
#define OREE_LAIR 14
#define KHARAZ_ARAD 15
#define PLANT_LEVEL 16
/* Tomb Of Xinroch */
#define DUAL_ENNER_BEAST_LEVEL 5
#define NECRO_CHAMBER_LEVEL 6
#define FUSANGA_COMM_LEVEL 3

// comm. fork (also, fusanga is 3)
#define KING_LEVEL 5

/* old:
#define IVAN_LEVEL 7
#define DARK_LEVEL 8
#define OREE_LAIR 12
*/

#define RECTANGLE 1
#define ROUND_CORNERS 2
#define MAZE_ROOM 3

#enum #Gods {
  VALPURUS = 1,
  LEGIFER,
  ATAVUS,
  DULCIS,
  SEGES,
  SOPHOS,
  SILVA,
  LORICATUS,
  MELLIS,
  CLEPTIA,
  NEFAS,
  SCABIES,
  INFUSCOR,
  CRUENTUS,
  MORTIFER,
  ATHEIST,
  SOLICITU,
};
#define TERRA SILVA
// yes, NOT "atheist" or "solicitu"!
#define LAST_REAL_GOD  MORTIFER

#define MAX_PRICE 2147483647


#enum #RoomTypes {
  ROOM_NORMAL = 1,
  ROOM_SHOP,
  ROOM_CATHEDRAL,
  ROOM_LIBRARY,
  ROOM_BANANA_DROP_AREA,
  ROOM_SUMO_ARENA,
  ROOM_VAULT,
  /*New!*/
  ROOM_OWNED_AREA,
};


//WARNING! this directly indexing the array in C++ code!
#enum #BeamTypes {
  BEAM_POLYMORPH,
  BEAM_STRIKE,
  BEAM_FIRE_BALL,
  BEAM_TELEPORT,
  BEAM_HASTE,
  BEAM_SLOW,
  BEAM_RESURRECT,
  BEAM_INVISIBILITY,
  BEAM_DUPLICATE,
  BEAM_LIGHTNING,
  BEAM_DOOR_CREATION,
  BEAM_ACID_RAIN,
  BEAM_NECROMANCY,
  // new in comm. fork
  BEAM_WEBBING,
  BEAM_ALCHEMY,
  BEAM_SOFTEN_MATERIAL,
  BEAM_WALL_CREATION,
}


#enum #BeamStyles {
  PARTICLE_BEAM,
  LIGHTNING_BEAM,
  SHIELD_BEAM,
  //
  BEAM_STYLES, //WARNING: MUST BE 3!
}



#define RANDOM_COLOR 65536

#define NO_LIMIT 65535

#define NO_BROKEN 1
#define IGNORE_BROKEN_PRICE 2

#define N_LOCK_ID 1024
#define S_LOCK_ID 16384
#define LOCK_DELTA 1024

#define BROKEN_LOCK S_LOCK_ID

/* Normal lock types, which can be randomized */

#define ROUND_LOCK (N_LOCK_ID + LOCK_DELTA * 1)
#define SQUARE_LOCK (N_LOCK_ID + LOCK_DELTA * 2)
#define TRIANGULAR_LOCK (N_LOCK_ID + LOCK_DELTA * 3)

/* Special lock types, which must be generated in the script */

#define HEXAGONAL_LOCK (S_LOCK_ID + LOCK_DELTA * 1)
#define OCTAGONAL_LOCK (S_LOCK_ID + LOCK_DELTA * 2)
#define HEART_SHAPED_LOCK (S_LOCK_ID + LOCK_DELTA * 3)
//k8 exclusive
#define PENTAGONAL_LOCK (S_LOCK_ID + LOCK_DELTA * 4)

#enum #GWTerrainConfigs {
  DESERT = 1,
  JUNGLE,
  STEPPE,
  LEAFY_FOREST,
  EVERGREEN_FOREST,
  TUNDRA, // this is actually snow, wtf?!
  GLACIER,
  OCEAN,
  // special pseudoterrain which contains worldmap options
  // initially not defined, but can be overriden in modules
  WORLDMAP_OPTIONS := 64,
};

//#define _HEXNUM_X_  0x29a  // test


#bitenum #WalkabilityType {
  NO_MOVE = 0;
  WALK,
  SWIM,
  FLY,
  ETHEREAL,
  ANY_MOVE = 15,
};


#enum #RustType {
  NOT_RUSTED,
  SLIGHTLY_RUSTED,
  RUSTED,
  VERY_RUSTED,
};


/* contentscript<character> Flags */

#define IS_LEADER 1
#define IS_MASTER 2

#define DEPENDS_ON_ATTRIBUTES 65535

#define FOLLOW_PLAYER 1
#define FLEE_FROM_ENEMIES 2
#define DONT_CHANGE_EQUIPMENT 4
#define DONT_CONSUME_ANYTHING_VALUABLE 8

#define NO_PARAMETERS 255

#define GRAY_FRACTAL 0
#define RED_FRACTAL 1
#define GREEN_FRACTAL 2
#define BLUE_FRACTAL 3
#define YELLOW_FRACTAL 4

#bitenum #DamageKind {
  BLUNT,
  SLASH,
  PIERCE,
};


/* sparkle flags */
#bitenum #SparkleFlag {
  SKIN_COLOR,
  CAP_COLOR,
  HAIR_COLOR,
  EYE_COLOR,
  TORSO_MAIN_COLOR,
  BELT_COLOR,
  BOOT_COLOR,
  TORSO_SPECIAL_COLOR,
  ARM_MAIN_COLOR,
  GAUNTLET_COLOR,
  ARM_SPECIAL_COLOR,
  LEG_MAIN_COLOR,
  LEG_SPECIAL_COLOR,
};

#define CLOTH_COLOR (CAP_COLOR|TORSO_MAIN_COLOR|ARM_MAIN_COLOR|GAUNTLET_COLOR|LEG_MAIN_COLOR)


/*************************/
/* Common DataBase flags */
/*************************/

/* CommonFlags */
#define IS_ABSTRACT 1
#define HAS_SECONDARY_MATERIAL 2
#define CREATE_DIVINE_CONFIGURATIONS 4
#define CAN_BE_WISHED 8
#define CAN_BE_DESTROYED 16
#define IS_VALUABLE 32
#define CAN_BE_MIRRORED 64
#define CAN_BE_DETECTED 128

/* NameFlags */
#define USE_AN 1
#define USE_ADJECTIVE_AN 2
#define NO_ARTICLE 4
#define FORCE_THE 8
#define SHOW_MATERIAL 16

/***************************/
/* Material DataBase flags */
/***************************/

/* CommonFlags */
/* NameFlags (only USE_AN) */

/* CategoryFlags */
#define IS_METAL 1
#define IS_BLOOD 2
#define CAN_BE_TAILORED 4
#define IS_SPARKLING 8
#define IS_SCARY 16
#define IS_GOLEM_MATERIAL 32
#define IS_BEVERAGE 64

/* BodyFlags */
#define IS_ALIVE 1
#define IS_WARM 2
#define CAN_HAVE_PARASITE 4
#define USE_MATERIAL_ATTRIBUTES 8
#define CAN_REGENERATE 16
#define IS_WARM_BLOODED 32

/* InteractionFlags */
#define CAN_BURN 1
#define CAN_EXPLODE 2
#define CAN_DISSOLVE 4
#define AFFECT_INSIDE 8
#define EFFECT_IS_GOOD 16
#define IS_AFFECTED_BY_MUSTARD_GAS 32
// new in comm. fork
#define RISES_FROM_ASHES 64

/*************************/
/* End of DataBase flags */
/*************************/

/* number of amulets of live saving given to the player at the start of the game */
VAR BONUS_LIVES = 0;

/* room flags */

#define NO_MONSTER_GENERATION  1

#define NO_TAMING  -1

/* player alignment from `game::GetPlayerAlignment()` */
#define ALIGNMENT_EXTREMLY_LAWFUL   4
#define ALIGNMENT_VERY_LAWFUL       3
#define ALIGNMENT_LAWFUL            2
#define ALIGNMENT_MILDLY_LAWFUL     1
#define ALIGNMENT_NEUTRAL           0
#define ALIGNMENT_MILDLY_CHAOTIC   -1
#define ALIGNMENT_CHAOTIC          -2
#define ALIGNMENT_VERY_CHAOTIC     -3
#define ALIGNMENT_EXTREMLY_CHAOTIC -4


/* for the engine */
#enum {
  HAS_HIT,
  HAS_BLOCKED,
  HAS_DODGED,
  HAS_DIED,
  DID_NO_DAMAGE,
  HAS_FAILED,
};
