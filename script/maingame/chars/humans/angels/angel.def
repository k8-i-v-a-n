angel
{
  DefaultArmStrength = 25;
  DefaultLegStrength = 25;
  DefaultDexterity = 25;
  DefaultAgility = 25;
  DefaultEndurance = 25;
  DefaultPerception = 35;
  DefaultIntelligence = 25;
  DefaultWisdom = 35;
  DefaultCharisma = 50;
  DefaultMana = 35;
  TamingDifficulty = 30;
  Sex = FEMALE;
  TotalVolume = 60000;
  //!TorsoBitmapPos = 432, 0;
  TotalSize = 200;
  CanRead = true;
  NameSingular = "angel";
  UsesLongArticle = true;
  ClassStates = ESP|GAS_IMMUNITY|TELEPORT_CONTROL;
  SkinColor = rgb16(200, 200, 200);
  HairColor = rgb16(180, 180, 0);
  EyeColor = rgb16(48, 48, 255);
  HeadBitmapPos = 112, 256;
  TorsoBitmapPos = 48, 256;
  ArmBitmapPos = 80, 256;
  /* LegBitmapPos is not used */
  CreateDivineConfigurations = true;
  IsAbstract = true;
  /* Equipment initialization overridden */
  PanicLevel = 0;
  BaseUnarmedStrength = 200;
  HostileReplies :=
  {
    "\"Repent!\"",
    "\"Wrath of @Gd be upon thee!\"",
    "\"With the power of @Gd, I shall slay thee, sinner!\"";
  }
  FriendlyReplies :=
  {
    "\"Do not be afraid!\"",
    "\"@Gd be with you, mortal.\"",
    "\"@Gd is in the Heavens. All is right over the world.\"",
    "\"Even though you walk through the valley of the shadow of death, you shall fear no one for @Gd is at your side.\"",
    "\"I am but a humble servant of @Gd.\"",
    "\"You are not worthy of @Gd, mortal. Alas, gods need your prayers badly.\"",
    "\"I was created to sing praises on @Gd. I am destined to die for @Gd. All hail @Gd!\"";
  }
  DeathMessage = "@Dd leaves this mortal plane behind.";
  StandVerb = "flying";
  AttachedGod = NONE;
  BodyPartsDisappearWhenSevered = true;
  CanBeConfused = false;
  WieldedPosition = 0, -2;
  MoveType = FLY;
  UsesNutrition = false;
  IsPolymorphable = false;
  ScienceTalkPossibility = 25;
  ScienceTalkIntelligenceModifier = 25;
  ScienceTalkWisdomModifier = 100;
  ScienceTalkIntelligenceRequirement = 10;
  ScienceTalkWisdomRequirement = 20;
  CanChoke = false;
  UndeadVersions = false;

  Config ATAVUS;
  {
    BodyArmor = ARCANITE bodyarmor(PLATE_MAIL);
    RightWielded = ARCANITE meleeweapon(HALBERD) { Enchantment = 1; }
    KnownCWeaponSkills == POLE_ARMS;
    CWeaponSkillHits == 350;
    RightSWeaponSkillHits = 100;
  }

  Config CLEPTIA;
  {
    IsSadist = true;
    RightWielded = whipofthievery { Enchantment = 1; }
    KnownCWeaponSkills == WHIPS;
    CWeaponSkillHits == 400;
    RightSWeaponSkillHits = 100;
    LeftSWeaponSkillHits = 100;
    Cloak = ANGEL_HAIR cloak;
    RightGauntlet = ANGEL_HAIR gauntlet;

  }

  Config CRUENTUS;
  {
    IsSadist = true;
    RightWielded = MITHRIL MITHRIL meleeweapon(TWO_HANDED_SCIMITAR) { Enchantment = 1; }
    KnownCWeaponSkills == LARGE_SWORDS;
    CWeaponSkillHits == 400;
    RightSWeaponSkillHits = 100;
  }

  Config DULCIS;
  {
    BodyArmor = ANGEL_HAIR bodyarmor(PLATE_MAIL);
    Cloak = ANGEL_HAIR cloak;
    RightGauntlet = ANGEL_HAIR gauntlet;
    RightWielded = MARBLE meleeweapon(HAMMER) { Enchantment = 2; }
    LeftWielded = MARBLE meleeweapon(HAMMER) { Enchantment = 2; }
    KnownCWeaponSkills == BLUNT_WEAPONS;
    CWeaponSkillHits == 350;
    RightSWeaponSkillHits = 100;
    LeftSWeaponSkillHits = 100;
  }

  Config INFUSCOR;
  {
    IsSadist = true;
    RightWielded = OCTIRON OCTIRON meleeweapon(QUARTER_STAFF);
    KnownCWeaponSkills == BLUNT_WEAPONS;
    CWeaponSkillHits == 400;
    RightSWeaponSkillHits = 100;
    Cloak = OMMEL_HAIR cloak { Enchantment = 1; }
  }

  Config LEGIFER;
  {
    BodyArmor = ILLITHIUM bodyarmor(CHAIN_MAIL);
    RightWielded = ILLITHIUM ILLITHIUM meleeweapon(KATANA);
    LeftWielded = ILLITHIUM shield;
    KnownCWeaponSkills = { 2, LARGE_SWORDS, SHIELDS; }
    CWeaponSkillHits = { 2, 500, 500; }
    //CWeaponSkillHits == 400;
    RightSWeaponSkillHits = 150;
  }

  Config LORICATUS;
  {
    RightWielded = MITHRIL meleeweapon(WAR_HAMMER) { Enchantment = 1; }
    LeftWielded = MITHRIL meleeweapon(WAR_HAMMER) { Enchantment = 1; }
    KnownCWeaponSkills == BLUNT_WEAPONS;
    CWeaponSkillHits == 300;
    RightSWeaponSkillHits = 100;
    LeftSWeaponSkillHits = 100;
  }

  Config MELLIS;
  {
    IsSadist = true;
    BodyArmor = MITHRIL bodyarmor(CHAIN_MAIL);
    RightWielded = MITHRIL pickaxe { Enchantment = 1; }
    KnownCWeaponSkills == AXES;
    CWeaponSkillHits == 300;
    RightSWeaponSkillHits = 100;
  }

  Config MORTIFER;
  {
    RightWielded = OMMEL_TOOTH meleeweapon(SCYTHE) { Enchantment = 1; }
    Belt = BONE belt { Enchantment = 8; }
    EyeColor = rgb16(255, 48, 48);
    IsSadist = true;
    KnownCWeaponSkills == POLE_ARMS;
    CWeaponSkillHits == 500;
    RightSWeaponSkillHits = 200;
  }

  Config NEFAS;
  {
    IsSadist = true;
    IsMasochist = true;
    RightWielded = ANGEL_HAIR whip;
    LeftWielded = ANGEL_HAIR whip;
    KnownCWeaponSkills == WHIPS;
    CWeaponSkillHits == 400;
    RightSWeaponSkillHits = 100;
    LeftSWeaponSkillHits = 100;
    Belt = HARDENED_LEATHER belt;
    Cloak = HARDENED_LEATHER cloak;
    RightGauntlet = HARDENED_LEATHER gauntlet;
    HeadBitmapPos = 112, 240;
  }

  Config SCABIES;
  {
    IsSadist = true;
    RightWielded = ARCANITE daggerofvenom { Enchantment = 3; }
    LeftWielded = ARCANITE daggerofvenom { Enchantment = 3; }
    KnownCWeaponSkills == SMALL_SWORDS;
    CWeaponSkillHits == 400;
    RightSWeaponSkillHits = 100;
    LeftSWeaponSkillHits = 100;
    Cloak = ANGEL_HAIR cloak { Enchantment = 1; }
  }

  Config SEGES;
  {
    BodyArmor = ANGEL_HAIR bodyarmor(PLATE_MAIL);
    RightWielded = UNICORN_HORN UNICORN_HORN meleeweapon(DAGGER) { Enchantment = 4; }
    KnownCWeaponSkills == SMALL_SWORDS;
    CWeaponSkillHits == 300;
    RightSWeaponSkillHits = 100;
  }

  Config SILVA;
  {
    RightWielded = EBONY_WOOD EBONY_WOOD meleeweapon(QUARTER_STAFF) { Enchantment = 2; }
    KnownCWeaponSkills == BLUNT_WEAPONS;
    CWeaponSkillHits == 300;
    RightSWeaponSkillHits = 100;
  }

  Config SOPHOS;
  {
    RightWielded = OCTIRON meleeweapon(HAMMER) { Enchantment = 2; }
    LeftWielded = OCTIRON meleeweapon(HAMMER) { Enchantment = 2; }
    KnownCWeaponSkills == BLUNT_WEAPONS;
    CWeaponSkillHits == 350;
    RightSWeaponSkillHits = 100;
    LeftSWeaponSkillHits = 100;
  }

  Config VALPURUS;
  {
    RightWielded = VALPURIUM VALPURIUM meleeweapon(LONG_SWORD);
    LeftWielded = VALPURIUM shield;
    KnownCWeaponSkills = { 2, LARGE_SWORDS, SHIELDS; }
    CWeaponSkillHits = { 2, 500, 500; }
    //CWeaponSkillHits == 400;
    RightSWeaponSkillHits = 150;
  }
}
