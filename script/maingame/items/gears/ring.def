ring
{
  DefaultSize = 2;
  Category = RING;
  DefaultMainVolume = 3;
  StrengthModifier = 100;
  BitmapPos = 16, 400;
  FormModifier = 10;
  NameSingular = "ring";
  MainMaterialConfig == DIAMOND;
  Roundness = 10; /* it is considered here opened */
  IsAbstract = true;
  DrawOutlined = true;
  WieldedBitmapPos = 160, 368;
  TeleportPriority = 300;
  DescriptiveInfo = "A piece of jewellery imbued with arcane powers.";

  Config RING_OF_FIRE_RESISTANCE;
  {
    Possibility = 40;
    FireResistance = 15;
    PostFix = "of fire resistance";
    MainMaterialConfig == DRAGON_HIDE;
    Price = 250;
    AttachedGod = LORICATUS;
  }

  Config RING_OF_POLYMORPH_CONTROL;
  {
    Possibility = 5;
    PostFix = "of polymorph control";
    MainMaterialConfig == SAPPHIRE;
    GearStates = POLYMORPH_CONTROL;
    Price = 750;
    AttachedGod = SCABIES;
    Alias == "polycontrol";
  }

  Config RING_OF_INFRA_VISION;
  {
    Possibility = 10;
    PostFix = "of infravision";
    MainMaterialConfig == SILVER;
    GearStates = INFRA_VISION;
    Price = 500;
    AttachedGod = LEGIFER;
  }

  Config RING_OF_TELEPORTATION;
  {
    Possibility = 30;
    PostFix = "of teleportation";
    GearStates = TELEPORT;
    MainMaterialConfig == MITHRIL;
    Alias == "ring of teleport";
    Price = 250;
    WearWisdomLimit = 15;
    AttachedGod = SOPHOS;
  }

  Config RING_OF_TELEPORT_CONTROL;
  {
    Possibility = 10;
    PostFix = "of teleport control";
    MainMaterialConfig == RUBY;
    GearStates = TELEPORT_CONTROL;
    Price = 500;
    AttachedGod = SOPHOS;
    TeleportPriority = 1000;
    Alias == "telecontrol";
  }

  Config RING_OF_POLYMORPH;
  {
    Possibility = 30;
    PostFix = "of polymorph";
    MainMaterialConfig == ARCANITE;
    GearStates = POLYMORPH;
    Price = 250;
    WearWisdomLimit = 15;
    AttachedGod = SCABIES;
  }

  Config RING_OF_POISON_RESISTANCE;
  {
    Possibility = 30;
    PoisonResistance = 2;
    PostFix = "of poison resistance";
    MainMaterialConfig == TIN;
    Price = 250;
    AttachedGod = SEGES;
  }

  Config RING_OF_INVISIBILITY;
  {
    Possibility = 1;
    PostFix = "of invisibility";
    MainMaterialConfig == GLASS;
    GearStates = INVISIBLE;
    Price = 2500;
    AttachedGod = CLEPTIA;
  }

  Config RING_OF_ELECTRICITY_RESISTANCE;
  {
    Possibility = 30;
    ElectricityResistance = 15;
    PostFix = "of electricity resistance";
    MainMaterialConfig == EBONY_WOOD;
    Price = 250;
    AttachedGod = LORICATUS;
  }

  Config RING_OF_SEARCHING;
  {
    Possibility = 20;
    PostFix = "of searching";
    MainMaterialConfig == OAK_WOOD;
    Price = 500;
    AttachedGod = LEGIFER;
    GearStates = SEARCHING;
    DescriptiveInfo = "This ring bestows the diligent observational skills necessary to readily identify any hidden traps you might happen upon during your dungeoneering adventures.";
  }

  Config RING_OF_ACID_RESISTANCE;
  {
    Possibility = 20;
    AcidResistance = 2;
    PostFix = "of acid resistance";
    MainMaterialConfig == GOLD;
    Price = 250;
    AttachedGod = LORICATUS;
    TeleportPriority = 1000;
  }

  Config RING_OF_LIGHT;
  {
    Possibility = 2;
    PostFix = "of light";
    MainMaterialConfig == SUN_CRYSTAL;
    Price = 500;
    AttachedGod = LEGIFER;
  }

  Config RING_OF_MAGIC_RESISTANCE;
  {
    Possibility = 0;
    EnergyResistance = 15;
    PostFix = "of magic resistance";
    MainMaterialConfig == OCTIRON;
    Price = 250;
    AttachedGod = INFUSCOR;
    SirenSongResistance = 8; //k8: default is 4
  }

  Config RING_OF_DETECTION;
  {
    Possibility = 10;
    PostFix = "of detection";
    MainMaterialConfig == OBSIDIAN;
    Price = 500;
    AttachedGod = SOPHOS;
    GearStates = DETECTING;
    Alias == "ring of detecting";
  }

  Config RING_OF_POLYMORPH_LOCK;
  {
    Possibility = 0;
    PostFix = "of unchanging";
    MainMaterialConfig == ICE;
    GearStates = POLYMORPH_LOCK;
    Price = 2500;
    AttachedGod = SEGES;
    TeleportPriority = 1000;
    Alias == "ring of polymorph lock";
    CanBeWished = false;
    IsPolymorphSpawnable = false;
    CanBeCloned = false;
    CanBeMirrored = true;
  }

  Config RING_OF_WORM;
  {
    Possibility = 1;
    AcidResistance = 100;
    PoisonResistance = 100;
    NameSingular = "mutated symbiotic worm-ring";
    MainMaterialConfig == MIND_WORM_FLESH; /*URANIUM?*/
    Price = 2500;
    AttachedGod = SCABIES;
    GearStates = PARASITE_TAPE_WORM|DISEASE_IMMUNITY;
    TeleportPriority = 500;
    ArticleMode = FORCE_THE;
    CanBeWished = false;
    IsMaterialChangeable = false;
    IsPolymorphable = false;
    IsDestroyable = false;
    CanBeCloned = false;
    CanBeMirrored = true;
    CanBeBroken = false;
    CanBeBurned = false;
    CanBePiled = false;
    Alias == "worm ring";
    DescriptiveInfo = "A coiled worm transmuted into a sickly glowing ring. It whispers promises of safety into your mind, but also nibbles at your hand with a barely restrained hunger.";
  }

  Config RING_OF_BRAVERY;
  {
    Possibility = 15;
    PostFix = "of bravery";
    MainMaterialConfig == STEEL;
    Price = 250;
    AttachedGod = CRUENTUS;
    GearStates = FEARLESS;
    Alias = { 2, "fearlessness", "bravery"; }
  }

  Config RING_OF_SPEED;
  {
    Possibility = 0; /* Artifact held by One-eyed Sam the black marketeer. */
    PostFix = "of ludicrous speed";
    MainMaterialConfig == QUICK_SILVER;
    Price = 2500;
    AttachedGod = CLEPTIA;
    GearStates = HASTE;
    TeleportPriority = 500;
    ArticleMode = FORCE_THE;
    CanBeWished = false;
    IsMaterialChangeable = false;
    IsPolymorphable = false;
    IsPolymorphSpawnable = false;
    IsDestroyable = false;
    CanBeCloned = false;
    CanBeMirrored = true;
    CanBeBroken = false;
    CanBeBurned = false;
    CanBePiled = false;
    Alias = { 2, "speed", "haste"; }
  }
}
