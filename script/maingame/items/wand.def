wand
{
  Category = WAND;
  DefaultMainVolume = 125;
  StrengthModifier = 50;
  BitmapPos = 0, 288;
  FormModifier = 30;
  DefaultSize = 30;
  NameSingular = "wand";
  IsAbstract = true;
  Roundness = 5;
  CanBePiled = false;
  DrawOutlined = true;
  BreakEffectRangeSquare = 2;
  WieldedBitmapPos = 160, 352;
  TeleportPriority = 1000;
  DescriptiveInfo = "A fragile rod filled to the brim with magic.";

  Config WAND_OF_ACID_RAIN;
  {
    Possibility = 30;
    Price = 500;
    PostFix = "of acid rain";
    MainMaterialConfig == SULFUR;
    BeamRange = 15;
    MinCharges = 2;
    MaxCharges = 5;
    BeamColor = GREEN;
    BeamEffect = BEAM_ACID_RAIN;
    AttachedGod = SCABIES;
    BreakEffectRangeSquare = 1;
    IsKamikazeWeapon = true;
    BreakMsg = "vaporizes in an instant, unleashing a deadly rainstorm of polluted acid";
    DescriptiveInfo = "The magic of this wand catalyses a reaction between the water vapors in the air and the naturally occurring nitrogen. The resulting nitric acid condenses into larger particles, forming a caustic shower that wreaks havoc on anything caught inside.";
  }

  Config WAND_OF_CLONING;
  {
    Possibility = 5;
    Price = 2000;
    IsPolymorphSpawnable = false;
    PostFix = "of cloning";
    MainMaterialConfig == RUBY;
    BeamRange = 5;
    MinCharges = 1;
    MaxCharges = 3;
    CanBeCloned = false;
    BeamColor = PINK;
    BeamEffect = BEAM_DUPLICATE;
    AttachedGod = INFUSCOR;
    BreakEffectRangeSquare = 0;
    BreakMsg = "breaks up, its dispersing splinters multiplying infinitely";
    DescriptiveInfo = "The pinnacle of alchemical expertise, this wand allows one to fabricate a perfect, permanent duplicate of nearly anything imaginable.";
  }

  Config WAND_OF_DOOR_CREATION;
  {
    Possibility = 20;
    Price = 250;
    PostFix = "of door creation";
    MainMaterialConfig == OAK_WOOD;
    /* BeamRange has no effect */
    MinCharges = 3;
    MaxCharges = 6;
    BeamColor = LIGHT_GRAY;
    BeamEffect = BEAM_DOOR_CREATION;
    BeamStyle = SHIELD_BEAM;
    AttachedGod = LORICATUS;
    BreakMsg = "releases its powers, causing a number of highly unexpected effects to occur around itself";
    DescriptiveInfo = "Transmuting the surrounding matter and focusing it through a Platonic ideal of a gateway, this wand can manifest door out of the proverbial thin air.";
  }

  Config WAND_OF_FIRE_BALLS;
  {
    Possibility = 40;
    Price = 500;
    PostFix = "of fireballs";
    MainMaterialConfig == BRONZE;
    BeamRange = 50;
    MinCharges = 1;
    MaxCharges = 4;
    BeamColor = YELLOW;
    BeamEffect = BEAM_FIRE_BALL;
    AttachedGod = INFUSCOR;
    BreakEffectRangeSquare = 0;
    IsKamikazeWeapon = true;
    BreakMsg = "turns into a fiery ball of rapidly expanding plasma";
    DescriptiveInfo = "A fire elemental is trapped inside and tormented every time you zap this wand. Its wrath is then channeled into a mighty explosion. It might not be very understanding should it ever break free.";
  }

  Config WAND_OF_HASTE;
  {
    Possibility = 40;
    Price = 500;
    PostFix = "of haste";
    MainMaterialConfig == TIN;
    BeamRange = 5;
    MinCharges = 3;
    MaxCharges = 6;
    BeamColor = RED;
    BeamEffect = BEAM_HASTE;
    AttachedGod = CLEPTIA;
    BreakMsg = "breaks, speeding up everything around it";
    DescriptiveInfo = "A bit of extra time saved up for when you really need it.";
  }

  Config WAND_OF_INVISIBILITY;
  {
    Possibility = 40;
    Price = 250;
    PostFix = "of invisibility";
    MainMaterialConfig == GLASS;
    BeamRange = 10;
    MinCharges = 3;
    MaxCharges = 6;
    BeamColor = TRANSPARENT_COLOR; /* invisible */
    BeamEffect = BEAM_INVISIBILITY;
    AttachedGod = CLEPTIA;
    BreakMsg = "becomes increasingly transparent and then vanishes abruptly";
  }

  Config WAND_OF_LIGHTNING;
  {
    Possibility = 60;
    Price = 250;
    PostFix = "of lightning";
    MainMaterialConfig == GOLD;
    BeamRange = 15;
    MinCharges = 2;
    MaxCharges = 5;
    BeamColor = WHITE;
    BeamEffect = BEAM_LIGHTNING;
    BeamStyle = LIGHTNING_BEAM;
    AttachedGod = INFUSCOR;
    BreakEffectRangeSquare = 9;
    IsKamikazeWeapon = true;
    BreakMsg = "breaks and releases all of its stored electricity at once";
    DescriptiveInfo = "The actinic glare of a lightning bolt searing through a darkened corridor would be enough to strike terror in the most stout of warrior. One can only imagine their terror when the bolt, which seemed to just miss them, bounces off a wall to strike them from behind! In the past, massive thunderstorm farms were necessary to produce and harvest the lightning stored in this wand. Nowadays, the progress in the techniques of hattifattener breeding allowes for much quicker, cheaper and more environmental friendly manufacture.";
  }

  Config WAND_OF_MIRRORING;
  {
    Possibility = 15;
    Price = 1000;
    IsPolymorphSpawnable = false;
    PostFix = "of mirroring";
    MainMaterialConfig == SAPPHIRE;
    BeamRange = 5;
    MinCharges = 1;
    MaxCharges = 3;
    CanBeCloned = false;
    BeamColor = PINK;
    BeamEffect = BEAM_DUPLICATE; /* difference from cloning overridden */
    AttachedGod = INFUSCOR;
    BreakEffectRangeSquare = 0;
    BreakMsg = "shatters, its slivers reflecting each other recursively ad infinitum";
    DescriptiveInfo = "The enchantments of this wand construct an astral simulacrum of the target. Unfortunately, the false matter of the duplicate is only metastable and will eventually discorporate.";
  }

  Config WAND_OF_NECROMANCY;
  {
    Possibility = 30;
    Price = 500;
    PostFix = "of necromancy";
    MainMaterialConfig == BONE;
    BeamRange = 5;
    MinCharges = 2;
    MaxCharges = 5;
    BeamColor = BLACK;
    BeamEffect = BEAM_NECROMANCY;
    AttachedGod = INFUSCOR;
    BreakMsg = "emits a gross of unholy rays and self-obliterates";
    DescriptiveInfo = "Dabblers who lack the motivation or ability to learn proper incantations use this wand to infuse the fallen bodies of their foes with a sick parody of life. Contemporary necromantic science is unfortunately unable to induce undeath in non-humanoid corpses, though at least one research team has recently reported a successful animation of a puppy skeleton.";
  }

  Config WAND_OF_POLYMORPH;
  {
    Possibility = 20;
    Price = 500;
    IsPolymorphSpawnable = false;
    PostFix = "of polymorph";
    MainMaterialConfig == OCTIRON;
    BeamRange = 5;
    MinCharges = 2;
    MaxCharges = 5;
    BeamColor = RANDOM_COLOR;
    BeamEffect = BEAM_POLYMORPH;
    CanBeCloned = false;
    AttachedGod = SCABIES;
    IsKamikazeWeapon = true;
    BreakMsg = "shatters, its pieces transforming into a myriad of exotic forms before vanishing";
    DescriptiveInfo = "The target of this wand is not really transformed. It is shunted to a pocket dimension while the enchantments shift through a myriad of quantum variations on this reality, substantiating what the target could have been if a tiny change was introduced millions of years ago.";
  }

  Config WAND_OF_RESURRECTION;
  {
    Possibility = 20;
    Price = 750;
    PostFix = "of resurrection";
    MainMaterialConfig == PHOENIX_FEATHER;
    BeamRange = 5;
    MinCharges = 1;
    MaxCharges = 3;
    BeamColor = BLACK;
    BeamEffect = BEAM_RESURRECT;
    AttachedGod = SEGES;
    BreakMsg = "breaks, releasing countless green rays of highly compressed life energy";
    DescriptiveInfo = "Filled with life energy drained from a thousand sacrifices and capable of bringing back the dead, this wand is rare and powerful. All claims of permanent brain damage, demonic possession or other side effects of resurrection are unsubstantiated.";
  }

  Config WAND_OF_SLOW;
  {
    Possibility = 40;
    Price = 500;
    PostFix = "of slow";
    MainMaterialConfig == GRANITE;
    BeamRange = 5;
    MinCharges = 3;
    MaxCharges = 6;
    BeamColor = BLUE;
    BeamEffect = BEAM_SLOW;
    AttachedGod = INFUSCOR;
    BreakMsg = "breaks, dilating time around it";
  }

  Config WAND_OF_STRIKING;
  {
    Possibility = 30;
    Price = 500;
    PostFix = "of striking";
    MainMaterialConfig == IRON;
    BeamRange = 10;
    MinCharges = 2;
    MaxCharges = 5;
    BeamColor = WHITE;
    BeamEffect = BEAM_STRIKE;
    AttachedGod = INFUSCOR;
    IsKamikazeWeapon = true;
    BreakMsg = "explodes, releasing all of its energy at once";
    DescriptiveInfo = "Loaded with raw destructive magic, this wand is highly popular among the wandslingers in the western lands.";
  }

  Config WAND_OF_TELEPORTATION;
  {
    Possibility = 60;
    Price = 500;
    PostFix = "of teleportation";
    MainMaterialConfig == SILVER;
    Alias == "wand of teleport";
    BeamRange = 5;
    MinCharges = 3;
    MaxCharges = 6;
    BeamColor = BLUE;
    BeamEffect = BEAM_TELEPORT;
    AttachedGod = SOPHOS;
    BreakEffectRangeSquare = 9;
    BreakMsg = "breaks apart and a thousand splinters scatter in random directions, each cutting a tiny hole in space and time";
    DescriptiveInfo = "A hole was torn in the space-time continuum and then sealed in this wand. It's absolutely, perfectly, hundred percent safe. Really.";
  }

  Config WAND_OF_WEBBING;
  {
    Possibility = 20;
    Price = 500;
    PostFix = "of webbing";
    MainMaterialConfig == SPIDER_SILK;
    BeamRange = 5;
    MinCharges = 3;
    MaxCharges = 6;
    BeamColor = WHITE;
    BeamEffect = BEAM_WEBBING;
    AttachedGod = SCABIES;
    BreakMsg = "breaks, sending strands of spider silk everywhere";
    DescriptiveInfo = "When you zap this wand, a spray of special high-pressure shear-thinning liquid is ejected from an extradimensional reservoir. On contact with air, this long-chain polymer rapidly knits and forms an extremely tough webbing.";
  }

  Config WAND_OF_ALCHEMY;
  {
    Possibility = 5;
    Price = 1000;
    PostFix = "of alchemy";
    MainMaterialConfig == LEAD;
    BeamRange = 5;
    MinCharges = 1;
    MaxCharges = 3;
    BeamColor = YELLOW;
    BeamEffect = BEAM_ALCHEMY;
    AttachedGod = MELLIS;
    BreakMsg = "explodes in a puff of golden dust";
    DescriptiveInfo = "Monetary value, as every scholar of Mellis knows, is an intrinsic quality of every object, just as its temperature or mass. This wand can dispose of the unnecessary substance of an item, transferring its value to you directly.";
  }

  Config WAND_OF_SOFTEN_MATERIAL;
  {
    Possibility = 5;
    Price = 500;
    PostFix = "of soften material";
    MainMaterialConfig == FUNGI_WOOD;
    BeamRange = 5;
    MinCharges = 2;
    MaxCharges = 5;
    BeamColor = YELLOW;
    BeamEffect = BEAM_SOFTEN_MATERIAL;
    AttachedGod = SCABIES; /* Change to SOLICITUS? */
    BreakMsg = "turns itself into dust";
    DescriptiveInfo = "Inscribed with the same magic formula as a scroll of harden material, except backwards.";
  }
}
