/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_ALLOCATE_H__
#define __FELIB_ALLOCATE_H__

#include "typedef.h"


template <class type> inline void Alloc2D (type **&Map, int XSize, int YSize) {
  if (XSize < 0 || YSize < 0) __builtin_trap();
  cint Size = XSize*(sizeof(type *)+YSize*sizeof(type));
  Map = reinterpret_cast<type **>(new char[Size]);
  type *XPointer = reinterpret_cast<type *>(Map+XSize);
  for (int x = 0; x < XSize; ++x, XPointer += YSize) {
    Map[x] = XPointer;
    #if 1
    memset(XPointer, 0, (unsigned)YSize * sizeof(type));
    #endif
  }
}


template <class type> inline void Alloc2D (type **&Map, int XSize, int YSize, const type &Initializer) {
  if (XSize < 0 || YSize < 0) __builtin_trap();
  cint Size = XSize*(sizeof(type *)+YSize*sizeof(type));
  Map = reinterpret_cast<type **>(new char[Size]);
  type* XPointer = reinterpret_cast<type *>(Map+XSize);
  for (int x = 0; x < XSize; ++x, XPointer += YSize) {
    Map[x] = XPointer;
    for (int y = 0; y < YSize; ++y) {
      Map[x][y] = Initializer;
    }
  }
}


template <class type> inline void Alloc3D (type ***&Map, int XSize, int YSize, int ZSize) {
  if (XSize < 0 || YSize < 0 || ZSize < 0) __builtin_trap();
  cint Size = XSize*(sizeof(type **)+YSize*(sizeof(type *)+ZSize*sizeof(type)));
  Map = reinterpret_cast<type ***>(new char[Size]);
  type **XPointer = reinterpret_cast<type **>(Map+XSize);
  type *YPointer = reinterpret_cast<type *>(XPointer+XSize*YSize);
  for (int x = 0; x < XSize; ++x, XPointer += YSize) {
    Map[x] = XPointer;
    for (int y = 0; y < YSize; ++y, YPointer += ZSize) {
      Map[x][y] = YPointer;
      #if 1
      memset(YPointer, 0, (unsigned)ZSize * sizeof(type));
      #endif
    }
  }
}


template <class type> inline void Alloc3D (type ***&Map, int XSize, int YSize, int ZSize, const type &Initializer) {
  if (XSize < 0 || YSize < 0 || ZSize < 0) __builtin_trap();
  cint Size = XSize*(sizeof(type **)+YSize*(sizeof(type *)+ZSize*sizeof(type)));
  Map = reinterpret_cast<type ***>(new char[Size]);
  type **XPointer = reinterpret_cast<type **>(Map+XSize);
  type *YPointer = reinterpret_cast<type *>(XPointer+XSize*YSize);
  for (int x = 0; x < XSize; ++x, XPointer += YSize) {
    Map[x] = XPointer;
    for (int y = 0; y < YSize; ++y, YPointer += ZSize) {
      Map[x][y] = YPointer;
      for (int z = 0; z < ZSize; ++z) {
        Map[x][y][z] = Initializer;
      }
    }
  }
}


#endif
