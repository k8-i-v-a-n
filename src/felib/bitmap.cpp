/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include <stdint.h>
#include <cmath>
#include <ctime>
#include <png.h>
/*#include <zlib.h>*/

#include "vwfileio.h"
#include "bitmap.h"
#include "graphics.h"
#include "fesave.h"
#include "allocate.h"
#include "femath.h"
#include "rawbit.h"


/*
 * Blitting must be as fast as possible, even if no optimizations are used;
 * therefore we can't use inline functions inside loops, since they may be
 * left unexpanded. These macros will do the job efficiently, even if they
 * are rather ugly
 */
#define LOAD_SRC()     int SrcCol = *SrcPtr;
#define LOAD_DEST()    int DestCol = *DestPtr;
#define LOAD_ALPHA()   int Alpha = *AlphaPtr;
#define STORE_COLOR()  *DestPtr = Red | Green | Blue;

#define NEW_LUMINATE_RED() \
  int Red = (SrcCol & 0xF800)+NewRedLuminance;\
  if (Red >= 0) { if(Red > 0xF800) Red = 0xF800; } else Red = 0;

#define NEW_LUMINATE_GREEN() \
  int Green = (SrcCol & 0x7E0)+NewGreenLuminance;\
  if (Green >= 0) { if(Green > 0x7E0) Green = 0x7E0; } else Green = 0;

#define NEW_LUMINATE_BLUE() \
  int Blue = (SrcCol & 0x1F)+NewBlueLuminance;\
  if (Blue >= 0) { if(Blue > 0x1F) Blue = 0x1F; } else Blue = 0;

#define NEW_APPLY_ALPHA_RED() {\
  int DestRed = (DestCol & 0xF800);\
  Red = (((Red - DestRed) * Alpha >> 8) + DestRed) & 0xF800;\
}

#define NEW_APPLY_ALPHA_GREEN() {\
  int DestGreen = (DestCol & 0x7E0);\
  Green = (((Green - DestGreen) * Alpha >> 8) + DestGreen) & 0x7E0;\
}

#define NEW_APPLY_ALPHA_BLUE() {\
  int DestBlue = (DestCol & 0x1F);\
  Blue = ((Blue - DestBlue) * Alpha >> 8) + DestBlue;\
}

#define NEW_LOAD_AND_APPLY_ALPHA_RED()\
  int Red;\
  {\
    int DestRed = DestCol & 0xF800;\
    Red = ((((SrcCol & 0xF800) - DestRed) * Alpha >> 8) + DestRed) & 0xF800;\
  }

#define NEW_LOAD_AND_APPLY_ALPHA_GREEN()\
  int Green;\
  {\
    int DestGreen = DestCol & 0x7E0;\
    Green = ((((SrcCol & 0x7E0) - DestGreen) * Alpha >> 8) + DestGreen) & 0x7E0;\
  }

#define NEW_LOAD_AND_APPLY_ALPHA_BLUE()\
  int Blue;\
  {\
    int DestBlue = DestCol & 0x1F;\
    Blue = (((SrcCol & 0x1F) - DestBlue) * Alpha >> 8) + DestBlue;\
  }


// ////////////////////////////////////////////////////////////////////////// //
// RawBitmapScreenShot
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  RawBitmapScreenShot::RawBitmapScreenShot
//
//==========================================================================
RawBitmapScreenShot::RawBitmapScreenShot (v2 asize, const uChar *rgb)
  : size(0, 0)
  , rgb16(0)
{
  IvanAssert(asize.X > 0 && asize.X <= 8192);
  IvanAssert(asize.Y > 0 && asize.Y <= 8192);
  IvanAssert(rgb);
  size = asize;
  Pack16(rgb);
}


//==========================================================================
//
//  RawBitmapScreenShot::RawBitmapScreenShot
//
//==========================================================================
RawBitmapScreenShot::RawBitmapScreenShot (SQArchive *sqa, SQAFile fl) {
  if (!sqa || !fl) return;

  char sign[8];
  sqa->FileRead(fl, sign, 8);
  if (sqa->WasError()) return;
  if (memcmp(sign, "RAWBMP00", 8) != 0) return;

  sqa->FileRead(fl, &size.X, sizeof(size.X));
  sqa->FileRead(fl, &size.Y, sizeof(size.Y));
  if (sqa->WasError()) { size = v2(0, 0); return; }
  if (size.X < 1 || size.Y < 1 || size.X > 8192 || size.Y > 8192) { size = v2(0, 0); return; }

  rgb16 = new uChar[size.Y * size.X * 2];
  sqa->FileRead(fl, rgb16, (size_t)(size.Y * size.X * 2));
  if (sqa->WasError()) {
    size = v2(0, 0);
    delete [] rgb16;
    return;
  }
}


//==========================================================================
//
//  RawBitmapScreenShot::~RawBitmapScreenShot
//
//==========================================================================
RawBitmapScreenShot::~RawBitmapScreenShot () {
  delete [] rgb16; rgb16 = 0;
  size = v2(0, 0);
}


//==========================================================================
//
//  RawBitmapScreenShot::Pack16
//
//==========================================================================
void RawBitmapScreenShot::Pack16 (const uChar *rgb) {
  IvanAssert(size.X > 0 && size.X <= 8192);
  IvanAssert(size.Y > 0 && size.Y <= 8192);
  delete [] rgb16; rgb16 = 0;

  rgb16 = new uChar[size.Y * size.X * 2];
  uChar *dest = rgb16;

  for (int f = 0; f != size.X * size.Y; f += 1) {
    *(uint16_t *)dest = (uint16_t)MakeRGB16(rgb[0], rgb[1], rgb[2]);
    rgb += 3; dest += 2;
  }
}


//==========================================================================
//
//  RawBitmapScreenShot::Unpack16
//
//==========================================================================
uChar *RawBitmapScreenShot::Unpack16 () {
  IvanAssert(size.X > 0 && size.X <= 8192);
  IvanAssert(size.Y > 0 && size.Y <= 8192);
  IvanAssert(rgb16);

  uChar *rgb = new uChar[size.Y * size.X * 3];
  uChar *dest = rgb;
  const uChar *src = rgb16;

  for (int f = 0; f != size.X * size.Y; f += 1) {
    uint16_t c = *(const uint16_t *)src;
    dest[0] = GetRed16(c);
    dest[1] = GetGreen16(c);
    dest[2] = GetBlue16(c);
    src += 2; dest += 3;
  }

  return rgb;
}


//==========================================================================
//
//  RawBitmapScreenShot::Save
//
//==========================================================================
void RawBitmapScreenShot::Save (SQArchive *sqa, SQAFile fl) {
  if (!sqa || !fl) return;
  sqa->FileWrite(fl, "RAWBMP00", 8);
  if (rgb16 && size.X > 0 && size.X <= 8192 && size.Y > 0 && size.Y <= 8192) {
    sqa->FileWrite(fl, &size.X, sizeof(size.X));
    sqa->FileWrite(fl, &size.Y, sizeof(size.Y));
    sqa->FileWrite(fl, rgb16, (size_t)size.X * size.Y * 2);
  } else {
    size = v2(0, 0);
    sqa->FileWrite(fl, &size.X, sizeof(size.X));
    sqa->FileWrite(fl, &size.Y, sizeof(size.Y));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// bitmap
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  bitmap::bitmap
//
//==========================================================================
bitmap::bitmap (cfestring &FileName)
  : FastFlag(0)
  , AlphaMap(0)
  , PriorityMap(0)
  , RandMap(0)
{
  auto rawpic = rawbitmap(FileName);
  mSize = rawpic.GetSize();
  const uChar *Palette = rawpic.GetPalette();
  XSizeTimesYSize = mSize.X*mSize.Y;
  Alloc2D(Image, mSize.Y, mSize.X);
  packcol16 *Buffer = Image[0];
  const uChar *buf = rawpic.GetBuffer();
  for (int y = 0; y < mSize.Y; ++y) {
    for (int x = 0; x < mSize.X; ++x) {
      int Char1 = *buf++;
      int Char3 = Char1 + (Char1 << 1);
      *Buffer++ = (int(Palette[Char3] >> 3) << 11) |
                  (int(Palette[Char3 + 1] >> 2) << 5) |
                  int(Palette[Char3 + 2] >> 3);
    }
  }
}


//==========================================================================
//
//  bitmap::bitmap
//
//==========================================================================
bitmap::bitmap (cbitmap *Bitmap, int Flags, truth CopyAlpha)
  : mSize(Bitmap->mSize)
  , XSizeTimesYSize(Bitmap->XSizeTimesYSize)
  , FastFlag(0)
  , PriorityMap(0)
  , RandMap(0)
{
  Alloc2D(Image, mSize.Y, mSize.X);
  if (CopyAlpha && Bitmap->AlphaMap) {
    Alloc2D(AlphaMap, mSize.Y, mSize.X);
    Bitmap->BlitAndCopyAlpha(this, Flags);
  } else {
    AlphaMap = 0;
    if (!Flags) Bitmap->FastBlit(this); else Bitmap->NormalBlit(this, Flags);
  }
}


//==========================================================================
//
//  bitmap::bitmap
//
//==========================================================================
bitmap::bitmap (v2 aSize)
  : mSize(aSize)
  , XSizeTimesYSize(mSize.X * mSize.Y)
  , FastFlag(0)
  , AlphaMap(0)
  , PriorityMap(0)
  , RandMap(0)
{
  Alloc2D(Image, mSize.Y, mSize.X);
}


//==========================================================================
//
//  bitmap::bitmap
//
//==========================================================================
bitmap::bitmap (v2 aSize, col16 Color)
  : mSize(aSize)
  , XSizeTimesYSize(mSize.X*mSize.Y)
  , FastFlag(0)
  , AlphaMap(0)
  , PriorityMap(0)
  , RandMap(0)
{
  Alloc2D(Image, mSize.Y, mSize.X);
  ClearToColor(Color);
}


//==========================================================================
//
//  bitmap::~bitmap
//
//==========================================================================
bitmap::~bitmap () {
  delete [] Image;
  delete [] AlphaMap;
  delete [] PriorityMap;
  delete [] RandMap;
}


//==========================================================================
//
//  bitmap::Save
//
//==========================================================================
void bitmap::Save (outputfile &SaveFile) const {
  SaveFile.WriteBytes(reinterpret_cast<char *>(Image[0]), XSizeTimesYSize*sizeof(packcol16));
  if (AlphaMap) {
    SaveFile.Put(true);
    SaveFile.WriteBytes(reinterpret_cast<char *>(AlphaMap[0]), XSizeTimesYSize*sizeof(packalpha));
  } else {
    SaveFile.Put(false);
  }
  if (PriorityMap) {
    SaveFile.Put(true);
    SaveFile.WriteBytes(reinterpret_cast<char *>(PriorityMap[0]), XSizeTimesYSize*sizeof(packpriority));
  } else {
    SaveFile.Put(false);
  }
  SaveFile << uChar(FastFlag);
}


//==========================================================================
//
//  bitmap::Load
//
//==========================================================================
void bitmap::Load (inputfile &SaveFile) {
  SaveFile.ReadBytes(reinterpret_cast<char *>(Image[0]), XSizeTimesYSize*sizeof(packcol16));
  if (SaveFile.Get()) {
    Alloc2D(AlphaMap, mSize.Y, mSize.X);
    SaveFile.ReadBytes(reinterpret_cast<char *>(AlphaMap[0]), XSizeTimesYSize*sizeof(packalpha));
  }
  if (SaveFile.Get()) {
    Alloc2D(PriorityMap, mSize.Y, mSize.X);
    SaveFile.ReadBytes(reinterpret_cast<char *>(PriorityMap[0]), XSizeTimesYSize*sizeof(packpriority));
  }
  FastFlag = ReadType(uChar, SaveFile);
}


//==========================================================================
//
//  pngWrite
//
//==========================================================================
static void pngWrite (png_structp png_ptr, png_bytep data, png_size_t length) {
  FILE *fp = (FILE *)png_get_io_ptr(png_ptr);
  fwrite(data, length, 1, fp);
}


//==========================================================================
//
//  bitmap::SavePNG
//
//==========================================================================
void bitmap::SavePNG (cfestring &FileName) const {
  png_structp png_ptr;
  png_infop info_ptr;
  //int ret;
  //png_colorp palette;
  png_byte **row_pointers = NULL;
  png_ptr = NULL;
  info_ptr = NULL;
  //palette = NULL;
  //ret = -1;
  FILE *fp = fopen(FileName.CStr(), "wb");
  if (!fp) {
    ConLogf("ERROR creating PNG file '%s'!", FileName.CStr());
    return;
  }
  row_pointers = (png_byte **)malloc(mSize.Y*sizeof(png_byte *));
  IvanAssert(row_pointers);
  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL,NULL,NULL);
  IvanAssert(png_ptr);
  info_ptr = png_create_info_struct(png_ptr);
  IvanAssert(info_ptr);
  // setup custom writer function
  png_set_write_fn(png_ptr, (void *)fp, pngWrite, NULL);
  if (setjmp(png_jmpbuf(png_ptr))) {
    ConLogf("ERROR writing PNG file '%s'!", FileName.CStr());
    //FIXME: memleaks?
    png_destroy_write_struct(&png_ptr, &info_ptr);
    if (row_pointers) free(row_pointers);
    fclose(fp);
    return;
  }

  png_set_filter(png_ptr, 0, PNG_FILTER_NONE);
  png_set_compression_level(png_ptr, 9/*Z_BEST_COMPRESSION*/); //Z_DEFAULT_COMPRESSION // Z_NO_COMPRESSION

  png_set_IHDR(png_ptr, info_ptr, mSize.X, mSize.Y, 8,
               PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
               PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
  png_write_info(png_ptr, info_ptr);

  uChar *tcImg = (uChar *)malloc((mSize.X*3)*mSize.Y), *tc = tcImg;
  for (int y = 0; y < mSize.Y; y++) {
    row_pointers[y] = (png_bytep)tc;
    for (int x = 0; x < mSize.X; x++) {
      col16 Pixel = GetPixel(x, y);
      uint8_t r, g, b;
      RGB16ToRGB32(Pixel, &r, &g, &b);
      *tc++ = r;
      *tc++ = g;
      *tc++ = b;
    }
  }
  png_write_image(png_ptr, row_pointers);
  png_write_end(png_ptr, NULL);
  fclose(fp);
  free(tcImg);

  png_destroy_write_struct(&png_ptr, &info_ptr);
  if (row_pointers) free(row_pointers);
}


//==========================================================================
//
//  bitmap::createScaled
//
//==========================================================================
uChar *bitmap::createScaled (double scale, v2 *size) const {
  int newX = (int)((double)mSize.X*scale);
  int newY = (int)((double)mSize.Y*scale);
  if (newX < 1 || newY < 1) return 0;
  uChar *unp = new uChar[mSize.X * mSize.Y * 3];
  memset(unp, 0, mSize.X * mSize.Y * 3);

  // unpack image
  uChar *pp = unp;
  for (int y = 0; y != mSize.Y; y += 1) {
    for (int x = 0; x != mSize.X; x += 1) {
      col16 Pixel = GetPixel(x, y);
      uint8_t r = 0, g = 0, b = 0;
      RGB16ToRGB32(Pixel, &r, &g, &b);
      *pp++ = r;
      *pp++ = g;
      *pp++ = b;
    }
  }
  IvanAssert(pp == unp + mSize.X * mSize.Y * 3);

  // now scale
  uChar *nbx = new uChar[newX * mSize.Y * 3];
  uChar *nb = new uChar[newX * newY * 3];
  const double sx = (double)mSize.X/(double)newX;
  const double sy = (double)mSize.Y/(double)newY;

  memset(nbx, 0, newX * mSize.Y * 3);
  memset(nb, 0, newX * newY * 3);

#define GETRGB(x_,y_,r_,g_,b_)  do { \
  r_ = unp[((y_)*mSize.X+(x_))*3+0]; \
  g_ = unp[((y_)*mSize.X+(x_))*3+1]; \
  b_ = unp[((y_)*mSize.X+(x_))*3+2]; \
} while (0)
  // first X
  {
    double cx;
    uChar *dst = nbx;
    for (int y = 0; y != mSize.Y; y += 1) {
      cx = 0;
      for (int x = 0; x != newX; x += 1, cx += sx) {
        double ix, rx;
        int px, r0, g0, b0, r1, g1, b1;
        rx = modf(cx, &ix);
        px = (int)ix;
        if (px >= 0 && px < mSize.X) {
          GETRGB(px, y, r0, g0, b0);
        } else {
          r0 = g0 = b0 = 0;
        }
        if (px >= 0 && px + 1 < mSize.X) {
          GETRGB(px + 1, y, r1, g1, b1);
          r0 = (int)((double)r0*(1.0-rx)+(double)r1*rx);
          g0 = (int)((double)g0*(1.0-rx)+(double)g1*rx);
          b0 = (int)((double)b0*(1.0-rx)+(double)b1*rx);
        }
        r0 = Clamp(r0, 0, 255);
        g0 = Clamp(g0, 0, 255);
        b0 = Clamp(b0, 0, 255);
        *dst++ = r0 & 0xff;
        *dst++ = g0 & 0xff;
        *dst++ = b0 & 0xff;
      }
    }
    IvanAssert(dst == nbx + newX * mSize.Y * 3);
  }
#undef GETRGB

#define GETRGB1(x_,y_,r_,g_,b_)  do { \
  r_ = nbx[((y_)*newX+(x_))*3+0]; \
  g_ = nbx[((y_)*newX+(x_))*3+1]; \
  b_ = nbx[((y_)*newX+(x_))*3+2]; \
} while (0)
  // now Y
  {
    double cx, cy = 0.0;
    uChar *dst = nb;
    for (int y = 0; y != newY; y += 1, cy += sy) {
      cx = 0;
      for (int x = 0; x != newX; x += 1, cx += sx) {
        double iy, ry;
        int py, r0, g0, b0, r1, g1, b1;
        ry = modf(cy, &iy);
        py = (int)iy;
        if (py >= 0 && py < mSize.Y) {
          GETRGB1(x, py, r0, g0, b0);
        } else {
          r0 = g0 = b0 = 0;
        }
        if (py >= 0 && py + 1 < mSize.Y) {
          GETRGB1(x, py + 1, r1, g1, b1);
          r0 = (int)((double)r0*(1.0-ry)+(double)r1*ry);
          g0 = (int)((double)g0*(1.0-ry)+(double)g1*ry);
          b0 = (int)((double)b0*(1.0-ry)+(double)b1*ry);
        }
        r0 = Clamp(r0, 0, 255);
        g0 = Clamp(g0, 0, 255);
        b0 = Clamp(b0, 0, 255);
        *dst++ = r0 & 0xff;
        *dst++ = g0 & 0xff;
        *dst++ = b0 & 0xff;
      }
    }
    IvanAssert(dst == nb + newX * newY * 3);
  }
#undef GETRGB1
  delete [] nbx;
  delete [] unp;

  // done
  if (size) *size = v2(newX, newY);
  return nb;
}


//==========================================================================
//
//  bitmap::CreateRaw
//
//==========================================================================
RawBitmapScreenShot *bitmap::CreateRaw (double scale) const {
  v2 size;
  uChar *rgb = createScaled(scale, &size);
  RawBitmapScreenShot *raw = new RawBitmapScreenShot(size, rgb);
  delete [] rgb;
  return raw;
}


//==========================================================================
//
//  bitmap::SetFromRaw
//
//==========================================================================
truth bitmap::SetFromRaw (RawBitmapScreenShot *raw) {
  if (!raw || !raw->rgb16 || raw->size.X < 1 || raw->size.Y < 1 ||
      raw->size.X > 8192 || raw->size.Y > 8192)
  {
    return false;
  }

  // set new image
  delete [] Image;
  delete [] AlphaMap;
  delete [] PriorityMap;
  delete [] RandMap;
  PriorityMap = 0;
  RandMap = 0;
  AlphaMap = 0;
  mSize.X = raw->size.X;
  mSize.Y = raw->size.Y;
  Alloc2D(Image, mSize.Y, mSize.X);
  //Alloc2D(AlphaMap, mSize.Y, mSize.X);
  const uChar *pp = raw->rgb16;
  for (int y = 0; y < mSize.Y; ++y) {
    for (int x = 0; x < mSize.X; ++x) {
      PutPixel(x, y, *(const uint16_t *)pp);
      pp += 2;
    }
  }

  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
void bitmap::Fill (v2 TopLeft, int Width, int Height, col16 Color) { Fill(TopLeft.X, TopLeft.Y, Width, Height, Color); }
void bitmap::Fill (int X, int Y, v2 FillSize, col16 Color) { Fill(X, Y, FillSize.X, FillSize.Y, Color); }
void bitmap::Fill (v2 TopLeft, v2 FillSize, col16 Color) { Fill(TopLeft.X, TopLeft.Y, FillSize.X, FillSize.Y, Color); }

//==========================================================================
//
//  bitmap::Fill
//
//==========================================================================
void bitmap::Fill (int X, int Y, int Width, int Height, col16 Color) {
  if (Width < 1 || Height < 1) return;
  if (X >= mSize.X || Y >= mSize.Y) return;
  if (X < 0) {
    Width += X;
    if (Width < 1) return;
    X = 0;
  }
  if (Y < 0) {
    Height += Y;
    if (Height < 1) return;
    Y = 0;
  }
  Width = Min(Width, mSize.X);
  Height = Min(Height, mSize.Y);
  if (X + Width > mSize.X) Width = mSize.X - X;
  if (Y + Height > mSize.Y) Height = mSize.Y - Y;
  if (Color >> 8 == (Color & 0xFF)) {
    Width <<= 1;
    for (int y = 0; y < Height; ++y) {
      memset(&Image[Y + y][X], Color, Width);
    }
  } else {
    for (int y = 0; y < Height; ++y) {
      packcol16 *Ptr = &Image[Y + y][X];
      cpackcol16 *const EndPtr = Ptr + Width;
      while (Ptr != EndPtr) {
        *Ptr++ = Color;
      }
    }
  }
}


//==========================================================================
//
//  bitmap::ClearToColor
//
//==========================================================================
void bitmap::ClearToColor (col16 Color) {
  packcol16 *Ptr = Image[0];
  if (Color >> 8 == (Color & 0xFF)) {
    memset(Ptr, Color, XSizeTimesYSize*sizeof(packcol16));
  } else {
    cpackcol16 *const EndPtr = Ptr + XSizeTimesYSize;
    while (Ptr != EndPtr) *Ptr++ = Color;
  }
}


//==========================================================================
//
//  bitmap::NormalBlit
//
//==========================================================================
void bitmap::NormalBlit (cblitdata &BlitData) const {
  blitdata B = BlitData;
  if (!FastFlag) {
    if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap blit attempt detected!");
    if ((B.Flags&ROTATE) && B.Border.X != B.Border.Y) ABORT("Blit error: FeLib supports only square rotating!");
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  }
  packcol16 **SrcImage = Image;
  packcol16 **DestImage = B.Bitmap->Image;
  switch (B.Flags&7) {
    case NONE: {
      if (!B.Src.X && !B.Src.Y && !B.Dest.X && !B.Dest.Y && B.Border.X == mSize.X && B.Border.Y == mSize.Y &&
          B.Border.X == B.Bitmap->mSize.X && B.Border.Y == B.Bitmap->mSize.Y)
      {
        memmove(DestImage[0], SrcImage[0], XSizeTimesYSize * sizeof(packcol16));
      } else {
        cint Bytes = B.Border.X * sizeof(packcol16);
        for (int y = 0; y < B.Border.Y; ++y) memmove(&DestImage[B.Dest.Y + y][B.Dest.X], &SrcImage[B.Src.Y + y][B.Src.X], Bytes);
      }
      break; }
    case MIRROR: {
      B.Dest.X += B.Border.X - 1;
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr) *DestPtr = *SrcPtr;
      }
      break; }
    case FLIP: {
      B.Dest.Y += B.Border.Y - 1;
      cint Bytes = B.Border.X * sizeof(packcol16);
      for (int y = 0; y < B.Border.Y; ++y) memmove(&DestImage[B.Dest.Y - y][B.Dest.X], &SrcImage[B.Src.Y + y][B.Src.X], Bytes);
      break; }
    case (MIRROR | FLIP): {
      B.Dest.X += B.Border.X - 1;
      B.Dest.Y += B.Border.Y - 1;
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = &DestImage[B.Dest.Y - y][B.Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr) *DestPtr = *SrcPtr;
      }
      break; }
    case ROTATE: {
      B.Dest.X += B.Border.X - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase - y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += TrueDestXMove) *DestPtr = *SrcPtr;
      }
      break; }
    case (MIRROR | ROTATE): {
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase + y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += TrueDestXMove) *DestPtr = *SrcPtr;
      }
      break; }
    case (FLIP | ROTATE): {
      B.Dest.X += B.Border.X - 1;
      B.Dest.Y += B.Border.Y - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase - y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= TrueDestXMove) *DestPtr = *SrcPtr;
      }
      break; }
    case (MIRROR | FLIP | ROTATE): {
      B.Dest.Y += B.Border.Y - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase + y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= TrueDestXMove) *DestPtr = *SrcPtr;
      }
      break; }
  }
}


//==========================================================================
//
//  XCopyPixel
//
//==========================================================================
static FORCE_INLINE void XCopyPixel (const packcol16 *Src, packcol16 *Dest, col16 MonoColor) {
  if (*Src != TRANSPARENT_COLOR) {
    *Dest = MonoColor;
  }
}


//==========================================================================
//
//  XCopyLine
//
//==========================================================================
static FORCE_INLINE void XCopyLine (const packcol16 *Src, packcol16 *Dest, int count, col16 MonoColor) {
  while (count > 0) {
    XCopyPixel(Src, Dest, MonoColor);
    Src += 1; Dest += 1;
    count -= 1;
  }
}


//==========================================================================
//
//  bitmap::BlitMono
//
//==========================================================================
void bitmap::BlitMono (cblitdata &BlitData, col16 MonoColor) const {
  if (MonoColor == TRANSPARENT_COLOR) return; // just in case
  blitdata B = BlitData;
  if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap blit attempt detected!");
  if ((B.Flags&ROTATE) && B.Border.X != B.Border.Y) ABORT("Blit error: FeLib supports only square rotating!");
  if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  packcol16 **SrcImage = Image;
  packcol16 **DestImage = B.Bitmap->Image;
  switch (B.Flags&7) {
    case NONE: {
      if (!B.Src.X && !B.Src.Y && !B.Dest.X && !B.Dest.Y && B.Border.X == mSize.X && B.Border.Y == mSize.Y &&
          B.Border.X == B.Bitmap->mSize.X && B.Border.Y == B.Bitmap->mSize.Y)
      {
        //memmove(DestImage[0], SrcImage[0], XSizeTimesYSize * sizeof(packcol16));
        for (size_t cnt = 0; cnt != XSizeTimesYSize; cnt += 1) {
          XCopyPixel(SrcImage[0] + cnt, DestImage[0] + cnt, MonoColor);
        }
      } else {
        cint Bytes = B.Border.X;// * sizeof(packcol16);
        for (int y = 0; y < B.Border.Y; ++y) {
          //memmove(&DestImage[B.Dest.Y + y][B.Dest.X], &SrcImage[B.Src.Y + y][B.Src.X], Bytes);
          for (int cnt = 0; cnt != Bytes; cnt += 1) {
            XCopyLine(&SrcImage[B.Src.Y + y][B.Src.X], &DestImage[B.Dest.Y + y][B.Dest.X], Bytes, MonoColor);
          }
        }
      }
      break; }
    case MIRROR: {
      B.Dest.X += B.Border.X - 1;
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr) {
          XCopyPixel(SrcPtr, DestPtr, MonoColor);
          //*DestPtr = *SrcPtr;
        }
      }
      break; }
    case FLIP: {
      B.Dest.Y += B.Border.Y - 1;
      cint Bytes = B.Border.X;// * sizeof(packcol16);
      for (int y = 0; y < B.Border.Y; ++y) {
        //memmove(&DestImage[B.Dest.Y - y][B.Dest.X], &SrcImage[B.Src.Y + y][B.Src.X], Bytes);
            XCopyLine(&SrcImage[B.Src.Y + y][B.Src.X], &DestImage[B.Dest.Y - y][B.Dest.X], Bytes, MonoColor);
      }
      break; }
    case (MIRROR | FLIP): {
      B.Dest.X += B.Border.X - 1;
      B.Dest.Y += B.Border.Y - 1;
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = &DestImage[B.Dest.Y - y][B.Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr) {
          //*DestPtr = *SrcPtr;
          XCopyPixel(SrcPtr, DestPtr, MonoColor);
        }
      }
      break; }
    case ROTATE: {
      B.Dest.X += B.Border.X - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase - y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += TrueDestXMove) {
          //*DestPtr = *SrcPtr;
          XCopyPixel(SrcPtr, DestPtr, MonoColor);
        }
      }
      break; }
    case (MIRROR | ROTATE): {
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase + y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += TrueDestXMove) {
          //*DestPtr = *SrcPtr;
          XCopyPixel(SrcPtr, DestPtr, MonoColor);
        }
      }
      break; }
    case (FLIP | ROTATE): {
      B.Dest.X += B.Border.X - 1;
      B.Dest.Y += B.Border.Y - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase - y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= TrueDestXMove) {
          //*DestPtr = *SrcPtr;
          XCopyPixel(SrcPtr, DestPtr, MonoColor);
        }
      }
      break; }
    case (MIRROR | FLIP | ROTATE): {
      B.Dest.Y += B.Border.Y - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase + y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= TrueDestXMove) {
          //*DestPtr = *SrcPtr;
          XCopyPixel(SrcPtr, DestPtr, MonoColor);
        }
      }
      break; }
  }
}


//==========================================================================
//
//  bitmap::LuminanceBlit
//
//==========================================================================
void bitmap::LuminanceBlit (cblitdata &BlitData) const {
  blitdata B = BlitData;
  if (B.Luminance == NORMAL_LUMINANCE) {
    B.Flags = 0;
    NormalBlit(B);
    return;
  }
  if (!FastFlag) {
    if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap blit attempt detected!");
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  }
  packcol16 **SrcImage = Image;
  packcol16 **DestImage = B.Bitmap->Image;
  int NewRedLuminance = (B.Luminance >> 7 & 0x1F800) - 0x10000;
  int NewGreenLuminance = (B.Luminance >> 4 & 0xFE0) - 0x800;
  int NewBlueLuminance = (B.Luminance >> 2 & 0x3F) - 0x20;
  for (int y = 0; y < B.Border.Y; ++y) {
    cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
    cpackcol16 *EndPtr = SrcPtr + B.Border.X;
    packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
    for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr) {
      LOAD_SRC();
      NEW_LUMINATE_RED();
      NEW_LUMINATE_GREEN();
      NEW_LUMINATE_BLUE();
      STORE_COLOR();
    }
  }
}


//==========================================================================
//
//  bitmap::NormalMaskedBlit
//
//==========================================================================
void bitmap::NormalMaskedBlit (cblitdata &BlitData) const {
  blitdata B = BlitData;
  if (!FastFlag) {
    if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap masked blit attempt detected!");
    if ((B.Flags&ROTATE) && B.Border.X != B.Border.Y) ABORT("MaskedBlit error: FeLib supports only square rotating!");
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  }
  packcol16 **SrcImage = Image;
  packcol16 **DestImage = B.Bitmap->Image;
  packcol16 PackedMaskColor = B.MaskColor;
  switch (B.Flags&7) {
    case NONE: {
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr) if (*SrcPtr != PackedMaskColor) *DestPtr = *SrcPtr;
      }
      break; }
    case MIRROR: {
      B.Dest.X += B.Border.X - 1;
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr) if (*SrcPtr != PackedMaskColor) *DestPtr = *SrcPtr;
      }
      break; }
    case FLIP: {
      B.Dest.Y += B.Border.Y - 1;
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = &DestImage[B.Dest.Y - y][B.Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr) if (*SrcPtr != PackedMaskColor) *DestPtr = *SrcPtr;
      }
      break; }
    case (MIRROR | FLIP): {
      B.Dest.X += B.Border.X - 1;
      B.Dest.Y += B.Border.Y - 1;
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = &DestImage[B.Dest.Y - y][B.Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr) if (*SrcPtr != PackedMaskColor) *DestPtr = *SrcPtr;
      }
      break; }
    case ROTATE: {
      B.Dest.X += B.Border.X - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase - y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += TrueDestXMove) if (*SrcPtr != PackedMaskColor) *DestPtr = *SrcPtr;
      }
      break; }
    case (MIRROR | ROTATE): {
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase + y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += TrueDestXMove) if (*SrcPtr != PackedMaskColor) *DestPtr = *SrcPtr;
      }
      break; }
    case (FLIP | ROTATE): {
      B.Dest.X += B.Border.X - 1;
      B.Dest.Y += B.Border.Y - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase - y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= TrueDestXMove) if (*SrcPtr != PackedMaskColor) *DestPtr = *SrcPtr;
      }
      break; }
    case (MIRROR | FLIP | ROTATE): {
      B.Dest.Y += B.Border.Y - 1;
      int TrueDestXMove = B.Bitmap->mSize.X;
      packcol16 *DestBase = &DestImage[B.Dest.Y][B.Dest.X];
      for (int y = 0; y < B.Border.Y; ++y) {
        cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
        cpackcol16 *EndPtr = SrcPtr + B.Border.X;
        packcol16 *DestPtr = DestBase + y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= TrueDestXMove) if (*SrcPtr != PackedMaskColor) *DestPtr = *SrcPtr;
      }
      break; }
  }
}


//==========================================================================
//
//  bitmap::LuminanceMaskedBlit
//
//==========================================================================
void bitmap::LuminanceMaskedBlit (cblitdata &BlitData) const {
  blitdata B = BlitData;
  if (B.Luminance == NORMAL_LUMINANCE) {
    B.Flags = 0;
    NormalMaskedBlit(B);
    return;
  }
  if (!FastFlag) {
    if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap masked blit attempt detected!");
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  }
  packcol16 **SrcImage = Image;
  packcol16 **DestImage = B.Bitmap->Image;
  int NewRedLuminance = (B.Luminance >> 7 & 0x1F800) - 0x10000;
  int NewGreenLuminance = (B.Luminance >> 4 & 0xFE0) - 0x800;
  int NewBlueLuminance = (B.Luminance >> 2 & 0x3F) - 0x20;
  for (int y = 0; y < B.Border.Y; ++y) {
    cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
    cpackcol16 *EndPtr = SrcPtr + B.Border.X;
    packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
    for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr) {
      LOAD_SRC();
      if (SrcCol != B.MaskColor) {
        NEW_LUMINATE_RED();
        NEW_LUMINATE_GREEN();
        NEW_LUMINATE_BLUE();
        STORE_COLOR();
      }
    }
  }
}


//==========================================================================
//
//  bitmap::SimpleAlphaBlit
//
//==========================================================================
void bitmap::SimpleAlphaBlit (bitmap *Bitmap, alpha Alpha, col16 MaskColor) const {
  if (Alpha == 255) {
    blitdata B = {
      Bitmap,
      { 0, 0 },
      { 0, 0 },
      { mSize.X, mSize.Y },
      { 0 },
      MaskColor,
      0
    };
    NormalMaskedBlit(B);
    return;
  }
  if (!FastFlag && (mSize.X != Bitmap->mSize.X || mSize.Y != Bitmap->mSize.Y)) ABORT("Fast simple alpha blit attempt of noncongruent bitmaps detected!");
  cpackcol16 *SrcPtr = Image[0];
  cpackcol16 *EndPtr = SrcPtr + XSizeTimesYSize;
  packcol16 *DestPtr = Bitmap->Image[0];
  for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr) {
    LOAD_SRC();
    if (SrcCol != MaskColor) {
      LOAD_DEST();
      NEW_LOAD_AND_APPLY_ALPHA_RED();
      NEW_LOAD_AND_APPLY_ALPHA_GREEN();
      NEW_LOAD_AND_APPLY_ALPHA_BLUE();
      STORE_COLOR();
    }
  }
}


//==========================================================================
//
//  bitmap::AlphaMaskedBlit
//
//==========================================================================
void bitmap::AlphaMaskedBlit (cblitdata &BlitData) const {
  blitdata B = BlitData;
  if (!AlphaMap) {
    B.Flags = 0;
    NormalMaskedBlit(B);
    return;
  }
  if (!FastFlag) {
    if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap alpha blit attempt detected!");
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  }

  packcol16 **SrcImage = Image;
  packalpha **SrcAlphaMap = AlphaMap;
  packcol16 **DestImage = B.Bitmap->Image;

  for (int y = 0; y < B.Border.Y; ++y) {
    cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
    cpackalpha *AlphaPtr = &SrcAlphaMap[B.Src.Y + y][B.Src.X];
    cpackcol16 *EndPtr = SrcPtr + B.Border.X;
    packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
    for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr, ++AlphaPtr) {
      LOAD_SRC();
      if (SrcCol != B.MaskColor) {
        LOAD_DEST();
        LOAD_ALPHA();
        NEW_LOAD_AND_APPLY_ALPHA_RED();
        NEW_LOAD_AND_APPLY_ALPHA_GREEN();
        NEW_LOAD_AND_APPLY_ALPHA_BLUE();
        STORE_COLOR();
      }
    }
  }
}


void bitmap::DrawLine (v2 From, int ToX, int ToY, col16 Color, truth Wide) { DrawLine(From.X, From.Y, ToX, ToY, Color, Wide); }
void bitmap::DrawLine (int FromX, int FromY, v2 To, col16 Color, truth Wide) { DrawLine(FromX, FromY, To.X, To.Y, Color, Wide); }
void bitmap::DrawLine (v2 From, v2 To, col16 Color, truth Wide) { DrawLine(From.X, From.Y, To.X, To.Y, Color, Wide); }

//==========================================================================
//
//  bitmap::DrawLine
//
//==========================================================================
void bitmap::DrawLine (int OrigFromX, int OrigFromY, int OrigToX, int OrigToY, col16 Color, truth Wide) {
  if (OrigFromY == OrigToY) {
    DrawHorizontalLine(OrigFromX, OrigToX, OrigFromY, Color, Wide);
    return;
  }
  if (OrigFromX == OrigToX) {
    DrawVerticalLine(OrigFromX, OrigFromY, OrigToY, Color, Wide);
    return;
  }

  static cint PointX[] = { 0, 0, -1, 1, 0 };
  static cint PointY[] = { 0, -1, 0, 0, 1 };
  cint Times = Wide ? 5 : 1;

  for (int c1 = 0; c1 < Times; ++c1) {
    cint X1 = OrigFromX + PointX[c1];
    cint Y1 = OrigFromY + PointY[c1];
    cint X2 = OrigToX + PointX[c1];
    cint Y2 = OrigToY + PointY[c1];
    cint DeltaX = abs(X2 - X1);
    cint DeltaY = abs(Y2 - Y1);
    int x, c2;
    int XChange, PtrXChange, PtrYChange;
    int DoubleDeltaX, DoubleDeltaY, End;
    if (DeltaX >= DeltaY) {
      x = X1;
      c2 = DeltaX;
      PtrXChange = XChange = X1 < X2 ? 1 : -1;
      PtrYChange = Y1 < Y2 ? mSize.X : -mSize.X;
      DoubleDeltaX = DeltaX << 1;
      DoubleDeltaY = DeltaY << 1;
      End = X2;
    } else {
      x = Y1;
      c2 = DeltaY;
      XChange = Y1 < Y2 ? 1 : -1;
      PtrXChange = Y1 < Y2 ? mSize.X : -mSize.X;
      PtrYChange = X1 < X2 ? 1 : -1;
      DoubleDeltaX = DeltaY << 1;
      DoubleDeltaY = DeltaX << 1;
      End = Y2;
    }
    packcol16 *Ptr = &Image[Y1][X1];
    *Ptr = Color;
    while (x != End) {
      x += XChange;
      Ptr += PtrXChange;
      c2 += DoubleDeltaY;
      if (c2 >= DoubleDeltaX) {
        c2 -= DoubleDeltaX;
        Ptr += PtrYChange;
      }
      *Ptr = Color;
    }
  }
}


//==========================================================================
//
//  bitmap::DrawVerticalLine
//
//==========================================================================
void bitmap::DrawVerticalLine (int OrigX, int OrigFromY, int OrigToY, col16 Color, truth Wide) {
  static cint PointX[] = { 0, -1, 1 };
  cint Times = Wide ? 3 : 1;
  for (int c = 0; c < Times; ++c) {
    int X = OrigX + PointX[c];
    int FromY = OrigFromY;
    int ToY = OrigToY;
    if (FromY > ToY) Swap(FromY, ToY);
    if (Wide && !c) {
      --FromY;
      ++ToY;
    }
    if (X < 0 || X >= mSize.X || ToY < 0 || FromY >= mSize.Y) continue;
    FromY = Max(FromY, 0);
    ToY = Min(ToY, mSize.Y-1);
    packcol16 *Ptr = &Image[FromY][X];
    for (int y = FromY; y <= ToY; ++y, Ptr += mSize.X) *Ptr = Color;
  }
}


//==========================================================================
//
//  bitmap::DrawHorizontalLine
//
//==========================================================================
void bitmap::DrawHorizontalLine (int OrigFromX, int OrigToX, int OrigY, col16 Color, truth Wide) {
  static cint PointY[] = { 0, -1, 1 };
  cint Times = Wide ? 3 : 1;
  for (int c = 0; c < Times; ++c) {
    int Y = OrigY + PointY[c];
    int FromX = OrigFromX;
    int ToX = OrigToX;
    if (FromX > ToX) Swap(FromX, ToX);
    if (Wide && !c) {
      --FromX;
      ++ToX;
    }
    if (Y < 0 || Y >= mSize.Y || ToX < 0 || FromX >= mSize.X) continue;
    FromX = Max(FromX, 0);
    ToX = Min(ToX, mSize.X-1);
    packcol16 *Ptr = &Image[Y][FromX];
    for (int x = FromX; x <= ToX; ++x, ++Ptr) *Ptr = Color;
  }
}


//==========================================================================
//
//  bitmap::DrawPolygon
//
//==========================================================================
void bitmap::DrawPolygon (int CenterX, int CenterY, int Radius, int NumberOfSides,
                          col16 Color, truth DrawSides, truth DrawDiameters, double Rotation)
{
  if (!DrawSides && !DrawDiameters) return;
  v2 *Point = new v2[NumberOfSides];
  double AngleDelta = 2 * FPI / NumberOfSides;
  int c;
  for (c = 0; c < NumberOfSides; ++c) {
    Point[c].X = CenterX + int(sin(AngleDelta * c + Rotation) * Radius);
    Point[c].Y = CenterY + int(cos(AngleDelta * c + Rotation) * Radius);
  }
  if (DrawDiameters) {
    if (DrawSides) {
      for (c = 0; c < NumberOfSides; ++c)
        for (int a = 0; a < NumberOfSides; ++a)
          if (c != a) DrawLine(Point[c].X, Point[c].Y, Point[a].X, Point[a].Y, Color, true);
    } else {
      for (c = 0; c < NumberOfSides; ++c)
        for (int a = 0; a < NumberOfSides; ++a)
          if ((int(c - a) > 1 || int(a - c) > 1) && (a || c != NumberOfSides - 1) && (c || a != NumberOfSides - 1))
            DrawLine(Point[c].X, Point[c].Y, Point[a].X, Point[a].Y, Color, true);
    }
  } else {
    for (c = 0; c < NumberOfSides - 1; ++c)
      DrawLine(Point[c].X, Point[c].Y, Point[c + 1].X, Point[c + 1].Y, Color, true);
    DrawLine(Point[NumberOfSides - 1].X, Point[NumberOfSides - 1].Y, Point[0].X, Point[0].Y, Color, true);
  }
  delete [] Point;
}


//==========================================================================
//
//  bitmap::CreateAlphaMap
//
//==========================================================================
void bitmap::CreateAlphaMap (alpha InitialValue) {
  if (AlphaMap) ABORT("Alpha leak detected!");
  Alloc2D(AlphaMap, mSize.Y, mSize.X);
  memset(AlphaMap[0], InitialValue, XSizeTimesYSize);
}


//==========================================================================
//
//  bitmap::Fade
//
//==========================================================================
truth bitmap::Fade (sLong &AlphaSum, packalpha& AlphaAverage, int Amount) {
  if (!AlphaMap) ABORT("No alpha map to fade.");
  truth Changes = false;
  sLong Alphas = 0;
  sLong NewAlphaSum = 0;
  sLong Size = XSizeTimesYSize;
  for (sLong c = 0; c < Size; ++c) {
    packalpha *AlphaPtr = &AlphaMap[0][c];
    if (*AlphaPtr) {
      if (*AlphaPtr > Amount) {
        *AlphaPtr -= Amount;
        NewAlphaSum += *AlphaPtr;
        ++Alphas;
        Changes = true;
      } else {
        *AlphaPtr = 0;
        Changes = true;
        if (RandMap) UpdateRandMap(c, false);
      }
    }
  }
  AlphaSum = NewAlphaSum;
  AlphaAverage = Alphas ? NewAlphaSum / Alphas : 0;
  return Changes;
}


//==========================================================================
//
//  bitmap::Outline
//
//==========================================================================
void bitmap::Outline (col16 Color, alpha Alpha, priority Priority) {
  if (!AlphaMap) CreateAlphaMap(255);
  col16 LastColor, NextColor;
  int XMax = mSize.X;
  int YMax = mSize.Y - 1;
  for (int x = 0; x < XMax; ++x) {
    packcol16 *Buffer = &Image[0][x];
    LastColor = *Buffer;
    for (int y = 0; y < YMax; ++y) {
      NextColor = *(Buffer + XMax);
      if ((LastColor == TRANSPARENT_COLOR || !y) && NextColor != TRANSPARENT_COLOR) {
        *Buffer = Color;
        SetAlpha(x, y, Alpha);
        SafeSetPriority(x, y, Priority);
      }
      Buffer += XMax;
      if (LastColor != TRANSPARENT_COLOR && (NextColor == TRANSPARENT_COLOR || y == YMax - 1)) {
        *Buffer = Color;
        SetAlpha(x, y + 1, Alpha);
        SafeSetPriority(x, y + 1, Priority);
      }
      LastColor = NextColor;
    }
  }
  --XMax;
  ++YMax;
  for (int y = 0; y < YMax; ++y) {
    packcol16 *Buffer = Image[y];
    LastColor = *Buffer;
    for (int x = 0; x < XMax; ++x) {
      NextColor = *(Buffer + 1);
      if ((LastColor == TRANSPARENT_COLOR || !x) && NextColor != TRANSPARENT_COLOR) {
        *Buffer = Color;
        SetAlpha(x, y, Alpha);
        SafeSetPriority(x, y, Priority);
      }
      ++Buffer;
      if (LastColor != TRANSPARENT_COLOR && (NextColor == TRANSPARENT_COLOR || x == XMax - 1)) {
        *Buffer = Color;
        SetAlpha(x + 1, y, Alpha);
        SafeSetPriority(x + 1, y, Priority);
      }
      LastColor = NextColor;
    }
  }
}


//==========================================================================
//
//  bitmap::FadeToScreen
//
//==========================================================================
void bitmap::FadeToScreen (bitmapeditor BitmapEditor) {
  bitmap Backup(DOUBLE_BUFFER);
  Backup.ActivateFastFlag();
  blitdata B = {
    Bitmap:DOUBLE_BUFFER,
    Src:v2(0, 0),
    Dest:v2(0, 0),
    Border:v2(RES.X, RES.Y),
    {Flags:0},
    MaskColor:0,
    CustomData:0
  };
  for (int c = 0; c <= 5; ++c) {
    clock_t StartTime = clock();
    const int Element = 127 - c * 25;
    B.Luminance = MakeRGB24(Element, Element, Element);
    Backup.LuminanceMaskedBlit(B);
    if (BitmapEditor) BitmapEditor(this, true);
    SimpleAlphaBlit(DOUBLE_BUFFER, c * 50, 0);
    graphics::BlitDBToScreen();
    while(clock() - StartTime < 0.05 * CLOCKS_PER_SEC);
  }
  DOUBLE_BUFFER->ClearToColor(0);
  if (BitmapEditor) BitmapEditor(this, true);
  B.Flags = 0;
  NormalMaskedBlit(B);
  graphics::BlitDBToScreen();
}


//==========================================================================
//
//  bitmap::StretchBlit
//
//==========================================================================
void bitmap::StretchBlit (cblitdata &BlitData) const {
  blitdata B = BlitData;
  //if (!FastFlag)
  {
    if (B.Dest.X >= mSize.X || B.Dest.Y >= mSize.Y) {
      return;
    }

    //B.Border.X = Min(B.Border.X, B.Bitmap->mSize.X);
    //B.Border.Y = Min(B.Border.Y, B.Bitmap->mSize.Y);
    if (B.Border.X <= 0 || B.Border.Y <= 0) {
      //ABORT("Zero-sized bitmap stretch blit attempt detected!");
      return;
    }

    // this is wrong
    /*
    if (!femath::Clip(B.Src.X, B.Src.Y,
                      B.Dest.X, B.Dest.Y,
                      B.Border.X, B.Border.Y,
                      mSize.X, mSize.Y,
                      B.Bitmap->mSize.X, B.Bitmap->mSize.Y))
    {
      return;
    }
    */
  }
  if (B.Stretch > 1) {
    int tx = B.Dest.X;
    for (int x1 = B.Src.X; x1 < B.Src.X + B.Border.X; ++x1, tx += B.Stretch) {
      int ty = B.Dest.Y;
      for (int y1 = B.Src.Y; y1 < B.Src.Y + B.Border.Y; ++y1, ty += B.Stretch) {
        packcol16 Pixel = GetPixel(x1, y1); //Image[y1][x1];
        if (Pixel != TRANSPARENT_COLOR) {
          for (int x2 = tx; x2 < tx + B.Stretch; ++x2) {
            for (int y2 = ty; y2 < ty + B.Stretch; ++y2) {
              //B.Bitmap->Image[y2][x2] = Pixel;
              B.Bitmap->PutPixel(x2, y2, Pixel);
            }
          }
        }
      }
    }
  } else if (B.Stretch < -1) {
    int tx = B.Dest.X;
    for (int x1 = B.Src.X; x1 < B.Src.X + B.Border.X; x1 -= B.Stretch, ++tx) {
      int ty = B.Dest.Y;
      for (int y1 = B.Src.Y; y1 < B.Src.Y + B.Border.Y; y1 -= B.Stretch, ++ty) {
        packcol16 Pixel = GetPixel(x1, y1); //Image[y1][x1];
        if (Pixel != TRANSPARENT_COLOR) {
          //B.Bitmap->Image[ty][tx] = Pixel;
          B.Bitmap->PutPixel(tx, ty, Pixel);
        }
      }
    }
  } else {
    B.Flags = 0;
    NormalMaskedBlit(B);
  }
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, cbitmap *Bitmap) {
  if (Bitmap) {
    SaveFile.Put(1);
    SaveFile << Bitmap->GetSize();
    Bitmap->Save(SaveFile);
  } else {
    SaveFile.Put(0);
  }
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, bitmap *&Bitmap) {
  if (SaveFile.Get()) {
    Bitmap = new bitmap(ReadType(v2, SaveFile));
    Bitmap->Load(SaveFile);
  } else {
    Bitmap = 0;
  }
  return SaveFile;
}


void bitmap::DrawRectangle (v2 TopLeft, int Right, int Bottom, col16 Color, truth Wide) { DrawRectangle(TopLeft.X, TopLeft.Y, Right, Bottom, Color, Wide); }
void bitmap::DrawRectangle (int Left, int Top, v2 BottomRight, col16 Color, truth Wide) { DrawRectangle(Left, Top, BottomRight.X, BottomRight.Y, Color, Wide); }
void bitmap::DrawRectangle (v2 TopLeft, v2 BottomRight, col16 Color, truth Wide) { DrawRectangle(TopLeft.X, TopLeft.Y, BottomRight.X, BottomRight.Y, Color, Wide); }

//==========================================================================
//
//  bitmap::DrawRectangle
//
//==========================================================================
void bitmap::DrawRectangle (int Left, int Top, int Right, int Bottom, col16 Color, truth Wide) {
  DrawHorizontalLine(Left, Right, Top, Color, Wide);
  DrawHorizontalLine(Left, Right, Bottom, Color, Wide);
  DrawVerticalLine(Right, Top, Bottom, Color, Wide);
  DrawVerticalLine(Left, Top, Bottom, Color, Wide);
}


//==========================================================================
//
//  bitmap::AlphaLuminanceBlit
//
//==========================================================================
void bitmap::AlphaLuminanceBlit (cblitdata &BlitData) const {
  if (BlitData.Luminance == NORMAL_LUMINANCE) {
    AlphaMaskedBlit(BlitData);
    return;
  }
  if (!AlphaMap) {
    LuminanceMaskedBlit(BlitData);
    return;
  }
  blitdata B = BlitData;
  if (!FastFlag) {
    if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap alpha blit attempt detected!");
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  }
  packcol16 **SrcImage = Image;
  packalpha **SrcAlphaMap = AlphaMap;
  packcol16 **DestImage = B.Bitmap->Image;
  int NewRedLuminance = (B.Luminance >> 7 & 0x1F800) - 0x10000;
  int NewGreenLuminance = (B.Luminance >> 4 & 0xFE0) - 0x800;
  int NewBlueLuminance = (B.Luminance >> 2 & 0x3F) - 0x20;
  for (int y = 0; y < B.Border.Y; ++y) {
    cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
    cpackalpha *AlphaPtr = &SrcAlphaMap[B.Src.Y + y][B.Src.X];
    cpackcol16 *EndPtr = SrcPtr + B.Border.X;
    packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
    for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr, ++AlphaPtr) {
      LOAD_SRC();
      if (SrcCol != B.MaskColor) {
        LOAD_DEST();
        LOAD_ALPHA();
        NEW_LUMINATE_RED();
        NEW_APPLY_ALPHA_RED();
        NEW_LUMINATE_GREEN();
        NEW_APPLY_ALPHA_GREEN();
        NEW_LUMINATE_BLUE();
        NEW_APPLY_ALPHA_BLUE();
        STORE_COLOR();
      }
    }
  }
}


//==========================================================================
//
//  IsDarkBlack
//
//==========================================================================
static bool IsDarkBlack (col16 clr) {
  const int r = GetRed16(clr);
  const int g = GetGreen16(clr);
  const int b = GetBlue16(clr);
  return (r == g && r == b && r <= 0x2f);
}


//==========================================================================
//
//  bitmap::CreateFlames
//
//  Only works for 16x16 pictures :(
//
//==========================================================================
void bitmap::CreateFlames (rawbitmap *RawBitmap, v2 RawPos, feuLong SeedNFlags, int Frame) {
  //auto saviour = femath::SeedSaviour();
  //femath::SaveSeed();
  //femath::SetSeed(SeedNFlags);
  auto prng = ngprng;
  prng.SetSeed(SeedNFlags);
  int FlameTop[16], FlameBottom[16], FlamePhase[16];
  int x, y;
  for (x = 0; x < 16; ++x) {
    FlameBottom[x] = NO_FLAME;
    for (y = 0; y < 16; ++y) {
      col16 cpx = GetPixel(x, y);
      //k8: `IsDarkBlack()` for outlines
      if (cpx != TRANSPARENT_COLOR && !IsDarkBlack(cpx)) {
        if (1 << RawBitmap->GetMaterialColorIndex(RawPos.X + x, RawPos.Y + y) & SeedNFlags) {
          FlamePhase[x] = X_RAND_16(prng);
          if (y > 1) {
            FlameBottom[x] = y - 1;
            if (y >= 5) FlameTop[x] = (y - (X_RAND_32(prng) * y >> 5)) >> 1; else FlameTop[x] = 0;
          } else {
            FlameBottom[x] = 1;
            FlameTop[x] = 0;
          }
        }
        break;
      }
    }
  }

  for (x = 0; x < 16; ++x) {
    if (FlameBottom[x] != NO_FLAME) {
      int Phase = (Frame + FlamePhase[x]) & 15;
      int Length = FlameBottom[x] - FlameTop[x];
      int Top = FlameBottom[x] - Length + Phase * (15 - Phase) * Length / 56;
      for (y = Top; y <= FlameBottom[x]; ++y) {
        int Pos = y - Top;
        PowerPutPixel(x, y, MakeRGB16(255, 255 - (Pos << 7) / Length, 0),
                      127 + (Pos << 6) / Length, AVERAGE_PRIORITY);
      }
    }
  }
  //femath::LoadSeed();
}


//==========================================================================
//
//  bitmap::CreateSparkle
//
//==========================================================================
void bitmap::CreateSparkle (v2 SparklePos, int Frame) {
  if (Frame) {
    int Size = (Frame - 1) * (16 - Frame) / 10;
    PowerPutPixel(SparklePos.X, SparklePos.Y, WHITE, 255, SPARKLE_PRIORITY);
    for (int c = 1; c < Size; ++c) {
      int Lightness = 191 + ((Size - c) << 6) / Size;
      col16 RGB = MakeRGB16(Lightness, Lightness, Lightness);
      PowerPutPixel(SparklePos.X + c, SparklePos.Y, RGB, 255, SPARKLE_PRIORITY);
      PowerPutPixel(SparklePos.X - c, SparklePos.Y, RGB, 255, SPARKLE_PRIORITY);
      PowerPutPixel(SparklePos.X, SparklePos.Y + c, RGB, 255, SPARKLE_PRIORITY);
      PowerPutPixel(SparklePos.X, SparklePos.Y - c, RGB, 255, SPARKLE_PRIORITY);
    }
  }
}


//==========================================================================
//
//  bitmap::CreateFlies
//
//==========================================================================
void bitmap::CreateFlies (feuLong Seed, int Frame, int FlyAmount) {
  //auto saviour = femath::SeedSaviour();
  //femath::SaveSeed();
  //femath::SetSeed(Seed);
  auto prng = ngprng;
  prng.SetSeed(Seed);
  for (int c = 0; c < FlyAmount; ++c) {
    double Constant = double(X_RAND_N(prng, 10000)) / 10000 * FPI;
    v2 StartPos = v2(5 + X_RAND_N(prng, 6), 5 + X_RAND_N(prng, 6));
    double Temp = (double(16 - Frame) * FPI) / 16;
    if (X_RAND_2(prng)) Temp = -Temp;
    v2 Where;
    Where.X = int(StartPos.X + sin(Constant + Temp) * 3);
    Where.Y = int(StartPos.Y + sin(2*(Constant + Temp)) * 3);
    PowerPutPixel(Where.X, Where.Y, MakeRGB16(40, 40, 60), 255, FLY_PRIORITY);
  }
  //femath::LoadSeed();
}


//==========================================================================
//
//  bitmap::CreateLightning
//
//==========================================================================
void bitmap::CreateLightning (feuLong Seed, col16 Color) {
  //auto saviour = femath::SeedSaviour();
  //femath::SaveSeed();
  //femath::SetSeed(Seed);
  auto prng = ngprng;
  prng.SetSeed(Seed);
  v2 StartPos;
  v2 Direction(0, 0);
  do {
    do {
      if (X_RAND_2(prng)) {
        if (X_RAND_2(prng)) {
          StartPos.X = 0;
          Direction.X = 1;
        } else {
          StartPos.X = mSize.X - 1;
          Direction.X = -1;
        }
        StartPos.Y = X_RAND_N(prng, mSize.Y);
      } else {
        if (X_RAND_2(prng)) {
          StartPos.Y = 0;
          Direction.Y = 1;
        } else {
          StartPos.Y = mSize.Y - 1;
          Direction.Y = -1;
        }
        StartPos.X = X_RAND_N(prng, mSize.X);
      }
    } while (GetPixel(StartPos) != TRANSPARENT_COLOR);
  } while (!CreateLightning(StartPos, Direction, NO_LIMIT, Color));
  //femath::LoadSeed();
}


struct pixelvectorcontroller {
  static truth Handler (int x, int y) {
    if (CurrentSprite->GetPixel(x, y) == TRANSPARENT_COLOR) {
      PixelVector.push_back(v2(x, y));
      return true;
    }
    return false;
  }
  static std::vector<v2> PixelVector;
  static bitmap *CurrentSprite;
};


std::vector<v2> pixelvectorcontroller::PixelVector;
bitmap *pixelvectorcontroller::CurrentSprite;


//==========================================================================
//
//  bitmap::CreateLightning
//
//==========================================================================
truth bitmap::CreateLightning (v2 StartPos, v2 Direction, int MaxLength, col16 Color) {
  pixelvectorcontroller::CurrentSprite = this;
  std::vector<v2> &PixelVector = pixelvectorcontroller::PixelVector;
  PixelVector.clear();
  v2 LastMove(0, 0);
  int Counter = 0;
  for (;;) {
    v2 Move(1 + NG_RAND_4, 1 + NG_RAND_4);
    if (Direction.X < 0 || (!Direction.X && NG_RAND_2)) Move.X = -Move.X;
    if (Direction.Y < 0 || (!Direction.Y && NG_RAND_2)) Move.Y = -Move.Y;
    LimitRef(Move.X, -StartPos.X, mSize.X - StartPos.X - 1);
    LimitRef(Move.Y, -StartPos.Y, mSize.X - StartPos.Y - 1);
    if (Counter < 10 && ((!Move.Y && !LastMove.Y) || (Move.Y && LastMove.Y && (Move.X << 10) / Move.Y == (LastMove.X << 10) / LastMove.Y))) {
      ++Counter;
      continue;
    }
    Counter = 0;
    if (!mapmath<pixelvectorcontroller>::DoLine(StartPos.X, StartPos.Y, StartPos.X + Move.X, StartPos.Y + Move.Y, LINE_BOTH_DIRS) || feuLong(MaxLength) <= PixelVector.size()) {
      int Limit = Min<int>(PixelVector.size(), MaxLength);
      for (int c = 0; c < Limit; ++c) {
        PutPixel(PixelVector[c], Color);
        SafeSetPriority(PixelVector[c], LIGHTNING_PRIORITY);
      }
      PixelVector.clear();
      return true;
    }
    StartPos += Move;
    LastMove = Move;
    if ((Direction.X && (!StartPos.X || StartPos.X == mSize.X - 1)) || (Direction.Y && (!StartPos.Y || StartPos.Y == mSize.X - 1))) {
      PixelVector.clear();
      return false;
    }
  }
}


//==========================================================================
//
//  bitmap::BlitAndCopyAlpha
//
//==========================================================================
void bitmap::BlitAndCopyAlpha (bitmap *Bitmap, int Flags) const {
  if (!FastFlag) {
    if (!AlphaMap || !Bitmap->AlphaMap) ABORT("Attempt to blit and copy alpha without an alpha map detected!");
    if ((Flags&ROTATE) && mSize.X != mSize.Y) ABORT("Blit and copy alpha error: FeLib supports only square rotating!");
    if (mSize.X != Bitmap->mSize.X || mSize.Y != Bitmap->mSize.Y) ABORT("Blit and copy alpha attempt of noncongruent bitmaps detected!");
  }
  packcol16 **SrcImage = Image;
  packalpha **SrcAlphaMap = AlphaMap;
  packcol16 **DestImage = Bitmap->Image;
  packalpha **DestAlphaMap = Bitmap->AlphaMap;
  switch (Flags&7) {
    case NONE: {
      memmove(DestImage[0], SrcImage[0], XSizeTimesYSize * sizeof(packcol16));
      memmove(DestAlphaMap[0], SrcAlphaMap[0], XSizeTimesYSize * sizeof(packalpha));
      break; }
    case MIRROR: {
      int Width = mSize.X;
      int Height = mSize.Y;
      int DestX = Width - 1;
      cpackcol16 *SrcPtr = SrcImage[0];
      cpackalpha *SrcAlphaPtr = SrcAlphaMap[0];
      for (int y = 0; y < Height; ++y) {
        cpackcol16 *EndPtr = SrcPtr + Width;
        packcol16 *DestPtr = &DestImage[y][DestX];
        packalpha *DestAlphaPtr = &DestAlphaMap[y][DestX];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr, ++SrcAlphaPtr, --DestAlphaPtr) {
          *DestPtr = *SrcPtr;
          *DestAlphaPtr = *SrcAlphaPtr;
        }
      }
      break; }
    case FLIP: {
      int Height = mSize.Y;
      int Width = mSize.X;
      int DestY = Height - 1;
      for (int y = 0; y < Height; ++y) {
        memmove(DestImage[DestY - y], SrcImage[y], Width * sizeof(packcol16));
        memmove(DestAlphaMap[DestY - y], SrcAlphaMap[y], Width * sizeof(packalpha));
      }
      break; }
    case (MIRROR | FLIP): {
      cpackcol16 *SrcPtr = SrcImage[0];
      cpackcol16 *EndPtr = SrcPtr + XSizeTimesYSize;
      cpackalpha *SrcAlphaPtr = SrcAlphaMap[0];
      packcol16 *DestPtr = &DestImage[mSize.Y - 1][mSize.X - 1];
      packalpha *DestAlphaPtr = &DestAlphaMap[mSize.Y - 1][mSize.X - 1];
      for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr, ++SrcAlphaPtr, --DestAlphaPtr) {
        *DestPtr = *SrcPtr;
        *DestAlphaPtr = *SrcAlphaPtr;
      }
      break; }
    case ROTATE: {
      cint Width = mSize.X;
      cpackcol16 *SrcPtr = SrcImage[0];
      cpackalpha *SrcAlphaPtr = SrcAlphaMap[0];
      packcol16 *DestBase = &DestImage[0][Width - 1];
      packalpha *DestAlphaBase = &DestAlphaMap[0][Width - 1];
      for (int y = 0; y < Width; ++y) {
        cpackcol16 *EndPtr = SrcPtr + Width;
        packcol16 *DestPtr = DestBase - y;
        packalpha *DestAlphaPtr = DestAlphaBase - y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += Width, ++SrcAlphaPtr, DestAlphaPtr += Width) {
          *DestPtr = *SrcPtr;
          *DestAlphaPtr = *SrcAlphaPtr;
        }
      }
      break; }
    case (MIRROR | ROTATE): {
      cint Width = mSize.X;
      cpackcol16 *SrcPtr = SrcImage[0];
      cpackalpha *SrcAlphaPtr = SrcAlphaMap[0];
      packcol16 *DestBase = DestImage[0];
      packalpha *DestAlphaBase = DestAlphaMap[0];
      for (int y = 0; y < Width; ++y) {
        cpackcol16 *EndPtr = SrcPtr + Width;
        packcol16 *DestPtr = DestBase + y;
        packalpha *DestAlphaPtr = DestAlphaBase + y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += Width, ++SrcAlphaPtr, DestAlphaPtr += Width) {
          *DestPtr = *SrcPtr;
          *DestAlphaPtr = *SrcAlphaPtr;
        }
      }
      break; }
    case (FLIP | ROTATE): {
      cint Width = mSize.X;
      cpackcol16 *SrcPtr = SrcImage[0];
      cpackalpha *SrcAlphaPtr = SrcAlphaMap[0];
      packcol16 *DestBase = &DestImage[Width - 1][Width - 1];
      packalpha *DestAlphaBase = &DestAlphaMap[Width - 1][Width - 1];
      for (int y = 0; y < Width; ++y) {
        cpackcol16 *EndPtr = SrcPtr + Width;
        packcol16 *DestPtr = DestBase - y;
        packalpha *DestAlphaPtr = DestAlphaBase - y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= Width, ++SrcAlphaPtr, DestAlphaPtr -= Width) {
          *DestPtr = *SrcPtr;
          *DestAlphaPtr = *SrcAlphaPtr;
        }
      }
      break; }
    case (MIRROR | FLIP | ROTATE): {
      cint Width = mSize.X;
      cpackcol16 *SrcPtr = SrcImage[0];
      cpackalpha *SrcAlphaPtr = SrcAlphaMap[0];
      packcol16 *DestBase = DestImage[Width - 1];
      packalpha *DestAlphaBase = DestAlphaMap[Width - 1];
      for (int y = 0; y < Width; ++y) {
        cpackcol16 *EndPtr = SrcPtr + Width;
        packcol16 *DestPtr = DestBase + y;
        packalpha *DestAlphaPtr = DestAlphaBase + y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= Width, ++SrcAlphaPtr, DestAlphaPtr -= Width) {
          *DestPtr = *SrcPtr;
          *DestAlphaPtr = *SrcAlphaPtr;
        }
      }
      break; }
  }
}


//==========================================================================
//
//  bitmap::FillAlpha
//
//==========================================================================
void bitmap::FillAlpha (alpha Alpha) {
  memset(AlphaMap[0], Alpha, XSizeTimesYSize);
}


//==========================================================================
//
//  bitmap::PowerPutPixel
//
//==========================================================================
void bitmap::PowerPutPixel (int X, int Y, col16 Color, alpha Alpha, priority Priority) {
  if (X >= 0 && Y >= 0 && X < mSize.X && Y < mSize.Y) {
    Image[Y][X] = Color;
    if (AlphaMap) {
      AlphaMap[Y][X] = Alpha;
    } else if (Alpha != 255) {
      CreateAlphaMap(255);
      AlphaMap[Y][X] = Alpha;
    }
    if (PriorityMap) PriorityMap[Y][X] = Priority;
  }
}


//==========================================================================
//
//  bitmap::MaskedPriorityBlit
//
//==========================================================================
void bitmap::MaskedPriorityBlit (cblitdata &BlitData) const {
  if (!PriorityMap || !BlitData.Bitmap->PriorityMap) {
    LuminanceMaskedBlit(BlitData);
    return;
  }
  blitdata B = BlitData;
  if (!FastFlag) {
    if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap masked priority blit attempt detected!");
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  }
  packcol16 **SrcImage = Image;
  packpriority **SrcPriorityMap = PriorityMap;
  packcol16 **DestImage = B.Bitmap->Image;
  packpriority **DestPriorityMap = B.Bitmap->PriorityMap;
  int NewRedLuminance = (B.Luminance >> 7 & 0x1F800) - 0x10000;
  int NewGreenLuminance = (B.Luminance >> 4 & 0xFE0) - 0x800;
  int NewBlueLuminance = (B.Luminance >> 2 & 0x3F) - 0x20;

  if (B.Dest.X < 0) B.Dest.X = 0; //FIXME: k8: ???

  for (int y = 0; y < B.Border.Y; ++y) {
    cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
    cpackpriority *SrcPriorityPtr = &SrcPriorityMap[B.Src.Y + y][B.Src.X];
    cpackcol16 *EndPtr = SrcPtr + B.Border.X;
    packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
    packpriority *DestPriorityPtr = &DestPriorityMap[B.Dest.Y + y][B.Dest.X];
    for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr, ++SrcPriorityPtr, ++DestPriorityPtr) {
      LOAD_SRC();
      if (SrcCol != B.MaskColor) {
        priority SrcPriority = *SrcPriorityPtr;
        priority DestPriority = *DestPriorityPtr;
        if ((SrcPriority & 0xF) >= (DestPriority & 0xF) || (SrcPriority & 0xF0) >= (DestPriority & 0xF0)) {
          NEW_LUMINATE_RED();
          NEW_LUMINATE_GREEN();
          NEW_LUMINATE_BLUE();
          STORE_COLOR();
          *DestPriorityPtr = SrcPriority;
        }
      }
    }
  }
}


//==========================================================================
//
//  bitmap::AlphaPriorityBlit
//
//==========================================================================
void bitmap::AlphaPriorityBlit (cblitdata &BlitData) const {
  if (!AlphaMap) {
    MaskedPriorityBlit(BlitData);
    return;
  }
  if (!PriorityMap || !BlitData.Bitmap->PriorityMap) {
    AlphaLuminanceBlit(BlitData);
    return;
  }
  blitdata B = BlitData;
  if (!FastFlag) {
    if (!B.Border.X || !B.Border.Y) ABORT("Zero-sized bitmap alpha priority blit attempt detected!");
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  } else {
    // but who cares?
    if (!femath::Clip(B.Src.X, B.Src.Y, B.Dest.X, B.Dest.Y, B.Border.X, B.Border.Y, mSize.X, mSize.Y, B.Bitmap->mSize.X, B.Bitmap->mSize.Y)) return;
  }
  packcol16 **SrcImage = Image;
  packalpha **SrcAlphaMap = AlphaMap;
  packpriority **SrcPriorityMap = PriorityMap;
  packcol16 **DestImage = B.Bitmap->Image;
  packpriority **DestPriorityMap = B.Bitmap->PriorityMap;
  int NewRedLuminance = (B.Luminance >> 7 & 0x1F800) - 0x10000;
  int NewGreenLuminance = (B.Luminance >> 4 & 0xFE0) - 0x800;
  int NewBlueLuminance = (B.Luminance >> 2 & 0x3F) - 0x20;
  for (int y = 0; y < B.Border.Y; ++y) {
    cpackcol16 *SrcPtr = &SrcImage[B.Src.Y + y][B.Src.X];
    cpackalpha *AlphaPtr = &SrcAlphaMap[B.Src.Y + y][B.Src.X];
    cpackpriority *SrcPriorityPtr = &SrcPriorityMap[B.Src.Y + y][B.Src.X];
    cpackcol16 *EndPtr = SrcPtr + B.Border.X;
    packcol16 *DestPtr = &DestImage[B.Dest.Y + y][B.Dest.X];
    packpriority *DestPriorityPtr = &DestPriorityMap[B.Dest.Y + y][B.Dest.X];
    for (; SrcPtr != EndPtr; ++SrcPtr, ++DestPtr, ++AlphaPtr, ++SrcPriorityPtr, ++DestPriorityPtr) {
      LOAD_SRC();
      if (SrcCol != B.MaskColor) {
        priority SrcPriority = *SrcPriorityPtr;
        priority DestPriority = *DestPriorityPtr;
        if ((SrcPriority & 0xF) >= (DestPriority & 0xF) || (SrcPriority & 0xF0) >= (DestPriority & 0xF0)) {
          LOAD_DEST();
          LOAD_ALPHA();
          NEW_LUMINATE_RED();
          NEW_APPLY_ALPHA_RED();
          NEW_LUMINATE_GREEN();
          NEW_APPLY_ALPHA_GREEN();
          NEW_LUMINATE_BLUE();
          NEW_APPLY_ALPHA_BLUE();
          STORE_COLOR();
          *DestPriorityPtr = SrcPriority;
        }
      }
    }
  }
}


//==========================================================================
//
//  bitmap::InitPriorityMap
//
//==========================================================================
void bitmap::InitPriorityMap (priority InitialValue) {
  if (!PriorityMap) Alloc2D(PriorityMap, mSize.Y, mSize.X);
  memset(PriorityMap[0], InitialValue, XSizeTimesYSize);
}


//==========================================================================
//
//  bitmap::FillPriority
//
//==========================================================================
void bitmap::FillPriority (priority Priority) {
  memset(PriorityMap[0], Priority, XSizeTimesYSize);
}


//==========================================================================
//
//  bitmap::FastBlitAndCopyAlpha
//
//==========================================================================
void bitmap::FastBlitAndCopyAlpha (bitmap *Bitmap) const {
  if (!FastFlag) {
    if (!AlphaMap || !Bitmap->AlphaMap) ABORT("Attempt to fast blit and copy alpha without an alpha map detected!");
    if (mSize.X != Bitmap->mSize.X || mSize.Y != Bitmap->mSize.Y) ABORT("Fast blit and copy alpha attempt of noncongruent bitmaps detected!");
  }
  memmove(Bitmap->Image[0], Image[0], XSizeTimesYSize * sizeof(packcol16));
  memmove(Bitmap->AlphaMap[0], AlphaMap[0], XSizeTimesYSize * sizeof(packalpha));
}


//==========================================================================
//
//  bitmap::NormalBlit
//
//==========================================================================
void bitmap::NormalBlit (bitmap *Bitmap, int Flags) const {
  blitdata B = {
    Bitmap:Bitmap,
    Src:{ 0, 0 },
    Dest:{ 0, 0 },
    Border:{ mSize.X, mSize.Y },
    { Flags:Flags },
    MaskColor:0,
    CustomData:0,
  };
  NormalBlit(B);
}


//==========================================================================
//
//  bitmap::FastBlit
//
//==========================================================================
void bitmap::FastBlit (bitmap *Bitmap) const {
  if (mSize.X > Bitmap->mSize.X || mSize.Y > Bitmap->mSize.Y) {
    NormalBlit(Bitmap);
  } else if (mSize.X == Bitmap->mSize.X) {
    memmove(Bitmap->Image[0], Image[0], XSizeTimesYSize*sizeof(packcol16));
  } else {
    FastBlit(Bitmap, v2(0, 0));
  }
}


//==========================================================================
//
//  bitmap::FastBlit
//
//==========================================================================
void bitmap::FastBlit (bitmap *Bitmap, v2 Pos) const {
  if (!Bitmap || Pos.X >= Bitmap->mSize.X || Pos.Y >= Bitmap->mSize.Y) return;
  if (Pos.X < 0 && Pos.X + Bitmap->mSize.X <= 0) return;
  if (Pos.Y < 0 && Pos.Y + Bitmap->mSize.Y <= 0) return;
  if (Pos.X < 0 || Pos.Y < 0 ||
      mSize.X > Bitmap->mSize.X || mSize.Y > Bitmap->mSize.Y ||
      Bitmap->mSize.X - Pos.X < mSize.X || Bitmap->mSize.Y - Pos.Y < mSize.Y)
  {
    blitdata B = {
      Bitmap:Bitmap,
      Src:{ 0, 0 },
      Dest:{ Pos.X, Pos.Y },
      Border:{ mSize.X, mSize.Y },
      { Flags:0 },
      MaskColor:0,
      CustomData:0,
    };
    NormalBlit(B);
  } else {
    packcol16 **SrcImage = Image;
    packcol16 **DestImage = Bitmap->Image;
    cint Bytes = mSize.X * sizeof(packcol16);
    cint Height = mSize.Y;
    for (int y = 0; y != Height; y += 1) {
      memmove(&DestImage[Pos.Y + y][Pos.X], SrcImage[y], Bytes);
    }
  }
}


//==========================================================================
//
//  bitmap::FastCopyFrom
//
//==========================================================================
void bitmap::FastCopyFrom (bitmap *Bitmap, v2 Pos) {
  if (!Bitmap || Pos.X >= Bitmap->mSize.X || Pos.Y >= Bitmap->mSize.Y) return;
  if (Pos.X < 0 && Pos.X + Bitmap->mSize.X <= 0) return;
  if (Pos.Y < 0 && Pos.Y + Bitmap->mSize.Y <= 0) return;
  if (Pos.X < 0 || Pos.Y < 0 ||
      Pos.X + mSize.X > Bitmap->mSize.X || Pos.Y + mSize.Y > Bitmap->mSize.Y)
  {
    blitdata B = {
      Bitmap:this,
      Src:{ Pos.X, Pos.Y },
      Dest:{ 0, 0 },
      Border:{ mSize.X, mSize.Y },
      { Flags:0 },
      MaskColor:0,
      CustomData:0,
    };
    Bitmap->NormalBlit(B);
  } else {
    packcol16 **SrcImage = Bitmap->Image;
    packcol16 **DestImage = Image;
    cint Bytes = mSize.X * sizeof(packcol16);
    cint Height = mSize.Y;
    for (int y = 0; y != Height; y += 1) {
      memmove(&DestImage[y][0], &SrcImage[Pos.Y + y][Pos.X], Bytes);
    }
  }
}


//==========================================================================
//
//  bitmap::UpdateRandMap
//
//==========================================================================
void bitmap::UpdateRandMap (sLong Index, truth Value) {
  sLong c1 = XSizeTimesYSize + Index;
  RandMap[c1] = Value;
  for (sLong c2 = c1 >> 1; c2; c1 = c2, c2 >>= 1) {
    Value |= RandMap[c1 ^ 1];
    if (!RandMap[c2] != !Value) RandMap[c2] = Value; else return;
  }
}


//==========================================================================
//
//  bitmap::InitRandMap
//
//==========================================================================
void bitmap::InitRandMap () {
  if (!RandMap) RandMap = new truth[XSizeTimesYSize << 1];
  memset(RandMap, 0, (XSizeTimesYSize << 1) * sizeof(truth));
}


//==========================================================================
//
//  bitmap::RandomizePixel
//
//==========================================================================
v2 bitmap::RandomizePixel () const {
  if (!RandMap[1]) return ERROR_V2;
  feuLong Rand = NG_RAND_U32();
  feuLong c, RandMask = 1;
  feuLong MapSize = XSizeTimesYSize << 1;
  for (c = 2; c < MapSize; c <<= 1) {
    if (RandMap[c + 1] && (!RandMap[c] || Rand & (RandMask <<= 1))) {
      ++c;
    }
  }
  c = (c - MapSize) >> 1;
  return v2(c % mSize.X, c / mSize.X);
}


//==========================================================================
//
//  bitmap::CalculateRandMap
//
//==========================================================================
void bitmap::CalculateRandMap () {
  if (!AlphaMap) ABORT("Alpha map needed to calculate random map.");
  feuLong Size = XSizeTimesYSize;
  for (feuLong c = 0; c < Size; ++c) UpdateRandMap(c, AlphaMap[0][c]);
}


//==========================================================================
//
//  bitmap::AlphaPutPixel
//
//==========================================================================
void bitmap::AlphaPutPixel (int x, int y, col16 SrcCol, col24 Luminance, alpha Alpha) {
  if (x < 0 || y < 0 || x >= mSize.X || y >= mSize.Y) return;
  int DestCol = Image[y][x];
  int NewRedLuminance = (Luminance >> 7 & 0x1F800) - 0x10000;
  int NewGreenLuminance = (Luminance >> 4 & 0xFE0) - 0x800;
  int NewBlueLuminance = (Luminance >> 2 & 0x3F) - 0x20;
  NEW_LUMINATE_RED();
  NEW_APPLY_ALPHA_RED();
  NEW_LUMINATE_GREEN();
  NEW_APPLY_ALPHA_GREEN();
  NEW_LUMINATE_BLUE();
  NEW_APPLY_ALPHA_BLUE();
  Image[y][x] = Red|Green|Blue;
}


//==========================================================================
//
//  bitmap::CalculateAlphaAverage
//
//==========================================================================
alpha bitmap::CalculateAlphaAverage () const {
  if (!AlphaMap) ABORT("Alpha map needed to calculate alpha average!");
  sLong Alphas = 0;
  sLong AlphaSum = 0;
  feuLong Size = XSizeTimesYSize;
  for (feuLong c = 0; c < Size; ++c) {
    packalpha *AlphaPtr = &AlphaMap[0][c];
    if (*AlphaPtr) {
      AlphaSum += *AlphaPtr;
      ++Alphas;
    }
  }
  return Alphas ? AlphaSum / Alphas : 0;
}


static const cint WaveDelta[] = { 1, 2, 2, 2, 1, 0, -1, -2, -2, -2, -1 };


//==========================================================================
//
//  bitmap::Wobble
//
//==========================================================================
void bitmap::Wobble (int Frame, int SpeedShift, truth Horizontally) {
  int WavePos = (Frame << SpeedShift >> 1) - 14;
  if (Horizontally) {
    for (int c = 0; c < 11; ++c) {
      if (WavePos + c >= 0 && WavePos + c < mSize.Y) {
        MoveLineHorizontally(WavePos + c, WaveDelta[c]);
      }
    }
  } else {
    for (int c = 0; c < 11; ++c) {
      if (WavePos + c >= 0 && WavePos + c < mSize.X) {
        MoveLineVertically(WavePos + c, WaveDelta[c]);
      }
    }
  }
}


//==========================================================================
//
//  bitmap::MoveLineVertically
//
//==========================================================================
void bitmap::MoveLineVertically (int X, int Delta) {
  int y;
  if (Delta < 0) {
    for (y = 0; y < mSize.Y + Delta; ++y) {
      PowerPutPixel(X, y, GetPixel(X, y - Delta), AlphaMap ? GetAlpha(X, y - Delta) : 255, AVERAGE_PRIORITY);
    }
    for (int y = -1; y >= Delta; --y) {
      PowerPutPixel(X, mSize.Y + y, TRANSPARENT_COLOR, 255, AVERAGE_PRIORITY);
    }
  } else if (Delta > 0) {
    for (y = mSize.Y - 1; y >= Delta; --y) {
      PowerPutPixel(X, y, GetPixel(X, y - Delta), AlphaMap ? GetAlpha(X, y - Delta) : 255, AVERAGE_PRIORITY);
    }
    for (y = 0; y < Delta; ++y) {
      PowerPutPixel(X, y, TRANSPARENT_COLOR, 255, AVERAGE_PRIORITY);
    }
  }
}


//==========================================================================
//
//  bitmap::MoveLineHorizontally
//
//==========================================================================
void bitmap::MoveLineHorizontally (int Y, int Delta) {
  int x;
  if (Delta < 0) {
    for (x = 0; x < mSize.X + Delta; ++x) {
      PowerPutPixel(x, Y, GetPixel(x - Delta, Y), AlphaMap ? GetAlpha(x - Delta, Y) : 255, AVERAGE_PRIORITY);
    }
    for (x = -1; x >= Delta; --x) {
      PowerPutPixel(mSize.X + x, Y, TRANSPARENT_COLOR, 255, AVERAGE_PRIORITY);
    }
  } else if (Delta > 0) {
    for (x = mSize.X - 1; x >= Delta; --x) {
      PowerPutPixel(x, Y, GetPixel(x - Delta, Y), AlphaMap ? GetAlpha(x - Delta, Y) : 255, AVERAGE_PRIORITY);
    }
    for (x = 0; x < Delta; ++x) {
      PowerPutPixel(x, Y, TRANSPARENT_COLOR, 255, AVERAGE_PRIORITY);
    }
  }
}


//==========================================================================
//
//  bitmap::InterLace
//
//==========================================================================
void bitmap::InterLace () {
  for (int y = 0; y < mSize.Y; ++y) {
    if (!(y % 3)) {
      for (int x = 0; x < mSize.X; ++x) {
        if (Image[y][x] != 0) Image[y][x] = 1;
      }
    }
  }
}


//==========================================================================
//
//  bitmap::OutlineRect
//
//==========================================================================
void bitmap::OutlineRect (v2 pos, v2 size, col16 color) {
  if (size.X >= 3 && size.Y >= 3) {
    //FIXME: optimise this!
    uint8_t *xmap = new uint8_t[mSize.X * mSize.Y];
    for (int y = 0; y != mSize.Y; y += 1) {
      for (int x = 0; x != mSize.X; x += 1) {
        uint8_t v;
        if (GetPixel(x, y) != TRANSPARENT_COLOR) {
          v = 1;
        } else {
          v = 0;
        }
        xmap[y * mSize.X + x] = v;
      }
    }

    for (int ty = 0; ty < size.Y; ty += 1) {
      for (int tx = 0; tx < size.X; tx += 1) {
        const int xx = pos.X + tx;
        const int yy = pos.Y + ty;
        if (xx >= 0 && yy >= 0 && xx < mSize.X && yy < mSize.Y &&
            xmap[yy * mSize.X + xx] != 0)
        {
          for (int dy = -1; dy <= 1; dy += 1) {
            for (int dx = -1; dx <= 1; dx += 1) {
              if ((dx | dy) != 0 && (dx == 0 || dy == 0)) {
                const int sx = xx + dx;
                const int sy = yy + dy;
                if (sx >= 0 && sy >= 0 && sx < mSize.X && sy < mSize.Y &&
                    sx >= pos.X && sy >= pos.Y &&
                    sx < pos.X + size.X && sy < pos.Y + size.Y &&
                    xmap[sy * mSize.X + sx] == 0)
                {
                  PutPixel(sx, sy, color);
                }
              }
            }
          }
        }
      }
    }

    delete [] xmap;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct BezierInfo {
  bitmap *bmp;
  float x, y;
  col16 color;
  float invscale;
  float f2;
};


//==========================================================================
//
//  BezierPoint
//
//==========================================================================
static inline void BezierPoint (BezierInfo *nfo, float x, float y) {
  #if 1
  int ix0 = (int)(nfo->x * nfo->invscale + 0.5f);
  int iy0 = (int)(nfo->y * nfo->invscale + 0.5f);
  int ix1 = (int)(x * nfo->invscale + 0.5f);
  int iy1 = (int)(y * nfo->invscale + 0.5f);
  #else
  int ix0 = (int)(nfo->x * nfo->invscale);
  int iy0 = (int)(nfo->y * nfo->invscale);
  int ix1 = (int)(x * nfo->invscale);
  int iy1 = (int)(y * nfo->invscale);
  #endif
  if (ix0 != ix1 || iy0 != iy1) {
    nfo->bmp->DrawLine(ix0, iy0, ix1, iy1, nfo->color);
  }
  nfo->x = x; nfo->y = y;
}


//==========================================================================
//
//  BezierDCJ
//
//==========================================================================
static void BezierDCJ (BezierInfo *nfo,
                       float x1, float y1, float x2, float y2, float x3, float y3,
                       float x4, float y4, int level)
{
  const int MaxBezierLevel = 20; /* this is more than enough */
  const float TessTolSq = 1.0f * 1.0f /*0.5 * 0.5*/; /* more than enough even for antialiased rendering */
  float x12, y12, x23, y23, x34, y34, x123, y123;
  float dx, dy, d2, d3;
  float x234, y234, x1234, y1234;

  if (level <= MaxBezierLevel) {
    x12 = (x1 + x2) * 0.5f;
    y12 = (y1 + y2) * 0.5f;
    x23 = (x2 + x3) * 0.5f;
    y23 = (y2 + y3) * 0.5f;
    x34 = (x3 + x4) * 0.5f;
    y34 = (y3 + y4) * 0.5f;
    x123 = (x12 + x23) * 0.5f;
    y123 = (y12 + y23) * 0.5f;

    dx = x4 - x1;
    dy = y4 - y1;
    d2 = fabsf((x2 - x4) * dy - (y2 - y4) * dx);
    d3 = fabsf((x3 - x4) * dy - (y3 - y4) * dx);

    /* try to approximate the full cubic curve by a single straight line */
    if ((d2 + d3) * (d2 + d3) < TessTolSq * (dx * dx + dy * dy)) {
      BezierPoint(nfo, x4, y4);
    } else {
      x234 = (x23 + x34) * 0.5f;
      y234 = (y23 + y34) * 0.5f;
      x1234 = (x123 + x234) * 0.5f;
      y1234 = (y123 + y234) * 0.5f;
      /* "taxicab" / "manhattan" check for flat curves */
      if (fabsf(x1+x3-x2-x2)+fabsf(y1+y3-y2-y2)+fabsf(x2+x4-x3-x3)+fabsf(y2+y4-y3-y3) < TessTolSq*TessTolSq /* *0.25*/) {
        BezierPoint(nfo, x1234, y1234);
      } else {
        BezierDCJ(nfo, x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1);
        BezierDCJ(nfo, x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1);
      }
    }
  }
}


//==========================================================================
//
//  BezierOminc
//
//==========================================================================
static void BezierOminc (BezierInfo *nfo,
                         int x0, int y0, int xd0, int yd0,
                         int x1, int y1, int xd1, int yd1,
                         int level)
{
  int x, y, xd, yd;
  if (abs(xd0 - xd1) + abs(yd0 - yd1) < 8 || level > 64) {
    BezierPoint(nfo, x0, y0);
  } else {
    x = ((xd0 - xd1) / 4 + x0 + x1 + 1) / 2;
    y = ((yd0 - yd1) / 4 + y0 + y1 + 1) / 2;
    xd = ((x1 - x0) * 3 - (xd0 + xd1) / 2 + 2) / 4;
    yd = ((y1 - y0) * 3 - (yd0 + yd1) / 2 + 2) / 4;
    BezierOminc(nfo, x0, y0, xd0 / 2, yd0 / 2, x, y, xd, yd, level + 1);
    BezierOminc(nfo, x, y, xd, yd, x1, y1, xd1 / 2, yd1 / 2, level + 1);
  }
}


//==========================================================================
//
//  bitmap::Bezier
//
//==========================================================================
void bitmap::Bezier (int x1, int y1, int x2, int y2, int x3, int y3,
                     int x4, int y4, col16 Color)
{
  BezierInfo nfo;
  nfo.bmp = this;
  nfo.color = Color;
  nfo.invscale = 1.0f;
  #if 1
  nfo.x = x1; nfo.y = y1;
  BezierDCJ(&nfo, x1, y1, x2, y2, x3, y3, x4, y4, 0);
  #else
  nfo.x = x1; nfo.y = y1;
  BezierOminc(&nfo, x1, y1, (x2 - x1) * 3, (y2 - y1) * 3,
                    x4, y4, (x4 - x3) * 3, (y4 - y3) * 3, 0);
  #endif
  BezierPoint(&nfo, x4, y4);
}


//==========================================================================
//
//  bitmap::QuadBezier
//
//==========================================================================
void bitmap::QuadBezier (int x1, int y1, int x2, int y2, int x3, int y3, col16 Color) {
  const float scale = 16.0f;
  const float fx1 = x1 * scale;
  const float fy1 = y1 * scale;
  const float fx2 = x2 * scale;
  const float fy2 = y2 * scale;
  const float fx3 = x3 * scale;
  const float fy3 = y3 * scale;

  // first cubic control point
  float cp1xr = fx2 - fx1; cp1xr = fx1 + (cp1xr * 2.0f / 3.0f);
  float cp1yr = fy2 - fy1; cp1yr = fy1 + (cp1yr * 2.0f / 3.0f);
  // second cubic control point
  float cp2xr = fx2 - fx3; cp2xr = fx3 + (cp2xr * 2.0f / 3.0f);
  float cp2yr = fy2 - fy3; cp2yr = fy3 + (cp2yr * 2.0f / 3.0f);
  //Bezier(fx1, fy1, cp1xr, cp1yr, cp2xr, cp2yr, fx3, fy3, Color);

  BezierInfo nfo;
  nfo.bmp = this;
  nfo.color = Color;
  nfo.invscale = 1.0f / scale;
  nfo.x = fx1; nfo.y = fy1;
  #if 1
  BezierDCJ(&nfo, fx1, fy1, cp1xr, cp1yr, cp2xr, cp2yr, fx3, fy3, 0);
  #else
  BezierOminc(&nfo, x1, y1, (cp1xr - x1) * 3, (cp1yr - y1) * 3,
                    x3, y3, (x3 - cp2xr) * 3, (y3 - cp2yr) * 3, 0);
  #endif
  BezierPoint(&nfo, fx3, fy3);
}


//==========================================================================
//
//  bitmap::RoundRect
//
//==========================================================================
void bitmap::RoundRect (int x0, int y0, int width, int height, int radius, col16 color) {
  if (width < 2 || height < 2) return;
  int x1 = x0 + width - 1;
  int y1 = y0 + height - 1;
  if (radius < 1 || radius >= Min(width, height) / 2) {
    DrawRectangle(x0, y0, x1, y1, color);
  } else {
    // left top corner
    QuadBezier(x0, y0 + radius, x0, y0, x0 + radius, y0, color);
    // right top corner
    QuadBezier(x1, y0 + radius, x1, y0, x1 - radius, y0, color);
    // left bottom corner
    QuadBezier(x0, y1 - radius, x0, y1, x0 + radius, y1, color);
    // right bottom corner
    QuadBezier(x1, y1 - radius, x1, y1, x1 - radius, y1, color);
    // sides
    DrawHorizontalLine(x0 + radius, x1 - radius, y0, color); // top
    DrawHorizontalLine(x0 + radius, x1 - radius, y1, color); // bottom
    DrawVerticalLine(x0, y0 + radius, y1 - radius, color); // left
    DrawVerticalLine(x1, y0 + radius, y1 - radius, color); // right
  }
}


//==========================================================================
//
//  bitmap::DrawSimplePopup
//
//==========================================================================
void bitmap::DrawSimplePopup (v2 pos, v2 size, col16 Border, col16 Back) {
  if (size.X > 2 && size.Y > 2) {
    #if 0
    Fill(pos, size.X, size.Y, Back);
    DrawRectWH(pos, size.X, size.Y, Border);
    #else
    col16 shade = MakeRGB16(GetRed16(Border) - GetRed16(Border) / 3,
                            GetGreen16(Border) - GetGreen16(Border) / 3,
                            GetBlue16(Border) - GetBlue16(Border) / 3);
    Fill(pos + v2(1, 1), size.X - 1, size.Y - 1, Back);
    DrawHLine(pos + v2(1, 0),          size.X - 2, Border);
    DrawHLine(pos + v2(1, size.Y - 1), size.X - 2, Border);
    DrawVLine(pos + v2(0, 1),          size.Y - 2, Border);
    DrawVLine(pos + v2(size.X - 1, 1), size.Y - 2, Border);
    //
    //PutPixel(pos, shade);
    //PutPixel(pos + v2(size.X - 1, 0), shade);
    //PutPixel(pos + v2(0, size.Y - 1), shade);
    //PutPixel(pos + v2(size.X - 1, size.Y - 1), shade);
    //
    PutPixel(pos + v2(1, 1), shade);
    PutPixel(pos + v2(size.X - 2, 1), shade);
    PutPixel(pos + v2(1, size.Y - 2), shade);
    PutPixel(pos + v2(size.X - 2, size.Y - 2), shade);
    #endif
    //RoundRect(pos.X, pos.Y, size.X, size.Y, 6, LIGHT_GRAY);
  }
}


#if 1
static const float BlurKernel[3][3] = {
  { 0.3, 0.6, 0.3 },
  { 0.6, 0.8, 0.6 },
  { 0.3, 0.6, 0.3 },
};
#else
static const float BlurKernel[3][3] = {
  { 0.2, 0.5, 0.2 },
  { 0.5, 0.8, 0.5 },
  { 0.2, 0.5, 0.2 },
};
#endif


static float BlurSum = 0.0f;

//==========================================================================
//
//  bitmap::CreateFOWTo
//
//==========================================================================
void bitmap::CreateFOWTo (bitmap *DestBmp) {
  IvanAssert(DestBmp);
  const int ex = mSize.X;
  const int ey = mSize.Y;
  const int x1 = Min(ex, DestBmp->mSize.X);
  const int y1 = Min(ey, DestBmp->mSize.Y);
  if (BlurSum == 0.0f) {
    for (int dy = -1; dy <= 1; dy += 1) {
      for (int dx = -1; dx <= 1; dx += 1) {
        BlurSum += BlurKernel[dy + 1][dx + 1];
      }
    }
  }
  for (int y = 0; y != y1; y += 1) {
    for (int x = 0; x != x1; x += 1) {
      float r = 0.0f, g = 0.0f, b = 0.0f;
      for (int dy = -1; dy <= 1; dy += 1) {
        for (int dx = -1; dx <= 1; dx += 1) {
          #if 0
          const int sx = Clamp(x + dx, 0, ex - 1);
          const int sy = Clamp(y + dy, 0, ey - 1);
          #else
          int sx = x + dx;
          if (sx < 0) sx = 1 - sx; else if (sx >= ex) sx = ex - (sx - ex + 2);
          int sy = y + dy;
          if (sy < 0) sy = 1 - sy; else if (sy >= ey) sy = ey - (sy - ey + 2);
          #endif
          col16 cc = GetPixelUnsafe(sx, sy);
          const float m = BlurKernel[dy + 1][dx + 1];
          r += GetRed16(cc) * m;
          g += GetGreen16(cc) * m;
          b += GetBlue16(cc) * m;
        }
      }
      const float mult = 1.0f / (BlurSum + ((x ^ y) & 1 ? 1.8f : 0.6f));
      r *= mult;
      g *= mult;
      b *= mult;
      DestBmp->PutPixelUnsafe(x, y, MakeRGB16((int)r, (int)g, (int)b));
    }
  }
}
