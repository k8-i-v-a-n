/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_BITMAP_H__
#define __FELIB_BITMAP_H__

#include "felibdef.h"
#include "v2.h"
#include "vwfileio.h"
#include "sq3support.h"

class bitmap;
class rawbitmap;
class outputfile;
class inputfile;
class festring;

typedef void (*bitmapeditor) (bitmap *, truth);


// ////////////////////////////////////////////////////////////////////////// //
struct blitdata {
  bitmap *Bitmap;
  v2 Src;
  v2 Dest;
  v2 Border;
  union {
    int32_t Flags;
    int32_t Stretch;
    col24 Luminance;
  };
  col16 MaskColor;
  feuLong CustomData;
};


// ////////////////////////////////////////////////////////////////////////// //
class RawBitmapScreenShot {
  friend class bitmap;
private:
  v2 size;
  uChar *rgb16;

private:
  RawBitmapScreenShot () : size(0, 0), rgb16(0) {}
  RawBitmapScreenShot (v2 asize, const uChar *rgb);

  void Pack16 (const uChar *rgb);
  uChar *Unpack16 ();

public:
  RawBitmapScreenShot (SQArchive *sqa, SQAFile fl);
  ~RawBitmapScreenShot ();
  void Save (SQArchive *sqa, SQAFile fl);
};


// ////////////////////////////////////////////////////////////////////////// //
class bitmap {
public:
  bitmap (cfestring &FileName);
  bitmap (cbitmap *Bitmap, int Flags=0, truth CopyAlpha=true);
  bitmap (v2 aSize);
  bitmap (v2 aSize, col16 Color);
  ~bitmap ();

  void Save (outputfile &) const;
  void Load (inputfile &);

  void SavePNG (cfestring &) const;

  // used to save savegame screenshots
  RawBitmapScreenShot *CreateRaw (double scale) const;
  truth SetFromRaw (RawBitmapScreenShot *raw);

  FORCE_INLINE void PutPixel (int X, int Y, col16 Color) {
    if (X >= 0 && Y >= 0 && X < mSize.X && Y < mSize.Y) {
      Image[Y][X] = Color;
    }
  }
  FORCE_INLINE void PutPixel (v2 Pos, col16 Color) { PutPixel(Pos.X, Pos.Y, Color); }

  FORCE_INLINE col16 GetPixel (int X, int Y) const {
    if (X >= 0 && Y >= 0 && X < mSize.X && Y < mSize.Y) {
      return Image[Y][X];
    } else {
      return TRANSPARENT_COLOR;
    }
  }
  FORCE_INLINE col16 GetPixel (v2 Pos) const { return GetPixel(Pos.X, Pos.Y); }

  void PowerPutPixel (int, int, col16, alpha, priority);

  FORCE_INLINE col16 GetPixelUnsafe (int X, int Y) const { return Image[Y][X]; }
  FORCE_INLINE col16 GetPixelUnsafe (v2 Pos) const { return GetPixelUnsafe(Pos.X, Pos.Y); }

  FORCE_INLINE void PutPixelUnsafe (int X, int Y, col16 Color) { Image[Y][X] = Color; }
  FORCE_INLINE void PutPixelUnsafe (v2 Pos, col16 Color) { PutPixelUnsafe(Pos.X, Pos.Y, Color); }

  void Fill (int X, int Y, int Width, int Height, col16 Color);
  void Fill (v2 TopLeft, int Width, int Height, col16 Color);
  void Fill (int X, int Y, v2 FillSize, col16 Color);
  void Fill (v2 TopLeft, v2 FillSize, col16 Color);

  void ClearToColor (col16);
  void NormalBlit (cblitdata &) const;
  void NormalBlit (bitmap *Bitmap, int Flags=0) const;
  void FastBlit (bitmap *Bitmap) const;
  void FastBlit (bitmap *Bitmap, v2 Pos) const;
  void FastCopyFrom (bitmap *Bitmap, v2 Pos);

  void CreateFOWTo (bitmap *DestBmp);

  void DrawLine (int OrigFromX, int OrigFromY, int OrigToX, int OrigToY, col16 Color, truth Wide=false);
  void DrawLine (v2 From, int ToX, int ToY, col16 Color, truth Wide=false);
  void DrawLine (int FromX, int FromY, v2 To, col16 Color, truth Wide=false);
  void DrawLine (v2 From, v2 To, col16 Color, truth Wide=false);

  void DrawVerticalLine (int OrigX, int OrigFromY, int OrigToY, col16 Color, truth Wide=false);
  void DrawHorizontalLine (int OrigFromX, int OrigToX, int OrigY, col16 Color, truth Wide=false);

  FORCE_INLINE void DrawHLine (int x, int y, int len, col16 Color, truth Wide=false) {
    if (len > 0) DrawHorizontalLine(x, x + len - 1, y, Color, Wide);
  }

  FORCE_INLINE void DrawHLine (v2 xy, int len, col16 Color, truth Wide=false) {
    DrawHLine(xy.X, xy.Y, len, Color, Wide);
  }

  FORCE_INLINE void DrawVLine (int x, int y, int hgt, col16 Color, truth Wide=false) {
    if (hgt > 0) DrawVerticalLine(x, y, y + hgt - 1, Color, Wide);
  }

  FORCE_INLINE void DrawVLine (v2 xy, int hgt, col16 Color, truth Wide=false) {
    DrawVLine(xy.X, xy.Y, hgt, Color, Wide);
  }

  void DrawRectangle (int Left, int Top, int Right, int Bottom, col16 Color, truth Wide=false);
  void DrawRectangle (v2 TopLeft, int Right, int Bottom, col16 Color, truth Wide=false);
  void DrawRectangle (int Left, int Top, v2 BottomRight, col16 Color, truth Wide=false);
  void DrawRectangle (v2 TopLeft, v2 BottomRight, col16 Color, truth Wide=false);

  FORCE_INLINE void DrawRectWH (int x0, int y0, int width, int height, col16 Color, truth wide=false) {
    if (width < 1 || height < 1) return;
    if (width == 1) {
      DrawVerticalLine(x0, y0, y0 + height - 1, Color, wide);
    } else if (height == 1) {
      DrawHorizontalLine(x0, x0 + width - 1, y0, Color, wide);
    } else {
      DrawRectangle(x0, y0, x0 + width - 1, y0 + height - 1, Color, wide);
    }
  }

  FORCE_INLINE void DrawRectWH (v2 xy0, v2 size, col16 Color, truth wide=false) {
    DrawRectWH(xy0.X, xy0.Y, size.X, size.Y, Color, wide);
  }

  FORCE_INLINE void DrawRectWH (int x0, int y0, v2 size, col16 Color, truth wide=false) {
    DrawRectWH(x0, y0, size.X, size.Y, Color, wide);
  }

  FORCE_INLINE void DrawRectWH (v2 xy0, int width, int height, col16 Color, truth wide=false) {
    DrawRectWH(xy0.X, xy0.Y, width, height, Color, wide);
  }

  void DrawSimplePopup (v2 pos, v2 size, col16 Border, col16 Back);

  /*
  void Bezier (float x0, float y0, float x1, float y1, float x2, float y2,
               float x, float y, col16 Color);
  */

  void Bezier (int x0, int y0, int x1, int y1, int x2, int y2, int x3, int y3, col16 Color);
  void QuadBezier (int x0, int y0, int x1, int y1, int x2, int y2, col16 Color);

  void RoundRect (int x0, int y0, int width, int height, int radius, col16 color);

  void LuminanceBlit (cblitdata &) const;
  void NormalMaskedBlit (cblitdata &) const;
  void LuminanceMaskedBlit (cblitdata &) const;
  void SimpleAlphaBlit (bitmap *, alpha, col16 = TRANSPARENT_COLOR) const;
  void AlphaMaskedBlit (cblitdata &) const;
  void AlphaLuminanceBlit (cblitdata &) const;
  void StretchBlit (cblitdata &) const;

  void BlitAndCopyAlpha (bitmap *, int = 0) const;
  void MaskedPriorityBlit (cblitdata &) const;
  void AlphaPriorityBlit (cblitdata &) const;
  void FastBlitAndCopyAlpha (bitmap *) const;

  void BlitMono (cblitdata &BlitData, col16 MonoColor) const;

  v2 GetSize () const { return mSize; }
  int GetWidth () const { return mSize.X; }
  int GetHeight () const { return mSize.Y; }
  void DrawPolygon (int CenterX, int CenterY, int Radius, int NumberOfSides,
                    col16 Color, truth DrawSides=true, truth DrawDiameters=false, double Rotation=0);
  void CreateAlphaMap (alpha);
  truth Fade (sLong &, packalpha &, int);
  void SetAlpha (int X, int Y, alpha Alpha) { AlphaMap[Y][X] = Alpha; }
  void SetAlpha (v2 Pos, alpha Alpha) { AlphaMap[Pos.Y][Pos.X] = Alpha; }
  alpha GetAlpha (int X, int Y) const { return AlphaMap[Y][X]; }
  alpha GetAlpha (v2 Pos) const { return AlphaMap[Pos.Y][Pos.X]; }
  void Outline (col16, alpha, priority);
  void FadeToScreen (bitmapeditor = 0);
  void CreateFlames (rawbitmap *, v2, feuLong, int);
  truth IsValidPos (v2 What) const { return What.X >= 0 && What.Y >= 0 && What.X < mSize.X && What.Y < mSize.Y; }
  truth IsValidPos (int X, int Y) const { return X >= 0 && Y >= 0 && X < mSize.X && Y < mSize.Y; }
  void CreateSparkle (v2, int);
  void CreateFlies (feuLong, int, int);
  void CreateLightning (feuLong, col16);
  truth CreateLightning (v2, v2, int, col16);
  packcol16 **GetImage () const { return Image; }
  packalpha **GetAlphaMap () const { return AlphaMap; }
  static truth PixelVectorHandler (sLong, sLong);
  void FillAlpha (alpha);
  void InitPriorityMap (priority);
  void FillPriority (priority);
  void SafeSetPriority (int, int, priority);
  void SafeSetPriority (v2 Pos, priority What) { SafeSetPriority(Pos.X, Pos.Y, What); }
  void SafeUpdateRandMap (v2, truth);
  void UpdateRandMap (sLong, truth);
  void InitRandMap ();
  v2 RandomizePixel () const;
  void AlphaPutPixel (int x, int y, col16 SrcCol, col24 Luminance, alpha Alpha);
  void AlphaPutPixel (v2 Pos, col16 Color, col24 Luminance, alpha Alpha) { AlphaPutPixel(Pos.X, Pos.Y, Color, Luminance, Alpha); }
  void CalculateRandMap ();
  alpha CalculateAlphaAverage () const;
  void ActivateFastFlag () { FastFlag = 1; }
  void DeactivateFastFlag () { FastFlag = 0; }
  void Wobble (int, int, truth);
  void MoveLineVertically (int, int);
  void MoveLineHorizontally (int, int);
  void InterLace ();
  void OutlineRect (v2 pos, v2 size, col16 color);

private:
  //RGB (3 bytes per pixel)
  uChar *createScaled (double scale, v2* size) const;

protected:
  v2 mSize;
  feuLong XSizeTimesYSize : 31;
  feuLong FastFlag : 1;
  packcol16 **Image;
  packalpha **AlphaMap;
  packpriority **PriorityMap;
  truth *RandMap;
};


FORCE_INLINE void bitmap::SafeUpdateRandMap (v2 Pos, truth What) {
  if (RandMap) UpdateRandMap(Pos.Y*mSize.X+Pos.X, What);
}


FORCE_INLINE void bitmap::SafeSetPriority (int x, int y, priority What) {
  if (PriorityMap) PriorityMap[y][x] = What;
}


outputfile &operator << (outputfile &, cbitmap *);
inputfile &operator >> (inputfile &, bitmap *&);


#endif
