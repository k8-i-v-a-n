/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include <limits.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#ifdef SHITDOZE
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
#endif
#include <algorithm>

#include "cconsole.h"
#include "feerror.h"
#include "festring.h"
#include "fesave.h"

#define FE_PUBLIC


// ////////////////////////////////////////////////////////////////////////// //
// comatoze
// ////////////////////////////////////////////////////////////////////////// //

#define COMATOZE_BUF_NUM   (16u)
#define COMATOZE_BUF_SIZE  (128u)

static char comatozebuf[COMATOZE_BUF_NUM][COMATOZE_BUF_SIZE];
static uint32_t comatozebufnext = 0;


//==========================================================================
//
//  NextComatozeBuf
//
//==========================================================================
static FORCE_INLINE char *NextComatozeBuf () {
  char *cbuf = comatozebuf[comatozebufnext];
  comatozebufnext = (comatozebufnext + 1)%COMATOZE_BUF_NUM;
  return cbuf;
}


//==========================================================================
//
//  comatozeU32
//
//==========================================================================
const char *ComatozeU32 (uint32_t n){
  char *cbuf = NextComatozeBuf() + COMATOZE_BUF_SIZE;
  *--cbuf = 0;
  int digleft = 3;
  do {
    if (digleft == 0) { *--cbuf = ','; digleft = 3; }
    *--cbuf = (n % 10u) + '0';
    digleft -= 1;
  } while ((n /= 10u) != 0);
  return cbuf;
}


//==========================================================================
//
//  comatozeI32
//
//==========================================================================
const char *ComatozeI32 (int32_t n) {
  char *cbuf;
  if ((uint32_t)n == 0x80000000u) {
    cbuf = (char *)ComatozeU32((uint32_t)n);
  } else {
    cbuf = (char *)ComatozeU32((uint32_t)abs(n));
  }
  if (n < 0) {
    *--cbuf = '-';
  }
  return cbuf;
}


//==========================================================================
//
//  IsSpace
//
//==========================================================================
static FORCE_INLINE bool IsSpace (char ch) {
  return (ch > 0 && ch <= 32);
}


//==========================================================================
//
//  CharEquCI
//
//==========================================================================
static FORCE_INLINE bool CharEquCI (char c0, char c1) {
  if (c0 >= 'A' && c0 <= 'Z') c0 += 32;
  if (c1 >= 'A' && c1 <= 'Z') c1 += 32;
  return (c0 == c1);
}


//==========================================================================
//
//  SafeChar
//
//==========================================================================
static FORCE_INLINE char SafeChar (char ch) {
  return ((ch >= 0 && ch <= 32) || ch == 127 ? ' ' : ch);
}


//==========================================================================
//
//  IsControlChar
//
//==========================================================================
static FORCE_INLINE bool IsControlChar (char ch) {
  return (ch == '\t' || ch == '\n' || ch == '\r');
}


// ////////////////////////////////////////////////////////////////////////// //
union cbtree_node {
  // internal node
  struct inode_t {
    // if bit 1 is set, this is an internal node, otherwise it is a value node
    void *child[2]; // [0] is `next_free`
    // byte index at which bits are different
    uint32_t byte;
    // in `otherbits` all bits except the critical bit are set
    uint8_t otherbits;
  } inode;
  // value node
  CBTree::Value value;
  // free node
  //cbtree_node_t *next_free; // next free node, or nullptr
};


// internal nodes have bit 0 in their pointer set
#define ISINTRNODE(ptr_)  (((uint8_t)(intptr_t)(ptr_))&0x01U)
#define NODEADDR(ptr_)    ((cbtree_node *)((char *)(ptr_)-1))
#define MKINTRNODE(ptr_)  ((void *)((char *)(ptr_)+1))


//==========================================================================
//
//  xstlen
//
//==========================================================================
static FORCE_INLINE int xstlen (const void *u, const int ulen) {
  return (ulen >= 0 ? ulen : u ? (int)strlen((const char *)u) : 0);
}


struct KBuf {
private:
  uint8_t kbuf[128];

public:
  const void *lokey; // locased
  const void *realkey; // real, not copied
  uint32_t klen; // length

public:
  inline KBuf (const void *key, int keylen) {
    keylen = xstlen(key, keylen);
    realkey = key; klen = (uint32_t)keylen;
    // create locase key
    if (keylen) {
      uint8_t *kp;
      if (keylen > (int)sizeof(kbuf)) {
        kp = (uint8_t *)::malloc(klen);
      } else {
        kp = kbuf;
      }
      lokey = kp;
      for (uint32_t f = 0; f != (uint32_t)keylen; f += 1) {
        uint8_t ch = ((const uint8_t *)key)[f];
        if (ch >= 'A' && ch <= 'Z') ch += 32;
        kp[f] = ch;
      }
    } else {
      lokey = kbuf; // doesn't matter
    }
  }

  inline ~KBuf () {
    if (lokey != kbuf) ::free((void *)lokey);
    lokey = kbuf; klen = 0; // just in case
  }

  // no copies!
  KBuf (const KBuf &) = delete;
  KBuf &operator = (const KBuf &) = delete;
};


//==========================================================================
//
//  CBTree::alloc_node
//
//  returns new node pointer or `nullptr`
//
//==========================================================================
cbtree_node *CBTree::alloc_node () {
  cbtree_node *res = (cbtree_node *)this->free_list;
  if (!res) {
    // no free nodes, allocate a new one
    res = (cbtree_node *)::malloc(sizeof(cbtree_node));
    if (!res) ABORT("out of memory for critbit this nodes");
  } else {
    this->free_list = res->inode.child[0];
  }
  this->node_count += 1;
  memset((void *)res, 0, sizeof(*res));
  return res;
}


//==========================================================================
//
//  CBTree::alloc_value_node
//
//==========================================================================
cbtree_node *CBTree::alloc_value_node (const KBuf &key, CTData *udata) {
  cbtree_node *node = alloc_node();
  if (node) {
    if (key.klen) {
      node->value.key = (const void *)::calloc(1, key.klen + 1);
      if (!node->value.key) ABORT("out of memory for crit-this key");
      node->value.realkey = (const void *)::calloc(1, key.klen + 1);
      if (!node->value.realkey) ABORT("out of memory for crit-this key");
      #if 0
      fprintf(stderr, "len=%u; <%.*s> : <%.*s>\n", key.klen,
              key.klen, (const char *)key.lokey,
              key.klen, (const char *)key.realkey);
      #endif
      memcpy((void *)node->value.key, key.lokey, key.klen);
      memcpy((void *)node->value.realkey, key.realkey, key.klen);
      memcpy((void *)(&node->value.klen), &key.klen, 4);
    }
    node->value.udata = udata;
  }
  return node;
}


//==========================================================================
//
//  CBTree::release_node
//
//==========================================================================
void CBTree::release_node (cbtree_node *node) {
  node->inode.child[0] = this->free_list;
  this->free_list = node;
  //this->node_count -= 1;
}


//==========================================================================
//
//  equ_value_node
//
//==========================================================================
static FORCE_INLINE int equ_value_node (const cbtree_node *vnode, const void *u, int ulen) {
  return
    (vnode->value.klen == (uint32_t)ulen) &&
    (!ulen || ::memcmp(vnode->value.key, u, (uint32_t)ulen) == 0);
}


//==========================================================================
//
//  CBTree::clear_traverse
//
//==========================================================================
void CBTree::clear_traverse (void *top) {
  uint8_t *p = (uint8_t *)top;
  if (ISINTRNODE(p)) {
    cbtree_node *q = NODEADDR(p);
    clear_traverse(q->inode.child[0]);
    clear_traverse(q->inode.child[1]);
    release_node(q);
  } else {
    delete ((cbtree_node *)p)->value.udata;
    ::free((void *)(((cbtree_node *)p)->value.key));
    ::free((void *)(((cbtree_node *)p)->value.realkey));
    release_node((cbtree_node *)p);
  }
}


//==========================================================================
//
//  CBTree::Compress
//
//  deallocate all unused free nodes
//
//==========================================================================
void CBTree::Compress () {
  while (this->free_list) {
    cbtree_node *nn = (cbtree_node *)this->free_list;
    this->free_list = nn->inode.child[0];
    ::free(nn);
    this->node_count -= 1;
  }
}


//==========================================================================
//
//  CBTree::Clear
//
//==========================================================================
void CBTree::Clear () {
  if (this->root) clear_traverse(this->root);
  Compress();
  this->root = 0;
  this->free_list = 0;
  this->node_count = 0;
  this->item_count = 0;
}


//==========================================================================
//
//  CBTree::Find
//
//  returns non-NULL if the key `key` is in the this
//
//==========================================================================
CBTree::Value *CBTree::Find (const void *key, int keylen) {
  if (!this->root) return nullptr;
  KBuf kbuf(key, keylen);

  const uint8_t *ubytes = (const uint8_t *)kbuf.lokey;
  const uint8_t uulen = kbuf.klen;
  uint8_t *p = (uint8_t *)this->root;

  while (ISINTRNODE(p)) {
    cbtree_node *q = NODEADDR(p);
    uint8_t c = 0;
    if (q->inode.byte < uulen) c = ubytes[q->inode.byte];
    const unsigned dir = (1U+(q->inode.otherbits|c))>>8;
    p = (uint8_t *)(q->inode.child[dir]);
  }

  return
    equ_value_node((cbtree_node *)p, kbuf.lokey, (int)uulen)
          ? &((cbtree_node *)p)->value : nullptr;
}


//==========================================================================
//
//  CBTree::Insert
//
//  insert null-terminated string `key` into the this
//  will NOT replace the existing data
//
//==========================================================================
int CBTree::Insert (const void *key, int keylen, CTData *udata) {
  if (!udata) return -1;
  KBuf kbuf(key, keylen);
  if (kbuf.lokey && kbuf.klen == 0) return -1;

  const uint8_t *ubytes = (const uint8_t *)kbuf.lokey;
  const uint32_t ulen = kbuf.klen;
  uint8_t *p = (uint8_t *)this->root;

  // empty tree: allocate terminal node (value node)
  if (!p) {
    this->root = alloc_value_node(kbuf, udata);
    if (!this->root) return -1;
    this->item_count = 1U;
    return 0;
  } else {
    while (ISINTRNODE(p)) {
      cbtree_node *q = NODEADDR(p);
      uint8_t c = 0;
      if (q->inode.byte < ulen) c = ubytes[q->inode.byte];
      const unsigned dir = (1U+(q->inode.otherbits|c))>>8;
      p = (uint8_t *)(q->inode.child[dir]);
    }

    uint32_t newotherbits;

    const uint32_t plen = ((cbtree_node *)p)->value.klen;
    const uint8_t *pdata = (const uint8_t *)(((cbtree_node *)p)->value.key);
    uint32_t newbyte = 0;
    bool addNewKey = false;
    while (newbyte < ulen && !addNewKey) {
      const uint8_t pbyte = (newbyte < plen ? *pdata : 0);
      if (pbyte != ubytes[newbyte]) {
        newotherbits = pbyte ^ ubytes[newbyte];
        addNewKey = true;
      } else {
        newbyte += 1; pdata += 1;
      }
    }

    if (!addNewKey && newbyte < plen) {
      //newotherbits = p[newbyte];
      newotherbits = *pdata;
      //goto different_byte_found;
      addNewKey = true;
    }

    //return 1; // in the this already
    if (addNewKey) {
      //different_byte_found:
      // we recursively fold the upper bits into the lower bits to yield a byte `x` with
      // all true bits below the most significant bit. then `x&~(x>>1)` yields the
      // most significant bit.
      // once we have this value, we invert all the bits resulting in a value suitable
      // for our `otherbits` member.
      newotherbits |= newotherbits>>1;
      newotherbits |= newotherbits>>2;
      newotherbits |= newotherbits>>4;
      newotherbits = (newotherbits&~(newotherbits>>1))^0xffU;

      //uint8_t c = p[newbyte];
      const uint8_t c = (newbyte < plen ? *pdata : 0);
      const unsigned newdir = (1U+(newotherbits|c))>>8;

      // allocate non-terminal node
      cbtree_node *newnode = alloc_node();
      if (!newnode) return -1;

      // allocate terminal node (value node)
      cbtree_node *vnode = alloc_value_node(kbuf, udata);
      if (!vnode) { release_node(newnode); return -1; }
      //memcpy(&vnode->value, udata, sizeof(CBTree::Value));
      this->item_count += 1;

      newnode->inode.byte = newbyte;
      newnode->inode.otherbits = newotherbits;
      newnode->inode.child[1U-newdir] = vnode;

      void **wherep = &this->root;
      for (;;) {
        uint8_t *xp = (uint8_t *)(*wherep);
        if (!(ISINTRNODE(xp))) break;
        cbtree_node *q = NODEADDR(xp);
        if (q->inode.byte > newbyte) break;
        if (q->inode.byte == newbyte && q->inode.otherbits > newotherbits) break;
        uint8_t xc = 0;
        if (q->inode.byte < ulen) xc = ubytes[q->inode.byte];
        const unsigned dir = (1U+(q->inode.otherbits|xc))>>8;
        wherep = q->inode.child+dir;
      }

      newnode->inode.child[newdir] = *wherep;
      *wherep = MKINTRNODE(newnode);

      return 0;
    }

    return 1;
  }
}


//==========================================================================
//
//  CBTree::Delete
//
//  returns non-zero if the string was succesfully found and deleted
//
//==========================================================================
bool CBTree::Delete (const void *key, int keylen) {
  if (!this->root) return 0;
  KBuf kbuf(key, keylen);

  const uint8_t *ubytes = (const uint8_t *)kbuf.lokey;
  const uint32_t ulen = kbuf.klen;
  uint8_t *p = (uint8_t *)this->root;
  void **wherep = &this->root;
  void **whereq = nullptr;
  cbtree_node *q = nullptr;
  unsigned dir = 0;

  while (ISINTRNODE(p)) {
    whereq = wherep;
    q = NODEADDR(p);
    uint8_t c = 0;
    if (q->inode.byte < ulen) c = ubytes[q->inode.byte];
    dir = (1U+(q->inode.otherbits|c))>>8;
    wherep = q->inode.child+dir;
    p = (uint8_t *)(*wherep);
  }

  if (!equ_value_node((cbtree_node *)p, kbuf.lokey, ulen)) return false;

  delete ((cbtree_node *)p)->value.udata;
  ::free((void *)(((cbtree_node *)p)->value.key));
  ::free((void *)(((cbtree_node *)p)->value.realkey));
  release_node((cbtree_node *)p);
  this->item_count -= 1;

  if (!whereq) {
    this->root = nullptr;
  } else {
    *whereq = q->inode.child[1U-dir];
    release_node(q);
  }

  return true;
}


//==========================================================================
//
//  allprefixed_traverse
//
//==========================================================================
static int allprefixed_traverse (uint8_t *top,
                                 int (*ncb) (const CBTree::Value *val, void *uarg),
                                 void *uarg)
{
  if (ISINTRNODE(top)) {
    cbtree_node *q = NODEADDR(top);
    for (unsigned dir = 0; dir < 2; ++dir) {
      const int res = allprefixed_traverse((uint8_t *)(q->inode.child[dir]), ncb, uarg);
      if (res) return res;
    }
    return 0;
  }
  return ncb(&((cbtree_node *)top)->value, uarg);
}


//==========================================================================
//
//  CBTree::PrefixIterate
//
//==========================================================================
int CBTree::PrefixIterate (const void *prefix, int pfxlen,
                           int (*ncb) (const CBTree::Value *val, void *uarg),
                           void *uarg)
{
  if (!ncb) return -1;
  if (!this->root) return 0;
  KBuf kpfx(prefix, pfxlen);

  const uint8_t *ubytes = (const uint8_t *)kpfx.lokey;
  uint8_t *p = (uint8_t *)this->root;
  uint8_t *top = p;

  while (ISINTRNODE(p)) {
    cbtree_node *q = NODEADDR(p);
    uint8_t c = 0;
    if (q->inode.byte < kpfx.klen) c = ubytes[q->inode.byte];
    const unsigned dir = (1U+(q->inode.otherbits|c))>>8;
    p = (uint8_t *)(q->inode.child[dir]);
    if (q->inode.byte < kpfx.klen) top = p;
  }

  // check prefix
  cbtree_node *vn = (cbtree_node *)p;
  if (vn->value.klen < kpfx.klen) return 0;
  if (kpfx.klen && memcmp(vn->value.key, kpfx.lokey, kpfx.klen) != 0) return 0;

  return allprefixed_traverse(top, ncb, uarg);
}


//==========================================================================
//
//  AllTraverse
//
//==========================================================================
static int AllTraverse (void *top, int indent,
                        int (*ncb) (const CBTree::Value *val, void *uarg), void *uarg)
{
  //for (int f = 0; f < indent; ++f) fputc(' ', stderr);
  int res;
  uint8_t *p = (uint8_t *)top;
  if (ISINTRNODE(p)) {
    cbtree_node *q = NODEADDR(p);
    //fprintf(stderr, "INODE:%p: otherbits=0x%02x; byte=0x%08x\n", q, q->otherbits, q->byte);
    res = AllTraverse(q->inode.child[0], indent + 2, ncb, uarg);
    if (res == 0) {
      res = AllTraverse(q->inode.child[1], indent + 2, ncb, uarg);
    }
  } else {
    cbtree_node *vn = (cbtree_node *)p;
    res = ncb(&vn->value, uarg);
  }
  return res;
}


//==========================================================================
//
//  CBTree::IterateAll
//
//==========================================================================
int CBTree::IterateAll (int (*ncb) (const CBTree::Value *val, void *uarg), void *uarg) {
  if (!ncb) return -1;
  if (!this->root) return 0;
  return AllTraverse(this->root, 0, ncb, uarg);
}


// ////////////////////////////////////////////////////////////////////////// //
// console buffer

#define BUF_LINES  (1024)

struct CBLine {
  char *data; // [cbwidth + 1]
  int len; // real length
  int wrapped;
};


uint32_t ConUpdateCount = 0;

static uint32_t cbtail = 0;
static uint32_t cbwidth = 80;
static uint32_t cbpos = 0;
static uint32_t cblinecount = 1;
static bool cbnlpending = false;
static CBLine cblines[BUF_LINES] = {0};
static int cbOutputFD = -1;
static char cboutbuf[512];
static size_t cbobpos = 0;
static int conOutputDisabled = 0;
static char ccprintbuf[16384];


//==========================================================================
//
//  ConDisableOutput
//
//==========================================================================
void ConDisableOutput () {
  if (conOutputDisabled == 0) {
    conOutputDisabled = 1;
    for (int f = 0; f != BUF_LINES; f += 1) {
      ::free(cblines[f].data);
    }
    memset(cblines, 0, sizeof(cblines));
    if (cbOutputFD > 2) {
      close(cbOutputFD);
    }
    cbOutputFD = -1;
  }
}


//==========================================================================
//
//  ConEnableStdErr
//
//==========================================================================
void ConEnableStdErr () {
  #ifdef SHITDOZE
  festring mydir = inputfile::GetMyDir();
  mydir << "/" << "console.log";
  cbOutputFD = open(mydir.CStr(), O_WRONLY | O_CREAT | O_APPEND, 0666);
  #else
  cbOutputFD = 2;
  #endif
}


//==========================================================================
//
//  ConEnsureLineData
//
//==========================================================================
static inline void ConEnsureLineData (CBLine *line) {
  IvanAssert(line);
  IvanAssert(conOutputDisabled == 0);
  if (!line->data) {
    IvanAssert(cbwidth > 0 && cbwidth < 4096);
    line->data = (char *)::malloc(cbwidth + 1);
    if (!line->data) ABORT("out of memory for console data");
    line->data[0] = 0;
    line->len = 0;
  }
}


//==========================================================================
//
//  ConNewLine
//
//==========================================================================
static inline void ConNewLine (int wrapped) {
  IvanAssert(conOutputDisabled == 0);
  cblines[cbtail].wrapped = wrapped;
  cbtail = (cbtail + 1) % BUF_LINES;
  ConEnsureLineData(&cblines[cbtail]);
  cblines[cbtail].data[0] = 0;
  cblines[cbtail].len = 0;
  cblines[cbtail].wrapped = 0;
  cbpos = 0;
  cblinecount = Min(cblinecount + 1, (uint32_t)BUF_LINES);
  cbnlpending = false;
}


//==========================================================================
//
//  ConFlushFD
//
//==========================================================================
static void ConFlushFD () {
  if (cbOutputFD >= 0 && cbobpos != 0) {
    ::write(cbOutputFD, cboutbuf, cbobpos);
  }
  cbobpos = 0;
}


//==========================================================================
//
//  ConAppendChar
//
//==========================================================================
static void ConAppendChar (char ch) {
  if (conOutputDisabled) return;
  if (cbOutputFD >= 0) {
    if (cbobpos == sizeof(cboutbuf)) ConFlushFD();
    cboutbuf[cbobpos] = (IsControlChar(ch) ? ch : SafeChar(ch));
    cbobpos += 1;
    if (ch == '\n' || ch == '\r') ConFlushFD();
  }
  ConEnsureLineData(&cblines[cbtail]);
  if (ch == '\n') {
    if (cbnlpending) ConNewLine(0);
    cbnlpending = true;
  } else if (ch == '\r') {
    if (cbnlpending) ConNewLine(0);
    cblines[cbtail].wrapped = 0;
    cbpos = 0;
  } else if (ch == 9) {
    if (cbnlpending) ConNewLine(0);
    uint32_t left = 8 - (cbpos % 8);
    while (left) {
      if (cbpos == cbwidth) {
        ConNewLine(1);
        return;
      }
      left -= 1;
      ConAppendChar(' ');
    }
  } else {
    if (cbnlpending) ConNewLine(0);
    if (ch >= 0 && ch < 32) ch = 32;
    if (cbpos == cbwidth) ConNewLine(1);
    cblines[cbtail].data[cbpos] = ch;
    if (cbpos >= (uint32_t)cblines[cbtail].len) {
      cblines[cbtail].data[cbpos + 1] = 0;
      cblines[cbtail].len += 1;
    }
    cbpos += 1;
  }
}


//==========================================================================
//
//  ConAppend
//
//==========================================================================
static void ConAppend (const void *sdata, uint32_t slen) {
  if (slen == 0) return;
  if (conOutputDisabled) return;
  ConUpdateCount += 1;
  const char *str = (const char *)sdata;
  bool spacePending = false;
  RawTextData raw(str, slen);
  while (!raw.IsEmpty()) {
    char ch = raw.SkipChar();
    if (ch != 0 && RawTextData::IsPadBlank(ch)) {
      spacePending = true;
    } else {
      if (spacePending) {
        ConAppendChar(' ');
        spacePending = false;
      }
      ConAppendChar(ch);
    }
  }
}


//==========================================================================
//
//  ConReformat
//
//==========================================================================
static void ConReformat (uint32_t newwdt) {
  IvanAssert(newwdt > 0 && newwdt < 4096);
  if (conOutputDisabled) return;
  if (newwdt != cbwidth) {
    CBLine *xarr = (CBLine *)::malloc(sizeof(CBLine) * BUF_LINES);
    IvanAssert(xarr);
    for (int f = 0; f != BUF_LINES; f += 1) {
      xarr[f] = cblines[f];
    }
    memset(cblines, 0, sizeof(cblines));
    uint32_t ln = (cbtail + 1) % BUF_LINES;
    cbtail = 0;
    cbwidth = newwdt;
    cbpos = 0;
    cblinecount = 1;
    cbnlpending = false;
    cbobpos = 0;
    const int cbfd = cbOutputFD;
    cbOutputFD = -1;
    for (int f = 0; f != BUF_LINES; f += 1) {
      if (xarr[ln].data) {
        const char *str = xarr[ln].data;
        while (*str) {
          ConAppendChar(*str);
          str += 1;
        }
        if (!xarr[ln].wrapped) ConAppendChar('\n');
      }
      ln = (ln + 1) % BUF_LINES;
    }
    cbOutputFD = cbfd;
    for (int f = 0; f != BUF_LINES; f += 1) {
      ::free(xarr[f].data);
    }
    ::free(xarr);
  }
}


static CBTree *cmdlistPtr = 0;


//==========================================================================
//
//  CmdList
//
//==========================================================================
static FORCE_INLINE CBTree &CmdList () {
  if (!cmdlistPtr) {
    cmdlistPtr = new CBTree();
  }
  return *cmdlistPtr;
}


//==========================================================================
//
//  ConInit
//
//  call this on startup to initialise the buffer
//
//==========================================================================
void ConInit (int wdt) {
  //TODO: reformat!
  IvanAssert(wdt >= 8 && wdt < 32760);
  /*
  for (uint32_t f = 0; f != BUF_LINES; f += 1) {
    ::free(cblines[f].data);
  }
  memset(&cblines[0], 0, sizeof(cblines));
  cbtail = 0;
  cbwidth = (uint32_t)wdt / 8;
  cbpos = 0;
  cbnlpending = false;
  cbobpos = 0;
  */
  ConReformat((uint32_t)wdt / 8);
}


//==========================================================================
//
//  IsEmptyString
//
//==========================================================================
static inline bool IsEmptyString (const char *str, int slen) {
  if (slen != 0) {
    RawTextData raw(str, slen);
    while (!raw.IsEmpty()) {
      char ch = raw.SkipChar();
      if (ch != 0) {
        if (ch >= 0 && ch <= 32) {
          if (ch == 13 || ch == 10) return false;
        } else {
          return false;
        }
      }
    }
  }
  return true;
}


//==========================================================================
//
//  ConPrintVA
//
//==========================================================================
static void ConPrintVA (va_list aporig, const char *fmt, bool newline) {
  if (conOutputDisabled) return;
  char *bufptr = ccprintbuf;
  int bufsz = (int)sizeof(ccprintbuf) - 1;
  for (;;) {
    va_list ap;
    int n;
    char *np;
    va_copy(ap, aporig);
    n = vsnprintf(bufptr, bufsz, fmt, ap);
    va_end(ap);
    if (n > -1 && n < bufsz) {
      if (!IsEmptyString(bufptr, n)) {
        ConAppend(bufptr, (uint32_t)n);
        if (newline) ConAppendChar('\n');
      }
      if (bufptr != ccprintbuf) ::free(bufptr);
      return;
    }
    if (n < -1) n = bufsz + 4096;
    if (bufptr == ccprintbuf) {
      np = (char *)::malloc(n + 1);
    } else {
      np = (char *)::realloc(bufptr, n + 1);
    }
    if (np == NULL) {
      //__builtin_trap();
      if (bufptr != ccprintbuf) ::free(bufptr);
      return;
    }
    bufptr = np;
  }
}


//==========================================================================
//
//  ConLogf
//
//==========================================================================
LIKE_PRINTF(1, 2) void ConLogf (const char *fmt, ...) {
  if (conOutputDisabled == 0) {
    va_list ap;
    va_start(ap, fmt);
    ConPrintVA(ap, fmt, true);
    va_end(ap);
  }
}


//==========================================================================
//
//  ConPrintf
//
//==========================================================================
LIKE_PRINTF(1, 2) void ConPrintf (const char *fmt, ...) {
  if (conOutputDisabled == 0) {
    va_list ap;
    va_start(ap, fmt);
    ConPrintVA(ap, fmt, false);
    va_end(ap);
  }
}


static const char *emptyStr = "";

//==========================================================================
//
//  ConGetLine
//
//  can be used to render console text.
//  return `false` if no more lines.
//  `text` is ASCIIZ.
//  `0` is the last line, `1` is the previous line, etc.
//
//==========================================================================
bool ConGetLine (uint32_t lineidx, const char *&text, int &textlen) {
  if (lineidx < 0 || lineidx >= BUF_LINES || conOutputDisabled) {
    text = 0; textlen = 0;
    return false;
  } else {
    uint32_t ll = (cbtail - lineidx) % BUF_LINES;
    if (!cblines[ll].data) {
      text = emptyStr; textlen = 0;
    } else {
      text = cblines[ll].data;
      //textlen = cblines[ll].len;
      textlen = (int)strlen(cblines[ll].data);
    }
    return true;
  }
}


//==========================================================================
//
//  ConMaxLines
//
//==========================================================================
int ConMaxLines () {
  return (int)cblinecount;
}


// ////////////////////////////////////////////////////////////////////////// //
// console commands

//==========================================================================
//
//  ConParse
//
//  will not clear `dest`!
//
//==========================================================================
void ConParse (std::vector<festring> &dest, const char *cmd) {
  if (!cmd) return;
  while (*cmd && IsSpace(*cmd)) cmd += 1;
  if (!cmd[0]) return;

  festring curarg;
  while (*cmd) {
    curarg.Empty();
    if (*cmd == '"' || *cmd == '\'') {
      char qc = *cmd; cmd += 1;
      while (*cmd && *cmd != qc) {
        if (*cmd == '\\' && cmd[1]) {
          cmd += 1;
          switch (*cmd) {
            case 't': curarg.AppendChar('\t'); break;
            case 'r': curarg.AppendChar('\r'); break;
            case 'n': curarg.AppendChar('\n'); break;
            default: curarg.AppendChar(SafeChar(*cmd)); break;
          }
        } else {
          curarg.AppendChar(SafeChar(*cmd));
        }
        cmd += 1;
      }
      if (*cmd == qc) cmd += 1;
    } else {
      while (*cmd && !IsSpace(*cmd)) {
        curarg.AppendChar(SafeChar(*cmd));
        cmd += 1;
      }
    }
    while (*cmd && IsSpace(*cmd)) cmd += 1;
    dest.push_back(curarg);
  }
}


//==========================================================================
//
//  ConUnParse
//
//==========================================================================
festring ConUnParse (const std::vector<festring> &dest) {
  festring res;
  for (size_t f = 0; f != dest.size(); f += 1) {
    festring ss = dest[f];
    if (!res.IsEmpty()) res.AppendChar(' ');
    if (!ss.IsEmpty()) {
      bool needQ = false;
      for (festring::sizetype pos = 0; pos != ss.GetSize() && !needQ; pos += 1) {
        char ch = ss[pos];
        if (ch == '"' || ch == '\'' || ch == '\\' || ch == 0 || IsSpace(ch)) {
          needQ = true;
        }
      }
      if (needQ) res.AppendChar('"');
      for (festring::sizetype pos = 0; pos != ss.GetSize(); pos += 1) {
        char ch = ss[pos];
        if (ch == '"' || ch == '\'' || ch == '\\') {
          res.AppendChar('\\');
          res.AppendChar(ch);
        } else if (ch == '\t') {
          res << "\\t";
        } else if (ch == '\r') {
          res << "\\r";
        } else if (ch == '\n') {
          res << "\\n";
        } else if ((ch >= 0 && ch <= 32) || ch == 127) {
          res.AppendChar(' ');
        } else {
          res.AppendChar(ch);
        }
      }
      if (needQ) res.AppendChar('"');
    } else {
      res << "\"\"";
    }
  }
  return res;
}


//==========================================================================
//
//  ConExecute
//
//==========================================================================
void ConExecute (const char *cmd) {
  std::vector<festring> args;
  ConParse(args, cmd);

  if (args.size() == 0) return;

  CBTree::Value *val = CmdList().Find(args[0].CStr(), -1);
  if (val && val->udata) {
    ConCmd *cmd = (ConCmd *)val->udata;
    if (cmd->Enabled(true)) {
      cmd->Do(args);
    }
  } else {
    ConLogf("%s?", args[0].CStr());
  }
}


static int acCount = 0;
static int acUsed = 0;
static const CBTree::Value **acList = 0;


//==========================================================================
//
//  GetACName
//
//==========================================================================
static inline const char *GetACName (int idx) {
  if (idx >= 0 && idx < acCount) {
    return (const char *)(acList[idx]->realkey);
  } else {
    return "";
  }
}


//==========================================================================
//
//  AppendAC
//
//==========================================================================
static inline void AppendAC (const CBTree::Value *val) {
  if (acCount == acUsed) {
    // fuck you, shitsix!
    if (acList) {
      acList = (const CBTree::Value **)::realloc(acList, (acUsed + 8192) * sizeof(void *));
    } else {
      acList = (const CBTree::Value **)::malloc((acUsed + 8192) * sizeof(void *));
    }
    if (!acList) ABORT("out of memory for autocompletion list!");
    acUsed += 8192;
  }
  acList[acCount] = val;
  acCount += 1;
}


//==========================================================================
//
//  AutoCompCounter
//
//==========================================================================
static int AutoCompCounter (const CBTree::Value *val, void *uarg) {
  (void)uarg;
  #if 0
  fprintf(stderr, ":: <%s> : <%s>\n", (const char *)val->key,
          (const char *)val->realkey);
  #endif
  ConCmd *cmd = (ConCmd *)val->udata;
  if (cmd->Enabled(false)) {
    AppendAC(val);
  }
  return 0;
}


//==========================================================================
//
//  RemoveArg
//
//==========================================================================
static void RemoveArg (int len, void (*putcharCB) (char ch, void *udata), void *udata) {
  while (len > 0) {
    len -= 1;
    putcharCB('\x08', udata);
  }
}


//==========================================================================
//
//  PutArg
//
//  return `true` if quoted.
//  will not put the final quote.
//
//==========================================================================
static bool PutArg (const char *str, void (*putcharCB) (char ch, void *udata), void *udata) {
  if (!str || !str[0]) return false;

  bool needQ = false;
  for (const char *tmp = str; *tmp && !needQ; tmp += 1) {
    char ch = *tmp;
    if (ch == '"' || ch == '\'' || ch == '\\' || ch == 0 || IsSpace(ch)) {
      needQ = true;
    }
  }

  if (needQ) putcharCB('"', udata);
  while (*str) {
    char ch = *str; str += 1;
    if (ch == '"' || ch == '\'' || ch == '\\') {
      putcharCB('\\', udata);
      putcharCB(ch, udata);
    } else if (ch == '\t') {
      putcharCB('\\', udata);
      putcharCB('t', udata);
    } else if (ch == '\r') {
      putcharCB('\\', udata);
      putcharCB('r', udata);
    } else if (ch == '\n') {
      putcharCB('\\', udata);
      putcharCB('n', udata);
    } else if ((ch >= 0 && ch <= 32) || ch == 127) {
      putcharCB(' ', udata);
    } else {
      putcharCB(ch, udata);
    }
  }

  return needQ;
}


//==========================================================================
//
//  ConAutocomplete
//
//==========================================================================
void ConAutocomplete (const char *cmd,
                      void (*suggestionCB) (const char *sugstr, void *udata),
                      void (*putcharCB) (char ch, void *udata),
                      void *udata)
{
  if (!cmd) cmd = "";

  size_t slen = strlen(cmd);
  //if (slen == 0) return;
  const bool lastIsSpace = (slen ? IsSpace(cmd[slen - 1]) : false);

  int argLen = 0;
  if (!lastIsSpace) {
    while (slen != 0 && !IsSpace(cmd[slen - 1])) { slen -= 1; argLen += 1; }
    if (cmd[slen] == '"' || cmd[slen] == '\'') return;
    //if (!cmd[slen]) return;
  }

  std::vector<festring> args;
  ConParse(args, cmd);

  if (args.size() == 0) {
    acCount = 0;
    //cmdlist.IterateAll(&AutoCompCounter, NULL);
    CmdList().PrefixIterate("", 0, &AutoCompCounter, NULL);
    if (acCount == 0) return;
    suggestionCB(NULL, udata);
    char lastChar = -1;
    for (int f = 0; f != acCount; f += 1) {
      const char *n = GetACName(f);
      #if 0
      fprintf(stderr, "<%s>\n", n);
      #endif
      char cc = n[0];
      if (cc >= 'a' && cc <= 'z') cc -= 32;
      if (lastChar != cc) {
        char tmp[32];
        snprintf(tmp, sizeof(tmp), "%c...", cc);
        suggestionCB(tmp, udata);
        lastChar = cc;
      }
    }
    return;
  }

  const bool emptyLast = (args.size() != 0 && lastIsSpace);
  if (emptyLast) args.push_back(festring());

  #if 0
  fprintf(stderr, "=================================\n");
  #endif

  if (args.size() > 1) {
    CBTree::Value *val = CmdList().Find(args[0].CStr(), -1);
    if (!val || !val->udata) return;
    ConCmd *cmd = (ConCmd *)val->udata;
    if (!cmd->Enabled(true)) return;
    const int rr = cmd->AutocompleteLastArg(args);
    if (!rr) return;
    if (!lastIsSpace) RemoveArg(argLen, putcharCB, udata);
    const bool quoted = PutArg(args[args.size() - 1].CStr(), putcharCB, udata);
    if (rr > 0) {
      // full
      if (quoted) putcharCB('"', udata);
      putcharCB(' ', udata);
    }
    return;
  }

  // command autocompletion
  acCount = 0;
  CmdList().PrefixIterate(cmd + slen, -1, &AutoCompCounter, NULL);
  if (acCount == 0) return;

  RemoveArg(argLen, putcharCB, udata);

  if (acCount == 1) {
    const bool quoted = PutArg(GetACName(0), putcharCB, udata);
    if (quoted) putcharCB('"', udata);
    putcharCB(' ', udata);
  } else {
    const char *bestPfx = GetACName(0);
    int bestLen = (int)strlen(bestPfx);
    #if 0
    fprintf(stderr, "%d: <%s>\n", bestLen, bestPfx);
    #endif
    for (int f = 1; f != acCount; f += 1) {
      const char *n = GetACName(f);
      int nlen = 0;
      while (nlen != bestLen && bestPfx[nlen] && n[nlen] && CharEquCI(bestPfx[nlen], n[nlen])) {
        nlen += 1;
      }
      if (nlen < bestLen) {
        bestLen = nlen;
        bestPfx = n;
      }
      #if 0
      fprintf(stderr, "n=%d; bl=%d; n=<%s>; bps=<%s>\n", nlen, bestLen, n, bestPfx);
      #endif
    }

    // put prefix
    for (int f = 0; f != bestLen; f += 1) {
      putcharCB(bestPfx[f], udata);
    }

    // print suggestions
    #if 0
    fprintf(stderr, "------------------------------------\n");
    #endif
    acCount = 0;
    CmdList().PrefixIterate(bestPfx, bestLen, &AutoCompCounter, NULL);
    for (int f = 0; f != acCount; f += 1) {
      #if 0
      fprintf(stderr, "!!! %d: <%s>\n", f, GetACName(f));
      #endif
      if (f == 0) {
        suggestionCB(NULL, udata);
      }
      suggestionCB(GetACName(f), udata);
    }
  }
}


//==========================================================================
//
//  ConParseInt
//
//==========================================================================
bool ConParseInt (const festring &str, int *val) {
  festring::sizetype pos = 0;
  while (pos != str.GetSize() && IsSpace(str[pos])) pos += 1;
  if (pos == str.GetSize()) return false;

  festring ss = str;
  ss.TrimAll();
  if (ss.IsEmpty()) return false; // just in case

  char *eptr;
  long int v = strtol(ss.CStr(), &eptr, 0);
  if (*eptr == 0 && v >= INT_MIN && v <= INT_MAX) {
    if (val) *val = (int)v;
    return true;
  } else {
    return false;
  }
}


//==========================================================================
//
//  ConParseBool
//
//==========================================================================
bool ConParseBool (const festring &str, bool *val) {
  festring ss = str;
  ss.TrimAll();
  if (ss.IsEmpty()) return false; // just in case

  if (ss.EquCI("on") || ss.EquCI("yes") || ss.EquCI("tan") || ss.EquCI("true") || ss == "1") {
    if (val) *val = true;
    return true;
  }

  if (ss.EquCI("off") || ss.EquCI("no") || ss.EquCI("ona") || ss.EquCI("false") || ss == "0") {
    if (val) *val = false;
    return true;
  }

  return false;
}


//==========================================================================
//
//  ConCalculateAC
//
//==========================================================================
__attribute__((sentinel)) int ConCalculateAC (festring &str, ...) {
  va_list ap;
  std::vector<festring> cclist;

  va_start(ap, str);
  for (;;) {
    const char *cmd = va_arg(ap, const char *);
    if (!cmd || !cmd[0]) break;
    festring cc; cc << cmd;
    if (str.IsEmpty() || cc.StartsWithCI(str)) {
      cclist.push_back(cc);
    }
  }
  va_end(ap);

  if (cclist.size() == 0) return 0;
  if (cclist.size() == 1) { str = cclist[0]; return 1; }

  if (!str.IsEmpty()) {
    festring res = cclist[0];
    festring::sizetype bestLen = res.GetSize();
    for (size_t f = 1; f != cclist.size(); f += 1) {
      festring n = cclist[f];
      festring::sizetype nlen = 0;
      while (nlen != bestLen && nlen != n.GetSize() &&
             festring::CharUpper(res[nlen]) == festring::CharUpper(n[nlen]))
      {
        nlen += 1;
      }
      if (nlen < bestLen) {
        bestLen = nlen;
        res = n;
      }
    }

    res.Left((int)bestLen);

    str = res;
    cclist.clear();

    va_start(ap, str);
    for (;;) {
      const char *cmd = va_arg(ap, const char *);
      if (!cmd || !cmd[0]) break;
      festring cc; cc << cmd;
      if (cc.StartsWithCI(str)) {
        cclist.push_back(cc);
      }
    }
    va_end(ap);

    if (cclist.size() < 2) return 1; // should not happen
  }

  std::sort(cclist.begin(), cclist.end(),
    // `<`
    [&] (const festring &a, const festring &b) {
      return (a.CompareCI(b) < 0);
    });

  ConLogf("========");
  for (size_t f = 0; f != cclist.size(); f += 1) {
    ConLogf("  %s", cclist[f].CStr());
  }

  return -1;
}


//==========================================================================
//
//  ConCalculateACVector
//
//==========================================================================
int ConCalculateACVector (festring &str, const std::vector<festring> &list) {
  std::vector<festring> cclist;

  for (size_t lidx = 0; lidx != list.size(); lidx += 1) {
    if (str.IsEmpty() || list[lidx].StartsWithCI(str)) {
      cclist.push_back(list[lidx]);
    }
  }

  if (cclist.size() == 0) return 0;
  if (cclist.size() == 1) { str = cclist[0]; return 1; }

  if (!str.IsEmpty()) {
    festring res = cclist[0];
    festring::sizetype bestLen = res.GetSize();
    for (size_t f = 1; f != cclist.size(); f += 1) {
      festring n = cclist[f];
      festring::sizetype nlen = 0;
      while (nlen != bestLen && nlen != n.GetSize() &&
             festring::CharUpper(res[nlen]) == festring::CharUpper(n[nlen]))
      {
        nlen += 1;
      }
      if (nlen < bestLen) {
        bestLen = nlen;
        res = n;
      }
    }

    res.Left((int)bestLen);

    str = res;
    cclist.clear();

    for (size_t lidx = 0; lidx != list.size(); lidx += 1) {
      if (list[lidx].StartsWithCI(str)) {
        cclist.push_back(list[lidx]);
      }
    }

    if (cclist.size() < 2) return 1; // should not happen
  }

  std::sort(cclist.begin(), cclist.end(),
    // `<`
    [&] (const festring &a, const festring &b) {
      return (a.CompareCI(b) < 0);
    });

  ConLogf("========");
  for (size_t f = 0; f != cclist.size(); f += 1) {
    ConLogf("  %s", cclist[f].CStr());
  }

  return -1;
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmd
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  ConCmd::ConCmd
//
//==========================================================================
ConCmd::ConCmd (const char *name, bool unique)
  : CTData()
  , mName()
{
  IvanAssert(name && name[0]);
  mName << name;
  const int res = CmdList().Insert(name, -1, this);
  if (res < 0) ABORT("error adding console command `%s`", name);
  if (res != 0) {
    if (unique) ABORT("duplicate console command `%s`", name);
    // replace
    CBTree::Value *val = CmdList().Find(name, -1);
    IvanAssert(val);
    if (val->udata != this) {
      delete val->udata;
      val->udata = this;
    }
  }
  #if 0
  fprintf(stderr, "REGISTERED: '%s'\n", name);
  #endif
}


//==========================================================================
//
//  ConCmd::Enabled
//
//==========================================================================
bool ConCmd::Enabled (bool verbose) {
  return true;
}


//==========================================================================
//
//  ConCmd::AutocompleteLastArg
//
//==========================================================================
int ConCmd::AutocompleteLastArg (std::vector<festring> &args) {
  return 0;
}


// ////////////////////////////////////////////////////////////////////////// //
// ConIVar
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  ConIVar::ConIVar
//
//==========================================================================
ConIVar::ConIVar (const char *name, int *vptr)
  : ConCmd(name)
  , varptr(vptr)
{
  IvanAssert(vptr);
}


//==========================================================================
//
//  ConIVar::Do
//
//==========================================================================
void ConIVar::Do (const std::vector<festring> &args) {
  if (args.size() == 1) {
    ConLogf("%s: %d", args[0].CStr(), *varptr);
  } else if (args.size() == 2) {
    int v = 0;
    if (ConParseInt(args[1], &v)) {
      *varptr = (int)v;
      ConLogf("%s: %d", args[0].CStr(), *varptr);
    } else {
      ConLogf("ERROR: invalid value `%s` for variable `%s` (integer expected)",
              args[1].CStr(), args[0].CStr());
    }
  } else {
    ConLogf("ERROR: variable `%s` requires one integer value", args[0].CStr());
  }
}
