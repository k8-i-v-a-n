/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef FELIB_CCONSOLE_HEADER
#define FELIB_CCONSOLE_HEADER

#include <stddef.h>
#include <stdint.h>
#include "felibdef.h"
#include "festring.h"

#include <vector>

/*
 * this code implements djb "critbit trie". it can be used instead of a
 * hashtable, to store data keyed by arbitrary strings. the advantage of
 * crittree is the absence of rehashing (so the code doesn't need to
 * reallocate internal tables).
 *
 * this code is inspired by Dan Bernstein's "qhasm" and implements a binary
 * crit-bit (also known as PATRICIA) tree for byte keys.
 *
 * internal nodes in a crit-bit store a position in the input and two children.
 * the position is the next location in which two members differ (the critical
 * bit). for a given set of elements there is a unique crit-bit tree representing
 * that set, thus a crit-bit tree does not need complex balancing algorithms.
 * the depth of a crit-bit tree is bounded by the length of the longest element,
 * rather than the number of elements (as with an unbalanced tree).
 *
 * crit-bit trees also support the usual tree operations quickly:
 * membership-testing, insertion, removal, predecessor, successor and easy iteration.
 * for strings they are especially helpful since they don't require an expensive
 * string comparison at each step.
 *
 * if it is not clear: crit-bit tree is a binary trie, optimised to branch on
 * different bits (and it sees the key as a bitstring).
 */

/*
 * tree node keeps user-supplied data after node data.
 * user data structure should provide the pointer, and
 * the length of the string key.
 * WARNING! neither key, nor udata CONTENTS will be copied!
 *          i.e. you should take care of keeping them alive,
 *          and not moved until the tree itself is alive.
 */



// this is case-insensitive
struct CBTree {
private:
  // if bit 1 is set, this is an internal node, otherwise it is a value node
  void *root;
  void *free_list;
  uint32_t node_count; // total number of nodes (both internal and external)
  uint32_t item_count; // number of items in the tree (coincidentally, the number of external nodes)

public:
  // cb-tree user data.
  class CTData {
  public:
    CTData () {}
    virtual ~CTData () {}
  };

  // user data (it is stored in the tree exactly in this format)
  // WARNING! NEVER change the key, or everything will break!
  struct Value {
    // pointers put first for better align
    const void *key; // key; crittree code will never change this; it is lowercased
    const void *realkey; // key as supplied by the caller
    CTData *udata; // user data (you are free to change this one)
    const uint32_t klen; // key length in bytes
  };

public:
  inline CBTree () { root = 0; free_list = 0; node_count = 0; item_count = 0; }
  inline ~CBTree () { Clear(); }

  // no copies!
  CBTree (const CBTree &) = delete;
  CBTree &operator = (const CBTree &) = delete;

  // deallocate all unused free nodes
  void Compress ();

  // deallocate all tree memory (thus clearing it)
  void Clear ();

  // returns non-NULL if the key `key` is in the tree
  // if `keylen` is negative, will use `strlen()`
  // UB if `key` is longer than 2^31-1
  Value *Find (const void *key, int keylen);

  // insert key into the tree
  // returns:
  //  <0 on error (oom, etc.)
  //  >0 if already in the tree
  //   0 if succesfully inserted
  // if `keylen` is negative, will use `strlen()`
  // UB if `key` is longer than 2^31-1
  // `udata` will be copied (bitblitted) to the tree
  // key data will be owned
  int Insert (const void *key, int keylen, CTData *udata);

  // delete the key from the tree
  // returns `true` if the string was succesfully found and deleted
  // if `keylen` is negative, will use `strlen()`
  // UB if `key` is longer than 2^31-1
  // note that deleting tree node will not free any pool memory
  bool Delete (const void *key, int keylen);

  // if iterator returns non-zero, `cbtree_prefixed()` will return that value

  // return all strings starting with the given prefix
  // returns:
  //   `0` if callback returned `0`
  //   other values on error (or non-zero iterator value)
  // callback should return:
  //   `0` to continue iteration
  //   other value: the iteration will stop, the function will return this value
  // if `pfxlen` is negative, will use `strlen()`
  // UB if `prefix` is longer than 2^31-1
  int PrefixIterate (const void *prefix, int pfxlen,
                     int (*ncb) (const Value *udata, void *uarg),
                     void *uarg);

  int IterateAll (int (*ncb) (const Value *udata, void *uarg), void *uarg);

private:
  //union cbtree_node;
  union cbtree_node *alloc_node ();
  union cbtree_node *alloc_value_node (const struct KBuf &key, CTData *udata);
  void release_node (union cbtree_node *node);
  void clear_traverse (void *top);
};


// ////////////////////////////////////////////////////////////////////////// //
// console buffer

extern uint32_t ConUpdateCount;

// call this on startup to initialise the buffer
void ConInit (int wdt);

void ConDisableOutput ();

// enable stderr output (or file output for shitdoze)
void ConEnableStdErr ();

int ConMaxLines ();

// automatic newline if the string is not empty
LIKE_PRINTF(1, 2) void ConLogf (const char *fmt, ...);

// no automatic newline
LIKE_PRINTF(1, 2) void ConPrintf (const char *fmt, ...);

// can be used to render console text.
// return `false` if no more lines.
// `text` is ASCIIZ.
// `0` is the last line, `1` is the previous line, etc.
bool ConGetLine (uint32_t lineidx, const char *&text, int &textlen);


const char *ComatozeU32 (uint32_t n);
const char *ComatozeI32 (int32_t n);


// ////////////////////////////////////////////////////////////////////////// //
// console commands

// will not clear `dest`!
void ConParse (std::vector<festring> &dest, const char *cmd);
festring ConUnParse (const std::vector<festring> &dest);

// will not change `*val` on error. `*val` can be `NULL`.
// return `true` on success, `false` on error.
bool ConParseInt (const festring &str, int *val);
bool ConParseBool (const festring &str, bool *val);

void ConExecute (const char *cmd);

void ConAutocomplete (const char *cmd,
                      void (*suggestionCB) (const char *sugstr, void *udata),
                      void (*putcharCB) (char ch, void *udata),
                      void *udata);


// useful for arguments autocompletion.
// use like this:
//
//  int CmdXX:AutocompleteLastArg (std::vector<festring> &args) {
//    if (args.size() == 2) {
//      return CalculateAC(args[1], "on", "off", NULL);
//    }
//    return 0;
//  }
//
int ConCalculateAC (festring &str, ...) __attribute__((sentinel));
int ConCalculateACVector (festring &str, const std::vector<festring> &list);


class ConCmd : public CBTree::CTData {
private:
  festring mName;

public:
  ConCmd () = delete;
  ConCmd (const char *name, bool unique=true);
  virtual ~ConCmd () {}

  const char *GetName () const { return mName.CStr(); }

  virtual bool Enabled (bool verbose);

  // [0] is command itself
  virtual void Do (const std::vector<festring> &args) = 0;
  // change last arg.
  // return 0 if no autocompletion, -1 if partial, 1 if full
  virtual int AutocompleteLastArg (std::vector<festring> &args);
};


class ConIVar : public ConCmd {
private:
  int *varptr;

public:
  ConIVar () = delete;
  ConIVar (const char *name, int *vptr);

  virtual void Do (const std::vector<festring> &args) override;
};

#endif
