/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "config.h"
#include "fesave.h"
#include "feparse.h"
#include "graphics.h"
#include "bitmap.h"
#include "rawbit.h"
#include "felist.h"
#include "feio.h"


configoption *configsystem::Option[MAX_CONFIG_OPTIONS];
festring configsystem::OptionHelp[MAX_CONFIG_OPTIONS];
festring configsystem::ConfigFileName;
int configsystem::Options;


//==========================================================================
//
//  configsystem::AddOption
//
//==========================================================================
void configsystem::AddOption (configoption *O, cfestring &help) {
  OptionHelp[Options] = help;
  Option[Options++] = O;
}


//==========================================================================
//
//  configsystem::NormalStringChanger
//
//==========================================================================
void configsystem::NormalStringChanger (stringoption *O, cfestring &What) {
  O->Value = What;
}


//==========================================================================
//
//  configsystem::NormalNumberChanger
//
//==========================================================================
void configsystem::NormalNumberChanger (numberoption *O, sLong What) {
  O->Value = What;
}


//==========================================================================
//
//  configsystem::NormalTruthChanger
//
//==========================================================================
void configsystem::NormalTruthChanger (truthoption *O, truth What) {
  O->Value = What;
}


//==========================================================================
//
//  configoption::configoption
//
//==========================================================================
configoption::configoption (cchar *aName, cchar *aDescription)
  : Name(aName)
  , Description(aDescription)
{
}


//==========================================================================
//
//  configoption::Hidden
//
//==========================================================================
truth configoption::Hidden () const {
  return (!Description || !Description[0] || Description[0] == '!');
}


//==========================================================================
//
//  stringoption::stringoption
//
//==========================================================================
stringoption::stringoption (cchar *Name, cchar *Desc, cfestring &Value,
                            void (*ValueDisplayer)(const stringoption *, festring &),
                            truth (*ChangeInterface)(stringoption *),
                            void (*ValueChanger)(stringoption *, cfestring &))
  : configoption(Name, Desc)
  , Value(Value)
  , ValueDisplayer(ValueDisplayer)
  , ChangeInterface(ChangeInterface)
  , ValueChanger(ValueChanger)
{
}


//==========================================================================
//
//  stringoption::Hidden
//
//==========================================================================
truth stringoption::Hidden () const {
  return (!ValueDisplayer || !Description || !Description[0] || Description[0] == '!');
}


//==========================================================================
//
//  numberoption::numberoption
//
//==========================================================================
numberoption::numberoption (cchar *Name, cchar *Desc, sLong Value,
                            void (*ValueDisplayer)(const numberoption *, festring &),
                            truth (*ChangeInterface)(numberoption *),
                            void (*ValueChanger)(numberoption*, sLong))
  : configoption(Name, Desc)
  , Value(Value)
  , ValueDisplayer(ValueDisplayer)
  , ChangeInterface(ChangeInterface)
  , ValueChanger(ValueChanger)
{
}


//==========================================================================
//
//  numberoption::Hidden
//
//==========================================================================
truth numberoption::Hidden () const {
  return (!ValueDisplayer || !Description || !Description[0] || Description[0] == '!');
}


//==========================================================================
//
//  scrollbaroption::scrollbaroption
//
//==========================================================================
scrollbaroption::scrollbaroption (cchar *Name, cchar *Desc, sLong Value,
                                  void (*ValueDisplayer)(const numberoption *, festring &),
                                  truth (*ChangeInterface)(numberoption *),
                                  void (*ValueChanger)(numberoption *, sLong),
                                  void (*BarHandler)(sLong))
  : numberoption(Name, Desc, Value, ValueDisplayer, ChangeInterface, ValueChanger)
  , BarHandler(BarHandler)
{
}


//==========================================================================
//
//  truthoption::truthoption
//
//==========================================================================
truthoption::truthoption (cchar *Name, cchar *Desc, truth Value,
                          void (*ValueDisplayer)(const truthoption *, festring &),
                          truth (*ChangeInterface)(truthoption *),
                          void (*ValueChanger)(truthoption *, truth))
  : configoption(Name, Desc)
  , Value(Value)
  , ValueDisplayer(ValueDisplayer)
  , ChangeInterface(ChangeInterface)
  , ValueChanger(ValueChanger)
{
}


//==========================================================================
//
//  truthoption::Hidden
//
//==========================================================================
truth truthoption::Hidden () const {
  return (!ValueDisplayer || !Description || !Description[0] || Description[0] == '!');
}


// ////////////////////////////////////////////////////////////////////////// //
// configsystem
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  configsystem::Save
//
//==========================================================================
truth configsystem::Save () {
  std::ofstream SaveFile(ConfigFileName.CStr(), std::ios::out);
  if (!SaveFile.is_open()) return false;
  for (int c = 0; c < Options; ++c) {
    SaveFile << Option[c]->Name << " = ";
    Option[c]->SaveValue(SaveFile);
    SaveFile << ";\n";
  }
  return true;
}


//==========================================================================
//
//  configsystem::Load
//
//==========================================================================
truth configsystem::Load () {
  TextInputFile SaveFile(ConfigFileName, 0, false);
  if (!SaveFile.IsOpen()) return false;
  festring Word;
  for (SaveFile.ReadWord(Word, false); !SaveFile.Eof(); SaveFile.ReadWord(Word, false)) {
    /* Inefficient, but speed is probably not an issue here */
    for (int c = 0; c < Options; ++c) {
      if (Word == Option[c]->Name) Option[c]->LoadValue(SaveFile);
    }
  }
  return true;
}


//==========================================================================
//
//  configsystem::Show
//
//==========================================================================
void configsystem::Show (void (*BackGroundDrawer)(), void (*ListAttributeInitializer)(felist&),
                         truth SlaveScreen)
{
  int Chosen;
  truth TruthChange = false;
  felist List(CONST_S("Which setting do you wish to configure?"));

  // this sets list width
  if (SlaveScreen && ListAttributeInitializer) {
    ListAttributeInitializer(List);
  }

  // calculate maximum text width
  int maxWdt = 0;
  for (int c = 0; c < Options; ++c) {
    if (!Option[c]->Hidden()) {
      festring Entry = CONST_S(Option[c]->Description);
      Entry.Capitalize();
      const int wdt = FONT->TextWidth(Entry);
      if (maxWdt < wdt) maxWdt = wdt;
    }
  }
  maxWdt += 8;

  #if 0
  if (!SlaveScreen || !ListAttributeInitializer) {
    List.SetWidth(maxWdt + 128);
  }
  #endif

  const col16 c2[2] = { LIGHT_GRAY, MakeRGB16(192 + 16, 192 + 16, 192 + 16) };
  configoption *VisOptions[MAX_CONFIG_OPTIONS];
  int VisOptionsCount;
  for (;;) {
    if (SlaveScreen) BackGroundDrawer();
    List.Empty();
    List.SetIntraLine(4);
    VisOptionsCount = 0;
    for (int c = 0; c != Options; ++c) {
      if (!Option[c]->Hidden()) {
        VisOptions[VisOptionsCount] = Option[c];
        VisOptionsCount += 1;
        festring Entry = CONST_S(Option[c]->Description);
        Entry.Capitalize();
        //Entry.Resize(60);
        #if 0
        FONT->RPadToPixWidth(Entry, nameWdt);
        #else
        FONT->RPadToPixWidth(Entry, maxWdt);
        #endif
        Option[c]->DisplayeValue(Entry);
        List.AddEntry(Entry, c2[1 - (VisOptionsCount & 1)]);
        List.AddLastEntryHelp(OptionHelp[c]);
      }
    }
    List.SetFlags(SELECTABLE|(SlaveScreen ? DRAW_BACKGROUND_AFTERWARDS : 0) |
                  (!SlaveScreen && !TruthChange ? FADE : 0));
    if (VisOptionsCount > 26 + 10) {
      // too many options, split
      List.SetPageLength(20);
    }
    Chosen = List.Draw();
    festring String;
    if (Chosen >= 0 && Chosen < VisOptionsCount) {
      TruthChange = VisOptions[Chosen]->ActivateChangeInterface();
      DOUBLE_BUFFER->ClearToColor(0);
    } else {
      Save();
      return;
    }
  }
}


//==========================================================================
//
//  configsystem::NormalStringDisplayer
//
//==========================================================================
void configsystem::NormalStringDisplayer (const stringoption *O, festring &Entry) {
  if (!O->Value.IsEmpty()) Entry << O->Value; else Entry << "\1R-\2";
}


//==========================================================================
//
//  configsystem::NormalNumberDisplayer
//
//==========================================================================
void configsystem::NormalNumberDisplayer (const numberoption *O, festring &Entry) {
  Entry << O->Value;
}


//==========================================================================
//
//  configsystem::NormalTruthDisplayer
//
//==========================================================================
void configsystem::NormalTruthDisplayer (const truthoption *O, festring &Entry) {
  Entry << (O->Value ? "yes" : "no");
}


//==========================================================================
//
//  configsystem::NormalTruthChangeInterface
//
//==========================================================================
truth configsystem::NormalTruthChangeInterface (truthoption *O) {
  O->ChangeValue(!O->Value);
  return true;
}


//==========================================================================
//
//  configsystem::NormalStringChangeInterface
//
//==========================================================================
truth configsystem::NormalStringChangeInterface (stringoption *O) {
  festring String;
  if (iosystem::StringQuestion(String, CONST_S("Set new ")+O->Description+':', v2(30, 30), WHITE, 0, 80, true, true) == NORMAL_EXIT) {
    O->ChangeValue(String);
  }
  return false;
}


//==========================================================================
//
//  configsystem::NormalNumberChangeInterface
//
//==========================================================================
truth configsystem::NormalNumberChangeInterface (numberoption *O) {
  truth escaped;
  sLong val = iosystem::NumberQuestion(CONST_S("Set new ")+O->Description+':', v2(30, 30),
                                       WHITE, iosystem::FADE_IN, &escaped);
  if (!escaped) O->ChangeValue(val);
  return false;
}


//==========================================================================
//
//  stringoption::SaveValue
//
//==========================================================================
void stringoption::SaveValue (std::ofstream &SaveFile) const {
  SaveFile << '\"' << Value.CStr() << '\"';
}


//==========================================================================
//
//  stringoption::LoadValue
//
//==========================================================================
void stringoption::LoadValue (TextInput &SaveFile) {
  SaveFile.ReadWord();
  SaveFile.ReadWord(Value);
}


/* ??? */
void numberoption::SaveValue (std::ofstream &SaveFile) const { SaveFile << Value; }
void numberoption::LoadValue (TextInput &SaveFile) { Value = SaveFile.ReadNumber(); }
void truthoption::SaveValue (std::ofstream &SaveFile) const { SaveFile << Value; }
void truthoption::LoadValue (TextInput &SaveFile) { Value = SaveFile.ReadNumber(); }
