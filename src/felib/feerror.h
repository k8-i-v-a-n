/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_ERROR_H__
#define __FELIB_ERROR_H__

#include "felibdef.h"
#include "cconsole.h"

#define ABORT  globalerrorhandler::Abort


#define IvanAssert(cond_)  do { if (__builtin_expect(!(cond_), 0)) globalerrorhandler::AssertAbort(__FILE__, __PRETTY_FUNCTION__, __LINE__, "%s", #cond_); } while (0)


class globalerrorhandler {
public:
  static void Install ();
  static void DeInstall ();
  static void NO_RETURN LIKE_PRINTF(4, 5) AssertAbort (cchar *fname, cchar *func, int line, cchar *fmt, ...);
  static void NO_RETURN LIKE_PRINTF(1, 2) Abort (cchar *, ...);
  static cchar *GetBugMsg () { return BugMsg; }

  static void activateGDBMode ();
  static bool isGDB ();

  static void activateGDBReadMode ();
  static bool isGDBRead ();

  static void segfault ();

private:
  static void NewHandler ();
  static void (*OldNewHandler) ();

private:
  static cchar *BugMsg;
  static bool errorGDB;
  static bool errorGDBRead;
};


#endif
