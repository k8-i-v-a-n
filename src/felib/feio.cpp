/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include <ctime>
#include <cctype>

#include <dirent.h>
#include <stddef.h>
#include <cstdio>
#include <sys/types.h>
#include <algorithm>

#include "graphics.h"
#include "feio.h"
#include "fesave.h"
#include "whandler.h"
#include "felist.h"
#include "rawbit.h"
#include "festring.h"
#include "bitmap.h"


#define PENT_WIDTH  (70)


/*
static col16 MkDarkerColor (col16 col) {
  auto r = GetRed16(col);
  auto g = GetGreen16(col);
  auto b = GetBlue16(col);
  if (r > 64) r -= 64; else r = 0;
  if (g > 64) g -= 64; else g = 0;
  if (b > 64) b -= 64; else b = 0;
  return MakeRGB16(r, g, b);
}
*/


//==========================================================================
//
//  iosystem::TextScreen
//
//  Prints screen full of Text in color Color. If GKey is true function
//  waits for keypress. BitmapEditor is a pointer to function that is
//  called during every fade tick.
//
//==========================================================================
void iosystem::TextScreen (cfestring &Text, v2 Disp, col16 Color, truth GKey,
                           truth Fade, bitmapeditor BitmapEditor)
{
  //col16 col2 = MkDarkerColor(Color);
  auto cursave = graphics::GetCursorState();
  graphics::HideCursor();
  bitmap Buffer(RES, 0);
  Buffer.ActivateFastFlag();

  int LineNumber = 0;
  for (festring::sizetype c = 0; c < Text.GetSize(); ++c) {
    if (Text[c] == '\n') ++LineNumber;
  }
  LineNumber >>= 1;

  int Lines = 0;
  festring txt = Text;
  while (txt.GetSize() > 0) {
    festring::sizetype c = 0;
    while (c < txt.GetSize() && txt[c] != '\n') ++c;
    festring curline = txt;
    curline.Erase(c, curline.GetSize()-c);
    v2 PrintPos((RES.X - FONT->TextWidth(curline)) / 2 + Disp.X,
                (RES.Y * 2) / 5 - (LineNumber - Lines) * 15 + Disp.Y);
    FONT->PrintStr(&Buffer, PrintPos, Color, curline);
    if (c < txt.GetSize()) { Lines += 1; c += 1; }
    txt.Erase(0, c);
  }

  if (Fade) {
    Buffer.FadeToScreen(BitmapEditor);
  } else {
    BitmapEditor(&Buffer, true);
    Buffer.FastBlit(DOUBLE_BUFFER);
    graphics::BlitDBToScreen();
  }

  if (GKey) {
    if (BitmapEditor) {
      while (!READ_KEY()) BitmapEditor(DOUBLE_BUFFER, false);
    } else {
      GET_KEY();
    }
  }
}


//==========================================================================
//
//  CountChars
//
//  Returns amount of chars cSF in string sSH
//
//==========================================================================
static int CountChars (char cSF, cfestring &sSH) {
  int iReturnCounter = 0;
  for (festring::sizetype i = 0; i < sSH.GetSize(); ++i) {
    if (sSH[i] == '\1') {
      if (cSF == '\1') ++iReturnCounter;
      ++i;
      continue;
    }
    if (sSH[i] == cSF) ++iReturnCounter;
  }
  return iReturnCounter;
}


//==========================================================================
//
//  iosystem::Menu
//
//  Draws a menu on bitmap BackGround to position Pos. festring Topic
//  is the text that is shown before the choices '\r' is a line-ending
//  character. Topic must end with a '\r'. sMS is a list of choices
//  separated by '\r'. sMS must end with '\r'.
//
//  Color is the col of font of sMS, SmallText1 and SmallText2. SmallText1
//  is printed to the lower-left corner and SmallText2 is printed to the
//  lower-right. They both can have line-ending characters ('\r') and must
//  also always end with one.
//
//  Warning: This function is utter garbage that just happens to work.
//  If you need to use this function use the comments. Don't try to
//  understand it. It is impossible.
//
//==========================================================================
int iosystem::Menu (cbitmap *BackGround, v2 Pos, cfestring &Topic, cfestring &sMS, col16 Color,
                    cfestring &SmallText1, cfestring &SmallText2, truth allowEsc)
{
  if (CountChars('\r', sMS) < 1) return -1;
  truth bReady = false;
  int iSelected = 0;
  auto cursave = graphics::GetCursorState();
  graphics::HideCursor();
  bitmap Backup(DOUBLE_BUFFER);
  Backup.ActivateFastFlag();
  bitmap Buffer(RES);
  Buffer.ActivateFastFlag();
  int c = 0;
  if (BackGround) {
    BackGround->FastBlit(&Buffer, v2((Buffer.GetWidth() - BackGround->GetWidth()) / 2,
                                     (Buffer.GetHeight() - BackGround->GetHeight()) / 2));
  } else {
    Buffer.ClearToColor(0);
  }
  festring sCopyOfMS;
  festring VeryUnGuruPrintf;
  while (!bReady) {
    clock_t StartTime = clock();
    sCopyOfMS = Topic;
    int i;
    for (i = 0; i < CountChars('\r', Topic); ++i) {
      festring::sizetype RPos = sCopyOfMS.Find('\r');
      VeryUnGuruPrintf = sCopyOfMS;
      VeryUnGuruPrintf.Resize(RPos);
      sCopyOfMS.Erase(0, RPos+1);
      v2 PrintPos(Pos.X-FONT->TextWidth(VeryUnGuruPrintf) / 2,
                  Pos.Y-30-(CountChars('\r', Topic)+CountChars('\r', sMS))*25+i*25);
      FONT->PrintStr(&Buffer, PrintPos, RED, VeryUnGuruPrintf);
    }
    sCopyOfMS = sMS;
    for (i = 0; i < CountChars('\r', sMS); ++i) {
      festring::sizetype RPos = sCopyOfMS.Find('\r');
      VeryUnGuruPrintf = sCopyOfMS;
      VeryUnGuruPrintf.Resize(RPos);
      sCopyOfMS.Erase(0, RPos+1);
      festring ss; ss << i + 1 << ". " << VeryUnGuruPrintf;
      int swdt = FONT->TextWidth(ss);
      int XPos = Pos.X-(swdt / 2);
      int YPos = Pos.Y-CountChars('\r', sMS)*25+i*50;
      Buffer.Fill(XPos, YPos, swdt, 9, 0);
      FONT->PrintStr(&Buffer, v2(XPos + 1, YPos + 1), (i == iSelected ? WHITE : Color), ss);
    }
    sCopyOfMS = SmallText1;
    for (i = 0; i < CountChars('\r', SmallText1); ++i) {
      festring::sizetype RPos = sCopyOfMS.Find('\r');
      VeryUnGuruPrintf = sCopyOfMS;
      VeryUnGuruPrintf.Resize(RPos);
      sCopyOfMS.Erase(0, RPos+1);
      v2 PrintPos(3, RES.Y-CountChars('\r', SmallText1)*10+i*10);
      FONT->PrintStr(&Buffer, PrintPos, Color, VeryUnGuruPrintf);
    }
    sCopyOfMS = SmallText2;
    for (i = 0; i < CountChars('\r', SmallText2); ++i) {
      festring::sizetype RPos = sCopyOfMS.Find('\r');
      VeryUnGuruPrintf = sCopyOfMS;
      VeryUnGuruPrintf.Resize(RPos);
      sCopyOfMS.Erase(0, RPos+1);
      v2 PrintPos(RES.X - FONT->TextWidth(VeryUnGuruPrintf) - 2,
                  RES.Y - CountChars('\r', SmallText2)*10+i*10);
      FONT->PrintStr(&Buffer, PrintPos, Color, VeryUnGuruPrintf);
    }
    int k;
    if (c < 5) {
      int Element = 127-c*25;
      blitdata BlitData = {
        DOUBLE_BUFFER,
        { 0, 0 },
        { 0, 0 },
        { RES.X, RES.Y },
        { (int)MakeRGB24(Element, Element, Element) },
        0,
        0
      };
      Backup.LuminanceMaskedBlit(BlitData);
      Buffer.SimpleAlphaBlit(DOUBLE_BUFFER, c++ * 50, 0);
      graphics::BlitDBToScreen();
      while (clock() - StartTime < 0.05 * CLOCKS_PER_SEC) {}
      k = READ_KEY();
    } else {
      Buffer.FastBlit(DOUBLE_BUFFER);
      graphics::BlitDBToScreen();
      k = GET_KEY(false);
    }
    switch (k) {
      case KEY_UP:
        if (iSelected > 0) --iSelected; else iSelected = (CountChars('\r', sMS)-1);
        break;
      case KEY_DOWN:
        if (iSelected < (CountChars('\r', sMS)-1)) ++iSelected; else iSelected = 0;
        break;
      case 0x00D:
        bReady = true;
        break;
      case KEY_ESC:
        if (allowEsc) return -1;
        break;
      default:
        if (k > 0x30 && k < 0x31+CountChars('\r', sMS)) return k-0x31;
    }
  }
  return iSelected;
}


//==========================================================================
//
//  iosystem::StringQuestion
//
//  Asks the user a question requiring a string answer. The answer is saved
//  to Input. Input can also already have a default something retyped for
//  the user. Topic is the question or other topic for the question. Pos the
//  cordinates of where the question is printed on the screen. Color is the
//  col of all the fonts in this function. Enter is only accepted when the
//  answers length is between MinLetters and MaxLetters. If Fade is true the
//  question is asked on a black background and the transition to that is a
//  fade. If AllowExit is true the user can abort with the esc-key.
//
//  The function returns ABORTED (when user aborts with esc) or NORMAL_EXIT.
//
//==========================================================================
int iosystem::StringQuestion (festring &Input, cfestring &Topic, v2 Pos, col16 Color,
                              festring::sizetype MinLetters, festring::sizetype MaxLetters,
                              truth Fade, truth AllowExit, stringkeyhandler StringKeyHandler)
{
  v2 V(RES.X, 10); ///???????????
  bitmap BackUp(V, 0);
  blitdata B = {
    &BackUp,
    { Pos.X, Pos.Y + 10 },
    { 0, 0 },
    { (int)((MaxLetters << 3) + 9), 10 },
    { 0 },
    0,
    0
  };
  if (Fade) {
    bitmap Buffer(RES, 0);
    Buffer.ActivateFastFlag();
    FONT->PrintStr(&Buffer, Pos, Color, Topic);
    FONT->PrintStr(&Buffer, v2(Pos.X, Pos.Y+10), Color, Input);
    Buffer.FadeToScreen();
  } else {
    DOUBLE_BUFFER->NormalBlit(B);
  }
  auto cursave = graphics::GetCursorState();
  graphics::CaretCursor();
  graphics::ShowCursor();
  truth TooShort = false;
  FONT->PrintStr(DOUBLE_BUFFER, Pos, Color, Topic);
  Swap(B.Src, B.Dest);
  int curpos = Input.GetSize();
  ENABLE_TEXT_INPUT();
  for (int LastKey = 0;; LastKey = 0) {
    B.Bitmap = DOUBLE_BUFFER;
    BackUp.NormalBlit(B);
    FONT->PrintStr(DOUBLE_BUFFER, v2(Pos.X, Pos.Y+10), Color, Input);
    curpos = Clamp(curpos, 0, Input.Length());
    const int curX = FONT->TextWidth(Input.LeftCopy(curpos));
    graphics::GotoXY(Pos.X + curX, Pos.Y + 10);
    if (TooShort) {
      FONT->PrintStr(DOUBLE_BUFFER, v2(Pos.X, Pos.Y+30), Color, CONST_S("Too short!"));
      TooShort = false;
    }
    graphics::BlitDBToScreen();
    if (TooShort) DOUBLE_BUFFER->Fill(Pos.X, Pos.Y+30, 81, 9, 0);
    /* if LastKey is less than 20 it is a control character not available in the font */
    while (!(IsAcceptableForStringQuestion(LastKey))) {
      LastKey = GET_KEY(false);
      if (StringKeyHandler != 0 && StringKeyHandler(LastKey, Input)) { LastKey = 0; break; }
    }
    if (!LastKey) continue;
    if (LastKey == KEY_ESC && AllowExit) {
      DISABLE_TEXT_INPUT();
      return ABORTED;
    }
    if (LastKey == KEY_BACKSPACE) {
      //if (!Input.IsEmpty()) Input.Resize(Input.GetSize()-1);
      if (!Input.IsEmpty() && curpos > 0) {
        if (curpos == Input.Length()) {
          Input.Resize(Input.GetSize()-1);
        } else {
          Input.Erase(curpos-1, 1);
        }
        curpos -= 1;
      }
      continue;
    }
    if (LastKey == KEY_LEFT) { --curpos; continue; }
    if (LastKey == KEY_RIGHT) { ++curpos; continue; }
    if (LastKey == KEY_HOME) { curpos = 0; continue; }
    if (LastKey == KEY_END) { curpos = Input.Length(); continue; }
    if (LastKey == KEY_UP || LastKey == KEY_DOWN) continue;
    if (LastKey == KEY_ENTER) {
      if (Input.GetSize() >= MinLetters) break;
      TooShort = true;
      continue;
    }
    if (LastKey == KEY_DEL_LINE) {
      Input.Empty();
      continue;
    }
    if (LastKey >= ' ' && LastKey < 127 && Input.GetSize() < MaxLetters) {
      //Input << char(LastKey);
      // do not allow duplicate spaces
      bool ok = true;
      if (LastKey == ' ') {
        if (curpos == 0) ok = false;
        if (ok && Input[curpos - 1] == ' ') ok = false;
        if (ok && curpos != Input.Length() && Input[curpos] == ' ') ok = false;
      }
      if (ok) {
        Input.Insert(curpos, (char)LastKey);
        curpos += 1;
      }
      continue;
    }
  }
  /* Delete all the trailing spaces */
  /*
  festring::sizetype LastAlpha = festring::NPos;
  for (festring::sizetype c = 0; c < Input.GetSize(); ++c) if (Input[c] != ' ') LastAlpha = c;
  // note: festring::NPos + 1 == 0
  Input.Resize(LastAlpha+1);
  */
  DISABLE_TEXT_INPUT();
  Input.TrimRight();
  return NORMAL_EXIT;
}


//==========================================================================
//
//  iosystem::NumberQuestion
//
//  Ask a question defined by Topic. This function only accepts numbers.
//  The question is drawn to cordinates given by Pos. All fonts are Color
//  coled. If Fade is true the question is asked on a black background
//  and the transition to that is a fade.
//
//==========================================================================
sLong iosystem::NumberQuestion (cfestring &Topic, v2 Pos, col16 Color, unsigned Flags,
                                truth *escaped)
{
  if (escaped) *escaped = false;
  v2 V(RES.X, 10); ///???????????
  bitmap BackUp(V, 0);
  blitdata B = {
    &BackUp,
    { Pos.X, Pos.Y + 10 },
    { 0, 0 },
    { 105, 10 },
    { 0 },
    0,
    0
  };
  if (Flags & FADE_IN) {
    bitmap Buffer(RES, 0);
    Buffer.ActivateFastFlag();
    FONT->PrintStr(&Buffer, Pos, Color, Topic);
    Buffer.FadeToScreen();
  } else {
    DOUBLE_BUFFER->NormalBlit(B);
  }
  auto cursave = graphics::GetCursorState();
  //graphics::CaretCursor();
  graphics::RectCursor();
  graphics::ShowCursor();
  festring Input;
  FONT->PrintStr(DOUBLE_BUFFER, Pos, Color, Topic);
  Swap(B.Src, B.Dest);
  ENABLE_TEXT_INPUT();
  bool done = false;
  for (int LastKey = 0; !done; LastKey = 0) {
    B.Bitmap = DOUBLE_BUFFER;
    BackUp.NormalBlit(B);
    FONT->PrintStr(DOUBLE_BUFFER, v2(Pos.X, Pos.Y+10), Color, Input);
    graphics::GotoXY(Pos.X + FONT->TextWidth(Input), Pos.Y + 10);
    graphics::BlitDBToScreen();
    while (LastKey == 0) {
      LastKey = GET_KEY(false);
      if (LastKey == KEY_BACKSPACE || LastKey == KEY_ENTER || LastKey == KEY_ESC) {
        /* ok */
      } else if (LastKey == '-' && !Input.IsEmpty()) {
        if ((Flags & ALLOW_NEGATIVE) == 0) {
          LastKey = 0;
        }
      } else if (LastKey >= '0' && LastKey <= '9') {
        /* ok */
      } else {
        LastKey = 0;
      }
    }
    switch (LastKey) {
      case KEY_BACKSPACE:
        if (!Input.IsEmpty()) {
          Input.Resize(Input.GetSize() - 1);
        }
        break;
      case KEY_ENTER:
        if (!Input.IsEmpty() && Input != "-") {
          done = true;
        }
        break;
      case KEY_ESC:
        if (escaped) *escaped = true;
        if (Flags & ZERO_ON_ESC) Input.Empty();
        done = true;
        break;
      default:
        if (Input.GetSize() < 12) {
          Input.AppendChar(LastKey);
        }
        break;
    }
  }
  DISABLE_TEXT_INPUT();
  if (Input.IsEmpty()) return 0;
  return atoi(Input.CStr());
}


//==========================================================================
//
//  iosystem::ScrollBarQuestion
//
//  Asks a question defined by Topic and the answer is numeric. The value is
//  represented by a scroll bar. The topic is drawn to position Pos. Step is
//  the step size. Min and Max are the minimum and maximum values. If the
//  player aborts with the esc key AbortValue is returned. Color1 is the
//  left portion controls the col of left portion of the scroll bar and
//  Color2 the right portion. LeftKey and RightKey are the keys for changing
//  the scrollbar. Although '<' and '>' also work always. If Fade is true
//  the screen is faded to black before drawing th scrollbar. If Handler is
//  set it is called always when the value of the scroll bar changes.
//
//==========================================================================
sLong iosystem::ScrollBarQuestion (cfestring &Topic, v2 Pos,
                                   sLong StartValue, sLong Step,
                                   sLong Min, sLong Max, sLong AbortValue,
                                   col16 TopicColor, col16 Color1, col16 Color2,
                                   int LeftKey, int RightKey,
                                   truth Fade, void (*Handler)(sLong))
{
  sLong BarValue = StartValue;
  festring Input;
  truth FirstTime = true;
  v2 V(RES.X, 20); ///???????????
  bitmap BackUp(V, 0);
  if (Fade) {
    bitmap Buffer(RES, 0);
    Buffer.ActivateFastFlag();
    FONT->Printf(&Buffer, Pos, TopicColor, "%s \1Y%d", Topic.CStr(), StartValue);
    Buffer.DrawHorizontalLine(Pos.X + 1, Pos.X + 201, Pos.Y + 15, Color2, false);
    Buffer.DrawVerticalLine(Pos.X + 201, Pos.Y + 12, Pos.Y + 18, Color2, false);
    Buffer.DrawHorizontalLine(Pos.X + 1, Pos.X + 1 + (BarValue - Min) * 200 / (Max - Min), Pos.Y + 15, Color1, true);
    Buffer.DrawVerticalLine(Pos.X + 1, Pos.Y + 12, Pos.Y + 18, Color1, true);
    Buffer.DrawVerticalLine(Pos.X + 1 + (BarValue - Min) * 200 / (Max - Min), Pos.Y + 12, Pos.Y + 18, Color1, true);
    Buffer.FadeToScreen();
  } else {
    blitdata B = {
      &BackUp,
      { Pos.X, Pos.Y },
      { 0, 0 },
      { RES.X, 20 },
      { 0 },
      0,
      0
    };
    DOUBLE_BUFFER->NormalBlit(B);
  }
  blitdata B1 = {
    0,
    { 0, 0 },
    { Pos.X, Pos.Y },
    { (int)(((Topic.TextLength() + 14) << 3) + 1), 10 },
    { 0 },
    0,
    0
  };
  blitdata B2 = {
    0,
    { 0, 10 },
    { Pos.X, Pos.Y + 10 },
    { 203, 10 },
    { 0 },
    0,
    0
  };
  const int curXOfs = FONT->TextWidth(Topic) + FONT->CharWidth(' ');
  auto cursave = graphics::GetCursorState();
  graphics::RectCursor();
  //graphics::CaretCursor();
  graphics::ShowCursor();
  for (int LastKey = 0;; LastKey = 0) {
    if (!FirstTime) BarValue = Input.IsEmpty() ? Min : atoi(Input.CStr());
    if (BarValue < Min) BarValue = Min;
    if (BarValue > Max) BarValue = Max;
    if (Handler) Handler(BarValue);
    B1.Bitmap = B2.Bitmap = DOUBLE_BUFFER;
    BackUp.NormalBlit(B1);
    BackUp.NormalBlit(B2);
    if (FirstTime) {
      festring ss;
      ss << Topic << " \1Y" << StartValue;
      FONT->PrintStr(DOUBLE_BUFFER, Pos, TopicColor, ss);
      graphics::GotoXY(Pos.X + FONT->TextWidth(ss), Pos.Y);
      FirstTime = false;
    } else {
      FONT->Printf(DOUBLE_BUFFER, Pos, TopicColor, "%s \1Y%s", Topic.CStr(), Input.CStr());
      graphics::GotoXY(Pos.X + curXOfs + FONT->TextWidth(Input), Pos.Y);
    }
    DOUBLE_BUFFER->DrawHorizontalLine(Pos.X + 1, Pos.X + 201, Pos.Y + 15, Color2, false);
    DOUBLE_BUFFER->DrawVerticalLine(Pos.X + 201, Pos.Y + 12, Pos.Y + 18, Color2, false);
    DOUBLE_BUFFER->DrawHorizontalLine(Pos.X + 1, Pos.X + 1 + (BarValue - Min) * 200 / (Max - Min), Pos.Y + 15, Color1, true);
    DOUBLE_BUFFER->DrawVerticalLine(Pos.X + 1, Pos.Y + 12, Pos.Y + 18, Color1, true);
    DOUBLE_BUFFER->DrawVerticalLine(Pos.X + 1 + (BarValue - Min) * 200 / (Max - Min), Pos.Y + 12, Pos.Y + 18, Color1, true);
    graphics::BlitDBToScreen();
    while (!isdigit(LastKey) && LastKey != KEY_ESC && LastKey != KEY_BACKSPACE && LastKey != KEY_ENTER &&
           LastKey != KEY_SPACE && LastKey != RightKey && LastKey != LeftKey)
    {
      LastKey = GET_KEY(false);
    }
    if (LastKey == KEY_ESC) {
      BarValue = AbortValue;
      break;
    }
    if (LastKey == KEY_BACKSPACE) {
      if (!Input.IsEmpty()) Input.Resize(Input.GetSize()-1);
      continue;
    }
    if (LastKey == KEY_ENTER || LastKey == KEY_SPACE) break;
    if (LastKey == LeftKey || LastKey == KEY_LEFT) {
      BarValue -= Step;
      if (BarValue < Min) BarValue = Min;
      Input.Empty();
      Input << BarValue;
      continue;
    }
    if (LastKey == RightKey || LastKey == KEY_RIGHT) {
      BarValue += Step;
      if (BarValue > Max) BarValue = Max;
      Input.Empty();
      Input << BarValue;
      continue;
    }
    if (LastKey == KEY_HOME) {
      BarValue = Min;
      Input.Empty();
      Input << BarValue;
      continue;
    }
    if (LastKey == KEY_END) {
      BarValue = Max;
      Input.Empty();
      Input << BarValue;
      continue;
    }
    if (Input.GetSize() < 12) Input << char(LastKey);
  }
  return BarValue;
}


//==========================================================================
//
//  iosystem::ContinueMenu
//
//  DirectoryName is the directory where the savefiles are located. Returns
//  the selected file or "" if an error occures or if no files are found.
//
//==========================================================================
festring iosystem::ContinueMenu (col16 TopicColor, col16 ListColor, cfestring &DirectoryName) {
  DIR *dp;
  struct dirent *ep;
  auto cursave = graphics::GetCursorState();
  graphics::HideCursor();
  festring Buffer;
  felist List(CONST_S("Choose a file and be sorry:"), TopicColor);
  List.SetSaveSelector(DirectoryName);
  dp = opendir(DirectoryName.CStr());
  if (dp) {
    while ((ep = readdir(dp))) {
      Buffer.Empty();
      Buffer << ep->d_name;
      if (Buffer.EndsWithCI(".save")) {
        /* Add to List all save files */
        List.AddEntry(Buffer.LeftCopy(Buffer.GetSize() - 5), ListColor);
      }
    }
    closedir(dp);
    if (List.IsEmpty()) {
      TextScreen(CONST_S("You don't have any previous saves."), ZERO_V2, TopicColor);
      return CONST_S("");
    } else {
      int Check = List.Draw();
      if (Check & FELIST_ERROR_BIT) return CONST_S("");
      return List.GetEntry(Check);
    }
  }
  return CONST_S("");
}


//==========================================================================
//
//  iosystem::FindSingleSave
//
//==========================================================================
festring iosystem::FindSingleSave (cfestring &DirectoryName) {
  festring res;
  struct dirent *ep;
  DIR *dp = opendir(DirectoryName.CStr());
  if (dp) {
    while ((ep = readdir(dp))) {
      festring ss;
      ss << ep->d_name;
      if (ss.EndsWithCI(".save")) {
        if (!res.IsEmpty()) return festring();
        res = ss; res.Left(res.GetSize() - 5);
      }
    }
    closedir(dp);
  }
  return res;
}


//==========================================================================
//
//  iosystem::IsAcceptableForStringQuestion
//
//==========================================================================
truth iosystem::IsAcceptableForStringQuestion (int Key) {
  switch (Key) {
    case '|':
    case '<':
    case '>':
    case '?':
    case '*':
    case '/':
    case '\\':
    case ':':
      return false;
    default:
      break;
  }
  if (Key < 0x20 && !(Key == KEY_BACKSPACE || Key == KEY_ENTER || Key == KEY_ESC)) return false;
  return true;
}
