/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_FEIO_H__
#define __FELIB_FEIO_H__

#include "felibdef.h"
#include "festring.h"
#include "v2.h"


class bitmap;

typedef truth (*stringkeyhandler) (int, festring &);
typedef void (*bitmapeditor) (bitmap *, truth);


class iosystem {
public:
  // flags for `NumberQuestion`
  static const unsigned DEFAULT = 0u;
  static const unsigned FADE_IN = 1u << 0;
  static const unsigned ZERO_ON_ESC = 1u << 1;
  static const unsigned ALLOW_NEGATIVE = 1u << 2;
public:
  static festring ContinueMenu (col16, col16, cfestring &);
  static int StringQuestion (festring &, cfestring &, v2, col16, festring::sizetype, festring::sizetype,
    truth, truth, stringkeyhandler = 0);
  static sLong NumberQuestion (cfestring &Topic, v2 Pos, col16 Color,
                               unsigned Flags=iosystem::DEFAULT, truth *escaped=0);
  static sLong ScrollBarQuestion (cfestring &, v2, sLong, sLong, sLong, sLong, sLong, col16, col16, col16, int,
    int, truth, void (*)(sLong) = 0);
  static int Menu (cbitmap *BackGround, v2 Pos, cfestring &Topic, cfestring &sMS,
                   col16 Color, cfestring &SmallText1=CONST_S(""), cfestring &SmallText2=CONST_S(""),
                   truth allowEsc=false);
  static void TextScreen (cfestring &, v2 Disp=ZERO_V2, col16=0xFFFF,
                          truth GKey=true, truth Fade=true, bitmapeditor BitmapEditor=0);
  static truth IsAcceptableForStringQuestion (int Key);

  static festring FindSingleSave (cfestring &DirectoryName);
};


#endif
