// public domain
#ifndef FE_JOAAT_HASH
#define FE_JOAAT_HASH

#include "felibdef.h"


static FORCE_INLINE uint32_t joaatHashBufPart (const void *buf, size_t len, uint32_t hash) noexcept {
  const uint8_t *s = (const uint8_t *)buf;
  while (len--) {
    hash += *s++;
    hash += hash<<10;
    hash ^= hash>>6;
  }
  return hash;
}


static FORCE_INLINE uint32_t joaatHashBufPartCI (const void *buf, size_t len, uint32_t hash) noexcept {
  const uint8_t *s = (const uint8_t *)buf;
  while (len--) {
    hash += (*s++)|0x20;
    hash += hash<<10;
    hash ^= hash>>6;
  }
  return hash;
}


static FORCE_INLINE uint32_t joaatHashBufFinish (uint32_t hash) noexcept {
  hash += hash<<3;
  hash ^= hash>>11;
  hash += hash<<15;
  return hash;
}


static FORCE_INLINE uint32_t joaatHashBuf (const void *buf, size_t len, uint32_t seed=0u) noexcept {
  const uint32_t hash = joaatHashBufPart(buf, len, seed);
  return joaatHashBufFinish(hash);
}


static FORCE_INLINE uint32_t joaatHashBufCI (const void *buf, size_t len, uint32_t seed=0u) noexcept {
  const uint32_t hash = joaatHashBufPartCI(buf, len, seed);
  return joaatHashBufFinish(hash);
}

#endif
