/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_FELIBDEF_H__
#define __FELIB_FELIBDEF_H__

static_assert(sizeof(void*) == 4, "64 bit is NOT SUPPORTED and WILL NOT WORK!");


/*
 * Global defines for the project FeLib.
 * This file is created to decrease the need of including headers in
 * other headers just for the sake of some silly macros, because it
 * decreases compilation efficiency and may cause cross-including
 *
 * List of macros that should be gathered here:
 * 1. all numeric defines used in multiple .cpp or .h files
 * 2. all inline functions used in multiple .cpp or .h files
 *    and independent enough (do not require other headers)
 * 3. class construction macros used in multiple .h files
 */

#include "typedef.h"

extern cint MapMoveX[9];
extern cint MapMoveY[9];

extern culong SquarePartTickMask[4];

#define FPI  (3.1415926535897932384626433832795)

/* Btw, both __attribute__ ((regparm(3))) and __fastcall SUCK! */

#define NO_ALIGNMENT          __attribute__ ((packed))
#define NO_RETURN             __attribute__((noreturn))
#define LIKE_PRINTF(p1, p2)   __attribute__((format(printf, p1, p2)))
#define FORCE_INLINE          inline __attribute__((always_inline))
#define OK_UNUSED             __attribute__((unused))

template <class type> constexpr FORCE_INLINE type Max (type X, type Y) { return (X >= Y ? X : Y); }
template <class type> constexpr FORCE_INLINE type Max (type X, type Y, type Z) { return (X >= Y ? (X >= Z ? X : Z) : (Y >= Z ? Y : Z)); }
template <class type> constexpr FORCE_INLINE type Min (type X, type Y) { return (X <= Y ? X : Y); }
template <class type> constexpr FORCE_INLINE type Min (type X, type Y, type Z) { return (X <= Y ? (X <= Z ? X : Z) : (Y <= Z ? Y : Z)); }
template <class type> constexpr FORCE_INLINE type HypotSquare (type X, type Y) { return X * X + Y * Y; }
template <class type> constexpr FORCE_INLINE type Limit (type Value, type Minimum, type Maximum) { return (Value >= Minimum ? (Value <= Maximum ? Value : Maximum) : Minimum); }
template <class type> constexpr FORCE_INLINE type Clamp (type Value, type Minimum, type Maximum) { return (Value >= Minimum ? (Value <= Maximum ? Value : Maximum) : Minimum); }

template <class type> constexpr FORCE_INLINE type Sign (type a) { return (a < 0 ? -1 : a > 0 ? 1 : 0); }

template <class type> FORCE_INLINE void LimitRef (type &Value, type Minimum, type Maximum) {
       if (Value <= Minimum) Value = Minimum;
  else if (Value >= Maximum) Value = Maximum;
}

template <class type> FORCE_INLINE void Swap (type &X, type &Y) {
  const type T = X;
  X = Y;
  Y = T;
}

// divide by two
static FORCE_INLINE int Div2Ass (int n) noexcept {
  // `Div2Ass(1)` == `1`. that's how i want it.
  // in case you wonder why it is like this, think binary!
  if (n > 0) {
    return -((-n) >> 1);
  } else {
    return (n >> 1);
  }
}


static constexpr FORCE_INLINE col16 GetRed16 (col16 Color) { return (Color>>8)&0xF8; }
static constexpr FORCE_INLINE col16 GetGreen16 (col16 Color) { return (Color>>3)&0xFC; }
static constexpr FORCE_INLINE col16 GetBlue16 (col16 Color) { return (Color<<3)&0xF8; }

//constexpr FORCE_INLINE unsigned int MakeColor16 (int c) { return (c < 0 ? 0 : (c > 255 ? 63 : 63*c/255)); }
static constexpr FORCE_INLINE col16 MakeRGB16 (int Red, int Green, int Blue) {
  Red = Clamp(Red, 0, 255);
  Green = Clamp(Green, 0, 255);
  Blue = Clamp(Blue, 0, 255);
  return ((Red << 8) & 0xF800) | ((Green << 3) & 0x7E0) | ((Blue >> 3) & 0x1F);
}

static FORCE_INLINE void RGB16ToRGB32 (const col16 c, uint8_t *r, uint8_t *g, uint8_t *b) {
  *b = (uint8_t)(c << 3);
  *g = (uint8_t)((c >> 5) << 2);
  *r = (uint8_t)((c >> 11) << 3);
  #if 1
  *b |= (*b >> 5);
  *g |= (*g >> 6);
  *r |= (*r >> 5);
  #endif
}

static constexpr FORCE_INLINE col16 MakeShadeColor (col16 Color) {
  return MakeRGB16(GetRed16(Color) / 3, GetGreen16(Color) / 3, GetBlue16(Color) / 3);
}

static constexpr FORCE_INLINE col24 GetRed24 (col24 Color) { return (Color >> 16) & 0xFF; }
static constexpr FORCE_INLINE col24 GetGreen24 (col24 Color) { return (Color >> 8) & 0xFF; }
static constexpr FORCE_INLINE col24 GetBlue24 (col24 Color) { return Color & 0xFF; }

static constexpr FORCE_INLINE col24 MakeRGB24 (int Red, int Green, int Blue) {
  Red = Clamp(Red, 0, 255);
  Green = Clamp(Green, 0, 255);
  Blue = Clamp(Blue, 0, 255);
  return ((Red << 16) & 0xFF0000) | ((Green << 8) & 0xFF00) | ((Blue) & 0xFF);
}

static constexpr FORCE_INLINE int GetMaxColor24 (col24 Color) {
  return Max(GetRed24(Color), GetGreen24(Color), GetBlue24(Color));
}

static constexpr FORCE_INLINE int GetMinColor24 (col24 Color) {
  return Min(GetRed24(Color), GetGreen24(Color), GetBlue24(Color));
}


#define NONE    (0)
#define MIRROR  (1)
#define FLIP    (2)
#define ROTATE  (4)

#define TRANSPARENT_COLOR  (0xF81F) /* pink */

#define RED         (0xF800)
#define GREEN       (0x07E0)
#define BLUE        (0x2ABF)
#define YELLOW      (0xFFE0)
#define PINK        (0xF01E)
#define WHITE       (0xFFFF)
#define LIGHT_GRAY  (0x94B2)
#define DARK_GRAY   (0x528A)
#define BLACK       (0x0000)
#define CYAN        (0x07FF)
#define ORANGE      MakeRGB16(255, 127, 0)

//#define SEL_BLUE    (0x2AAF)
//#define SEL_BLUE    MakeRGB16(40, 84, 120)
#define SEL_BLUE    MakeRGB16(40-10, 84-10, 120-20)

#define NORMAL_LUMINANCE  (0x808080)

// special keycodes for console commands
#define KEY_CC_REDRAW  (1)
#define KEY_CC_ACTED   (2)

#define KEY_MOD_ALT     (0x1000)
#define KEY_MOD_CTRL    (0x2000)
#define KEY_MOD_SHIFT   (0x4000)
#define KEY_MOD_HYPER   (0x8000)

#define KEY_STRIP_MOD_MASK  (0x0fff)

#define KEY_BACKSPACE   (0x08)
#define KEY_ESC         (0x1B)
#define KEY_ENTER       (0x0D)
#define KEY_UP          (0x110)
#define KEY_DOWN        (0x111)
#define KEY_RIGHT       (0x112)
#define KEY_LEFT        (0x113)
#define KEY_HOME        (0x114)
#define KEY_END         (0x115)
#define KEY_PAGE_DOWN   (0x116)
#define KEY_PAGE_UP     (0x117)
#define KEY_INS         (0x120)
#define KEY_DEL         (0x121)
#define KEY_PLUS        (0x130)
#define KEY_MINUS       (0x131)
#define KEY_MUL         (0x132)
#define KEY_DIV         (0x133)
#define KEY_SPACE       (0x20)
#define KEY_NUMPAD_5    (0x105)

#define KEY_PRINTSCREEN (0x140)

#define KEY_TAB         (0x09)

#define KEY_DEL_LINE    (0x7f)

#define KEY_F1          (0x201)
#define KEY_F2          (0x202)
#define KEY_F3          (0x203)
#define KEY_F4          (0x204)
#define KEY_F5          (0x205)
#define KEY_F6          (0x206)
#define KEY_F7          (0x207)
#define KEY_F8          (0x208)
#define KEY_F9          (0x209)
#define KEY_F10         (0x20a)

#define NO_FLAME  (0xFFFF)

/* felist options */
#define SELECTABLE                  (1)
#define INVERSE_MODE                (2)
#define BLIT_AFTERWARDS             (4)
#define DRAW_BACKGROUND_AFTERWARDS  (8)
#define FADE                        (16)
#define FE_COLON_HEADER             (32)
#define FE_ALLOW_TAB                (64)
#define FE_ALLOW_EXCLAM             (128)

/* special list flag */
#define FELIST_NO_BADKEY_EXIT  (0x80)

/* felist errors */
#define FELIST_ERROR_BIT  (0x8000)
#define LIST_WAS_EMPTY    (0xFFFF)
#define ESCAPED           (0xFFFE)
#define NOTHING_SELECTED  (0xFFFD)
#define TAB_PRESSED       (0xFFFC)
#define EXCLAM_PRESSED    (0xFFFB)

#define NO_LIMIT  (0xFFFF)

#define MAX_CONTROLS  (0x10)

#define HIGHEST  (0xFF)

#define NORMAL_EXIT  (0)
#define ABORTED      (1)

#define MAX_CONFIG_OPTIONS  (0x100)

#define FLY_PRIORITY        ((10 << 4) + 10)
#define SPARKLE_PRIORITY    ((12 << 4) + 12)
#define LIGHTNING_PRIORITY  ((14 << 4) + 14)
#define AVERAGE_PRIORITY    ((8 << 4) + 8)

#define NO_IMAGE  (0xFFFF)


using rcint = uint32_t;
#if 0
# define REFS(ptr)   (reinterpret_cast<rcint*>(ptr)[-1])
# define REFSA(ptr)  &(reinterpret_cast<rcint*>(ptr)[-1])
#else
# define REFS(ptr)   (((rcint *)((void *)(ptr)))[-1])
# define REFSA(ptr)  &(((rcint *)((void *)(ptr)))[-1])
#endif

#define SKIP_FIRST         (1)
#define ALLOW_END_FAILURE  (2)
#define SKIP_LAST          (4)
#define LINE_BOTH_DIRS     (8)

//#define MAX_RAND  (0x7FFFFFFF)

#define TRANSPARENT_PALETTE_INDEX  (191)

#define MAX_HIGHSCORES  (100)

/* sparkling flags */

#define SPARKLING_A  (1)
#define SPARKLING_B  (2)
#define SPARKLING_C  (4)
#define SPARKLING_D  (8)


#endif
