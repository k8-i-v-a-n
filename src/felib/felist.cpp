/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include <fstream>

#include "felist.h"
#include "graphics.h"
#include "bitmap.h"
#include "whandler.h"
#include "rawbit.h"
#include "fesave.h"
#include "festring.h"


static const felist *FelistCurrentlyDrawn = 0;
static truth fastListMode = 0;
static int felistPageCount = 0;


truth felist::GetFastListMode () { return fastListMode; }
void felist::SetFastListMode (truth modeon) { fastListMode = modeon; }


truth FelistDrawController () {
  FelistCurrentlyDrawn->DrawPage(DOUBLE_BUFFER, felistPageCount);
  return true;
}


struct felistentry {
public:
  festring String;
  festring Help;
  col16 Color;
  uInt Marginal;
  uInt ImageKey;
  truth Selectable;
  feuLong UData;
  void *UPtr;

public:
  felistentry () : ImageKey(NO_IMAGE) {}
  felistentry (cfestring &aString, col16 aColor, uInt aMarginal, uInt aImageKey,
               truth aSelectable, feuLong udata, void *uptr)
    : String(aString)
    , Help("")
    , Color(aColor)
    , Marginal(aMarginal)
    , ImageKey(aImageKey)
    , Selectable(aSelectable)
    , UData(udata)
    , UPtr(uptr)
  {
    if (String.IsEmpty()) {
      String = CONST_S(" ");
      Selectable = false;
    }
  }
};


outputfile &operator << (outputfile &SaveFile, const felistentry *Entry) {
  SaveFile << Entry->String << Entry->Color << Entry->Marginal << Entry->Selectable;
  return SaveFile;
}


inputfile &operator >> (inputfile &SaveFile, felistentry *&Entry) {
  Entry = new felistentry;
  SaveFile >> Entry->String >> Entry->Color >> Entry->Marginal >> Entry->Selectable;
  return SaveFile;
}


struct felistdescription {
public:
  festring String;
  col16 Color;

public:
  felistdescription () {}
  felistdescription (cfestring &String, col16 Color) : String(String), Color(Color) {}
};


//==========================================================================
//
//  felist::felist
//
//==========================================================================
felist::felist (cfestring &Topic, col16 TopicColor, uInt Maximum)
  : mSaveSelector(false)
  , mSaveSelBmp(0)
  , mSaveSelBmpPath("")
  , mSaveDir("")
  , Maximum(Maximum)
  , Selected(0)
  , Pos(10, 10)
  , Width(RES.X - 20)
  , PageLength(26+10)
  , BackColor(0)
  , Flags(SELECTABLE|FADE)
  , UpKey(KEY_UP)
  , DownKey(KEY_DOWN)
  , IntraLine(0)
  , EntryDrawer(0)
  , helpYPos(0)
  , helpHeight(0)
{
  AddDescription(Topic, TopicColor);
}


//==========================================================================
//
//  felist::~felist
//
//==========================================================================
felist::~felist () {
  delete mSaveSelBmp; mSaveSelBmp = 0;
  Empty();
  for (uInt c = 0; c < Description.size(); ++c) delete Description[c];
}


truth felist::IsEntrySelectable (uInt idx) const { return (idx < Entry.size() ? Entry[idx]->Selectable : false); }

feuLong felist::GetEntryUData (uInt idx) const { return (idx < Entry.size() ? Entry[idx]->UData : 0); }
void felist::SetEntryUData (uInt idx, feuLong udata) { if (idx < Entry.size()) Entry[idx]->UData = udata; }

void *felist::GetEntryUPtr (uInt idx) const { return (idx < Entry.size() ? Entry[idx]->UPtr : 0); }
void felist::SetEntryUPtr (uInt idx, void *uptr) { if (idx < Entry.size()) Entry[idx]->UPtr = uptr; }


//==========================================================================
//
//  felist::Pop
//
//==========================================================================
void felist::Pop () {
  delete Entry[GetLastEntryIndex()];
  Entry.pop_back();
}


//==========================================================================
//
//  felist::CountSelectables
//
//==========================================================================
uInt felist::CountSelectables () const {
  const uInt len = Entry.size();
  uInt Selectables = 0;
  for (uInt c = 0; c != len; c += 1) {
    if (Entry[c]->Selectable) ++Selectables;
  }
  return Selectables;
}


//==========================================================================
//
//  felist::GetSelectable
//
//==========================================================================
felistentry *felist::GetSelectable (uInt idx) const {
  const uInt len = Entry.size();
  for (uInt c = 0; c != len; c += 1) {
    if (Entry[c]->Selectable) {
      if (idx == 0) return Entry[c];
      idx -= 1;
    }
  }
  return nullptr;
}


//==========================================================================
//
//  felist::SelectedToIndex
//
//==========================================================================
uInt felist::SelectedToIndex (uInt idx) const {
  const uInt len = (uInt)Entry.size();
  for (uInt c = 0; c != len; c += 1) {
    if (Entry[c]->Selectable) {
      if (idx == 0) return c;
      idx -= 1;
    }
  }
  return ~(uInt)0;
}


//==========================================================================
//
//  felist::Draw
//
//==========================================================================
uInt felist::Draw () {
  while (Entry.size() && Entry[GetLastEntryIndex()]->String.IsEmpty()) Pop();
  if (Entry.empty()) return LIST_WAS_EMPTY;

  const felist *oldCD = FelistCurrentlyDrawn;
  const int oldPC = felistPageCount;

  int pageCount = 0;
  for (size_t f = 0; f != Entry.size(); f += 1) {
    if (Entry[f]->Selectable) {
      pageCount += 1;
    }
  }
  //ConLogf("000: pageCount=%d; pagelen=%d", pageCount, (int)PageLength);
  pageCount = (pageCount + Max(0, (int)PageLength - 1)) / Max(0, (int)PageLength);
  felistPageCount = pageCount;
  //ConLogf("001: pageCount=%d", pageCount);

  FelistCurrentlyDrawn = this;
  bool removeCLoop = false;
  if (globalwindowhandler::ControlLoopsInstalled()) {
    globalwindowhandler::InstallControlLoop(FelistDrawController);
    removeCLoop = true;
  }

  bitmap BackGround(RES);
  bitmap *Buffer;

  BackGround.ActivateFastFlag();
  if (Flags & FADE) {
    Buffer = new bitmap(RES, 0);
    Buffer->ActivateFastFlag();
    BackGround.ClearToColor(0);
  } else {
    Buffer = DOUBLE_BUFFER;
    Buffer->FastBlit(&BackGround);
  }

  //uInt c;
  uInt Return, Selectables = CountSelectables();
  truth JustSelectMove = false; // block fade?
  if (Selected >= Selectables) Selected = Selectables-1;

       if (Flags & SELECTABLE) PageBegin = Selected-Selected%PageLength;
  else if (Flags & INVERSE_MODE) PageBegin = GetLastEntryIndex()-GetLastEntryIndex()%PageLength;
  else PageBegin = 0;

  bool updateBack = false;

  for (;;) {
    if (helpHeight != 0 || updateBack) {
      BackGround.FastBlit(Buffer);
      updateBack = false;
    }
    truth AtTheEnd = DrawPage(Buffer, pageCount);

    if (Flags & FADE) {
      IvanAssert(Buffer != DOUBLE_BUFFER);
      if (JustSelectMove) {
        Buffer->FastBlit(DOUBLE_BUFFER); // it is already `DOUBLE_BUFFER`
        graphics::BlitDBToScreen();
      } else {
        Buffer->FadeToScreen();
      }
      JustSelectMove = false;
    } else {
      IvanAssert(Buffer == DOUBLE_BUFFER);
      graphics::BlitDBToScreen();
    }

    int Pressed = GET_KEY(false);

    if (Flags & SELECTABLE) {
      // list movement and selections
      int prs = -1;

           if (Pressed >= 'A' && Pressed <= 'Z') prs = Pressed-'A';
      else if (Pressed >= '0' && Pressed <= '9') prs = Pressed-'0'+26;

      if (prs >= 0) {
        if ((uInt)prs < PageLength && (uInt)prs + PageBegin < Selectables) {
          uInt newIdx = (uInt)prs + PageBegin;
          if (newIdx == Selected) {
            Return = Selected = newIdx;
            break;
          }
          Return = Selected = newIdx;
          if (fastListMode) {
            JustSelectMove = true;
            continue;
          } else {
            break;
          }
        }
        continue;
      }

      if (Pressed == UpKey) {
        if (Selected) {
          Selected -= 1;
          if (Selected < PageBegin) {
            PageBegin -= PageLength;
            updateBack = true;
          } else {
            JustSelectMove = true;
          }
        } else {
          uInt c;
          for (c = 0, Selected = 0; c < Entry.size(); ++c) {
            if (Entry[c]->Selectable) ++Selected;
          }
          Selected -= 1;
          if (PageBegin == Selected - Selected % PageLength) {
            JustSelectMove = true;
          } else {
            PageBegin = Selected - Selected % PageLength;
            updateBack = true;
          }
        }
        continue;
      }

      if (Pressed == DownKey) {
        if (!AtTheEnd || Selected != Selectables-1) {
          Selected += 1;
          if (Selected > PageBegin + PageLength - 1) {
            PageBegin += PageLength;
            updateBack = true;
          } else {
            JustSelectMove = true;
          }
        } else {
          if (!PageBegin) {
            JustSelectMove = true;
          } else {
            updateBack = true;
          }
          Selected = PageBegin = 0;
        }
        continue;
      }

      if (Pressed == KEY_HOME) {
        Selected = PageBegin;
        JustSelectMove = true;
        continue;
      }

      if (Pressed == KEY_END) {
        if (Selectables) {
          Selected = Clamp((int)(PageBegin + PageLength - 1), 0, (int)Selectables - 1);
        }
        JustSelectMove = true;
        continue;
      }

      if (Pressed == KEY_LEFT && Selectables && Selected != PageBegin) {
        Selected = PageBegin;
        JustSelectMove = true;
        continue;
      }

      if (Pressed == KEY_RIGHT && Selectables) {
        const int nsl = Clamp((int)(PageBegin + PageLength - 1), 0, (int)Selectables - 1);
        if (nsl != (int)Selected) {
          Selected = nsl;
          JustSelectMove = true;
          continue;
        }
      }

      if (Pressed == KEY_PAGE_UP && Selectables && Selected != PageBegin) {
        Selected = PageBegin;
        JustSelectMove = true;
        continue;
      }

      if (Pressed == KEY_PAGE_DOWN && Selectables) {
        const int nsl = Clamp((int)(PageBegin + PageLength - 1), 0, (int)Selectables - 1);
        const bool atBot = (nsl == (int)Selected);
        if (!atBot) {
          Selected = nsl;
          JustSelectMove = true;
          continue;
        }
      }

      if (Pressed == KEY_LEFT || Pressed == KEY_PAGE_UP) {
        // previous page
        if (Selectables && PageBegin > 0) {
          //const bool atTop = (Selected == PageBegin);
          PageBegin = (PageBegin < PageLength ? 0 : PageBegin - PageLength);
          Selected = Clamp((int)(PageBegin + PageLength - 1), 0, (int)Selectables - 1);
          updateBack = true;
        } else if (Selectables && PageBegin == 0 && Pressed == KEY_PAGE_UP) {
          Selected = PageBegin;
          JustSelectMove = true;
        }
        continue;
      }

      if (Pressed == KEY_RIGHT || Pressed == KEY_PAGE_DOWN) {
        // next page
        if (Selectables) {
          //const int nsl = Clamp((int)(PageBegin + PageLength - 1), 0, (int)Selectables - 1);
          //const bool atBot = (nsl == (int)Selected);
          const uInt pgend = PageBegin + PageLength;
          if (pgend < Selectables) {
            PageBegin += PageLength;
            Selected = PageBegin;
            updateBack = true;
          } else if (Pressed == KEY_PAGE_DOWN) {
            Selected = Selectables - 1;
            JustSelectMove = true;
          }
        }
        continue;
      }

      if (Pressed == KEY_ENTER) {
        Return = Selected;
        break;
      }
    } else {
      // not selectable: allow navigation with left/right
      if (Pressed == KEY_LEFT || Pressed == KEY_PAGE_UP) {
        if (PageBegin > 0) {
          PageBegin -= (PageBegin < PageLength ? PageBegin : PageLength);
          updateBack = true;
        }
        continue;
      }

      if (Pressed == KEY_RIGHT || Pressed == KEY_PAGE_DOWN) {
        if (!AtTheEnd) {
          PageBegin += PageLength;
          updateBack = true;
        }
        continue;
      }
    }

    if (Pressed == KEY_TAB && (Flags & FE_ALLOW_TAB) != 0) {
      Return = TAB_PRESSED;
      //ConLogf("TAB! 0x%04x", Return);
      break;
    }

    if (KEY_EQU(Pressed, "S-1")) {
      if ((Flags & FE_ALLOW_EXCLAM) != 0) {
        Return = EXCLAM_PRESSED;
        break;
      }
      continue;
    }

    if (Pressed == KEY_ESC) {
      Return = ESCAPED;
      break;
    }

    if (Flags & INVERSE_MODE) {
      // message log view
      if (Pressed == KEY_RIGHT || Pressed == KEY_PAGE_DOWN || Pressed == KEY_SPACE) {
        if (PageBegin > 0) {
          PageBegin -= (PageBegin < PageLength ? PageBegin : PageLength);
          updateBack = true;
        } else if (Pressed == KEY_SPACE) {
          Return = NOTHING_SELECTED;
          break;
        }
        continue;
      }

      if (Pressed == KEY_LEFT || Pressed == KEY_PAGE_UP) {
        if (!AtTheEnd) {
          PageBegin += PageLength;
          updateBack = true;
        }
        continue;
      }
    } else {
      if (Pressed == KEY_LEFT || Pressed == KEY_PAGE_UP) {
        if (PageBegin > 0) {
          PageBegin -= (PageBegin < PageLength ? PageBegin : PageLength);
          updateBack = true;
        }
        continue;
      }

      if (Pressed == KEY_RIGHT || Pressed == KEY_PAGE_DOWN || Pressed == KEY_SPACE) {
        if (!AtTheEnd) {
          PageBegin += PageLength;
          updateBack = true;
        } else if (Pressed == KEY_SPACE && !(Flags & FELIST_NO_BADKEY_EXIT)) {
          Return = NOTHING_SELECTED;
          break;
        }
        continue;
      }

      if (AtTheEnd) {
        if (Flags & FELIST_NO_BADKEY_EXIT) continue;
        Return = NOTHING_SELECTED;
        break;
      }

      /*
      // old code
      if ((AtTheEnd && !(Flags&INVERSE_MODE)) || (!PageBegin && (Flags&INVERSE_MODE))) {
        if (Flags&FELIST_NO_BADKEY_EXIT) continue;
        Return = NOTHING_SELECTED;
        break;
      } else {
        BackGround.FastBlit(Buffer);
        if (Flags&INVERSE_MODE) PageBegin -= PageLength; else PageBegin += PageLength;
        if (Flags&SELECTABLE) Selected = PageBegin;
      }
      */
    }
  }

  if (Flags & FADE) {
    IvanAssert(Buffer != DOUBLE_BUFFER);
    delete Buffer;
  } else {
    IvanAssert(Buffer == DOUBLE_BUFFER);
    if (Flags & DRAW_BACKGROUND_AFTERWARDS) {
      BackGround.FastBlit(DOUBLE_BUFFER);
    }
    if (Flags & BLIT_AFTERWARDS) {
      graphics::BlitDBToScreen();
    }
  }

  FelistCurrentlyDrawn = oldCD;
  felistPageCount = oldPC;

  if (removeCLoop) {
    globalwindowhandler::DeInstallControlLoop(FelistDrawController);
  }
  return Return;
}


//==========================================================================
//
//  felist::DrawPage
//
//==========================================================================
truth felist::DrawPage (bitmap *Buffer, int pageCount) const {
  uInt LastFillBottom = Pos.Y + 23 + Description.size() * 10;
  //const uInt startY = LastFillBottom;
  const uInt startY = Pos.Y + 4 + Description.size() * 10;
  festring Str;

  DrawDescription(Buffer);
  uInt c, i; // c == entry index, i == selectable index
  int selIdx = -1;
  for (c = 0, i = 0; i != PageBegin; ++c) {
    if (Entry[c]->Selectable) {
      i += 1;
    }
  }
  while (!Entry[c]->Selectable && Entry[c]->String.IsEmpty()) c += 1;
  std::vector<festring> Chapter;

  felistentry *selEntry = nullptr;
  int selY = 0, lastRenderY = 0;

  //col16 selBack = BackColor^0xFFFF, selText = BackColor;
  //col16 selBack = LIGHT_GRAY, selText = BLACK;
  col16 selBack = SEL_BLUE, selText = WHITE;

  // calc maximum char selection width
  int maxCW = 0;
  for (char ch = 'A'; ch <= 'Z'; ch += 1) {
    const int cw = FONT->CharWidth(ch);
    if (maxCW < cw) maxCW = cw;
  }
  for (char ch = '0'; ch <= '9'; ch += 1) {
    const int cw = FONT->CharWidth(ch);
    if (maxCW < cw) maxCW = cw;
  }

  bool firstLine = true;
  for (;;) {
    Str.Empty();

    uInt Marginal = -(Entry[c]->Marginal * FONT->CharWidth(' '));
    if ((Flags & SELECTABLE) != 0 && Entry[c]->Selectable) {
      Str << "\1#10c000|";
      if (i-PageBegin <= 25) {
        Str << char('A'+(i-PageBegin));
      } else {
        Str << char('0'+(i-PageBegin-26));
      }
      FONT->RPadToPixWidth(Str, maxCW);
      Str << ":\2";
      FONT->RPadToPixWidth(Str, maxCW + FONT->CharWidth(':') + 4);
      Marginal -= FONT->TextWidth(Str);
    }

    Str << Entry[c]->String;

    bool selected = (Flags & SELECTABLE && Entry[c]->Selectable && Selected == i);
    if (selected) {
      selIdx = (int)c;
      selEntry = Entry[c];
      selY = LastFillBottom;
    }

    col16 ecolor = Entry[c]->Color;
    if (selected) {
      if (ecolor != RED && ecolor != GREEN && ecolor != PINK && ecolor != ORANGE) {
        ecolor = selText;
      }
    }

    lastRenderY = LastFillBottom;

    if (firstLine) {
      firstLine = false;
    } else if (IntraLine > 0) {
      Buffer->Fill(Pos.X + 3, LastFillBottom, Width - 6, IntraLine, BackColor);
      LastFillBottom += IntraLine;
    }

    if (Entry[c]->ImageKey != NO_IMAGE) {
      // with icon
      Chapter.clear();
      uInt ChapterSize = FONT->WordWrap(Str, Chapter, Width - 50, Marginal);
      //const int swdt = FONT->TextWidth(Str);
      if (ChapterSize == 1/*swdt <= (int)Width - 50 && Str.Find('\n') == festring::NPos*/) {
        // one line
        Buffer->Fill(Pos.X + 3, LastFillBottom, Width - 6, 20, (!selected ? BackColor : selBack));
        if (EntryDrawer) {
          EntryDrawer(Buffer, v2(Pos.X + 13, LastFillBottom), Entry[c]->ImageKey);
        }
        FONT->PrintStr(Buffer, Pos.X + 37, LastFillBottom + 6, ecolor, Str);
        LastFillBottom += 20;
      } else {
        // several lines
        bool doNormalRender = true;
        uInt PictureTop = LastFillBottom+ChapterSize*5-9;
        {
          Str = Chapter[0];
          festring::sizetype fcp = Str.Find(':');
          if (fcp != festring::NPos) {
            if (Flags & FE_COLON_HEADER) {
              festring::sizetype fcp1 = Str.Find(':', fcp + 1);
              if (fcp1 != festring::NPos) fcp = fcp1;
            }
            fcp += 1;
            doNormalRender = false;
            // cut header
            festring hdr = Str.LeftCopy((int)fcp);
            Str.Erase(0, fcp);
            festring pad;
            FONT->RPadToPixWidth(pad, FONT->TextWidth(hdr));
            Str = pad + Str;
            Chapter[0] = Str;
            // clear background
            int yy = LastFillBottom;
            for (uInt l = 0; l < ChapterSize; ++l) {
              Buffer->Fill(Pos.X + 3, LastFillBottom, Width - 6, 10, !selected ? BackColor : selBack);
              LastFillBottom += 10;
            }
            // draw header
            FONT->PrintStr(Buffer, Pos.X + 37, yy + 6, ecolor, hdr);
            // draw text
            for (uInt l = 0; l < ChapterSize; ++l) {
              FONT->PrintStr(Buffer, Pos.X + 37, yy + 1, ecolor, Chapter[l]);
              yy += 10;
            }
          }
        }
        if (doNormalRender) {
          for (uInt l = 0; l < ChapterSize; ++l) {
            Buffer->Fill(Pos.X + 3, LastFillBottom, Width - 6, 10, !selected ? BackColor : selBack);
            FONT->PrintStr(Buffer, Pos.X + 37, LastFillBottom + 1, ecolor, Chapter[l]);
            LastFillBottom += 10;
          }
        }
        if (EntryDrawer) {
          EntryDrawer(Buffer, v2(Pos.X+13, PictureTop), Entry[c]->ImageKey);
        }
      }
    } else {
      // without icon
      Chapter.clear();
      uInt ChapterSize = FONT->WordWrap(Str, Chapter, (Width-26), Marginal);
      for (uInt l = 0; l < ChapterSize; ++l) {
        Buffer->Fill(Pos.X+3, LastFillBottom, Width-6, 10, !selected ? BackColor : selBack);
        FONT->PrintStr(Buffer, Pos.X + 13, LastFillBottom + 1, ecolor, Chapter[l]);
        LastFillBottom += 10;
      }
    }

    if ((i-PageBegin == PageLength-1 && Entry[c]->Selectable) || c == Entry.size()-1) {
      if ((!(Flags&INVERSE_MODE) && c != Entry.size()-1) || (Flags&INVERSE_MODE && PageBegin)) {
        Buffer->Fill(Pos.X+3, LastFillBottom, Width-6, 30, BackColor);
        if (pageCount <= 0) {
          FONT->PrintStr(Buffer, v2(Pos.X+13, LastFillBottom+10), WHITE,
                         CONST_S("- Press \1YSPACE\2 to continue, \1RESC\2 to exit -"));
        } else {
          festring pt;
          pt << "- Press \1YSPACE\2 to continue, \1RESC\2 to exit - \1O[";
          pt << ((int)PageBegin / Max(1, (int)PageLength)) + 1 << "/";
          pt << pageCount << "]\2 -";
          FONT->PrintStr(Buffer, v2(Pos.X+13, LastFillBottom+10), WHITE, pt);
        }
        LastFillBottom += 30;
      } else if (pageCount > 1) {
        Buffer->Fill(Pos.X+3, LastFillBottom, Width-6, 30, BackColor);
        festring pt;
        pt << "- Press \1YSPACE\2 to continue, \1RESC\2 to exit - \1O[";
        pt << ((int)PageBegin / Max(1, (int)PageLength)) + 1 << "/";
        pt << pageCount << "]\2 -";
        FONT->PrintStr(Buffer, v2(Pos.X+13, LastFillBottom+10), WHITE, pt);
        LastFillBottom += 30;
      } else {
        Buffer->Fill(Pos.X+3, LastFillBottom, Width-6, 10, BackColor);
        LastFillBottom += 10;
      }
      Buffer->DrawRectangle(Pos.X+1, Pos.Y+1, Pos.X+Width-2, LastFillBottom+1, DARK_GRAY, true);
      break;
    }
    if (Entry[c++]->Selectable) ++i;
  }

  // draw help
  helpHeight = 0;
  if (/*helpHeight != 0 ||*/ (selEntry && !selEntry->Help.IsEmpty())) {
    std::vector<festring> text;
    int lines = 0;
    if (selEntry && !selEntry->Help.IsEmpty()) {
      lines = FONT->WordWrap(selEntry->Help, text, Width - 20);
    }
    uInt y0;
    int hgt = lines * 10 + 1;
    (void)startY; (void)selY; (void)lastRenderY;

    if (hgt > helpHeight) {
      helpHeight = hgt;
    } else {
      hgt = helpHeight;
    }

    v2 bmpsz = Buffer->GetSize();

    y0 = LastFillBottom - 3;
    if (y0 + hgt + 16 >= (uInt)bmpsz.Y) {
      //y0 = bmpsz.Y - hgt - 16;
      if (bmpsz.Y - hgt - 16 <= selY + 4) {
        y0 = startY + 6;
      } else {
        y0 = bmpsz.Y - hgt - 16;
      }
      //helpYPos = y0;
    } else {
      if (y0 < helpYPos) y0 = helpYPos; else helpYPos = y0;
    }

    Buffer->DrawSimplePopup(v2(Pos.X + 4, y0 - 3), v2(Width - 8, hgt + 4 + 1),
                            LIGHT_GRAY, MakeRGB16(24-8, 24-8, 24-8));

    y0 += 1;
    for (int f = 0; f != lines; f += 1) {
      FONT->PrintStr(Buffer, v2(Pos.X + 10, y0), /*YELLOW*/MakeRGB16(0xd0, 0xd0, 0x10), text[f]);
      y0 += 10;
    }
  }

  if (selIdx != -1 && mSaveSelector) {
    bool ok = false;
    festring imgName = mSaveDir;
    imgName << Entry[selIdx]->String << ".save";
    if (imgName != mSaveSelBmpPath && inputfile::fileExists(imgName)) {
      mSaveDesc.clear();
      delete mSaveSelBmp;
      mSaveSelBmp = new bitmap(v2(1, 1), 0);
      mSaveSelBmpPath = imgName;
      SQArchive sqa;
      sqa.Open(imgName, false);
      if (sqa.IsOpen()) {
        SQAFile ifd = sqa.FileOpen(CONST_S("screenshot.raw"));
        if (ifd) {
          RawBitmapScreenShot *rawss = new RawBitmapScreenShot(&sqa, ifd);
          if (mSaveSelBmp->SetFromRaw(rawss)) {
            ok = true;
          }
          sqa.FileClose(ifd);
          delete rawss;
        }
        if (ok) {
          ifd = sqa.FileOpen(CONST_S("info.txt"));
          if (ifd) {
            festring ss;
            while (sqa.FileTell(ifd) != sqa.FileSize(ifd)) {
              char ch;
              sqa.FileRead(ifd, &ch, 1);
              if (sqa.WasError()) break;
              if (ch == 13) continue;
              if (ch == 10) {
                mSaveDesc.push_back(ss);
                ss.Empty();
              } else {
                ss.AppendChar(ch);
              }
            }
            sqa.FileClose(ifd);
            if (!ss.IsEmpty()) mSaveDesc.push_back(ss);
          }
        }
        sqa.Close();
      }
    } else if (imgName != mSaveSelBmpPath) {
      mSaveSelBmpPath = imgName;
    }

    if (ok && mSaveSelBmp) {
      int x = Buffer->GetSize().X - mSaveSelBmp->GetSize().X-2;
      int y = Buffer->GetSize().Y - mSaveSelBmp->GetSize().Y-2;
      int w = mSaveSelBmp->GetSize().X;
      int h = mSaveSelBmp->GetSize().Y;
      blitdata bd = {
        Buffer,
        {0, 0}, //src
        {x, y}, //dest
        {w, h}, //border
        {0}, // luminance/flags
        0xDEAD, // mask color
        0 // custom data
      };
      mSaveSelBmp->NormalBlit(bd);
      Buffer->DrawRectangle(x-2, y-2, x+w, y+h, DARK_GRAY, true);

      // draw game description
      v2 dpos = v2(4, Buffer->GetSize().Y);
      for (int f = (int)mSaveDesc.size() - 1; f >= 0; f -= 1) {
        dpos.Y -= 10;
        FONT->PrintStr(Buffer, dpos, WHITE, mSaveDesc[f]);
      }
    } else {
      delete mSaveSelBmp; mSaveSelBmp = 0;
      mSaveDesc.clear();
    }
  }

  return (c == Entry.size() - 1);
}


//==========================================================================
//
//  felist::DrawDescription
//
//==========================================================================
void felist::DrawDescription (bitmap *Buffer) const {
  Buffer->Fill(Pos.X+3, Pos.Y+3, Width-6, 20, BackColor);
  for (uInt c = 0; c < Description.size(); ++c) {
    Buffer->Fill(Pos.X+3, Pos.Y+13+c*10, Width-6, 10, BackColor);
    FONT->PrintStr(Buffer, v2(Pos.X+13, Pos.Y+13+c*10),
                       Description[c]->Color, Description[c]->String);
  }
  Buffer->Fill(Pos.X+3, Pos.Y+13+Description.size()*10, Width-6, 10, BackColor);
}


//==========================================================================
//
//  felist::QuickDraw
//
//  We suppose InverseMode != false here
//  this is used only for message log. and message log should be
//  already wrapped...
//
//==========================================================================
void felist::QuickDraw (bitmap *Bitmap, uInt PageLength) const {
  static std::vector<festring> Chapter;
  uInt Width = Bitmap->GetSize().X;
  Bitmap->Fill(3, 3, Width-6, 20+PageLength*10, 0);
  Bitmap->DrawRectangle(1, 1, Width-2, 24+PageLength*10, DARK_GRAY, true);
  const uInt LineSize = Width-26;
  uInt Index = 0;
  uInt Bottom = PageLength*10+3;
  for (uInt c1 = 0; c1 <= Selected; ++c1) {
    const felistentry *CurrentEntry = Entry[Selected-c1];
    Chapter.clear();
    uInt ChapterSize = FONT->WordWrap(CurrentEntry->String, Chapter, LineSize, CurrentEntry->Marginal);
    for (uInt c2 = 0; c2 < ChapterSize; ++c2) {
      col16 Color = CurrentEntry->Color;
      const uInt xidx = Min((uInt)3, Index);
      Color = MakeRGB16(
        GetRed16(Color)-((GetRed16(Color)*3*xidx/PageLength)>>2),
        GetGreen16(Color)-((GetGreen16(Color)*3*xidx/PageLength)>>2),
        GetBlue16(Color)-((GetBlue16(Color)*3*xidx/PageLength)>>2));
      FONT->PrintStr(Bitmap, v2(13, Bottom), Color, Chapter[ChapterSize-c2-1]);
      Bottom -= 10;
      if (++Index == PageLength) return;
    }
  }
}


//==========================================================================
//
//  felist::Empty
//
//==========================================================================
void felist::Empty () {
  for (uInt c = 0; c < Entry.size(); ++c) delete Entry[c];
  Entry.clear();
}


//==========================================================================
//
//  felist::AddEntry
//
//==========================================================================
void felist::AddEntry (cfestring &Str, col16 Color, uInt Marginal, uInt Key,
                       truth Selectable, feuLong udata, void *uptr)
{
  Entry.push_back(new felistentry(Str, Color, Marginal, Key, Selectable, udata, uptr));
  if (Maximum && Entry.size() > feuLong(Maximum)) {
    delete Entry[0];
    Entry.erase(Entry.begin());
  }
}


//==========================================================================
//
//  felist::AddLastEntryHelp
//
//==========================================================================
void felist::AddLastEntryHelp (cfestring &Str) {
  const uInt len = Entry.size();
  if (len != 0) {
    Entry[len - 1]->Help = Str;
  }
}


//==========================================================================
//
//  felist::Save
//
//==========================================================================
void felist::Save (outputfile &SaveFile) const {
  SaveFile << Entry << Maximum << Selected;
}


//==========================================================================
//
//  felist::Load
//
//==========================================================================
void felist::Load (inputfile& SaveFile) {
  SaveFile >> Entry >> Maximum >> Selected;
}


//==========================================================================
//
//  felist::PrintToFile
//
//==========================================================================
void felist::PrintToFile (cfestring& FileName) {
  std::ofstream SaveFile(FileName.CStr(), std::ios::out);
  if (!SaveFile.is_open()) return;
  uInt c;
  for (c = 0; c < Description.size(); ++c) SaveFile << Description[c]->String.CStr() << std::endl;
  SaveFile << std::endl;
  for (c = 0; c < Entry.size(); ++c) {
    if (Entry[c]->ImageKey != NO_IMAGE) SaveFile << "   ";
    SaveFile << Entry[c]->String.CStr() << std::endl;
  }
}


//==========================================================================
//
//  felist::AddDescription
//
//==========================================================================
void felist::AddDescription (cfestring &Str, col16 Color) {
  Description.push_back(new felistdescription(Str, Color));
}


//==========================================================================
//
//  felist::GetEntry
//
//==========================================================================
festring felist::GetEntry (uInt I) const {
  return (I < Entry.size() ? Entry[I]->String : festring::EmptyStr());
}


//==========================================================================
//
//  felist::GetSelectableEntry
//
//==========================================================================
festring felist::GetSelectableEntry (uInt I) const {
  uInt idx = SelectedToIndex(I);
  if (idx != ~(uInt)0) {
    return Entry[idx]->String;
  }
  return festring::EmptyStr();
}


//==========================================================================
//
//  felist::UpdateSelectableEntry
//
//==========================================================================
void felist::UpdateSelectableEntry (uInt I, cfestring &str) {
  uInt idx = SelectedToIndex(I);
  if (idx != ~(uInt)0) {
    Entry[idx]->String = str;
  }
}


//==========================================================================
//
//  felist::UpdateEntry
//
//==========================================================================
void felist::UpdateEntry (uInt I, cfestring &str) {
  if (I < Entry.size()) {
    Entry[I]->String = str;
  }
}


//==========================================================================
//
//  felist::GetColor
//
//==========================================================================
col16 felist::GetColor (uInt I) const {
  return (I < Entry.size() ? Entry[I]->Color : 0);
}


//==========================================================================
//
//  felist::SetColor
//
//==========================================================================
void felist::SetColor (uInt I, col16 What) {
  if (I < Entry.size()) {
    Entry[I]->Color = What;
  }
}
