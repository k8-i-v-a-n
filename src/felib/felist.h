/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_LIST_H__
#define __FELIB_LIST_H__

#include "felibdef.h"
#include <vector>

#include "festring.h"
#include "v2.h"


class outputfile;
class inputfile;
class rawbitmap;
class bitmap;
class festring;
struct felistentry;
struct felistdescription;


typedef void (*entrydrawer) (bitmap *Bitmap, v2 Pos, uInt);

class felist {
public:
  felist (cfestring &Topic, col16 TopicColor=WHITE, uInt Maximum=0);
  ~felist ();
  void AddEntry (cfestring &Str, col16 Color, uInt Marginal=0, uInt Key=NO_IMAGE,
                 truth Selectable=true, feuLong udata=0, void *uptr=nullptr);
  void AddLastEntryHelp (cfestring &Str);
  void AddDescription (cfestring &Str, col16 Color=WHITE);
  uInt Draw ();
  void QuickDraw (bitmap *Bitmap, uInt PageLength) const;
  void Empty ();
  void EmptyDescription () { Description.resize(1); }
  void Load (inputfile &SaveFile);
  void Save (outputfile &SaveFile) const;
  uInt GetSelected () const { return Selected; }
  void SetSelected (uInt What) { Selected = What; }
  void EditSelected (int What) { Selected += What; }
  uInt SelectedToIndex (uInt I) const;
  truth DrawPage (bitmap *Bitmap, int pageCount=-1) const; // return "at the end" flag
  void Pop ();
  void PrintToFile (cfestring &SaveFile);
  uInt GetWidth () const { return Width; }
  void SetPos (v2 What) { Pos = What; }
  void SetWidth (uInt What) { Width = What; }
  void SetPageLength (uInt What) { PageLength = What; }
  void SetBackColor (col16 What) { BackColor = What; }
  void SetFlags (uInt What) { Flags = What; }
  void AddFlags (uInt What) { Flags |= What; }
  void RemoveFlags (uInt What) { Flags &= ~What; }
  void SetUpKey (int What) { UpKey = What; }
  void SetDownKey (int What) { DownKey = What; }
  void SetEntryDrawer (entrydrawer What) { EntryDrawer = What; }
  truth IsEmpty () const { return Entry.empty(); }
  uInt GetLength () const { return Entry.size(); }
  uInt GetLastEntryIndex () const { return Entry.size()-1; }
  festring GetEntry (uInt I) const;
  truth IsEntrySelectable (uInt idx) const;
  feuLong GetEntryUData (uInt idx) const;
  void SetEntryUData (uInt idx, feuLong udata);
  void *GetEntryUPtr (uInt idx) const;
  void SetEntryUPtr (uInt idx, void *uptr);
  col16 GetColor (uInt I) const;
  void SetColor (uInt I, col16 What);

  festring GetSelectableEntry (uInt I) const;
  void UpdateSelectableEntry (uInt I, cfestring &str);

  void UpdateEntry (uInt I, cfestring &str);

  static truth GetFastListMode ();
  static void SetFastListMode (truth modeon);

  inline truth IsSaveSelector () const { return mSaveSelector; }
  inline void ResetSaveSelector () { mSaveSelector = false; }
  inline void SetSaveSelector (cfestring &dir) { mSaveDir = dir; mSaveSelector = true; }

  int GetIntraLine () const { return IntraLine; }
  void SetIntraLine (int v) { IntraLine = v; }

private:
  void DrawDescription (bitmap *Bitmap) const;

  uInt CountSelectables () const;
  felistentry *GetSelectable (uInt idx) const;

private:
  truth mSaveSelector;
  mutable class bitmap *mSaveSelBmp;
  mutable festring mSaveSelBmpPath;
  mutable std::vector<festring> mSaveDesc;
  festring mSaveDir;
  std::vector<felistentry *> Entry;
  std::vector<felistdescription *> Description;
  uInt PageBegin;
  uInt Maximum;
  uInt Selected;
  v2 Pos;
  uInt Width;
  uInt PageLength;
  col16 BackColor;
  uInt Flags;
  int UpKey;
  int DownKey;
  int IntraLine;
  entrydrawer EntryDrawer;
  mutable uInt helpYPos; //k8: sorry!
  mutable int helpHeight; //k8: sorry!
};


#endif
