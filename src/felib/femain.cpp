/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include <iostream>
#include <cstdlib>

#include <SDL2/SDL.h>
#include "feerror.h"
#include "sqlite3.h"
#include "vwfileio.h"
#include "cconsole.h"


extern int Main (int, char **);


static void errorLogCallback (void *pArg, int iErrCode, const char *zMsg) {
  switch (iErrCode) {
    case SQLITE_NOTICE:
      ConLogf("***SQLITE NOTICE: %s", zMsg);
      break;
    case SQLITE_WARNING:
      ConLogf("***SQLITE WARNING: %s", zMsg);
      break;
    case SQLITE_NOTICE_RECOVER_WAL:
      ConLogf("***SQLITE WAL RECOVER: %s", zMsg);
      break;
    case SQLITE_NOTICE_RECOVER_ROLLBACK:
      ConLogf("***SQLITE ROLLBACK RECOVER: %s", zMsg);
      break;
    case SQLITE_WARNING_AUTOINDEX:
      ConLogf("+++SQLITE AUTOINDEX WARNING: %s", zMsg);
      break;
    default:
      ConLogf("SQLITE ERROR LOG: (%d) %s", iErrCode, zMsg);
  }
}


int main (int argc, char *argv[]) {
  bool archfirst = true;
  bool setSQliteLog = false;
  int aidx = 1;
  while (aidx < argc) {
    const char *a = argv[aidx]; aidx += 1;
    bool removeit = false;
         if (!a) removeit = true;
    else if (strcmp(a, "--gdb") == 0) { archfirst = false; setSQliteLog = true; globalerrorhandler::activateGDBMode(); removeit = true; }
    else if (strcmp(a, "--gdb-read") == 0) { archfirst = false; setSQliteLog = true; globalerrorhandler::activateGDBReadMode(); removeit = true; }
    else if (strcmp(a, "--sqlite-log") == 0) { archfirst = false; setSQliteLog = true; removeit = true; }
    else if (strcmp(a, "--disk-first") == 0) { archfirst = false; removeit = true; }
    else if (strcmp(a, "--archive-first") == 0) { archfirst = true; removeit = true; }
    if (removeit) {
      aidx -= 1;
      for (int c = aidx + 1; c < argc; c += 1) argv[c - 1] = argv[c];
      argc -= 1;
    }
  }

  if (setSQliteLog) {
    sqlite3_config(SQLITE_CONFIG_LOG, errorLogCallback, nullptr);
  }

  if (archfirst) {
    vw_archives_first = 1;
  } else {
    vw_archives_first = 0;
  }

  /*try
  {*/
    return Main(argc, argv);
  /*}
  catch(...)
  {
    cchar* Msg = "Fatal Error: Unknown exception thrown.";
    std::cout << Msg << globalerrorhandler::GetBugMsg() << std::endl;
    exit(3);
  }
  exit(0);*/
}
