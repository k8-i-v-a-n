/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef SHITDOZE
# ifndef _GNU_SOURCE
#  define _GNU_SOURCE
# endif
# include <unistd.h>
# include <sys/syscall.h>
#endif

#include "felibdef.h"
#include <cmath>

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <unistd.h>

#ifdef SHITDOZE
# include <windows.h>
// fuck you, shitdoze!
# ifdef CharUpper
#  undef CharUpper
# endif
#else
# include <fcntl.h>
# include <stdlib.h>
# include <time.h>
# include <unistd.h>
# include <sys/stat.h>
# include <sys/types.h>
#endif

#include "femath.h"
#include "feerror.h"
#include "fesave.h"
#include "feparse.h"


cint basequadricontroller::OrigoDeltaX[4] = { 0, 1, 0, 1 };
cint basequadricontroller::OrigoDeltaY[4] = { 0, 0, 1, 1 };
int basequadricontroller::OrigoX, basequadricontroller::OrigoY;
int basequadricontroller::StartX, basequadricontroller::StartY;
int basequadricontroller::XSize, basequadricontroller::YSize;
int basequadricontroller::RadiusSquare;
truth basequadricontroller::SectorCompletelyClear;
#ifdef FE_USE_PCG32
PCG32 femath::prng(0x29a, 42);
#else
BJPRNG femath::prng(0x29a);
#endif


cint MapMoveX[9] = { -1, 0, 1, -1, 1, -1, 0, 1, 0 };
cint MapMoveY[9] = { -1, -1, -1, 0, 0, 1, 1, 1, 0 };

culong SquarePartTickMask[4] = { 0xFF, 0xFF00, 0xFF0000, 0xFF000000 };


#ifdef FE_USE_PCG32
PCG32 ngprng(0x29a, 0x29a);
#else
BJPRNG ngprng(0x29b);
#endif


// ////////////////////////////////////////////////////////////////////////// //
// ISAAC+ PRNG
// ////////////////////////////////////////////////////////////////////////// //

static FORCE_INLINE uint32_t isaac_rra (uint32_t value, unsigned int count) { return (value>>count)|(value<<(32-count)); }
static FORCE_INLINE uint32_t isaac_rla (uint32_t value, unsigned int count) { return (value<<count)|(value>>(32-count)); }

static FORCE_INLINE void isaac_putu32 (uint8_t *p, const uint32_t v) {
  p[0] = (uint8_t)(v&0xffu);
  p[1] = (uint8_t)((v>>8)&0xffu);
  p[2] = (uint8_t)((v>>16)&0xffu);
  p[3] = (uint8_t)((v>>24)&0xffu);
}

/*
 * ISAAC+ "variant", the paper is not clear on operator precedence and other
 * things. This is the "first in, first out" option!
 */
typedef struct isaacp_state_t {
  uint8_t buffer[1024];
  size_t left;
  // internal state; should be filled with good random values
  uint32_t state[256];
  uint32_t abc[3];
} isaacp_state;

#define isaacp_step(offset,mix) \
  x = mm[i+offset]; \
  a = (a^(mix))+(mm[(i+offset+128u)&0xffu]); \
  y = (a^b)+mm[(x>>2)&0xffu]; \
  mm[i+offset] = y; \
  b = (x+a)^mm[(y>>10)&0xffu]; \
  isaac_putu32(out+(i+offset)*4u, b);

//==========================================================================
//
//  isaacp_mix
//
//==========================================================================
static FORCE_INLINE void isaacp_mix (isaacp_state *st) {
  uint32_t x, y;
  uint32_t a = st->abc[0], b = st->abc[1], c = st->abc[2];
  uint32_t *mm = st->state;
  uint8_t *out = st->buffer;
  c = c+1u;
  b = b+c;
  for (unsigned i = 0u; i < 256u; i += 4u) {
    isaacp_step(0u, isaac_rla(a,13u))
    isaacp_step(1u, isaac_rra(a, 6u))
    isaacp_step(2u, isaac_rla(a, 2u))
    isaacp_step(3u, isaac_rra(a,16u))
  }
  st->abc[0] = a;
  st->abc[1] = b;
  st->abc[2] = c;
  st->left = 1024u;
}


//==========================================================================
//
//  isaacp_random
//
//==========================================================================
static void isaacp_random (isaacp_state *st, void *p, size_t len) {
  uint8_t *c = (uint8_t *)p;
  while (len) {
    const size_t use = (len > st->left ? st->left : len);
    memcpy(c, st->buffer+(sizeof(st->buffer)-st->left), use);
    st->left -= use;
    c += use;
    len -= use;
    if (!st->left) isaacp_mix(st);
  }
}


static isaacp_state isaac;
static bool isaacInited = false;


#ifndef SHITDOZE
//==========================================================================
//
//  xread
//
//==========================================================================
static void xread (int fd, void *buf, uint32_t size) {
  uint8_t *dest = (uint8_t *)buf;
  while (size != 0) {
    ssize_t rr = read(fd, dest, size);
    if (rr < 0) {
      if (errno == EINTR) continue;
      ABORT("cannot read urandom!");
    } else if (rr == 0) {
      ABORT("cannot read urandom!");
    } else {
      dest += rr;
      size -= (uint32_t)rr;
    }
  }
}
#endif


#ifdef SHITDOZE
typedef BOOLEAN WINAPI (*RtlGenRandomFn) (PVOID RandomBuffer, ULONG RandomBufferLength);
static RtlGenRandomFn RtlGenRandomXX = NULL;


//==========================================================================
//
//  ReadSysRandom
//
//==========================================================================
static void ReadSysRandom (void *buf, size_t len) {
  if (!len) __builtin_trap();
  if (!RtlGenRandomXX) {
    HMODULE libh = LoadLibraryA("advapi32.dll");
    if (!libh) ABORT("cannot load \"advapi32.dll\"");
    RtlGenRandomXX = (RtlGenRandomFn)(void *)GetProcAddress(libh, "SystemFunction036");
    if (!RtlGenRandomXX) ABORT("cannot find `RtlGetRandom()`");
    //FreeLibrary(libh);
  }
  BOOLEAN res = RtlGenRandomXX(buf, (ULONG)len);
  if (!res) ABORT("no reliable random source found");
}

#else

//==========================================================================
//
//  ReadSysRandom
//
//==========================================================================
static void ReadSysRandom (void *buf, size_t len) {
  if (!len) __builtin_trap();
  uint8_t *dest = (uint8_t *)buf;
  memset(buf, 0, len);
  while (len != 0) {
    uint32_t rd = (len > 256 ? 256 : (uint32_t)len);
    long res = syscall(SYS_getrandom, dest, rd, 0);
    if (res < 0) {
      fprintf(stderr, "ERROR: no reliable random source found!\n");
      int fd = open("/dev/urandom", O_RDONLY);
      if (fd < 0) __builtin_trap();
      xread(fd, dest, len);
      close(fd);
      len = 0;
    } else {
      if ((uint32_t)res > rd) __builtin_trap();
      #if 0
      fprintf(stderr, "PRNG: %d bytes read, dump follows:", (int)res);
      for (long f = 0; f != res; f += 1) {
             if (f % 16 == 0) fputc('\n', stderr);
        else if (f % 8 == 0) fputc(' ', stderr);
        fprintf(stderr, " %02X", dest[f]);
      }
      fputc('\n', stderr);
      #endif
      dest += (uint32_t)res;
      len -= (uint32_t)res;
    }
  }
}
#endif


//==========================================================================
//
//  isaac_seed
//
//==========================================================================
static void isaac_seed () {
  /*
  #ifndef SHITDOZE
  int fd = open("/dev/urandom", O_RDONLY);
  if (fd >= 0) {
    xread(fd, &isaac, sizeof(isaac));
    close(fd);
  }
  #else
  const uint32_t upid = (uint32_t)getpid();
  const uint32_t utim = (uint32_t)time(NULL);
  PCG32 pcg32;
  pcg32.RandomSeed();
  for (size_t f = 0; f != 1024; f += 1) {
    (void)pcg32.U32();
  }
  size_t left = sizeof(isaac);
  uint8_t *ptr = (uint8_t *)&isaac;
  while (left != 0) {
    uint32_t v = pcg32.U32();
    if (left >= 4) {
      memcpy(ptr, &v, 4);
      ptr += 4;
      left -= 4;
    } else {
      memcpy(ptr, &v, left);
      left = 0;
    }
  }
  #endif
  */
  ReadSysRandom(&isaac.state, sizeof(isaac.state));
  ReadSysRandom(&isaac.abc, sizeof(isaac.abc));
  for (int f = 0; f != 16; f += 1) {
    isaacp_mix(&isaac);
  }
}


//==========================================================================
//
//  RandomBuffer
//
//  uses independent PRNG
//
//==========================================================================
void RandomBuffer (void *buf, size_t bufsize) {
  if (bufsize != 0) {
    if (!isaacInited) {
      isaacInited = true;
      isaac_seed();
    }
    isaacp_random(&isaac, buf, bufsize);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// PCG32
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  PCG32::RandomSeed
//
//==========================================================================
void PCG32::RandomSeed (uint32_t astream) noexcept {
  /*
  #ifndef SHITDOZE
    const uint32_t upid = (uint32_t)getpid();
    const uint32_t utim = (uint32_t)time(NULL);
  #else
    const uint32_t upid = (uint32_t)GetCurrentProcessId();
    const uint32_t utim = (uint32_t)GetTickCount();
  #endif
  const uint32_t res0 = HashU32(upid ^ utim);
  const uint32_t res1 = HashU32(upid) ^ HashU32(utim);
  state = (((uint64_t)res0) << 32) | res1;
  #if 0
    //k8: i believe that this is overkill. no, really: do we need
    //    The Perfect Seed for the game at all?
    #ifndef SHITDOZE
      int fd = open("/dev/urandom", O_RDONLY);
      if (fd >= 0) {
        xread(fd, &state, sizeof(state));
        close(fd);
      }
    #endif
  #endif
  stream = (astream << 1) | 1;
  */
  RandomBuffer(&state, sizeof(state));
  stream = (astream << 1) | 1;
}


//==========================================================================
//
//  PCG32::Save
//
//==========================================================================
void PCG32::Save (outputfile &SaveFile) {
  SaveFile << state << stream;
}


//==========================================================================
//
//  PCG32::Load
//
//==========================================================================
void PCG32::Load (inputfile &SaveFile) {
  SaveFile >> state >> stream;
}


// ////////////////////////////////////////////////////////////////////////// //
// BJPRNG
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  BJPRNG::RandomSeed
//
//==========================================================================
void BJPRNG::RandomSeed () noexcept {
  /*
  #ifndef SHITDOZE
    const uint32_t upid = (uint32_t)getpid();
    const uint32_t utim = (uint32_t)time(NULL);
  #else
    const uint32_t upid = (uint32_t)GetCurrentProcessId();
    const uint32_t utim = (uint32_t)GetTickCount();
  #endif
  uint32_t xseed = HashU32(upid) - HashU32(utim);
  #if 0
    //k8: i believe that this is overkill. no, really: do we need
    //    The Perfect Seed for the game at all?
    #ifndef SHITDOZE
      int fd = open("/dev/urandom", O_RDONLY);
      if (fd >= 0) {
        xread(fd, &xseed, sizeof(xseed));
        close(fd);
      }
    #endif
  #endif
  SetSeed(xseed);
  */
  uint32_t xseed;
  RandomBuffer(&xseed, sizeof(xseed));
  SetSeed(xseed);
}


//==========================================================================
//
//  BJPRNG::Save
//
//==========================================================================
void BJPRNG::Save (outputfile &SaveFile) {
  SaveFile << a << b << c << d;
}


//==========================================================================
//
//  BJPRNG::Load
//
//==========================================================================
void BJPRNG::Load (inputfile &SaveFile) {
  SaveFile >> a >> b >> c >> d;
}


// ////////////////////////////////////////////////////////////////////////// //
// femath
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  femath::WeightedRand
//
//==========================================================================
int femath::WeightedRand (sLong *Possibility, sLong TotalPossibility) {
  IvanAssert(TotalPossibility > 0);
  sLong Rand = RAND_N(TotalPossibility), PartialSum = 0;
  for (int c = 0; ; ++c) {
    PartialSum += Possibility[c];
    if (PartialSum > Rand) return c;
  }
}


//==========================================================================
//
//  femath::WeightedRand
//
//==========================================================================
int femath::WeightedRand (const std::vector<sLong> &Possibility, sLong TotalPossibility) {
  IvanAssert(TotalPossibility > 0);
  sLong Rand = RAND_N(TotalPossibility), PartialSum = 0;
  for (int c = 0;; ++c) {
    PartialSum += Possibility[c];
    if (PartialSum > Rand) return c;
  }
}


//==========================================================================
//
//  femath::CalculateAngle
//
//==========================================================================
double femath::CalculateAngle (v2 Direction) {
  if (Direction.X < 0) return atan(double(Direction.Y)/Direction.X)+FPI;
  if (Direction.X > 0) {
    if (Direction.Y < 0) return atan(double(Direction.Y)/Direction.X)+2*FPI;
    return atan(double(Direction.Y)/Direction.X);
  }
  if (Direction.Y < 0) return 3*FPI/2;
  if (Direction.Y > 0) return FPI/2;
  ABORT("Illegal direction (0, 0) passed to femath::CalculateAngle()!");
  return 0;
}


//==========================================================================
//
//  femath::CalculateEnvironmentRectangle
//
//==========================================================================
void femath::CalculateEnvironmentRectangle (rect &Rect, const rect &MotherRect,
                                            v2 Origo, int Radius)
{
  Rect.X1 = Origo.X - Radius;
  Rect.Y1 = Origo.Y - Radius;
  Rect.X2 = Origo.X + Radius;
  Rect.Y2 = Origo.Y + Radius;
  if (Rect.X1 < MotherRect.X1) Rect.X1 = MotherRect.X1;
  if (Rect.Y1 < MotherRect.Y1) Rect.Y1 = MotherRect.Y1;
  if (Rect.X2 > MotherRect.X2) Rect.X2 = MotherRect.X2;
  if (Rect.Y2 > MotherRect.Y2) Rect.Y2 = MotherRect.Y2;
}


//==========================================================================
//
//  femath::Clip
//
//==========================================================================
truth femath::Clip (int &SourceX, int &SourceY, int &DestX, int &DestY, int &Width, int &Height,
                    int XSize, int YSize, int DestXSize, int DestYSize)
{
  /* This sentence is usually true */
  if (SourceX >= 0 && SourceY >= 0 && DestX >= 0 && DestY >= 0 &&
      SourceX + Width <= XSize && SourceY + Height <= YSize &&
      DestX + Width <= DestXSize && DestY + Height <= DestYSize)
  {
    return true;
  }
  if (SourceX < 0) {
    Width += SourceX;
    DestX -= SourceX;
    SourceX = 0;
  }
  if (SourceY < 0) {
    Height += SourceY;
    DestY -= SourceY;
    SourceY = 0;
  }
  if (DestX < 0) {
    Width += DestX;
    SourceX -= DestX;
    DestX = 0;
  }
  if (DestY < 0) {
    Height += DestY;
    SourceY -= DestY;
    DestY = 0;
  }
  if (SourceX + Width > XSize) Width = XSize - SourceX;
  if (SourceY + Height > YSize) Height = YSize - SourceY;
  if (DestX + Width > DestXSize) Width = DestXSize - DestX;
  if (DestY + Height > DestYSize) Height = DestYSize - DestY;
  return (Width > 0 && Height > 0);
}


//==========================================================================
//
//  ReadData
//
//==========================================================================
void ReadData (interval &I, TextInput &SaveFile) {
  I.Min = SaveFile.ReadNumber(HIGHEST, true);
  festring Word;
  SaveFile.ReadWord(Word);
  if (Word == ";" || Word == ",") {
    I.Max = I.Min;
  } else if (Word == ":") {
    I.Max = Max(SaveFile.ReadNumber(), I.Min);
  } else {
    ABORT("Odd interval terminator %s detected, file %s line %d!", Word.CStr(),
          SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
  }
}


//==========================================================================
//
//  ReadData
//
//==========================================================================
void ReadData (region &R, TextInput &SaveFile) {
  ReadData(R.X, SaveFile);
  ReadData(R.Y, SaveFile);
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const interval &I) {
  //SaveFile.Write(reinterpret_cast<cchar *>(&I), sizeof(I));
  SaveFile << I.Min << I.Max;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, interval &I) {
  //SaveFile.Read(reinterpret_cast<char *>(&I), sizeof(I));
  SaveFile >> I.Min >> I.Max;
  return SaveFile;
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const region &R) {
  //SaveFile.Write(reinterpret_cast<cchar *>(&R), sizeof(R));
  SaveFile << R.X << R.Y;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, region &R) {
  //SaveFile.Read(reinterpret_cast<char *>(&R), sizeof(R));
  SaveFile >> R.X >> R.Y;
  return SaveFile;
}


//==========================================================================
//
//  femath::SumArray
//
//==========================================================================
sLong femath::SumArray (const fearray<sLong> &Vector) {
  sLong Sum = 0;
  for (uInt c = 0; c < Vector.Size; ++c) Sum += Vector.Data[c];
  return Sum;
}


//==========================================================================
//
//  femath::GenerateFractalMap
//
//==========================================================================
void femath::GenerateFractalMap (int **Map, int Side, int StartStep, int Randomness) {
  IvanAssert(Side > 1);
  cint Limit = Side-1;
  Map[0][0] = 0;
  Map[0][Limit] = 0;
  Map[Limit][0] = 0;
  Map[Limit][Limit] = 0;
  for (int Step = StartStep, HalfStep = Step>>1; HalfStep;
    Step = HalfStep, HalfStep>>=1, Randomness = ((Randomness<<3)-Randomness)>>3)
  {
    int x, y, RandMod = (Randomness<<1)+1;

    for (x = HalfStep; x < Side; x += Step) {
      for (y = HalfStep; y < Side; y += Step) {
        Map[x][y] =
          ((Map[x-HalfStep][y-HalfStep]+
            Map[x-HalfStep][y+HalfStep]+
            Map[x+HalfStep][y-HalfStep]+
            Map[x+HalfStep][y+HalfStep])>>2)-Randomness+RAND_N(RandMod);
      }
    }

    for (x = HalfStep; x < Side; x += Step) {
      for (y = 0; y < Side; y += Step) {
        int HeightSum = Map[x-HalfStep][y]+Map[x+HalfStep][y];
        int Neighbours = 2;
        if (y) {
          HeightSum += Map[x][y-HalfStep];
          ++Neighbours;
        }
        if (y != Limit) {
          HeightSum += Map[x][y+HalfStep];
          ++Neighbours;
        }
        if (Neighbours == 4) HeightSum >>= 2; else HeightSum /= Neighbours;
        Map[x][y] = HeightSum-Randomness+RAND_N(RandMod);
      }
    }

    for (x = 0; x < Side; x += Step) {
      for (y = HalfStep; y < Side; y += Step) {
        int HeightSum = Map[x][y-HalfStep]+Map[x][y+HalfStep];
        int Neighbours = 2;
        if (x) {
          HeightSum += Map[x-HalfStep][y];
          ++Neighbours;
        }
        if (x != Limit) {
          HeightSum += Map[x+HalfStep][y];
          ++Neighbours;
        }
        if (Neighbours == 4) HeightSum >>= 2; else HeightSum /= Neighbours;
        Map[x][y] = HeightSum-Randomness+RAND_N(RandMod);
      }
    }
  }
}
