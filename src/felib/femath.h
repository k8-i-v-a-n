/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_FEMATH_H__
#define __FELIB_FEMATH_H__

#include "felibdef.h"
#include <string.h>
#include <vector>
#include <cmath>

#include "v2.h"
#include "rect.h"

// use PCG32 instead of Bob Jenkins' PRNG?
// there is no reason to do it, BJPRNG is really good.
// both generators have the same-sized internal state.
// the one advantage of PCG32 is streams, which we never using anyway.
// another advandage of PCG32 is the ability to fast-forward and rewind. not used too.
//#define FE_USE_PCG32

// always use uniform generator? this makes `RAND_N()` slower, but better.
// let's try, why not?
#define FE_USE_UNIFORM_PRNG

#ifdef FE_USE_PCG32
# define PRNG  PCG32
#else
# define PRNG  BJPRNG
#endif


class outputfile;
class inputfile;


// using of `RAND` should be removed from the code base.
// this is mostly because it is immediately truncated with modulo anyway.
//#define RAND        femath::Rand
#define RANDU32     femath::RandU32
#define RAND_N      femath::RandN
// inclusive
#define RAND_BETWEEN(min_,max_)  femath::RandBetween((min_), (max_))
#ifdef FE_USE_UNIFORM_PRNG
# define RAND_2      femath::RandN(2)
# define RAND_4      femath::RandN(4)
# define RAND_8      femath::RandN(8)
# define RAND_16     femath::RandN(16)
# define RAND_32     femath::RandN(32)
# define RAND_64     femath::RandN(64)
# define RAND_128    femath::RandN(128)
# define RAND_256    femath::RandN(256)
#else
# define RAND_2      ((int32_t)(femath::RandU32()&1))
# define RAND_4      ((int32_t)(femath::RandU32()&3))
# define RAND_8      ((int32_t)(femath::RandU32()&7))
# define RAND_16     ((int32_t)(femath::RandU32()&15))
# define RAND_32     ((int32_t)(femath::RandU32()&31))
# define RAND_64     ((int32_t)(femath::RandU32()&63))
# define RAND_128    ((int32_t)(femath::RandU32()&127))
# define RAND_256    ((int32_t)(femath::RandU32()&255))
#endif
// used mostly to generate monsters, and for science talks
#define RAND_GOOD   femath::RandGood


// "non-gameplay"
//#define NG_RAND       ngprng.I32 -- should never be used
#define NG_RAND_U32       ngprng.U32
#define NG_RAND_N         ngprng.I32N
#define NG_RAND_2         ((int32_t)(ngprng.U32()&1))
#define NG_RAND_4         ((int32_t)(ngprng.U32()&3))
#define NG_RAND_8         ((int32_t)(ngprng.U32()&7))
#define NG_RAND_16        ((int32_t)(ngprng.U32()&15))
#define NG_RAND_32        ((int32_t)(ngprng.U32()&31))
#define NG_RAND_64        ((int32_t)(ngprng.U32()&63))
#define NG_RAND_128       ((int32_t)(ngprng.U32()&127))
#define NG_RAND_256       ((int32_t)(ngprng.U32()&255))
#define NG_RAND_GOOD(N_)  I32Uniform(ngprng, (N_))

//#define X_RAND(prng_)       ((prng_).I32()) -- should never be used
#define X_RAND_N(prng_,N_)      ((prng_).I32N(N_))
#define X_RAND_2(prng_)         ((int32_t)((prng_).U32()&1))
#define X_RAND_4(prng_)         ((int32_t)((prng_).U32()&3))
#define X_RAND_8(prng_)         ((int32_t)((prng_).U32()&7))
#define X_RAND_16(prng_)        ((int32_t)((prng_).U32()&15))
#define X_RAND_32(prng_)        ((int32_t)((prng_).U32()&31))
#define X_RAND_64(prng_)        ((int32_t)((prng_).U32()&63))
#define X_RAND_128(prng_)       ((int32_t)((prng_).U32()&127))
#define X_RAND_256(prng_)       ((int32_t)((prng_).U32()&255))
#define X_RAND_GOOD(prng_,N_)   I32Uniform((prng_), (N_))


// ////////////////////////////////////////////////////////////////////////// //
// hash 32-but number

static FORCE_INLINE uint32_t HashU32 (uint32_t a) {
  a -= (a<<6);
  a ^= (a>>17);
  a -= (a<<9);
  a ^= (a<<4);
  a -= (a<<3);
  a ^= (a<<10);
  a ^= (a>>15);
  return a;
}


// ////////////////////////////////////////////////////////////////////////// //
// uses independent PRNG
void RandomBuffer (void *buf, size_t bufsize);


// ////////////////////////////////////////////////////////////////////////// //
// *Really* minimal PCG32 code / (c) 2014 M.E. O'Neill / pcg-random.org
// Licensed under Apache License 2.0 (NO WARRANTY, etc. see website)
class PCG32 {
private:
  uint64_t state;
  uint64_t stream;

public:
  inline PCG32 () noexcept { SetSeed(0x29a, 0); }
  inline PCG32 (uint64_t astate, uint64_t astream=0) noexcept { SetSeed(astate, astream); }
  inline PCG32 (const PCG32 &other) noexcept { state = other.state; stream = other.stream; }

  inline PCG32 &operator = (const PCG32 &src) noexcept { state = src.state; stream = src.stream; return *this; }

  inline void SetSeed (uint64_t astate, uint64_t astream=0) noexcept {
    state = astate; stream = (astream << 1) | 1;
  }

  inline uint64_t GetBaseSeed () const noexcept { return state; }
  inline uint64_t GetStream () const noexcept { return (stream >> 1); }

  inline void SetBaseSeed (uint64_t aseed) noexcept { state = aseed; }
  inline void SetStream (uint64_t astream) noexcept { stream = (astream << 1) | 1; }

  void RandomSeed (uint32_t astream=0) noexcept;

  inline uint32_t U32 () noexcept {
    const uint64_t oldstate = this->state;
    // advance internal state
    this->state = oldstate*6364136223846793005ULL+this->stream;
    // calculate output function (XSH RR), uses old state for max ILP
    const uint32_t xorshifted = ((oldstate>>18u)^oldstate)>>27u;
    const uint32_t rot = oldstate>>59u;
    return (xorshifted>>rot)|(xorshifted<<((-rot)&31)); //k8: UB(?), but i don't care
  }

  // never negative, non-uniform
  //inline int32_t I32 () noexcept { return (int32_t)(U32()&0x7FFFFFFFu); }

  // non-uniform, `[0..N)`; `N` MUST NOT be negative!
  // this is basically the same as `%N`, only faster, and allows `N` to be zero.
  inline int32_t I32N (int32_t N) noexcept {
    if (N > 1) {
      return (int32_t)(((uint64_t)U32() * (uint32_t)N) >> 32);
    } else {
      return 0;
    }
  }

  void Save (outputfile &SaveFile);
  void Load (inputfile &SaveFile);
};


// ////////////////////////////////////////////////////////////////////////// //
// see http://burtleburtle.net/bob/rand/smallprng.html

// Bob Jenkins says that third rotation improves avalanche.
// BJPRNG is good enough with two rotations, but why not?
// or, another question: why yes? Bob himself is happy with
// two rotations, so should be i. it's not *that* important
// for IVAN anyway, and it seems that most people are using
// 2-rotation variant (including Bob himself, and the author
// of PractRand). there is no reson to try to outsmart them.
//#define BJPRNG_IMPROVED

// any decent compiler should immediately recognize this as `rol` instruction.
#define BJ_ROL(xx_,kk_)  (((xx_)<<(kk_))|((xx_)>>(32-(kk_))))

class BJPRNG {
private:
  uint32_t a, b, c, d;

public:
  inline BJPRNG () noexcept { SetSeed(0x29a); }
  inline BJPRNG (uint32_t astate) noexcept { SetSeed(astate); }
  inline BJPRNG (const BJPRNG &other) noexcept { a = other.a; b = other.b; c = other.c; d = other.c; }

  inline BJPRNG &operator = (const BJPRNG &other) noexcept { a = other.a; b = other.b; c = other.c; d = other.c; return *this; }

  inline void SetSeed (uint32_t astate) noexcept {
    a = 0xf1ea5eedU; b = c = d = astate;
    for (int i = 0; i < 20; i += 1) (void)U32();
  }

  void RandomSeed () noexcept;

  inline uint32_t U32 () noexcept {
    const uint32_t // wow, what a wonderful hack!
    #ifdef BJPRNG_IMPROVED
      e = a - BJ_ROL(b, 23);
      a = b ^ BJ_ROL(c, 16);
      b = c + BJ_ROL(d, 11);
    #else
      e = a - BJ_ROL(b, 27);
      a = b ^ BJ_ROL(c, 17);
      b = c + d;
    #endif
    c = d + e;
    d = e + a;
    return d;
  }

  // never negative, non-uniform
  //inline int32_t I32 () noexcept { return (int32_t)(U32()&0x7FFFFFFFu); }

  // non-uniform, `[0..N)`; `N` MUST NOT be negative!
  // this is basically the same as `%N`, only faster, and allows `N` to be zero.
  inline int32_t I32N (int32_t N) noexcept {
    if (N > 1) {
      return (int32_t)(((uint64_t)U32() * (uint32_t)N) >> 32);
    } else {
      return 0;
    }
  }

  void Save (outputfile &SaveFile);
  void Load (inputfile &SaveFile);
};


// ////////////////////////////////////////////////////////////////////////// //
// see https://www.pcg-random.org/posts/bounded-rands.html for various debiased methods.

// generate random number in `[0..N)` range.
// `N` MUST NOT be negative!
// NOTE: we could also use debiased multiplication method,
//       which is slightly faster than division. but meh.
template <class TPRNG> int32_t I32Uniform (TPRNG &prng, int32_t N) {
  //IvanAssert(N > 0);
  if (N <= 1) return 0;
  /*
  #if 0
  // calculate `2**32 % N` (this is, of course, UB by idiotic standards. who fuckin' cares?)
  const uint32_t t = (uint32_t)(-N) % (uint32_t)N;
  for (;;) {
    const uint32_t r = prng.U32();
    if (r >= t) return (r % (uint32_t)N);
  }
  #else
  // this is faster, but expects fast `__builtin_clz()`.
  // it basically throws away everything that is out of POT range.
  // this should work with any good PRNG (which doesn't have bias in low bits,
  // but your PRNG *should* be like that anyway).
  uint32_t mask = ~(uint32_t)0;
  N -= 1;
  mask >>= __builtin_clz((uint32_t)N | 1);
  uint32_t x;
  do { x = prng.U32() & mask; } while (x > N);
  return x;
  #endif
  */
  // this looks like a winner at the link i provided above.
  // so let's simply trust the author, and use it.
  // don't ask me, i only copypasted it here.
  // despite using 64-bit multiplications, the compiler
  // should have enough hints to see that 32x32->64 mul is suffice.
  uint64_t m = (uint64_t)prng.U32() * (uint32_t)N;
  uint32_t l = (uint32_t)m;
  if (l < (uint32_t)N) {
    uint32_t t = (uint32_t)(-N);
    if (t >= (uint32_t)N) {
      t -= (uint32_t)N;
      if (t >= (uint32_t)N) t %= (uint32_t)N;
    }
    while (l < t) {
      m = (uint64_t)prng.U32() * (uint32_t)N;
      l = (uint32_t)m;
    }
  }
  #if 1
  if ((uint32_t)(m >> 32) >= (uint32_t)N) {
    __builtin_trap();
  }
  #endif
  return (uint32_t)(m >> 32);
}


// ////////////////////////////////////////////////////////////////////////// //
class TextInput;
template <class type> struct fearray;


// non-game-related PRNG.
// use for anything that is not related to gameplay, like sparkles, flame, etc.
// ALWAYS! manually init with `ngprng.RandomSeed()`!
extern PRNG ngprng;


class femath {
protected:
  static PRNG prng;

/*
public:
  friend class SeedSaviour;
  class SeedSaviour {
    friend class femath;
  private:
    PRNG saved;
  public:
    inline SeedSaviour () noexcept { saved = femath::prng; }
    inline ~SeedSaviour () noexcept { femath::prng = saved; }
    SeedSaviour &operator = (const SeedSaviour &) = delete;
  };
*/

public:
  //static inline PRNG GetPRNGCopy () noexcept { return prng; }
  //static inline void SetPRNGFromCopy (const PRNG &src) noexcept { prng = src; }

  // seed PRNG with some random initial seed.
  // not very strong, but works for us.
  static inline void RandSeed () noexcept { prng.RandomSeed(); }

  //static inline void SetSeed (feuLong seed) noexcept { prng.SetSeed(seed); }

  //static inline sLong RandN (sLong N) noexcept { return (sLong)((double)N*Rand()/0x80000000); }
  //static inline sLong RandGood (sLong N) noexcept { return (sLong)((double)N*Rand()/0x80000000); }

  // used only once, lol.
  static FORCE_INLINE uint32_t RandU32 () noexcept { return prng.U32(); }

  // never negative, non-uniform.
  // this is slightly biased. sigh.
  //static inline sLong Rand () noexcept { return prng.I32(); }

  // non-uniform, `[0..N)`
  static FORCE_INLINE sLong RandN (sLong N) noexcept {
    #ifdef FE_USE_UNIFORM_PRNG
    return I32Uniform(prng, N);
    #else
    return prng.I32N(N);
    #endif
  }
  // slower than `RandN()`, but uniform
  static FORCE_INLINE sLong RandGood (sLong N) noexcept { return I32Uniform(prng, N); }

  // inclusive
  static inline int RandBetween (int min, int max) noexcept {
    return (max > min ? min + RandN(max - min + 1) : min);
  }

  static int WeightedRand (sLong *Possibility, sLong TotalPossibility);
  static int WeightedRand (const std::vector<sLong> &Possibility, sLong TotalPossibility);

  static int LoopRoll (int ContinueChance, int Max);

  static void GenerateFractalMap (int **Map, int Side, int StartStep, int Randomness);

  // unrelated to random numbers
  static double CalculateAngle (v2 Direction);

  static void CalculateEnvironmentRectangle (rect &Rect, const rect &MotherRect,
                                             v2 Origo, int Radius);
  static truth Clip (int &SourceX, int &SourceY, int &DestX, int &DestY,
                     int &Width, int &Height,
                     int XSize, int YSize, int DestXSize, int DestYSize);

  static sLong SumArray (const fearray<sLong> &Vector);

  // related again ;-)
  static void SavePRNG (outputfile &SaveFile) { prng.Save(SaveFile); }
  static void LoadPRNG (inputfile &SaveFile) { prng.Load(SaveFile); }
};


struct interval {
  //sLong Randomize () const { return Min < Max ? Min + RAND() % (Max - Min + 1) : Min; }
  inline sLong Randomize () const { return Min + (Min < Max ? RAND_N(Max - Min + 1) : 0); }
  sLong Min;
  sLong Max;
};


struct region {
  v2 Randomize () const { return v2(X.Randomize(), Y.Randomize()); }
  interval X;
  interval Y;
};


void ReadData (interval &I, TextInput &SaveFile);
void ReadData (region &R, TextInput &SaveFile);

outputfile &operator << (outputfile &SaveFile, const interval &I);
inputfile &operator >> (inputfile &SaveFile, interval &I);
outputfile &operator << (outputfile &SaveFile, const region &R);
inputfile &operator >> (inputfile &SaveFile, region &R);


// ////////////////////////////////////////////////////////////////////////// //
/*
flags:
  SKIP_FIRST -- skip first line point
  SKIP_LAST  -- skip last line point
  LINE_BOTH_DIRS -- trace line in both directions
    tracing in backwards direction will swap meaning of skips
  ALLOW_END_FAILURE -- is reaching end point a failure?
    if not set, reaching end point is success

`DoLine` is used to trace a line between two points, calling check
handler on each step. `controller` is the class with the static member:

  static truth Handler (int x, int y);

the handler should return `false` to stop the tracing, and return failure
(i.e. `false`). note that is `SKIP_FIRST` is not set, the handler will
be called for the first line point, but its return value will be ignored.

k8: i don't like that we cannot pass any user values to handler, and have
    to use static vars instead. this seems to be the bad design.
    so i added `MapMathEx` class (see below).
*/
template <class controller> class mapmath {
public:
  static truth DoLine (int X1, int Y1, int X2, int Y2, int Flags=0);
  static void DoArea ();
  static void DoQuadriArea (int OrigoX, int OrigoY, int RadiusSquare, int XSize, int YSize);

private:
  static truth DoLineOneDir (int X1, int Y1, int X2, int Y2, int Flags=0);
};


template <class controller>
inline truth mapmath<controller>::DoLine (int X1, int Y1, int X2, int Y2, int Flags) {
  truth res = DoLineOneDir(X1, Y1, X2, Y2, Flags);
  if (!res && (Flags&LINE_BOTH_DIRS) != 0) {
    if (Flags&SKIP_FIRST) Flags = (Flags&~SKIP_FIRST)|SKIP_LAST;
    res = DoLineOneDir(X2, Y2, X1, Y1, Flags);
  }
  return res;
}


template <class controller>
inline truth mapmath<controller>::DoLineOneDir (int X1, int Y1, int X2, int Y2, int Flags) {
  if (!(Flags & SKIP_FIRST)) {
    (void)controller::Handler(X1, Y1);
  }
  cint DeltaX = abs(X2 - X1);
  cint DeltaY = abs(Y2 - Y1);
  cint DoubleDeltaX = DeltaX * 2;
  cint DoubleDeltaY = DeltaY * 2;
  cint XChange = (X1 < X2 ? 1 : -1);
  cint YChange = (Y1 < Y2 ? 1 : -1);
  sLong x = X1, y = Y1;
  if (DeltaX >= DeltaY) {
    cint End = X2;
    sLong c = DeltaX;
    while (x != End) {
      x += XChange;
      c += DoubleDeltaY;
      if (c >= DoubleDeltaX) {
        c -= DoubleDeltaX;
        y += YChange;
      }
      if ((Flags&SKIP_LAST) && x == X2 && y == Y2) break;
      if (!controller::Handler(x, y)) {
        return (x == End && !(Flags & ALLOW_END_FAILURE));
      }
    }
  } else {
    cint End = Y2;
    sLong c = DeltaY;
    while (y != End) {
      y += YChange;
      c += DoubleDeltaX;
      if (c >= DoubleDeltaY) {
        c -= DoubleDeltaY;
        x += XChange;
      }
      if ((Flags&SKIP_LAST) && x == X2 && y == Y2) break;
      if (!controller::Handler(x, y)) {
        return (y == End && !(Flags & ALLOW_END_FAILURE));
      }
    }
  }
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
/*
flags:
  SKIP_FIRST -- skip first line point
  SKIP_LAST  -- skip last line point
  LINE_BOTH_DIRS -- trace line in both directions
    tracing in backwards direction will swap meaning of skips
  ALLOW_END_FAILURE -- is reaching end point a failure?
    if not set, reaching end point is success

`DoLine` is used to trace a line between two points, calling check
handler on each step. `controller` is the class with the method:

  int Handler (int x, int y);

the handler should return non-zero to stop the tracing, and return that
value.
*/
template <class controller> class MapMathEx {
public:
  static int DoLine (controller &ctl, int X1, int Y1, int X2, int Y2, int Flags=0);

private:
  static int DoLineOneDir (controller &ctl, int X1, int Y1, int X2, int Y2, int Flags=0);
};


template <class controller>
inline int MapMathEx<controller>::DoLine (controller &ctl, int X1, int Y1, int X2, int Y2,
                                          int Flags)
{
  int res = DoLineOneDir(ctl, X1, Y1, X2, Y2, Flags);
  if (res == 0 && (Flags&LINE_BOTH_DIRS) != 0) {
    if ((Flags&(SKIP_FIRST|SKIP_LAST)) != (SKIP_FIRST|SKIP_LAST)) {
      if (Flags&SKIP_FIRST) Flags = (Flags&~SKIP_FIRST)|SKIP_LAST;
      else if (Flags&SKIP_LAST) Flags = (Flags&~SKIP_LAST)|SKIP_FIRST;
    }
    res = DoLineOneDir(ctl, X2, Y2, X1, Y1, Flags);
  }
  return res;
}


template <class controller>
inline int MapMathEx<controller>::DoLineOneDir (controller &ctl, int X1, int Y1, int X2, int Y2,
                                                int Flags)
{
  int hres = 0;
  if ((Flags & SKIP_FIRST) == 0) {
    hres = ctl.Handler(X1, Y1);
    if (hres != 0) return hres;
  }
  cint DeltaX = abs(X2 - X1);
  cint DeltaY = abs(Y2 - Y1);
  cint DoubleDeltaX = DeltaX * 2;
  cint DoubleDeltaY = DeltaY * 2;
  cint XChange = (X1 < X2 ? 1 : -1);
  cint YChange = (Y1 < Y2 ? 1 : -1);
  sLong x = X1, y = Y1;
  if (DeltaX >= DeltaY) {
    cint End = X2;
    sLong c = DeltaX;
    while (x != End) {
      x += XChange;
      c += DoubleDeltaY;
      if (c >= DoubleDeltaX) {
        c -= DoubleDeltaX;
        y += YChange;
      }
      if ((Flags&SKIP_LAST) != 0 && x == X2 && y == Y2) break;
      hres = ctl.Handler(x, y);
      if (hres != 0) return hres;
    }
  } else {
    cint End = Y2;
    sLong c = DeltaY;
    while (y != End) {
      y += YChange;
      c += DoubleDeltaX;
      if (c >= DoubleDeltaY) {
        c -= DoubleDeltaY;
        x += XChange;
      }
      if ((Flags&SKIP_LAST) != 0 && x == X2 && y == Y2) break;
      hres = ctl.Handler(x, y);
      if (hres != 0) return hres;
    }
  }
  return hres;
}


// ////////////////////////////////////////////////////////////////////////// //
struct basequadricontroller {
  static cint OrigoDeltaX[4];
  static cint OrigoDeltaY[4];
  static int OrigoX, OrigoY;
  static int StartX, StartY;
  static int XSize, YSize;
  static int RadiusSquare;
  static truth SectorCompletelyClear;
};


template <class controller>
struct quadricontroller : public basequadricontroller {
  static truth Handler (int, int);
  static int GetStartX (int I) {
    SectorCompletelyClear = true;
    return StartX = (OrigoX<<1)+OrigoDeltaX[I];
  }
  static int GetStartY (int I) {
    return StartY = (OrigoY<<1)+OrigoDeltaY[I];
  }
};


template <class controller> truth quadricontroller<controller>::Handler (int x, int y) {
  cint HalfX = x>>1, HalfY = y>>1;
  if (HalfX >= 0 && HalfY >= 0 && HalfX < XSize && HalfY < YSize) {
    feuLong &SquareTick = controller::GetTickReference(HalfX, HalfY);
    cint SquarePartIndex = (x&1)+((y&1)<<1);
    culong Mask = SquarePartTickMask[SquarePartIndex];
    if ((SquareTick & Mask) < controller::ShiftedTick[SquarePartIndex]) {
      SquareTick = (SquareTick & ~Mask) | controller::ShiftedQuadriTick[SquarePartIndex];
      int DeltaX = OrigoX-HalfX, DeltaY = OrigoY-HalfY;
      if (DeltaX*DeltaX+DeltaY*DeltaY <= RadiusSquare) {
        if (SectorCompletelyClear) {
          if (controller::Handler(x, y)) return true;
          SectorCompletelyClear = false;
        } else {
          return mapmath<controller>::DoLine(StartX, StartY, x, y,
                                             SKIP_FIRST|ALLOW_END_FAILURE|LINE_BOTH_DIRS);
        }
      }
    }
  }
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
const cint ChangeXArray[4][3] = {
  { -1,  0, -1 },
  {  0,  1,  1 },
  { -1, -1,  0 },
  {  1,  0,  1 }
};

const cint ChangeYArray[4][3] = {
  { -1, -1,  0 },
  { -1, -1,  0 },
  {  0,  1,  1 },
  {  0,  1,  1 }
};


template <class controller> inline void mapmath<controller>::DoArea () {
  int Buffer[2][2048];
  int *OldStack = Buffer[0];
  int *NewStack = Buffer[1];
  for (int c1 = 0; c1 < 4; ++c1) {
    cint *ChangeX = ChangeXArray[c1], *ChangeY = ChangeYArray[c1];
    int OldStackPos = 0, NewStackPos = 0;
    int StartX = controller::GetStartX(c1);
    int StartY = controller::GetStartY(c1);
    for (int c2 = 0; c2 < 3; ++c2) {
      OldStack[OldStackPos] = StartX+ChangeX[c2];
      OldStack[OldStackPos + 1] = StartY+ChangeY[c2];
      OldStackPos += 2;
    }
    while (OldStackPos) {
      while (OldStackPos) {
        OldStackPos -= 2;
        cint X = OldStack[OldStackPos], Y = OldStack[OldStackPos+1];
        if (controller::Handler(X, Y)) {
          for (int c2 = 0; c2 < 3; ++c2) {
            NewStack[NewStackPos] = X+ChangeX[c2];
            NewStack[NewStackPos+1] = Y+ChangeY[c2];
            NewStackPos += 2;
          }
        }
      }
      OldStackPos = NewStackPos;
      NewStackPos = 0;
      int *T = OldStack;
      OldStack = NewStack;
      NewStack = T;
    }
  }
}


template <class controller>
inline void mapmath<controller>::DoQuadriArea (int OrigoX, int OrigoY, int RadiusSquare,
                                               int XSize, int YSize)
{
  basequadricontroller::OrigoX = OrigoX;
  basequadricontroller::OrigoY = OrigoY;
  basequadricontroller::RadiusSquare = RadiusSquare;
  basequadricontroller::XSize = XSize;
  basequadricontroller::YSize = YSize;
  for (int c = 0; c < 4; ++c) {
    controller::Handler((OrigoX * 2) + basequadricontroller::OrigoDeltaX[c],
                        (OrigoY * 2) + basequadricontroller::OrigoDeltaY[c]);
  }
  mapmath<quadricontroller<controller> >::DoArea();
}


// ////////////////////////////////////////////////////////////////////////// //
/* Chance for n < Max to be returned is (1-CC)*CC^n, for n == Max chance is CC^n. */
inline int femath::LoopRoll (int ContinueChance, int Max) {
  int R;
  for (R = 0; RAND_N(100) < ContinueChance && R < Max; ++R) {}
  return R;
}


template <class type, class predicate> type *&ListFind (type *&Start, predicate Predicate) {
  type **E;
  for(E = &Start; *E && !Predicate(*E); E = &(*E)->Next);
  return *E;
}


template <class type> struct pointercomparer {
  pointercomparer (const type *Element) : Element(Element) { }
  truth operator()(const type *E) const { return E == Element; }
  const type *Element;
};


#endif
