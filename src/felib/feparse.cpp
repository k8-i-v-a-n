/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include "felibdef.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cctype>

#include "feparse.h"
#include "femath.h"


//==========================================================================
//
//  isBlank
//
//==========================================================================
static FORCE_INLINE bool isBlank (char ch) {
  return ((uint8_t)ch <= 32);
}


//==========================================================================
//
//  digitInBase
//
//==========================================================================
static int digitInBase (char ch, int base) {
  int n;
       if (ch >= '0' && ch <= '9') n = ch - '0';
  else if (ch >= 'A' && ch <= 'Z') n = ch - 'A' + 10;
  else if (ch >= 'a' && ch <= 'z') n = ch - 'a' + 10;
  else return -1;
  if (n < 0 || n >= base) return -1;
  return n;
}


//==========================================================================
//
//  xxInt
//
//==========================================================================
static void xxInt (cfestring &str, int *val, bool *ok) {
  *ok = false; *val = 0;
  int len = str.GetSize();
  int pos = 0;
  while (pos != len && isBlank(str[pos])) pos += 1;
  if (pos != len) {
    bool neg = false;
    if (str[pos] == '-') { neg = true; pos += 1; }
    else if (str[pos] == '+') { neg = false; pos += 1; }
    if (pos != len && digitInBase(str[pos], 10) >= 0) {
      int n = 0;
      while (pos != len && digitInBase(str[pos], 10) >= 0) {
        int tt = n;
        n = n * 10 - digitInBase(str[pos], 10);
        if (n > tt || (n == 0 && tt != 0)) return; // overflow
        pos += 1;
      }
      while (pos != len && isBlank(str[pos])) pos += 1;
      if (pos != len) return;
      if (!neg) {
        if ((uint32_t)n == 0x80000000u) return;
        n = -n;
      }
      *ok = true; *val = n;
    }
  }
}


//==========================================================================
//
//  xxCompare
//
//==========================================================================
static int xxCompare (cfestring &op0, cfestring &op1, bool asInt) {
  if (asInt) {
    int i0, i1;
    bool ok0, ok1;
    xxInt(op0, &i0, &ok0);
    if (ok0) {
      xxInt(op1, &i1, &ok1);
      if (ok1) {
        return (i0 < i1 ? -1 : i0 > i1 ? 1 : 0);
      }
    }
  }
  if (op0 < op1) return -1;
  if (op0 > op1) return  1;
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// InputFileSaved
// ////////////////////////////////////////////////////////////////////////// //

struct InputFileSaved {
  friend TextInput;

private:
  TextInput *ifile = nullptr;
  void *svbuf = nullptr;
  int mCharBuf[TextInput::MaxUngetChars] = {0};
  int mCharBufPos = 0;
  int mCurrentLine = 0;
  int mTokenLine = 0;
  sLong mRealPos = 0;

private:
  InputFileSaved (TextInput *aifile) : ifile(aifile), svbuf(nullptr) {
    if (aifile) {
      memcpy(mCharBuf, aifile->mCharBuf, sizeof(mCharBuf));
      mCharBufPos = aifile->mCharBufPos;
      mCurrentLine = aifile->mCurrentLine;
      mTokenLine = aifile->mTokenLine;
      mRealPos = aifile->RealGetPos();
    }
  }

public:
  ~InputFileSaved () {
    if (ifile) {
      memcpy(ifile->mCharBuf, mCharBuf, sizeof(ifile->mCharBuf));
      ifile->mCharBufPos = mCharBufPos;
      ifile->mCurrentLine = mCurrentLine;
      ifile->mTokenLine = mTokenLine;
      ifile->RealSetPos(mRealPos);
    }
  }
};


// ////////////////////////////////////////////////////////////////////////// //
// TextFileLocation
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  TextFileLocation::Set
//
//==========================================================================
void TextFileLocation::Set (TextInput &infile) {
  fname = infile.GetFileName();
  line = infile.TokenLine();
}


////////////////////////////////////////////////////////////////////////////////
// TextInput
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  TextInput::TextInput
//
//==========================================================================
TextInput::TextInput (const valuemap *aValueMap) {
  Setup(aValueMap);
}


//==========================================================================
//
//  TextInput::~TextInput
//
//==========================================================================
TextInput::~TextInput () {
  Close();
}


//==========================================================================
//
//  TextInput::Setup
//
//==========================================================================
void TextInput::Setup (const valuemap *aValueMap) {
  ValueMap = aValueMap;
  lastWasNL = false;
  lastWordWasString = false;
  mCharBufPos = 0;
  mCurrentLine = 1;
  mTokenLine = 1;
  mNumStr = "";
  mCollectingNumStr = false;
  mAllowFloatNums = false;
}


//==========================================================================
//
//  TextInput::Close
//
//==========================================================================
void TextInput::Close () {
  lastWasNL = false;
  while (!mIfStack.empty()) mIfStack.pop();
  mCharBufPos = 0;
  mCurrentLine = 0;
  mTokenLine = 0;
  mNumStr = "";
  mCollectingNumStr = false;
  mAllowFloatNums = false;
}


//==========================================================================
//
//  TextInput::GetChar
//
//==========================================================================
int TextInput::GetChar () {
  if (mCharBufPos != 0) {
    mCharBufPos -= 1;
    return mCharBuf[mCharBufPos];
  } else {
    if (lastWasNL) { mCurrentLine += 1; lastWasNL = false; }
    int ch = RealGetChar();
    if (ch == 0) ch = ' ';
    lastWasNL = (ch == '\n');
    return (ch < 0 ? EOF : ch);
  }
}


//==========================================================================
//
//  TextInput::UngetChar
//
//==========================================================================
void TextInput::UngetChar (int ch) {
  if (ch >= 0) {
    if (mCharBufPos > MaxUngetChars) {
      Error("too many unread chars");
    }
    mCharBuf[mCharBufPos++] = ch;
  }
}


//==========================================================================
//
//  TextInput::UngetStr
//
//==========================================================================
void TextInput::UngetStr (cchar *str) {
  if (str && str[0]) {
    size_t slen = strlen(str);
    while (slen != 0) {
      slen -= 1;
      UngetChar(str[slen]);
    }
  }
}


//==========================================================================
//
//  TextInput::Eof
//
//==========================================================================
truth TextInput::Eof () {
  if (mCharBufPos > 0) return false;
  return IsRealEof();
}


//==========================================================================
//
//  TextInput::GotCharSkipComment
//
//  just read `ch`, skip possible comment; returns `ch` or -1
//
//==========================================================================
int TextInput::GotCharSkipComment (int ch, truth allowSingleLineComments) {
  if (ch < 0) ABORT("The thing that should not be");
  if (ch != '/') return ch;
  ch = GetChar();
  if (ch == EOF) return '/';

  // single-line comment?
  if (allowSingleLineComments && ch == '/') {
    while (ch != EOF && ch != '\n') ch = GetChar();
    return -1;
  }

  // multiline comment? (possibly nested)
  if (ch != '*') { UngetChar(ch); return '/'; }

  int prevch = 0, level = 1;
  for (;;) {
    ch = GetChar();
    if (ch == EOF) {
      ABORT("Unterminated comment in file %s, beginning at line %d!", GetFileName().CStr(), mTokenLine);
    }
    // close comment
    else if (prevch == '*' && ch == '/') {
      if (--level == 0) return -1;
      prevch = 0;
    }
    // open comment
    else if (prevch == '/' && ch == '*') {
      ++level;
      prevch = 0;
    }
    else {
      // other chars
      prevch = ch;
    }
  }
}


//==========================================================================
//
//  TextInput::SkipBlanks
//
//==========================================================================
void TextInput::SkipBlanks () {
  for (;;) {
    int ch = GetChar();
    if (ch == EOF) return;
    if (ch <= ' ') continue;
    ch = GotCharSkipComment(ch);
    if (ch < 0) continue;
    UngetChar(ch);
    return;
  }
}


//==========================================================================
//
//  TextInput::CountArrayItems
//
//==========================================================================
int TextInput::CountArrayItems (char echar) {
  enum { StackDepth = 256 };
  auto savedPos = InputFileSaved(this);
  char stack[StackDepth]; // end chars
  int sp = 0;
  stack[0] = echar;
  SkipBlanks();
  int ch = GetChar();
  if (ch == EOF) return -1; // oops
  if (ch == ',' || ch == ';') return -1;
  //ConLogf("COUNT: ch='%c'", ch);
  if (ch == echar) return 0;
  UngetChar(ch);
  int count = 1;
  while (sp >= 0) {
    SkipBlanks();
    ch = GetChar();
    if (ch == EOF) return -1; // oops
    // string?
    if (ch == '"' || ch == '\'') {
      echar = ch;
      while (ch != EOF) {
        ch = GetChar();
        if (ch == '\\') {
          ch = GetChar();
          if (ch == EOF) return -1; // oops
        } else if (ch == echar) {
          break;
        }
      }
      continue;
    }
    if (sp == 0 && (ch == ',' || ch == ';')) {
      SkipBlanks();
      ch = GetChar();
      if (ch == EOF) return -1;
      if (ch == ',' || ch == ';') return -1;
      //ConLogf(" oldcount=%d; ch='%c'; sp=%d", count, ch, sp);
      if (sp == 0 && ch == stack[0]) {} else ++count;
      //ConLogf("  newcount=%d; ch='%c'; sp=%d", count, ch, sp);
      //if (ch != ')' && ch != ']' && ch != '}') ++count;
    }
    // endchar?
    if (ch == stack[sp]) {
      //ConLogf(" *close; ch='%c'; sp=%d", ch, sp);
      --sp;
      continue;
    }
    // check for openings
    switch (ch) {
      case '(': echar = ')'; break;
      case '[': echar = ']'; break;
      case '{': echar = '}'; break;
      case ')': case ']': case '}': return -1; // oops
      default: echar = 0; break;
    }
    if (echar) {
      if (sp >= StackDepth-1) return -1; // oops
      //ConLogf(" *open; ch='%c'; echar='%c'; sp=%d", ch, echar, sp);
      stack[++sp] = echar;
    }
  }
  return count;
}


//==========================================================================
//
//  TextInput::FindVar
//
//==========================================================================
festring TextInput::FindVar (cfestring &name, truth *found) const {
  VarMap::const_iterator i = mVars.find(name);
  if (i != mVars.end()) {
    if (found) *found = true;
    return i->second;
  }
  if (found) *found = false;
  return CONST_S("");
}


//==========================================================================
//
//  TextInput::GetVar
//
//==========================================================================
festring TextInput::GetVar (cfestring &name, truth scvar) {
  truth found;
  if (!scvar) {
    int val; bool ok;
    xxInt(name, &val, &ok);
    if (ok) {
      festring ires;
      ires << val;
      return ires;
    }
  }
  festring res = FindVar(name, &found);
  if (!found) {
    if (mGetVar) {
      res = mGetVar(this, name, scvar);
    } else {
      Error("unknown variable: %s", name.CStr());
    }
  }
  return res;
}


//==========================================================================
//
//  TextInput::SetVar
//
//==========================================================================
void TextInput::SetVar (cfestring &name, cfestring &value) {
  mVars[name] = value;
}


//==========================================================================
//
//  TextInput::DelVar
//
// TODO: invoke callback
//
//==========================================================================
truth TextInput::DelVar (cfestring &name) {
  VarMap::iterator i = mVars.find(name);
  if (i != mVars.end()) {
    mVars.erase(i);
    return true;
  }
  return false;
}


// ////////////////////////////////////////////////////////////////////////// //
// 0: term
// 1: unary
// 2: comparisons
// 3: &&
// 4: ||
static const int maxCPrio = 4;
static const char *opers[5][7] = {
  {NULL},
  {NULL},
  {"<", ">", "<=", ">=", "==", "!=", NULL},
  {"&&", NULL},
  {"||", NULL}
};


//==========================================================================
//
//  TextInput::ReadCondition
//
//==========================================================================
festring TextInput::ReadCondition (festring &token, int prio, truth skipIt) {
  festring res, op1, opc;
  //ConLogf("IN:  prio: %d; skip: %s; [%s]", prio, (skipIt ? "t" : "o"), token.CStr());
  switch (prio) {
    case 0: // term
      if (token == "(") {
        ReadWordIntr(token, true);
        res = ReadCondition(token, maxCPrio, skipIt);
        if (token != ")") Error("')' expected");
      } else if (token == "@") {
        ReadWordIntr(token, true);
        if (!skipIt) res = GetVar(token, true);
      } else {
        res = GetVar(token, false);
      }
      ReadWordIntr(token, true);
      goto done;
      //return res;
    case 1:
      if (token == "!") {
        ReadWordIntr(token, true);
        res = ReadCondition(token, 1, skipIt);
        if (!skipIt) {
          if (res == "") res = "tan"; else res = "";
        }
      } else {
        res = ReadCondition(token, prio-1, skipIt);
      }
      goto done;
      //return res;
  }

  if (prio > 4) return res;
  res = ReadCondition(token, prio-1, skipIt);
  for (;;) {
    //ReadWordIntr(token, true);
    bool myOp = false;
    //if (token == "=") die("no assignments yet!");
    if (token == ";") {
      //ConLogf(" RET: [%s]", res.CStr());
      break;
    }
    bool asInt = false;
         if (token == "less") token = "<";
    else if (token == "great") token = ">";
    else if (token == "equ") token = "==";
    else if (token == "neq") token = "!=";
    else if (token == "lessequ") token = "<=";
    else if (token == "greatequ") token = ">=";
    else if (token == "=") { token = "=="; asInt = true; }
    else if (token == "#") { token = "!="; asInt = true; }
    else if (token == "AND") token = "&&";
    else if (token == "OR") token = "||";
    else asInt = true;
    for (int f = 0; opers[prio][f]; f++) {
      if (!strcmp(opers[prio][f], token.CStr())) { myOp = true; break; }
    }
    //ConLogf("tk: [%s]; %s", token.CStr(), (myOp ? "MY" : "skip"));
    if (!myOp) break;
    opc = token;
    ReadWordIntr(token, true);
    op1 = ReadCondition(token, prio-1, skipIt);
    //ConLogf(" prio: %d; opc=[%s]; res=[%s]; op1=[%s]", prio, opc.CStr(), res.CStr(), op1.CStr());
    switch (prio) {
      case 2: // comparisons
        if (!skipIt) {
          int cmp = xxCompare(res, op1, asInt);
          bool trr = false;
               if (opc == "==") trr = (cmp == 0);
          else if (opc == "!=") trr = (cmp != 0);
          else if (opc == "<") trr = (cmp < 0);
          else if (opc == ">") trr = (cmp > 0);
          else if (opc == "<=") trr = (cmp <= 0);
          else if (opc == ">=") trr = (cmp >= 0);
          res = (trr ? "tan" : "");
        }
        break;
      case 3: // &&
        if (opc == "&&") {
          if (!skipIt) {
            res = (res != "" && op1 != "" ? "tan" : "");
            if (res == "") skipIt = true;
          }
        }
        break;
      case 4: // ||
        if (opc == "||") {
          if (!skipIt) {
            res = (res != "" || op1 != "" ? "tan" : "");
            if (res != "") skipIt = true;
          }
        }
        break;
      default:
        Error("invalid priority");
    }
  }
done:
  //ConLogf("OUT: prio: %d; skip: %s; [%s]", prio, skipIt?"t":"o", token.CStr());
  return res;
}


//==========================================================================
//
//  TextInput::ReadWord
//
//  stack top:
//   1: processing 'then'
//   2: processing 'else'
//  -1: skiping 'then'
//  -2: skiping 'else'
//  -3: skiping whole 'if', 'then' part
//  -4: skiping whole 'if', 'else' part
//  -666: skiping '{}'
//  666: in '{}', processing
//
//==========================================================================
truth TextInput::ReadWord (festring &str, truth abortOnEOF) {
  for (;;) {
    int prc = (mIfStack.empty() ? 0 : mIfStack.top());
    if (!ReadWordIntr(str, abortOnEOF)) return false; // EOF
    if (str == "if") {
      ReadWordIntr(str, true);
      festring res = ReadCondition(str, maxCPrio, prc<0);
      if (str != ";") Error("';' expected");
      if (prc < 0) {
        // skiping
        mIfStack.push(-3);
      } else {
        mIfStack.push(res.IsEmpty() ? -1 : 1);
      }
      continue;
    }
    if (str == "else") {
      switch (prc) {
        case 1: // processing 'then'
          mIfStack.pop();
          mIfStack.push(-2);
          break;
        case -1: // skiping 'then'
          mIfStack.pop();
          mIfStack.push(2);
          break;
        case -3: // skiping whole, 'then'
          mIfStack.pop();
          mIfStack.push(-4);
          break;
        default: Error("unexpected 'else'");
      }
      continue;
    }
    if (str == "endif") {
      switch (prc) {
        case 1: // processing 'then'
        case 2: // processing 'else'
        case -1: // skiping 'then'
        case -2: // skiping 'else'
        case -3: // skiping whole, 'then'
        case -4: // skiping whole, 'else'
          mIfStack.pop();
          break;
          default: Error("unexpected 'endif'");
      }
      continue;
    }
    if (str == "{") {
      mIfStack.push(prc >= 0 ? 666 : -666);
      if (prc >= 0) return true;
      continue;
    }
    if (str == "}") {
      if (abs(prc) != 666) Error("unexpected '}'");
      mIfStack.pop();
      if (prc >= 0) return true;
      continue;
    }
    if (prc >= 0) return true;
  }
}


//==========================================================================
//
//  TextInput::ReadWord
//
//==========================================================================
festring TextInput::ReadWord (truth abortOnEOF) {
  festring ToReturn;
  ReadWord(ToReturn, abortOnEOF);
  return ToReturn;
}


//==========================================================================
//
//  TextInput::ReadWordIntr
//
//==========================================================================
truth TextInput::ReadWordIntr (festring &String, truth abortOnEOF) {
  String.Empty();
  lastWordWasString = false;
  SkipBlanks();
  mTokenLine = mCurrentLine;
  for (;;) {
    int ch = GetChar();
    if (ch == EOF) {
      if (abortOnEOF) ABORT("Unexpected end of file %s!", GetFileName().CStr());
      return false;
    }
    // identifier?
    if (isalpha(ch) || ch == '_') {
      String << (char)(ch);
      for (;;) {
        ch = GetChar();
        if (ch == EOF) break;
        if (ch != '_' && !isalpha(ch) && !isdigit(ch)) { UngetChar(ch); break; }
        String << (char)(ch);
      }
      return true;
    }
    // number?
    if (isdigit(ch)) {
      String << (char)(ch);
      bool wasdot = !mAllowFloatNums;
      bool ishex = false;
      // allow hex literals
      if (!mAllowFloatNums && ch == '0') {
        ch = GetChar();
        if (ch == 'X' || ch == 'x') {
          ishex = true;
          String << 'x';
        } else {
          UngetChar(ch);
        }
      }
      for (;;) {
        ch = GetChar();
        if (ch == EOF) break;
        if (ch == '_') continue;
        if (ch == '.') {
          if (wasdot) Error("invalid number");
          wasdot = true;
          String << '.';
          continue;
        }
        if (ishex) {
          if (isxdigit(ch)) { String << (char)(ch); continue; }
        } else {
          if (isdigit(ch)) { String << (char)(ch); continue; }
        }
        if (isalpha(ch)) Error("invalid number");
        UngetChar(ch);
        break;
      }
      return true;
    }
    // string?
    if (ch == '"') {
      lastWordWasString = true;
      for (;;) {
        ch = GetChar();
        if (ch == EOF) ABORT("Unterminated string in file %s, beginning at line %d!", GetFileName().CStr(), mTokenLine);
        if (ch == '"') return true;
        if (ch == '\\') {
          ch = GetChar();
          if (ch == EOF) ABORT("Unterminated string in file %s, beginning at line %d!", GetFileName().CStr(), mTokenLine);
          switch (ch) {
            case 't': String << '\t'; break;
            case 'n': String << '\n'; break;
            case 'r': String << '\r'; break;
            case '1': String << '\x01'; break;
            case '2': String << '\x02'; break;
            case '"': String << '"'; break;
            default: ABORT("Invalid escape in string in file %s at line %d!", GetFileName().CStr(), mTokenLine);
          }
          continue;
        }
        String << (char)ch;
      }
    }
    // punctuation
    ch = GotCharSkipComment(ch);
    if (ch < 0) continue;
    // delimiter
    String << (char)ch;
    // two-char delimiters?
    if (ch == '=' || ch == '<' || ch == '>' || ch == '!') {
      ch = GetChar();
      if (ch == '=') String << (char)ch; else UngetChar(ch);
    } else if (ch == '&' || ch == '|') {
      int c1 = GetChar();
      if (c1 == ch) String << (char)c1; else UngetChar(c1);
    } else if (ch == ':') {
      ch = GetChar();
      if (ch == '=') String << (char)ch; else UngetChar(ch);
    }
    return true;
  }
}


//==========================================================================
//
//  TextInput::ReadLetter
//
//==========================================================================
char TextInput::ReadLetter (truth abortOnEOF) {
  mTokenLine = mCurrentLine;
  for (;;) {
    int ch = GetChar();
    if (ch == EOF) {
      if (abortOnEOF) ABORT("Unexpected end of file %s!", GetFileName().CStr());
      return 0;
    }
    if (ch <= ' ') continue;
    //ch = GotCharSkipComment(ch);
    //if (ch >= 0) return ch;
    return ch;
  }
}


//==========================================================================
//
//  TextInput::ReadNumberIntr
//
//  Reads a number or a formula from inputfile. Valid values could be for
//  instance "3", "5 * 4+5", "2+Variable%4" etc.
//
//==========================================================================
template<typename numtype>
festring TextInput::ReadNumberIntr (int CallLevel, numtype *num, truth *isString,
                                    truth allowStr, truth PreserveTerminator,
                                    truth *wasCloseBrc, truth allowFloats)
{
  struct AllowFloatSaver {
    truth oldallowfloat;
    truth *var;
    AllowFloatSaver (truth *avar, truth newval) { oldallowfloat = *avar; var = avar; *avar = newval; }
    ~AllowFloatSaver () { *var = oldallowfloat; }
  };
  numtype Value = 0;
  festring Word, res;
  truth NumberCorrect = false;
  truth firstWord = true;
  if (isString) *isString = false;
  if (num) *num = 0;
  if (wasCloseBrc) *wasCloseBrc = false;
  mTokenLine = mCurrentLine;
  auto oldflt = AllowFloatSaver(&mAllowFloatNums, allowFloats);
  //ConLogf(">>> ReadNumberIntr()");
  for (;;) {
    ReadWord(Word);
    //ConLogf("  ReadNumberIntr: word='%s'", Word.CStr());
    // specials?
    if (Word == "@") {
      // variable
      if (mCollectingNumStr) mNumStr << Word;
      ReadWord(Word, true);
      if (mCollectingNumStr) mNumStr << Word;
      //ConLogf("var: [%s]", Word.CStr());
      Word = GetVar(Word, true);
      //ConLogf(" value: [%s]", Word.CStr());
      const char *s = Word.CStr();
      char *e;
      sLong l = strtoll(s, &e, 10);
      if (*e == '\0') {
        //ConLogf(" number: [%d]", l);
        Value = (numtype)l;
        NumberCorrect = true;
        continue;
      }
      if (firstWord && allowStr) {
        if (isString) *isString = true;
        return Word;
      } else {
        ABORT("Number expected in file %s, line %d!", GetFileName().CStr(), mTokenLine);
      }
    }
    // first word?
    if (firstWord) {
      if (allowStr && lastWordWasString) {
        if (isString) *isString = true;
        ReadWord(res);
        if (res.GetSize() == 1) {
          if (res[0] != ';' && res[0] != ',' && res[0] != ':') {
            ABORT("Invalid terminator in file %s, line %d!", GetFileName().CStr(), mTokenLine);
          }
          if (PreserveTerminator) UngetChar(res[0]);
        } else {
          ABORT("Terminator expected in file %s, line %d!", GetFileName().CStr(), mTokenLine);
        }
        return Word;
      }
      firstWord = false;
    }
    // other things
    char First = Word[0];
    // number?
    if (isdigit(First)) {
      if (mCollectingNumStr) mNumStr << Word;
      char *e;
      if (allowFloats) {
        Value = (numtype)strtod(Word.CStr(), &e);
      } else {
        Value = (numtype)strtol(Word.CStr(), &e, 0);
      }
      if (*e != '\0') ABORT("Invalid number '%s' in file %s, line %d!", Word.CStr(), GetFileName().CStr(), mTokenLine);
      NumberCorrect = true;
      // HACK: autoinsert terminator
      SkipBlanks();
      int ch = GetChar();
      if (ch != EOF) {
        UngetChar(ch);
        if (ch == '}') UngetChar(';');
      }
      continue;
    }
    // delimiter/math?
    if (Word.GetSize() == 1) {
      //ConLogf("  ReadNumberIntr: First='%c'", First);
      if (First == ';' || First == ',' || First == ':' || (wasCloseBrc && First == '}')) {
        if (First == '}' && wasCloseBrc) *wasCloseBrc = true;
        if (CallLevel != HIGHEST || PreserveTerminator) UngetChar(First);
        if (num) *num = Value;
        return res;
      }
      if (First == ')') {
        if ((CallLevel != HIGHEST && CallLevel != 4) || PreserveTerminator) UngetChar(')');
        if (num) *num = Value;
        return res;
      }
      if (First == '~') {
        if (mCollectingNumStr) mNumStr << Word;
        Value = ~ReadNumber(4);
        NumberCorrect = true;
        continue;
      }
/* Convert this into inline function! */
#define CHECK_OP(op, cl, opertype) \
  if (First == #op[0]) { \
    if (cl < CallLevel) {\
      if (mCollectingNumStr) mNumStr << Word; \
      /*Value op##= ReadNumber(cl);*/\
      numtype rhs = 0;\
      ReadNumberIntr<numtype>(cl, &rhs, nullptr, false, false, nullptr, allowFloats);\
      /*Value op##= rhs;*/\
      Value = (numtype)((opertype)Value op (opertype)rhs);\
      NumberCorrect = true;\
      continue;\
    } else {\
      UngetChar(#op[0]);\
      if (num) *num = Value;\
      return res;\
    } \
  }
      CHECK_OP(&, 1, int);
      CHECK_OP(|, 1, int);
      CHECK_OP(^, 1, int);
      CHECK_OP(*, 2, numtype);
      CHECK_OP(/, 2, numtype);
      CHECK_OP(%, 2, int);
      CHECK_OP(+, 3, numtype);
      CHECK_OP(-, 3, numtype);
#undef CHECK_OP
      if (First == '<') {
        char Next = GetChar();
        if (Next == '<') {
          if (1 < CallLevel) {
            if (mCollectingNumStr) mNumStr << "<<";
            //Value <<= ReadNumber(1);
            Value = (numtype)((int)Value<<(int)ReadNumber(1));
            NumberCorrect = true;
            continue;
          } else {
            UngetChar('<');
            UngetChar('<');
            if (num) *num = Value;
            return res;
          }
        } else {
          UngetChar(Next);
        }
      }
      if (First == '>') {
        char Next = GetChar();
        if (Next == '>') {
          if (1 < CallLevel) {
            if (mCollectingNumStr) mNumStr << ">>";
            //Value >>= ReadNumber(1);
            Value = (numtype)((int)Value>>(int)ReadNumber(1));
            NumberCorrect = true;
            continue;
          } else {
            UngetChar('>');
            UngetChar('>');
            if (num) *num = Value;
            return res;
          }
        } else {
          UngetChar(Next);
        }
      }
      if (First == '(') {
        if (NumberCorrect) {
          UngetChar('(');
          if (num) *num = Value;
          return res;
        } else {
          if (mCollectingNumStr) mNumStr << Word;
          Value = ReadNumber(4);
          if (mCollectingNumStr) mNumStr << ")";
          NumberCorrect = false;
          continue;
        }
      }
      if (First == '=' && CallLevel == HIGHEST) continue;
      if (First == '#') {
        // for #defines
        UngetChar('#');
        if (num) *num = Value;
        return res;
      }
    }
    /*
    if (Word == "enum" || Word == "bitenum") {
      if (CallLevel != HIGHEST || PreserveTerminator) UngetChar(';');
      if (num) *num = Value;
      return res;
    }
    */
    if (Word == "CONST" && CallLevel == HIGHEST) {
      UngetStr("CONST ");
      if (num) *num = Value;
      return res;
    }
    if (Word == "VAR" && CallLevel == HIGHEST) {
      UngetStr("VAR ");
      if (num) *num = Value;
      return res;
    }
    // rgbX?
    if (Word == "rgb16" || Word == "rgb24") {
      truth is16 = (Word == "rgb16");
      if (mCollectingNumStr) mNumStr << Word;
      int Red = ReadNumber();
      if (Red < 0 || Red > 255) ABORT("Illegal Red value (%d) file %s, line %d!", Red, GetFileName().CStr(), mTokenLine);
      int Green = ReadNumber();
      if (Green < 0 || Green > 255) ABORT("Illegal Green value (%d) file %s, line %d!", Green, GetFileName().CStr(), mTokenLine);
      int Blue = ReadNumber();
      if (Blue < 0 || Blue > 255) ABORT("Illegal Blue value (%d) file %s, line %d!", Blue, GetFileName().CStr(), mTokenLine);
      Value = (is16 ? MakeRGB16(Red, Green, Blue) : MakeRGB24(Red, Green, Blue));
      NumberCorrect = true;
      continue;
    }
    // `true` literal?
    if (Word == "true" || Word == "tan") {
      if (mCollectingNumStr) mNumStr << Word;
      Value = 1;
      NumberCorrect = true;
      continue;
    }
    // `false` literal?
    if (Word == "false" || Word == "ona") {
      if (mCollectingNumStr) mNumStr << Word;
      Value = 0;
      NumberCorrect = true;
      continue;
    }
    // known value?
    if (ValueMap) {
      valuemap::const_iterator Iterator = ValueMap->find(Word);
      if (Iterator != ValueMap->end()) {
        if (mCollectingNumStr) mNumStr << Word;
        Value = Iterator->second;
        NumberCorrect = true;
        continue;
      }
    }
    // something bad
    ABORT("Odd numeric value \"%s\" encountered in file %s, line %d!", Word.CStr(), GetFileName().CStr(), mTokenLine);
  }
}


//==========================================================================
//
//  TextInput::ReadNumber
//
//==========================================================================
sLong TextInput::ReadNumber (int CallLevel, truth PreserveTerminator, truth *wasCloseBrc) {
  sLong num = 0;
  ReadNumberIntr<sLong>(CallLevel, &num, nullptr, false, PreserveTerminator, wasCloseBrc, false);
  return num;
}


//==========================================================================
//
//  ParseBool
//
//==========================================================================
static int ParseBool (cfestring &Word) {
  if (Word.EquCI("true") || Word.EquCI("tan") || Word.EquCI("yes") || Word.EquCI("on")) {
    return 1;
  }
  if (Word.EquCI("false") || Word.EquCI("ona") || Word.EquCI("no") || Word.EquCI("off")) {
    return 0;
  }
  if (Word == "0") return 0;
  if (Word == "1") return 1;
  return -1;
}


//==========================================================================
//
//  TextInput::ReadBool
//
//  FIXME: `@var` syntax!
//
//==========================================================================
bool TextInput::ReadBool () {
  festring Word;
  ReadWord(Word);
  if (Word != "=") Error("`=` expected");
  ReadWord(Word);
  int res = ParseBool(Word);
  if (res < 0) {
    // known value?
    if (ValueMap) {
      valuemap::const_iterator Iterator = ValueMap->find(Word);
      if (Iterator != ValueMap->end()) {
        if (Iterator->second == 0) res = false;
        else if (Iterator->second == 1) res = true;
      }
    }
  }
  if (res < 0) {
    ABORT("Illegal `bool` value (%s) file %s, line %d!", Word.CStr(), GetFileName().CStr(), mTokenLine);
  }
  ReadWord(Word);
  if (Word != ";") {
    ABORT("Unterminated `bool` value, file %s, line %d!", GetFileName().CStr(), mTokenLine);
  }
  return (res != 0);
}


//==========================================================================
//
//  TextInput::ReadStringOrNumber
//
//==========================================================================
festring TextInput::ReadStringOrNumber (sLong *num, truth *isString, truth PreserveTerminator,
                                        truth *wasCloseBrc)
{
  return ReadNumberIntr<sLong>(HIGHEST, num, isString, true, PreserveTerminator, wasCloseBrc, false);
}


//==========================================================================
//
//  TextInput::ReadFloat
//
//==========================================================================
float TextInput::ReadFloat () {
  float num = 0;
  ReadNumberIntr<float>(HIGHEST, &num, nullptr, false, false, nullptr, true);
  return num;
}


//==========================================================================
//
//  TextInput::SkipCurly
//
//==========================================================================
void TextInput::SkipCurly (int level) {
  festring Word;
  IvanAssert(level >= 0);
  while (level != 0) {
    ReadWord(Word);
    if (Word == "{") level += 1;
    else if (Word == "}") level -= 1;
  }
}


//==========================================================================
//
//  TextInput::Error
//
//==========================================================================
void TextInput::Error (cchar *fmt, ...) {
  static char Buffer[16384];
  char *bufptr = Buffer;
  int bufsz = (int)sizeof(Buffer)-1;
  for (;;) {
    va_list ap;
    int n;
    char *np;
    va_start(ap, fmt);
    n = vsnprintf(bufptr, bufsz, fmt, ap);
    va_end(ap);
    if (n > -1 && n < bufsz) break;
    if (n < -1) n = bufsz+4096;
    np = (char *)realloc((bufptr == Buffer ? NULL : bufptr), n+1);
    if (np == NULL) exit(4); //FIXME
    bufptr = np;
  }
  ABORT("ERROR in %s:%d: %s", GetFileName().CStr(), TokenLine(), Buffer);
  exit(4);
}


//==========================================================================
//
//  TextInput::Warning
//
//==========================================================================
void TextInput::Warning (cchar *fmt, ...) {
  static char Buffer[16384];
  char *bufptr = Buffer;
  int bufsz = (int)sizeof(Buffer)-1;
  for (;;) {
    va_list ap;
    int n;
    char *np;
    va_start(ap, fmt);
    n = vsnprintf(bufptr, bufsz, fmt, ap);
    va_end(ap);
    if (n > -1 && n < bufsz) break;
    if (n < -1) n = bufsz+4096;
    np = (char *)realloc((bufptr == Buffer ? NULL : bufptr), n+1);
    if (np == NULL) exit(4); //FIXME
    bufptr = np;
  }
  ConLogf("WARNING in %s:%d: %s", GetFileName().CStr(), TokenLine(), Buffer);
}


// ////////////////////////////////////////////////////////////////////////// //
// fuck you, shitplusplus!
struct FuckedShitForFuckedFinally {
  truth *var;
  truth oval;
  inline FuckedShitForFuckedFinally (truth *avar, truth nval=true) {
    var = avar;
    oval = *var;
    *var = nval;
  }
  inline ~FuckedShitForFuckedFinally () {
    *var = oval;
  }
};


//==========================================================================
//
//  TextInput::ReadNumberKeepStr
//
//==========================================================================
sLong TextInput::ReadNumberKeepStr (int CallLevel, truth PreserveTerminator, truth *wasCloseBrc) {
  auto fuck = FuckedShitForFuckedFinally(&mCollectingNumStr);
  mNumStr = "";
  return ReadNumber(CallLevel, PreserveTerminator, wasCloseBrc);
}


//==========================================================================
//
//  TextInput::ReadStringOrNumberKeepStr
//
//==========================================================================
festring TextInput::ReadStringOrNumberKeepStr (sLong *num, truth *isString,
                                               truth PreserveTerminator, truth *wasCloseBrc)
{
  auto fuck = FuckedShitForFuckedFinally(&mCollectingNumStr);
  mNumStr = "";
  return ReadStringOrNumber(num, isString, PreserveTerminator, wasCloseBrc);
}


//==========================================================================
//
//  TextInput::ReadVector2d
//
//==========================================================================
v2 TextInput::ReadVector2d () {
  SkipBlanks();
  int ch = GetChar();
  if (ch == '{') ch = '}'; else { UngetChar(ch); ch = 0; }

  v2 Vector;
  Vector.X = ReadNumber();
  Vector.Y = ReadNumber();

  if (ch) {
    SkipBlanks();
    if (GetChar() != ch) ABORT("Vector syntax error: \"%c\" expected in file %s, line %d!", ch, GetFileName().CStr(), TokenLine());
    SkipBlanks();
    ch = GetChar();
         if (ch == '}') UngetChar(ch);
    else if (ch != ';' && ch != ',') ABORT("Vector syntax error: terminator expected in file %s, line %d!", GetFileName().CStr(), TokenLine());
  }

  return Vector;
}


//==========================================================================
//
//  TextInput::ReadRect
//
//==========================================================================
rect TextInput::ReadRect () {
  SkipBlanks();
  int ch = GetChar();
  if (ch == '{') ch = '}'; else { UngetChar(ch); ch = 0; }

  rect Rect;
  Rect.X1 = ReadNumber();
  Rect.Y1 = ReadNumber();
  Rect.X2 = ReadNumber();
  Rect.Y2 = ReadNumber();

  if (ch) {
    SkipBlanks();
    if (GetChar() != ch) ABORT("Vector syntax error: \"%c\" expected in file %s, line %d!", ch, GetFileName().CStr(), TokenLine());
    SkipBlanks();
    ch = GetChar();
         if (ch == '}') UngetChar(ch);
    else if (ch != ';' && ch != ',') ABORT("Vector syntax error: terminator expected in file %s, line %d!", GetFileName().CStr(), TokenLine());
  }

  return Rect;
}


// ////////////////////////////////////////////////////////////////////////// //
#define GETC(var)  do { \
    var = inFile->GetChar(); \
    if (var == EOF) { \
      if (infStack.empty()) ABORT("'}' missing in datafile %s line %d!", inFile->GetFileName().CStr(), inFile->TokenLine()); \
      delete inFile; \
      *iff = inFile = infStack.top(); \
      infStack.pop(); \
      continue; \
    } \
    break; \
  } while (1) \


//==========================================================================
//
//  collectSourceCode
//
//==========================================================================
static festring collectSourceCode (std::stack<TextInput *> &infStack, TextInput **iff) {
  if (!iff || !*iff) ABORT("Wut?!");
  TextInput *inFile = *iff;
  int brclevel = 0;
  int ch;
  festring res;

  for (;;) {
    GETC(ch);
    res << (char)ch;
    // brackets?
    if (ch == '{') { ++brclevel; continue; }
    if (ch == '}') { if (--brclevel == 0) break; continue; }
    // string?
    if (ch == '"') {
      // waiting for bracket?
      if (brclevel == 0) ABORT("'{' expected in datafile %s line %d!", inFile->GetFileName().CStr(), inFile->TokenLine()); \
      for (;;) {
        GETC(ch);
        res << (char)ch;
        if (ch == '"') break;
        if (ch == '\\') {
          GETC(ch);
          res << (char)ch;
        }
      }
      continue;
    }
    // comment
    if (ch == '/') {
      GETC(ch);
      res << (char)ch;
      // single-line?
      if (ch == '/') {
        for (;;) {
          GETC(ch);
          res << (char)ch;
          if (ch == '\n') break;
        }
        continue;
      }
      // multiline?
      if (ch == '*') {
        int prevch = 0;
        int level = 1;
        for (;;) {
          GETC(ch);
          res << (char)ch;
          // comment start (nested)
          if (prevch == '/' && ch == '*') {
            ++level;
            prevch = 0;
            continue;
          }
          // comment end
          if (prevch == '*' && ch == '/') {
            if (--level == 0) break;
            prevch = 0;
            continue;
          }
          prevch = ch;
        }
        continue;
      }
      // waiting for bracket?
      if (brclevel == 0) ABORT("'{' expected in datafile %s line %d!", inFile->GetFileName().CStr(), inFile->TokenLine()); \
      continue;
    }
    if (brclevel == 0 && ch > ' ') ABORT("'{' expected in datafile %s line %d!", inFile->GetFileName().CStr(), inFile->TokenLine()); \
  }

  return res;
}

#undef GETC


//==========================================================================
//
//  collectEventHandlerSource
//
//==========================================================================
static EventHandlerSource collectEventHandlerSource (std::stack<TextInput *> &infStack,
                                                     TextInput **iff, sLong configNumber)
{
  if (!iff || !*iff) ABORT("Wut?!");
  TextInput *inFile = *iff;
  EventHandlerSource res;
  // read type
  res.fname = inFile->GetFileName();
  res.type = inFile->ReadWord();
  if (configNumber >= 0) {
    res.type << " " << configNumber;
  }
  res.startline = inFile->CurrentLine();
  res.text = collectSourceCode(infStack, iff);
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
// EventHandlerMap
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  EventHandlerMap::EventHandlerMap
//
//==========================================================================
EventHandlerMap::EventHandlerMap () {
}


//==========================================================================
//
//  EventHandlerMap::~EventHandlerMap
//
//==========================================================================
EventHandlerMap::~EventHandlerMap () {
  Clear();
}


//==========================================================================
//
//  EventHandlerMap::Clear
//
//==========================================================================
void EventHandlerMap::Clear () {
  mMap.clear();
}


//==========================================================================
//
//  EventHandlerMap::CollectSource
//
//  "on" skipped, expecting type
//
//==========================================================================
void EventHandlerMap::CollectSource (std::stack<TextInput *> &infStack, TextInput **iff,
                                     sLong configNumber)
{
  auto ehs = collectEventHandlerSource(infStack, iff, configNumber);
  if (ehs.type.IsEmpty()) ABORT("Empty handler type in file '%s' at line %d", ehs.fname.CStr(), ehs.startline);
  auto it = mMap.find(ehs.type);
  if (it != mMap.end()) {
    //ABORT("Duplicate handler type '%s' in file '%s' at line %d", ehs.type.CStr(), ehs.fname.CStr(), ehs.startline);
    mMap.erase(it);
  }
  mMap.insert(std::make_pair(ehs.type, ehs));
}


//==========================================================================
//
//  EventHandlerMap::CollectSource
//
//  "on" skipped, expecting type
//
//==========================================================================
void EventHandlerMap::CollectSource (TextInput &iff) {
  std::stack<TextInput *> infStack;
  TextInput *ifp = &iff;
  infStack.push(ifp);
  CollectSource(infStack, &ifp, -1);
}


//==========================================================================
//
//  EventHandlerMap::OpenHandler
//
//  can return `nullptr`
//
//==========================================================================
TextInput *EventHandlerMap::OpenHandler (cfestring &atype, sLong configNumber,
                                         const valuemap *aValueMap) const
{
  if (configNumber >= 0) {
    festring ss;
    ss << atype << " " << configNumber;
    auto it1 = mMap.find(ss);
    if (it1 != mMap.end()) {
      return new MemTextInputFile((*it1).second.fname, (*it1).second.startline, (*it1).second.text, aValueMap);
    }
  }
  auto it = mMap.find(atype);
  if (it == mMap.end()) return nullptr;
  return new MemTextInputFile((*it).second.fname, (*it).second.startline, (*it).second.text, aValueMap);
}


// ////////////////////////////////////////////////////////////////////////// //
// data readers
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  ReadData
//
//==========================================================================
void ReadData (festring &String, TextInput &SaveFile) {
  SaveFile.ReadWord(String);
  if (String == "=") SaveFile.ReadWord(String);
  SaveFile.ReadWord();
}


//==========================================================================
//
//  ReadData
//
//==========================================================================
void ReadData (fearray<sLong> &Array, TextInput &SaveFile) {
  Array.Clear();
  festring Word;
  SaveFile.ReadWord(Word);
  if (Word == "==") {
    Array.Allocate(1);
    Array.Data[0] = SaveFile.ReadNumber();
  } else if (Word == ":=") {
    SaveFile.ReadWord(Word);
    if (Word != "{") ABORT("Array syntax error \"%s\" found in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
    std::vector<sLong> v;
    for (;;) {
      truth wasCloseBrc = false;
      sLong n = SaveFile.ReadNumber(HIGHEST, false, &wasCloseBrc);
      if (wasCloseBrc) break;
      v.push_back(n);
    }
    Array.Allocate(v.size());
    for (size_t f = 0; f < v.size(); ++f) Array.Data[f] = v[f];
  } else if (Word == "=") {
    SaveFile.ReadWord(Word);
    if (Word != "{") ABORT("Array syntax error \"%s\" found in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
    fearray<sLong>::sizetype Size = SaveFile.ReadNumber();
    Array.Allocate(Size);
    for (fearray<sLong>::sizetype c = 0; c < Size; ++c) Array.Data[c] = SaveFile.ReadNumber();
    if (SaveFile.ReadWord() != "}") ABORT("Illegal array terminator \"%s\" encountered in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
  } else {
    ABORT("Array syntax error: '=', '==' or ':=' expected in file %s, line %d!", SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
  }
}


//==========================================================================
//
//  ReadData
//
//==========================================================================
void ReadData (fearray<festring> &Array, TextInput &SaveFile) {
  Array.Clear();
  festring Word;
  SaveFile.ReadWord(Word);
  if (Word == "==") {
    Array.Allocate(1);
    SaveFile.ReadWord(Array.Data[0]);
    if (SaveFile.ReadWord() != ";") ABORT("Array syntax error \"%s\" found in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
  } else if (Word == ":=") {
    SaveFile.ReadWord(Word);
    if (Word != "{") ABORT("Array syntax error \"%s\" found in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
    std::vector<festring> v;
    for (;;) {
      SaveFile.ReadWord(Word);
      if (Word == "}") break;
      v.push_back(Word);
      SaveFile.ReadWord(Word);
      if (Word == "}") break;
      if (Word != "," && Word != ";") ABORT("Array syntax error \"%s\" found in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
    }
    Array.Allocate(v.size());
    for (size_t f = 0; f < v.size(); ++f) Array.Data[f] = v[f];
  } else if (Word == "=") {
    SaveFile.ReadWord(Word);
    if (Word != "{") ABORT("Array syntax error \"%s\" found in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
    fearray<festring>::sizetype Size = SaveFile.ReadNumber();
    Array.Allocate(Size);
    for (fearray<festring>::sizetype c = 0; c < Size; ++c) {
      SaveFile.ReadWord(Array.Data[c]);
      SaveFile.ReadWord(Word);
      if (Word != "," && Word != ";") ABORT("Array syntax error \"%s\" found in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
    }
    if (SaveFile.ReadWord() != "}") ABORT("Illegal array terminator \"%s\" encountered in file %s, line %d!", Word.CStr(), SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
  } else {
    ABORT("Array syntax error: '=', '==' or ':=' expected in file %s, line %d!", SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
  }
}


//==========================================================================
//
//  ReadData
//
//==========================================================================
void ReadData (RandomChance &rc, TextInput &fl) {
  festring w;
  rc.Clear();
  w = fl.ReadWord();
  auto tkline = fl.TokenLine();
  if (w == "==") {
    rc.rnd = fl.ReadNumber();
  } else if (w == "=" || w == ":=") {
    if (fl.ReadWord() != "{") ABORT("RandomChance syntax error: '{' expected in file %s, line %d!", fl.GetFileName().CStr(), fl.TokenLine());
    for (;;) {
      w = fl.ReadWord();
      if (w == "}") break;
      sLong *fptr = nullptr;
           if (w.EquCI("add")) fptr = &rc.add;
      else if (w.EquCI("rnd")) fptr = &rc.rnd;
      else if (w.EquCI("rand")) fptr = &rc.rnd;
      else if (w.EquCI("rmin")) fptr = &rc.rmin;
      else if (w.EquCI("rmax")) fptr = &rc.rmax;
      if (!fptr) ABORT("RandomChance syntax error: unknown field '%s' in file %s, line %d!", w.CStr(), fl.GetFileName().CStr(), fl.TokenLine());
      w = fl.ReadWord();
      if (w != ":" && w != "=") ABORT("RandomChance syntax error: ':' expected in file %s, line %d!", fl.GetFileName().CStr(), fl.TokenLine());
      *fptr = fl.ReadNumber();
    }
  } else {
    ABORT("RandomChance syntax error: '=' or '==' expected in file %s, line %d!", fl.GetFileName().CStr(), tkline);
  }
  if (rc.rnd < 0) ABORT("Invalid random chance in file %s, line %d!", fl.GetFileName().CStr(), tkline);
}


// ////////////////////////////////////////////////////////////////////////// //
// TextInputFile
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  TextInputFile::TextInputFile
//
//==========================================================================
TextInputFile::TextInputFile (cfestring &FileName, const valuemap *aValueMap, truth AbortOnErr) {
  ifile.Open(FileName, AbortOnErr);
  Setup(aValueMap);
}


//==========================================================================
//
//  TextInputFile::~TextInputFile
//
//==========================================================================
TextInputFile::~TextInputFile () {
  Close();
}


//==========================================================================
//
//  TextInputFile::GetFileName
//
//==========================================================================
cfestring &TextInputFile::GetFileName () const {
  return ifile.GetFileName();
}


//==========================================================================
//
//  TextInputFile::RealGetChar
//
//==========================================================================
int TextInputFile::RealGetChar () {
  return ifile.Get();
}


//==========================================================================
//
//  TextInputFile::IsRealEof
//
//==========================================================================
truth TextInputFile::IsRealEof () {
  return ifile.Eof();
}


//==========================================================================
//
//  TextInputFile::IsOpen
//
//==========================================================================
truth TextInputFile::IsOpen () {
  return ifile.IsOpen();
}


//==========================================================================
//
//  TextInputFile::Close
//
//==========================================================================
void TextInputFile::Close () {
  ifile.Close();
  TextInput::Close();
}


//==========================================================================
//
//  TextInputFile::RealGetPos
//
//==========================================================================
sLong TextInputFile::RealGetPos () {
  return (ifile.IsOpen() ? ifile.TellPos() : 0);
}


//==========================================================================
//
//  TextInputFile::RealSetPos
//
//==========================================================================
void TextInputFile::RealSetPos (sLong apos) {
  if (ifile.IsOpen()) ifile.SeekPosBegin(apos);
}


// ////////////////////////////////////////////////////////////////////////// //
// MemTextInputFile
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  MemTextInputFile::MemTextInputFile
//
//==========================================================================
MemTextInputFile::MemTextInputFile (cfestring &afname, cfestring &str, const valuemap *aValueMap)
  : buf(nullptr)
  , bufSize(0)
  , bufPos(0)
  , tfname(afname)
{
  bufSize = str.GetSize();
  buf = (unsigned char *)calloc(1, bufSize+1);
  memmove(buf, str.CStr(), bufSize);
  Setup(aValueMap);
}


//==========================================================================
//
//  MemTextInputFile::MemTextInputFile
//
//==========================================================================
MemTextInputFile::MemTextInputFile (cfestring &afname, int stline, cfestring &str,
                                    const valuemap *aValueMap)
  : buf(nullptr)
  , bufSize(0)
  , bufPos(0)
  , tfname(afname)
{
  bufSize = str.GetSize();
  buf = (unsigned char *)calloc(1, bufSize+1);
  memmove(buf, str.CStr(), bufSize);
  Setup(aValueMap);
  mCurrentLine = mTokenLine = stline;
}


//==========================================================================
//
//  MemTextInputFile::~MemTextInputFile
//
//==========================================================================
MemTextInputFile::~MemTextInputFile () {
  Close();
}


//==========================================================================
//
//  MemTextInputFile::RealGetChar
//
//==========================================================================
int MemTextInputFile::RealGetChar () {
  if (bufPos >= bufSize) return EOF;
  return buf[bufPos++];
}


//==========================================================================
//
//  MemTextInputFile::GetFileName
//
//==========================================================================
cfestring &MemTextInputFile::GetFileName () const {
  return tfname;
}


//==========================================================================
//
//  MemTextInputFile::IsRealEof
//
//==========================================================================
truth MemTextInputFile::IsRealEof () {
  return (bufPos >= bufSize);
}


//==========================================================================
//
//  MemTextInputFile::IsOpen
//
//==========================================================================
truth MemTextInputFile::IsOpen () {
  return (buf != nullptr);
}


//==========================================================================
//
//  MemTextInputFile::Close
//
//==========================================================================
void MemTextInputFile::Close () {
  if (buf) {
    free(buf);
    buf = nullptr;
    tfname = "";
    bufSize = 0;
    bufPos = 0;
  }
  TextInput::Close();
}


//==========================================================================
//
//  MemTextInputFile::RealGetPos
//
//==========================================================================
sLong MemTextInputFile::RealGetPos () {
  return bufPos;
}


//==========================================================================
//
//  MemTextInputFile::RealSetPos
//
//==========================================================================
void MemTextInputFile::RealSetPos (sLong apos) {
  if (buf != nullptr) bufPos = apos;
}


//==========================================================================
//
//  ReadMCData
//
//  material config, with chances
//
//==========================================================================
void ReadMCData (TextInput &infile, TextInput *&t1, TextInput *&t2) {
  t1 = 0; t2 = 0;
  festring arr1, arr2;
  festring Word;

  festring fname = infile.GetFileName();
  int stline = infile.TokenLine();

  arr1 << ":= { ";
  arr2 << ":= { ";

  infile.ReadWord(Word);
  if (Word != ":=") infile.Error("Array syntax error \"%s\"", Word.CStr());
  infile.ReadWord(Word);
  if (Word != "{") infile.Error("Array syntax error \"%s\"", Word.CStr());
  infile.ReadWord(Word);
  bool needComma1 = false;
  bool needComma2 = false;
  while (Word != "}") {
    if (needComma1) arr1 << ", "; else needComma1 = true;
    arr1 << Word;
    infile.ReadWord(Word);
    if (Word != ":") infile.Error("Array syntax error \"%s\"", Word.CStr());

    infile.ReadWord(Word);
    if (needComma2) arr2 << ", "; else needComma2 = true;
    arr2 << Word;

    infile.ReadWord(Word);
    if (Word != "}") {
      if (Word != "," && Word != ";") {
        infile.Error("Array syntax error \"%s\"", Word.CStr());
      }
      infile.ReadWord(Word);
    }
  }
  //infile.ReadWord(Word);

  arr1 << "; }";
  arr2 << "; }";

  #if 0
  ConLogf("arr1: <%s>", arr1.CStr());
  ConLogf("arr2: <%s>", arr2.CStr());
  #endif

  t1 = new MemTextInputFile(fname, stline, arr1, infile.GetValueMap());
  t2 = new MemTextInputFile(fname, stline, arr2, infile.GetValueMap());
}
