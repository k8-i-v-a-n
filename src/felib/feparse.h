/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_PARSE_H__
#define __FELIB_PARSE_H__

#include "felibdef.h"
#include "feerror.h"
#include "festring.h"
#include "fearray.h"
#include "fesave.h"
#include "femath.h"


// ////////////////////////////////////////////////////////////////////////// //
class TextInput;
class TextInputFile;
class MemTextInputFile;


using valuemap = std::unordered_map<festring, sLong>;
using InputFileGetVarFn = festring (*) (TextInput *fl, cfestring &name, truth scvar);


// ////////////////////////////////////////////////////////////////////////// //
struct TextFileLocation {
public:
  festring fname;
  int line;

public:
  inline TextFileLocation () : fname(), line(0) {}
  inline TextFileLocation (TextInput &infile) : fname(), line(0) { Set(infile); }
  void Set (TextInput &infile);
};


class TextInput {
  friend struct InputFileSaved;
protected:
  enum { MaxUngetChars = 12 };
  typedef std::unordered_map<festring, festring> VarMap;

protected:
  const valuemap *ValueMap;
  truth lastWasNL;
  truth lastWordWasString;
  VarMap mVars;
  InputFileGetVarFn mGetVar;
  std::stack<int> mIfStack;
  int mCharBuf[MaxUngetChars];
  int mCharBufPos;
  int mCurrentLine;
  int mTokenLine;
  festring mNumStr; // when reading a number, and `mCollectingNumStr` flag is set, this will be filled
  truth mCollectingNumStr;
  truth mAllowFloatNums;

protected:
  TextInput (const valuemap *aValueMap=0);

  void Setup (const valuemap *aValueMap);

public:
  // no copies
  TextInput (const TextInput &ti) = delete;
  TextInput &operator = (const TextInput &ti) = delete;

  virtual ~TextInput ();

  const valuemap *GetValueMap () { return ValueMap; }

  virtual cfestring &GetFileName () const = 0;
  virtual truth IsOpen () = 0;
  virtual void Close (); // for dtor

  truth Eof ();

  int GetChar ();
  void UngetChar (int ch);
  void UngetStr (cchar *str);

  festring ReadWord (truth abortOnEOF=true);
  truth ReadWord (festring &str, truth abortOnEOF=true); // returns `false` on EOF
  char ReadLetter (truth abortOnEOF=true);
  sLong ReadNumber (int CallLevel=HIGHEST, truth PreserveTerminator=false, truth *wasCloseBrc=0);
  festring ReadStringOrNumber (sLong *num, truth *isString, truth PreserveTerminator=false, truth *wasCloseBrc=0);
  sLong ReadNumberKeepStr (int CallLevel=HIGHEST, truth PreserveTerminator=false, truth *wasCloseBrc=0);
  festring ReadStringOrNumberKeepStr (sLong *num, truth *isString, truth PreserveTerminator=false, truth *wasCloseBrc=0);
  v2 ReadVector2d ();
  rect ReadRect ();
  float ReadFloat ();
  bool ReadBool ();

  inline sLong ReadNumberPreserveTerm () { return ReadNumber(HIGHEST, true); }

  festring GetVar (cfestring &name, truth scvar);
  void SetVar (cfestring &name, cfestring &value);
  truth DelVar (cfestring &name);

  void SetGetVarCB (InputFileGetVarFn cb) { mGetVar = cb; }

  void SkipBlanks (); // comments too

  inline int TokenLine () const { return mTokenLine; }
  inline int CurrentLine () const { return mCurrentLine; }

  // when reading a number, and `mCollectingNumStr` flag is set, this will be filled
  inline cfestring &GetNumStr () const { return mNumStr; }

  // just read `ch`, skip possible comment; returns `ch` or -1
  int GotCharSkipComment (int ch, truth allowSingleLineComments=true);

  void SkipCurly (int level=1);

  void NO_RETURN LIKE_PRINTF(2, 3) Error (cchar *fmt, ...);
  void LIKE_PRINTF(2, 3) Warning (cchar *fmt, ...);

  // to be used in templated function below
  int CountArrayItems (char echar);

protected:
  template<typename numtype> festring ReadNumberIntr (int CallLevel, numtype *num, truth *isString, truth allowStr, truth PreserveTerminator, truth *wasCloseBrc, truth allowFloats);

  festring FindVar (cfestring &name, truth *found) const;
  truth ReadWordIntr (festring &String, truth abortOnEOF); // returns `false` on EOF

  festring ReadCondition (festring &token, int prio, truth skipIt);

protected:
  virtual int RealGetChar () = 0;
  virtual truth IsRealEof () = 0;

  // used to save/restore parsing positions
  virtual sLong RealGetPos () = 0;
  virtual void RealSetPos (sLong apos) = 0;
};


// ////////////////////////////////////////////////////////////////////////// //
class TextInputFile : public TextInput {
protected:
  inputfile ifile;

public:
  // no copies
  TextInputFile (const TextInputFile &ti) = delete;
  TextInputFile &operator = (const TextInputFile &ti) = delete;

  TextInputFile (cfestring &FileName, const valuemap *aValueMap=0, truth AbortOnErr=true);
  virtual ~TextInputFile ();

  virtual cfestring &GetFileName () const override;
  virtual truth IsOpen () override;
  virtual void Close () override;

protected:
  virtual int RealGetChar () override;
  virtual truth IsRealEof () override;

  virtual sLong RealGetPos () override;
  virtual void RealSetPos (sLong apos) override;
};


// ////////////////////////////////////////////////////////////////////////// //
class MemTextInputFile : public TextInput {
protected:
  unsigned char *buf;
  int bufSize;
  int bufPos;
  festring tfname;

public:
  // no copies
  MemTextInputFile (const MemTextInputFile &ti) = delete;
  MemTextInputFile &operator = (const MemTextInputFile &ti) = delete;

  MemTextInputFile (cfestring &afname, cfestring &str, const valuemap *aValueMap=0);
  MemTextInputFile (cfestring &afname, int stline, cfestring &str, const valuemap *aValueMap=0);
  virtual ~MemTextInputFile ();

  virtual cfestring &GetFileName () const override;
  virtual truth IsOpen () override;
  virtual void Close () override;

protected:
  virtual int RealGetChar () override;
  virtual truth IsRealEof () override;

  virtual sLong RealGetPos () override;
  virtual void RealSetPos (sLong apos) override;
};


// ////////////////////////////////////////////////////////////////////////// //
// this IS game-related
// `Rand()` returns `add+RAND_N(rnd)`
// `InRange()` returns `true` if `Rand()` is in [rmin..rmax] (inclusive!)
// script syntax:
//   RCField = { add: num; rnd: num; rmin: num; rmax: num; }
//   any field may absent
//   or simplified:
//   RCField == rnd;
struct RandomChance {
public:
  sLong add = 0;
  sLong rnd = 0;
  sLong rmin = 0, rmax = 0;

public:
  inline void Clear () { add = rnd = rmin = rmax = 0; }
  inline sLong Rand () const { return add + RAND_N(rnd); }
  // if `rnd` is zero, assume "no chances".
  // if no rmin/rmax is set, this is equal to `!Rand()`.
  inline truth InRange () const {
    if (rnd > 0) {
      auto v = Rand();
      return (v >= rmin && v <= rmax);
    }
    return false;
  }
};


inline void ReadData (bool &Type, TextInput &infile) { Type = infile.ReadBool(); }
inline void ReadData (sByte &Type, TextInput &infile) { Type = infile.ReadNumber(); }
inline void ReadData (uChar &Type, TextInput &infile) { Type = infile.ReadNumber(); }
inline void ReadData (short &Type, TextInput &infile) { Type = infile.ReadNumber(); }
inline void ReadData (uShort &Type, TextInput &infile) { Type = infile.ReadNumber(); }
//inline void ReadData (sLong &Type, inputfile &SaveFile) { Type = SaveFile.ReadNumber(); } //k8:64
inline void ReadData (feuLong &Type, TextInput &infile) { Type = infile.ReadNumber(); }
inline void ReadData (int &Type, TextInput &infile) { Type = infile.ReadNumber(); }
inline void ReadData (packv2 &Type, TextInput &infile) { Type = infile.ReadVector2d(); }
inline void ReadData (v2 &Type, TextInput &infile) { Type = infile.ReadVector2d(); }
inline void ReadData (rect &Type, TextInput &infile) { Type = infile.ReadRect(); }
inline void ReadData (float &Type, TextInput &infile) { Type = infile.ReadFloat(); }

void ReadData (festring &, TextInput &);
void ReadData (fearray<sLong> &, TextInput &);
void ReadData (fearray<festring> &, TextInput &);
void ReadData (RandomChance &rc, TextInput &fl);


template <class type> void ReadData (fearray<type> &Array, TextInput &infile) {
  Array.Clear();
  festring Word;
  infile.ReadWord(Word);
  // one element?
  if (Word == "==") {
    Array.Allocate(1);
    ReadData(Array.Data[0], infile);
    return;
  }
  // more than one element
  typedef typename fearray<type>::sizetype sizetype;
  sizetype Size;
  // unknown number of elements?
  if (Word == ":=") {
    infile.ReadWord(Word);
    if (Word != "{") {
      infile.Error("Array syntax error \"%s\"", Word.CStr());
    }
    int count = infile.CountArrayItems('}');
    // HACK
    if (count < 1) {
      infile.Error("Array count error");
    }
    Size = count;
  } else {
    if (Word != "=") {
      infile.Error("Array syntax error: '=', '==' or ':=' expected");
    }
    infile.ReadWord(Word);
    if (Word != "{") {
      infile.Error("Array syntax error \"%s\"", Word.CStr());
    }
    Size = infile.ReadNumber();
    int count = infile.CountArrayItems('}');
    if (count < 1) {
      infile.Error("Array count error");
    }
    //HACK: 2 for vectors, 4 for rects
    //if ((sizetype)count != Size && (sizetype)count/2 != Size && (sizetype)count/4 != Size) ABORT("Array count error (%u != %d) found in file %s, line %d!", Size, count, SaveFile.GetFileName().CStr(), SaveFile.TokenLine());
    if ((sizetype)count != Size) {
      infile.Error("Array count error (%u != %d)", Size, count);
    }
  }
  Array.Allocate(Size);
  for (sizetype c = 0; c < Size; ++c) {
    ReadData(Array.Data[c], infile);
  }
  if (infile.ReadWord() != "}") {
    infile.Error("Illegal array terminator \"%s\" encountered", Word.CStr());
  }
}


// material config, with chances
void ReadMCData (TextInput &infile, TextInput *&t1, TextInput *&t2);


// ////////////////////////////////////////////////////////////////////////// //
struct EventHandlerSource {
  festring type;
  int startline;
  festring text;
  festring fname;
};

struct EventHandlerMap {
private:
  std::unordered_map<festring, EventHandlerSource> mMap;

public:
  //EventHandlerMap (const EventHandlerMap &) = delete; // disable copying

  EventHandlerMap ();
  virtual ~EventHandlerMap ();

  void Clear ();

  // "on" skipped, expecting type
  void CollectSource (std::stack<TextInput *> &infStack, TextInput **iff, sLong configNumber);

  // "on" skipped, expecting type
  void CollectSource (TextInput &iff);

  // can return `nullptr`
  TextInput *OpenHandler (cfestring &atype, sLong configNumber, const valuemap *aValueMap=0) const;
};


#endif
