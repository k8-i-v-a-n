/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include "felibdef.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <cctype>

#include "feparse2.h"


// ////////////////////////////////////////////////////////////////////////// //
TextParser::TextParser (const TextParser &ti) {
  if (&ti == this) return;
  mFile = ti.mFile;
  for (auto &tk : ti.mTList) mTList.push_back(tk);
  mLine = ti.mLine;
  mCol = ti.mCol;
  mSavedChar = ti.mSavedChar;
  mFName = ti.mFName;
}


TextParser::TextParser () : mFile(inoutfile()), mLine(1), mCol(1), mSavedChar(EOF), mFName(festring()) {
}


TextParser::TextParser (inoutfile afl, int aline, int acol) {
  mFile = afl;
  mLine = aline;
  mCol = acol;
  mSavedChar = EOF;
  mFName = afl.GetFileName();
}


TextParser::~TextParser () {
  mFile.Close();
  mTList.clear();
  mLine = 1;
  mCol = 1;
  mSavedChar = EOF;
  mFName = festring();
}


TextParser &TextParser::operator = (const TextParser &ti) {
  if (&ti == this) return *this;
  mFile = ti.mFile;
  mTList.clear();
  for (auto &tk : ti.mTList) mTList.push_back(tk);
  mLine = ti.mLine;
  mCol = ti.mCol;
  mSavedChar = ti.mSavedChar;
  mFName = ti.mFName;
  return *this;
}


TextParser::Token TextParser::peek (int idx) {
  if (idx < 0) ABORT("invalid token requested");
  while ((int)mTList.size() <= idx) {
    auto tk = parseToken();
    if (tk.isEOF()) break;
  }
  if (idx >= (int)mTList.size()) return Token();
  return mTList[idx];
}


TextParser::Token TextParser::get () {
  if (mTList.size() > 0) {
    Token tk = mTList[0];
    mTList.erase(mTList.begin());
    return tk;
  }
  return parseToken();
}


int TextParser::getChar () {
  int res = mSavedChar;
  if (res == EOF) res = mFile.Get(); else mSavedChar = EOF;
  if (res == '\n') { ++mLine; mCol = 1; } else ++mCol;
  return res;
}


int TextParser::peekChar () {
  if (mSavedChar == EOF) mSavedChar = mFile.Get();
  return mSavedChar;
}


int TextParser::SkipBlanks () {
  for (;;) {
    int ch = getChar();
    if (ch == EOF) return EOF;
    // single-line comment
    if (ch == '/' && peekChar() == '/') {
      for (;;) {
        ch = getChar();
        if (ch == EOF) return EOF;
        if (ch == '\n') break;
      }
      continue;
    }
    // multiline comment
    if (ch == '/' && peekChar() == '*') {
      int level = 1;
      getChar(); // skip star
      for (;;) {
        ch = getChar();
        if (ch == EOF) return EOF;
        if (ch == '/' && peekChar() == '*') { getChar(); ++level; continue; }
        if (ch == '*' && peekChar() == '/') { getChar(); if (--level == 0) break; continue; }
      }
      continue;
    }
    // other chars
    if (ch > ' ') return ch;
  }
}


void TextParser::error (cfestring &amsg) const {
  if (mTList.size() > 0) throw new ParseError(mTList[0].loc, amsg);
  throw new ParseError(Loc(mFName, mLine, mCol), amsg);
}


void TextParser::errorAt (const Loc &aloc, cfestring &amsg) const {
  throw new ParseError(aloc, amsg);
}


TextParser::Token TextParser::parseToken () {
  Token tk;
  int ch = SkipBlanks();
  tk.loc = Loc(mFName, mLine, mCol-(ch != EOF ? 1 : 0));
  if (ch == EOF) return tk;
  // string?
  if (ch == '"' || ch == '\'' || ch == '`') {
    tk.type = Token::Str;
    int ech = ch;
    for (;;) {
      ch = getChar();
      if (ch == EOF) errorAt(tk.loc, CONST_S("unterminated string"));
      if (ech != '`' && ch == '\\') {
        auto xloc = getLoc();
        ch = getChar();
        if (ch == EOF) errorAt(xloc, CONST_S("invalid escape"));
        switch (ch) {
          case 't': tk.s += '\t'; break;
          case 'r': tk.s += '\r'; break;
          case 'n': tk.s += '\n'; break;
          case '"': case '`': case '\\': case '\'': tk.s += ch; break;
          case 'C': tk.s += '\1'; break; // color code start
          case '~': tk.s += '\2'; break; // normal color
          default: errorAt(xloc, CONST_S("invalid escape"));
        }
        continue;
      }
      if (ch == ech) break;
      tk.s << ch;
    }
    return tk;
  }
  // identifier?
  if (isalpha(ch) || ch == '_') {
    tk.type = Token::Id;
    tk.s << ch;
    for (;;) {
      ch = peekChar();
      if (ch == EOF) break;
      if (isalnum(ch) || ch == '_') { tk.s << (char)getChar(); continue; }
      break;
    }
    return tk;
  }
  // number?
  if (isdigit(ch)) {
    tk.type = Token::Int;
    tk.s << ch;
    bool hex = false;
    if (peekChar() == 'x' || peekChar() == 'X') {
      tk.s << (char)getChar();
      if (peekChar() == EOF || !isxdigit(peekChar())) {
        errorAt(tk.loc, CONST_S("invalid number"));
      }
      hex = true;
    }
    for (;;) {
      ch = peekChar();
      if (ch == '_') continue;
      if (ch == EOF) break;
      if (hex) {
        if (!isxdigit(ch)) break;
      } else {
        if (!isdigit(ch)) break;
      }
      tk.s << (char)getChar();
    }
    ch = peekChar();
    if (isalnum(ch) || ch >= 127) errorAt(tk.loc, CONST_S("invalid number"));
    return tk;
  }
  // delimiter
  tk.type = Token::Delim;
  tk.s << ch;
       if (ch == '<' && peekChar() == '<') tk.s << (char)getChar();
  else if (ch == '<' && peekChar() == '=') tk.s << (char)getChar();
  else if (ch == '>' && peekChar() == '=') tk.s << (char)getChar();
  else if (ch == '>' && peekChar() == '>') tk.s << (char)getChar();
  else if (ch == '=' && peekChar() == '=') tk.s << (char)getChar();
  else if (ch == '!' && peekChar() == '=') tk.s << (char)getChar();
  else if (ch == ':' && peekChar() == '=') tk.s << (char)getChar();
  else if (ch == '&' && peekChar() == '&') tk.s << (char)getChar();
  else if (ch == '|' && peekChar() == '|') tk.s << (char)getChar();
  return tk;
}
