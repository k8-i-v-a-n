/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_PARSE2_H__
#define __FELIB_PARSE2_H__

#include "felibdef.h"
#include <stdlib.h>

#include <functional>
#include <map>
#include <set>
#include <vector>

#include "feerror.h"
#include "festring.h"
#include "fearray.h"
#include "fesave.h"
#include "femath.h"


// ////////////////////////////////////////////////////////////////////////// //
class TextParser {
public:
  //using InputFileGetVarFn = std::function<void (cfestring &name)>;
  //using valuemap = std::map<festring, sLong>;
  //using cvaluemap = const valuemap;

public:
  struct Loc {
    festring fname;
    int line, col;

    inline Loc () : fname(festring()), line(1), col(1) {}
    inline Loc (cfestring &afname, int aline, int acol) : fname(afname), line(aline), col(acol) {}
  };

  struct Token {
    enum {
      Eof,
      Id,
      Int,
      Float,
      Str,
      Delim,
    };
    festring s = festring();
    int type = Eof;
    Loc loc;

    inline bool isEOF () const { return (type == Eof); }
    inline bool isId () const { return (type == Id); }
    inline bool isInt () const { return (type == Int); }
    inline bool isFloat () const { return (type == Float); }
    inline bool isStr () const { return (type == Str); }
    inline bool isDelim () const { return (type == Delim); }

    inline int asInt (int def=-1) const {
      if (type == Int || type == Float) {
        char *e;
        const char *p = s.CStr();
        errno = 0;
        int res = strtol(p, &e, 0);
        if (p == e || *e != 0 || errno == ERANGE) return def;
        return res;
      }
      return def;
    }

    inline double asFloat (double def=0.0) const {
      if (type == Int || type == Float) {
        char *e;
        const char *p = s.CStr();
        errno = 0;
        double res = strtod(p, &e);
        if (p == e || *e != 0 || errno == ERANGE) return def;
        return res;
      }
      return def;
    }

    inline bool asBool () const {
      switch (type) {
        case Id: return (s == "true" || s == "tan");
        case Int: return (asInt(0) != 0);
        case Float: return (asFloat(0) != 0);
        case Str: case Delim: return (s.GetSize() != 0);
      }
      return false;
    }
  };

private:
  std::vector<Token> mTList;
  inoutfile mFile;
  int mLine, mCol;
  int mSavedChar;
  festring mFName;

public:
  TextParser (const TextParser &ti);
  TextParser ();
  TextParser (inoutfile afl, int aline=1, int acol=1);
  virtual ~TextParser ();

  TextParser &operator = (const TextParser &ti);

  inline cfestring &GetFileName () const { return mFile.GetFileName(); }
  inline bool hasStoredTokens () const { return (mTList.size() == 0); }

  inline Loc getLoc () const { return Loc(mFName, mLine, mCol); }

  Token peek (int idx=0);
  Token get ();

  //WARNING! if parser has some stored tokens, file position is after all tokens!
  int getChar (); // char or EOF
  int peekChar (); // char or EOF
  int SkipBlanks (); // comments too; returns char or EOF

  // at first memorized token, or at current position
  void error (cfestring &amsg) const;
  void errorAt (const Loc &aloc, cfestring &amsg) const;

private:
  Token parseToken ();
};


// ////////////////////////////////////////////////////////////////////////// //
class ParseError {
public:
  TextParser::Loc loc;
  festring msg;

public:
  ParseError (const TextParser::Loc &aloc, cfestring &amsg) { loc = aloc; msg = amsg; }
};


#endif
