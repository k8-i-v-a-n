/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include "felibdef.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

#include <cctype>

#include "vwfileio.h"
#include "fesave.h"
#include "femath.h"


//#define FE_DEBUG_SQA

/*
#ifdef USE_ZLIB
# define Xgetc  gzgetc
# define Xfeof  gzeof
# define Xungc  gzungetc
# define Xseek  gzseek
# define Xtell  gztell
# define Xclre  gzclearerr
#else
# define Xgetc  fgetc
# define Xfeof  feof
# define Xungc  ungetc
# define Xseek  fseek
# define Xtell  ftell
# define Xclre  clearerr
#endif
*/


static SQArchive *feCurrArchive = 0;
static int feSQTransactionCount = 0;


//==========================================================================
//
//  SaveArchiveTempSwap::SaveArchiveTempSwap
//
//==========================================================================
SaveArchiveTempSwap::SaveArchiveTempSwap (SQArchive **aNew)
  : mOld(feCurrArchive)
  , mOldTXC(feSQTransactionCount)
  , mNew(aNew)
{
  IvanAssert(aNew);
  IvanAssert(*aNew);
  feCurrArchive = *aNew;
  feSQTransactionCount = 0;
}


//==========================================================================
//
//  SaveArchiveTempSwap::~SaveArchiveTempSwap
//
//==========================================================================
SaveArchiveTempSwap::~SaveArchiveTempSwap () {
  IvanAssert(feCurrArchive);
  IvanAssert(feCurrArchive == *mNew);
  feCurrArchive->Close();
  delete feCurrArchive;
  *mNew = 0;
  feCurrArchive = mOld;
  feSQTransactionCount = mOldTXC;
}


//==========================================================================
//
//  GetSaveArchiveObj
//
//==========================================================================
SQArchive *GetSaveArchiveObj () {
  return feCurrArchive;
}


//==========================================================================
//
//  IsSaveArchiveErrored
//
//==========================================================================
bool IsSaveArchiveErrored () {
  return (feCurrArchive ? feCurrArchive->WasError() : false);
}


//==========================================================================
//
//  IsSaveArchiveOpened
//
//==========================================================================
bool IsSaveArchiveOpened () {
  return !!feCurrArchive;
}


//==========================================================================
//
//  GetSaveArchiveTransactionCount
//
//==========================================================================
int GetSaveArchiveTransactionCount () {
  return (feCurrArchive ? feSQTransactionCount : 0);
}


//==========================================================================
//
//  SaveArchiveBeginTransaction
//
//==========================================================================
bool SaveArchiveBeginTransaction () {
  if (feCurrArchive) {
    #ifdef FE_DEBUG_SQA
    fprintf(stderr, "SVA-B: tc=%d; err=%d\n", feSQTransactionCount, (int)feCurrArchive->WasError());
    #endif
    if (feSQTransactionCount == 0) {
      feCurrArchive->OpenTransaction();
    }
    feSQTransactionCount += 1;
    return !feCurrArchive->WasError();
  }
  return false;
}


//==========================================================================
//
//  SaveArchiveEndTransaction
//
//==========================================================================
bool SaveArchiveEndTransaction () {
  if (feCurrArchive) {
    #ifdef FE_DEBUG_SQA
    fprintf(stderr, "SVA-E: tc=%d; err=%d\n", feSQTransactionCount, (int)feCurrArchive->WasError());
    #endif
    if (feSQTransactionCount != 0) {
      feSQTransactionCount -= 1;
      if (feSQTransactionCount == 0) {
        feCurrArchive->CloseTransaction();
      }
    } else {
      feCurrArchive->AbortTransaction();
      feCurrArchive->SetError();
    }
    return !feCurrArchive->WasError();
  }
  return false;
}


//==========================================================================
//
//  SaveArchiveAbortAllTransactions
//
//==========================================================================
void SaveArchiveAbortAllTransactions () {
  if (feCurrArchive) {
    feSQTransactionCount = 0;
    feCurrArchive->AbortTransaction();
    feCurrArchive->SetError();
  }
}


//==========================================================================
//
//  CloseSaveArchive
//
//  return `false` if there were any errors
//
//==========================================================================
bool CloseSaveArchive () {
  bool res = true;
  if (feCurrArchive) {
    #ifdef FE_DEBUG_SQA
    fprintf(stderr, "SQA: closing archive.\n");
    #endif
    feCurrArchive->Close();
    if (feCurrArchive->WasError()) res = false;
    delete feCurrArchive;
  }
  feCurrArchive = 0;
  feSQTransactionCount = 0;
  return res;
}


//==========================================================================
//
//  SaveArchiveVacuum
//
//==========================================================================
void SaveArchiveVacuum () {
  if (feCurrArchive && !feCurrArchive->WasError()) {
    feCurrArchive->Vacuum();
  }
}


//==========================================================================
//
//  OpenSaveArchive
//
//==========================================================================
bool OpenSaveArchive (cfestring &filename, bool allowCreate) {
  CloseSaveArchive();
  if (filename.IsEmpty()) return false;
  if (!allowCreate && !inputfile::fileExists(filename)) {
    return false;
  }
  feCurrArchive = new SQArchive();
  feCurrArchive->Open(filename, allowCreate);
  if (feCurrArchive->WasError()) {
    delete feCurrArchive;
    feCurrArchive = 0;
    return false;
  } else {
    #ifdef FE_DEBUG_SQA
    fprintf(stderr, "SQA: opened archive '%s'.\n", filename.CStr());
    #endif
    return true;
  }
}


//#define FE_DEBUG_NO_COMPRESSION

#define FE_WRITE_ABORT

#ifdef FE_WRITE_ABORT
# define FWERROR(msg_)  ABORT("%s", msg_)
#else
# define FWERROR(msg_)  throw new FileError(CONST_S(msg_))
#endif


// ////////////////////////////////////////////////////////////////////////// //
class RawDiskFile : public RawFile {
private:
  FILE *flptr;
  VFile vfd;
  SQAFile sqfd;
  bool mWriteMode;

public:
  RawDiskFile (cfestring &aFileName, int complevel=9, bool awritemode=false);
  RawDiskFile (int dummy, cfestring &aFileName, int complevel, bool awritemode);
  virtual ~RawDiskFile ();

  virtual bool isOpen () const override;
  virtual bool canRead ()  const override;
  virtual bool canWrite () const override;
  virtual bool canSeek () const override;
  virtual bool isEof () override;
  virtual int read (void *buf, int len) override;
  virtual int write (const void *buf, int len) override;
  virtual int tell () override;
  virtual int seek (int ofs, int whence) override;
  virtual bool flush () override;
  virtual bool close () override;

private:
  int calcFileSize ();
};


RawDiskFile::RawDiskFile (cfestring &aFileName, int complevel, bool awritemode) {
  vfd = 0; sqfd = 0; flptr = 0;
  if (complevel <= 0 && awritemode) {
    flptr = fopen(aFileName.CStr(), (awritemode ? "wb" : "rb"));
  } else {
    if (!awritemode && !aFileName.IsEmpty() && !vw_is_absolute_path(aFileName.CStr())) {
      vfd = vwopen(aFileName.CStr());
    }
    if (vfd == 0) {
      flptr = fopen(aFileName.CStr(), (awritemode ? "wb" : "rb"));
    }
  }
  mWriteMode = awritemode;
  //ConLogf("RawDiskFile(\"%s\", %s, %s) -> %s", aFileName.CStr(),
  //        (maxcomp ? "true" : "false"), (awritemode ? "true" : "false"), (flptr ? "tan" : "ona"));
}


RawDiskFile::RawDiskFile (int dummy, cfestring &aFileName, int complevel, bool awritemode) {
  vfd = 0; sqfd = 0; flptr = 0;
  if (awritemode) {
    if (feCurrArchive) {
      sqfd = feCurrArchive->FileCreate(aFileName, complevel);
    } else {
      ABORT("cannot create file '%s' out of the archive!", aFileName.CStr());
    }
    mWriteMode = true;
  } else {
    if (feCurrArchive) {
      sqfd = feCurrArchive->FileOpen(aFileName);
    } else {
      ABORT("cannot open file '%s' out of the archive!", aFileName.CStr());
    }
    mWriteMode = false;
  }
}


RawDiskFile::~RawDiskFile () {
  close();
}


bool RawDiskFile::isOpen () const { return (vfd != 0 || sqfd != 0 || flptr != nullptr); }
bool RawDiskFile::canRead ()  const { return !mWriteMode; }
bool RawDiskFile::canWrite () const { return mWriteMode; }
bool RawDiskFile::canSeek () const { return true;/*!mWriteMode;*/ }


bool RawDiskFile::isEof () {
  if (isOpen()) {
    if (vfd != 0) return (vwsize(vfd) == vwtell(vfd));
    if (sqfd != 0) {
      // archive might be closed due to exception
      if (feCurrArchive) {
        return (feCurrArchive->FileTell(sqfd) == feCurrArchive->FileSize(sqfd));
      } else {
        return true;
      }
    }
    return !!feof(flptr);
  } else {
    return false;
  }
}


int RawDiskFile::read (void *buf, int len) {
  if (!isOpen()) return -1;
  if (!canRead()) return -1;
  if (len <= 0) return -1;
  if (vfd != 0) return vwread(vfd, buf, len);
  if (sqfd != 0) {
    // archive might be closed due to exception
    if (feCurrArchive) {
      int fpos = feCurrArchive->FileTell(sqfd);
      int fsize = feCurrArchive->FileSize(sqfd);
      if (fpos >= fsize) return 0;
      if (fsize - fpos < len) len = fsize - fpos;
      feCurrArchive->FileRead(sqfd, buf, (size_t)len);
      return (feCurrArchive->WasError() ? -1 : len);
    } else {
      return -1;
    }
  }
  auto rd = fread(buf, 1, len, flptr);
  if (rd < 0) return -1;
  return (int)rd;
}


int RawDiskFile::write (const void *buf, int len) {
  if (!isOpen()) return -1;
  if (!canWrite()) return -1;
  if (len <= 0) return -1;
  if (vfd != 0) return -1;
  if (sqfd != 0) {
    // archive might be closed due to exception
    if (feCurrArchive) {
      feCurrArchive->FileWrite(sqfd, buf, (size_t)len);
      return len;
    } else {
      return -1;
    }
  }
  auto wr = fwrite(buf, 1, len, flptr);
  if (wr < 0) return -1;
  return (int)wr;
}


int RawDiskFile::tell () {
  if (vfd != 0) return vwtell(vfd);
  if (sqfd != 0) {
    // archive might be closed due to exception
    if (feCurrArchive) {
      return feCurrArchive->FileTell(sqfd);
    } else {
      return -1;
    }
  }
  if (isOpen()) {
    return (int)ftell(flptr);
  } else {
    return 0;
  }
}


int RawDiskFile::calcFileSize () {
  if (vfd != 0) return vwsize(vfd);
  if (sqfd != 0) {
    // archive might be closed due to exception
    if (feCurrArchive) {
      return feCurrArchive->FileSize(sqfd);
    } else {
      return -1;
    }
  }
  if (flptr) {
    long res;
    auto opos = ftell(flptr);
    if (opos == -1) {
      res = -2;
    } else {
      if (fseek(flptr, 0, SEEK_END) == -1) {
        res = -2;
      } else {
        res = ftell(flptr);
        if (res == -1) res = -2;
      }
      fseek(flptr, opos, SEEK_SET);
    }
    return (int)res;
  }
  return -1;
}


int RawDiskFile::seek (int ofs, int whence) {
  if (!isOpen()) return -1;
  if (whence == SeekCur && ofs == 0) return tell(); // special case
  if (!canSeek()) return -1;
  switch (whence) {
    case SeekSet:
      break;
    case SeekCur:
      ofs += tell();
      break;
    case SeekEnd:
      {
        int sz = calcFileSize();
        if (sz == -2) return -1;
        ofs += sz;
        break;
      }
    default: return -1;
  }
  if (ofs < 0) return -1;
  if (vfd != 0) {
    if (vwseek(vfd, ofs) != 0) return -1;
    return ofs;
  }
  if (sqfd != 0) {
    // archive might be closed due to exception
    if (feCurrArchive) {
      feCurrArchive->FileSeek(sqfd, ofs);
      return (feCurrArchive->WasError() ? -1 : feCurrArchive->FileTell(sqfd));
    } else {
      return -1;
    }
  }
  if (fseek(flptr, ofs, SEEK_SET) == -1) return -1;
  return ftell(flptr);
}


bool RawDiskFile::flush () {
  if (!isOpen()) return false;
  if (!canWrite()) return true;
  if (vfd != 0) return true;
  if (sqfd != 0) return true;
  return (fflush(flptr) != -1);
}


bool RawDiskFile::close () {
  if (vfd != 0) {
    vwclose(vfd);
    vfd = 0;
  }
  if (sqfd != 0) {
    // archive might be closed due to exception
    if (feCurrArchive) {
      feCurrArchive->FileClose(sqfd);
      if (feCurrArchive->WasError()) {
        sqfd = 0;
        return false;
      }
    }
    sqfd = 0;
  }
  if (!isOpen()) return true;
  int res = fclose(flptr); flptr = nullptr;
  return (res == 0);
}


// ////////////////////////////////////////////////////////////////////////// //
// will take ownership of afl
inoutfile::inoutfile (RawFile *afl, cfestring &aFileName) : sdata(nullptr) {
  initialize(afl, aFileName);
}


void inoutfile::initialize (RawFile *afl, cfestring &aFileName) {
  Close();
  if (afl) {
    sdata = new SharedData;
    sdata->rc = 1;
    sdata->FileName = aFileName;
    sdata->flptr = afl;
    //ConLogf("inoutfile::initialize(\"%s\") -> %s", sdata->FileName.CStr(),
    //        (afl->isOpen() ? "tan" : "ona"));
  }
}


void inoutfile::WriteBytes (const void *buf, sLong size) {
  if (size < 0) FWERROR("cannot write negative number of bytes");
  if (IsOpen()) {
    if (!sdata->flptr->canWrite()) FWERROR("cannot write to read-only file");
    if (size == 0) return;
    const uint8_t *wb = (const uint8_t *)buf;
    while (size > 0) {
      auto wr = sdata->flptr->write(wb, size);
      if (wr <= 0) FWERROR("write error");
      wb += wr;
      size -= wr;
    }
  } else {
    FWERROR("cannot write to closed file");
  }
}


void inoutfile::ReadBytes (void *buf, sLong size) {
  if (size < 0) throw new FileError(CONST_S("cannot read negative number of bytes"));
  if (IsOpen()) {
    if (!sdata->flptr->canRead()) throw new FileError(CONST_S("cannot read from write-only file"));
    if (size == 0) return;
    uint8_t *rb = (uint8_t *)buf;
    while (size > 0) {
      auto rd = sdata->flptr->read(rb, size);
      if (rd <= 0) throw new FileError(CONST_S("read error"));
      rb += rd;
      size -= rd;
    }
  } else {
    throw new FileError(CONST_S("cannot read from closed file"));
  }
}


int inoutfile::Get () {
  if (sdata && sdata->flptr) {
    uint8_t ch;
    auto res = sdata->flptr->read(&ch, 1);
    if (res == 1) return ch;
    if (res < 0) throw new FileError(CONST_S("read error"));
  }
  return EOF;
}


void inoutfile::xseek (int ofs, int whence) {
  if (IsOpen()) {
    if (!sdata->flptr->canSeek()) {
      if (sdata->flptr->canWrite()) FWERROR("cannot seek in non-seekable file");
      else throw new FileError(CONST_S("cannot seek in non-seekable file"));
    }
    if (sdata->flptr->seek(ofs, whence) == -1) throw new FileError(CONST_S("seek error"));
  } else {
    throw new FileError(CONST_S("cannot seek in closed file"));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void outputfile::MakeDir (cfestring &name) {
#ifndef SHITDOZE
  mkdir(name.CStr(), 0755);
#else
  mkdir(name.CStr());
#endif
}


//==========================================================================
//
//  outputfile::RemoveDir
//
//==========================================================================
void outputfile::RemoveDir (cfestring &name) {
  #if 0
  ConLogf("removing directory <%s>", name.CStr());
  #endif
  std::vector<festring> flist;
  DIR *dp = opendir(name.CStr());
  if (dp) {
    struct dirent *ep;
    while ((ep = readdir(dp))) {
      /* Buffer = ep->d_name; Doesn't work because of a festring bug */
      festring buf;
      buf.Empty();
      buf << ep->d_name;
      if (buf != "." && buf != "..") {
        flist.push_back(buf);
      }
    }
    closedir(dp);
  }

  for (auto &fname : flist) {
    #if 0
    ConLogf(":::<%s>", fname.CStr());
    #endif
    festring xx = name;
    if (!xx.EndsWith("/")) xx << "/";
    xx << fname;
    remove(xx.CStr());
  }

  rmdir(name.CStr());
}


//==========================================================================
//
//  outputfile::RenameDir
//
//==========================================================================
void outputfile::RenameDir (cfestring &oldname, cfestring &newname) {
  rename(oldname.CStr(), newname.CStr());
}


// ////////////////////////////////////////////////////////////////////////// //
outputfile::outputfile (cfestring &aFileName, int complevel, truth AbortOnErr)
  : inoutfile()
{
  auto afl = new RawDiskFile(aFileName, complevel, true);
  if (afl->isOpen()) {
    initialize(afl, aFileName);
  } else {
    delete afl;
    if (AbortOnErr) ABORT("Can't open %s for output!", aFileName.CStr());
  }
}


// this will use opened vwad archive
outputfile::outputfile (int dummy, cfestring &aFileName, int complevel, truth AbortOnErr)
  : inoutfile()
{
  auto afl = new RawDiskFile(dummy, aFileName, complevel, true);
  if (afl->isOpen()) {
    initialize(afl, aFileName);
  } else {
    delete afl;
    if (AbortOnErr) ABORT("Can't open %s for output!", aFileName.CStr());
  }
}


/*
void outputfile::doSection (cfestring &secname, outputfile::SectionWriterFn writer) {
  *this << secname;
  writer(*this);
}
*/


// ////////////////////////////////////////////////////////////////////////// //
truth inputfile::fileExists (const festring &fname) {
  struct stat st;
  if (stat(fname.CStr(), &st)) return false;
  if (!S_ISREG(st.st_mode)) return false;
  return access(fname.CStr(), R_OK) == 0;
}


truth inputfile::DataFileExists (const festring &fname) {
  return (vwexists(fname.CStr()) != 0);
}


festring inputfile::GetMyDir () {
#ifdef IVAN_DATA_DIR
  return IVAN_DATA_DIR;
#else
# ifndef SHITDOZE
  char myDir[8192];
  char buf[128];
  pid_t mypid = getpid();
  memset(myDir, 0, sizeof(myDir));
  sprintf(buf, "/proc/%u/exe", (unsigned int)mypid);
  if (readlink(buf, myDir, sizeof(myDir)-1) < 0) {
    strcpy(myDir, ".");
  } else {
    char *p = (char *)strrchr(myDir, '/');
    if (!p) strcpy(myDir, "."); else *p = '\0';
  }
  if (myDir[strlen(myDir)-1] == '/') myDir[strlen(myDir)-1] = '\0';
  return festring(myDir);
# else
  return festring(".");
# endif
#endif
}


festring inputfile::buildIncludeName (cfestring &basename, cfestring incname) {
  festring xname = incname;
  while (xname.GetSize() > 0 && xname[0] == '/') xname.Erase(0, 1);
  if (xname.GetSize() == 0) ABORT("Cannot include file with empty name");
  festring res = basename;
  // remove name
  while (res.GetSize() && res[res.GetSize()-1] != '/') res.Erase(res.GetSize()-1, 1);
  res += xname;
  //ConLogf("buildIncludeName(\"%s\", \"%s\") -> \"%s\"", basename.CStr(), incname.CStr(), res.CStr());
  return res;
}


////////////////////////////////////////////////////////////////////////////////
inputfile::inputfile (cfestring &aFileName, truth AbortOnErr) : inoutfile() {
  Open(aFileName, AbortOnErr);
}


inputfile::inputfile (int dummy, cfestring &aFileName, truth AbortOnErr) : inoutfile() {
  Open(dummy, aFileName, AbortOnErr);
}


/*
festring inputfile::GetFileName () const {
  festring res;
  if (sdata) {
    festring res = sdata->FileName;
    ConLogf("inputfile::GetFileName(0): <%s>", res.CStr());
    auto mydir = GetMyDir();
    if (mydir.GetSize() > 0 && res.StartsWith(mydir)) {
      res.Erase(0, mydir.GetSize()+(mydir[mydir.GetSize()-1] != '/' ? 1 : 0));
    }
    //ConLogf("inputfile::GetFileName(1): <%s>", res.CStr());
  }
  return res;
}
*/


bool inputfile::Open (cfestring &aFileName, truth AbortOnErr) {
  Close();
  auto afl = new RawDiskFile(aFileName);
  //ConLogf("inputfile::Open(\"%s\", %s) -> %s", aFileName.CStr(), (AbortOnErr ? "true" : "false"), (afl->isOpen() ? "tan" : "ona"));
  if (afl->isOpen()) {
    initialize(afl, aFileName);
  } else {
    delete afl;
    if (AbortOnErr) ABORT("Can't open %s for input!", aFileName.CStr());
  }
  return IsOpen();
}


bool inputfile::Open (int dummy, cfestring &aFileName, truth AbortOnErr) {
  Close();
  auto afl = new RawDiskFile(dummy, aFileName, 9, false);
  //ConLogf("inputfile::Open(\"%s\", %s) -> %s", aFileName.CStr(), (AbortOnErr ? "true" : "false"), (afl->isOpen() ? "tan" : "ona"));
  if (afl->isOpen()) {
    initialize(afl, aFileName);
  } else {
    delete afl;
    if (AbortOnErr) ABORT("Can't open %s for input!", aFileName.CStr());
  }
  return IsOpen();
}


/*
void inputfile::doSection (cfestring &secname, SectionReaderFn reader) {
  festring sn;
  *this >> sn;
  if (sn != secname) throw new FileError(festring("loader: expected section '")+secname+"', got section '"+sn+"'");
  reader(*this);
}
*/


// ////////////////////////////////////////////////////////////////////////// //
outputfile &operator << (outputfile &SaveFile, cfestring &String) {
  const festring::sizetype Length = String.GetSize();
  IvanAssert(Length <= 0x40000000);
  SaveFile.writeUInt((uint32_t)Length);
  if (Length) SaveFile.WriteBytes(String.getConstDataBuffer(), Length);
  return SaveFile;
}


inputfile &operator >> (inputfile &SaveFile, festring &String) {
  const uint32_t Length = SaveFile.readUInt();
  IvanAssert(Length <= 0x40000000);
  String.ResetToLength(Length);
  if (Length) SaveFile.ReadBytes(String.getDataBuffer(), Length);
  return SaveFile;
}


outputfile &operator << (outputfile &SaveFile, cchar *String) {
  if (!String) {
    SaveFile.writeUInt((uint32_t)0);
  } else {
    const size_t Length = (String && String[0] ? strlen(String) : 0);
    IvanAssert(Length <= 0x40000000);
    SaveFile.writeUInt((uint32_t)(Length + 1));
    if (Length) SaveFile.WriteBytes(String, Length);
  }
  return SaveFile;
}


inputfile &operator >> (inputfile &SaveFile, char *&String) {
  const uint32_t Length = SaveFile.readUInt();
  if (Length == 0) {
    String = 0;
  } else {
    IvanAssert(Length <= 0x40000000 + 1);
    String = new char[Length];
    if (Length != 1) SaveFile.ReadBytes(String, Length - 1);
    String[Length] = 0;
  }
  return SaveFile;
}
