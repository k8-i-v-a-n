/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_SAVE_H__
#define __FELIB_SAVE_H__

#include "felibdef.h"
#include <ctime>
#include <cstdio>
#include <vector>
#include <stack>
#include <deque>
#include <list>
#include <map>
#include <set>
#include <functional>
#include <unordered_map>
#include <unordered_set>


#include "feerror.h"
#include "festring.h"
#include "fearray.h"
#include "sq3support.h"


// use variable-sized integers?
#define FESAVE_USE_VARINTS

// define to throw, otherwise abort
#define FESAVE_RANGE_CHECK_THROW

// write some type info to check the data?
//#define FESAVE_TYPE_CHECK_ENABLED

// write only type sizes, not full names
#define FESAVE_SIMPLIFIED_TYPE_WRITER

// this should not be required
//#define FESAVE_GENERAL_TYPE_IO


// ////////////////////////////////////////////////////////////////////////// //
bool OpenSaveArchive (cfestring &filename, bool allowCreate);
// return `false` if there were any errors
bool CloseSaveArchive ();
bool IsSaveArchiveOpened ();
bool IsSaveArchiveErrored ();

void SaveArchiveVacuum ();

// this is recursive
bool SaveArchiveBeginTransaction ();
bool SaveArchiveEndTransaction ();
void SaveArchiveAbortAllTransactions ();

int GetSaveArchiveTransactionCount ();

SQArchive *GetSaveArchiveObj ();


// will delete `aNew`.
// no transactions should be open.
struct SaveArchiveTempSwap {
private:
  SQArchive *mOld;
  int mOldTXC;
  SQArchive **mNew;
public:
  SaveArchiveTempSwap (SQArchive **aNew);
  ~SaveArchiveTempSwap ();

  SaveArchiveTempSwap (const SaveArchiveTempSwap &) = delete;
  SaveArchiveTempSwap &operator = (const SaveArchiveTempSwap &) = delete;
};


// ////////////////////////////////////////////////////////////////////////// //
/*
#define RAW_SAVE_LOAD(type) \
inline outputfile &operator << (outputfile &SaveFile, type Value) { \
  SaveFile.WriteBytes(reinterpret_cast<char *>(&Value), sizeof(Value)); \
  return SaveFile; \
} \
 \
inline inputfile &operator >> (inputfile &SaveFile, type &Value) { \
  SaveFile.ReadBytes(reinterpret_cast<char *>(&Value), sizeof(Value)); \
  return SaveFile; \
}
*/


// ////////////////////////////////////////////////////////////////////////// //
// variable-length integers, encoding and decoding

static FORCE_INLINE int DecodeVIntFastLength (uint8_t firstByte) {
  if (firstByte < 0x80) return 1;
  if (((1u << 6) & firstByte) == 0) return 2;
  if (((1u << 5) & firstByte) == 0) return 3;
  if (((1u << 4) & firstByte) == 0) return 4;
  if (firstByte == 0xf0 + 4) return 5;
  return -1;
}


// 1 to 5 bytes, never more than that.
// return number of bytes written, always `[1..5]`.
template<bool zigzag> static inline int EncodeVIntFast (int32_t val, void *destbuf) {
  uint8_t *data = (uint8_t *)destbuf; // fuck with UB!
  if (zigzag) {
    // zigzag encoding for negative integers.
    // this is UB in idiotic c/c++, but fuck with that.
    val = (int32_t)(((uint32_t)val << 1) ^ (val >> 31));
  }
  if ((uint32_t)val < 0x80u) {
    data[0] = (uint8_t)val;
    return 1;
  }
  if ((uint32_t)val < 0x4000u + 0x80u) {
    val -= 0x80;
    data[0] = (uint8_t)(0x80u | (val & 0x3f));
    data[1] = (uint8_t)((uint32_t)val >> 6);
    return 2;
  }
  if ((uint32_t)val < 0x200000u + 0x4080u) {
    val -= 0x4080;
    data[0] = (uint8_t)(0xC0u | (val & 0x1f));
    data[1] = (uint8_t)((uint32_t)val >> 5);
    data[2] = (uint8_t)((uint32_t)val >> 13);
    return 3;
  }
  if ((uint32_t)val < 0x10000000u + 0x204080u) {
    val -= 0x204080;
    data[0] = (uint8_t)(0xE0u | (val & 0x0f));
    data[1] = (uint8_t)((uint32_t)val >> 4);
    data[2] = (uint8_t)((uint32_t)val >> 12);
    data[3] = (uint8_t)((uint32_t)val >> 20);
    return 4;
  }
  // we can use low 4 bits as length here
  data[0] = (uint8_t)0xF4u;
  data[1] = (uint8_t)val;
  data[2] = (uint8_t)((uint32_t)val >> 8);
  data[3] = (uint8_t)((uint32_t)val >> 16);
  data[4] = (uint8_t)((uint32_t)val >> 24);
  return 5;
}


// return number of bytes read, or -1 for invalid encoding
template<bool zigzag> static inline int DecodeVIntFast (const void *srcbuf, int32_t *v) {
  const uint8_t *data = (const uint8_t *)srcbuf; // fuck with UB!
  uint32_t val, len;
  val = data[0];
  if (val < 0x80u) {
    len = 1;
  } else if (((1u << 6) & val) == 0) {
    len = 2;
    val = (val & 0x3fu) + (((uint32_t)data[1]) << 6) + 0x80u;
  } else if (((1u << 5) & val) == 0) {
    len = 3;
    val = (val & 0x1fu) + (((uint32_t)data[1]) << 5) + (((uint32_t)data[2]) << 13) +
          0x4080u;
  } else if (((1u << 4) & val) == 0) {
    len = 4;
    val = (val & 0x0fu) + (((uint32_t)data[1]) << 4) + (((uint32_t)data[2]) << 12) +
          (((uint32_t)data[3]) << 20) + 0x204080u;
  } else if (val == 0xF4u) {
    len = 5;
    val = ((uint32_t)data[1]) + (((uint32_t)data[2]) << 8) + (((uint32_t)data[3]) << 16) +
          (((uint32_t)data[4]) << 24);
  } else {
    return -1;
  }
  if (zigzag) {
    // zigzag encoding for negative integers.
    // this is UB in idiotic c/c++, but fuck with that.
    *v = (int32_t)((val >> 1) ^ (uint32_t)(-(int32_t)(val & 1)));
  } else {
    *v = (int32_t)val;
  }
  return len;
}



// ////////////////////////////////////////////////////////////////////////// //
class inputfile;
class outputfile;


//typedef std::map<festring, sLong> valuemap;


// ////////////////////////////////////////////////////////////////////////// //
class FileError {
public:
  festring msg;

public:
  FileError (cfestring &amsg) { msg = amsg; }
};


// ////////////////////////////////////////////////////////////////////////// //
class RawFile {
public:
  enum {
    SeekSet,
    SeekCur,
    SeekEnd,
  };

public:
  virtual ~RawFile () {}
  virtual bool isOpen () const = 0;
  virtual bool canRead () const = 0;
  virtual bool canWrite () const = 0;
  virtual bool canSeek () const = 0;
  virtual bool isEof () = 0;
  virtual int read (void *buf, int len) = 0;
  virtual int write (const void *buf, int len) = 0; // result can be less than len
  virtual int tell () = 0;
  virtual int seek (int ofs, int whence) = 0; // whence: Seek*; returns new position or -1
  virtual bool flush () = 0;
  virtual bool close () = 0;
};


// ////////////////////////////////////////////////////////////////////////// //
class inoutfile {
private:
  struct SharedData {
    int rc;
    RawFile *flptr;
    festring FileName;
  };

  FORCE_INLINE void incref () const {
    if (sdata) ++(sdata->rc);
  }

  FORCE_INLINE void decref () const {
    if (sdata) {
      if (--(sdata->rc) <= 0) {
        if (!sdata->flptr) ABORT("WTF?!");
        if (sdata->rc < 0) ABORT("WTF?!");
        sdata->flptr->close();
        delete sdata->flptr;
        sdata->flptr = nullptr; // just in case
        delete sdata;
      }
      sdata = nullptr;
    }
  }

protected:
  void initialize (RawFile *afl, cfestring &aFileName); // will take ownership of afl

public:
  inline inoutfile () : sdata(nullptr) {}
  inoutfile (RawFile *afl, cfestring &aFileName); // will take ownership of afl
  inline inoutfile (const inoutfile &afl) : sdata(afl.sdata) { incref(); }
  inline ~inoutfile () { decref(); }

  inline inoutfile &operator = (const inoutfile &afl) {
    if (afl.sdata == sdata) return *this;
    if (&afl == this) return *this;
    // order matters!
    afl.incref();
    decref();
    sdata = afl.sdata;
    return *this;
  }

  inline cfestring &GetFileName () const { return (sdata ? sdata->FileName : festring::EmptyStr()); }

  inline void Close () { decref(); }
  inline truth IsOpen () const { return (sdata && sdata->flptr && sdata->flptr->isOpen()); }
  inline truth Eof () { return (sdata && sdata->flptr ? sdata->flptr->isEof() : true); }

  FORCE_INLINE void Put (char What) { WriteBytes(&What, 1); }
  int Get (); // returns EOF on eof
  void WriteBytes (const void *buf, sLong size);
  void ReadBytes (void *buf, sLong size);
  void Flush () { if (sdata && sdata->flptr) sdata->flptr->flush(); }

  inline sLong TellPos () { return (sdata && sdata->flptr ? sdata->flptr->tell() : 0); }
  FORCE_INLINE void SeekPosBegin (sLong Offset) { xseek(Offset, RawFile::SeekSet); }
  FORCE_INLINE void SeekPosCurrent (sLong Offset) { xseek(Offset, RawFile::SeekCur); }
  FORCE_INLINE void SeekPosEnd (sLong Offset) { xseek(Offset, RawFile::SeekEnd); }

protected:
  void xseek (int ofs, int whence);

protected:
  mutable SharedData *sdata;
};


// ////////////////////////////////////////////////////////////////////////// //
class outputfile : public inoutfile {
public:
  using SectionWriterFn = std::function<void (outputfile &)>;

public:
  inline outputfile () : inoutfile() {}
  outputfile (cfestring &aFileName, int complevel=9, truth AbortOnErr=true);
  // this will use opened vwad archive
  outputfile (int dummy, cfestring &aFileName, int complevel=9, truth AbortOnErr=true);

  //void doSection (cfestring &secname, SectionWriterFn writer);

  inline void writeInt (int32_t v) {
    #ifdef FESAVE_USE_VARINTS
    uint8_t buf[5];
    const int len = EncodeVIntFast<true>(v, buf);
    WriteBytes(buf, len);
    #else
    WriteBytes(&v, 4);
    #endif
  }

  inline void writeUInt (uint32_t v) {
    #ifdef FESAVE_USE_VARINTS
    uint8_t buf[5];
    const int len = EncodeVIntFast<false>(v, buf);
    WriteBytes(buf, len);
    #else
    WriteBytes(&v, 4);
    #endif
  }

  #ifdef FESAVE_SIMPLIFIED_TYPE_WRITER
  FORCE_INLINE void writeTypeSize (int n) {
    #ifdef FESAVE_TYPE_CHECK_ENABLED
    writeInt(n);
    #endif
  }

  inline void writeSimpleBool (const bool v) { writeTypeSize(-16); const uint8_t b = (v ? 1 : 0); WriteBytes(&b, 1); }
  //inline void writeSimpleC8 (const signed char v) { writeTypeSize(1); WriteBytes(&v, 1); }
  inline void writeSimpleI8 (const int8_t v) { writeTypeSize(1); WriteBytes(&v, 1); }
  inline void writeSimpleU8 (const uint8_t v) { writeTypeSize(-1); WriteBytes(&v, 1); }
  inline void writeSimpleI16 (const int16_t v) { writeTypeSize(2); writeInt((int32_t)v); }
  inline void writeSimpleU16 (const uint16_t v) { writeTypeSize(-2); writeUInt((uint32_t)v); }
  inline void writeSimpleI32 (const int32_t v) { writeTypeSize(4); writeInt(v); }
  inline void writeSimpleU32 (const uint32_t v) { writeTypeSize(-4); writeUInt(v); }
  inline void writeSimpleI64 (const int64_t v) { writeTypeSize(8); writeUInt((uint32_t)(v&0xffffffff)); writeUInt((uint32_t)((v >> 32)&0xffffffff)); }
  inline void writeSimpleU64 (const uint64_t v) { writeTypeSize(-8); writeUInt((uint32_t)v); writeUInt((uint32_t)(v >> 32)); }
  inline void writeSimpleFloat (const float v) { writeTypeSize(-24); WriteBytes(&v, sizeof(float)); }
  inline void writeSimpleDouble (const double v) { writeTypeSize(-28); WriteBytes(&v, sizeof(double)); }
  inline void writeSimpleV2 (const v2 &v) { writeTypeSize(42+1); writeInt(v.X); writeInt(v.Y); }
  inline void writeSimplePV2 (const packv2 &v) { writeTypeSize(42+2); writeInt(v.X); writeInt(v.Y); }
  inline void writeSimpleRect (const rect &v) { writeTypeSize(42+3); writeInt(v.X1); writeInt(v.Y1); writeInt(v.X2); writeInt(v.Y2); }
  #endif

  #ifdef FESAVE_GENERAL_TYPE_IO
  template <typename T> inline void writeSimple (const T &v) {
    #ifdef FESAVE_SIMPLIFIED_TYPE_WRITER
      #ifdef FESAVE_TYPE_CHECK_ENABLED
      // write type size only
      writeInt((int32_t)(sizeof(T)) + 69);
      #endif
    #else
      // get type name
      static const char *tname = nullptr;
      static uint32_t tnamelen = 0;
      if (!tname) {
        tname = getCPPTypeNameCStr<T>();
        if (!tname) ABORT("wtf?!");
        tnamelen = (uint32_t)strlen(tname);
        if (tnamelen == 0 || tnamelen > 254) ABORT("wtf?!");
      }
      // write type name
      writeUInt(tnamelen);
      WriteBytes(tname, (sLong)tnamelen);
      // write type size
      writeUInt((uint32_t)(sizeof(T)));
    #endif
    // write value. c++ is absolutely fucked up language:
    // there is no fuckin' way to check `v` type in runtime. shit.
    WriteBytes(&v, sizeof(T));
  }
  #endif

  static void MakeDir (cfestring &name);
  static void RemoveDir (cfestring &name);
  static void RenameDir (cfestring &oldname, cfestring &newname);
};


// ////////////////////////////////////////////////////////////////////////// //
class inputfile : public inoutfile {
public:
  using SectionReaderFn = std::function<void (inputfile &)>;

public:
  inline inputfile () : inoutfile() {}
  inputfile (cfestring &aFileName, truth AbortOnErr=true);

  // open with `vwopen_ro()`
  inputfile (int dummy, cfestring &aFileName, truth AbortOnErr=true);

  bool Open (cfestring &aFileName, truth AbortOnErr=true);
  bool Open (int dummy, cfestring &aFileName, truth AbortOnErr=true);

  //void doSection (cfestring &secname, SectionReaderFn reader);

  template<bool hassign> int32_t readXInt () {
    int32_t res;
    #ifdef FESAVE_USE_VARINTS
    uint8_t buf[5];
    ReadBytes(buf, 1);
    int len = DecodeVIntFastLength(buf[0]);
    if (len < 1 || len > 5) {
      #ifdef FESAVE_RANGE_CHECK_THROW
      throw new FileError(festring("invalid variable integer in save file"));
      #else
      ABORT("invalid %s variable integer in save file (len=%d; byte=0x%02X)",
            (hassign ? "signed" : "unsigned"), len, buf[0]);
      #endif
    }
    if (len > 1) ReadBytes(&buf[1], len - 1);
    int len1 = DecodeVIntFast<hassign>(buf, &res);
    if (len1 != len) {
      #ifdef FESAVE_RANGE_CHECK_THROW
      throw new FileError(festring("invalid variable integer in save file (decoding)"));
      #else
      ABORT("invalid variable integer in save file (decoding)");
      #endif
    }
    #else
    ReadBytes(&res, 4);
    #endif
    return res;
  }

  FORCE_INLINE int32_t readInt () { return readXInt<true>(); }
  FORCE_INLINE uint32_t readUInt () { return (uint32_t)readXInt<false>(); }

  int32_t readIntMinMax (int32_t min, int32_t max) {
    const int32_t v = readInt();
    if (v < min || v > max) {
      #ifdef FESAVE_RANGE_CHECK_THROW
      throw new FileError(festring("invalid type range in save file"));
      #else
      ABORT("invalid int type range in save file: [%d:%d], got %d", min, max, v);
      #endif
    }
    return v;
  }

  uint32_t readUIntMax (uint32_t max) {
    const uint32_t v = readUInt();
    if (v > max) {
      #ifdef FESAVE_RANGE_CHECK_THROW
      throw new FileError(festring("invalid type range in save file"));
      #else
      ABORT("invalid uint type range in save file: [%u], got %u", max, v);
      #endif
    }
    return v;
  }

  #ifdef FESAVE_SIMPLIFIED_TYPE_WRITER
  FORCE_INLINE void checkTypeSize (int sz) {
    #ifdef FESAVE_TYPE_CHECK_ENABLED
    if (readInt() != sz) {
      #ifdef FESAVE_RANGE_CHECK_THROW
      throw new FileError(festring("invalid type size in save file"));
      #else
      ABORT("invalid type size in save file");
      #endif
    }
    #else
    //fprintf(stderr, "{tid=%d} ", sz); fflush(stderr);
    #endif
  }

  //TODO: better error checking
  inline void readSimpleBool (bool &v) {
    checkTypeSize(-16);
    uint8_t b = 69; ReadBytes(&b, 1);
    if (b != 0 && b != 1) {
      #ifdef FESAVE_RANGE_CHECK_THROW
      throw new FileError(festring("invalid boolean"));
      #else
      ABORT("invalid boolean in save file (b=%u)", b);
      #endif
    }
    v = (b != 0);
  }
  //inline void readSimpleC8 (signed char &v) { checkTypeSize(1); ReadBytes(&v, 1); }
  inline void readSimpleI8 (int8_t &v) { checkTypeSize(1); ReadBytes(&v, 1); }
  inline void readSimpleU8 (uint8_t &v) { checkTypeSize(-1); ReadBytes(&v, 1); }
  inline void readSimpleI16 (int16_t &v) { checkTypeSize(2); v = readIntMinMax(-32768, 32767); }
  inline void readSimpleU16 (uint16_t &v) { checkTypeSize(-2); v = readUIntMax(65535); }
  inline void readSimpleI32 (int32_t &v) { checkTypeSize(4); v = readInt(); }
  inline void readSimpleU32 (uint32_t &v) { checkTypeSize(-4); v = readUInt(); }
  inline void readSimpleI64 (int64_t &v) { checkTypeSize(8); uint32_t v0 = readUInt(); uint32_t v1 = readUInt(); v = (int64_t)(((uint64_t)v1 << 32) | v0); }
  inline void readSimpleU64 (uint64_t &v) { checkTypeSize(-8); uint32_t v0 = readUInt(); uint32_t v1 = readUInt(); v = (uint64_t)(((uint64_t)v1 << 32) | v0); }
  inline void readSimpleFloat (float &v) { checkTypeSize(-24); ReadBytes(&v, sizeof(float)); }
  inline void readSimpleDouble (double &v) { checkTypeSize(-28); ReadBytes(&v, sizeof(double)); }
  inline void readSimpleV2 (v2 &v) { checkTypeSize(42+1); v.X = readInt(); v.Y = readInt(); }
  inline void readSimplePV2 (packv2 &v) { checkTypeSize(42+2); v.X = readIntMinMax(-32768, 32767); v.Y = readIntMinMax(-32768, 32767); }
  inline void readSimpleRect (rect &v) { checkTypeSize(42+3); v.X1 = readInt(); v.Y1 = readInt(); v.X2 = readInt(); v.Y2 = readInt(); }
  #endif

  #ifdef FESAVE_GENERAL_TYPE_IO
  template <typename T> inline void readSimple (T &v) {
    #ifdef FESAVE_SIMPLIFIED_TYPE_WRITER
      #ifdef FESAVE_TYPE_CHECK_ENABLED
      checkTypeSize((int32_t)(sizeof(T)) + 69);
      #endif
    #else
      // get type name
      static const char *tname = nullptr;
      static uint32_t tnamelen = 0;
      if (!tname) {
        tname = getCPPTypeNameCStr<T>();
        if (!tname) ABORT("wtf?!");
        tnamelen = (uint32_t)strlen(tname);
        if (tnamelen == 0 || tnamelen > 254) ABORT("wtf?!");
      }
      // read and compare type name
      uint32_t rdtlen = readUInt();
      if (rdtlen == 0 || rdtlen > 254) throw new FileError(festring("expected type '")+tname+"', got some shit");
      char rdtname[256];
      ReadBytes(rdtname, (sLong)rdtlen);
      rdtname[rdtlen] = 0;
      if (rdtlen != tnamelen || strcmp(rdtname, tname) != 0) throw new FileError(festring("expected type '")+tname+"', got '"+rdtname+"' shit");
      // read type size
      if (readUInt() != sizeof(T)) throw new FileError(festring("invalid size for type '")+tname+"'");
    #endif
    // read value
    ReadBytes(&v, sizeof(T));
  }
  #endif

  static truth fileExists (const festring &fname);
  static truth DataFileExists (const festring &fname);
  static festring GetMyDir ();
  static festring buildIncludeName (cfestring &basename, cfestring incname);
};


// ////////////////////////////////////////////////////////////////////////// //
/* Reads a binary form variable of type type and returns it.
 * An inputfile template member function would be far more elegant,
 * but VC doesn't seem to understand it. */
template <class type> inline type _ReadType_ (inputfile &SaveFile, const char *tag) {
  type Variable;
  SaveFile >> Variable;
  return Variable;
}

#define ReadType(typename,fl)  _ReadType_<typename>(fl, #typename)


// ////////////////////////////////////////////////////////////////////////// //
// `<decltype(Value)>`
#define FESAVE_SCALAR_IO(type_,tn_) \
inline outputfile &operator << (outputfile &SaveFile, const type_ Value) { SaveFile.writeSimple##tn_(Value); return SaveFile; } \
inline inputfile &operator >> (inputfile &SaveFile, type_ &Value) { SaveFile.readSimple##tn_(Value); return SaveFile; }

FESAVE_SCALAR_IO(bool, Bool)
//FESAVE_SCALAR_IO(signed char, C8)
FESAVE_SCALAR_IO(sByte, I8)
FESAVE_SCALAR_IO(uChar, U8)
FESAVE_SCALAR_IO(short, I16)
FESAVE_SCALAR_IO(uShort, U16)
FESAVE_SCALAR_IO(int, I32)
FESAVE_SCALAR_IO(uInt, U32)
FESAVE_SCALAR_IO(int64_t, I64)
FESAVE_SCALAR_IO(uint64_t, U64)
FESAVE_SCALAR_IO(float, Float)
FESAVE_SCALAR_IO(double, Double)
FESAVE_SCALAR_IO(v2, V2)
FESAVE_SCALAR_IO(packv2, PV2)
FESAVE_SCALAR_IO(rect, Rect)

inline outputfile &operator << (outputfile &SaveFile, time_t Value) { SaveFile.writeSimpleU64((uint64_t)Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, time_t &Value) { uint64_t v; SaveFile.readSimpleU64(v); Value = (time_t)v; return SaveFile; }
//inline outputfile &operator << (outputfile &SaveFile, const packv2 &Value) { SaveFile.writeSimplePV2(Value); return SaveFile; }
//inline inputfile &operator >> (inputfile &SaveFile, packv2 &Value) { SaveFile.readSimplePV2(Value); return SaveFile; }
//inline outputfile &operator << (outputfile &SaveFile, const v2 &Value) { SaveFile.writeSimpleV2(Value); return SaveFile; }
//inline inputfile &operator >> (inputfile &SaveFile, v2 &Value) { SaveFile.readSimpleV2(Value); return SaveFile; }
//inline outputfile &operator << (outputfile &SaveFile, const rect &Value) { SaveFile.writeSimpleRect(Value); return SaveFile; }
//inline inputfile &operator >> (inputfile &SaveFile, rect &Value) { SaveFile.readSimpleRect(Value); return SaveFile; }

/*
inline outputfile &operator << (outputfile &SaveFile, bool Value) { SaveFile.writeSimpleBool(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, bool &Value) { SaveFile.readSimpleBool(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, char Value) { SaveFile.writeSimpleC8(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, char &Value) { SaveFile.readSimpleC8(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, uChar Value) { SaveFile.writeSimpleU8(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, uChar &Value) { SaveFile.readSimpleU8(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, short Value) { SaveFile.writeSimpleI16(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, short &Value) { SaveFile.readSimpleI16(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, uShort Value) { SaveFile.writeSimpleU16(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, uShort &Value) { SaveFile.readSimpleU16(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, int Value) { SaveFile.writeSimpleI32(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, int &Value) { SaveFile.readSimpleI32(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, uInt Value) { SaveFile.writeSimpleU32(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, uInt &Value) { SaveFile.readSimpleU32(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, int64_t Value) { SaveFile.writeSimpleI64(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, int64_t &Value) { SaveFile.readSimpleI64(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, uint64_t Value) { SaveFile.writeSimpleU64(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, uint64_t &Value) { SaveFile.readSimpleU64(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, time_t Value) { SaveFile.writeSimpleU64((uint64_t)Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, time_t &Value) { uint64_t v; SaveFile.readSimpleU64(v); Value = (time_t)v; return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, float Value) { SaveFile.writeSimpleFloat(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, float &Value) { SaveFile.readSimpleFloat(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, double Value) { SaveFile.writeSimpleDouble(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, double &Value) { SaveFile.readSimpleDouble(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, const packv2 &Value) { SaveFile.writeSimplePV2(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, packv2 &Value) { SaveFile.readSimplePV2(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, const v2 &Value) { SaveFile.writeSimpleV2(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, v2 &Value) { SaveFile.readSimpleV2(Value); return SaveFile; }
inline outputfile &operator << (outputfile &SaveFile, const rect &Value) { SaveFile.writeSimpleRect(Value); return SaveFile; }
inline inputfile &operator >> (inputfile &SaveFile, rect &Value) { SaveFile.readSimpleRect(Value); return SaveFile; }
*/

outputfile &operator << (outputfile &, cfestring &);
inputfile &operator >> (inputfile &, festring &);
outputfile &operator << (outputfile &, cchar *);
inputfile &operator >> (inputfile &, char *&);


// ////////////////////////////////////////////////////////////////////////// //
// std::pair
// ////////////////////////////////////////////////////////////////////////// //
template <class type1, class type2> inline outputfile &operator << (outputfile &SaveFile, const std::pair<type1, type2> &Pair) {
  SaveFile << Pair.first << Pair.second;
  return SaveFile;
}

template <class type1, class type2> inline inputfile &operator >> (inputfile &SaveFile, std::pair<type1, type2> &Pair) {
  SaveFile >> Pair.first >> Pair.second;
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// std:vector
// ////////////////////////////////////////////////////////////////////////// //
template <class type> inline outputfile &operator << (outputfile &SaveFile, const std::vector<type> &Vector) {
  const feuLong len = (feuLong)Vector.size();
  SaveFile << len;
  for (feuLong c = 0; c != len; c += 1) SaveFile << Vector[c];
  return SaveFile;
}

template <class type> inline inputfile &operator >> (inputfile &SaveFile, std::vector<type> &Vector) {
  Vector.resize(ReadType(feuLong, SaveFile), type());
  const feuLong len = (feuLong)Vector.size();
  for (feuLong c = 0; c != len; c += 1) SaveFile >> Vector[c];
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// std::deque
// ////////////////////////////////////////////////////////////////////////// //
template <class type> inline outputfile &operator << (outputfile &SaveFile, const std::deque<type> &Deque) {
  const feuLong len = (feuLong)Deque.size();
  SaveFile << len;
  for (feuLong c = 0; c != len; c += 1) SaveFile << Deque[c];
  return SaveFile;
}

template <class type> inline inputfile &operator >> (inputfile &SaveFile, std::deque<type> &Deque) {
  Deque.resize(ReadType(feuLong, SaveFile), type());
  const feuLong len = (feuLong)Deque.size();
  for (feuLong c = 0; c != len; c += 1) SaveFile >> Deque[c];
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// std::list
// ////////////////////////////////////////////////////////////////////////// //
template <class type> inline outputfile &operator << (outputfile &SaveFile, const std::list<type> &List) {
  const feuLong len = (feuLong)List.size();
  SaveFile << len;
  for (typename std::list<type>::const_iterator i = List.begin(); i != List.end(); ++i) {
    SaveFile << *i;
  }
  return SaveFile;
}

template <class type> inline inputfile &operator >> (inputfile &SaveFile, std::list<type> &List) {
  List.resize(ReadType(feuLong, SaveFile), type());
  for (typename std::list<type>::iterator i = List.begin(); i != List.end(); ++i) SaveFile >> *i;
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// std::map
// ////////////////////////////////////////////////////////////////////////// //
template <class type1, class type2> inline outputfile &operator << (outputfile &SaveFile, const std::map<type1, type2> &Map) {
  const feuLong len = (feuLong)Map.size();
  SaveFile << len;
  for (typename std::map<type1, type2>::const_iterator i = Map.begin(); i != Map.end(); ++i) {
    SaveFile << i->first << i->second;
  }
  return SaveFile;
}

template <class type1, class type2> inline inputfile &operator >> (inputfile &SaveFile, std::map<type1, type2> &Map) {
  Map.clear();
  type1 First;
  feuLong Size;
  SaveFile >> Size;
  typename std::map<type1, type2>::iterator i;
  for (feuLong c = 0; c != Size; c += 1) {
    SaveFile >> First;
    i = Map.insert(Map.end(), std::make_pair(First, type2()));
    SaveFile >> i->second;
  }
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// std::unordered_map
// ////////////////////////////////////////////////////////////////////////// //
template <class type1, class type2> inline outputfile &operator << (outputfile &SaveFile, const std::unordered_map<type1, type2> &Map) {
  const feuLong len = (feuLong)Map.size();
  SaveFile << len;
  for (typename std::unordered_map<type1, type2>::const_iterator i = Map.begin(); i != Map.end(); ++i) {
    SaveFile << i->first << i->second;
  }
  return SaveFile;
}

template <class type1, class type2> inline inputfile &operator >> (inputfile &SaveFile, std::unordered_map<type1, type2> &Map) {
  Map.clear();
  type1 First;
  feuLong Size;
  SaveFile >> Size;
  typename std::unordered_map<type1, type2>::iterator i;
  for (feuLong c = 0; c != Size; c += 1) {
    SaveFile >> First;
    i = Map.insert(Map.end(), std::make_pair(First, type2()));
    SaveFile >> i->second;
  }
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// std::set
// ////////////////////////////////////////////////////////////////////////// //
template <class type> inline outputfile &operator << (outputfile &SaveFile, const std::set<type> &Set) {
  const feuLong len = (feuLong)Set.size();
  SaveFile << len;
  for (typename std::set<type>::const_iterator i = Set.begin(); i != Set.end(); ++i) {
    SaveFile << *i;
  }
  return SaveFile;
}

template <class type> inline inputfile &operator >> (inputfile &SaveFile, std::set<type> &Set) {
  Set.clear();
  feuLong Size;
  SaveFile >> Size;
  for (feuLong c = 0; c != Size; c += 1) {
    type Value;
    SaveFile >> Value;
    Set.insert(Value);
  }
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// std::unordered_set
// ////////////////////////////////////////////////////////////////////////// //
template <class type> inline outputfile &operator << (outputfile &SaveFile, const std::unordered_set<type> &Set) {
  const feuLong len = (feuLong)Set.size();
  SaveFile << len;
  for (typename std::unordered_set<type>::const_iterator i = Set.begin(); i != Set.end(); ++i) {
    SaveFile << *i;
  }
  return SaveFile;
}

template <class type> inline inputfile &operator >> (inputfile &SaveFile, std::unordered_set<type> &Set) {
  Set.clear();
  feuLong Size;
  SaveFile >> Size;
  for (feuLong c = 0; c != Size; c += 1) {
    type Value;
    SaveFile >> Value;
    Set.insert(Value);
  }
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// fearray
// ////////////////////////////////////////////////////////////////////////// //
template <class type> inline outputfile &operator << (outputfile &SaveFile, const fearray<type> &Array) {
  typename fearray<type>::sizetype c, Size = Array.Size;
  SaveFile << Size;
  for (c = 0; c < Size; ++c) SaveFile << Array[c];
  return SaveFile;
}

template <class type> inline inputfile &operator >> (inputfile &SaveFile, fearray<type> &Array) {
  typename fearray<type>::sizetype c, Size;
  SaveFile >> Size;
  Array.Allocate(Size);
  for (c = 0; c < Size; ++c) SaveFile >> Array[c];
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// `type *`
// ////////////////////////////////////////////////////////////////////////// //
template <class type> inline outputfile &SaveLinkedList (outputfile &SaveFile, const type *Element) {
  for (const type* E = Element; E; E = E->Next) {
    SaveFile.Put(true);
    SaveFile << E;
  }
  SaveFile.Put(false);
  return SaveFile;
}

template <class type> inline inputfile &LoadLinkedList(inputfile &SaveFile, type *&Element) {
  if (SaveFile.Get()) {
    SaveFile >> Element;
    type *E;
    for (E = Element; SaveFile.Get(); E = E->Next) SaveFile >> E->Next;
    E->Next = 0;
  } else {
    Element = 0;
  }
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// `type *`, `count`
// ////////////////////////////////////////////////////////////////////////// //
template <class type> inline outputfile &SaveArray (outputfile &SaveFile, const type *Array, int Count) {
  for (int c = 0; c < Count; ++c) SaveFile << Array[c];
  return SaveFile;
}

template <class type> inline inputfile &LoadArray (inputfile &SaveFile, type *Array, int Count) {
  for (int c = 0; c < Count; ++c) SaveFile >> Array[c];
  return SaveFile;
}


#endif
