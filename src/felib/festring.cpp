/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include <cctype>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "festring.h"
#include "allocate.h"
#include "feerror.h"


const char *festring::EmptyString = "";
festring::csizetype festring::NPos = festring::sizetype(-1);
festring festring::mEmptyString = CONST_S("");


//==========================================================================
//
//  RawTextData::SkipChar
//
//==========================================================================
char RawTextData::SkipChar () {
  if (length == 0) return 0;

  char ch = *text;
  text += 1; length -= 1;

  if (ch == 1) {
    int hdigs[6]; int hdcount;
    if (length == 0) {
      lastClr[0] = 2;
      lastClr[1] = 0;
      return 0;
    }
    ch = *text; text += 1; length -= 1;
    lastClr[0] = 1;
    lastClr[1] = ch;
    int ccpos = 2;
    bool perma = false;
    switch (ch) {
      case 'R': curColor = RED; break;
      case 'G': curColor = GREEN; break;
      case 'B': curColor = BLUE; break;
      case 'Y': curColor = YELLOW; break;
      case 'W': curColor = WHITE; break; //(Color != WHITE ? WHITE : LIGHT_GRAY); break;
      case 'C': curColor = CYAN; break;
      case 'P': curColor = PINK; break;
      case 'D': curColor = DARK_GRAY; break;
      case 'L': curColor = LIGHT_GRAY; break;
      case 'S': curColor = SEL_BLUE; break;
      case 'O': curColor = ORANGE; break;
      case '!': Color = curColor; perma = true; break;
      case '#':
        hdcount = 0;
        while (length != 0 && hdcount != 6 && RawTextData::HexDigit(text[0]) >= 0) {
          lastClr[ccpos++] = text[0];
          hdigs[hdcount] = RawTextData::HexDigit(text[0]);
          hdcount += 1;
          text += 1; length -= 1;
        }
        if (hdcount == 3) {
          curColor = MakeRGB16(hdigs[0] * 16 + hdigs[0],
                               hdigs[1] * 16 + hdigs[1],
                               hdigs[2] * 16 + hdigs[2]);
        } else if (hdcount == 6) {
          curColor = MakeRGB16(hdigs[0] * 16 + hdigs[1],
                               hdigs[2] * 16 + hdigs[3],
                               hdigs[4] * 16 + hdigs[5]);
        } else {
          ccpos = 0;
        }
        if (length != 0) {
          switch (text[0]) {
            case '|':
              lastClr[ccpos++] = '|'; text += 1; length -= 1;
              break;
            case '!':
              lastClr[ccpos++] = '!'; text += 1; length -= 1;
              Color = curColor;
              perma = true;
              break;
          }
          if (ccpos == 1) ccpos = 0;
        }
        break;
      default:
        ccpos = 0;
        break;
    }
    if (ccpos < 2) {
      lastClr[0] = 2;
      lastClr[1] = 0;
    } else {
      lastClr[ccpos] = 0;
    }
    if (perma) strcpy(permaClr, lastClr);
    return 0;
  }

  if (ch == 2) {
    curColor = Color;
    if (permaClr[0]) {
      strcpy(lastClr, permaClr);
    } else {
      lastClr[0] = 2; lastClr[1] = 0;
    }
    return 0;
  }

  return ch;
}


// ////////////////////////////////////////////////////////////////////////// //
// festring
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  festring::CreateOwnData
//
//==========================================================================
void festring::CreateOwnData (cchar *CStr, sizetype N) {
  if (N == 0) { Empty(); return; }
  char *optr = Data;
  Size = N;
  Reserved = AlignReserved(N);
  Data = allocData(Reserved);
  if (CStr) memmove(Data, CStr, N); else memset(Data, 0, N);
  if (OwnsData) {
    if (!optr) ABORT("The thing that should not be!");
    if (DecRefPtr(optr)) DeleteDataPtr(optr);
  }
  OwnsData = true;
}


//==========================================================================
//
//  festring::EnsureUnique
//
//==========================================================================
void festring::EnsureUnique () {
  if (Size == 0) { Empty(); return; }
  if (!Data) ABORT("The thing that should not be!");
  if (OwnsData && IsUnique()) return;
  CreateOwnData(Data, Size);
}


//==========================================================================
//
//  festring::CStr
//
//==========================================================================
cchar *festring::CStr () const {
  if (Data && Size != 0) {
    // it is always safe to write zero into owned data
    // invariant: non-owned data is ALWAYS asciiz
    if (OwnsData) {
      if (Size > Reserved) {
        fprintf(stderr, "FESTRING: Size=%u; Reserved=%d; wtf?!\n",
                (unsigned)Size, (unsigned)Reserved);
        __builtin_trap();
      }
      Data[Size] = 0;
    }
    return Data;
  }
  return EmptyString;
}


//==========================================================================
//
//  festring::Empty
//
//==========================================================================
void festring::Empty () {
  if (OwnsData) {
    if (DecRef()) DeleteData();
  }
  Data = nullptr;
  Size = 0;
  Reserved = 0;
  OwnsData = false;
}


//==========================================================================
//
//  festring::SlowAppend
//
//==========================================================================
void festring::SlowAppend (cchar *CStr, sizetype N) {
  if (N == 0) return;
  if (!CStr) ABORT("festring::SlowAppend() with null string and non-zero size (%u)!", (unsigned)N);
  if (Size == 0) { CreateOwnData(CStr, N); return; }
  char *OldPtr = Data;
  if (OwnsData && !OldPtr) ABORT("The thing that should not be!");
  sizetype OldSize = Size;
  sizetype NewSize = OldSize + N;
  Size = NewSize;
  if (OwnsData && IsUnique() && NewSize <= Reserved) {
    memmove(OldPtr + OldSize, CStr, N);
  } else {
    void *DeletePtr = nullptr;
    if (OwnsData) {
      if (DecRef()) DeletePtr = OldPtr;
    }
    Reserved = AlignReserved(NewSize);
    char *NewPtr = allocData(Reserved);
    Data = NewPtr;
    if (OldSize > 0) memmove(NewPtr, OldPtr, OldSize);
    memmove(NewPtr + OldSize, CStr, N);
    DeleteDataPtr(DeletePtr);
    OwnsData = true;
  }
}


//==========================================================================
//
//  festring::AppendIntr
//
//==========================================================================
festring &festring::AppendIntr (cchar *CStr, sizetype N) {
  if (N == 0) return *this;
  sizetype OldSize = Size;
  sizetype NewSize = OldSize+N;
  char *OldPtr = Data;
  if (OwnsData && IsUnique() && NewSize <= Reserved) {
    memmove(OldPtr + OldSize, CStr, N);
    Size = NewSize;
  } else {
    SlowAppend(CStr, N);
  }
  return *this;
}


//==========================================================================
//
//  festring::AppendChar
//
//==========================================================================
festring &festring::AppendChar (char ch) {
  return AppendIntr(&ch, 1);
}


//==========================================================================
//
//  festring::AppendBuf
//
//==========================================================================
festring &festring::AppendBuf (const void *buf, int len) {
  if (buf && len > 0) {
    return AppendIntr((cchar *)buf, (sizetype)len);
  } else {
    return *this;
  }
}


//==========================================================================
//
//  festring::AppendStr
//
//==========================================================================
festring &festring::AppendStr (cchar *s) {
  if (s && s[0]) {
    return AppendIntr(s, (sizetype)strlen(s));
  } else {
    return *this;
  }
}


//==========================================================================
//
//  festring::operator =
//
//==========================================================================
festring &festring::operator = (cfestring &Str) {
  if (&Str == this) return *this;
  if (Data == Str.Data) {
    if (OwnsData && Data) IncRef();
    return *this;
  }
  // clear this string
  Empty();
  if (Str.Size == 0) return *this;
  char *StrPtr = Str.Data;
  if (!StrPtr) ABORT("The thing that should not be!");
  Data = StrPtr;
  Size = Str.Size;
  OwnsData = Str.OwnsData;
  Reserved = Str.Reserved;
  if (OwnsData) IncRef();
  return *this;
}


//==========================================================================
//
//  festring::Capitalize
//
//  Size must be > 0
//
//==========================================================================
festring &festring::Capitalize () {
  if (Size > 0) {
    festring::sizetype pos = 0;
    while (pos != Size && (Data[pos] <= 32 || Data[pos] >= 127)) pos += 1;
    if (pos != Size && Data[pos] >= 'a' && Data[pos] <= 'z') {
      EnsureUnique();
      Data[pos] -= 0x20;
    }
  }
  return *this;
}


//==========================================================================
//
//  IsUninterestingWord
//
//==========================================================================
static bool IsUninterestingWord (const char *s, festring::sizetype len) {
  if (len == 1) {
    return (festring::CharUpper(s[0]) == 'A');
  }
  if (len == 2) {
    return
      (festring::CharUpper(s[0]) == 'O' && festring::CharUpper(s[1]) == 'F') ||
      (festring::CharUpper(s[0]) == 'A' && festring::CharUpper(s[1]) == 'N') ||
      (festring::CharUpper(s[0]) == 'I' && festring::CharUpper(s[1]) == 'S') ||
      (festring::CharUpper(s[0]) == 'I' && festring::CharUpper(s[1]) == 'N') ||
      false;
  }
  if (len == 3) {
    return
      (festring::CharUpper(s[0]) == 'T' && festring::CharUpper(s[1]) == 'H' && festring::CharUpper(s[2]) == 'E');
  }
  return false;
}


//==========================================================================
//
//  NeedCapAll
//
//==========================================================================
static bool NeedCapAll (const char *str, festring::sizetype len) {
  festring::sizetype pos = 0;
  while (pos != len) {
    char ch = str[pos];
    if (ch <= 32 || ch >= 127) {
      pos += 1;
    } else if (ch >= 'a' && ch <= 'z') {
      festring::sizetype epos = pos + 1;
      while (epos != len && str[epos] > 32 && str[epos] < 127) epos += 1;
      if (!IsUninterestingWord(str + pos, epos - pos)) return true;
      pos = epos;
    } else {
      // skip the word
      while (pos != len && str[pos] > 32 && str[pos] < 127) pos += 1;
    }
  }
  return false;
}


//==========================================================================
//
//  festring::CapitalizeAll
//
//==========================================================================
festring &festring::CapitalizeAll () {
  if (NeedCapAll(Data, Size)) {
    sizetype pos = 0;
    csizetype len = Size;
    EnsureUnique();
    while (pos != len) {
      char ch = Data[pos];
      if (ch <= 32 || ch >= 127) {
        pos += 1;
      } else if (ch >= 'a' && ch <= 'z') {
        sizetype epos = pos + 1;
        while (epos != len && Data[epos] > 32 && Data[epos] < 127) epos += 1;
        if (!IsUninterestingWord(Data + pos, epos - pos)) {
          Data[pos] -= 0x20;
        }
        pos = epos;
      } else {
        // skip the word
        while (pos != len && Data[pos] > 32 && Data[pos] < 127) pos += 1;
      }
    }
  }
  return *this;
}


//==========================================================================
//
//  festring::Assign
//
//==========================================================================
void festring::Assign (sizetype N, char C) {
  if (N == 0) { Empty(); return; }
  Size = N;
  char *Ptr = Data;
  if (OwnsData) {
    IvanAssert(Ptr);
    if (IsUnique() && N <= Reserved) {
      memset(Ptr, C, N);
      return;
    }
    if (DecRef()) DeleteData();
    OwnsData = false; Data = 0;
  }
  Reserved = AlignReserved(N);
  Ptr = allocData(Reserved);
  Data = Ptr;
  memset(Ptr, C, N);
  OwnsData = true;
}


//==========================================================================
//
//  festring::Resize
//
//==========================================================================
void festring::Resize (sizetype N, char C) {
  if (N == 0) { Empty(); return; }
  sizetype OldSize = Size;
  char *OldPtr = Data;
  char *NewPtr;
  Size = N;
  if (N > OldSize) {
    void *DeletePtr = nullptr;
    if (OwnsData && OldPtr) {
      if (IsUnique() && N <= Reserved) {
        memset(OldPtr + OldSize, C, N - OldSize);
        return;
      }
      if (DecRef()) DeletePtr = OldPtr;
    }
    Reserved = AlignReserved(N);
    NewPtr = allocData(Reserved);
    Data = NewPtr;
    if (OldSize > 0) memmove(NewPtr, OldPtr, OldSize);
    if (N > OldSize) memset(NewPtr + OldSize, C, N - OldSize);
    OwnsData = true;
    DeleteDataPtr(DeletePtr);
  } else {
    // we want it to be smaller or equal
    if (OwnsData && OldPtr) {
      if (IsUnique()) return;
      (void)DecRef();
    }
    Reserved = AlignReserved(N);
    NewPtr = allocData(Reserved);
    Data = NewPtr;
    if (OldPtr) memmove(NewPtr, OldPtr, N); else memset(NewPtr, C, N);
    OwnsData = true;
  }
}


//==========================================================================
//
//  festring::Find
//
//==========================================================================
festring::sizetype festring::Find (char Char, sizetype Pos) const {
  char *Ptr = Data;
  if (Ptr) {
    char *Result = static_cast<char *>(memchr(Ptr+Pos, Char, Size-Pos));
    if (Result) return Result-Ptr;
  }
  return NPos;
}


//==========================================================================
//
//  festring::Find
//
//==========================================================================
festring::sizetype festring::Find (cchar *CStr, sizetype Pos, sizetype N) const {
  if (N && Pos < Size) {
    const char *Ptr = Data;
    if (Ptr) {
      char Char = CStr[0];
      while (Size - Pos >= N) {
        //if (Size - Pos < N) return NPos;
        const char *Result = (const char *)memchr(Ptr + Pos, Char, Size - Pos);
        if (!Result) return NPos;
        if ((uintptr_t)(Ptr + Size - Result) < N) return NPos;
        if (memcmp(Result, CStr, N) == 0) return (sizetype)(uintptr_t)(Result - Ptr);
        Pos = Result - Ptr + 1;
      }
    }
  }
  return NPos;
}


//==========================================================================
//
//  festring::FindLast
//
//==========================================================================
festring::sizetype festring::FindLast (char Char, sizetype Pos) const {
  char *Ptr = Data;
  if (Ptr) {
    if (Pos >= Size) Pos = Size-1;
    sizetype c;
    for (c = Pos; c != NPos && Ptr[c] != Char; --c);
    return c;
  }
  return NPos;
}


//==========================================================================
//
//  festring::FindLast
//
//==========================================================================
festring::sizetype festring::FindLast (const char *CStr, sizetype Pos, sizetype N) const {
  if (N) {
    char *Ptr = Data;
    if (Ptr && Size >= N) {
      char Char = CStr[0];
      if (Pos > Size-N) Pos = Size-N;
      for (sizetype c = Pos; c != NPos; --c) if (Ptr[c] == Char && !memcmp(Ptr+c, CStr, N)) return c;
      return NPos;
    }
  }
  return NPos;
}


//==========================================================================
//
//  festring::Erase
//
//==========================================================================
void festring::Erase (sizetype Pos, sizetype Length) {
  if (Pos >= Size) return;
  if (Length >= Size && Pos == 0) { Empty(); return; }
  char *OldPtr = Data;
  if (OldPtr && Length) {
    sizetype OldSize = Size;
    if (Pos < OldSize) {
      truth MoveReq = (Length < OldSize-Pos);
      if (OwnsData) {
        if (IsUnique()) {
          if (MoveReq) {
            sizetype End = Pos+Length;
            memmove(OldPtr+Pos, OldPtr+End, OldSize-End);
          }
          Size -= Length;
          return;
        } else {
          (void)DecRef();
        }
      }
      sizetype NewSize = (MoveReq ? OldSize-Length : Pos);
      Size = NewSize;
      Reserved = AlignReserved(NewSize);
      char *Ptr = allocData(Reserved);
      Data = Ptr;
      OwnsData = true;
      if (Pos > 0) memmove(Ptr, OldPtr, Pos);
      if (MoveReq) {
        sizetype End = Pos+Length;
        if (OldSize > End) memmove(Ptr+Pos, OldPtr+End, OldSize-End);
      }
    }
  }
}


//==========================================================================
//
//  festring::Insert
//
//==========================================================================
void festring::Insert (sizetype Pos, cchar *CStr, sizetype N) {
  if (N) {
    sizetype OldSize = Size;
    if (Pos < OldSize) {
      // this implies Data != 0
      char *OldPtr = Data;
      void *DeletePtr = nullptr;
      sizetype NewSize = OldSize+N;
      Size = NewSize;
      if (OwnsData) {
        if (IsUnique()) {
          if (NewSize <= Reserved) {
            char *Ptr = OldPtr+Pos;
            memmove(Ptr+N, Ptr, OldSize-Pos);
            memmove(Ptr, CStr, N);
            return;
          } else {
            DeletePtr = OldPtr;
          }
        } else {
          (void)DecRef();
        }
      }
      Reserved = AlignReserved(NewSize);
      char *NewPtr = allocData(Reserved);
      Data = NewPtr;
      if (Pos > 0) memmove(NewPtr, OldPtr, Pos);
      memmove(NewPtr+Pos, CStr, N);
      if (OldSize > Pos) memmove(NewPtr+Pos+N, OldPtr+Pos, OldSize-Pos);
      OwnsData = true;
      DeleteDataPtr(DeletePtr);
    } else if (Pos == OldSize) {
      AppendIntr(CStr, N);
    } else {
      ABORT("Illegal festring insertion detected!");
    }
  }
}


//==========================================================================
//
//  festring::AppendShort
//
//==========================================================================
festring &festring::AppendShort (int16_t ival) {
  char buf[32];
  int len = snprintf(buf, sizeof(buf), "%d", (int32_t)ival);
  return AppendBuf(buf, len);
}


//==========================================================================
//
//  festring::AppendUShort
//
//==========================================================================
festring &festring::AppendUShort (uint16_t ival) {
  char buf[32];
  int len = snprintf(buf, sizeof(buf), "%u", (uint32_t)ival);
  return AppendBuf(buf, len);
}


//==========================================================================
//
//  festring::AppendInt
//
//==========================================================================
festring &festring::AppendInt (int32_t ival) {
  char buf[32];
  int len = snprintf(buf, sizeof(buf), "%d", ival);
  return AppendBuf(buf, len);
}


//==========================================================================
//
//  festring::AppendUInt
//
//==========================================================================
festring &festring::AppendUInt (uint32_t ival) {
  char buf[32];
  int len = snprintf(buf, sizeof(buf), "%u", ival);
  return AppendBuf(buf, len);
}


//==========================================================================
//
//  festring::AppendInt64
//
//==========================================================================
festring &festring::AppendInt64 (int64_t ival) {
  char buf[128];
  int len = snprintf(buf, sizeof(buf), LONG_LONG_PFORMAT, ival);
  return AppendBuf(buf, len);
}


//==========================================================================
//
//  festring::AppendUInt64
//
//==========================================================================
festring &festring::AppendUInt64 (uint64_t ival) {
  char buf[128];
  int len = snprintf(buf, sizeof(buf), ULONG_LONG_PFORMAT, ival);
  return AppendBuf(buf, len);
}


//==========================================================================
//
//  isCtlChar
//
//==========================================================================
static FORCE_INLINE bool isCtlChar (char ch) {
  return (ch == 1 || ch == 2);
}


//==========================================================================
//
//  festring::HasCtlCodes
//
//==========================================================================
bool festring::HasCtlCodes () const {
  cchar *str = Data;
  for (sizetype c = Size; c != 0; --c, ++str) {
    if (isCtlChar(*str)) return true;
  }
  return false;
}


//==========================================================================
//
//  festring::TextLength
//
//  without color codes
//
//==========================================================================
int festring::TextLength () const {
  int len = 0;
  RawTextData raw(Data, Length());
  while (!raw.IsEmpty()) {
    if (raw.SkipChar() != 0) {
      len += 1;
    }
  }
  return len;
}


//==========================================================================
//
//  festring::StartsWith
//
//==========================================================================
truth festring::StartsWith (cchar *str, int slen) const {
  if (!str) return (slen <= 0);
  int realslen = (int)strlen(str);
  if (slen < 0) slen = (int)realslen; else if (slen > realslen) return false;
  if ((sizetype)slen > Size) return false;
  if (slen == 0) return true;
  return (memcmp(Data, str, slen) == 0);
}


//==========================================================================
//
//  festring::EndsWith
//
//==========================================================================
truth festring::EndsWith (cchar *str, int slen) const {
  if (!str) return (slen <= 0);
  int realslen = (int)strlen(str);
  if (slen < 0) slen = (int)realslen; else if (slen > realslen) return false;
  if (slen == 0) return true;
  if ((sizetype)slen > Size) return false;
  return (memcmp(Data+Size-slen, str, slen) == 0);
}


//==========================================================================
//
//  festring::StartsWithCI
//
//==========================================================================
truth festring::StartsWithCI (cchar *str, int slen) const {
  if (!str) return (slen <= 0);
  int realslen = (int)strlen(str);
  if (slen < 0) slen = (int)realslen; else if (slen > realslen) return false;
  if (slen == 0) return true;
  if ((sizetype)slen > Size) return false;
  cchar *ep = Data;
  for (; slen > 0; ++str, ++ep, --slen) {
    if (CharUpper(*str) != CharUpper(*ep)) return false;
  }
  return true;
}


//==========================================================================
//
//  festring::EndsWithCI
//
//==========================================================================
truth festring::EndsWithCI (cchar *str, int slen) const {
  if (!str) return (slen <= 0);
  int realslen = (int)strlen(str);
  if (slen < 0) slen = (int)realslen; else if (slen > realslen) return false;
  if (slen == 0) return true;
  if ((sizetype)slen > Size) return false;
  cchar *ep = Data+Size-slen;
  for (; slen > 0; ++str, ++ep, --slen) {
    if (CharUpper(*str) != CharUpper(*ep)) return false;
  }
  return true;
}


//==========================================================================
//
//  festring::FindCI
//
//  Returns the position of the first occurance of What in Where
//  starting at Begin or after it, ignoring the case of letters.
//  If the search fails, festring::NPos is returned instead.
//
//==========================================================================
festring::sizetype festring::FindCI (cfestring &Where, cfestring &What, sizetype Begin) {
  if (What.IsEmpty()) return Begin;
  for (; Where.GetSize() >= What.GetSize() + Begin; ++Begin) {
    if (CharUpper(Where[Begin]) == CharUpper(What[0])) {
      truth Equal = true;
      for (sizetype c = 1; c < What.GetSize(); ++c)
      if (CharUpper(Where[Begin + c]) != CharUpper(What[c])) {
        Equal = false;
        break;
      }
      if (Equal) return Begin;
    }
  }
  return NPos;
}


//==========================================================================
//
//  festring::SearchAndReplace
//
//  Replaces all occurances of What in Where after Begin with With
//
//==========================================================================
void festring::SearchAndReplace (festring &Where, cfestring &What, cfestring &With, sizetype Begin) {
  for (sizetype Pos = Where.Find(What, Begin); Pos != NPos; Pos = Where.Find(What, Pos)) {
    Where.Erase(Pos, What.GetSize());
    Where.Insert(Pos, With);
    Pos += With.GetSize();
  }
}


//==========================================================================
//
//  festring::TransformSpacesTo
//
//  FIXME: make this faster!
//
//==========================================================================
festring festring::TransformSpacesTo (char spacech) const {
  bool wasBlank = false;
  for (sizetype pos = 0; !wasBlank && pos != Size; pos += 1) {
    char ch = Data[pos];
    if ((ch >= 0 && ch <= 32) || ch == 127 || ch == '_') {
      wasBlank = true;
    }
  }
  if (wasBlank) {
    festring res;
    wasBlank = false;
    for (sizetype pos = 0; pos != Size; pos += 1) {
      char ch = Data[pos];
      if ((ch >= 0 && ch <= 32) || ch == 127 || ch == '_') {
        wasBlank = true;
      } else {
        if (wasBlank && res.GetSize() != 0) {
          res.AppendChar(spacech);
        }
        wasBlank = false;
        res.AppendChar(ch);
      }
    }
    return res;
  } else {
    return *this;
  }
}


//==========================================================================
//
//  festring::CompareCI
//
//  Returns whether First is behind Second in alphabetical order, ignoring case
//
//==========================================================================
bool festring::CompareCILess (cfestring &First, cfestring &Second) noexcept {
  const char *d0 = First.Data;
  const char *d1 = Second.Data;
  sizetype len = Min(First.GetSize(), Second.GetSize());
  while (len != 0) {
    const char Char1 = CharUpper(*d0++);
    const char Char2 = CharUpper(*d1++);
    if (Char1 != Char2) return ((uint8_t)Char1 < (uint8_t)Char2);
  }
  return First.GetSize() < Second.GetSize();
}


//==========================================================================
//
//  festring::SwapData
//
//==========================================================================
void festring::SwapData (festring &Str) {
  char *const TData = Data;
  csizetype TSize = Size;
  csizetype TReserved = Reserved;
  ctruth TOwnsData = OwnsData;
  Data = Str.Data;
  Size = Str.Size;
  Reserved = Str.Reserved;
  OwnsData = Str.OwnsData;
  Str.Data = TData;
  Str.Size = TSize;
  Str.Reserved = TReserved;
  Str.OwnsData = TOwnsData;
}


//==========================================================================
//
//  festring::LeftCopy
//
//==========================================================================
festring festring::LeftCopy (int count) const {
  if (count <= 0 || IsEmpty()) return festring();
  if ((sizetype)count >= Size) return festring(*this);
  // invariant: non-owned data is ALWAYS asciiz. sigh.
  festring res((sizetype)count);
  memcpy(res.Data, Data, count);
  return res;
}


//==========================================================================
//
//  festring::Left
//
//==========================================================================
void festring::Left (int count) {
  if (IsEmpty()) return;
  if (count <= 0) { Empty(); return; }
  if ((sizetype)count >= Size) return;
  SetLength((sizetype)count);
}


//==========================================================================
//
//  festring::ChopLeft
//
//==========================================================================
void festring::ChopLeft (int count) {
  if (IsEmpty() || count <= 0) return;
  if ((sizetype)count >= Size) { Empty(); return; }
  Erase(0, (sizetype)count);
}


//==========================================================================
//
//  festring::ChopRight
//
//==========================================================================
void festring::ChopRight (int count) {
  if (IsEmpty() || count <= 0) return;
  if ((sizetype)count >= Size) { Empty(); return; }
  Erase(Size - (sizetype)count, (sizetype)count);
}


//==========================================================================
//
//  festring::TrimLeft
//
//==========================================================================
void festring::TrimLeft () {
  if (Size == 0) return;
  sizetype pos = 0;
  while (pos != Size && RawTextData::IsBlank(Data[pos])) pos += 1;
  if (pos == Size) {
    Empty();
  } else if (pos != 0) {
    if (IsUnique()) {
      Size -= pos;
      memmove(Data, Data + pos, Size);
    } else {
      CreateOwnData(Data + pos, Size - pos);
    }
  }
}


//==========================================================================
//
//  festring::TrimRight
//
//==========================================================================
void festring::TrimRight () {
  if (Size == 0) return;
  sizetype pos = Size;
  while (pos != 0 && RawTextData::IsBlank(Data[pos - 1])) pos -= 1;
  if (pos == 0) {
    Empty();
  } else if (pos != Size) {
    if (IsUnique()) {
      Size = pos;
    } else {
      CreateOwnData(Data, pos);
    }
  }
}


//==========================================================================
//
//  festring::TrimAll
//
//==========================================================================
void festring::TrimAll () {
  TrimLeft();
  TrimRight();
}


//==========================================================================
//
//  festring::Compare
//
//  Returns -1 if this is before Str in alphabetical order,
//  zero if strings are identical, else 1
//
//==========================================================================
int festring::Compare (cfestring &Str) const {
  sizetype ThisSize = Size;
  sizetype StrSize = Str.Size;
  if (ThisSize && StrSize) {
    int Comp = memcmp(Data, Str.Data, (StrSize > ThisSize ? ThisSize : StrSize));
    if (Comp) return (Comp < 0 ? -1 : 1);
  }
  return (ThisSize < StrSize ? -1 : ThisSize != StrSize);
}


//==========================================================================
//
//  festring::Compare
//
//==========================================================================
int festring::Compare (cchar *str) const {
  sizetype ThisSize = Size;
  sizetype StrSize = (sizetype)(str ? strlen(str) : 0);
  if (ThisSize && StrSize) {
    int Comp = memcmp(Data, str, (StrSize > ThisSize ? ThisSize : StrSize));
    if (Comp) return (Comp < 0 ? -1 : 1);
  }
  return (ThisSize < StrSize ? -1 : ThisSize != StrSize);
}


//==========================================================================
//
//  festring::CompareCI
//
//==========================================================================
int festring::CompareCI (cfestring &Str) const {
  sizetype ThisSize = Size;
  sizetype StrSize = Str.Size;
  if (ThisSize && StrSize) {
    sizetype len = (ThisSize < StrSize ? ThisSize : StrSize);
    for (sizetype Pos = 0; Pos < len; ++Pos) {
      char Char1 = CharUpper(Data[Pos]);
      char Char2 = CharUpper(Str[Pos]);
      if (Char1 != Char2) return (Char1 < Char2 ? -1 : 1);
    }
    if (ThisSize == StrSize) return 0;
  }
  return (ThisSize < StrSize ? -1 : ThisSize != StrSize);
}


//==========================================================================
//
//  festring::CompareCI
//
//==========================================================================
int festring::CompareCI (cchar *str) const {
  sizetype ThisSize = Size;
  sizetype StrSize = (sizetype)(str ? strlen(str) : 0);
  if (ThisSize && StrSize) {
    sizetype len = (ThisSize < StrSize ? ThisSize : StrSize);
    for (sizetype Pos = 0; Pos < len; ++Pos) {
      char Char1 = CharUpper(Data[Pos]);
      char Char2 = CharUpper(str[Pos]);
      if (Char1 != Char2) return (Char1 < Char2 ? -1 : 1);
    }
    if (ThisSize == StrSize) return 0;
  }
  return (ThisSize < StrSize ? -1 : ThisSize != StrSize);
}


//==========================================================================
//
//  festring::EquData
//
//==========================================================================
bool festring::EquData (cchar *data, sizetype datasize) const {
  if (datasize != Size) return false;
  if (datasize == 0) return true;
  if (!data) return false; // just in case
  return (memcmp(data, Data, datasize) == 0);
}


//==========================================================================
//
//  festring::EquCIData
//
//==========================================================================
bool festring::EquCIData (cchar *data, sizetype datasize) const {
  if (datasize != Size) return false;
  if (datasize == 0) return true;
  if (!data) return false; // just in case
  cchar *d2 = Data;
  while (datasize != 0) {
    datasize -= 1;
    if (CharUpper(*data) != CharUpper(*d2)) return false;
    data += 1; d2 += 1;
  }
  return true;
}


//==========================================================================
//
//  festring::CreateOwned
//
//==========================================================================
festring festring::CreateOwned (const void *buf, sizetype N) {
  if (N == 0) return festring();
  if (!buf) return festring(N);
  festring res;
  res.CreateOwnData((cchar *)buf, N);
  return res;
}


/*
sLong festring::GetCheckSum () const {
  sLong Counter = 0;
  for (uShort c = 0; c < GetSize(); ++c) Counter = sLong(Data[c]);
  return Counter;
}
*/


//==========================================================================
//
//  festring::NormalizeSpaces
//
//  replace all spaces and newlines with a single proper space,
//  but leave color codes intact.
//
//==========================================================================
festring festring::NormalizeSpaces (bool removeColors) const {
  festring res;
  if (Length() == 0) return res;

  RawTextData raw(Data, Length());

  bool seen = false;
  bool prevSpace = true;
  char ch;

  while (!raw.IsEmpty() && !seen) {
    ch = raw.SkipChar();
    if (ch != 0) {
      if (ch > 2 && ch < 32) {
        seen = true;
      } else if (raw.IsAnyBlank(ch)) {
        if (prevSpace) seen = true; else prevSpace = true;
      } else {
        prevSpace = false;
      }
    }
  }

  if (!seen) {
    IvanAssert(raw.IsEmpty());
    res = *this;
  } else {
    raw = RawTextData(Data, Length());
    prevSpace = true;
    while (!raw.IsEmpty()) {
      ch = raw.SkipChar();
      if (ch != 0) {
        if (raw.IsAnyBlank(ch)) {
          if (!prevSpace) {
            res.AppendChar(' ');
            prevSpace = true;
          }
        } else {
          res.AppendChar(ch);
          prevSpace = false;
        }
      } else {
        if (!removeColors) res.AppendStr(raw.lastClr);
      }
    }
  }

  if (prevSpace) {
    IvanAssert(res.Size != 0);
    res.Left(res.Length() - 1);
  }

  return res;
}


//==========================================================================
//
//  festring::PutWeight
//
//==========================================================================
void festring::PutWeight (int w, cchar *wcolor, cchar *ecolor) {
  *this << wcolor;
  if (w < 1000) {
    *this << w;
    if ((wcolor && wcolor[0]) || (ecolor && ecolor[0])) {
      *this << "\x10"; // small space
    }
    *this << "g";
  } else {
    // i prolly was on high when i wrote the previous version of this code.
    // we have a fixed point number here, why all that mess?
    const int wround = w + 55; // round the number
    const int kg = wround / 1000; // integral part
    const int g = wround / 100 % 10; // first digit of the fractional part
    if (kg * 1000 + g * 100 != w) {
      // splitted number is not equal to the original, so add "inexact" sign
      *this << "~";
    }
    *this << kg;
    if (g != 0) {
      *this << "." << g;
    }
    if ((wcolor && wcolor[0]) || (ecolor && ecolor[0])) {
      *this << "\x11"; // small space
    }
    *this << "kg";
  }
  *this << ecolor;
}
