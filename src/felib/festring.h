/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_FESTRING_H__
#define __FELIB_FESTRING_H__

#include "felibdef.h"
#include "fejoaat.h"

#include <cctype>
#include <vector>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <time.h>

#define LONG_LONG_PFORMAT   "%lld"
#define ULONG_LONG_PFORMAT  "%llu"

// the game sometimes uses constant strings incorrectly.
// just always allocate new strings, because why not?
//#define FE_ALWAYS_ALLOCATE_STRING


// ////////////////////////////////////////////////////////////////////////// //
// RawTextData
// ////////////////////////////////////////////////////////////////////////// //

struct RawTextData {
public:
  const char *text;
  int length;
  packcol16 Color;
  packcol16 curColor;
  char lastClr[16]; // last color control code, 0-terminated
  char permaClr[16]; // last permanent color code, 0-terminated; empty if none was set

public:
  inline RawTextData () : text(""), length(0), Color(0), curColor(0) { ClearColorInfo(0); }

  inline RawTextData (const RawTextData &src)
    : text(src.text)
    , length(src.length)
    , Color(src.Color)
    , curColor(src.curColor)
  {
    strcpy(lastClr, src.lastClr);
    strcpy(permaClr, src.permaClr);
  }

  inline RawTextData (const void *buf, int len)
    : text(buf && len > 0 ? (const char *)buf : "")
    , length(len < 0 ? 0 : len)
    , Color(0)
    , curColor(0)
  {
    ClearColorInfo(0);
  }

  inline RawTextData (cfestring &festr);

  FORCE_INLINE void ClearColorInfo (packcol16 aclr) {
    lastClr[0] = permaClr[0] = 0;
    Color = curColor = aclr;
  }

  FORCE_INLINE bool IsEmpty () const { return (length == 0); }

  inline RawTextData &operator = (const RawTextData &src) {
    text = src.text;
    length = src.length;
    Color = src.Color;
    curColor = src.curColor;
    if ((void *)lastClr != (void *)src.lastClr) strcpy(lastClr, src.lastClr);
    if ((void *)permaClr != (void *)src.permaClr) strcpy(permaClr, src.permaClr);
    return *this;
  }

  FORCE_INLINE int operator - (const RawTextData &src) {
    return (int)(ptrdiff_t)(src.text - text);
  }

  FORCE_INLINE const char *SavePos () const { return text; }
  FORCE_INLINE int PosDiff (const char *pos) const { return (int)(ptrdiff_t)(text - pos); }

  // return 0 for color control code, or char.
  // full control code is stored in `lastClr`.
  char SkipChar ();

  // return 0 for color control code, or char
  FORCE_INLINE char PeekChar () const {
    if (length == 0) return 0;
    if (*text == 1 || *text == 2) return 0;
    return (*text ?: 32);
  }

  FORCE_INLINE bool IsCurrBlank () const {
    if (length == 0) return false;
    return IsBlank(*text ?: 32);
  }

  FORCE_INLINE bool IsCurrPadBlank () const {
    if (length == 0) return false;
    return IsPadBlank(*text ?: 32);
  }

  FORCE_INLINE bool IsCurrNonPadBlank () const {
    if (length == 0) return false;
    return (IsBlank(*text ?: 32) && !IsPadBlank(*text ?: 32));
  }

  // ctrl char is non-blank too
  FORCE_INLINE bool IsCurrNonBlank () const {
    if (length == 0) return false;
    return !IsBlank(*text ?: 32);
  }

  FORCE_INLINE bool IsCurrNonBlankOrPad () const {
    if (length == 0) return false;
    return (!IsBlank(*text ?: 32) || IsPadBlank(*text ?: 32));
  }

  FORCE_INLINE bool IsCurrCtl () const {
    if (length == 0) return false;
    return (*text == 1 || *text == 2);
  }

  static FORCE_INLINE bool IsBlank (char ch) { return (ch > 2 && ch <= 32); }
  static FORCE_INLINE bool IsPadBlank (char ch) { return (ch >= 15 && ch <= 25); }

  static FORCE_INLINE bool IsAnyBlank (char ch) { return (ch > 2 && ch <= 32); }
  static FORCE_INLINE bool IsRealBlank (char ch) { return (IsAnyBlank(ch) && !IsPadBlank(ch)); }

  static inline int HexDigit (char ch) {
    if (ch >= '0' && ch <= '9') return ch - '0';
    if (ch >= 'A' && ch <= 'F') return ch - 'A' + 10;
    if (ch >= 'a' && ch <= 'f') return ch - 'a' + 10;
    return -1;
  }
};


// ////////////////////////////////////////////////////////////////////////// //
// festring
// ////////////////////////////////////////////////////////////////////////// //

// non-breaking space
#define NBSP      ((char)24)
#define NBSP1PX   ((char)25)

class festring {
  friend class RawTextData;
public:
  typedef unsigned int sizetype;
  typedef const unsigned int csizetype;

public:
  static FORCE_INLINE char CharUpper (char ch) noexcept {
    return (ch >= 'a' && ch <= 'z' ? ch - 32 : ch);
  }

  static FORCE_INLINE char CharLower (char ch) noexcept {
    return (ch >= 'A' && ch <= 'Z' ? ch + 32 : ch);
  }

private:
  enum { FESTRING_PAGE = 0x7Fu };

  static FORCE_INLINE sizetype AlignReserved (csizetype strsize) {
    return (strsize | FESTRING_PAGE) + 1;
  }

  static inline char *allocData (sizetype sz) {
    char *res = new char[sz + sizeof(rcint) + 4];
    res += sizeof(rcint);
    REFS(res) = 0;
    return res;
  }

  FORCE_INLINE void IncRef () const {
    if (OwnsData) {
      if (!Data) {
        fprintf(stderr, "FESTRING: trying to incref nil string!\n");
        __builtin_trap();
      }
      ++REFS(Data);
    }
  }

  // return `true` if the string was unique
  FORCE_INLINE bool DecRef () const {
    if (OwnsData) {
      if (!Data) {
        fprintf(stderr, "FESTRING: trying to decref nil string!\n");
        __builtin_trap();
      }
      const bool res = (REFS(Data) == 0);
      if (!res) --REFS(Data);
      return res;
    } else {
      return false;
    }
  }

  // return `true` if the string was unique
  static FORCE_INLINE bool DecRefPtr (void *ptr) {
    if (ptr) {
      const bool res = (REFS(ptr) == 0);
      if (!res) --REFS(ptr);
      return res;
    } else {
      return false;
    }
  }

  FORCE_INLINE void DeleteData () {
    if (Data) { delete [] REFSA(Data); Data = 0; }
  }

  static FORCE_INLINE void DeleteDataPtr (void *ptr) {
    if (ptr) delete [] REFSA(ptr);
  }

public:
  FORCE_INLINE bool IsUnique () const { return (OwnsData && Data && (REFS(Data) == 0)); }

public:
  FORCE_INLINE festring () : Data(0), Size(0), Reserved(0), OwnsData(false) {}

  static festring CreateOwned (const void *buf, sizetype N);

  explicit inline festring (sizetype N) : Size(N), Reserved(AlignReserved(N)), OwnsData(true) {
    if (N != 0) {
      Data = allocData(Reserved);
      memset(Data, 0, N);
    } else {
      Data = nullptr;
      Reserved = 0;
      OwnsData = false;
    }
  }

  inline festring (sizetype N, char C) : Size(N), Reserved(AlignReserved(N)), OwnsData(true) {
    if (N != 0) {
      Data = allocData(Reserved);
      memset(Data, C, N);
    } else {
      Data = nullptr;
      Reserved = 0;
      OwnsData = false;
    }
  }

  // invariant: non-owned data is ALWAYS asciiz!
  #ifdef FE_ALWAYS_ALLOCATE_STRING
  inline festring (cchar *CStr, bool/*dummy arg*/, bool/*dummy arg*/)
    : Data(0)
    , Size(0)
    , Reserved(0)
    , OwnsData(false)
  {
    if (CStr && CStr[0]) CreateOwnData(CStr, (sizetype)strlen(CStr));
  }
  #else
  FORCE_INLINE festring (cchar *CStr, bool/*dummy arg*/, bool/*dummy arg*/)
    : Data((char *)CStr)
    , Size(CStr ? (sizetype)strlen(CStr) : 0)
    , Reserved(0)
    , OwnsData(false)
  {
    if (Size == 0) Data = 0;
  }
  #endif

  FORCE_INLINE explicit festring (cchar *CStr)
    : Data(0)
    , Size(0)
    , Reserved(0)
    , OwnsData(false)
  {
    if (CStr && CStr[0]) CreateOwnData(CStr, (sizetype)strlen(CStr));
  }

  FORCE_INLINE festring (cfestring &Str)
    : Data(Str.Data)
    , Size(Str.Size)
    , Reserved(Str.Reserved)
    , OwnsData(Str.OwnsData)
  {
    if (OwnsData) {
      IncRef();
    } else if (!Data) {
      // just in case
      Size = 0;
      Reserved = 0;
      OwnsData = false;
    }
  }

  FORCE_INLINE ~festring () {
    if (OwnsData) {
      if (DecRef()) DeleteData();
    }
    Data = nullptr;
    Size = 0;
    Reserved = 0;
    OwnsData = false;
  }

  void ResetToLength (sizetype N) {
    Empty(); // everything is zero after this
    if (N != 0) {
      Size = N;
      Reserved = AlignReserved(N);
      Data = allocData(Reserved);
      memset(Data, 0, N);
      OwnsData = true;
    }
  }

  void SetLength (sizetype N, char fillch=' ') {
    if (N == 0) {
      Empty();
    } else if (N < Size) {
      EnsureUnique();
      Size = N;
    } else if (N > Size) {
      char *oldData = Data;
      const sizetype oldSize = Size;
      const bool wasOwned = OwnsData;
      Size = N;
      Reserved = AlignReserved(N);
      Data = allocData(Reserved);
      OwnsData = true;
      if (oldSize > 0) memcpy(Data, oldData, oldSize);
      memset(Data + oldSize, fillch, N - oldSize);
      if (wasOwned && oldData && DecRefPtr(oldData)) DeleteDataPtr(oldData);
    }
  }

  festring LeftCopy (int count) const;
  void Left (int count);

  void ChopLeft (int count);
  void ChopRight (int count);

  festring &Capitalize ();
  festring &CapitalizeAll (); // all words except "the", "of", "a", etc.

  festring CapitalizeCopy () const { return festring(*this).Capitalize(); }
  festring CapitalizeAllCopy () const { return festring(*this).CapitalizeAll(); }

  // always copies, why not
  FORCE_INLINE festring &operator = (cchar *CStr) {
    if (!CStr || CStr[0] == 0) {
      Empty();
    } else {
      CreateOwnData(CStr, (sizetype)strlen(CStr));
    }
    return *this;
  }

  festring &operator = (cfestring &);

  int Compare (cfestring &) const;
  int CompareCI (cfestring &) const;
  int Compare (cchar *) const;
  int CompareCI (cchar *) const;

  FORCE_INLINE festring &operator << (char Char) { AppendChar(Char); return *this; }
  FORCE_INLINE festring &operator << (cchar *CStr) { AppendStr(CStr); return *this; }
  FORCE_INLINE festring &operator << (cfestring &Str) { AppendIntr(Str.Data, Str.Size); return *this; }

  FORCE_INLINE festring &operator += (char Char) { return *this << Char; }
  FORCE_INLINE festring &operator += (cchar *CStr) { return *this << CStr; }
  FORCE_INLINE festring &operator += (cfestring &Str) { return *this << Str; }

  festring &operator << (int16_t Int) { return AppendShort(Int); }
  festring &operator << (uint16_t Int) { return AppendUShort(Int); }
  festring &operator << (int32_t Int) { return AppendInt(Int); }
  festring &operator << (uint32_t Int) { return AppendUInt(Int); }
  festring &operator << (int64_t Int) { return AppendInt64(Int); }
  festring &operator << (uint64_t Int) { return AppendUInt64(Int); }
  //festring &operator << (time_t Int) { return AppendUInt64((uint64_t)Int); }

  FORCE_INLINE bool operator < (cfestring &Str) const { return (Compare(Str) < 0); }
  FORCE_INLINE bool operator <= (cfestring &Str) const { return (Compare(Str) <= 0); }
  FORCE_INLINE bool operator > (cfestring &Str) const { return (Compare(Str) > 0); }
  FORCE_INLINE bool operator >= (cfestring &Str) const { return (Compare(Str) >= 0); }
  FORCE_INLINE bool operator < (cchar *CStr) const { return (Compare(CStr) < 0); }
  FORCE_INLINE bool operator <= (cchar *CStr) const { return (Compare(CStr) <= 0); }
  FORCE_INLINE bool operator > (cchar *CStr) const { return (Compare(CStr) > 0); }
  FORCE_INLINE bool operator >= (cchar *CStr) const { return (Compare(CStr) >= 0); }

  FORCE_INLINE bool operator == (cfestring &Str) const { return EquData(Str.Data, Str.Size); }
  FORCE_INLINE bool operator != (cfestring &Str) const { return !EquData(Str.Data, Str.Size); }
  FORCE_INLINE bool operator == (cchar *CStr) const { return EquData(CStr, (CStr ? (sizetype)strlen(CStr) : 0)); }
  FORCE_INLINE bool operator != (cchar *CStr) const { return !EquData(CStr, (CStr ? (sizetype)strlen(CStr) : 0)); }

  FORCE_INLINE int EquCI (cchar *other) const { return EquCIData(other, (other ? (sizetype)strlen(other) : 0)); }
  FORCE_INLINE int EquCI (cfestring &other) const { return EquCIData(other.Data, other.Size); }

  cchar *CStr () const;
  FORCE_INLINE sizetype GetSize () const { return Size; }
  // our strings should never be longer than 2GB!
  FORCE_INLINE int Length () const { if (Size < 0x7fffffffU) return (int)Size; __builtin_trap(); }
  void Empty ();
  void Assign (sizetype, char);
  void Resize (sizetype, char=' ');

  sizetype Find (char Char, sizetype Pos=0) const;
  sizetype Find (cchar *CStr, sizetype Pos=0) const { return Find(CStr, Pos, strlen(CStr)); }
  sizetype Find (cchar *CStr, sizetype Pos, sizetype N) const;
  sizetype Find (cfestring &S, sizetype Pos=0) const { return Find(S.Data, Pos, S.Size); }
  sizetype FindLast (char Char, sizetype Pos=NPos) const;
  sizetype FindLast (const char *CStr, sizetype Pos=NPos) const { return FindLast(CStr, Pos, strlen(CStr)); }
  sizetype FindLast (const char *CStr, sizetype Pos, sizetype N) const;
  sizetype FindLast (const festring& S, sizetype Pos=NPos) const { return FindLast(S.Data, Pos, S.Size); }

  void Erase (sizetype Pos, sizetype Length);

  void Insert (sizetype Pos, cchar *CStr) { Insert(Pos, CStr, strlen(CStr)); }
  void Insert (sizetype Pos, cchar *CStr, sizetype N);
  void Insert (sizetype Pos, cfestring &S) { Insert(Pos, S.Data, S.Size); }
  void Insert (sizetype Pos, char ch) { Insert(Pos, &ch, 1); }

  inline festring &Append (cfestring &Str, sizetype N) { return AppendIntr(Str.Data, N); }

  void TrimLeft ();
  void TrimRight ();
  void TrimAll ();

  festring &AppendChar (char ch);
  festring &AppendBuf (const void *buf, int len);
  festring &AppendStr (cchar *s);

  truth StartsWith (cchar *str, int slen=-1) const;
  truth EndsWith (cchar *str, int slen=-1) const;
  truth StartsWithCI (cchar *str, int slen=-1) const;
  truth EndsWithCI (cchar *str, int slen=-1) const;

  FORCE_INLINE truth StartsWith (cfestring &str) const { return StartsWith(str.Data, (int)str.Size); }
  FORCE_INLINE truth EndsWith (cfestring &str) const { return EndsWith(str.Data, (int)str.Size); }
  FORCE_INLINE truth StartsWithCI (cfestring &str) const { return StartsWithCI(str.Data, (int)str.Size); }
  FORCE_INLINE truth EndsWithCI (cfestring &str) const { return EndsWithCI(str.Data, (int)str.Size); }

  static csizetype NPos;

  festring SpacesToUndersCopy () const { return TransformSpacesTo('_'); }
  festring UndersToSpacesCopy () const { return TransformSpacesTo(' '); }

  static sizetype FindCI (cfestring &, cfestring &, sizetype=0);
  static void SearchAndReplace (festring &Where, cfestring &What, cfestring &With,
                                sizetype Begin=0);
  static bool CompareCILess (cfestring &First, cfestring &Second) noexcept;
  FORCE_INLINE truth IsEmpty () const { return (Size == 0); }
  FORCE_INLINE char operator [] (sizetype Index) const {
    if (Index >= Size) {
      fprintf(stderr, "FESTRING: invalid index %u (len is %u)\n",
              (unsigned)Index, (unsigned)Size);
      __builtin_trap();
    }
    return Data[Index];
  }
  void SwapData (festring &);
  //sLong GetCheckSum () const;
  void EnsureUnique (); // won't alloc data for empty string; actually, will free any allocated data for empty string

  bool HasCtlCodes () const;
  int TextLength () const; // without color codes

  // nicely formatted weight
  void PutWeight (int w, cchar *wcolor, cchar *ecolor);

  // replace all spaces and newlines with a single proper space,
  // but leave color codes intact.
  festring NormalizeSpaces (bool removeColors=false) const;

  FORCE_INLINE char *getDataBuffer () { return Data; } // DON'T USE!
  FORCE_INLINE cchar *getConstDataBuffer () const { return Data; } // DON'T USE!

  static FORCE_INLINE cfestring &EmptyStr () { return mEmptyString; }

  FORCE_INLINE uint32_t Hash (uint32_t seed=0x29a) const noexcept { return joaatHashBuf(Data, (size_t)Size, seed); }
  FORCE_INLINE uint32_t HashCI (uint32_t seed=0x29a) const noexcept { return joaatHashBufCI(Data, (size_t)Size, seed); }

private:
  festring TransformSpacesTo (char ch) const;

  void CreateOwnData (cchar *, sizetype);

  festring &AppendIntr (cchar *CStr, sizetype clen);

  festring &AppendShort (int16_t ival);
  festring &AppendUShort (uint16_t ival);
  festring &AppendInt (int32_t ival);
  festring &AppendUInt (uint32_t ival);
  festring &AppendInt64 (int64_t ival);
  festring &AppendUInt64 (uint64_t ival);

  void SlowAppend (cchar *, sizetype);
  bool EquCIData (cchar *data, sizetype datasize) const;
  bool EquData (cchar *data, sizetype datasize) const;

private:
  static const char *EmptyString;
  static festring mEmptyString;
  char *Data;
  sizetype Size;
  sizetype Reserved;
  bool OwnsData;
};


// ////////////////////////////////////////////////////////////////////////// //
// hash for std::unordered_*
// ////////////////////////////////////////////////////////////////////////// //
MAKE_STD_HASHER(festring)


// ////////////////////////////////////////////////////////////////////////// //
// RawTextData
// ////////////////////////////////////////////////////////////////////////// //

inline RawTextData::RawTextData (cfestring &festr)
  : text(festr.Data)
  , length((int)festr.Size)
  , Color(0)
  , curColor(0)
{
  ClearColorInfo(0);
}


// ////////////////////////////////////////////////////////////////////////// //
// festringpile
// ////////////////////////////////////////////////////////////////////////// //

class festringpile {
public:
  explicit festringpile () {}
  FORCE_INLINE festringpile (cfestring &String) : String(String) {}
  template <class type> festringpile &operator + (type What) { String << What; return *this; }
  FORCE_INLINE festringpile &operator + (cfestring &What) { String << What; return *this; }
  FORCE_INLINE festringpile &operator + (const festringpile& What) { String << What.String; return *this; }
  FORCE_INLINE operator festring () const { return String; }
  FORCE_INLINE cchar *CStr () const { return String.CStr(); }
private:
  festring String;
};

template <class type> FORCE_INLINE festringpile operator + (cfestring& S, type What) { return festringpile(S)+What; }
FORCE_INLINE festringpile operator + (cfestring &S, cfestring &What) { return festringpile(S)+What; }
FORCE_INLINE festringpile operator + (cfestring &S, const festringpile &What) { return festringpile(S)+What; }


struct ccharhasher {
  FORCE_INLINE size_t operator () (cchar *s) const noexcept {
    const size_t len = (s ? strlen(s) : 0);
    return (size_t)joaatHashBuf(s, len, 0x29a);
  }
};

struct ccharequ {
  FORCE_INLINE bool operator () (cchar *s0, cchar *s1) const noexcept {
    const size_t len0 = (s0 ? strlen(s0) : 0);
    const size_t len1 = (s1 ? strlen(s1) : 0);
    if (len0 == len1) {
      return (len0 == 0 || memcmp(s0, s1, len0) == 0);
    } else {
      return false;
    }
  }
};

struct ccharcomparer {
  FORCE_INLINE bool operator () (cchar *const &S1, cchar *const &S2) const noexcept {
    return (strcmp((S1 ? S1 : ""), (S2 ? S2 : "")) < 0);
  }
};

struct ignorecaseorderer {
  FORCE_INLINE bool operator () (cfestring &S1, cfestring &S2) const noexcept {
    return festring::CompareCILess(S1, S2);
  }
};


//#define CONST_S(str) festring(str, sizeof(str)-1)
#define CONST_S(str) festring(str, true, true)


/*
 * This macro doesn't evaluate with if what
 * is not found so it's often faster
 */
#define SEARCH_N_REPLACE(where, what, with) \
  if (where.Find(what) != festring::NPos) festring::SearchAndReplace(where, what, with);


// ////////////////////////////////////////////////////////////////////////// //
#if 1
#include <string>
#include <cstdlib>
#include <cxxabi.h>

template<typename T> const char *getCPPTypeNameCStr () {
  static char *res = nullptr;
  if (!res) {
    int status;
    std::string name = typeid(T).name();
    char *demangled = abi::__cxa_demangle(name.c_str(), 0/*NULL*/, 0/*NULL*/, &status);
    if (status == 0) {
      name = demangled;
      std::free(demangled);
    }
    res = (char *)calloc(1, name.length()+1);
    if (name.length() > 0) memcpy(res, name.c_str(), name.length());
  }
  return res;
}


template<typename T> festring getCPPTypeName () {
  int status;
  std::string name = typeid(T).name();
  char *demangled = abi::__cxa_demangle(name.c_str(), 0/*NULL*/, 0/*NULL*/, &status);
  if (status == 0) {
    name = demangled;
    std::free(demangled);
  }
  festring res;
  res << name.c_str();
  return res;
}
#endif


#endif
