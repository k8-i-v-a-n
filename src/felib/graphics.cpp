/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include <math.h>
#include <SDL2/SDL.h>

#include "cconsole.h"
#include "graphics.h"
#include "bitmap.h"
#include "whandler.h"
#include "feerror.h"
#include "festring.h"
#include "rawbit.h"
#include "vwfileio.h"

//#define FEGL_SRGB_OPTION
//#define FEGL_POW2_TEXTURES

#define VV_GLIMPORTS
#define VGLAPIPTR(x,optional)  x##_t graphics::p_##x
#include "glimports.h"
#undef VGLAPIPTR
#undef VV_GLIMPORTS

#ifdef FEGL_SRGB_OPTION
static bool useSRGB = false;
#endif
int graphics::fboW;
int graphics::fboH;
int graphics::fboMult;
GLenum graphics::ClampToEdge;

bool graphics::mConActive = false;
bool graphics::mConEnabled = false;
int graphics::mConHeight = 20;
static int mConFirstLine = 0;

#define HISTORY_SIZE  (128)
static festring conHistory[HISTORY_SIZE + 8];
static int conHistoryCount = 0;
static int conHistoryIdx = 0;


static bool isFullscreen = false;
static truth dblRes = false;
static uint8_t *bufc32 = 0;
static int bufcw = -1, bufch = -1;

static float DIVISOR = 1.7;


#if !defined(SHITDOZE) && !defined(DISABLE_SOUND) && defined(ENABLE_ALSA)
# include <alsa/asoundlib.h>
// shut the fuck up, alsa!

static void alsa_message_fucker (const char *file, int line, const char *function_, int err, const char *fmt, ...) {}

void fuck_alsa_messages () {
  snd_lib_error_set_handler(&alsa_message_fucker);
}
#else
void fuck_alsa_messages () {}
#endif


void (*graphics::SwitchModeHandler) ();

SDL_Window *graphics::OSWindow = 0;
//SDL_Renderer *graphics::Renderer = 0;
SDL_GLContext glCtx;

bitmap *graphics::DoubleBuffer = 0;
v2 graphics::Res;
//rawbitmap *graphics::DefaultFont = 0;
//rawbitmap *graphics::ConsoleFont = 0;
rawfont *graphics::DefaultFont = 0;
rawfont *graphics::ConsoleFont = 0;

int graphics::curx = -666;
int graphics::cury = -666;
int graphics::curvisible = 0;
graphics::CurType graphics::curtype = graphics::CurCaret;

truth graphics::blurryScale = true;
truth graphics::blurryOversample = false;


#ifndef GL_GENERATE_MIPMAP
#define GL_GENERATE_MIPMAP                0x8191
#endif
#ifndef GL_TEXTURE_MAX_LEVEL
#define GL_TEXTURE_MAX_LEVEL              0x813D
#endif


//==========================================================================
//
//  allocConvBuf
//
//==========================================================================
static void allocConvBuf (int resx, int resy) {
  if (!bufc32 || bufcw != resx || bufch != resy) {
    bufc32 = (uint8_t*)realloc(bufc32, resx*resy*4);
    if (!bufc32) ABORT("Out of memory for conversion buffer");
    bufcw = resx;
    bufch = resy;
  }
}


//==========================================================================
//
//  fixVal
//
//==========================================================================
static inline int fixVal (int v) {
  return (dblRes ? (int)(v * DIVISOR) : v);
}


// ////////////////////////////////////////////////////////////////////////// //
static GLuint maintid = 0;
static GLuint bigFBO = 0;
static GLuint bigFBOColorTid = 0;
#ifdef FEGL_POW2_TEXTURES
static GLfloat gltex, gltey;
#else
#define gltex  (1.0f)
#define gltey  (1.0f)
#endif


template <typename T> T nextPow2 (T in) {
  return ((in&(T)(in-1)) ? (1U<<(sizeof(T)*8-__builtin_clz(in))) : in);
}


//==========================================================================
//
//  VGetGLErrorStr
//
//==========================================================================
static inline const char *VGetGLErrorStr (const GLenum glerr) {
  switch (glerr) {
    case GL_NO_ERROR: return "no error";
    case GL_INVALID_ENUM: return "invalid enum";
    case GL_INVALID_VALUE: return "invalid value";
    case GL_INVALID_OPERATION: return "invalid operation";
    case GL_STACK_OVERFLOW: return "stack overflow";
    case GL_STACK_UNDERFLOW: return "stack underflow";
    case GL_OUT_OF_MEMORY: return "out of memory";
    default: break;
  }
  static char errstr[32];
  snprintf(errstr, sizeof(errstr), "0x%04x", (unsigned)glerr);
  return errstr;
}

#define GLDRW_RESET_ERROR()  (void)glGetError()

#define GLDRW_CHECK_ERROR(actionmsg_)  do { \
  GLenum glerr_ = glGetError(); \
  if (glerr_ != 0) ABORT("OpenGL: cannot %s, error: %s", actionmsg_, VGetGLErrorStr(glerr_)); \
} while (0)


//==========================================================================
//
//  graphics:killFBO
//
//==========================================================================
void graphics::killFBO () {
  #if 0
  ConLogf("fbo:%u; fbotex:%u", bigFBO, bigFBOColorTid);
  #endif
  if (bigFBO != 0) {
    p_glBindFramebuffer(GL_FRAMEBUFFER, bigFBO);
    p_glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
    p_glBindFramebuffer(GL_FRAMEBUFFER, 0);
    p_glDeleteFramebuffers(1, &bigFBO);
    bigFBO = 0;
    if (bigFBOColorTid != 0) {
      glDeleteTextures(1, &bigFBOColorTid);
      bigFBOColorTid = 0;
    }
  }
}


//==========================================================================
//
//  killTexture
//
//==========================================================================
void graphics::killTexture () {
  if (maintid != 0) {
    glDeleteTextures(1, &maintid);
    maintid = 0;
  }
  killFBO();
}


//==========================================================================
//
//  graphics::createFBO
//
//==========================================================================
void graphics::createFBO (int awidth, int aheight) {
  GLDRW_RESET_ERROR();
  #ifdef FEGL_SRGB_OPTION
  if (graphics::useSRGB) glEnable(GL_FRAMEBUFFER_SRGB); else glDisable(GL_FRAMEBUFFER_SRGB);
  #endif

  GLDRW_RESET_ERROR();
  graphics::p_glGenFramebuffers(1, &bigFBO);
  if (bigFBO == 0) {
    ABORT("OpenGL: cannot create FBO: error is %s", VGetGLErrorStr(glGetError()));
  }
  graphics::p_glBindFramebuffer(GL_FRAMEBUFFER, bigFBO);
  //graphics::p_glObjectLabelVA(GL_FRAMEBUFFER, bigFBO, "FBO(%u)", bigFBO);
  GLDRW_CHECK_ERROR("FBO: glBindFramebuffer");

  // attach 2D texture to this FBO
  glGenTextures(1, &bigFBOColorTid);
  if (bigFBOColorTid == 0) {
    ABORT("OpenGL: cannot create RGBA texture for FBO: error is %s", VGetGLErrorStr(glGetError()));
  }
  glBindTexture(GL_TEXTURE_2D, bigFBOColorTid);
  GLDRW_CHECK_ERROR("FBO: glBindTexture");
  //graphics::p_glObjectLabelVA(GL_TEXTURE, bigFBOColorTid, "FBO(%u) color texture", bigFBO);

  glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, graphics::ClampToEdge);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, graphics::ClampToEdge);

  if (blurryScale) {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  } else {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }
  //if (graphics::anisotropyExists) glTexParameterf(GL_TEXTURE_2D, GLenum(GL_TEXTURE_MAX_ANISOTROPY_EXT), 1.0f); // 1 is minimum, i.e. "off"

  // empty texture
  GLDRW_RESET_ERROR();
  // float
  //glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, awidth, aheight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
  #ifdef FEGL_SRGB_OPTION
  const GLint fmt = (graphics::useSRGB ? GL_SRGB8 : GL_RGB8);
  #else
  const GLint fmt = GL_RGB8;
  #endif
  glTexImage2D(GL_TEXTURE_2D, 0, fmt, awidth, aheight, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
  GLDRW_CHECK_ERROR("FBO: glTexImage2D");
  graphics::p_glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bigFBOColorTid, 0);
  GLDRW_CHECK_ERROR("FBO: glFramebufferTexture2D");

  GLenum status = graphics::p_glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if (status != GL_FRAMEBUFFER_COMPLETE) {
    ABORT("OpenGL: framebuffer creation failed (status=0x%04x)", (unsigned)status);
  }

  glDrawBuffer(GL_COLOR_ATTACHMENT0);
  glReadBuffer(GL_COLOR_ATTACHMENT0);

  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // black background
  glClearStencil(0);
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

  glBindTexture(GL_TEXTURE_2D, 0);
  graphics::p_glBindFramebuffer(GL_FRAMEBUFFER, 0);

  GLDRW_RESET_ERROR();
}


//==========================================================================
//
//  graphics::createTexture
//
//==========================================================================
void graphics::createTexture () {
  if (Res.X < 16 || Res.Y < 16) ABORT("WTF?!");

  killTexture();

  glGenTextures(1, &maintid);
  if (maintid == 0) ABORT("Couldn't create OpenGL texture.");

  glBindTexture(GL_TEXTURE_2D, maintid);
  glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_FALSE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

  //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, graphics::ClampToEdge);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, graphics::ClampToEdge);

  if (DIVISOR == 2.0f) {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  } else if (blurryOversample) {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  } else {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }
  //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  #ifdef FEGL_POW2_TEXTURES
  const int gszx = nextPow2(Res.X);
  const int gszy = nextPow2(Res.Y);
  #else
  const int gszx = Res.X;
  const int gszy = Res.Y;
  #endif

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, gszx, gszy, 0, /*GL_RGBA*/GL_RGB, GL_UNSIGNED_BYTE, NULL);
  #ifdef FEGL_POW2_TEXTURES
  gltex = ((decltype(gltex))Res.X) / ((decltype(gltex))gszx);
  gltey = ((decltype(gltey))Res.Y) / ((decltype(gltey))gszy);
  #endif
  glBindTexture(GL_TEXTURE_2D, 0);

  //ConLogf("RES=%dx%d; tx=%dx%d; gle=%f;%f", Res.X, Res.Y, gszx, gszy, (double)gltex, (double)gltey);

  fboW = Res.X;
  fboH = Res.Y;
  fboMult = 1;
  while (fboW * (fboMult + 1) <= 4096 && fboH * (fboMult + 1) <= 4096) {
    fboMult += 1;
  }
  if (DIVISOR == 2.0f) {
    while (fboMult > 1 && fboMult % 2 != 0) fboMult -= 1;
  } else {
    while (fboMult > 1 && fboMult % 2 == 0) fboMult -= 1;
  }
  fboW *= fboMult;
  fboH *= fboMult;
  #if 0
  ConLogf("texture: %dx%d; FBO: %dx%d (mult: %d)", gszx, gszy, fboW, fboH, fboMult);
  #endif
  createFBO(fboW, fboH);
}


//==========================================================================
//
//  graphics::GetConCmdLine
//
//==========================================================================
festring graphics::GetConCmdLine () {
  return conHistory[0];
}


//==========================================================================
//
//  graphics::SetConCmdLine
//
//==========================================================================
void graphics::SetConCmdLine (cchar *str) {
  conHistoryIdx = 0;
  conHistory[0].Empty();
  if (str) {
    while (*str && conHistory[0].GetSize() < 1024) {
      char ch = *str; str += 1;
      if (ch >= 0 && ch < 32) ch = 32;
      conHistory[0].AppendChar(ch);
    }
  }
}


//==========================================================================
//
//  graphics::PrintSuggestion
//
//==========================================================================
void graphics::PrintSuggestion (const char *sugstr, void *udata) {
  if (!sugstr) {
    // header
    ConLogf("-------------------------");
  } else {
    ConLogf("  %s", sugstr);
  }
}


//==========================================================================
//
//  graphics::PutCmdChar
//
//==========================================================================
void graphics::PutCmdChar (char ch, void *udata) {
  if (ch != 8 && (ch >= 0 && ch < 32)) ch = ' ';
  ConCmdCharKey(ch);
}


//==========================================================================
//
//  AppendHistory
//
//==========================================================================
static void AppendHistory () {
  if (!conHistory[0].IsEmpty()) {
    int hpos = 1;
    while (hpos <= conHistoryCount && conHistory[hpos] != conHistory[0]) hpos += 1;

    // remove duplcate command
    if (hpos <= conHistoryCount) {
      for (int f = hpos + 1; f <= conHistoryCount; f += 1) {
        conHistory[f - 1] = conHistory[f];
      }
      conHistoryCount -= 1;
    }

    // make room for the new command
    if (conHistoryCount < HISTORY_SIZE) conHistoryCount += 1;
    for (int f = conHistoryCount - 1; f >= 0; f -= 1) {
      conHistory[f + 1] = conHistory[f];
    }
    conHistory[0].Empty();
  }
  conHistoryIdx = 0;
}


//==========================================================================
//
//  graphics::CalcConHeight
//
//  in chars, including the command line
//
//==========================================================================
int graphics::CalcConHeight () {
  int ch = mConHeight;
  if (ch * (CON_FONT->FontHeight() + 2) > Res.Y) ch = Res.Y / (CON_FONT->FontHeight() + 2);
  return Max(2, ch);
}


//==========================================================================
//
//  graphics::CalcConWidth
//
//  in chars
//
//==========================================================================
int graphics::CalcConWidth () {
  return Max(2, Res.X / CON_FONT->FontWidth());
}


//==========================================================================
//
//  graphics::ConCmdCharKey
//
//==========================================================================
void graphics::ConCmdCharKey (char ch) {
  if (ch == KEY_DEL_LINE) {
    // kill whole line
    conHistory[0].Empty();
  } else if (ch == '\n') {
    festring cmd = conHistory[0];
    cmd.TrimAll();
    if (!cmd.IsEmpty()) AppendHistory();
    mConFirstLine = 0;
    if (!cmd.IsEmpty()) {
      ConLogf(">%s", cmd.CStr());
      ConExecute(cmd.CStr());
    }
  } else if (ch == 9) {
    // tab
    festring cmd = conHistory[0];
    ConAutocomplete(cmd.CStr(), &PrintSuggestion, &PutCmdChar, 0);
  } else if (ch == 8) {
    // backspace
    if (!conHistory[0].IsEmpty()) {
      conHistory[0].Left((int)conHistory[0].GetSize() - 1);
    }
  } else if (ch == 7) {
    // ctrl+backspace
    if (!conHistory[0].IsEmpty()) {
      festring::sizetype len = conHistory[0].GetSize();
      while (len != 0 && RawTextData::IsAnyBlank(conHistory[0][len - 1])) len -= 1;
      while (len != 0 && !RawTextData::IsAnyBlank(conHistory[0][len - 1])) len -= 1;
      conHistory[0].Left(len);
    }
  } else if (ch == '\x01') {
    // up
    if (conHistoryIdx < conHistoryCount) {
      conHistoryIdx += 1;
      conHistory[0] = conHistory[conHistoryIdx];
    }
  } else if (ch == '\x02') {
    // down
    if (conHistoryIdx > 0) {
      conHistoryIdx -= 1;
      conHistory[0] = conHistory[conHistoryIdx];
    }
  } else if (ch == '\x13') {
    // page up
    const int ch = CalcConHeight();
    const int mxl = ConMaxLines();
    if (mConFirstLine + (ch - 1) < mxl + ch / 2) mConFirstLine += ch - 1;
  } else if (ch == '\x14') {
    // page down
    const int ch = CalcConHeight();
    mConFirstLine = Max(0, mConFirstLine - (ch - 1));
  } else if (ch == '`' && conHistory[0].IsEmpty()) {
    SetConsoleActive(false);
  } else if (ch >= 32 && conHistory[0].GetSize() < 1024) {
    conHistory[0].AppendChar(ch);
  }
}


//==========================================================================
//
//  mkclr
//
//==========================================================================
static inline uint32_t mkclr (uint32_t rgb, int darken) {
  int r = (rgb >> 16) & 0xff;
  int g = (rgb >> 8) & 0xff;
  int b = rgb & 0xff;
  r = Clamp(r - darken, 0, 255);
  g = Clamp(g - darken, 0, 255);
  b = Clamp(b - darken, 0, 255);
  return (r | (g << 8) | (b << 16));
}


//==========================================================================
//
//  graphics::RenderChar
//
//==========================================================================
void graphics::RenderChar (int x, int y, char ch, uint32_t rgbFG, uint32_t rgbBG) {
  if (x >= 0 && y >= 0 && x + CON_FONT->FontWidth() <= Res.X && y + CON_FONT->FontHeight() <= Res.Y) {
    const uint32_t *fbmp = CON_FONT->GetCharBitmap(ch);
    uint32_t *dptr = (uint32_t *)bufc32 + y * Res.X + x;
    for (int dy = 0; dy != CON_FONT->FontHeight(); dy += 1) {
      const uint32_t fg = mkclr(rgbFG, (7 - dy) * 14);
      const uint32_t bg = mkclr(rgbBG, 0);
      uint8_t b = *fbmp; fbmp += 1;
      uint32_t *dest = dptr; dptr += Res.X;
      for (int dx = 0; dx != CON_FONT->FontWidth(); dx += 1) {
        *dest = (b & 0x01 ? fg : bg);
        dest += 1; b >>= 1;
      }
    }
  }
}


//==========================================================================
//
//  graphics::RenderStr
//
//==========================================================================
void graphics::RenderStr (int x, int y, const char *str, uint32_t rgbFG, uint32_t rgbBG) {
  if (str && str[0]) {
    while (*str) {
      RenderChar(x, y, *str, rgbFG, rgbBG);
      x += CON_FONT->FontWidth(); str += 1;
    }
  }
}


//==========================================================================
//
//  graphics::RenderCmdLine
//
//==========================================================================
void graphics::RenderCmdLine () {
  if (!mConActive || mConHeight < 2) return;
  const int ch = CalcConHeight();
  if (ch * (CON_FONT->FontHeight() + 2) > Res.Y) return;
  const int cw = CalcConWidth() - 2;
  if (cw <= 0) return; //wtf?!
  int clen = (int)conHistory[0].GetSize();
  int y = (ch - 1) * (CON_FONT->FontHeight() + 2);
  RenderChar(0, y, '>', 0xff9f00, 0x000000);
  const char *cmdstr = conHistory[0].CStr();
  if (clen > cw) { cmdstr += clen - cw; clen = (int)strlen(cmdstr); }
  RenderStr(CON_FONT->FontWidth(), y, cmdstr, 0xffffff, 0x000000);
  const bool curhi = (SDL_GetTicks()%1200 < 600);
  if (curhi) {
    RenderChar(CON_FONT->FontWidth() + clen * CON_FONT->FontWidth(), y, ' ', 0x000000, 0x00ff00);
  } else {
    RenderChar(CON_FONT->FontWidth() + clen * CON_FONT->FontWidth(), y, ' ', 0x000000, 0x009f00);
  }
}


//==========================================================================
//
//  graphics::RenderConsole
//
//==========================================================================
void graphics::RenderConsole () {
  if (!mConActive || mConHeight < 2) return;
  const int ch = CalcConHeight();
  if (ch * (CON_FONT->FontHeight() + 2) > Res.Y) return;
  memset(bufc32, 0, Res.X * (ch * (CON_FONT->FontHeight() + 2)) * 4);
  int y = (ch - 2) * (CON_FONT->FontHeight() + 2);
  for (int f = 0; f != ch - 1; f += 1) {
    const char *line; int linelen;
    if (!ConGetLine((uint32_t)(f + mConFirstLine), line, linelen)) break;
    //uint32_t clr = (f & 1 ? 0xe0e0e0 : 0xb0b0b0);
    uint32_t clr = (f & 1 ? 0xe0e0d0 : 0xe0e050);
    RenderStr(0, y, line, clr, 0x000000);
    y -= (CON_FONT->FontHeight() + 2);
  }
  RenderCmdLine();
}


//==========================================================================
//
//  graphics::createTextureImage
//
//==========================================================================
void graphics::createTextureImage () {
  if (Res.X < 16 || Res.Y < 16) ABORT("WTF?!");
  allocConvBuf(Res.X, Res.Y);

  const packcol16 *SrcPtr = DoubleBuffer->GetImage()[0];

  // convert source buffer
  const packcol16 *sptr = SrcPtr;
  uint32_t *dptr = (uint32_t *)bufc32;
  for (int y = 0; y < Res.Y; ++y) {
    for (int x = 0; x < Res.X; ++x) {
      packcol16 c = *sptr++;
      uint8_t r, g, b;
      RGB16ToRGB32(c, &r, &g, &b);
      *dptr++ = r|(g<<8)|(b<<16);
    }
  }
}


//==========================================================================
//
//  graphics::PutPixel32
//
//==========================================================================
void graphics::PutPixel32 (int x, int y, uint32_t color) {
  if (x >= 0 && y >= 0 && x < Res.X && y < Res.Y) {
    uint32_t *dptr = (uint32_t *)bufc32;
    dptr += y * Res.X + x;
    *dptr = color;
  }
}


//==========================================================================
//
//  graphics::drawCursorImage
//
//==========================================================================
void graphics::drawCursorImage () {
  if (curvisible > 0) {
    const bool curhi = (SDL_GetTicks()%1200 < 600);
    //const uint32_t color = (curhi ? 0xffffff : 0x7f7f7f);
    const uint32_t color = (curhi ? 0x00ff00 : 0x007f00);
    switch (curtype) {
      case CurCaret:
        for (int y = -1; y != 9; y += 1) {
          PutPixel32(curx + 0, cury + y, color);
          PutPixel32(curx + 1, cury + y, color);
        }
        for (int x = -1; x != 3; x += 1) {
          PutPixel32(curx + x, cury - 2, color);
          PutPixel32(curx + x, cury + 9, color);
        }
        break;
      case CurRect:
        for (int y = -1; y != 8; y += 1) {
          for (int x = 0; x != 6; x += 1) {
            PutPixel32(curx + x, cury + y, color);
          }
        }
        break;
      case CurUnder:
        for (int x = 0; x != 6; x += 1) {
          PutPixel32(curx + x, cury + 7, color);
        }
        break;
    }
  }
}


//==========================================================================
//
//  graphics::BlitDBToScreen
//
//==========================================================================
void graphics::BlitDBToScreen () {
  if (maintid == 0) ABORT("OpenGL not initialized.");

  createTextureImage();
  drawCursorImage();

  if (mConActive) RenderConsole();

  int newx = fixVal(Res.X);
  int newy = fixVal(Res.Y);

  int winw = fixVal(Res.X);
  int winh = fixVal(Res.Y);
  SDL_GL_GetDrawableSize(OSWindow, &winw, &winh);

  glViewport(0, 0, winw, winh);

  // letterboxing
  int xofs = (winw - newx) / 2;
  int yofs = (winh - newy) / 2;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glEnable(GL_TEXTURE_2D);
  glDisable(GL_LIGHTING);
  glDisable(GL_DITHER);
  glDisable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_STENCIL_TEST);
  glDisable(GL_SCISSOR_TEST);
  glDisable(GL_CULL_FACE);
  glDisable(GL_POLYGON_OFFSET_FILL);
  glDisable(GL_ALPHA_TEST);
  glDisable(GL_FOG);
  glDisable(GL_COLOR_LOGIC_OP);
  glDisable(GL_INDEX_LOGIC_OP);
  glDisable(GL_POLYGON_SMOOTH);
  glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  glBindTexture(GL_TEXTURE_2D, maintid);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0/*x*/, 0/*y*/, Res.X, Res.Y, GL_RGBA/*GL_BGRA*/, GL_UNSIGNED_BYTE, bufc32);

  #if 0
  glMatrixMode(GL_PROJECTION); // for ortho camera
  glLoadIdentity();
  //glOrtho(0, newx, newy, 0, -1, 1); // top-to-bottom
  glOrtho(0, winw, winh, 0, -1, 1); // top-to-bottom

  //glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT*/);

  glBegin(GL_QUADS);
    glTexCoord2f( 0.0f,  0.0f); glVertex2i(xofs, yofs); // top-left
    glTexCoord2f(gltex,  0.0f); glVertex2i(xofs + newx, yofs); // top-right
    glTexCoord2f(gltex, gltey); glVertex2i(xofs + newx, yofs + newy); // bottom-right
    glTexCoord2f( 0.0f, gltey); glVertex2i(xofs, yofs + newy); // bottom-left
  glEnd();

  #else
  // oversampling

  // first, blit the texture to the FBO, with blurring
  IvanAssert(bigFBO != 0);
  p_glBindFramebuffer(GL_FRAMEBUFFER, bigFBO);
  #if 0
  ConLogf("(%fx%f) : (%dx%d)", gltex, gltey, fboW, fboH);
  #endif
  glMatrixMode(GL_MODELVIEW);
  glMatrixMode(GL_PROJECTION); // for ortho camera
  glLoadIdentity();
  glOrtho(0, fboW, fboH, 0, -1, 1); // top-to-bottom
  glViewport(0, 0, fboW, fboH);
  glBegin(GL_QUADS);
    glTexCoord2f( 0.0f,  0.0f); glVertex2i(   0, fboH); // top-left
    glTexCoord2f(gltex,  0.0f); glVertex2i(fboW, fboH); // top-right
    glTexCoord2f(gltex, gltey); glVertex2i(fboW, 0); // bottom-right
    glTexCoord2f( 0.0f, gltey); glVertex2i(   0, 0); // bottom-left
  glEnd();
  p_glBindFramebuffer(GL_FRAMEBUFFER, 0);

  // now blit the FBO
  glBindTexture(GL_TEXTURE_2D, bigFBOColorTid);
  #if 0
  xofs *= fboMult;
  yofs *= fboMult;
  newx *= fboMult;
  newy *= fboMult;
  #endif
  #if 0
  ConLogf("(%d,%d); (%dx%d); fbo:(%dx%d)", xofs, yofs, newx, newy, fboW, fboH);
  #endif
  glMatrixMode(GL_PROJECTION); // for ortho camera
  glLoadIdentity();
  //glOrtho(0, newx, newy, 0, -1, 1); // top-to-bottom
  glOrtho(0, winw, winh, 0, -1, 1); // top-to-bottom
  //glOrtho(0, winw * 4 / 3, winh * 4 / 3, 0, -1, 1); // top-to-bottom
  glViewport(0, 0, winw, winh);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT /*| GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT*/);
  glBegin(GL_QUADS);
    glTexCoord2f(0.0f, 0.0f); glVertex2i(xofs, yofs); // top-left
    glTexCoord2f(1.0f, 0.0f); glVertex2i(xofs + newx, yofs); // top-right
    glTexCoord2f(1.0f, 1.0f); glVertex2i(xofs + newx, yofs + newy); // bottom-right
    glTexCoord2f(0.0f, 1.0f); glVertex2i(xofs, yofs + newy); // bottom-left
  glEnd();
  #endif

  glBindTexture(GL_TEXTURE_2D, 0);
  glMatrixMode(GL_MODELVIEW);

  SDL_GL_SwapWindow(OSWindow);
}


//==========================================================================
//
//  graphics::Init
//
//==========================================================================
void graphics::Init () {
  static truth AlreadyInstalled = false;
  if (!AlreadyInstalled) {
    AlreadyInstalled = true;
    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER
                 #if !defined(DISABLE_SOUND)
                 |SDL_INIT_AUDIO
                 #endif
        ))
    {
      ABORT("Can't initialize SDL.");
    }
    atexit(graphics::DeInit);
  }
}


//==========================================================================
//
//  graphics::DeInit
//
//==========================================================================
void graphics::DeInit () {
  killTexture();
  delete DefaultFont;
  delete ConsoleFont;
  DefaultFont = 0;
  ConsoleFont = 0;
  SDL_Quit();
}


//==========================================================================
//
//  graphics::SetMode
//
//  this is called only once!
//
//==========================================================================
void graphics::SetMode (cchar *Title, cchar *IconName, v2 NewRes,
                        truth FullScreen, sLong adresmod,
                        truth BlurryScale, truth BlurryOversample)
{
  IvanAssert(!OSWindow);
  ConInit(NewRes.X);
  ConLogf("initalising video; resolution: %dx%d", NewRes.X, NewRes.Y);

  blurryScale = BlurryScale;
  blurryOversample = BlurryOversample;
  /*
  if (IconName) {
    SDL_Surface *Icon = SDL_LoadBMP(IconName);
    SDL_SetColorKey(Icon, SDL_SRCCOLORKEY, SDL_MapRGB(Icon->format, 255, 255, 255));
    SDL_WM_SetIcon(Icon, NULL);
  }
  */

  if (adresmod < 0) adresmod = 0; else if (adresmod > 10) adresmod = 10;
  dblRes = (adresmod > 0);
  if (adresmod == 10) DIVISOR = 2.0f; else DIVISOR = 1.0f+(adresmod/10.0f);

  Uint32 Flags = SDL_WINDOW_OPENGL;

  if (FullScreen) {
    Flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
    isFullscreen = true;
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
  SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 0); // we don't care
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 0);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 0);
  //SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  OSWindow = SDL_CreateWindow(Title,
                              SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              fixVal(NewRes.X), fixVal(NewRes.Y),
                              Flags);

  if (!OSWindow) {
    ABORT("Couldn't create main game window.");
  }

  if (IconName && IconName[0]) {
    VFile fl = vwopen(IconName);
    if (fl) {
      SDL_Surface *Icon = 0;
      SDL_RWops *rw = vwAllocSDLRW(fl);
      if (rw) {
        Icon = SDL_LoadBMP_RW(rw, 0);
        vwFreeSDLRW(rw);
      }
      vwclose(fl);
      if (Icon) {
        SDL_SetColorKey(Icon, SDL_TRUE, SDL_MapRGB(Icon->format, 255, 255, 255));
        SDL_SetWindowIcon(OSWindow, Icon);
        SDL_FreeSurface(Icon);
      }
    }
  }

  glCtx = SDL_GL_CreateContext(OSWindow);
  SDL_GL_MakeCurrent(OSWindow, glCtx);

  SDL_GL_SetSwapInterval(0);

#define VV_GLIMPORTS
#define VGLAPIPTR(x,required)  do { \
  p_##x = x##_t(GetExtFuncPtr(#x)); \
  if (!p_##x /*|| strstr(#x, "Framebuffer")*/) { \
    p_##x = nullptr; \
    festring extfn(#x); \
    extfn << "EXT"; \
    /*extfn += "ARB";*/ \
    /*GCon->Logf(NAME_Init, "OpenGL: trying `%s` instead of `%s`", *extfn, #x);*/ \
    p_##x = x##_t(GetExtFuncPtr(extfn.CStr())); \
    /*if (p_##x) GCon->Logf(NAME_Init, "OpenGL: ...found `%s`.", *extfn);*/ \
  } \
  if (required && !p_##x) ABORT("OpenGL: `%s()` not found!", ""#x); \
} while (0)
#include "glimports.h"
#undef VGLAPIPTR
#undef VV_GLIMPORTS

  // clamp to edge extension
  if (CheckExtension("GL_SGIS_texture_edge_clamp") || CheckExtension("GL_EXT_texture_edge_clamp")) {
    /*GCon->Log(NAME_Init, "Clamp to edge extension found.");*/
    ClampToEdge = GL_CLAMP_TO_EDGE_SGIS;
  } else {
    ClampToEdge = GL_CLAMP;
  }

  globalwindowhandler::Init();
  DoubleBuffer = new bitmap(NewRes);
  Res = NewRes;
  fuck_alsa_messages();
  createTexture();
}


//==========================================================================
//
//  graphics::GotoXY
//
//==========================================================================
void graphics::GotoXY (int cx, int cy) {
  curx = cx;
  cury = cy;
}


//==========================================================================
//
//  graphics::IsCursorVisible
//
//==========================================================================
bool graphics::IsCursorVisible () {
  return (curvisible > 0);
}


//==========================================================================
//
//  graphics::ShowCursor
//
//==========================================================================
void graphics::ShowCursor () {
  ++curvisible;
}


//==========================================================================
//
//  graphics::HideCursor
//
//==========================================================================
void graphics::HideCursor () {
  --curvisible;
}


//==========================================================================
//
//  graphics::GetCursorState
//
//==========================================================================
graphics::CursorState graphics::GetCursorState () {
  return CursorState(curx, cury, curvisible, curtype);
}


//==========================================================================
//
//  graphics::SwitchMode
//
//==========================================================================
void graphics::SwitchMode () {
  IvanAssert(OSWindow);

  if (SwitchModeHandler) SwitchModeHandler();

  isFullscreen = !isFullscreen;
  if (isFullscreen) {
    SDL_SetWindowFullscreen(OSWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
  } else {
    SDL_SetWindowFullscreen(OSWindow, 0);
    SDL_SetWindowSize(OSWindow, fixVal(Res.X), fixVal(Res.Y));
  }

  createTexture();
  BlitDBToScreen();
}


//==========================================================================
//
//  graphics::ReinitBlurry
//
//==========================================================================
void graphics::ReinitBlurry (bool isBlurry, bool isOverBlurry) {
  blurryScale = isBlurry;
  blurryOversample = isOverBlurry;
  createTexture();
  BlitDBToScreen();
}


//==========================================================================
//
//  graphics::ReinitMode
//
//==========================================================================
void graphics::ReinitMode (sLong adresmod) {
  if (adresmod < 0) adresmod = 0; else if (adresmod > 10) adresmod = 10;
  dblRes = (adresmod > 0);
  if (adresmod == 10) DIVISOR = 2.0f; else DIVISOR = 1.0f+(adresmod/10.0f);

  SDL_SetWindowSize(OSWindow, fixVal(Res.X), fixVal(Res.Y));
  SDL_SetWindowFullscreen(OSWindow, (isFullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));

  createTexture();
  BlitDBToScreen();
}


//==========================================================================
//
//  graphics::LoadDefaultFont
//
//==========================================================================
void graphics::LoadDefaultFont (cfestring &FileName) {
  //DefaultFont = new rawbitmap(FileName);
  delete DefaultFont;
  DefaultFont = new rawfont(FileName, 5, 8, 8, 16, 16, true);
}


//==========================================================================
//
//  graphics::LoadConsoleFont
//
//==========================================================================
void graphics::LoadConsoleFont (cfestring &FileName) {
  //ConsoleFont = new rawbitmap(FileName);
  delete ConsoleFont;
  //ConsoleFont = new rawfont(FileName, 5, 8, 8, 8, 8, false);
  //ConsoleFont = new rawfont(FileName + "cp437_8x8.png", 5, 8, 8, 8, 8, false);
  ConsoleFont = new rawfont(FileName + "cp437_8x12.png", 5, 8, 12, 8, 12, false);
}


//==========================================================================
//
//  skipBlanks
//
//==========================================================================
static const char *skipBlanks (const char *s) {
  if (!s) s = "";
  while (*s && (unsigned char)(*s) <= 32) s += 1;
  return s;
}


//==========================================================================
//
//  skipNonBlanks
//
//==========================================================================
static const char *skipNonBlanks (const char *s) {
  if (!s) s = "";
  while ((unsigned char)(*s) > 32) s += 1;
  return s;
}


//==========================================================================
//
//  isEqu
//
//==========================================================================
static bool isEqu (const char *s0, const char *s1, const char *str) {
  size_t slen = strlen(str);
  if ((size_t)(s1 - s0) != slen) return false;
  while (slen && *s0 == *str) {
    s0 += 1; str += 1; slen -= 1;
  }
  return (slen == 0);
}


//==========================================================================
//
//  graphics::CheckExtension
//
//==========================================================================
bool graphics::CheckExtension (const char *ext) {
  if (!ext || !ext[0]) return false;
  const char *exts = (const char *)glGetString(GL_EXTENSIONS);
  exts = skipBlanks(exts);
  while (*exts) {
    const char *ee = skipNonBlanks(exts);
    if (isEqu(exts, ee, ext)) return true;
    exts = skipBlanks(ee);
  }
  return false;
}


//==========================================================================
//
//  graphics::GetExtFuncPtr
//
//==========================================================================
void *graphics::GetExtFuncPtr (const char *name) {
  return SDL_GL_GetProcAddress(name);
}


//==========================================================================
//
//  graphics::DrawTransientPopup
//
//==========================================================================
void graphics::DrawTransientPopup (cfestring &msg) {
  if (!FONT || !DOUBLE_BUFFER) return;
  int wdt = FONT->TextWidth(msg);
  if (wdt > 0) {
    std::vector<festring> text;
    FONT->WordWrap(msg, text, RES.X - 16 * 4);
    int hgt = (int)text.size() * 10;
    wdt = 0;
    for (size_t f = 0; f != text.size(); f += 1) {
      wdt = Max(wdt, FONT->TextWidth(text[f]));
    }
    v2 pos = v2(16 + 8, 32 + 8);
    bitmap *store = new bitmap(v2(wdt + 6, hgt + 4));
    store->FastCopyFrom(DOUBLE_BUFFER, pos);
    const v2 savePos = pos;
    DOUBLE_BUFFER->DrawSimplePopup(pos, v2(wdt + 6, hgt + 4), WHITE, MakeRGB16(16, 16, 16));
    for (size_t f = 0; f != text.size(); f += 1) {
      FONT->PrintStr(DOUBLE_BUFFER, pos + v2(3, 3), YELLOW, text[f]);
      pos.Y += 10;
    }
    BlitDBToScreen();
    store->FastBlit(DOUBLE_BUFFER, savePos);
    delete store;
  }
}
