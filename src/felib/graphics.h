/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_GRAPHICS_H__
#define __FELIB_GRAPHICS_H__

#include "felibdef.h"
#include <SDL2/SDL.h>
#include <GL/gl.h>
#include "v2.h"

#define VV_GLDECLS
#include "glimports.h"
#undef VV_GLDECLS

#define DOUBLE_BUFFER   graphics::GetDoubleBuffer()
#define RES             graphics::GetRes()
#define FONT            graphics::GetDefaultFont()
#define CON_FONT        graphics::GetConsoleFont()


class bitmap;
class rawbitmap;
class rawfont;
class festring;
class graphics;


class graphics {
  friend class bitmap;

public:
  enum CurType {
    CurCaret,
    CurRect,
    CurUnder,
  };

public:
  struct CursorState {
    friend graphics;
  private:
    int x, y;
    int vis;
    CurType type;
  private:
    inline CursorState (int ax, int ay, int avis, CurType atype) noexcept
      : x(ax)
      , y(ay)
      , vis(avis)
      , type(atype)
    {}
  public:
    inline ~CursorState () noexcept {
      graphics::curx = x; graphics::cury = y;
      graphics::curvisible = vis; graphics::curtype = type;
    }
  };

public:
  static void Init ();
  static void DeInit ();
  static void SwitchMode ();
  static void SetMode (cchar *Title, cchar *IconName, v2 NewRes, truth FullScreen,
                       sLong adresmod, truth BlurryScale, truth BlurryOversample);
  static void BlitDBToScreen ();
  static v2 GetRes () { return Res; }
  static bitmap *GetDoubleBuffer () { return DoubleBuffer; }
  static void LoadDefaultFont (cfestring &FileName);
  static void LoadConsoleFont (cfestring &FileName);
  static rawfont *GetDefaultFont () { return DefaultFont; }
  static rawfont *GetConsoleFont () { return ConsoleFont; }
  static void SetSwitchModeHandler (void (*What)()) { SwitchModeHandler = What; }
  static void ReinitMode (sLong adresmod);
  static void ReinitBlurry (bool isBlurry, bool isOverBlurry);

  static void GotoXY (int cx, int cy);
  static FORCE_INLINE void GotoXY (v2 pos) { GotoXY(pos.X, pos.Y); }
  static bool IsCursorVisible ();
  static void ShowCursor ();
  static void HideCursor ();
  static void CaretCursor () { curtype = CurCaret; }
  static void RectCursor () { curtype = CurRect; }
  static void UnderlineCursor () { curtype = CurUnder; }

  // on destroying, `CursorState` will restore the state
  static CursorState GetCursorState ();

  static inline bool IsConsoleActive () noexcept { return (mConActive && mConEnabled); }
  static inline void ToggleConsole () noexcept { if (mConEnabled) mConActive = !mConActive; }
  static inline void SetConsoleActive (bool v) noexcept { mConActive = (v && mConEnabled); }
  // in text lines
  static inline int ConsoleHeight () noexcept { return mConHeight; }
  static inline void SetConsoleHeight (int l) noexcept { mConHeight = Clamp(l, 1, 64); }

  static festring GetConCmdLine ();
  static void SetConCmdLine (cchar *str);

  static void ConCmdCharKey (char ch);

  static void EnableConsole () { mConEnabled = true; }
  static bool IsConsoleEnabled () { return mConEnabled; }

public:
  // used for "saving..."-like popups. renders popup,
  // blits the screen, restores dblbuffer under the popup
  static void DrawTransientPopup (cfestring &msg);

public:
  static SDL_Window *OSWindow;

private:
  static bool mConEnabled;
  static bool mConActive;
  static int mConHeight;

  static int CalcConHeight (); // in chars, including the command line
  static int CalcConWidth (); // in chars

  static void RenderChar (int x, int y, char ch, uint32_t rgbFG, uint32_t rgbBG);
  static void RenderStr (int x, int y, const char *str, uint32_t rgbFG, uint32_t rgbBG);

  static void RenderCmdLine ();
  static void RenderConsole ();

  static void PrintSuggestion (const char *sugstr, void *udata);
  static void PutCmdChar (char ch, void *udata);

private:
  static void (*SwitchModeHandler)();
  static bitmap *DoubleBuffer;
  static v2 Res;
  //static rawbitmap *DefaultFont;
  //static rawbitmap *ConsoleFont;
  static rawfont *DefaultFont;
  static rawfont *ConsoleFont;

  static int curx;
  static int cury;
  static int curvisible;
  static CurType curtype;

  static truth blurryScale;
  static truth blurryOversample;

  friend CursorState;

  static int fboW, fboH;
  static int fboMult;

  static void createTexture ();
  static void createTextureImage ();
  static void drawCursorImage ();

  static void PutPixel32 (int x, int y, uint32_t color);

  static void createFBO (int awidth, int aheight);
  static void killFBO ();

  static void killTexture ();

public:
  static void *GetExtFuncPtr (const char *name);
  static bool CheckExtension (const char *ext);

  static GLenum ClampToEdge;
#define VV_GLIMPORTS
#define VGLAPIPTR(x,optional)  static x##_t p_##x
#include "glimports.h"
#undef VGLAPIPTR
#undef VV_GLIMPORTS
};


#endif
