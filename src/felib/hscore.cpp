/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

#include "felibdef.h"
#include "hscore.h"
#include "graphics.h"
#include "rawbit.h"
#include "fesave.h"
#include "felist.h"
#include "feio.h"
#include "femath.h"

#include "sq3support.h"


#define HIGH_SCORE_FILENAME  "k8ivan.scores"


bool highscore::lastAddFailed = true;
int highscore::lastId = 0;
festring highscore::dbfilename;


/* massacre types:
  0: "directly by you"
  1: "by your allies"
  2: "by some other reason"
*/


//==========================================================================
//
//  openHiDB
//
//==========================================================================
static bool openHiDB (SQ3DB &db, const char *fname, bool readWrite) {
  if (readWrite) {
    db.OpenRWCreate(fname);
  } else {
    db.OpenRO(fname);
  }
  if (!db.IsOpen()) return false;
  if (readWrite) {
    db.Exec(
      "BEGIN TRANSACTION;\n"
      "\n"
      "CREATE TABLE IF NOT EXISTS hashes (\n"
      "    hash BLOB NOT NULL UNIQUE\n"
      "  , hiid INTEGER NOT NULL UNIQUE\n"
      ") STRICT;\n"
      "\n"
      "CREATE TABLE IF NOT EXISTS hiscores (\n"
      "    id INTEGER PRIMARY KEY\n"
      "  , date INTEGER NOT NULL /*epoch*/\n"
      "  , score INTEGER NOT NULL\n"
      "  , quited INTEGER NOT NULL\n"
      "  , multiplier REAL NOT NULL\n"
      "  , finalscore INTEGER NOT NULL\n"
      "  , ticks INTEGER NOT NULL /* game ticks */\n"
      "  , turns INTEGER NOT NULL /* game turns */\n"
      "  , rtime INTEGER NOT NULL /* real time spent, seconds */\n"
      "  , gday INTEGER NOT NULL  /* game day */\n"
      "  , ghour INTEGER NOT NULL /* game hour */\n"
      "  , gmin INTEGER NOT NULL  /* game minute */\n"
      "  , title TEXT NOT NULL  /* full player title */\n"
      "  , death TEXT NOT NULL  /* death message */\n"
      "  , place TEXT NOT NULL  /* where the player died */\n"
      ") STRICT;\n"
      "CREATE INDEX IF NOT EXISTS hiscores_score ON hiscores (finalscore, id);\n"
      "\n"
      "CREATE TABLE IF NOT EXISTS massacres (\n"
      "    id INTEGER PRIMARY KEY\n"
      "  , hiid INTEGER NOT NULL\n"
      "  , type INTEGER NOT NULL\n"
      "  , what TEXT NOT NULL\n"
      ") STRICT;\n"
      "CREATE INDEX IF NOT EXISTS massacres_hiid ON massacres (id, hiid);\n"
      "\n"
      "CREATE TABLE IF NOT EXISTS massacredetails (\n"
      "    id INTEGER PRIMARY KEY\n"
      "  , mscid INTEGER NOT NULL\n"
      "  , how TEXT NOT NULL\n"
      ") STRICT;\n"
      "CREATE INDEX IF NOT EXISTS massacredetails_mscid ON massacredetails (id, mscid);\n"
      "\n"
    #ifdef HIGHSCORE_SAVE_MESSAGES
      "CREATE TABLE IF NOT EXISTS messages (\n"
      "    id INTEGER PRIMARY KEY\n"
      "  , hiid INTEGER NOT NULL\n"
      "  , message TEXT NOT NULL\n"
      ") STRICT;\n"
      "CREATE INDEX IF NOT EXISTS messages_hiid ON messages (id, hiid);\n"
    #endif
      "\n"
    #ifdef HIGHSCORE_SAVE_ITEMS
      "CREATE TABLE IF NOT EXISTS items (\n"
      "    id INTEGER PRIMARY KEY\n"
      "  , hiid INTEGER NOT NULL\n"
      "  , name TEXT NOT NULL\n"
      "  , wield TEXT NOT NULL\n"
      ") STRICT;\n"
      "CREATE INDEX IF NOT EXISTS items_hiid ON items (id, hiid);\n"
    #endif
      "\n"
      "COMMIT TRANSACTION;\n"
      "");
  }
  return true;
}


//==========================================================================
//
//  closeHiDB
//
//==========================================================================
static void closeHiDB (SQ3DB &db) {
  db.Close();
}


//==========================================================================
//
//  highscore::genFileName
//
//==========================================================================
festring highscore::genFileName (const festring &fname) {
  if (fname[0] != '/') {
    festring path;
  #ifdef LOCAL_SAVES
    path << inputfile::GetMyDir();
  #else
    path << getenv("HOME");
    if (path.IsEmpty()) {
      path << inputfile::GetMyDir();
    } else {
      path << "/.k8ivan";
      outputfile::MakeDir(path);
    }
  #endif
    path << "/" << fname;
    return path;
  }
  return fname;
}


//==========================================================================
//
//  highscore::HasHash
//
//  use to check if we already have the score with the given hash
//
//==========================================================================
bool highscore::HasHash (const Hash hash) {
  bool res = false;
  if (dbfilename.IsEmpty()) dbfilename = genFileName(CONST_S(HIGH_SCORE_FILENAME));
  SQ3DB db;
  if (openHiDB(db, dbfilename.CStr(), false/*WAL!*/)) {
    closeHiDB(db);
    if (openHiDB(db, dbfilename.CStr(), true/*WAL!*/)) {
      SQ3Stmt stmt;
      if (db.Stmt(stmt,
            "SELECT 1 FROM hashes "
            "WHERE hash=:hash "
            "LIMIT 1 "
            ""))
      {
        stmt.BindConstBlob(":hash", hash, (int)sizeof(Hash));
        while (stmt.Step() > 0) {
          res = true;
        }
        stmt.Close();
      }
      closeHiDB(db);
    }
  }
  return res;
}


//==========================================================================
//
//  highscore::Add
//
//==========================================================================
bool highscore::Add (const Info &info, bool showQuits) {
  bool hasHash = false;
  lastAddFailed = true;
  lastId = 0;

  if (dbfilename.IsEmpty()) dbfilename = genFileName(CONST_S(HIGH_SCORE_FILENAME));

  SQ3DB db;
  if (openHiDB(db, dbfilename.CStr(), true)) {
    #if 0
    ConLogf("***HISCORE: adding: <%s> <%s> <%s> %dx%f", plrName.CStr(), deathDesc.CStr(),
            place.CStr(), score, multiplier);
    #endif
    SQ3Stmt stmt;

    if (db.Stmt(stmt,
          "SELECT 1 FROM hashes "
          "WHERE hash=:hash "
          "LIMIT 1 "
          ""))
    {
      stmt.BindConstBlob(":hash", info.hash, (int)sizeof(info.hash));
      while (stmt.Step() > 0) {
        hasHash = true;
      }
      stmt.Close();
    }

    if (!hasHash) {
      if (db.Stmt(stmt,
            "INSERT INTO hiscores "
            "        (score, quited, multiplier, finalscore, ticks, turns, rtime, gday, ghour, gmin, title, death, place, date)\n"
            "  VALUES(:score,:quited,:multiplier,:finalscore,:ticks,:turns,:rtime,:gday,:ghour,:gmin,:title,:death,:place, unixepoch('now'))\n"
            "RETURNING id AS id\n"
            ""))
      {
        stmt.BindInt(":score", info.score);
        stmt.BindInt(":quited", (info.quited ? 1 : 0));
        stmt.BindDouble(":multiplier", info.multiplier);
        stmt.BindInt(":finalscore", (int)(info.score * info.multiplier));
        stmt.BindInt(":ticks", info.ticks);
        stmt.BindInt(":turns", info.turns);
        stmt.BindInt(":rtime", info.rtime);
        stmt.BindInt(":gday", info.gday);
        stmt.BindInt(":ghour", info.ghour);
        stmt.BindInt(":gmin", info.gmin);
        stmt.BindTextNormSpaces(":title", info.plrName);
        stmt.BindTextNormSpaces(":death", info.deathDesc);
        stmt.BindTextNormSpaces(":place", info.place);
        while (stmt.Step() > 0) {
          lastId = stmt.GetInt("id");
        }
        stmt.Close();
      }

      const char *selector =
        showQuits ?
          "SELECT"
          "  id AS id\n"
          "FROM hiscores\n"
          "WHERE quited=0\n"
          "ORDER BY finalscore DESC, id ASC\n"
          "LIMIT 100"
        :
          "SELECT"
          "  id AS id\n"
          "FROM hiscores\n"
          "ORDER BY finalscore DESC, id ASC\n"
          "LIMIT 100"
        ;

      if (db.Stmt(stmt, selector)) {
        while (stmt.Step() > 0 && lastAddFailed) {
          lastAddFailed = (stmt.GetInt("id") != lastId);
        }
        stmt.Close();
      }

      if (db.Stmt(stmt,
            "INSERT INTO hashes "
            "        ( hash, hiid)"
            "  VALUES(:hash, :hiid) "
            ""))
      {
        stmt.BindInt(":hiid", lastId);
        stmt.BindConstBlob(":hash", info.hash, (int)sizeof(info.hash));
        stmt.Exec();
        stmt.Close();
      }
    } else {
      lastAddFailed = true;
    }

    closeHiDB(db);
  } else {
    #if 0
    ConLogf("***HISCORE: cannot add: <%s> <%s> <%s> %dx%f", plrName.CStr(), deathDesc.CStr(),
            place.CStr(), score, multiplier);
    #endif
  }

  return hasHash;
}


//==========================================================================
//
//  highscore::Draw
//
//==========================================================================
void highscore::Draw (bool showQuits) {
  struct Record {
    bool latest;
    int score;
    festring text;
  };

  if (dbfilename.IsEmpty()) dbfilename = genFileName(CONST_S(HIGH_SCORE_FILENAME));

  SQ3DB db;
  // opening it for reading only leaves WAL log. sigh.
  if (!openHiDB(db, dbfilename.CStr(), !false)) {
    iosystem::TextScreen(CONST_S("There are no entries yet. Play a game to correct this. (no db)"));
    return;
  }

  const char *selector =
    showQuits ?
      "SELECT"
      "    id AS id\n"
      "  , title AS title\n"
      "  , finalscore AS finalscore\n"
      "  , death AS death\n"
      "  , place AS place\n"
      "FROM hiscores\n"
      "ORDER BY finalscore DESC, id ASC\n"
      "LIMIT 100"
    :
      "SELECT"
      "    id AS id\n"
      "  , title AS title\n"
      "  , finalscore AS finalscore\n"
      "  , death AS death\n"
      "  , place AS place\n"
      "FROM hiscores\n"
      "WHERE quited=0\n"
      "ORDER BY finalscore DESC, id ASC\n"
      "LIMIT 100"
    ;

  SQ3Stmt stmt;
  if (!db.Stmt(stmt, selector)) {
    closeHiDB(db);
    iosystem::TextScreen(CONST_S("Hiscore database error. Oops."));
    return;
  }

  std::vector<Record> Score;

  while (stmt.Step() > 0) {
    Record rec;
    rec.latest = (stmt.GetInt("id") == lastId);
    rec.score = stmt.GetInt("finalscore");
    rec.text << stmt.GetText("title");
    rec.text << " ";
    rec.text << stmt.GetText("death");
    festring place = stmt.GetText("place");
    if (!place.IsEmpty()) {
      rec.text << " in " << place;
    }
    Score.push_back(rec);
  }

  stmt.Close();
  closeHiDB(db);

  if (Score.size() == 0) {
    iosystem::TextScreen(CONST_S("There are no entries yet. Play a game to correct this."));
    return;
  }

  felist List(CONST_S("Adventurers' Hall of Fame"));

  int numWdt = 0, scoreWdt = 0;
  for (size_t c = 0; c < Score.size(); ++c) {
    // index
    festring ss; ss << c + 1;
    const int nw = FONT->TextWidth(ss);
    if (numWdt < nw) numWdt = nw;
    // score
    ss.Empty(); ss << Score[c].score;
    const int xww = FONT->TextWidth(ss);
    if (scoreWdt < xww) scoreWdt = xww;
  }

  festring Desc;
  bool even = false;
  for (uInt c = 0; c < Score.size(); ++c) {
    /*
    if (!showQuits) {
      if (Entry[c].Find("cowardly quit the game") != festring::NPos) {
        continue;
      }
    }
    */
    Desc.Empty();
    Desc << c + 1;
    FONT->LPadToPixWidth(Desc, numWdt);
    FONT->RPadToPixWidth(Desc, numWdt + 12);

    festring ssc; ssc << Score[c].score;
    FONT->LPadToPixWidth(ssc, scoreWdt);
    FONT->RPadToPixWidth(ssc, scoreWdt + 14);
    Desc << ssc;

    Desc << Score[c].text;

    col16 clr;
    if (Score[c].latest) {
      clr = YELLOW; //WHITE;
    } else if (even) {
      clr = MakeRGB16(144, 144, 144);
    } else {
      clr = MakeRGB16(196, 196, 196);
    }
    List.AddEntry(Desc, clr, 13);
    even = !even;
  }

  List.SetFlags(FADE);
  List.SetPageLength(16);
  List.SetIntraLine(2);
  List.Draw();
}


// ////////////////////////////////////////////////////////////////////////// //
static SQ3DB hidb;


//==========================================================================
//
//  highscore::BeginLastInfo
//
//==========================================================================
bool highscore::BeginLastInfo () {
  EndLastInfo();

  if (lastId != 0) {
    if (dbfilename.IsEmpty()) dbfilename = genFileName(CONST_S(HIGH_SCORE_FILENAME));

    if (openHiDB(hidb, dbfilename.CStr(), true)) {
      hidb.BeginTransaction();
    }
  }

  return hidb.IsOpen();
}


//==========================================================================
//
//  highscore::EndLastInfo
//
//==========================================================================
void highscore::EndLastInfo () {
  if (hidb.IsOpen()) {
    hidb.CommitTransaction();
    closeHiDB(hidb);
  }
}


//==========================================================================
//
//  highscore::AddLastItem
//
//==========================================================================
void highscore::AddLastItem (cfestring &name, cfestring &where) {
}


//==========================================================================
//
//  highscore::AddLastMassacre
//
//==========================================================================
void highscore::AddLastMassacre (int type, cfestring &what, const std::vector<festring> &how) {
  if (hidb.IsOpen()) {
    SQ3Stmt stmt;
    if (hidb.Stmt(stmt,
          "INSERT INTO massacres "
          "        ( hiid, type, what)\n"
          "  VALUES(:hiid,:type,:what)\n"
          "RETURNING id AS id\n"
          ""))
    {
      int mscid = 0;
      stmt.BindInt(":hiid", lastId);
      stmt.BindInt(":type", type);
      stmt.BindTextNormSpaces(":what", what);
      while (stmt.Step() > 0) {
        mscid = stmt.GetInt("id");
      }
      stmt.Close();

      if (hidb.Stmt(stmt,
            "INSERT INTO massacredetails "
            "        ( mscid, how)\n"
            "  VALUES(:mscid,:how)\n"
            ""))
      {
        for (size_t f = 0; f != how.size(); f += 1) {
          stmt.Reset();
          stmt.BindInt(":mscid", mscid);
          #if 0
          ConLogf("<%s> : {%s}", how[f].CStr(), how[f].NormalizeSpaces(true).CStr());
          #endif
          stmt.BindTextNormSpaces(":how", how[f]);
          stmt.Exec();
        }
        stmt.Close();
      }
    }
  }
}


//==========================================================================
//
//  highscore::AddLastMessage
//
//==========================================================================
void highscore::AddLastMessage (cfestring &msg) {
  #ifdef HIGHSCORE_SAVE_MESSAGES
  if (hidb.IsOpen() && !msg.IsEmpty()) {
    // TODO: it it start with blanks, this is continuation
    SQ3Stmt stmt;
    if (hidb.Stmt(stmt,
          "INSERT INTO messages "
          "        ( hiid, message)\n"
          "  VALUES(:hiid,:message)\n"
          ""))
    {
      stmt.BindInt(":hiid", lastId);
      stmt.BindText(":message", msg);
      stmt.Exec();
      stmt.Close();
    }
  }
  #endif
}
