/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_HSCORE_H__
#define __FELIB_HSCORE_H__

#include "felibdef.h"
#include <vector>
#include <ctime>

#include "festring.h"


/* Increment this if changes make highscores incompatible */
/* no more; high scores are in SQLite db now */
/*#define HIGH_SCORE_VERSION 123*/

//#define HIGHSCORE_SAVE_MESSAGES
// saving items is not implemented yet
//#define HIGHSCORE_SAVE_ITEMS


class highscore {
public:
  // 32 random bytes
  typedef uint8_t Hash[32];
public:
  struct Info {
    Hash hash;
    festring plrName;
    festring deathDesc;
    double multiplier;
    sLong score;
    festring place;
    bool quited;
    sLong ticks;
    sLong turns; // game time spent
    sLong rtime; // real time spent
    int gday, ghour, gmin;

    inline Info ()
      : plrName()
      , deathDesc()
      , multiplier(1.0)
      , score(0)
      , place()
      , quited(false)
      , ticks(0)
      , turns(0)
      , rtime(0)
      , gday(0)
      , ghour(0)
      , gmin(0)
    {}
  };

public:
  //highscore (cfestring &File=HIGH_SCORE_FILENAME);
  highscore () = delete;
  highscore (const highscore &) = delete;
  highscore &operator = (const highscore &) = delete;

  // use to check if we already have the score with the given hash
  static bool HasHash (const Hash hash);

  // return `false` for duplicate hash.
  // will set `LastAddFailed` flag.
  // `showQuits` is required for correct `LastAddFailed` setting.
  static bool Add (const Info &info, bool showQuits);

  static inline truth LastAddFailed () { return lastAddFailed; }
  static inline void SetLastAddFailed () { lastAddFailed = true; }

  static void Draw (bool showQuits);

  static bool BeginLastInfo ();
  static void EndLastInfo ();

  static void AddLastItem (cfestring &name, cfestring &where);
  static void AddLastMassacre (int type, cfestring &what, const std::vector<festring> &how);
  // i am not saving messages. meh.
  static void AddLastMessage (cfestring &msg);

private:
  static festring genFileName (const festring &fname);

private:
  static festring dbfilename;
  static bool lastAddFailed;
  static int lastId;
};

static_assert(sizeof(highscore::Hash) == 32, "oops");

#endif
