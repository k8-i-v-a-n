/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <new>

/*#include "ivancommon.h"*/


void *operator new (size_t size) noexcept(false) {
  if (size > 0x1fffffff) {
    fprintf(stderr, "`new`: size > 0x1fffffff! (%u)\n", (unsigned)size);
    __builtin_trap();
  }
  void *p = ::malloc(size + 64);
  if (!p) {
    fprintf(stderr, "`new`: out of memory for size=%u!\n", size);
    __builtin_trap();
  }
  ::memset(p, 0, size + 64);
  return p;
}

void *operator new[] (size_t size) noexcept(false) {
  if (size > 0x1fffffff) {
    fprintf(stderr, "`new`: size > 0x1fffffff! (%u)\n", (unsigned)size);
    __builtin_trap();
  }
  void *p = ::malloc(size + 64);
  if (!p) {
    fprintf(stderr, "`new`: out of memory for size=%u!\n", size);
    __builtin_trap();
  }
  ::memset(p, 0, size + 64);
  return p;
}


void operator delete (void *p) noexcept {
  ::free(p);
}

void operator delete[] (void *p) noexcept {
  ::free(p);
}

void operator delete (void *p, size_t /*size*/) noexcept {
  ::free(p);
}

void operator delete[] (void *p, size_t /*size*/) noexcept {
  ::free(p);
}
