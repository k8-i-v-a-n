/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __IVANCOMMON_H__
#define __IVANCOMMON_H__

#include <cstdlib>
#include <new>


void *operator new (size_t size) noexcept(false);
void *operator new[] (size_t size) noexcept(false);
void operator delete (void *p) noexcept;
void operator delete[] (void *p) noexcept;
void operator delete (void *p, size_t size) noexcept;
void operator delete[] (void *p, size_t size) noexcept;


#endif
