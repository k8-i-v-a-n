/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include <cstdarg>
#include <png.h>
/*#include <zlib.h>*/

#include "allocate.h"
#include "rawbit.h"
#include "bitmap.h"
#include "fesave.h"
#include "femath.h"


// ////////////////////////////////////////////////////////////////////////// //
// algo is like this:
// we will add chars and color codes directly to the line.
// on our way, we will remember the last word boundary (before the separating space).
// if new word char will overflow the line, we'll split at the word boundary,
// and remove all prepending spaces from the last word, then re-add it.

struct WrapDataFont {
public:
  const rawfont *bmp;
  int preWidthPix;
  int maxWidthPix;
  festring line; // current line
  int lineWidth; // current line width
  int lastWordStart;
    //  0: first word, no real chars (only indent and color codes)
    // -1: first word, was at least one real char
    // >0: position of the delimiting space of the current word
  int lastWordLastChar;
  bool prevWasNL;
    //  0: none
    // <0: nbsp
    // >0: normal
  char clrchars[16];
  char wstartclr[16];
  std::vector<festring> &resver;

public:
  inline WrapDataFont (const rawfont *abmp, int aPreWidth, int aMaxWidth, std::vector<festring> &aresver)
    : bmp(abmp)
    , preWidthPix(aPreWidth)
    , maxWidthPix(aMaxWidth)
    , line()
    , lineWidth(0)
    , lastWordStart(0)
    , lastWordLastChar(0)
    , prevWasNL(false)
    , resver(aresver)
  {
    clrchars[0] = 0; wstartclr[0] = 0;
    if (aPreWidth > 0) preWidthPix *= abmp->CharWidth(' '); else preWidthPix = -aPreWidth;
  }

private:
  void ResetLine () {
    line.Empty();
    lineWidth = 0;
    lastWordStart = 0;
    lastWordLastChar = 0;
  }

  void PutIndent () {
    int preLeft = preWidthPix;
    while (preLeft > 0) {
      int ww = (preLeft > 8 ? 8 : ww);
      line.AppendChar(15 + ww);
      lineWidth += ww;
      preLeft -= ww;
    }
  }

  void ForceFlushLine () {
    resver.push_back(line);
    ResetLine();
    line.AppendStr(wstartclr);
    strcpy(wstartclr, clrchars);
    PutIndent();
  }

  inline void SetLastChar (char ch) {
    if (ch != 0) {
      lastWordLastChar = (RawTextData::IsPadBlank(ch) ? -1 : 1);
    }
  }

public:
  void PutColorCode (const char *ccode) {
    if (ccode && ccode[0]) {
      const size_t slen = strlen(ccode);
      IvanAssert(slen < sizeof(clrchars));
      line.AppendStr(ccode);
      // remember color code, so we will start a new line with it
      if (ccode[0] == 2 && ccode[1] == 0) {
        // this is "revert to default color". no need to remembe it,
        // because each new line already starts with the default color.
        clrchars[0] = 0;
      } else {
        strcpy(clrchars, ccode);
      }
      if (lastWordStart == 0 || lastWordLastChar == 0) {
        strcpy(wstartclr, clrchars);
      }
    }
  }

  void PutChar (char ch) {
    IvanAssert(ch != 0);
    if (RawTextData::IsRealBlank(ch)) {
      // ignore leading line spaces
      if (lastWordStart != 0 || lastWordLastChar != 0 || (ch == '\n' && prevWasNL)) {
        if (ch == '\n') {
          // new line
          ForceFlushLine();
          lastWordLastChar = 0;
        } else {
          // new word starts here
          lastWordStart = line.Length();
          strcpy(wstartclr, clrchars);
        }
        lastWordLastChar = 0;
      }
      prevWasNL = (ch == '\n');
    } else {
      prevWasNL = false;
      const int cwdt = bmp->CharWidth(ch);
      if (lastWordStart == 0) {
        // first real char of the first word, yay
        strcpy(wstartclr, clrchars);
        line.AppendChar(ch);
        lineWidth += cwdt;
        lastWordStart = -1;
        SetLastChar(ch);
      } else if (lastWordStart < 0) {
        if (lineWidth + cwdt <= maxWidthPix) {
          line.AppendChar(ch);
          lineWidth += cwdt;
          SetLastChar(ch);
        } else {
          // this is the first word, and it should be forcefully split
          ForceFlushLine();
          PutChar(ch);
        }
      } else {
        int newLineWidth = lineWidth + cwdt;
        // append delimiting space
        if (lastWordLastChar == 0) {
          strcpy(wstartclr, clrchars);
        }
        const bool needSpace = (lastWordLastChar == 0 && !RawTextData::IsPadBlank(ch));
        if (needSpace) newLineWidth += bmp->CharWidth(' ');
        // does it fit?
        if (newLineWidth <= maxWidthPix) {
          // append delimiting space
          if (needSpace) line.AppendChar(' ');
          lineWidth = newLineWidth;
          line.AppendChar(ch);
          SetLastChar(ch);
        } else {
          // doesn't fit, flush the line, and start over
          festring word(line);
          word.Erase(0, (festring::sizetype)lastWordStart);
          line.SetLength((festring::sizetype)lastWordStart);
          ForceFlushLine();
          RawTextData raw(word);
          while (!raw.IsEmpty()) {
            char c1 = raw.SkipChar();
            if (c1 == 0) {
              PutColorCode(raw.lastClr);
            } else {
              PutChar(c1);
            }
          }
          PutChar(ch);
        }
      }
    }
  }

  void ProcessStr (cfestring &str) {
    RawTextData raw(str);
    while (!raw.IsEmpty()) {
      char ch = raw.SkipChar();
      if (ch == 0) {
        PutColorCode(raw.lastClr);
      } else {
        PutChar(ch);
      }
    }
  }

  void Finish () {
    // check if we have anything printable in the current line
    RawTextData raw(line);
    bool hasPrintable = false;
    while (!hasPrintable && !raw.IsEmpty()) {
      char ch = raw.SkipChar();
      hasPrintable = (ch != 0 && !RawTextData::IsBlank(ch) && bmp->CharWidth(ch) > 0);
    }
    if (hasPrintable) {
      resver.push_back(line);
    }
    ResetLine(); prevWasNL = false;
    clrchars[0] = 0;
  }
};


// ////////////////////////////////////////////////////////////////////////// //
festring rawbitmap::curfile;


//==========================================================================
//
//  intensityGamma2
//
//  roughly 2.0 gamma, faster than converting to CIE and back
//
//==========================================================================
static OK_UNUSED FORCE_INLINE float intensityGamma2 (col16 col) {
  auto r = GetRed16(col);
  auto g = GetGreen16(col);
  auto b = GetBlue16(col);
  const float rf = r / 255.0f;
  const float gf = g / 255.0f;
  const float bf = b / 255.0f;
  float i = 0.212655f * (rf * rf) + 0.715158f * (gf * gf) + 0.072187f * (bf * bf);
  if (i > 0) { i = sqrtf(i); if (i > 1.0f) i = 1.0f; }
  return i;
}


//==========================================================================
//
//  intensityRough
//
//==========================================================================
static OK_UNUSED FORCE_INLINE float intensityRough (col16 col) {
  auto r = GetRed16(col);
  auto g = GetGreen16(col);
  auto b = GetBlue16(col);
  return (0.212655f * r) + (0.715158f * g) + (0.072187f * b);
}


//==========================================================================
//
//  MkGradientColor
//
//==========================================================================
static inline col16 MkGradientColor (col16 col, int cy, int maxy) {
  const float intens = intensityGamma2(col);
  if (intens < 0.6f) {
    if (maxy - cy <= 1) return col;
    cy += 1;
  } else {
    if (maxy - cy <= 2) return col;
    cy += 1;
  }
  auto r = GetRed16(col);
  auto g = GetGreen16(col);
  auto b = GetBlue16(col);
  //auto mc = (r > g ? r : g); if (b > mc) mc = b;
  int dcr = (intens < 0.6f ? 96 : 128);
  dcr = (maxy - cy) * dcr / maxy;
  if (r > dcr) r -= dcr; else r = 0;
  if (g > dcr) g -= dcr; else g = 0;
  if (b > dcr) b -= dcr; else b = 0;
  return MakeRGB16(r, g, b);
}


//==========================================================================
//
//  rawbitmap::TranslateToPaletteOf
//
//==========================================================================
void rawbitmap::TranslateToPaletteOf (rawbitmap *srcbmp) {
  IvanAssert(srcbmp);
  IvanAssert(srcbmp->Palette);

  uint8_t ctrans[256]; // our color to srcbmp color
  for (int f = 0; f != 256; f += 1) {
    if (f != TRANSPARENT_PALETTE_INDEX) {
      const int dr = Palette[f * 3 + 0];
      const int dg = Palette[f * 3 + 1];
      const int db = Palette[f * 3 + 2];
      int bestColor = -1;
      int bestDist = 0x7fffffff;
      for (int c = 0; c != 256 && bestDist != 0; c += 1) {
        int sr = srcbmp->Palette[c * 3 + 0] - dr;
        int sg = srcbmp->Palette[c * 3 + 1] - dg;
        int sb = srcbmp->Palette[c * 3 + 2] - db;
        int dist = (sr * sr) + (sg * sg) + (sb * sb);
        IvanAssert(dist >= 0);
        if (dist < bestDist) {
          bestDist = dist;
          bestColor = c;
        }
      }
      IvanAssert(bestColor >= 0 && bestColor <= 255);
      ctrans[f] = bestColor;
    } else {
      ctrans[f] = TRANSPARENT_PALETTE_INDEX;
    }
  }

  // now translate colors
  for (int y = 0; y != Size.Y; y += 1) {
    for (int x = 0; x != Size.X; x += 1) {
      uChar clr = PaletteBuffer[y][x];
      PaletteBuffer[y][x] = ctrans[clr];
    }
  }

  // and copy palette
  memcpy(Palette, srcbmp->Palette, 256 * 3);
}


//==========================================================================
//
//  rawbitmap::Copy
//
//==========================================================================
RawBmpLump rawbitmap::Copy (v2 src, v2 size) const {
  RawBmpLump res(size.X, size.Y);
  if (size.X > 0 && size.Y > 0) {
    uint8_t *dest = res.GetBuffer();
    for (int y = 0; y != size.Y; y += 1) {
      for (int x = 0; x != size.X; x += 1) {
        *dest = GetRawPixel(src + v2(x, y));
        dest += 1;
      }
    }
  }
  return res;
}


//==========================================================================
//
//  rawbitmap::Paste
//
//==========================================================================
void rawbitmap::Paste (const RawBmpLump &bmp, v2 dest) {
  const int wdt = bmp.GetWidth();
  const int hgt = bmp.GetHeight();
  if (wdt > 0 && hgt > 0) {
    const uint8_t *src = bmp.GetBuffer();
    for (int y = 0; y != hgt; y += 1) {
      for (int x = 0; x != wdt; x += 1) {
        SetRawPixel(dest + v2(x, y), *src);
        src += 1;
      }
    }
  }
}


struct XData {
  VFile fd;
};


//==========================================================================
//
//  png_read_data_cb
//
//==========================================================================
static void png_read_data_cb (png_structp png_ptr, png_bytep data, png_size_t length) {
  XData *dp = (XData *)png_get_io_ptr(png_ptr);
  if (vwread(dp->fd, data, (int)length) != (int)length) {
    ABORT("cannot decode png file"); // oops
  }
}


//==========================================================================
//
//  rawbitmap::loadPNG
//
//==========================================================================
void rawbitmap::loadPNG (VFile fl) {
  if (vwseek(fl, 0) != 0) ABORT("Error reading file '%s'!", curfile.CStr());

  png_structp PNGStruct = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
  if (!PNGStruct) ABORT("Couldn't read PNG file %s!", curfile.CStr());

  png_infop PNGInfo = png_create_info_struct(PNGStruct);
  if (!PNGInfo) ABORT("Couldn't read PNG file %s!", curfile.CStr());

  if (setjmp(png_jmpbuf(PNGStruct)) != 0) {
    ABORT("Couldn't read PNG file %s!", curfile.CStr());
  }

  //png_init_io(PNGStruct, fl);
  XData xdata;
  xdata.fd = fl;
  png_set_read_fn(PNGStruct, &xdata, &png_read_data_cb);
  png_read_info(PNGStruct, PNGInfo);

  Size.X = png_get_image_width(PNGStruct, PNGInfo);
  Size.Y = png_get_image_height(PNGStruct, PNGInfo);

  if (Size.X < 1 || Size.X > 32700) ABORT("Invalid bitmap width: %s!", curfile.CStr());
  if (Size.Y < 1 || Size.Y > 32700) ABORT("Invalid bitmap height: %s!", curfile.CStr());

  const int bdepth = (int)png_get_bit_depth(PNGStruct, PNGInfo);
  if (bdepth != 1 && bdepth != 2 && bdepth != 4 && bdepth != 8) {
    ABORT("%s has wrong bit depth %d, should be 1, 2, 4 or 8!", curfile.CStr(), bdepth);
  }

  png_colorp PNGPalette;
  int PaletteSize;

  if (!png_get_PLTE(PNGStruct, PNGInfo, &PNGPalette, &PaletteSize)) {
    ABORT("%s is not in indexed color mode!", curfile.CStr());
  }

  if (PaletteSize > 256) {
    ABORT("%s has wrong palette size %d, should be <256!", curfile.CStr(), PaletteSize);
  }

  Palette = new uChar[768];
  memset(Palette, 0, 768);

  for (int i = 0; i != PaletteSize; i += 1) {
    Palette[i*3+0] = PNGPalette[i].red;
    Palette[i*3+1] = PNGPalette[i].green;
    Palette[i*3+2] = PNGPalette[i].blue;
  }

  Alloc2D(PaletteBuffer, Size.Y, Size.X);
  png_read_image(PNGStruct, PaletteBuffer);

  // unpack, if necessary
  uint8_t *temp = 0;
  if (bdepth == 1 || bdepth == 2 || bdepth == 4) {
    temp = (uint8_t *)::malloc(Size.X);
    if (!temp) ABORT("out of memory!");
    for (int y = 0; y != Size.Y; y += 1) {
      paletteindex *line = &PaletteBuffer[y][0];
      if (bdepth == 1) {
        memcpy(temp, line, (Size.X + 7) / 8);
        for (int x = 0; x != Size.X; x += 1) {
          uint8_t b = temp[x / 8] & (0x100 >> ((x & 7) + 1));
          line[x] = (b ? 1 : 0);
        }
      } else if (bdepth == 2) {
        memcpy(temp, line, (Size.X + 3) / 4);
        for (int x = 0; x != Size.X; x += 1) {
          uint8_t b = temp[x / 4];
          if ((x & 3) != 3) b >>= (3 - (x & 3)) * 2;
          line[x] = b & 0x03;
        }
      } else {
        IvanAssert(bdepth == 4);
        memcpy(temp, line, (Size.X + 1) / 2);
        for (int x = 0; x != Size.X; x += 1) {
          uint8_t b = temp[x / 2];
          if ((x & 1) == 0) b = (b >> 4);
          line[x] = b & 0x0f;
        }
      }
    }
  } else {
    IvanAssert(bdepth == 8);
  }

  if (temp) ::free(temp);

  png_destroy_read_struct(&PNGStruct, &PNGInfo, nullptr);
}


//==========================================================================
//
//  rawbitmap::loadFromFile
//
//==========================================================================
void rawbitmap::loadFromFile (VFile fl) {
  unsigned char c = 0;
  if (!fl) ABORT("Cannot read from nothing!");
  //if (fread(&c, 1, 1, fl) != 1) ABORT("Error reading file '%s'!", curfile.CStr());
  if (vwread(fl, &c, 1) != 1) ABORT("Error reading file '%s'!", curfile.CStr());
  if (c == 0x0a) ABORT("PCX support is phased out");
  loadPNG(fl);
}


//==========================================================================
//
//  rawbitmap::rawbitmap
//
//==========================================================================
rawbitmap::rawbitmap ()
  : Size(0, 0)
  , Palette(nullptr)
  , PaletteBuffer(nullptr)
{
}


//==========================================================================
//
//  rawbitmap::rawbitmap
//
//==========================================================================
rawbitmap::rawbitmap (cfestring &FileName)
  : Size(0, 0)
  , Palette(nullptr)
  , PaletteBuffer(nullptr)
{
  curfile = FileName;

  if (curfile.EndsWithCI(".pcx")) {
    curfile.Erase(curfile.GetSize()-4, 4);
    curfile += ".png";
  }

  VFile fl = vwopen(curfile.CStr());
  if (!fl) {
    ABORT("Cannot open file '%s'!", FileName.CStr());
  }
  loadFromFile(fl);
  vwclose(fl);

  curfile.Empty();
}


//==========================================================================
//
//  rawbitmap::rawbitmap
//
//==========================================================================
rawbitmap::rawbitmap (v2 Size)
  : Size(Size)
  , Palette(nullptr)
  , PaletteBuffer(nullptr)
{
  Palette = new uChar[768];
  Alloc2D(PaletteBuffer, Size.Y, Size.X);
}


//==========================================================================
//
//  rawbitmap::rawbitmap
//
//==========================================================================
rawbitmap::rawbitmap (const rawbitmap &src, v2 pos, v2 size)
  : Size(size)
  , Palette(nullptr)
  , PaletteBuffer(nullptr)
{
  IvanAssert((void *)this != (void *)&src);
  IvanAssert(size.X > 0 && size.Y > 0);
  Palette = new uChar[768];
  Alloc2D(PaletteBuffer, size.Y, size.X);
  memcpy(Palette, src.Palette, 768);
  for (int y = 0; y != size.Y; y += 1) {
    for (int x = 0; x != size.X; x += 1) {
      SetRawPixel(v2(x, y), src.GetRawPixel(pos + v2(x, y)));
    }
  }
}


//==========================================================================
//
//  rawbitmap::~rawbitmap
//
//==========================================================================
rawbitmap::~rawbitmap () {
  delete [] Palette;
  delete [] PaletteBuffer;
}


//==========================================================================
//
//  pngWrite
//
//==========================================================================
static void pngWrite (png_structp png_ptr, png_bytep data, png_size_t length) {
  FILE *fp = (FILE *)png_get_io_ptr(png_ptr);
  fwrite(data, length, 1, fp);
}


//==========================================================================
//
//  rawbitmap::SavePNG
//
//==========================================================================
void rawbitmap::SavePNG (cfestring &FileName) const {
  png_structp png_ptr;
  png_infop info_ptr;
  //int ret;
  //png_colorp palette;
  png_byte **row_pointers = NULL;
  png_ptr = NULL;
  info_ptr = NULL;
  //palette = NULL;
  //ret = -1;
  FILE *fp = fopen(FileName.CStr(), "wb");
  if (fp == 0) return;
  row_pointers = (png_byte **)malloc(Size.Y * sizeof(png_byte *));
  //if (!row_pointers);
  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL,NULL,NULL);
  //if (!png_ptr);
  info_ptr = png_create_info_struct(png_ptr);
  //if (!info_ptr);
  // setup custom writer function
  png_set_write_fn(png_ptr, (void *)fp, pngWrite, NULL);
  if (setjmp(png_jmpbuf(png_ptr))) {
    ABORT("err");
  }
  png_set_filter(png_ptr, 0, PNG_FILTER_NONE);
  png_set_compression_level(png_ptr, 9/*Z_BEST_COMPRESSION*/);

  png_set_IHDR(png_ptr, info_ptr, Size.X, Size.Y,
               8, PNG_COLOR_TYPE_PALETTE,
               PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

  png_color *pngpal = (png_color *)malloc(256 * sizeof(png_color));
  for (int f = 0; f != 256; f += 1) {
    pngpal[f].red = Palette[f * 3 + 0];
    pngpal[f].green = Palette[f * 3 + 1];
    pngpal[f].blue = Palette[f * 3 + 2];
  }
  png_set_PLTE(png_ptr, info_ptr, pngpal, 256);

  png_write_info(png_ptr, info_ptr);

  const paletteindex *Buffer = PaletteBuffer[0];
  for (int y = 0; y < Size.Y; y += 1) {
    row_pointers[y] = (png_bytep)(((char *)Buffer) + y * Size.X);
  }
  png_write_image(png_ptr, row_pointers);

  png_write_end(png_ptr, NULL);
  fclose(fp);
  free(pngpal);
//savedone: /* clean up and return */
  png_destroy_write_struct(&png_ptr, &info_ptr);
  if (row_pointers) free(row_pointers);
}


//==========================================================================
//
//  rawbitmap::MaskedBlit
//
//==========================================================================
void rawbitmap::MaskedBlit (bitmap *Bitmap, packcol16 *Color) const {
  MaskedBlit(Bitmap, ZERO_V2, ZERO_V2, Size, Color);
}


//==========================================================================
//
//  rawbitmap::MaskedBlit
//
//==========================================================================
void rawbitmap::MaskedBlit (bitmap *Bitmap, v2 Src, v2 Dest, v2 Border, packcol16 *Color) const {
  if (!femath::Clip(Src.X, Src.Y, Dest.X, Dest.Y, Border.X, Border.Y,
                    Size.X, Size.Y, Bitmap->GetSize().X, Bitmap->GetSize().Y)) return;
  paletteindex *Buffer = &PaletteBuffer[Src.Y][Src.X];
  packcol16 *DestBuffer = &Bitmap->GetImage()[Dest.Y][Dest.X];
  int BitmapXSize = Bitmap->GetSize().X;
  uChar *Palette = this->Palette; // eliminate the efficiency cost of dereferencing
  for (int y = 0; y < Border.Y; ++y) {
    for (int x = 0; x < Border.X; ++x) {
      int PaletteElement = Buffer[x];
      if (PaletteElement >= 192) {
        int ThisColor = Color[(PaletteElement-192)>>4];
        int Index = PaletteElement&15;
        int Red = (ThisColor >> 8 & 0xF8)*Index;
        int Green = (ThisColor >> 3 & 0xFC)*Index;
        int Blue = (ThisColor << 3 & 0xF8)*Index;
        if (Red > 0x7FF) Red = 0x7FF;
        if (Green > 0x7FF) Green = 0x7FF;
        if (Blue > 0x7FF) Blue = 0x7FF;
        DestBuffer[x] = (Red << 5 & 0xF800) | (Green & 0x7E0) | (Blue >> 6 & 0x1F);
      } else {
        int PaletteIndex = PaletteElement+(PaletteElement << 1);
        int ThisColor =
          ((Palette[PaletteIndex] & 0xFFF8) << 8) |
          ((Palette[PaletteIndex+1] & 0xFFFC) << 3) |
          (Palette[PaletteIndex+2] >> 3);
        if (ThisColor != TRANSPARENT_COLOR) DestBuffer[x] = ThisColor;
      }
    }
    DestBuffer += BitmapXSize;
    Buffer += Size.X;
  }
}


//==========================================================================
//
//  rawbitmap::Colorize
//
//==========================================================================
bitmap *rawbitmap::Colorize (v2 Pos, v2 Border, v2 Move, cpackcol16 *Color, alpha BaseAlpha,
                             cpackalpha *Alpha, cuchar *RustData, truth AllowReguralColors,
                             truth SimpleOutline) const
{
  bitmap *Bitmap = new bitmap(Border);

  v2 TargetPos(0, 0);
  if (Move.X || Move.Y) {
    Bitmap->ClearToColor(TRANSPARENT_COLOR);
    if (Move.X < 0) {
      Pos.X -= Move.X;
      Border.X += Move.X;
    } else if (Move.X > 0) {
      TargetPos.X = Move.X;
      Border.X -= Move.X;
    }
    if (Move.Y < 0) {
      Pos.Y -= Move.Y;
      Border.Y += Move.Y;
    } else if (Move.Y > 0) {
      TargetPos.Y = Move.Y;
      Border.Y -= Move.Y;
    }
  }

  paletteindex *Buffer = &PaletteBuffer[Pos.Y][Pos.X];
  packcol16 *DestBuffer = &Bitmap->GetImage()[TargetPos.Y][TargetPos.X];
  int BitmapXSize = Bitmap->GetSize().X;
  uChar *Palette = this->Palette; // eliminate the efficiency cost of dereferencing

  packalpha *AlphaMap;
  truth UseAlpha;
  if (BaseAlpha != 255 || (Alpha && (Alpha[0] != 255 || Alpha[1] != 255 || Alpha[2] != 255 || Alpha[3] != 255))) {
    Bitmap->CreateAlphaMap(BaseAlpha);
    AlphaMap = &Bitmap->GetAlphaMap()[TargetPos.Y][TargetPos.X];
    UseAlpha = true;
  } else {
    AlphaMap = 0;
    UseAlpha = false;
  }

  truth Rusted = RustData && (RustData[0] || RustData[1] || RustData[2] || RustData[3]);
  feuLong RustSeed[4];
  if (Rusted) {
    RustSeed[0] = (RustData[0]&0xFC)>>2;
    RustSeed[1] = (RustData[1]&0xFC)>>2;
    RustSeed[2] = (RustData[2]&0xFC)>>2;
    RustSeed[3] = (RustData[3]&0xFC)>>2;
  }

  for (int y = 0; y < Border.Y; ++y) {
    for (int x = 0; x < Border.X; ++x) {
      int PaletteElement = Buffer[x];
      if (PaletteElement >= 192) {
        int ColorIndex = (PaletteElement-192)>>4;
        int ThisColor = Color[ColorIndex];
        if (ThisColor != TRANSPARENT_COLOR) {
          int Index = PaletteElement & 15;
          int Red = (ThisColor >> 8 & 0xF8)*Index;
          int Green = (ThisColor >> 3 & 0xFC)*Index;
          int Blue = (ThisColor << 3 & 0xF8)*Index;
          if (Rusted && RustData[ColorIndex] &&
              (RustData[ColorIndex] & 3UL) > (RustSeed[ColorIndex] = RustSeed[ColorIndex] * 1103515245 + 12345) >> 30) {
            Green = ((Green << 1) + Green) >> 2;
            Blue >>= 1;
          }
          if (Red > 0x7FF) Red = 0x7FF;
          if (Green > 0x7FF) Green = 0x7FF;
          if (Blue > 0x7FF) Blue = 0x7FF;
          DestBuffer[x] = (Red << 5 & 0xF800) | (Green & 0x7E0) | (Blue >> 6 & 0x1F);
          if (UseAlpha) AlphaMap[x] = Alpha[ColorIndex];
        } else {
          DestBuffer[x] = TRANSPARENT_COLOR;
        }
      } else if (AllowReguralColors) {
        int PaletteIndex = PaletteElement+(PaletteElement<<1);
        DestBuffer[x] =
          ((Palette[PaletteIndex] & 0xFFF8) << 8) |
          ((Palette[PaletteIndex + 1] & 0xFFFC) << 3) |
          (Palette[PaletteIndex + 2] >> 3);
      } else {
        DestBuffer[x] = TRANSPARENT_COLOR;
      }
    }
    DestBuffer += BitmapXSize;
    AlphaMap += BitmapXSize;
    Buffer += Size.X;
  }

  if (SimpleOutline) {
    Bitmap->OutlineRect(v2(0, 0), Bitmap->GetSize(), MakeRGB16(42, 42, 42));
  }

  return Bitmap;
}


//==========================================================================
//
//  rawbitmap::AlterGradient
//
//==========================================================================
void rawbitmap::AlterGradient (v2 Pos, v2 Border, int MColor, int Amount, truth Clip) {
  int ColorMin = 192+(MColor<<4);
  int ColorMax = 207+(MColor<<4);
  if (Clip) {
    for (int x = Pos.X; x < Pos.X + Border.X; ++x) {
      for (int y = Pos.Y; y < Pos.Y + Border.Y; ++y) {
        int Pixel = PaletteBuffer[y][x];
        if (Pixel >= ColorMin && Pixel <= ColorMax) {
          int NewPixel = Pixel+Amount;
          if (NewPixel < ColorMin) NewPixel = ColorMin;
          if (NewPixel > ColorMax) NewPixel = ColorMax;
          PaletteBuffer[y][x] = NewPixel;
        }
      }
    }
  } else {
    int x;
    for (x = Pos.X; x < Pos.X + Border.X; ++x) {
      for (int y = Pos.Y; y < Pos.Y + Border.Y; ++y) {
        int Pixel = PaletteBuffer[y][x];
        if (Pixel >= ColorMin && Pixel <= ColorMax) {
          int NewPixel = Pixel+Amount;
          if (NewPixel < ColorMin) return;
          if (NewPixel > ColorMax) return;
        }
      }
    }
    for (x = Pos.X; x < Pos.X + Border.X; ++x) {
      for (int y = Pos.Y; y < Pos.Y + Border.Y; ++y) {
        int Pixel = PaletteBuffer[y][x];
        if (Pixel >= ColorMin && Pixel <= ColorMax) PaletteBuffer[y][x] = Pixel+Amount;
      }
    }
  }
}


//==========================================================================
//
//  rawbitmap::SwapColors
//
//==========================================================================
void rawbitmap::SwapColors (v2 Pos, v2 Border, int Color1, int Color2) {
  if (Color1 > 3 || Color2 > 3) ABORT("Illgal col swap!");
  for (int x = Pos.X; x < Pos.X + Border.X; ++x) {
    for (int y = Pos.Y; y < Pos.Y + Border.Y; ++y) {
      paletteindex &Pixel = PaletteBuffer[y][x];
      if (Pixel >= 192+(Color1<<4) && Pixel <= 207+(Color1<<4)) Pixel += (Color2-Color1)<<4;
      else if (Pixel >= 192+(Color2<<4) && Pixel <= 207+(Color2<<4)) Pixel += (Color1-Color2)<<4;
    }
  }
}


//==========================================================================
//
//  rawbitmap::Roll
//
//  TempBuffer must be an array of Border.X * Border.Y paletteindices
//
//==========================================================================
void rawbitmap::Roll (v2 Pos, v2 Border, v2 Move, paletteindex *TempBuffer) {
  int x, y;
  for (x = Pos.X; x < Pos.X + Border.X; ++x) {
    for (y = Pos.Y; y < Pos.Y + Border.Y; ++y) {
      int XPos = x+Move.X, YPos = y+Move.Y;
      if (XPos < Pos.X) XPos += Border.X;
      if (YPos < Pos.Y) YPos += Border.Y;
      if (XPos >= Pos.X+Border.X) XPos -= Border.X;
      if (YPos >= Pos.Y+Border.Y) YPos -= Border.Y;
      TempBuffer[(YPos-Pos.Y)*Border.X+XPos-Pos.X] = PaletteBuffer[y][x];
    }
  }
  for (x = Pos.X; x < Pos.X + Border.X; ++x)
    for (y = Pos.Y; y < Pos.Y + Border.Y; ++y)
      PaletteBuffer[y][x] = TempBuffer[(y-Pos.Y)*Border.X+x-Pos.X];
}


//==========================================================================
//
//  rawbitmap::RandomizeSparklePos
//
//  returns ERROR_V2 if fails find Pos else returns pos
//
//==========================================================================
v2 rawbitmap::RandomizeSparklePos (PRNG &prng, cv2 *ValidityArray, v2 *PossibleBuffer, v2 Pos, v2 Border,
  int ValidityArraySize, int SparkleFlags) const
{
  if (!SparkleFlags) return ERROR_V2;
  /* Don't use { } to initialize, or GCC optimizations will produce code that crashes! */
  v2 *BadPossible[4];
  BadPossible[0] = PossibleBuffer;
  BadPossible[1] = BadPossible[0]+((Border.X+Border.Y)<<1)-4;
  BadPossible[2] = BadPossible[1]+((Border.X+Border.Y)<<1)-12;
  BadPossible[3] = BadPossible[2]+((Border.X+Border.Y)<<1)-20;
  v2 *PreferredPossible = BadPossible[3]+((Border.X+Border.Y)<<1)-28;
  int Preferred = 0;
  int Bad[4] = { 0, 0, 0, 0 };
  int XMax = Pos.X+Border.X;
  int YMax = Pos.Y+Border.Y;
  for (int c = 0; c < ValidityArraySize; ++c) {
    v2 V = ValidityArray[c]+Pos;
    int Entry = PaletteBuffer[V.Y][V.X];
    if (IsMaterialColor(Entry) && 1 << GetMaterialColorIndex(Entry) & SparkleFlags) {
      int MinDist = 0x7FFF;
      if (V.X < Pos.X+4) MinDist = Min(V.X-Pos.X, MinDist);
      if (V.X >= XMax-4) MinDist = Min(XMax-V.X-1, MinDist);
      if (V.Y < Pos.Y+4) MinDist = Min(V.Y-Pos.Y, MinDist);
      if (V.Y >= YMax-4) MinDist = Min(YMax-V.Y-1, MinDist);
      if (MinDist >= 4) PreferredPossible[Preferred++] = V;
      else BadPossible[MinDist][Bad[MinDist]++] = V;
    }
  }
  v2 Return;
  if (Preferred) Return = PreferredPossible[X_RAND_N(prng, Preferred)]-Pos;
  else if (Bad[3]) Return = BadPossible[3][X_RAND_N(prng, Bad[3])]-Pos;
  else if (Bad[2]) Return = BadPossible[2][X_RAND_N(prng, Bad[2])]-Pos;
  else if (Bad[1]) Return = BadPossible[1][X_RAND_N(prng, Bad[1])]-Pos;
  else if (Bad[0]) Return = BadPossible[0][X_RAND_N(prng, Bad[0])]-Pos;
  else Return = ERROR_V2;
  return Return;
}


//==========================================================================
//
//  rawbitmap::CopyPaletteFrom
//
//==========================================================================
void rawbitmap::CopyPaletteFrom (rawbitmap *Bitmap) {
  memmove(Palette, Bitmap->Palette, 768);
}


//==========================================================================
//
//  rawbitmap::Clear
//
//==========================================================================
void rawbitmap::Clear () {
  memset(PaletteBuffer[0], TRANSPARENT_PALETTE_INDEX, Size.X*Size.Y*sizeof(paletteindex));
}


//==========================================================================
//
//  rawbitmap::NormalBlit
//
//==========================================================================
void rawbitmap::NormalBlit (rawbitmap *Bitmap, v2 Src, v2 Dest, v2 Border, int Flags) const {
  paletteindex **SrcBuffer = PaletteBuffer;
  paletteindex **DestBuffer = Bitmap->PaletteBuffer;
  int TrueDestXMove;
  paletteindex *DestBase;
  switch (Flags & 7) {
    case NONE:
      if (Size.X == Bitmap->Size.X && Size.Y == Bitmap->Size.Y) {
        memmove(DestBuffer[0], SrcBuffer[0], Size.X*Size.Y*sizeof(paletteindex));
      } else {
        cint Bytes = Border.X*sizeof(paletteindex);
        for (int y = 0; y < Border.Y; ++y) memmove(&DestBuffer[Dest.Y+y][Dest.X], &SrcBuffer[Src.Y+y][Src.X], Bytes);
      }
      break;
    case MIRROR:
      Dest.X += Border.X-1;
      for (int y = 0; y < Border.Y; ++y) {
        cpaletteindex *SrcPtr = &SrcBuffer[Src.Y+y][Src.X];
        cpaletteindex *EndPtr = SrcPtr+Border.X;
        paletteindex *DestPtr = &DestBuffer[Dest.Y+y][Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr) *DestPtr = *SrcPtr;
      }
      break;
    case FLIP: {
      Dest.Y += Border.Y-1;
      cint Bytes = Border.X*sizeof(paletteindex);
      for (int y = 0; y < Border.Y; ++y) memmove(&DestBuffer[Dest.Y-y][Dest.X], &SrcBuffer[Src.Y+y][Src.X], Bytes);
      } break;
    case (MIRROR | FLIP):
      Dest.X += Border.X-1;
      Dest.Y += Border.Y-1;
      for (int y = 0; y < Border.Y; ++y) {
        cpaletteindex *SrcPtr = &SrcBuffer[Src.Y+y][Src.X];
        cpaletteindex *EndPtr = SrcPtr+Border.X;
        paletteindex *DestPtr = &DestBuffer[Dest.Y-y][Dest.X];
        for (; SrcPtr != EndPtr; ++SrcPtr, --DestPtr) *DestPtr = *SrcPtr;
      }
      break;
    case ROTATE:
      Dest.X += Border.X-1;
      TrueDestXMove = Bitmap->Size.X;
      DestBase = &DestBuffer[Dest.Y][Dest.X];
      for (int y = 0; y < Border.Y; ++y) {
        cpaletteindex *SrcPtr = &SrcBuffer[Src.Y+y][Src.X];
        cpaletteindex *EndPtr = SrcPtr+Border.X;
        paletteindex *DestPtr = DestBase-y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += TrueDestXMove) *DestPtr = *SrcPtr;
      }
      break;
    case (MIRROR | ROTATE):
      TrueDestXMove = Bitmap->Size.X;
      DestBase = &DestBuffer[Dest.Y][Dest.X];
      for (int y = 0; y < Border.Y; ++y) {
        cpaletteindex *SrcPtr = &SrcBuffer[Src.Y+y][Src.X];
        cpaletteindex *EndPtr = SrcPtr+Border.X;
        paletteindex *DestPtr = DestBase+y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr += TrueDestXMove) *DestPtr = *SrcPtr;
      }
      break;
    case (FLIP | ROTATE):
      Dest.X += Border.X-1;
      Dest.Y += Border.Y-1;
      TrueDestXMove = Bitmap->Size.X;
      DestBase = &DestBuffer[Dest.Y][Dest.X];
      for (int y = 0; y < Border.Y; ++y) {
        cpaletteindex *SrcPtr = &SrcBuffer[Src.Y+y][Src.X];
        cpaletteindex *EndPtr = SrcPtr+Border.X;
        paletteindex *DestPtr = DestBase-y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= TrueDestXMove) *DestPtr = *SrcPtr;
      }
      break;
   case (MIRROR | FLIP | ROTATE):
      Dest.Y += Border.Y-1;
      TrueDestXMove = Bitmap->Size.X;
      DestBase = &DestBuffer[Dest.Y][Dest.X];
      for (int y = 0; y < Border.Y; ++y) {
        cpaletteindex *SrcPtr = &SrcBuffer[Src.Y+y][Src.X];
        cpaletteindex *EndPtr = SrcPtr+Border.X;
        paletteindex *DestPtr = DestBase+y;
        for (; SrcPtr != EndPtr; ++SrcPtr, DestPtr -= TrueDestXMove) *DestPtr = *SrcPtr;
      }
      break;
  }
}


//==========================================================================
//
//  rawbitmap::OutlineRect
//
//==========================================================================
void rawbitmap::OutlineRect (v2 pos, v2 size) {
  if (size.X >= 3 && size.Y >= 3) {
    //FIXME: optimise this!
    uint8_t *xmap = new uint8_t[Size.X * Size.Y];
    for (int y = 0; y != Size.Y; y += 1) {
      for (int x = 0; x != Size.X; x += 1) {
        uint8_t v;
        if (GetRGB16Pixel(v2(x, y)) != TRANSPARENT_COLOR) {
          v = 1;
        } else {
          v = 0;
        }
        xmap[y * Size.X + x] = v;
      }
    }

    for (int ty = 0; ty < size.Y; ty += 1) {
      for (int tx = 0; tx < size.X; tx += 1) {
        const int xx = pos.X + tx;
        const int yy = pos.Y + ty;
        if (xx >= 0 && yy >= 0 && xx < Size.X && yy < Size.Y &&
            xmap[yy * Size.X + xx] != 0)
        {
          for (int dy = -1; dy <= 1; dy += 1) {
            for (int dx = -1; dx <= 1; dx += 1) {
              if ((dx | dy) != 0 && (dx == 0 || dy == 0)) {
                const int sx = xx + dx;
                const int sy = yy + dy;
                if (sx >= 0 && sy >= 0 && sx < Size.X && sy < Size.Y &&
                    sx >= pos.X && sy >= pos.Y &&
                    sx < pos.X + size.X && sy < pos.Y + size.Y &&
                    xmap[sy * Size.X + sx] == 0)
                {
                  SetRawPixel(v2(sx, sy), 0);
                }
              }
            }
          }
        }
      }
    }

    delete [] xmap;
  }
}


//==========================================================================
//
//  rawbitmap::BuildOutlines
//
//==========================================================================
void rawbitmap::BuildOutlines () {
  uint8_t *xmap = new uint8_t[Size.X * Size.Y];
  for (int y = 0; y != Size.Y; y += 1) {
    for (int x = 0; x != Size.X; x += 1) {
      uint8_t v;
      if (GetRGB16Pixel(v2(x, y)) != TRANSPARENT_COLOR) {
        v = 1;
      } else {
        v = 0;
      }
      xmap[y * Size.X + x] = v;
    }
  }

  for (int y = 0; y != Size.Y / 16; y += 1) {
    for (int x = 0; x != Size.X / 16; x += 1) {
      for (int ty = 1; ty < 16; ty += 1) {
        for (int tx = 1; tx < 16; tx += 1) {
          int xx = x * 16 + tx;
          int yy = y * 16 + ty;
          if (xmap[yy * Size.X + xx] != 0) {
            for (int dy = -1; dy <= 1; dy += 1) {
              for (int dx = -1; dx <= 1; dx += 1) {
                if ((dx | dy) != 0 && (dx == 0 || dy == 0)) {
                  if (xmap[(yy + dy) * Size.X + (xx + dx)] == 0) {
                    SetRawPixel(v2(xx + dx, yy + dy), 0);
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  delete [] xmap;
}


//==========================================================================
//
//  rawbitmap::ChangeRawColor
//
//==========================================================================
void rawbitmap::ChangeRawColor (uChar from, uChar to) {
  for (int y = 0; y != Size.Y; y += 1) {
    for (int x = 0; x != Size.X; x += 1) {
      if (GetRawPixel(v2(x, y)) == from) {
        SetRawPixel(v2(x, y), to);
      }
    }
  }
}


//==========================================================================
//
//  rawbitmap::HFlipRect
//
//==========================================================================
void rawbitmap::HFlipRect (v2 pos, v2 size) {
  if (size.X > 0 && size.Y > 0) {
    uint8_t *xmap = new uint8_t[size.X];
    for (int y = 0; y < size.Y; y += 1) {
      if (y >= 0 && y < Size.Y) {
        for (int x = 0; x < size.X; x += 1) {
          xmap[x] = GetRawPixel(pos + v2(x, y));
        }
        for (int x = 0; x < size.X; x += 1) {
          SetRawPixel(pos + v2(size.X - x - 1, y), xmap[x]);
        }
      }
    }
    delete [] xmap;
  }
}


//==========================================================================
//
//  rawbitmap::VFlipRect
//
//==========================================================================
void rawbitmap::VFlipRect (v2 pos, v2 size) {
  if (size.X > 0 && size.Y > 0) {
    uint8_t *ymap = new uint8_t[size.X];
    for (int x = 0; x < size.X; x += 1) {
      if (x >= 0 && x < Size.X) {
        for (int y = 0; y < size.Y; y += 1) {
          ymap[y] = GetRawPixel(pos + v2(x, y));
        }
        for (int y = 0; y < size.Y; y += 1) {
          SetRawPixel(pos + v2(x, size.Y - y - 1), ymap[y]);
        }
      }
    }
    delete [] ymap;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// rawfont
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  rawfont::rawfont
//
//==========================================================================
rawfont::rawfont () {
  memset(widthInfo, 0, sizeof(widthInfo));
  memset(fontBitmap, 0, sizeof(fontBitmap));
  fontWidth = 8;
  fontHeight = 8;
}


//==========================================================================
//
//  rawfont::rawfont
//
//  font is `fw`x`fh`.
//  bitmap element for char is `bw`x`bh`.
//  `skip32` is set if first 32 chars are absent.
//  pixel at `(0, 0)` is transparent color.
//
//==========================================================================
rawfont::rawfont (cfestring &FileName, int spacewdt, int fw, int fh, int bw, int bh,
                  bool skip32)
{
  IvanAssert(spacewdt >= 0 && spacewdt <= 32);
  IvanAssert(fw > 0 && fw <= 32);
  IvanAssert(fh > 0 && fh <= 32);
  IvanAssert(bw > 0 && bw <= 64);
  IvanAssert(bh > 0 && bh <= 64);
  rawbitmap *bmp = new rawbitmap(FileName);
  IvanAssert(bmp);
  BuildFontInfo(bmp, spacewdt, fw, fh, bw, bh, skip32);
  delete bmp;
}


//==========================================================================
//
//  rawfont::~rawfont
//
//==========================================================================
rawfont::~rawfont () {
}


//==========================================================================
//
//  rawfont::BuildFontInfo
//
//==========================================================================
void rawfont::BuildFontInfo (rawbitmap *bmp, int spacewdt, int fw, int fh, int bw, int bh,
                             bool skip32) noexcept
{
  IvanAssert(bmp);
  IvanAssert(spacewdt >= 0 && spacewdt <= 32);
  IvanAssert(fw > 0 && fw <= 32);
  IvanAssert(fh > 0 && fh <= 32);
  IvanAssert(bw > 0 && bw <= 64);
  IvanAssert(bh > 0 && bh <= 64);

  memset(widthInfo, 0, sizeof(widthInfo));
  memset(fontBitmap, 0, sizeof(fontBitmap));

  fontWidth = (uint32_t)fw;
  fontHeight = (uint32_t)fh;

  const packcol16 transC = bmp->GetRGB16Pixel(0, 0);
  for (int cy = (skip32 ? 2 : 0); cy != 16; cy += 1) {
    for (int cx = 0; cx != 16; cx += 1) {
      const int rx0 = cx * bw;
      const int ry0 = (cy - (skip32 ? 2 : 0)) * bh;
      //const paletteindex *Buffer = &PaletteBuffer[ry0][rx0];
      int vfirst = 666, vlast = -666;
      for (int dx = 0; dx != fw; dx += 1) {
        bool empty = true;
        for (int dy = 0; empty && dy != fh; dy += 1) {
          empty = (bmp->GetRGB16Pixel(rx0 + dx, ry0 + dy) == transC);
        }
        if (!empty) {
          if (vfirst > dx) vfirst = dx;
          if (vlast < dx) vlast = dx;
        }
      }
      // calculated
      if (vlast > 0) {
        const int cwdt = (vlast - vfirst + 1);
        widthInfo[cy * 16 + cx] = (vfirst << 8) + (cwdt + 2);
        uint32_t *fdata = &fontBitmap[(cy * 16 + cx) * 32];
        for (int dy = 0; dy != fh; dy += 1) {
          uint32_t b = 0, mask = 0x01;
          for (int dx = 0; dx != fw; dx += 1) {
            if (bmp->GetRGB16Pixel(rx0 + dx, ry0 + dy) != transC) {
              b |= mask;
            }
            mask <<= 1;
          }
          *fdata = b; fdata += 1;
        }
      }
    }
  }

  // space is special
  widthInfo[32] = spacewdt;
  // spacing chars
  widthInfo[15] = 0;
  widthInfo[16] = 1;
  widthInfo[17] = 2;
  widthInfo[18] = 3;
  widthInfo[19] = 4;
  widthInfo[20] = 5;
  widthInfo[21] = 6;
  widthInfo[22] = 7;
  widthInfo[23] = 8;
  // nbsp
  widthInfo[24] = spacewdt;
  // nbsp, 1px
  widthInfo[25] = 1;
}


//==========================================================================
//
//  rawfont::CalcTextWidth
//
//==========================================================================
int rawfont::CalcTextWidth (const void *dataptr, int length, bool skipCtl) const noexcept {
  if (!dataptr || length <= 0) return 0;
  const char *data = (const char *)dataptr;
  int res = 0;
  if (skipCtl) {
    RawTextData raw(dataptr, length);
    while (!raw.IsEmpty()) {
      const char ch = raw.SkipChar();
      if (ch != 0) {
        res += CharWidth(ch);
      }
    }
  } else {
    while (length != 0) {
      res += CharWidth(*data);
      data += 1; length -= 1;
    }
  }
  return res;
}


//==========================================================================
//
//  rawfont::CalcTextWidth
//
//==========================================================================
int rawfont::CalcTextWidth (cfestring &festr, bool skipCtl) const noexcept {
  return (festr.IsEmpty() ? 0 : CalcTextWidth(festr.getConstDataBuffer(), festr.Length(), skipCtl));
}


//==========================================================================
//
//  rawfont::RPadToPixWidth
//
//==========================================================================
void rawfont::RPadToPixWidth (festring &str, int width, bool nbsp1px) const noexcept {
  int wdt = CalcTextWidth(str, true);
  if (wdt < width) {
    if (nbsp1px) {
      while (wdt < width) {
        str.AppendChar(NBSP1PX);
        wdt += 1;
      }
    } else {
      while (wdt < width) {
        int left = width - wdt;
        if (left > 8) left = 8;
        str.AppendChar(15 + left);
        wdt += left;
      }
    }
  }
}


//==========================================================================
//
//  rawfont::LPadToPixWidth
//
//==========================================================================
void rawfont::LPadToPixWidth (festring &str, int width, bool nbsp1px) const noexcept {
  int wdt = CalcTextWidth(str, true);
  if (wdt < width) {
    festring pad;
    if (nbsp1px) {
      while (wdt < width) {
        pad.AppendChar(NBSP1PX);
        wdt += 1;
      }
    } else {
      while (wdt < width) {
        int left = width - wdt;
        if (left > 8) left = 8;
        pad.AppendChar(15 + left);
        wdt += left;
      }
    }
    pad << str;
    str = pad;
  }
}


//==========================================================================
//
//  rawfont::PadCenter
//
//==========================================================================
void rawfont::PadCenter (festring &str, int width, bool nbsp1px) const noexcept {
  festring res, rp;
  const int sw = TextWidth(str);
  if (sw < width) {
    int ww = 0, pad;
    while (width - sw - ww >= 2) {
      if (nbsp1px) {
        res.AppendChar(NBSP1PX);
        rp.AppendChar(NBSP1PX);
      } else {
        pad = Min(8, (width - sw - ww) / 2);
        res.AppendChar(15 + pad);
        rp.AppendChar(15 + pad);
        ww += pad * 2;
      }
    }
    pad = width - sw - ww;
    IvanAssert(pad == 0 || pad == 1);
    if (pad) {
      if (nbsp1px) {
        res.AppendChar(NBSP1PX);
      } else {
        res.AppendChar(16);
      }
    }
  }
  res << str << rp;
  str = res;
}


//==========================================================================
//
//  rawfont::BlitCharProp
//
//==========================================================================
void rawfont::BlitCharProp (bitmap *Bitmap, char ch, int x, int y, packcol16 Color,
                            bool gradient) const noexcept
{
  const int cwdt = CharWidth(ch) - 2; // ignore last two empty pixels
  if (cwdt < 1) return;
  x += 1;
  const int BitmapXSize = Bitmap->GetSize().X;
  const int BitmapYSize = Bitmap->GetSize().Y;
  if (x < 0 || y < 0 || x + cwdt - 1 >= BitmapXSize || y + (int)fontHeight >= BitmapYSize) {
    return;
  }
  const uint8_t shift = CharOfs(ch);
  const uint32_t *fdata = &fontBitmap[(uint8_t)ch * 32];
  packcol16 *DestBuffer = &Bitmap->GetImage()[y][x];
  for (int y = 0; y != (int)fontHeight; y += 1) {
    uint32_t b = *fdata; fdata += 1;
    b >>= shift;
    const int clr = (gradient ? MkGradientColor(Color, y, 7) : Color);
    for (int x = 0; x != cwdt; x += 1) {
      if (b & 1) DestBuffer[x] = clr;
      b >>= 1;
    }
    DestBuffer += BitmapXSize;
  }
}


//==========================================================================
//
//  rawfont::PrintStr
//
//==========================================================================
void rawfont::PrintStr (bitmap *Bitmap, int x, int y, packcol16 Color, cfestring &str,
                        bool gradient, bool shade) const noexcept
{
  if (str.IsEmpty()) return;
  RawTextData raw(str);
  raw.ClearColorInfo(Color);
  packcol16 clrshade = MakeShadeColor(raw.curColor);
  packcol16 oldcolor = raw.curColor;
  while (!raw.IsEmpty()) {
    char ch = raw.SkipChar();
    if (ch != 0) {
      if (shade) {
        if (oldcolor != raw.curColor) {
          oldcolor = raw.curColor;
          clrshade = MakeShadeColor(raw.curColor);
        }
        BlitCharProp(Bitmap, ch, x + 1, y + 1, clrshade, false);
      }
      BlitCharProp(Bitmap, ch, x, y, raw.curColor, gradient);
      x += CharWidth(ch);
    }
  }
}


//==========================================================================
//
//  rawfont::PrintStrBlack
//
//==========================================================================
void rawfont::PrintStrFlat (bitmap *Bitmap, int x, int y, packcol16 Color,
                            cfestring &str) const noexcept
{
  if (str.IsEmpty()) return;
  RawTextData raw(str);
  raw.ClearColorInfo(Color);
  while (!raw.IsEmpty()) {
    char ch = raw.SkipChar();
    if (ch != 0) {
      BlitCharProp(Bitmap, ch, x, y, Color, false);
      x += CharWidth(ch);
    }
  }
}


/*
//==========================================================================
//
//  rawfont::PrintStrMono
//
//==========================================================================
void rawfont::PrintStrMono (bitmap *Bitmap, int x, int y, packcol16 Color, cfestring &str,
                            bool gradient, bool shade) const noexcept
{
  if (str.IsEmpty()) return;
  const packcol16 clrshade = MakeShadeColor(Color);
  RawTextData raw(str);
  raw.ClearColorInfo(Color);
  while (!raw.IsEmpty()) {
    char ch = raw.SkipChar();
    if (ch != 0) {
      int cx = CharOfs(ch) - 1;
      if (shade) BlitCharProp(Bitmap, ch, x + cx + 1, y + 1, clrshade, false);
      BlitCharProp(Bitmap, ch, x + cx, y, raw.curColor, gradient);
      x += (int)fontWidth;
    }
  }
}
*/


struct XPBuf {
public:
  char buffer[2048];
  char *bufptr;

public:
  inline XPBuf () : bufptr(buffer) {}
  inline ~XPBuf () { Reset(); }
  XPBuf (const XPBuf &) = delete;
  XPBuf &operator = (const XPBuf &) = delete;
  inline void Reset () { if (bufptr != buffer) ::free(bufptr); bufptr = buffer; }
};


//==========================================================================
//
//  FormatXPBuf
//
//==========================================================================
static void FormatXPBuf (XPBuf &buf, cchar *fmt, va_list vap) {
  buf.Reset();
  int bufsz = (int)sizeof(buf.buffer) - 1;
  for (;;) {
    va_list ap;
    int n;
    char *np;
    va_copy(ap, vap);
    n = vsnprintf(buf.bufptr, bufsz, fmt, ap);
    va_end(ap);
    if (n > -1 && n < bufsz) break;
    if (n < -1) n = bufsz + 4096;
    if (buf.bufptr == buf.buffer) {
      np = (char *)::malloc(n + 1);
    } else {
      np = (char *)::realloc((buf.bufptr == buf.buffer ? NULL : buf.bufptr), n + 1);
    }
    if (np == NULL) return; //FIXME
    buf.bufptr = np;
  }
}


//==========================================================================
//
//  rawfont::Printf
//
//==========================================================================
void rawfont::Printf (bitmap *Bitmap, v2 Pos, packcol16 Color, cchar *Format, ...) const noexcept {
  XPBuf buf;
  va_list ap;
  va_start(ap, Format);
  FormatXPBuf(buf, Format, ap);
  va_end(ap);
  PrintStr(Bitmap, Pos.X, Pos.Y, Color, CONST_S(buf.bufptr));
}


//==========================================================================
//
//  rawfont::PrintfOutline
//
//==========================================================================
void rawfont::PrintfOutline (bitmap *Bitmap, v2 Pos, packcol16 Color, cchar *Format, ...) const noexcept {
  XPBuf buf;
  va_list ap;
  va_start(ap, Format);
  FormatXPBuf(buf, Format, ap);
  va_end(ap);
  PrintStrOutline(Bitmap, Pos, Color, CONST_S(buf.bufptr));
}


//==========================================================================
//
//  rawfont::WordWrap
//
//  leftpad is in spaces, not in pixels!
//  doesn't clear `resver`.
//
//==========================================================================
int rawfont::WordWrap (cfestring &str, std::vector<festring> &resver,
                       int maxWidth, int leftPadNonFirst) const noexcept
{
  if (str.IsEmpty()) return 0;
  //if (leftPadNonFirst < 0) leftPadNonFirst = 0;

  if (maxWidth <= 0) {
    resver.push_back(festring(str));
    return 1;
  }

  const auto lstartcnt = resver.size();

  if (TextWidth(str) <= maxWidth && !memchr(str.getConstDataBuffer(), '\n', str.GetSize())) {
    resver.push_back(festring(str));
  } else {
    WrapDataFont wrapper(this, leftPadNonFirst, maxWidth, resver);
    wrapper.ProcessStr(str);
    wrapper.Finish();

    if (lstartcnt == resver.size()) {
      // force one empty string
      resver.push_back(festring());
    }
  }

  return (int)(ptrdiff_t)(resver.size() - lstartcnt);
}


//==========================================================================
//
//  rawfont::PrintStrOutline
//
//==========================================================================
void rawfont::PrintStrOutline (bitmap *Bitmap, const v2 Pos, packcol16 Color, cfestring &str) const {
  for (int dy = -1; dy <= 1; dy += 1) {
    for (int dx = -1; dx <= 1; dx += 1) {
      if ((dx | dy) != 0) {
        PrintStrFlat(Bitmap, Pos.X + dx, Pos.Y + dy, 0, str);
      }
    }
  }
  PrintStr(Bitmap, Pos, Color, str, true, false);
}
