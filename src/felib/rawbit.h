/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_COLORBIT_H__
#define __FELIB_COLORBIT_H__

#include "felibdef.h"
#include <stdio.h>

#include <cstdarg>
#include <map>
#include <vector>

#include "vwfileio.h"
#include "femath.h"
#include "v2.h"

class outputfile;
class inputfile;
class bitmap;
class festring;


// ////////////////////////////////////////////////////////////////////////// //
// used for copypasta in IGOR
class RawBmpLump {
protected:
  struct RawBmpLumpData {
    uint32_t refc;
    int wdt, hgt;
    void *data;
  };

protected:
  RawBmpLumpData *data;

  FORCE_INLINE void IncRef () const { if (data) data->refc += 1; }

  FORCE_INLINE void DecRef () {
    if (data) {
      data->refc -= 1;
      if (data->refc == 0) {
        delete data;
      }
      data = 0;
    }
  }

public:
  FORCE_INLINE RawBmpLump () : data(0) {}
  FORCE_INLINE RawBmpLump (const RawBmpLump &src) : data(src.data) { IncRef(); }

  FORCE_INLINE RawBmpLump (int wdt, int hgt) : data(0) {
    if (wdt > 0 && hgt > 0) {
      data = (RawBmpLumpData *)calloc(1, sizeof(RawBmpLumpData));
      data->refc = 1; data->wdt = wdt; data->hgt = hgt;
      data->data = calloc(1, wdt * hgt);
    }
  }

  FORCE_INLINE RawBmpLump &operator = (const RawBmpLump &src) { src.IncRef(); DecRef(); data = src.data; return *this; }

  FORCE_INLINE ~RawBmpLump () { DecRef(); }

  FORCE_INLINE void Clear () { DecRef(); }

  FORCE_INLINE void Create (int wdt, int hgt) {
    Clear();
    if (wdt > 0 && hgt > 0) {
      data = (RawBmpLumpData *)calloc(1, sizeof(RawBmpLumpData));
      data->refc = 1; data->wdt = wdt; data->hgt = hgt;
      data->data = calloc(1, wdt * hgt);
    }
  }

  FORCE_INLINE int GetWidth () const { return (data ? data->wdt : 0); }
  FORCE_INLINE int GetHeight () const { return (data ? data->hgt : 0); }

  FORCE_INLINE uint8_t *GetBuffer () const { return (data ? (uint8_t *)data->data : 0); }
};


// ////////////////////////////////////////////////////////////////////////// //
// actually this is simple 256-color with palette PCX codec
// this is also used as 256x256 font bitmap
class rawbitmap {
  friend class rawfont;
protected:
  v2 Size;
  uChar *Palette;
  paletteindex **PaletteBuffer;

public:
  rawbitmap ();
  rawbitmap (cfestring &FileName);
  rawbitmap (v2);
  ~rawbitmap ();

  explicit rawbitmap (const rawbitmap &src, v2 pos, v2 size);

  FORCE_INLINE v2 GetSize () const { return Size; }
  FORCE_INLINE int GetSizeX () const { return Size.X; }
  FORCE_INLINE int GetSizeY () const { return Size.Y; }

  void SavePNG (cfestring &FileName) const;

  void MaskedBlit (bitmap *Bitmap, v2 Src, v2 Dest, v2 Border, packcol16 *Color) const;
  void MaskedBlit (bitmap *Bitmap, packcol16 *Color) const;

  bitmap *Colorize (v2 Pos, v2 Border, v2 Move, cpackcol16 *Color, alpha BaseAlpha=255,
                    cpackalpha *Alpha=0, cuchar *RustData=0, truth AllowReguralColors=true,
                    truth SimpleOutline=false) const;

  void AlterGradient (v2 Pos, v2 Border, int MColor, int Amount, truth Clip);
  void SwapColors (v2 Pos, v2 Border, int Color1, int Color2);
  void Roll (v2 Pos, v2 Border, v2 Move, paletteindex *TempBuffer);

  static truth IsMaterialColor (int Color) { return Color >= 192; }
  static int GetMaterialColorIndex (int Color) { return (Color-192) >> 4; }

  int GetMaterialColorIndex (int X, int Y) const {
    return (X >= 0 && Y >= 0 && X < Size.X && Y < Size.Y
            ? ((PaletteBuffer[Y][X]-192) >> 4) : 0/*dunno*/);
  }

  FORCE_INLINE truth IsTransparent (v2 Pos) const {
    return (Pos.X >= 0 && Pos.Y >= 0 && Pos.X < Size.X && Pos.Y < Size.Y
            ? (PaletteBuffer[Pos.Y][Pos.X] == TRANSPARENT_PALETTE_INDEX) : true);
  }

  FORCE_INLINE truth IsMaterialColor1 (v2 Pos) const {
    if (Pos.X >= 0 && Pos.Y >= 0 && Pos.X < Size.X && Pos.Y < Size.Y) {
      const int P = PaletteBuffer[Pos.Y][Pos.X];
      return (P >= 192 && P < 208);
    } else {
      return false;
    }
  }

  v2 RandomizeSparklePos (PRNG &prng, cv2 *ValidityArray, v2 *PossibleBuffer,
                          v2 Pos, v2 Border, int ValidityArraySize, int SparkleFlags) const;
  void CopyPaletteFrom (rawbitmap *Bitmap);

  FORCE_INLINE void PutPixel (int X, int Y, paletteindex Color) {
    if (X >= 0 && Y >= 0 && X < Size.X && Y < Size.Y) {
      PaletteBuffer[Y][X] = Color;
    }
  }
  FORCE_INLINE void PutPixel (v2 Pos, paletteindex Color) { PutPixel(Pos.X, Pos.Y, Color); }

  paletteindex GetPixel (int X, int Y) const {
    return (X >= 0 && Y >= 0 && X < Size.X && Y < Size.Y
            ? PaletteBuffer[Y][X] : TRANSPARENT_PALETTE_INDEX);
  }
  FORCE_INLINE paletteindex GetPixel (v2 Pos) const { return GetPixel(Pos.X, Pos.Y); }

  void Clear ();

  void NormalBlit (rawbitmap *Bitmap, v2 Src, v2 Dest, v2 Border, int Flags=0) const;

  const uChar *GetPalette () const { return Palette; }
  const uChar *GetBuffer () const { return PaletteBuffer[0]; }

  packcol16 GetPalColor16 (int idx) const {
    if (idx >= 0 && idx <= 255) {
      return MakeRGB16(Palette[idx * 3 + 0],
                       Palette[idx * 3 + 1],
                       Palette[idx * 3 + 2]);
    } else {
      return TRANSPARENT_COLOR;
    }
  }

  uChar GetPalColorRed8 (int idx) const { return (idx >= 0 && idx <= 255 ? Palette[idx * 3 + 0] : 0); }
  uChar GetPalColorGreen8 (int idx) const { return (idx >= 0 && idx <= 255 ? Palette[idx * 3 + 1] : 0); }
  uChar GetPalColorBlue8 (int idx) const { return (idx >= 0 && idx <= 255 ? Palette[idx * 3 + 2] : 0); }

  RawBmpLump Copy (v2 src, v2 size) const;
  void Paste (const RawBmpLump &bmp, v2 dest);

  FORCE_INLINE uChar GetRawPixel (v2 pos) const {
    return (pos.X >= 0 && pos.Y >= 0 && pos.X < Size.X && pos.Y < Size.Y
            ? PaletteBuffer[pos.Y][pos.X] : TRANSPARENT_PALETTE_INDEX);
  }
  FORCE_INLINE uChar GetRawPixel (int x, int y) const { return GetRawPixel(v2(x, y)); }

  FORCE_INLINE void SetRawPixel (v2 pos, uChar clr) const {
    if (pos.X >= 0 && pos.Y >= 0 && pos.X < Size.X && pos.Y < Size.Y) {
      PaletteBuffer[pos.Y][pos.X] = clr;
    }
  }
  FORCE_INLINE void SetRawPixel (int x, int y, uChar clr) const { SetRawPixel(v2(x, y), clr); }

  FORCE_INLINE packcol16 GetRGB16Pixel (v2 pos) const {
    if (pos.X >= 0 && pos.Y >= 0 && pos.X < Size.X && pos.Y < Size.Y) {
      uint32_t idx = PaletteBuffer[pos.Y][pos.X] * 3;
      return MakeRGB16(Palette[idx + 0], Palette[idx + 1], Palette[idx + 2]);
    } else {
      return TRANSPARENT_COLOR;
    }
  }
  FORCE_INLINE packcol16 GetRGB16Pixel (int x, int y) const { return GetRGB16Pixel(v2(x, y)); }

  void TranslateToPaletteOf (rawbitmap *srcbmp);

  void OutlineRect (v2 pos, v2 size);
  void HFlipRect (v2 pos, v2 size);
  void VFlipRect (v2 pos, v2 size);

  void BuildOutlines ();

  void ChangeRawColor (uChar from, uChar to);

protected:
  void loadFromFile (VFile fl);
  void loadPNG (VFile fl);

  static festring curfile;
};


// ////////////////////////////////////////////////////////////////////////// //
// 1-bit font, up to 32x32
// special chars: [15..23] -- blanks from 1 to 8 pixels.
// #24: non-breaking space

class rawfont {
  friend class rawbitmap;
private:
  // for fonts, we will calculate widthes for proportional printing.
  // bits 0-7 is width, bits 8-15 is start offset.
  // width includes blank pixels.
  uint32_t widthInfo[256];
  uint32_t fontBitmap[256*32];
  uint32_t fontWidth;
  uint32_t fontHeight;

private:
  void BuildFontInfo (rawbitmap *bmp, int spacewdt, int fw, int fh, int bw, int bh, bool skip32) noexcept;

  int CalcTextWidth (const void *data, int length, bool skipCtl=true) const noexcept;
  int CalcTextWidth (cfestring &festr, bool skipCtl=true) const noexcept;

  void BlitCharProp (bitmap *Bitmap, char ch, int x, int y, packcol16 Color, bool gradient) const noexcept;

public:
  rawfont ();
  // `spacewdt` is width of space.
  // font is `fw`x`fh`.
  // bitmap element for char is `bw`x`bh`.
  // `skip32` is set if first 32 chars are absent.
  // pixel at `(0, 0)` is transparent color.
  rawfont (cfestring &FileName, int spacewdt, int fw, int fh, int bw, int bh, bool skip32);
  ~rawfont ();

  // used for console rendering
  FORCE_INLINE const uint32_t *GetCharBitmap (char ch) const noexcept {
    return &fontBitmap[(uint8_t)ch * 32];
  }

  FORCE_INLINE int CharOfs (char ch) const noexcept {
    return ((widthInfo[(uint8_t)ch] >> 8) & 0xffU);
  }

  FORCE_INLINE int CharWidth (char ch) const noexcept {
    return (widthInfo[(uint8_t)ch] & 0xffU);
  }

  FORCE_INLINE int FontWidth () const noexcept { return (int)fontWidth; }
  FORCE_INLINE int FontHeight () const noexcept { return (int)fontHeight; }

  // includes the last empty pixel, don't ignore control chars
  FORCE_INLINE int RawTextWidth (const char *str) const noexcept {
    if (!str || !str[0]) return 0;
    return CalcTextWidth(str, (int)strlen(str), false);
  }

  FORCE_INLINE int RawTextWidth (cfestring &str) const noexcept { return CalcTextWidth(str, false); }

  FORCE_INLINE int TextWidth (const char *str) const noexcept {
    if (!str || !str[0]) return 0;
    return CalcTextWidth(str, (int)strlen(str), true);
  }

  FORCE_INLINE int TextWidth (cfestring &str) const noexcept { return CalcTextWidth(str, true); }

  // leftpad is in spaces, not in pixels!
  // doesn't clear `resver`.
  // return number of added lines.
  int WordWrap (cfestring &text, std::vector<festring> &resver,
                int maxWidth, int leftPadNonFirst=0) const noexcept;

  void RPadToPixWidth (festring &str, int width, bool nbsp1px=false) const noexcept;
  void LPadToPixWidth (festring &str, int width, bool nbsp1px=false) const noexcept;
  void PadCenter (festring &str, int width, bool nbsp1px=false) const noexcept;

  // no color codes, no shading
  void PrintStrFlat (bitmap *Bitmap, int x, int y, packcol16 Color, cfestring &str) const noexcept;

  void PrintStr (bitmap *Bitmap, int x, int y, packcol16 Color, cfestring &str,
                 bool gradient=true, bool shade=true) const noexcept;

  FORCE_INLINE void PrintStr (bitmap *Bitmap, const v2 Pos, packcol16 Color, cfestring &str,
                              bool gradient=true, bool shade=true) const noexcept
  {
    PrintStr(Bitmap, Pos.X, Pos.Y, Color, str, gradient, shade);
  }

  void LIKE_PRINTF(5, 6) Printf (bitmap *Bitmap, v2 Pos, packcol16 Color, cchar *Format, ...) const noexcept;
  void LIKE_PRINTF(5, 6) PrintfOutline (bitmap *Bitmap, v2 Pos, packcol16 Color, cchar *Format, ...) const noexcept;

  void PrintStrOutline (bitmap *Bitmap, const v2 Pos, packcol16 Color, cfestring &str) const;
};


#endif
