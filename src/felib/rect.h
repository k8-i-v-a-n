/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_RECT_H__
#define __FELIB_RECT_H__

#include "felibdef.h"
#include "v2.h"


struct rect {
public:
  int X1, Y1, X2, Y2;

public:
  inline rect () {}
  inline rect (int X1, int Y1, int X2, int Y2) : X1(X1), Y1(Y1), X2(X2), Y2(Y2) {}
  inline rect (v2 V1, v2 V2) : X1(V1.X), Y1(V1.Y), X2(V2.X), Y2(V2.Y) {}

  inline rect operator + (const v2 &V) const { return rect(X1+V.X, Y1+V.Y, X2+V.X, Y2+V.Y); }

  inline bool isValid () const { return (X1 <= X2 && Y1 <= Y2); }

  inline int getWidth () const { return (X1 <= X2 ? X2-X1+1 : 0); }
  inline int getHeight () const { return (Y1 <= Y2 ? Y2-Y1+1 : 0); }

  // intersect rects, modify this one, and return it
  inline rect &intersect (const rect &r) {
    if (isValid() && r.isValid()) {
      if (X1 < r.X1) X1 = r.X1;
      if (Y1 < r.Y1) Y1 = r.Y1;
      if (X2 > r.X2) X2 = r.X2;
      if (Y2 > r.Y2) Y2 = r.Y2;
    }
    return *this;
  }
};


#endif
