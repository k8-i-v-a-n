/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include "sq3support.h"
#include "feerror.h"


//#define SQA_DEBUG_ERROR_ABORT
//#define SQA_DEBUG_TRANSACTIONS
//#define SQA_DEBUG_WRITE
//#define SQA_DEBUG_READ
//#define SQA_DEBUG_READ_CHUNKS

#define LZFF_CHUNK_SIZE   (65536)
#define LZFF_PKF_SIZE     (2)


// ////////////////////////////////////////////////////////////////////////// //
// LZFF3
// ////////////////////////////////////////////////////////////////////////// //
typedef int intbool_t;

enum {
  int_false = 0,
  int_true = 1
};

#define LZFF3_ERR_ARGS          (-1)
#define LZFF3_ERR_MEM           (-2)
#define LZFF3_ERR_FILE_TOO_BIG  (-3)


#define LZFF3_malloc(nothing_,size_)  ::malloc(size_)
#define LZFF3_mfree(nothing_,ptr_)    ::free(ptr_)


// ////////////////////////////////////////////////////////////////////////// //
#define crc32_init  (0xffffffffU)

static const uint32_t crctab[16] = {
  0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
  0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
  0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
  0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c,
};


#define CRC32BYTE(bt_)  do { \
  crc32 ^= (uint8_t)(bt_); \
  crc32 = crctab[crc32&0x0f]^(crc32>>4); \
  crc32 = crctab[crc32&0x0f]^(crc32>>4); \
} while (0)


static FORCE_INLINE uint32_t crc32_part (uint32_t crc32, const void *src, uint32_t len) {
  const uint8_t *buf = (const uint8_t *)src;
  while (len--) {
    CRC32BYTE(*buf++);
  }
  return crc32;
}

static FORCE_INLINE uint32_t crc32_final (uint32_t crc32) {
  return crc32^0xffffffffU;
}

static FORCE_INLINE uint32_t crc32_buf (const void *src, uint32_t len) {
  return crc32_final(crc32_part(crc32_init, src, len));
}


// ////////////////////////////////////////////////////////////////////////// //
typedef struct {
  uint32_t x1, x2;
  uint8_t *dest;
  int dpos, dend;
} EntEncoder;

typedef struct {
  uint32_t x1, x2, x;
  const uint8_t *src;
  int spos, send;
} EntDecoder;

typedef struct {
  uint16_t p1, p2;
} Predictor;


typedef struct {
  Predictor pred[2];
  int ctx;
} BitPPM;

typedef struct {
  Predictor predBits[2 * 256];
  int ctxBits;
} BytePPM;

typedef struct {
  // 8 bits, twice
  BytePPM bytes[2];
  // "second byte" flag
  BitPPM moreFlag;
} WordPPM;


// ////////////////////////////////////////////////////////////////////////// //
static void EncInit (EntEncoder *ee, void *outbuf, uint32_t obsize) {
  ee->x1 = 0; ee->x2 = 0xFFFFFFFFU;
  ee->dest = (uint8_t *)outbuf; ee->dpos = 0;
  ee->dend = obsize;
}

static FORCE_INLINE void EncEncode (EntEncoder *ee, int p, intbool_t bit) {
  uint32_t xmid = ee->x1 + (uint32_t)(((uint64_t)((ee->x2 - ee->x1) & 0xffffffffU) * (uint32_t)p) >> 17);
  if (bit) ee->x2 = xmid; else ee->x1 = xmid + 1;
  while ((ee->x1 ^ ee->x2) < (1u << 24)) {
    if (ee->dpos < ee->dend) {
      ee->dest[ee->dpos++] = (uint8_t)(ee->x2 >> 24);
    } else {
      ee->dpos = 0x7fffffff - 8;
    }
    ee->x1 <<= 8;
    ee->x2 = (ee->x2 << 8) + 255;
  }
}

static void EncFlush (EntEncoder *ee) {
  if (ee->dpos + 4 <= ee->dend) {
    ee->dest[ee->dpos++] = (uint8_t)(ee->x2 >> 24); ee->x2 <<= 8;
    ee->dest[ee->dpos++] = (uint8_t)(ee->x2 >> 24); ee->x2 <<= 8;
    ee->dest[ee->dpos++] = (uint8_t)(ee->x2 >> 24); ee->x2 <<= 8;
    ee->dest[ee->dpos++] = (uint8_t)(ee->x2 >> 24); ee->x2 <<= 8;
  } else {
    ee->dpos = 0x7fffffff - 8;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
static OK_UNUSED FORCE_INLINE uint8_t DecGetByte (EntDecoder *ee) {
  if (ee->spos < ee->send) {
    return ee->src[ee->spos++];
  } else {
    ee->spos = 0x7fffffff;
    return 0;
  }
}

static OK_UNUSED void DecInit (EntDecoder *ee, const void *inbuf, uint32_t insize) {
  ee->x1 = 0; ee->x2 = 0xFFFFFFFFU;
  ee->src = (const uint8_t *)inbuf; ee->spos = 0; ee->send = insize;
  ee->x = DecGetByte(ee);
  ee->x = (ee->x << 8) + DecGetByte(ee);
  ee->x = (ee->x << 8) + DecGetByte(ee);
  ee->x = (ee->x << 8) + DecGetByte(ee);
}

static OK_UNUSED FORCE_INLINE intbool_t DecDecode (EntDecoder *ee, int p) {
  uint32_t xmid = ee->x1 + (uint32_t)(((uint64_t)((ee->x2 - ee->x1) & 0xffffffffU) * (uint32_t)p) >> 17);
  intbool_t bit = (ee->x <= xmid);
  if (bit) ee->x2 = xmid; else ee->x1 = xmid + 1;
  while ((ee->x1 ^ ee->x2) < (1u << 24)) {
    ee->x1 <<= 8;
    ee->x2 = (ee->x2 << 8) + 255;
    ee->x = (ee->x << 8) + DecGetByte(ee);
  }
  return bit;
}


// ////////////////////////////////////////////////////////////////////////// //
static void PredInit (Predictor *pp) {
  pp->p1 = 1 << 15; pp->p2 = 1 << 15;
}

static FORCE_INLINE int PredGetP (Predictor *pp) {
  return (int)((uint32_t)pp->p1 + (uint32_t)pp->p2);
}

static FORCE_INLINE void PredUpdate (Predictor *pp, intbool_t bit) {
  if (bit) {
    pp->p1 += ((~((uint32_t)pp->p1)) >> 3) & 0b0001111111111111;
    pp->p2 += ((~((uint32_t)pp->p2)) >> 6) & 0b0000001111111111;
  } else {
    pp->p1 -= ((uint32_t)pp->p1) >> 3;
    pp->p2 -= ((uint32_t)pp->p2) >> 6;
  }
}

static FORCE_INLINE int PredGetPAndUpdate (Predictor *pp, intbool_t bit) {
  int p = PredGetP(pp);
  PredUpdate(pp, bit);
  return p;
}

static OK_UNUSED FORCE_INLINE intbool_t PredGetBitDecodeUpdate (Predictor *pp, EntDecoder *dec) {
  int p = PredGetP(pp);
  intbool_t bit = DecDecode(dec, p);
  PredUpdate(pp, bit);
  return bit;
}


// ////////////////////////////////////////////////////////////////////////// //
static void BitPPMInit (BitPPM *ppm, int initstate) {
  for (uint32_t f = 0; f < (uint32_t)sizeof(ppm->pred) / (uint32_t)sizeof(ppm->pred[0]); ++f) {
    PredInit(&ppm->pred[f]);
  }
  ppm->ctx = !!initstate;
}

static FORCE_INLINE void BitPPMEncode (BitPPM *ppm, EntEncoder *enc, intbool_t bit) {
  int p = ppm->ctx;
  if (bit) ppm->ctx = 1; else ppm->ctx = 0;
  p = PredGetPAndUpdate(&ppm->pred[p], bit);
  EncEncode(enc, p, bit);
}

static OK_UNUSED FORCE_INLINE intbool_t BitPPMDecode (BitPPM *ppm, EntDecoder *dec) {
  intbool_t bit = PredGetBitDecodeUpdate(&ppm->pred[ppm->ctx], dec);
  if (bit) ppm->ctx = 1; else ppm->ctx = 0;
  return bit;
}


// ////////////////////////////////////////////////////////////////////////// //
static void BytePPMInit (BytePPM *ppm) {
  for (uint32_t f = 0; f < (uint32_t)sizeof(ppm->predBits) / (uint32_t)sizeof(ppm->predBits[0]); ++f) {
    PredInit(&ppm->predBits[f]);
  }
  ppm->ctxBits = 0;
}

static FORCE_INLINE void BytePPMEncodeByte (BytePPM *ppm, EntEncoder *enc, int bt) {
  int c2 = 1;
  int cofs = ppm->ctxBits * 256;
  ppm->ctxBits = (bt >= 0x80);
  for (int f = 0; f <= 7; ++f) {
    intbool_t bit = (bt&0x80) != 0; bt <<= 1;
    int p = PredGetPAndUpdate(&ppm->predBits[cofs + c2], bit);
    EncEncode(enc, p, bit);
    c2 += c2; if (bit) ++c2;
  }
}

static OK_UNUSED FORCE_INLINE uint8_t BytePPMDecodeByte (BytePPM *ppm, EntDecoder *dec) {
  uint32_t n = 1;
  int cofs = ppm->ctxBits * 256;
  do {
    intbool_t bit = PredGetBitDecodeUpdate(&ppm->predBits[cofs + n], dec);
    n += n; if (bit) ++n;
  } while (n < 0x100);
  n -= 0x100;
  ppm->ctxBits = (n >= 0x80);
  return (uint8_t)n;
}


// ////////////////////////////////////////////////////////////////////////// //
static void WordPPMInit (WordPPM *ppm) {
  BytePPMInit(&ppm->bytes[0]); BytePPMInit(&ppm->bytes[1]);
  BitPPMInit(&ppm->moreFlag, 0);
}

static FORCE_INLINE void WordPPMEncodeInt (WordPPM *ppm, EntEncoder *enc, int n) {
  BytePPMEncodeByte(&ppm->bytes[0], enc, n&0xff);
  if (n >= 0x100) {
    BitPPMEncode(&ppm->moreFlag, enc, 1);
    BytePPMEncodeByte(&ppm->bytes[1], enc, (n>>8)&0xff);
  } else {
    BitPPMEncode(&ppm->moreFlag, enc, 0);
  }
}


static OK_UNUSED FORCE_INLINE int WordPPMDecodeInt (WordPPM *ppm, EntDecoder *dec) {
  int n = BytePPMDecodeByte(&ppm->bytes[0], dec);
  if (BitPPMDecode(&ppm->moreFlag, dec)) {
    n += BytePPMDecodeByte(&ppm->bytes[1], dec) * 0x100;
  }
  return n;
}


// ////////////////////////////////////////////////////////////////////////// //
//# define LZFF3_HASH_SIZE    (1021u)
#define LZFF3_HASH_SIZE     (2039u)
//# define LZFF3_HASH_SIZE    (4093u)
#define LZFF3_NUM_LIMIT     (0x10000)
#define LZFF3_OFS_LIMIT     LZFF3_NUM_LIMIT
#define LZFF3_NUM_BUCKETS   (LZFF3_OFS_LIMIT * 2)

typedef struct {
  uint32_t sptr;
  uint32_t bytes4;
  uint32_t prev;
} LZFFHashEntry;


//==========================================================================
//
//  CompressLZFF3
//
//  return size of compressed data, or negative error code
//
//==========================================================================
static OK_UNUSED int CompressLZFF3 (const void *source, int srclen,
                                    void *dest, int dstlen,
                                    intbool_t allow_lazy)
{
  uint32_t bestofs, bestlen, he, b4, lp, wr;
  uint32_t mmax, mlen, cpp, pos;
  uint32_t mtbestofs, mtbestlen;
  uint32_t ssizemax;
  uint32_t srcsize;
  uint32_t litcount, litpos, spos;
  uint32_t hfree, heidx, ntidx;
  uint32_t dict[LZFF3_HASH_SIZE];
  const uint8_t *src;
  EntEncoder enc;
  BytePPM ppmData;
  WordPPM ppmMtOfs, ppmMtLen, ppmLitLen;
  BitPPM ppmLitFlag;
  LZFFHashEntry *htbl;

  #define FlushLit()  do { \
    lp = litpos; \
    while (litcount != 0) { \
      BitPPMEncode(&ppmLitFlag, &enc, int_true); \
      wr = litcount; if (wr > LZFF3_NUM_LIMIT) wr = LZFF3_NUM_LIMIT; \
      litcount -= wr; \
      WordPPMEncodeInt(&ppmLitLen, &enc, wr - 1); \
      while (wr != 0) { \
        BytePPMEncodeByte(&ppmData, &enc, src[lp]); \
        lp += 1; wr -= 1; \
      } \
    } \
  } while (0)

  // repopulate hash buckets; this helps with huge files
  #define Rehash()  do { \
    memset(dict, -1, sizeof(dict)); \
    hfree = 0; \
    pos = (spos > LZFF3_OFS_LIMIT /*+ 1*/ ? spos - LZFF3_OFS_LIMIT - 1 : 0); \
    if (pos >= spos) __builtin_trap(); \
    b4 = \
      (uint32_t)(src[pos]) + \
      ((uint32_t)(src[pos + 1]) << 8) + \
      ((uint32_t)(src[pos + 2]) << 16) + \
      ((uint32_t)(src[pos + 3]) << 24); \
    do { \
      heidx = (b4 * 0x9E3779B1u) % LZFF3_HASH_SIZE; \
      he = dict[heidx]; \
      ntidx = hfree++; \
      htbl[ntidx].sptr = pos; \
      htbl[ntidx].bytes4 = b4; \
      htbl[ntidx].prev = he; \
      dict[heidx] = ntidx; \
      /*++pos;*/ \
      /* update bytes */ \
      b4 = (b4 >> 8) + ((uint32_t)src[pos + 4] << 24); \
    } while (pos++ != spos); \
  } while (0)

  #define AddHashAt()  do { \
    if (hfree == LZFF3_NUM_BUCKETS) Rehash(); \
    b4 = \
      (uint32_t)(src[spos]) + \
      ((uint32_t)(src[spos + 1]) << 8) + \
      ((uint32_t)(src[spos + 2]) << 16) + \
      ((uint32_t)(src[spos + 3]) << 24); \
    heidx = (b4 * 0x9E3779B1u) % LZFF3_HASH_SIZE; \
    he = dict[heidx]; \
    ntidx = hfree++; \
    htbl[ntidx].sptr = spos; \
    htbl[ntidx].bytes4 = b4; \
    htbl[ntidx].prev = he; \
    dict[heidx] = ntidx; \
    /*return he;*/ \
  } while (0)

  //void FindMatch (uint32_t *pbestofs, uint32_t *pbestlen)
  #define FindMatch(pbestofs, pbestlen, xxminlen)  do { \
    mtbestofs = 0; mtbestlen = xxminlen; /* we want matches of at least 4 bytes */ \
    ssizemax = srcsize - spos; \
    AddHashAt(); \
    while (he != ~(uint32_t)0) { \
      cpp = htbl[he].sptr; \
      if (spos - cpp > LZFF3_OFS_LIMIT) he = ~(uint32_t)0; \
      else { \
        mmax = srcsize - spos; \
        if (ssizemax < mmax) mmax = ssizemax; \
        if (LZFF3_OFS_LIMIT < mmax) mmax = LZFF3_OFS_LIMIT; \
        if (mmax > mtbestlen && htbl[he].bytes4 == b4) { \
          /* fast reject based on the last byte */ \
          if (mtbestlen == 3 || src[spos + mtbestlen] == src[cpp + mtbestlen]) { \
            /* yet another fast reject */ \
            if (mtbestlen <= 4 || \
                (src[spos + (mtbestlen>>1)] == src[cpp + (mtbestlen>>1)] && \
                 (mtbestlen < 8 || src[spos + mtbestlen - 1] == src[cpp + mtbestlen - 1]))) \
            { \
              mlen = 4; \
              while (mlen < mmax && src[spos + mlen] == src[cpp + mlen]) ++mlen; \
              if (mlen > mtbestlen) { \
                mtbestofs = spos - cpp; \
                mtbestlen = mlen; \
              } \
            } \
          } \
        } \
        he = htbl[he].prev; \
      } \
    } \
    pbestofs = mtbestofs; pbestlen = mtbestlen; \
  } while (0)


  if (srclen < 1 || srclen > 0x3fffffff) return LZFF3_ERR_ARGS;
  if (dstlen < 1 || dstlen > 0x3fffffff) return LZFF3_ERR_ARGS;

  src = (const uint8_t *)source;
  srcsize = (uint32_t)srclen;

  memset(dict, -1, sizeof(dict));
  htbl = (LZFFHashEntry *)LZFF3_malloc(mman, (uint32_t)sizeof(htbl[0]) * LZFF3_NUM_BUCKETS);
  if (htbl == NULL) return LZFF3_ERR_MEM;
  hfree = 0;

  BytePPMInit(&ppmData);
  WordPPMInit(&ppmMtOfs);
  WordPPMInit(&ppmMtLen);
  WordPPMInit(&ppmLitLen);
  BitPPMInit(&ppmLitFlag, 1);

  EncInit(&enc, dest, (uint32_t)dstlen);

  litpos = 0;
  if (srcsize <= 6) {
    // don't even bother trying
    litcount = srcsize;
  } else {
    // first byte is always a literal
    litcount = 1;
    spos = 1;
    while (spos < srcsize - 3) {
      FindMatch(bestofs, bestlen, 3); spos += 1;
      //ProgReport();
      if (bestofs == 0) {
        if (++litcount == 1) litpos = spos - 1;
      } else {
        // try match with the next char
        uint32_t xdiff;
        xdiff = bestlen - 1;
        if (allow_lazy && spos < srcsize - bestlen) {
          int doagain;
          do {
            uint32_t bestofs1, bestlen1;
            FindMatch(bestofs1, bestlen1, bestlen); spos += 1;
            if (bestlen1 > bestlen) {
              if (++litcount == 1) litpos = spos - 2;
              bestofs = bestofs1; bestlen = bestlen1;
              xdiff = bestlen1 - 1;
              doagain = (spos < srcsize - bestlen1);
            } else {
              xdiff = bestlen - 2;
              doagain = 0;
            }
          } while (doagain);
        }
        if (litcount) FlushLit();
        BitPPMEncode(&ppmLitFlag, &enc, int_false);
        WordPPMEncodeInt(&ppmMtLen, &enc, bestlen - 3);
        WordPPMEncodeInt(&ppmMtOfs, &enc, bestofs - 1);
        if (spos + xdiff < srcsize - 3) {
          do {
            AddHashAt();
            spos += 1;
          } while (--xdiff != 0);
        } else {
          spos += xdiff;
        }
        if (enc.dpos >= 0x7fff0000) spos = srcsize;
      }
    }
    // last bytes as literals
    if (litcount == 0) litpos = spos;
    litcount += srcsize - spos;
  }

  if (enc.dpos < 0x7fff0000) FlushLit();
  EncFlush(&enc);

  LZFF3_mfree(mman, htbl);

  return (enc.dpos < 0x7fff0000 ? enc.dpos : LZFF3_ERR_FILE_TOO_BIG);
}


//==========================================================================
//
//  CompressLZFF3LitOnly
//
//  return size of compressed data, or negative error code
//
//==========================================================================
static OK_UNUSED int CompressLZFF3LitOnly (const void *source, int srclen,
                                           void *dest, int dstlen)
{
  int srcsize;
  int litcount;
  const uint8_t *src;
  EntEncoder enc;
  BytePPM ppmData;
  WordPPM ppmLitLen;
  BitPPM ppmLitFlag;

  if (srclen < 1 || srclen > 0x3fffffff) return LZFF3_ERR_ARGS;
  if (dstlen < 1 || dstlen > 0x3fffffff) return LZFF3_ERR_ARGS;

  src = (const uint8_t *)source;
  srcsize = (uint32_t)srclen;

  BytePPMInit(&ppmData);
  WordPPMInit(&ppmLitLen);
  BitPPMInit(&ppmLitFlag, 1);

  EncInit(&enc, dest, (uint32_t)dstlen);

  litcount = srcsize;

  int lp = 0;
  while (litcount != 0) {
    BitPPMEncode(&ppmLitFlag, &enc, int_true);
    int wr = litcount; if (wr > LZFF3_NUM_LIMIT) wr = LZFF3_NUM_LIMIT;
    litcount -= wr;
    WordPPMEncodeInt(&ppmLitLen, &enc, wr - 1);
    while (wr != 0) {
      BytePPMEncodeByte(&ppmData, &enc, src[lp]);
      lp += 1; wr -= 1;
      if ((wr&0x3ff) == 0 && enc.dpos >= 0x7fff0000) { litcount = 0; wr = 0; }
    }
  }

  EncFlush(&enc);

  return (enc.dpos < 0x7fff0000 ? enc.dpos : LZFF3_ERR_FILE_TOO_BIG);
}


//==========================================================================
//
//  DecompressLZFF3
//
//==========================================================================
static OK_UNUSED intbool_t DecompressLZFF3 (const void *src, int srclen,
                                            void *dest, int unpsize)
{
  intbool_t error;
  EntDecoder dec;
  BytePPM ppmData;
  WordPPM ppmMtOfs, ppmMtLen, ppmLitLen;
  BitPPM ppmLitFlag;
  int litcount, n;
  uint32_t dictpos, spos;

  #define PutByte(bt_)  do { \
    if (unpsize != 0) { \
      ((uint8_t *)dest)[dictpos++] = (uint8_t)(bt_); unpsize -= 1; \
    } else { \
      error = int_true; \
    } \
  } while (0)

  if (srclen < 1 || srclen > 0x3fffffff) return 0;
  if (unpsize < 1 || unpsize > 0x3fffffff) return 0;

  error = int_false;
  dictpos = 0;

  BytePPMInit(&ppmData);
  WordPPMInit(&ppmMtOfs);
  WordPPMInit(&ppmMtLen);
  WordPPMInit(&ppmLitLen);
  BitPPMInit(&ppmLitFlag, 1);

  DecInit(&dec, src, srclen);

  if (!BitPPMDecode(&ppmLitFlag, &dec)) error = int_true;
  else {
    uint8_t sch;
    int len, ofs;

    litcount = WordPPMDecodeInt(&ppmLitLen, &dec) + 1;
    while (!error && litcount != 0) {
      litcount -= 1;
      n = BytePPMDecodeByte(&ppmData, &dec);
      PutByte((uint8_t)n);
      error = error || (dec.spos > dec.send);
    }

    while (!error && unpsize != 0) {
      if (BitPPMDecode(&ppmLitFlag, &dec)) {
        litcount = WordPPMDecodeInt(&ppmLitLen, &dec) + 1;
        while (!error && litcount != 0) {
          litcount -= 1;
          n = BytePPMDecodeByte(&ppmData, &dec);
          PutByte((uint8_t)n);
          error = error || (dec.spos > dec.send);
        }
      } else {
        len = WordPPMDecodeInt(&ppmMtLen, &dec) + 3;
        ofs = WordPPMDecodeInt(&ppmMtOfs, &dec) + 1;
        error = error || (dec.spos > dec.send) || (len > unpsize) || ((uint32_t)ofs > dictpos);
        if (!error) {
          spos = dictpos - ofs;
          while (!error && len != 0) {
            len -= 1;
            sch = ((const uint8_t *)dest)[spos];
            spos++;
            PutByte(sch);
          }
        }
      }
    }
  }

  return (!error && unpsize == 0);
}


// ////////////////////////////////////////////////////////////////////////// //
// SQ3DB
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  SQ3DB::SQ3DB
//
//==========================================================================
SQ3DB::SQ3DB (const char *fname, bool readWrite, bool allowCreate)
  : db(0)
  , stlist(0)
{
  if (readWrite) {
    if (allowCreate) OpenRWCreate(fname); else OpenRW(fname);
  } else {
    OpenRO(fname);
  }
}


//==========================================================================
//
//  SQ3DB::Exec
//
//==========================================================================
bool SQ3DB::Exec (const char *sql) {
  if (!db) return false;
  if (!sql || !sql[0]) return true;
  if (sqlite3_exec(db, sql, NULL, NULL, NULL) != SQLITE_OK) {
    return false;
  }
  return true;
}


//==========================================================================
//
//  SQ3DB::SetupPragmas
//
//==========================================================================
void SQ3DB::SetupPragmas () {
  if (db) {
    sqlite3_exec(db,
      "PRAGMA application_id=1230389582; /*IVAN*/\n"
      "PRAGMA auto_vacuum=NONE;\n"
      "PRAGMA busy_timeout=10000;\n"
      "PRAGMA checkpoint_fullfsync=OFF;\n"
      "PRAGMA encoding='UTF-8';\n"
      "PRAGMA foreign_keys=OFF;\n"
      "PRAGMA fullfsync=OFF;\n"
      "PRAGMA journal_mode=WAL;\n"
      "PRAGMA secure_delete=OFF;\n"
      "PRAGMA synchronous=OFF;\n"
      "PRAGMA trusted_schema=ON;\n"
      "PRAGMA writable_schema=OFF;\n"
      "", NULL, NULL, NULL);
  }
}


//==========================================================================
//
//  SQ3DB::SetupROPragmas
//
//==========================================================================
void SQ3DB::SetupROPragmas () {
  if (db) {
    sqlite3_exec(db,
      "PRAGMA busy_timeout=10000;\n"
      "PRAGMA encoding='UTF-8';\n"
      "PRAGMA foreign_keys=OFF;\n"
      "", NULL, NULL, NULL);
  }
}


//==========================================================================
//
//  SQ3DB::AppendStmt
//
//==========================================================================
void SQ3DB::AppendStmt (StmtIntr *stx) {
  if (stx) {
    IvanAssert(!stx->owner);
    IvanAssert(!stx->next);
    stx->owner = this;
    stx->next = stlist;
    stlist = stx;
  }
}


//==========================================================================
//
//  SQ3DB::RemoveStmt
//
//  this also closes it
//
//==========================================================================
void SQ3DB::RemoveStmt (StmtIntr *stx) {
  if (stx) {
    IvanAssert(stx->owner == this);
    StmtIntr *p = 0;
    StmtIntr *c = stlist;
    while (c && c != stx) {
      p = c; c = c->next;
    }
    IvanAssert(c == stx);
    if (stx->stmt) {
      sqlite3_finalize(stx->stmt);
      stx->stmt = 0;
    }
    stx->owner = 0;
    if (p) p->next = stx->next; else stlist = stx->next;
    stx->next = 0;
  }
}


//==========================================================================
//
//  SQ3DB::Close
//
//==========================================================================
void SQ3DB::Close () {
  // close all statements
  while (stlist) {
    StmtIntr *c = stlist; stlist = c->next;
    IvanAssert(c->owner == this);
    if (c->stmt) {
      sqlite3_finalize(c->stmt);
      c->stmt = 0;
    }
    c->owner = 0;
    c->next = 0;
  }
  if (db) {
    if (!sqlite3_db_readonly(db, NULL)) {
      sqlite3_exec(db, "PRAGMA optimise;", 0, 0, 0);
    }
    sqlite3_close_v2(db);
    db = 0;
  }
}


//==========================================================================
//
//  SQ3DB::OpenRO
//
//==========================================================================
bool SQ3DB::OpenRO (const char *fname) {
  Close();
  if (!fname || !fname[0]) return false;
  if (sqlite3_open_v2(fname, &db, SQLITE_OPEN_READONLY |
                                  SQLITE_OPEN_EXRESCODE, 0) != SQLITE_OK)
  {
    db = 0; // just in case
    return false;
  }
  SetupROPragmas();
  return true;
}


//==========================================================================
//
//  SQ3DB::OpenRW
//
//==========================================================================
bool SQ3DB::OpenRW (const char *fname) {
  Close();
  if (!fname || !fname[0]) return false;
  if (sqlite3_open_v2(fname, &db, SQLITE_OPEN_READWRITE |
                                  SQLITE_OPEN_EXRESCODE, 0) != SQLITE_OK)
  {
    db = 0; // just in case
    return false;
  }
  SetupPragmas();
  return true;
}


//==========================================================================
//
//  SQ3DB::OpenRWCreate
//
//==========================================================================
bool SQ3DB::OpenRWCreate (const char *fname) {
  Close();
  if (!fname || !fname[0]) return false;
  if (sqlite3_open_v2(fname, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE |
                                  SQLITE_OPEN_EXRESCODE, 0) != SQLITE_OK)
  {
    db = 0; // just in case
    return false;
  }
  SetupPragmas();
  return true;
}


//==========================================================================
//
//  SQ3DB::Stmt
//
//==========================================================================
bool SQ3DB::Stmt (SQ3Stmt &stmt, const char *sql) {
  stmt.Close();
  if (!sql || !sql[0] || !db) return false;

  sqlite3_stmt *stmt3;
  if (sqlite3_prepare_v2(db, sql, -1, &stmt3, 0) != SQLITE_OK) {
    return false;
  }

  StmtIntr *stx = new StmtIntr();
  stx->refc = 1;
  stx->stmt = stmt3;
  AppendStmt(stx);
  IvanAssert(!stmt.stmt);
  stmt.stmt = stx;
  return true;
}


// ////////////////////////////////////////////////////////////////////////// //
// SQ3Stmt
// ////////////////////////////////////////////////////////////////////////// //


//==========================================================================
//
//  SQ3Stmt::FindParam
//
//==========================================================================
int SQ3Stmt::FindParam (const char *param) {
  if (!Stmt3()) ABORT("cannot find binding `%s` in closed statement!", param);
  if (!param && !param[0]) ABORT("cannot find unnamed binding!");
  const int fidx = sqlite3_bind_parameter_index(Stmt3(), param);
  if (fidx == 0) {
    ABORT("hiscore binding `%s` not found!", param);
  }
  return fidx;
}


//==========================================================================
//
//  SQ3Stmt::FindColumn
//
//==========================================================================
int SQ3Stmt::FindColumn (const char *name) {
  if (!Stmt3()) ABORT("cannot find column `%s` in closed statement!", name);
  if (!name && !name[0]) ABORT("cannot find unnamed column!");
  const int count = sqlite3_column_count(Stmt3());
  for (int fidx = 0; fidx != count; fidx += 1) {
    if (strcmp(name, sqlite3_column_name(Stmt3(), fidx)) == 0) {
      return fidx;
    }
  }
  ABORT("hiscore column `%s` not found!", name);
  return -1;
}


// ////////////////////////////////////////////////////////////////////////// //
// sqlite file archive, used for saves and bones
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  AlignFOfs
//
//==========================================================================
static FORCE_INLINE uint32_t AlignFOfs (uint32_t fofs) {
  return (fofs / (uint32_t)LZFF_CHUNK_SIZE * (uint32_t)LZFF_CHUNK_SIZE);
}


//==========================================================================
//
//  ClusterIndex
//
//==========================================================================
static FORCE_INLINE uint32_t ClusterIndex (uint32_t fofs) {
  return fofs / (uint32_t)LZFF_CHUNK_SIZE;
}


//==========================================================================
//
//  ClusterCount
//
//==========================================================================
static FORCE_INLINE uint32_t ClusterCount (uint32_t fsize) {
  return (fsize + (uint32_t)LZFF_CHUNK_SIZE - 1u) / (uint32_t)LZFF_CHUNK_SIZE;
}


//==========================================================================
//
//  SQArchive::SQArchive
//
//==========================================================================
SQArchive::SQArchive ()
  : db(0)
  , inTransaction(false)
  , wasError(false)
  , files(0)
  , filesAlloced(0)
{
}


//==========================================================================
//
//  SQArchive::~SQArchive
//
//==========================================================================
SQArchive::~SQArchive () {
  Close();
}


//==========================================================================
//
//  SQArchive::SetError
//
//==========================================================================
void SQArchive::SetError () {
  #ifdef SQA_DEBUG_ERROR_ABORT
  __builtin_trap();
  #endif
  wasError = true;
}


//==========================================================================
//
//  SQArchive::Close
//
//==========================================================================
void SQArchive::Close () {
  if (db) {
    for (uint32_t f = 0; f != filesAlloced; f += 1) {
      File *fl = GetFI(u32fd(f + 1u));
      if (fl && fl->blobId != 0) {
        FileClose(u32fd(f + 1u));
      }
    }
    if (inTransaction) {
      SetError();
      inTransaction = false;
      sqlite3_exec(db, "ROLLBACK TRANSACTION", NULL, NULL, NULL);
    }
    if (sqlite3_close_v2(db) != 0) SetError();
    db = 0;
    if (files) ::free(files);
    files = 0; filesAlloced = 0;
  }
}


//==========================================================================
//
//  SQArchive::Vacuum
//
//==========================================================================
void SQArchive::Vacuum () {
  if (db && !wasError && !inTransaction) {
    sqlite3_exec(db, "VACUUM", NULL, NULL, NULL);
  }
}


//==========================================================================
//
//  SQArchive::Open
//
//==========================================================================
void SQArchive::Open (cfestring &filename, bool allowCreate) {
  Close();
  wasError = false;
  if (filename.IsEmpty()) { SetError(); return; }
  if (sqlite3_open_v2(filename.CStr(), &db,
                      SQLITE_OPEN_READWRITE |
                      (allowCreate ? SQLITE_OPEN_CREATE : 0) |
                      SQLITE_OPEN_EXRESCODE, 0) != SQLITE_OK)
  {
    db = 0; // just in case
    SetError();
    //wasError = true;
    return;
  }
  sqlite3_exec(db,
    //"PRAGMA page_size=8192; /*our files are big*/\n"
    "PRAGMA application_id=1230389582; /*IVAN*/\n"
    "PRAGMA auto_vacuum=NONE;\n"
    "PRAGMA busy_timeout=10000;\n"
    "PRAGMA checkpoint_fullfsync=OFF;\n"
    "PRAGMA encoding='UTF-8';\n"
    "PRAGMA foreign_keys=OFF;\n"
    "PRAGMA fullfsync=OFF;\n"
    "PRAGMA journal_mode=WAL/*DELETE*/;\n"
    "PRAGMA secure_delete=OFF;\n"
    "PRAGMA synchronous=OFF;\n"
    "PRAGMA trusted_schema=ON;\n"
    "PRAGMA writable_schema=OFF;\n"
    "", NULL, NULL, NULL);

  sqlite3_exec(db,
    "CREATE TABLE IF NOT EXISTS fileinfo (\n"
    "    wtime INTEGER NOT NULL DEFAULT 0 /* epoch */\n"
    "  , blobid INTEGER NOT NULL /* never 0! */\n"
    "  , fsize INTEGER NOT NULL /* unpacked */\n"
    "  , chunksize INTEGER NOT NULL DEFAULT 0 /* 0: raw */\n"
    "  , fname TEXT UNIQUE NOT NULL /* with path */\n"
    ") STRICT;\n"
    "\n"
    "CREATE TABLE IF NOT EXISTS blobs (\n"
    "    blobid INTEGER PRIMARY KEY\n"
    "  , data BLOB\n"
    ") STRICT;\n"
    "", NULL, NULL, NULL);
}


//==========================================================================
//
//  SQArchive::OpenTransaction
//
//==========================================================================
void SQArchive::OpenTransaction () {
  if (db && !wasError && !inTransaction) {
    if (sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
      SetError();
    } else {
      inTransaction = true;
      #ifdef SQA_DEBUG_TRANSACTIONS
      ConLogf("SQA: transaction opened!");
      #endif
    }
  } else {
    SetError();
  }
}


//==========================================================================
//
//  SQArchive::CloseTransaction
//
//==========================================================================
void SQArchive::CloseTransaction () {
  if (db && !wasError && inTransaction) {
    if (sqlite3_exec(db, "COMMIT TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
      SetError();
    }
    inTransaction = false;
    #ifdef SQA_DEBUG_TRANSACTIONS
    ConLogf("SQA: transaction closed!");
    #endif
  } else {
    if (inTransaction) {
      sqlite3_exec(db, "ROLLBACK TRANSACTION", NULL, NULL, NULL);
      inTransaction = false;
    }
    SetError();
  }
}


//==========================================================================
//
//  SQArchive::AbortTransaction
//
//==========================================================================
void SQArchive::AbortTransaction () {
  if (db && inTransaction) {
    sqlite3_exec(db, "ROLLBACK TRANSACTION", NULL, NULL, NULL);
    #ifdef SQA_DEBUG_TRANSACTIONS
    ConLogf("SQA: transaction aborted!");
    #endif
  }
  SetError();
  inTransaction = false;
}


//==========================================================================
//
//  SQArchive::AllocFD
//
//==========================================================================
SQAFile SQArchive::AllocFD () {
  uint32_t fd = 0;
  while (fd != filesAlloced && files[fd].blobId != 0) {
    fd += 1u;
  }
  if (fd == filesAlloced) {
    // allocate more file info records
    if (filesAlloced >= 1024) {
      ABORT("too many opened files in SQLite archive!");
    }
    const uint32_t newFA = filesAlloced + 128;
    if (!files) {
      files = (File *)::calloc(newFA, sizeof(File));
      if (!files) {
        ABORT("out of memory for files in SQLite archive!");
      }
    } else {
      File *f2 = (File *)::realloc(files, newFA * sizeof(File));
      if (!f2) {
        ABORT("out of memory for files in SQLite archive!");
      }
      files = f2;
      for (uint32_t f = filesAlloced; f != newFA; f += 1) {
        memset(&files[f], 0, sizeof(File));
      }
    }
    filesAlloced = newFA;
  }

  memset(&files[(uint32_t)fd], 0, sizeof(File));
  return u32fd(fd + 1u);
}


//==========================================================================
//
//  SQArchive::OpenPackedFile
//
//  called should close the blob on error (if `0` is returned).
//  otherwise, return fd.
//
//==========================================================================
SQAFile SQArchive::OpenPackedFile (const char *fname, sqlite3_blob *bb, uint32_t fsize) {
  IvanAssert(bb);

  const int blobSize = sqlite3_blob_bytes(bb);
  if (blobSize < 0) return 0;
  if (blobSize == 0 || fsize == 0) return 0;

  const uint32_t realClusterCount = ClusterCount(fsize);
  IvanAssert(realClusterCount != 0);

  uint8_t *dirData = (uint8_t *)::calloc(1, 8u + realClusterCount * (unsigned)LZFF_PKF_SIZE);
  if (!dirData) ABORT("out of memory for directory data");

  if (sqlite3_blob_read(bb, dirData, 8u + realClusterCount * (unsigned)LZFF_PKF_SIZE, 0) != SQLITE_OK) {
    ConLogf("error reading file '%s' directory", fname);
    ::free(dirData);
    return 0;
  }

  if (*(uint32_t *)dirData != crc32_buf(dirData + 4, 4u + realClusterCount * (unsigned)LZFF_PKF_SIZE)) {
    ConLogf("corrupted directory in file '%s'", fname);
    ::free(dirData);
    return 0;
  }

  // read number of clusters
  uint32_t clusterCount = 0;
  memcpy(&clusterCount, dirData + 4, 4);
  if (clusterCount != realClusterCount) {
    // invalid number of clusters
    ConLogf("file '%s' is corrupted: clusterCount=%s, expected %s",
            fname, ComatozeU32(clusterCount), ComatozeU32(realClusterCount));
    ::free(dirData);
    return 0;
  }
  IvanAssert(clusterCount != 0);

  // allocate clusters
  ClusterInfo *fatData = (ClusterInfo *)::calloc(realClusterCount, sizeof(ClusterInfo));
  if (!fatData) ABORT("out of memory for reader fat data");

  bool error = false;
  uint32_t calcFSize = 0;
  uint32_t currBOfs = 8u + (uint32_t)clusterCount * (unsigned)LZFF_PKF_SIZE;
  uint8_t *ccptr = dirData + 8u;
  uint32_t fsizeLeft = fsize;

  for (uint32_t f = 0; f != clusterCount; f += 1, ccptr += (unsigned)LZFF_PKF_SIZE) {
    ClusterInfo *ci = &fatData[f];

    ci->bofs = currBOfs;
    #if LZFF_PKF_SIZE == 2
      uint16_t pkfsize;
      memcpy(&pkfsize, ccptr, 2);
      ci->psize = (pkfsize ? pkfsize : 0x10000);
    #elif LZFF_PKF_SIZE == 3
      uint32_t pkfsize = 0;
      memcpy(&pkfsize, ccptr, 3);
      ci->psize = (pkfsize ? pkfsize : 0x1000000);
    #else
      uint32_t pkfsize = 0;
      memcpy(&pkfsize, ccptr, 4);
      IvanAssert(pkfsize != 0);
    #endif

    if (fsizeLeft >= LZFF_CHUNK_SIZE) {
      ci->usize = LZFF_CHUNK_SIZE;
      fsizeLeft -= LZFF_CHUNK_SIZE;
    } else {
      ci->usize = fsizeLeft;
      fsizeLeft = 0;
    }

    #ifdef SQA_DEBUG_READ_CHUNKS
    ConLogf("file '%s' cluster #%s: fofs=%s, bofs=%s; usize=%s; psize=%s",
            fname, ComatozeU32(f), ComatozeU32(calcFSize),
            ComatozeU32(ci->bofs), ComatozeU32(ci->usize), ComatozeU32(ci->psize));
    #endif

    // sanity checks
    if (ci->psize > ci->usize) { error = true; break; }
    if (ci->bofs + ci->psize > (uint32_t)blobSize) { error = true; break; }

    calcFSize += ci->usize;
    if (calcFSize > fsize) { error = true; break; }
    currBOfs += ci->psize + 4;
    if (currBOfs > (uint32_t)blobSize) { error = true; break; }
  }

  ::free(dirData);

  if (!error && calcFSize != fsize) error = true;

  if (!error) {
    SQAFile fd = AllocFD();
    File *fl = GetFI(fd);
    fl->blob = bb;
    fl->fsize = fsize;
    fl->fatData = fatData;
    memset(&fl->buffer, 0, sizeof(fl->buffer));
    fl->buffer.fofs = ~(uint32_t)0;
    return fd;
  } else {
    ::free(fatData);
    return 0;
  }
}


//==========================================================================
//
//  SQArchive::ReadCluster
//
//  read new cluster if necessary
//
//==========================================================================
void SQArchive::ReadCluster (File *fl, uint32_t fofs) {
  IvanAssert(fl);
  IvanAssert(fl->blob);
  if (fofs >= fl->fsize) { SetError(); return; }
  if (AlignFOfs(fofs) == fl->buffer.fofs) return;
  if (fl->fatData) {
    // packed file, read and unpack the buffer
    ClusterInfo *cc = &fl->fatData[ClusterIndex(fofs)];
    #ifdef SQA_DEBUG_READ_CHUNKS
    ConLogf("file '%s': looking for cluster #%s (fofs=%s; bofs=%s; usize=%s; psize=%s)...",
            fl->fname, ComatozeU32(ClusterIndex(fofs)), ComatozeU32(fofs),
            ComatozeU32(cc->bofs), ComatozeU32(cc->usize), ComatozeU32(cc->psize));
    #endif
    if (cc->psize == cc->usize) {
      // not packed
      if (sqlite3_blob_read(fl->blob, fl->tempBuf, cc->usize + 4, cc->bofs) != SQLITE_OK) {
        SetError();
        return;
      }
      const uint32_t crc = crc32_buf(fl->tempBuf + 4, cc->usize);
      if (memcmp(&crc, fl->tempBuf, 4) != 0) {
        ConLogf("file '%s' raw chunk #%u: invalid crc!", fl->fname, ClusterIndex(fofs));
        SetError();
        return;
      }
      memcpy(fl->buffer.data, fl->tempBuf + 4, cc->usize);
      fl->buffer.fofs = AlignFOfs(fofs);
      fl->buffer.used = cc->usize;
      #ifdef SQA_DEBUG_READ_CHUNKS
      ConLogf("file '%s': found unpacked cluster #%u (fofs=%u).", fl->fname, ClusterIndex(fofs), fofs);
      #endif
    } else {
      // packed
      if (sqlite3_blob_read(fl->blob, fl->tempBuf, cc->psize + 4, cc->bofs) != SQLITE_OK) {
        SetError();
        return;
      }
      if (!DecompressLZFF3(fl->tempBuf + 4, (int)cc->psize, fl->buffer.data, (int)cc->usize)) {
        ConLogf("file '%s' corrupted packed chunk #%u!", fl->fname, ClusterIndex(fofs));
        SetError();
        return;
      }
      const uint32_t crc = crc32_buf(fl->buffer.data, cc->usize);
      if (memcmp(&crc, fl->tempBuf, 4) != 0) {
        ConLogf("file '%s' packed chunk #%u: invalid crc!", fl->fname, ClusterIndex(fofs));
        SetError();
        return;
      }
      fl->buffer.fofs = AlignFOfs(fofs);
      fl->buffer.used = cc->usize;
      #ifdef SQA_DEBUG_READ_CHUNKS
      ConLogf("file '%s': found packed cluster #%u (fofs=%u).", fl->fname, ClusterIndex(fofs), fofs);
      #endif
    }
  } else {
    // raw unpacked file
    IvanAssert(fl->blob);
    const uint32_t bofs = AlignFOfs(fofs);
    uint32_t bsize = Min((uint32_t)LZFF_CHUNK_SIZE, fl->fsize - bofs);
    IvanAssert(bsize != 0);
    if (sqlite3_blob_read(fl->blob, fl->buffer.data, bsize, bofs) != SQLITE_OK) {
      SetError();
      return;
    }
    fl->buffer.fofs = bofs;
    fl->buffer.used = bsize;
  }
}


//==========================================================================
//
//  SQArchive::FlushDirtyBuffer
//
//==========================================================================
void SQArchive::FlushDirtyBuffer (File *fl) {
  IvanAssert(fl);
  if (fl->buffer.dirty) {
    IvanAssert(fl->buffer.fofs != ~(uint32_t)0);
    IvanAssert(fl->buffer.used > 0 && fl->buffer.used <= LZFF_CHUNK_SIZE);
    const uint32_t cidx = ClusterIndex(fl->buffer.fofs);
    IvanAssert(cidx < fl->fatAlloced);
    const uint32_t crc = crc32_buf(fl->buffer.data, fl->buffer.used);
    int cres;
    if (fl->buffer.used == 1) {
      cres = -1;
    } else if (fl->compLevel <= 1) {
      // litral-only mode
      cres = CompressLZFF3LitOnly(fl->buffer.data, (int)fl->buffer.used, fl->tempBuf, (int)fl->buffer.used - 1);
    } else if (fl->compLevel <= 2) {
      // no lazy matching
      cres = CompressLZFF3(fl->buffer.data, (int)fl->buffer.used, fl->tempBuf, (int)fl->buffer.used - 1, int_false);
    } else {
      // lazy matching
      cres = CompressLZFF3(fl->buffer.data, (int)fl->buffer.used, fl->tempBuf, (int)fl->buffer.used - 1, int_true);
    }
    ClusterInfo *cc = &fl->fatData[cidx];
    if (cc->packedData) { ::free(cc->packedData); cc->packedData = 0; }
    if (cres > 0 && cres < (int)fl->buffer.used) {
      // packed
      cc->packedData = (uint8_t *)::malloc((size_t)(cres + 4));
      if (!cc->packedData) ABORT("out of memory for packed data");
      memcpy(cc->packedData + 4, fl->tempBuf, (size_t)cres);
      cc->psize = (uint32_t)cres;
    } else {
      // unpacked
      cc->packedData = (uint8_t *)::malloc(fl->buffer.used + 4);
      if (!cc->packedData) ABORT("out of memory for packed data");
      memcpy(cc->packedData + 4, fl->buffer.data, fl->buffer.used);
      cc->psize = fl->buffer.used;
    }
    memcpy(cc->packedData, &crc, 4);
    cc->usize = fl->buffer.used;
    fl->buffer.dirty = false;
  }
}


//==========================================================================
//
//  SQArchive::WriterSeekAndUnpack
//
//==========================================================================
void SQArchive::WriterSeekAndUnpack (File *fl, uint32_t fofs) {
  IvanAssert(fl);
  if (fofs > fl->fsize) { SetError(); return; }
  if (!fl->fatAlloced) {
    if (fofs != 0) { SetError(); return; }
    IvanAssert(fl->fsize == 0);
    fl->fatAlloced = 256;
    fl->fatData = (ClusterInfo *)::calloc(fl->fatAlloced, sizeof(ClusterInfo));
    if (!fl->fatData) ABORT("out of memory for packed file clusters");
    fl->buffer.fofs = 0;
    fl->buffer.used = 0;
    fl->buffer.dirty = true;
  } else {
    const uint32_t cofs = AlignFOfs(fofs);
    if (fl->buffer.fofs != cofs) {
      FlushDirtyBuffer(fl);
      IvanAssert(!fl->buffer.dirty);
      const uint32_t cidx = ClusterIndex(cofs);
      if (cidx >= fl->fatAlloced) {
        // need more clusters
        const uint32_t newsz = (cidx | 255) + 1;
        IvanAssert(fl->fatAlloced != 0);
        IvanAssert(fl->fatData);
        ClusterInfo *newcc = (ClusterInfo *)::realloc(fl->fatData, newsz * sizeof(ClusterInfo));
        if (!newcc) ABORT("out of memory for fat table");
        memset(newcc + fl->fatAlloced, 0, (newsz - fl->fatAlloced) * sizeof(ClusterInfo));
        fl->fatAlloced = newsz;
        fl->fatData = newcc;
      }
      ClusterInfo *cc = &fl->fatData[cidx];
      if (fofs == fl->fsize) {
        // new cluster
        IvanAssert(!cc->usize);
        IvanAssert(!cc->psize);
        IvanAssert(!cc->packedData);
        fl->buffer.fofs = cofs;
        fl->buffer.used = 0;
      } else {
        // old cluster
        IvanAssert(cc->usize > 0 && cc->usize <= LZFF_CHUNK_SIZE);
        IvanAssert(cc->psize > 0 && cc->psize <= LZFF_CHUNK_SIZE);
        IvanAssert(cc->packedData);
        if (cc->psize == cc->usize) {
          memcpy(fl->buffer.data, cc->packedData + 4, cc->psize);
          fl->buffer.fofs = cofs;
          fl->buffer.used = cc->psize;
        } else if (DecompressLZFF3(fl->buffer.data, (int)cc->psize, cc->packedData + 4, (int)cc->usize)) {
          fl->buffer.fofs = cofs;
          fl->buffer.used = cc->usize;
        } else {
          SetError();
        }
      }
    }
  }
}


//==========================================================================
//
//  SQArchive::FileExists
//
//==========================================================================
bool SQArchive::FileExists (cfestring &fname) {
  if (fname.IsEmpty()) return false;
  if (wasError) return false;
  if (!db) { SetError(); return false; }

  sqlite3_stmt *stmt3;
  if (sqlite3_prepare_v2(db,
        "SELECT 1 FROM fileinfo "
        "WHERE fname=:fname "
        "LIMIT 1 "
        "", -1, &stmt3, 0) != SQLITE_OK)
  {
    SetError();
    return false;
  }

  const int fidx = sqlite3_bind_parameter_index(stmt3, ":fname");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":fname");
  sqlite3_bind_text(stmt3, fidx, fname.CStr(), -1, SQLITE_STATIC/*FIXME! multithreading!*/);

  bool res = false;
  for (;;) {
    int sqres = sqlite3_step(stmt3);
    if (sqres == SQLITE_ROW) {
      res = true;
    } else {
      if (sqres != SQLITE_DONE) SetError();
      break;
    }
  }
  sqlite3_finalize(stmt3);

  return res;
}


//==========================================================================
//
//  SQArchive::FileDelete
//
//  return `true` if the file was present.
//
//==========================================================================
bool SQArchive::FileDelete (cfestring &fname) {
  if (fname.IsEmpty()) return false;
  if (wasError) return false;
  if (!db) { SetError(); return false; }

  sqlite3_stmt *stmt3;
  int fidx, sqres;

  if (sqlite3_prepare_v2(db,
        "SELECT rowid, blobid "
        "FROM fileinfo "
        "WHERE fname=:fname "
        "LIMIT 1 "
        "", -1, &stmt3, 0) != SQLITE_OK)
  {
    SetError();
    return false;
  }

  fidx = sqlite3_bind_parameter_index(stmt3, ":fname");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":fname");
  sqlite3_bind_text(stmt3, fidx, fname.CStr(), -1, SQLITE_STATIC/*FIXME! multithreading!*/);

  uint32_t rowid = 0;
  uint32_t blobid = 0;
  for (;;) {
    sqres = sqlite3_step(stmt3);
    if (sqres == SQLITE_ROW) {
      rowid = sqlite3_column_int(stmt3, 0);
      blobid = sqlite3_column_int(stmt3, 1);
    } else {
      if (sqres != SQLITE_DONE) SetError();
      break;
    }
  }
  sqlite3_finalize(stmt3);

  if (blobid == 0) return false; // not an error
  if (wasError) return false;

  if (sqlite3_prepare_v2(db,
        "DELETE FROM fileinfo WHERE rowid=:rowid "
        "", -1, &stmt3, 0) != SQLITE_OK)
  {
    SetError();
    return false;
  }

  fidx = sqlite3_bind_parameter_index(stmt3, ":rowid");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":rowid");
  sqlite3_bind_int(stmt3, fidx, (int)rowid);

  for (;;) {
    sqres = sqlite3_step(stmt3);
    if (sqres == SQLITE_ROW) {
      rowid = sqlite3_column_int(stmt3, 0);
      blobid = sqlite3_column_int(stmt3, 1);
    } else {
      if (sqres != SQLITE_DONE) SetError();
      break;
    }
  }
  sqlite3_finalize(stmt3);
  if (wasError) return false;

  DeleteBlob(blobid);

  return true;
}


//==========================================================================
//
//  SQArchive::FileOpen
//
//==========================================================================
SQAFile SQArchive::FileOpen (cfestring &fname) {
  if (fname.IsEmpty()) return 0;
  if (wasError) return 0;
  if (!db) { SetError(); return 0; }

  sqlite3_stmt *stmt3;
  if (sqlite3_prepare_v2(db,
        "SELECT blobid, wtime, fsize, chunksize "
        "FROM fileinfo "
        "WHERE fname=:fname "
        "LIMIT 1 "
        "", -1, &stmt3, 0) != SQLITE_OK)
  {
    SetError();
    return 0;
  }

  const int fidx = sqlite3_bind_parameter_index(stmt3, ":fname");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":fname");
  sqlite3_bind_text(stmt3, fidx, fname.CStr(), -1, SQLITE_STATIC/*FIXME! multithreading!*/);

  uint32_t wtime = 0;
  uint32_t blobid = 0;
  uint32_t fsize = ~(uint32_t)0;
  uint32_t chunksize = ~(uint32_t)0;
  for (;;) {
    int sqres = sqlite3_step(stmt3);
    if (sqres == SQLITE_ROW) {
      blobid = sqlite3_column_int(stmt3, 0);
      wtime = sqlite3_column_int(stmt3, 1);
      fsize = sqlite3_column_int(stmt3, 2);
      chunksize = sqlite3_column_int(stmt3, 3);
    } else {
      if (sqres != SQLITE_DONE) SetError();
      break;
    }
  }
  sqlite3_finalize(stmt3);

  if (blobid == 0) return 0; // not an error
  if (wasError) return 0;

  if (fsize > 0x40000000) { SetError(); return 0; }
  if (fsize == 0) {
    if (chunksize != 0) { SetError(); return 0; }
  } else {
    if (chunksize != 0 && chunksize != LZFF_CHUNK_SIZE) { SetError(); return 0; }
  }

  sqlite3_blob *bb = 0;
  if (sqlite3_blob_open(db, "main", "blobs", "data", blobid, 0, &bb) != SQLITE_OK) {
    SetError();
    return 0;
  }

  const int blobSize = sqlite3_blob_bytes(bb);
  if (blobSize < 0) { sqlite3_blob_close(bb); SetError(); return 0; }

  SQAFile fd = 0;
  File *fl = 0;
  if (chunksize == 0) {
    // unpacked
    if ((uint32_t)blobSize < fsize) {
      sqlite3_blob_close(bb);
      SetError();
      return 0;
    }

    fd = AllocFD();
    fl = GetFI(fd);
    fl->blob = bb;
    memset(&fl->buffer, 0, sizeof(fl->buffer));
    fl->buffer.fofs = ~(uint32_t)0;
  } else {
    fd = OpenPackedFile(fname.CStr(), bb, fsize);
    if (fd == 0) {
      sqlite3_blob_close(bb);
      SetError();
      ConLogf("file '%s' is corrupted!", fname.CStr());
      return 0;
    }
  }

  fl = GetFI(fd);
  fl->blobId = blobid;
  fl->fname = ::strdup(fname.CStr());
  if (!fl->fname) {
    sqlite3_blob_close(bb);
    ABORT("SQA: out of memory to store file name");
  }
  fl->wtime = wtime;
  fl->fsize = fsize;

  #ifdef SQA_DEBUG_READ
  ConLogf("SQA: opened file '%s' (size=%s; wtime=%s)",
          fl->fname, ComatozeU32(fl->fsize), ComatozeU32(fl->wtime));
  #endif

  IvanAssert(!fl->buffer.data);
  fl->buffer.data = (uint8_t *)::malloc(LZFF_CHUNK_SIZE);
  if (!fl->buffer.data) ABORT("out of memory for file buffer");

  IvanAssert(!fl->tempBuf);
  if (fl->fatData) {
    fl->tempBuf = (uint8_t *)::malloc(LZFF_CHUNK_SIZE + 4/*crc*/);
    if (!fl->tempBuf) ABORT("out of memory for temp buffer");
  }

  return fd;
}


//==========================================================================
//
//  SQArchive::FileCreate
//
//  for reading and writing.
//  return 0 on error, >0 on success.
//  will replace existing file.
//
//==========================================================================
SQAFile SQArchive::FileCreate (cfestring &fname, int compLevel) {
  if (fname.IsEmpty()) { SetError(); return 0; }
  if (wasError) return 0;
  if (!db) { SetError(); return 0; }

  SQAFile fd = AllocFD();
  File *fl = GetFI(fd);
  fl->blob = 0;
  fl->blobId = ~(uint32_t)0; // invalid blob id
  fl->fname = ::strdup(fname.CStr());
  if (!fl->fname) {
    ABORT("SQA: out of memory to store file name");
  }
  fl->wtime = (uint32_t)time(NULL);
  fl->fsize = 0;
  fl->compLevel = compLevel;

  IvanAssert(!fl->buffer.data);
  fl->buffer.data = (uint8_t *)::malloc(LZFF_CHUNK_SIZE);
  if (!fl->buffer.data) ABORT("out of memory for file buffer");

  IvanAssert(!fl->tempBuf);
  fl->tempBuf = (uint8_t *)::malloc(LZFF_CHUNK_SIZE + 4/*crc*/);
  if (!fl->tempBuf) ABORT("out of memory for temp buffer");

  return fd;
}


//==========================================================================
//
//  SQArchive::DeleteBlob
//
//==========================================================================
void SQArchive::DeleteBlob (uint32_t blobid) {
  if (blobid != 0) {
    sqlite3_stmt *stmt3 = 0;
    int fidx, sqres;
    if (sqlite3_prepare_v2(db,
          "DELETE FROM blobs "
          "WHERE blobid=:blobid "
          "", -1, &stmt3, 0) != SQLITE_OK)
    {
      SetError();
      return;
    }
    fidx = sqlite3_bind_parameter_index(stmt3, ":blobid");
    if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":blobid");
    sqlite3_bind_int(stmt3, fidx, (int)blobid);
    for (;;) {
      sqres = sqlite3_step(stmt3);
      if (sqres != SQLITE_ROW) {
        if (sqres != SQLITE_DONE) SetError();
        break;
      }
    }
    sqlite3_finalize(stmt3);
  }
}


//==========================================================================
//
//  SQArchive::CreateNewBlob
//
//==========================================================================
sqlite3_blob *SQArchive::CreateNewBlob (uint32_t blobSize, uint32_t *blobidptr) {
  IvanAssert(blobSize <= 0x70000000);
  *blobidptr = 0;

  uint32_t blobid = 0;
  sqlite3_stmt *stmt3 = 0;
  int fidx, sqres;

  if (sqlite3_prepare_v2(db,
        "INSERT INTO blobs "
        "      ( data) "
        "VALUES(:data) "
        "RETURNING blobid "
        "", -1, &stmt3, 0) != SQLITE_OK)
  {
    #ifdef SQA_DEBUG_WRITE
    ConLogf("SQA: cannot create new blob statement.");
    #endif
    SetError();
    return 0;
  }

  fidx = sqlite3_bind_parameter_index(stmt3, ":data");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":data");
  sqlite3_bind_zeroblob(stmt3, fidx, blobSize);
  for (;;) {
    sqres = sqlite3_step(stmt3);
    if (sqres == SQLITE_ROW) {
      blobid = sqlite3_column_int(stmt3, 0);
    } else {
      if (sqres != SQLITE_DONE) SetError();
      break;
    }
  }
  sqlite3_finalize(stmt3);
  if (wasError) return 0;
  #ifdef SQA_DEBUG_WRITE
  ConLogf("SQA: ...new blobid is %u", blobid);
  #endif
  if (blobid == 0) { SetError(); return 0; }

  sqlite3_blob *bb = 0;
  if (sqlite3_blob_open(db, "main", "blobs", "data", blobid, 1, &bb) != SQLITE_OK) {
    #ifdef SQA_DEBUG_WRITE
    ConLogf("SQA: cannot open new blob for wrtiting.");
    #endif
    DeleteBlob(blobid);
    SetError();
    return 0;
  }

  *blobidptr = blobid;
  return bb;
}


//==========================================================================
//
//  SQArchive::WriteRawFileData
//
//  return blobid
//
//==========================================================================
uint32_t SQArchive::WriteRawFileData (File *fl) {
  uint32_t blobid = 0;
  sqlite3_blob *bb = CreateNewBlob(fl->fsize, &blobid);
  if (bb) {
    const uint32_t chunkCount = ClusterCount(fl->fsize);
    IvanAssert(chunkCount != 0);
    IvanAssert(chunkCount <= fl->fatAlloced);
    int bofs = 0;
    for (uint32_t cidx = 0; cidx != chunkCount; cidx += 1) {
      void *src;
      ClusterInfo *cc = &fl->fatData[cidx];
      IvanAssert(cc->usize > 0 && cc->usize <= LZFF_CHUNK_SIZE);
      if (cc->psize != cc->usize) {
        if (!DecompressLZFF3(cc->packedData + 4, (int)cc->psize, fl->tempBuf, (int)cc->usize)) {
          ABORT("internal error: corrupted packed file data");
        }
        src = fl->tempBuf;
      } else {
        src = cc->packedData + 4;
      }
      if (sqlite3_blob_write(bb, src, (int)cc->usize, bofs) != SQLITE_OK) {
        #ifdef SQA_DEBUG_WRITE
        ConLogf("SQA: error writing new blob.");
        #endif
        sqlite3_blob_close(bb);
        DeleteBlob(blobid);
        SetError();
        return 0;
      }
      bofs += (int)cc->usize;
    }
    IvanAssert(bofs == (int)fl->fsize);
    if (sqlite3_blob_close(bb) != SQLITE_OK) {
      SetError();
      DeleteBlob(blobid);
    }
  }

  return (wasError ? 0 : blobid);
}


//==========================================================================
//
//  SQArchive::WritePackedFileData
//
//  return blobid
//
//==========================================================================
uint32_t SQArchive::WritePackedFileData (File *fl) {
  uint32_t blobid = 0;
  sqlite3_blob *bb = 0;
  uint32_t bsize;
  const uint32_t chunkCount = ClusterCount(fl->fsize);
  IvanAssert(chunkCount != 0);
  IvanAssert(chunkCount <= fl->fatAlloced);

  // count chunks
  bsize = 8u + (unsigned)LZFF_PKF_SIZE * chunkCount;

  uint8_t *dirData = (uint8_t *)::malloc(bsize);
  if (!dirData) ABORT("out of memory for directory data");
  memcpy(dirData + 4, &chunkCount, 4);

  // setup chunk offsets
  uint8_t *ccptr = dirData + 8;
  for (uint32_t cidx = 0; cidx != chunkCount; cidx += 1) {
    ClusterInfo *cc = &fl->fatData[cidx];
    cc->bofs = bsize;
    memcpy(ccptr, &cc->psize, LZFF_PKF_SIZE);
    ccptr += (unsigned)LZFF_PKF_SIZE;
    bsize += cc->psize + 4u/*crc*/;
  }

  *(uint32_t *)dirData = crc32_buf(dirData + 4, 4u + chunkCount * (unsigned)LZFF_PKF_SIZE);

  bb = CreateNewBlob(bsize, &blobid);
  if (bb) {
    // write chunk directory
    if (sqlite3_blob_write(bb, dirData, 8u + chunkCount * (unsigned)LZFF_PKF_SIZE, 0) != SQLITE_OK) {
      ::free(dirData);
      sqlite3_blob_close(bb);
      DeleteBlob(blobid);
      SetError();
      return 0;
    }
    ::free(dirData);

    // write chunk data
    for (uint32_t cidx = 0; cidx != chunkCount; cidx += 1) {
      ClusterInfo *cc = &fl->fatData[cidx];
      if (sqlite3_blob_write(bb, cc->packedData, (int)cc->psize + 4, (int)cc->bofs) != SQLITE_OK) {
        sqlite3_blob_close(bb);
        DeleteBlob(blobid);
        SetError();
        return 0;
      }
    }

    if (sqlite3_blob_close(bb) != SQLITE_OK) {
      SetError();
      DeleteBlob(blobid);
    }
  } else {
    ::free(dirData);
  }

  return (wasError ? 0 : blobid);
}


//==========================================================================
//
//  SQArchive::WriteFileData
//
//==========================================================================
void SQArchive::WriteFileData (File *fl) {
  IvanAssert(fl);
  if (wasError) return;

  if (fl->fsize <= 4000) fl->compLevel = 0;

  if (fl->fsize != 0) {
    IvanAssert(fl->fatAlloced != 0);
    IvanAssert(fl->fatData);
  }

  // if nothing was packed, write raw data
  if (fl->compLevel != 0) {
    const uint32_t chunkCount = ClusterCount(fl->fsize);
    IvanAssert(chunkCount != 0);
    IvanAssert(chunkCount <= fl->fatAlloced);
    uint32_t pksize = 8u + (unsigned)LZFF_PKF_SIZE * chunkCount;
    for (uint32_t cidx = 0; cidx != chunkCount; cidx += 1) {
      ClusterInfo *cc = &fl->fatData[cidx];
      pksize += cc->psize + 4u/*crc*/;
    }
    if (pksize >= fl->fsize || (pksize / 4000u == fl->fsize / 4000u)) {
      fl->compLevel = 0;
    } else {
      ConLogf("writing packed file '%s': size=%s; pksize=%s (%s cluster%s); ratio=%u%%",
              fl->fname, ComatozeU32(fl->fsize), ComatozeU32(pksize),
              ComatozeU32(chunkCount), (chunkCount != 1 ? "s" : ""),
              100u * pksize / fl->fsize);
    }
  }

  uint32_t blobid;
  if (fl->compLevel) {
    blobid = WritePackedFileData(fl);
  } else {
    ConLogf("writing raw file '%s': size=%s", fl->fname, ComatozeU32(fl->fsize));
    blobid = WriteRawFileData(fl);
  }

  if (blobid == 0) SetError(); // just in case

  if (wasError) return;

  // write file info
  sqlite3_stmt *stmt3 = 0;
  int fidx, sqres;

  if (sqlite3_prepare_v2(db,
        "SELECT blobid "
        "FROM fileinfo "
        "WHERE fname=:fname "
        "LIMIT 1 "
        "", -1, &stmt3, 0) != SQLITE_OK)
  {
    SetError();
    DeleteBlob(blobid);
    return;
  }

  fidx = sqlite3_bind_parameter_index(stmt3, ":fname");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":fname");
  sqlite3_bind_text(stmt3, fidx, fl->fname, -1, SQLITE_STATIC);

  uint32_t oldblobid = 0;
  for (;;) {
    int sqres = sqlite3_step(stmt3);
    if (sqres == SQLITE_ROW) {
      oldblobid = sqlite3_column_int(stmt3, 0);
    } else {
      if (sqres != SQLITE_DONE) SetError();
      break;
    }
  }
  sqlite3_finalize(stmt3);

  if (wasError) {
    DeleteBlob(blobid);
    return;
  }

  if (sqlite3_prepare_v2(db,
        "INSERT INTO fileinfo "
        "      ( wtime, blobid, fsize, chunksize, fname) "
        "VALUES(:wtime,:blobid,:fsize,:chunksize,:fname) "
        "ON CONFLICT(fname) DO UPDATE "
        "  SET wtime=:wtime, blobid=:blobid "
        "    , fsize=:fsize, chunksize=:chunksize "
        "", -1, &stmt3, 0) != SQLITE_OK)
  {
    #ifdef SQA_DEBUG_WRITE
    ConLogf("SQA: cannot create fileinfo insert statement.");
    #endif
    SetError();
    return;
  }

  // wtime
  fidx = sqlite3_bind_parameter_index(stmt3, ":wtime");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":wtime");
  sqlite3_bind_int(stmt3, fidx, (int)fl->wtime);
  // blobid
  fidx = sqlite3_bind_parameter_index(stmt3, ":blobid");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":blobid");
  sqlite3_bind_int(stmt3, fidx, (int)blobid);
  // fsize
  fidx = sqlite3_bind_parameter_index(stmt3, ":fsize");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":fsize");
  sqlite3_bind_int(stmt3, fidx, (int)fl->fsize);
  // chunksize
  fidx = sqlite3_bind_parameter_index(stmt3, ":chunksize");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":chunksize");
  sqlite3_bind_int(stmt3, fidx, (int)(fl->compLevel ? LZFF_CHUNK_SIZE : 0));
  // fname
  fidx = sqlite3_bind_parameter_index(stmt3, ":fname");
  if (fidx == 0) ABORT("SQLite archive binding `%s` not found!", ":fname");
  sqlite3_bind_text(stmt3, fidx, fl->fname, -1, SQLITE_STATIC);
  for (;;) {
    sqres = sqlite3_step(stmt3);
    if (sqres != SQLITE_ROW) {
      if (sqres != SQLITE_DONE) SetError();
      break;
    }
  }
  sqlite3_finalize(stmt3);
  if (wasError) return;

  DeleteBlob(oldblobid);
}


//==========================================================================
//
//  SQArchive::FileClose
//
//==========================================================================
void SQArchive::FileClose (SQAFile fd) {
  File *fl = GetFI(fd);
  if (!fl || fl->blobId == 0) return;

  if (fl->blob) {
    sqlite3_blob_close(fl->blob);
  } else if (!wasError && fl->blobId == ~(uint32_t)0) {
    #ifdef SQA_DEBUG_WRITE
    ConLogf("SQA: writing file #%u (%s); size=%s", fdu32(fd), fl->fname, ComatozeU32(fl->fsize));
    #endif
    // write accumulated file
    if (!inTransaction) {
      #ifdef SQA_DEBUG_WRITE
      ConLogf("SQA: ...beginning automatic transaction.");
      #endif
      if (sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
        SetError();
      }
    }

    #if 0
    ConLogf("saving '%s' (comp=%d; fsize=%s).", fl->fname, fl->compLevel, ComatozeU32(fl->fsize));
    #endif
    FlushDirtyBuffer(fl);
    if (!wasError) {
      WriteFileData(fl);
    }

    if (!inTransaction) {
      if (wasError) {
        #ifdef SQA_DEBUG_WRITE
        ConLogf("SQA: ...rolling back automatic transaction.");
        #endif
        sqlite3_exec(db, "ROLLBACK TRANSACTION", NULL, NULL, NULL);
      } else {
        #ifdef SQA_DEBUG_WRITE
        ConLogf("SQA: ...commiting automatic transaction.");
        #endif
        if (sqlite3_exec(db, "COMMIT TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
          SetError();
        }
      }
    }
  }

  // free clusters
  for (uint32_t cidx = 0; cidx != fl->fatAlloced; cidx += 1) {
    ::free(fl->fatData[cidx].packedData);
  }

  // free buffers
  if (fl->tempBuf) { ::free(fl->tempBuf); fl->tempBuf = 0; }
  if (fl->buffer.data) { ::free(fl->buffer.data); fl->buffer.data = 0; }
  if (fl->fatData) { ::free(fl->fatData); fl->fatData = 0; }

  ::free(fl->fname);
  fl->fname = 0;
  fl->blob = 0;
  fl->blobId = 0;
}


//==========================================================================
//
//  SQArchive::FileSize
//
//  <0 on error.
//
//==========================================================================
int SQArchive::FileSize (SQAFile fd) {
  File *fl = GetFI(fd);
  if (!fl || fl->blobId == 0) { SetError(); return -1; }
  return (int)fl->fsize;
}


//==========================================================================
//
//  SQArchive::FileTell
//
//  <0 on error.
//
//==========================================================================
int SQArchive::FileTell (SQAFile fd) {
  File *fl = GetFI(fd);
  if (!fl || fl->blobId == 0) { SetError(); return -1; }
  return (int)fl->fpos;
}


//==========================================================================
//
//  SQArchive::FileSeek
//
//==========================================================================
void SQArchive::FileSeek (SQAFile fd, int newpos) {
  File *fl = GetFI(fd);
  if (!fl || fl->blobId == 0) { SetError(); return; }
  if (newpos < 0 || newpos > (int)fl->fsize) { SetError(); return; }
  fl->fpos = (uint32_t)newpos;
}


//==========================================================================
//
//  SQArchive::FileRead
//
//  read exactly this number of bytes.
//
//==========================================================================
void SQArchive::FileRead (SQAFile fd, void *buf, size_t len) {
  File *fl = GetFI(fd);
  if (wasError || !fl || fl->blobId == 0) {
    SetError();
    if (buf && len) memset(buf, 0, len);
    return;
  }
  if (len == 0) return;
  if (len > 0x40000000) { SetError(); return; }
  if (!buf) { SetError(); return; }
  #ifdef SQA_DEBUG_READ
  ConLogf("SQA: reading file '%s' (size=%s; pos=%s; len=%s)",
          fl->fname, ComatozeU32(fl->fsize), ComatozeU32(fl->fpos),
          ComatozeU32((unsigned)len));
  #endif
  if (fl->fsize - fl->fpos < len) {
    SetError();
    memset(buf, 0, len);
    return;
  }

  uint8_t *dest = (uint8_t *)buf;
  while (len != 0 && !wasError) {
    if (fl->blob) {
      ReadCluster(fl, fl->fpos);
    } else if (fl->fpos >= fl->fsize) {
      SetError();
    } else {
      WriterSeekAndUnpack(fl, fl->fpos);
    }
    if (!wasError) {
      const uint32_t bpos = fl->fpos - fl->buffer.fofs;
      uint32_t toCopy = fl->buffer.used - bpos;
      IvanAssert(toCopy > 0 && toCopy <= LZFF_CHUNK_SIZE);
      if (toCopy > len) toCopy = (uint32_t)len;
      memcpy(dest, fl->buffer.data + bpos, toCopy);
      dest += toCopy;
      len -= toCopy;
      fl->fpos += toCopy;
    } else {
      memset(dest, 0, len);
    }
  }
}


//==========================================================================
//
//  SQArchive::FileWrite
//
//  write exactly this number of bytes.
//
//  FIXME: we can use temp files here!
//
//==========================================================================
void SQArchive::FileWrite (SQAFile fd, const void *buf, size_t len) {
  File *fl = GetFI(fd);
  if (wasError || !fl || fl->blobId != ~(uint32_t)0 || fl->blob) { SetError(); return; }
  if (len == 0) return;
  if (!buf) { SetError(); return; }
  if (len > 0x20000000) { SetError(); return; }
  if (fl->fpos + len > 0x20000000) { SetError(); return; }
  if (fl->fpos > fl->fsize) { SetError(); return; }

  const uint8_t *src = (const uint8_t *)buf;
  while (len != 0 && !wasError) {
    WriterSeekAndUnpack(fl, fl->fpos);
    if (!wasError) {
      IvanAssert(fl->buffer.data);
      const uint32_t bpos = fl->fpos - fl->buffer.fofs;
      IvanAssert(bpos < LZFF_CHUNK_SIZE);
      uint32_t left = LZFF_CHUNK_SIZE - bpos;
      IvanAssert(left > 0 && left <= LZFF_CHUNK_SIZE);
      if (left > len) left = (uint32_t)len;
      memcpy(fl->buffer.data + bpos, src, left);
      const uint32_t newsz = bpos + left;
      IvanAssert(newsz <= LZFF_CHUNK_SIZE);
      fl->buffer.used = Max(fl->buffer.used, newsz);
      fl->buffer.dirty = true;
      src += left;
      len -= left;
      fl->fpos += left;
      fl->fsize = Max(fl->fsize, fl->fpos);
    }
  }
}


//==========================================================================
//
//  SQArchive::FilePrintf
//
//==========================================================================
void SQArchive::FilePrintf (SQAFile fd, const char *fmt, ...) {
  static char buf[16384];
  File *fl = GetFI(fd);
  if (wasError || !fl || fl->blobId == 0 || fl->blob) { SetError(); return; }
  char *bufptr = buf;
  int bufsz = (int)sizeof(buf) - 1;
  for (;;) {
    va_list ap;
    int n;
    char *np;
    va_start(ap, fmt);
    n = vsnprintf(bufptr, bufsz, fmt, ap);
    va_end(ap);
    if (n > -1 && n < bufsz) break;
    if (n < -1) n = bufsz + 4096;
    if (bufptr == buf) {
      np = (char *)::malloc((size_t)n + 1u);
    } else {
      np = (char *)::realloc(bufptr, (size_t)n + 1u);
    }
    if (np == NULL) ABORT("out of memory for sqa printf");
  }
  FileWrite(fd, buf, strlen(buf));
  if (bufptr != buf) free(bufptr);
}
