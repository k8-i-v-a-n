/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef FELIB_SQ3SUPPORT_HEADER
#define FELIB_SQ3SUPPORT_HEADER

#include "felibdef.h"
#include "sqlite3.h"
#include "festring.h"
#include "feerror.h"


class SQ3Stmt;

class SQ3DB {
  friend class SQ3Stmt;
  friend class StmtIntr;

private:
  class StmtIntr {
    friend class SQ3DB;
    friend class SQ3Stmt;
  private:
    SQ3DB *owner;
    StmtIntr *next;
    sqlite3_stmt *stmt;
    uint32_t refc;
  public:
    StmtIntr () : owner(0), next(0), stmt(0), refc(0) {}
    StmtIntr (const StmtIntr &) = delete;
    StmtIntr &operator = (const StmtIntr &) = delete;
  };

private:
  sqlite3 *db;
  StmtIntr *stlist;

private:
  void SetupPragmas ();
  void SetupROPragmas ();

  void AppendStmt (StmtIntr *stx);
  // this also closes it
  void RemoveStmt (StmtIntr *stx);

public:
  // no copies!
  SQ3DB (const SQ3DB &) = delete;
  SQ3DB &operator = (const SQ3DB &) = delete;

  inline SQ3DB () : db(0), stlist(0) {}
  SQ3DB (const char *fname, bool readWrite, bool allowCreate=true);

  inline bool IsOpen () const { return !!db; }
  inline bool IsRW () { return (db ? !sqlite3_db_readonly(db, NULL) : false); }

  void Close ();

  bool OpenRO (const char *fname);
  bool OpenRW (const char *fname);
  bool OpenRWCreate (const char *fname);

  inline bool OpenRO (const festring &fname) { return OpenRO(fname.CStr()); }
  inline bool OpenRW (const festring &fname) { return OpenRW(fname.CStr()); }
  inline bool OpenRWCreate (const festring &fname) { return OpenRWCreate(fname.CStr()); }

  bool Stmt (SQ3Stmt &stmt, const char *sql);
  inline bool Stmt (SQ3Stmt &stmt, const festring &sql) { return Stmt(stmt, sql.CStr()); }

  bool Exec (const char *sql);
  inline bool Exec (const festring &sql) { return Exec(sql.CStr()); }

  bool BeginTransaction () { return Exec("BEGIN TRANSACTION"); }
  bool AbortTransaction () { return Exec("ROLLBACK TRANSACTION"); }
  bool CommitTransaction () { return Exec("COMMIT TRANSACTION"); }
};


class SQ3Stmt {
  friend class SQ3DB;
private:
  SQ3DB::StmtIntr *stmt;

private:
  void IncRef () const noexcept { if (stmt) stmt->refc += 1; }

  void DecRef (SQ3DB::StmtIntr *stx) {
    if (stx && --(stx->refc) == 0) {
      if (stx->owner) {
        stx->owner->RemoveStmt(stx);
      }
      delete stx;
    }
  }

  inline sqlite3_stmt *Stmt3 () const noexcept { return (stmt ? stmt->stmt : 0); }

private:
  int FindParam (const char *param);
  int FindColumn (const char *name);

public:
  inline SQ3Stmt () : stmt(0) {}
  inline SQ3Stmt (const SQ3Stmt &src) : stmt(0) { operator=(src); }

  inline ~SQ3Stmt () { Close(); }

  inline SQ3Stmt &operator = (const SQ3Stmt &src) {
    if (src.stmt) {
      src.IncRef();
      DecRef(stmt);
      stmt = src.stmt;
    } else {
      DecRef(stmt);
      stmt = 0;
    }
    return *this;
  }

  // DO NOT USE! add required API instead!
  //inline sqlite3_stmt *GetSQ3Statement () const { return (stmt ? stmt->Stmt() : 0); }

  inline void Close () { DecRef(stmt); stmt = 0; }

  inline bool IsOpen () { return (stmt && stmt->owner && Stmt3()); }

  // reset to initial state, but keep bindings
  bool Reset () {
    if (Stmt3()) {
      if (sqlite3_reset(Stmt3()) == SQLITE_OK) return true;
    }
    return false;
  }

  // value binding
  void BindNull (const char *fld) {
    const int bidx = FindParam(fld);
    sqlite3_bind_null(Stmt3(), bidx);
  }

  void BindInt (const char *fld, int value) {
    const int bidx = FindParam(fld);
    sqlite3_bind_int(Stmt3(), bidx, value);
  }

  void BindInt64 (const char *fld, int64_t value) {
    const int bidx = FindParam(fld);
    sqlite3_bind_int64(Stmt3(), bidx, value);
  }

  void BindDouble (const char *fld, double value) {
    const int bidx = FindParam(fld);
    sqlite3_bind_double(Stmt3(), bidx, value);
  }

  void BindZeroBlob (const char *fld, int size) {
    IvanAssert(size >= 0 && size <= 0x40000000);
    const int bidx = FindParam(fld);
    sqlite3_bind_zeroblob(Stmt3(), bidx, size);
  }

  void BindConstBlob (const char *fld, const void *blob, int size) {
    IvanAssert(size >= 0 && size <= 0x40000000);
    const int bidx = FindParam(fld);
    if (blob) {
      sqlite3_bind_blob(Stmt3(), bidx, blob, size, SQLITE_STATIC);
    } else {
      sqlite3_bind_zeroblob(Stmt3(), bidx, size);
    }
  }

  void BindBlob (const char *fld, const void *blob, int size) {
    IvanAssert(size >= 0 && size <= 0x40000000);
    const int bidx = FindParam(fld);
    if (blob) {
      sqlite3_bind_blob(Stmt3(), bidx, blob, size, SQLITE_TRANSIENT);
    } else {
      sqlite3_bind_zeroblob(Stmt3(), bidx, size);
    }
  }

  void BindConstText (const char *fld, const char *text) {
    const int bidx = FindParam(fld);
    if (text) {
      sqlite3_bind_text(Stmt3(), bidx, text, -1, SQLITE_STATIC);
    } else {
      sqlite3_bind_text(Stmt3(), bidx, "", 0, SQLITE_STATIC);
    }
  }

  void BindText (const char *fld, const char *text) {
    const int bidx = FindParam(fld);
    if (text) {
      sqlite3_bind_text(Stmt3(), bidx, text, -1, SQLITE_TRANSIENT);
    } else {
      sqlite3_bind_text(Stmt3(), bidx, "", 0, SQLITE_STATIC);
    }
  }

  void BindText (const char *fld, const festring &text) {
    const int bidx = FindParam(fld);
    if (!text.IsEmpty()) {
      sqlite3_bind_text(Stmt3(), bidx, text.CStr(), text.Length(), SQLITE_TRANSIENT);
    } else {
      sqlite3_bind_text(Stmt3(), bidx, "", 0, SQLITE_STATIC);
    }
  }

  void BindTextNormSpaces (const char *fld, const festring &text) {
    festring s = text.NormalizeSpaces(true);
    BindText(fld, s);
  }

  // result retrieve
  int GetInt (const char *fld) {
    const int cidx = FindColumn(fld);
    return sqlite3_column_int(Stmt3(), cidx);
  }

  int64_t GetInt64 (const char *fld) {
    const int cidx = FindColumn(fld);
    return sqlite3_column_int64(Stmt3(), cidx);
  }

  double GetDouble (const char *fld) {
    const int cidx = FindColumn(fld);
    return sqlite3_column_double(Stmt3(), cidx);
  }

  festring GetText (const char *fld) {
    const int cidx = FindColumn(fld);
    const char *tx = (const char *)sqlite3_column_text(Stmt3(), cidx);
    const int len = sqlite3_column_bytes(Stmt3(), cidx);
    if (len > 0) {
      return festring::CreateOwned(tx, (festring::sizetype)len);
    } else {
      return festring();
    }
  }

  // stepping
  // <0: error; =0: done; >0: got row
  // this also resets finished statement
  int Step () {
    if (!Stmt3()) return -1;
    int sqres = sqlite3_step(Stmt3());
    if (sqres == SQLITE_ROW) return 1;
    if (sqres == SQLITE_DONE) {
      if (sqlite3_reset(Stmt3()) == SQLITE_OK) {
        return 0;
      }
    }
    return -1;
  }

  // this also resets finished statement
  bool Exec () {
    int res = 0;
    do { res = Step(); } while (res > 0);
    return (res == 0);
  }
};


// ////////////////////////////////////////////////////////////////////////// //
// sqlite file archive, used for saves and bones
// written files are accumulated in memory first, and flushed to the db
// on closing the file (not the db!). the writer tries to keep file chunks
// compressed, so for IVAN it is ok (the usual level compression ratio is ~x10).
// note that file names are case-sensitive.
// ////////////////////////////////////////////////////////////////////////// //

typedef struct SQAFile_t *SQAFile;

class SQArchive {
public:
  static const int COMP_DEFAULT = 2;

protected:
  struct Buffer {
    uint8_t *data; // 64K
    uint32_t used; // number of used bytes in `data`
    uint32_t fofs; // file offset of this buffer
    bool dirty;
  };

  struct ClusterInfo {
    uint32_t bofs; // offset in blob
    uint32_t usize;
    uint32_t psize;
    uint8_t *packedData; // (for writer)
  };
  //static_assert(sizeof(ClusterInfo) == 16, "bad `ClusterInfo` size");

  struct File {
    sqlite3_blob *blob; // for reading; NULL if writing
    uint32_t blobId; // 0: unused; ~0: writing
    char *fname;
    uint32_t wtime;
    uint32_t fpos; // current file position
    uint32_t fsize; // current file size
    // for unpacked files, no cluster info will be created.
    // thus, if `first == NULL`, and the file is opened for reading, it is unpacked.
    // for writer
    Buffer buffer; // current buffer
    uint8_t *tempBuf; // 64K temp buffer for unpacking
    ClusterInfo *fatData; // for reader: the whole chunk directory
    uint32_t fatAlloced; // for writer
    // for writer
    int32_t compLevel;
  };

protected:
  // read new cluster if necessary
  void ReadCluster (File *fl, uint32_t fofs);

  void PackCurrentBuffer (File *fl);

  void FlushDirtyBuffer (File *fl);

  // writing is done by one cluster.
  // when the cluster is finished, it is packed.
  // on seeking, the cluster is unpacked to buffer.
  void WriterSeekAndUnpack (File *fl, uint32_t fofs);
  // pack current buffer, update its cluster info.
  void WriterPack (File *fl);

  SQAFile OpenPackedFile (const char *fname, sqlite3_blob *bb, uint32_t fsize);

protected:
  sqlite3 *db;
  bool inTransaction;
  bool wasError;
  File *files;
  uint32_t filesAlloced;

  SQAFile AllocFD ();

  static FORCE_INLINE uint32_t fdu32 (const SQAFile fd) noexcept {
    return (uint32_t)(uintptr_t)fd;
  }

  static FORCE_INLINE SQAFile u32fd (const uint32_t fd) noexcept {
    return (fd ? (SQAFile)fd : nullptr);
  }

  FORCE_INLINE File *GetFI (const SQAFile fd) noexcept {
    return (db && fd && fdu32(fd) <= filesAlloced ? &files[fdu32(fd) - 1u] : nullptr);
  }

  sqlite3_blob *CreateNewBlob (uint32_t blobSize, uint32_t *blobidptr);
  void DeleteBlob (uint32_t blobid);

  // return blobid
  uint32_t WriteRawFileData (File *fl);
  // return blobid
  uint32_t WritePackedFileData (File *fl);

  void WriteFileData (File *fl);

public:
  SQArchive ();
  ~SQArchive ();

  // no copies
  SQArchive (const SQArchive &) = delete;
  SQArchive &operator = (const SQArchive &) = delete;

  FORCE_INLINE bool WasError () const noexcept { return wasError; }
  FORCE_INLINE bool IsOpen () const noexcept { return !!db; }

  void SetError ();
  FORCE_INLINE void ResetError () noexcept { wasError = false; }

  // execute `VACUUM` SQLite instruction.
  void Vacuum ();

  // open SQLite archive; if `allowCreate` is `true`,
  // the archive will be created if absent.
  // set error flag on error. use `IsOpen()` to check for success.
  void Open (cfestring &filename, bool allowCreate);
  // close SQLite archive; all files and transactions should be closed.
  // set error flag on error.
  void Close ();

  // *WARNING*: transactions are not recursive!
  FORCE_INLINE bool InTransaction () const noexcept { return inTransaction; }

  void OpenTransaction ();
  void CloseTransaction ();
  void AbortTransaction ();

  // check if
  // set error flag on error.
  bool FileExists (cfestring &fname);

  // return `true` if the file was present.
  // set error flag on error.
  bool FileDelete (cfestring &fname);

  // open file for reading only.
  // return 0 on error, !0 on success.
  // attempt to open non-existing file will not set error flag,
  // but any other error will do.
  SQAFile FileOpen (cfestring &fname);

  // open file for reading and writing.
  // return 0 on error, !0 on success.
  // will replace existing file.
  // `compLevel` is `[0..3]`. use `COMP_DEFAULT` if in doubt.
  // `0` means "uncompressed", and you should *NEVER* use it:
  // the writer compress file data on the fly, *and* it keeps
  // all compressed data in RAM up until `FileClose()` call.
  // this is because we need to know the blob size before
  // writing it. in uncompressed mode, the writer will keep
  // uncompressed data (obviously), using a lot of RAM for nothing.
  // note that compression is quite fast, so you can use default
  // compression all the time. using maximum compression is not
  // recommended, because it is almost 2 times slower, but will
  // improve ratio by 0.01% or something.
  // note that seeking is supported for such files, but it is
  // quite slow due to data recompression. IVAN doesn't seek.
  SQAFile FileCreate (cfestring &fname, int compLevel=COMP_DEFAULT);

  // close file.
  // set error flag on error.
  void FileClose (SQAFile fd);

  // get file size. return negative value on error.
  // set error flag on error.
  int FileSize (SQAFile fd);

  // get current file position. return negative value on error.
  // set error flag on error.
  int FileTell (SQAFile fd);

  // set current file position. note that invalid position is
  // error. allowed positions are `[0..FileSize()]`.
  // set error flag on error.
  void FileSeek (SQAFile fd, int newpos);

  // read exactly this number of bytes.
  // reading `0` bytes is not an error.
  // set error flag on error.
  void FileRead (SQAFile fd, void *buf, size_t len);

  // write exactly this number of bytes.
  // writing `0` bytes is not an error.
  // set error flag on error.
  void FileWrite (SQAFile fd, const void *buf, size_t len);

  // the usual `fprintf()`.
  // set error flag on error.
  LIKE_PRINTF(3, 4) void FilePrintf (SQAFile fd, const char *fmt, ...);
};


#endif
