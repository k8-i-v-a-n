/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_V2_H__
#define __FELIB_V2_H__

#include "felibdef.h"
#include "fejoaat.h"
#include <cstdlib>
#include <unordered_set>


struct v2;

struct packv2 {
  FORCE_INLINE operator v2 () const noexcept;
  short X = 0, Y = 0;
};


/* Standard structure for representing positions */
struct v2 {
public:
  int X, Y;

public:
  FORCE_INLINE v2 () noexcept : X(0), Y(0) {}
  FORCE_INLINE v2 (const v2 &V) noexcept : X(V.X), Y(V.Y) {}
  FORCE_INLINE v2 (int X, int Y) noexcept : X(X), Y(Y) {}
  FORCE_INLINE v2 &operator = (const v2 &V) noexcept { X = V.X; Y = V.Y; return *this; }
  FORCE_INLINE v2 operator + (const v2 &V) const noexcept { return v2(X+V.X, Y+V.Y); }
  FORCE_INLINE v2 &operator += (const v2 &V) noexcept { X += V.X; Y += V.Y; return *this; }
  FORCE_INLINE v2 operator - (const v2 &V) const noexcept { return v2(X-V.X, Y-V.Y); }
  FORCE_INLINE v2 &operator -= (const v2 &V) noexcept { X -= V.X; Y -= V.Y; return *this; }
  FORCE_INLINE v2 operator - () const noexcept { return v2(-X, -Y); }
  FORCE_INLINE v2 operator * (int I) const noexcept { return v2(X*I, Y*I); }
  FORCE_INLINE v2 &operator *= (int I) noexcept { X *= I; Y *= I; return *this; }
  FORCE_INLINE v2 operator / (int I) const noexcept { return v2(X/I, Y/I); }
  FORCE_INLINE v2 &operator /= (int I) noexcept { X /= I; Y /= I; return *this; }
  FORCE_INLINE v2 operator * (double D) const noexcept { return v2(int(X*D), int(Y*D)); }
  FORCE_INLINE v2 &operator *= (double D) noexcept { X = int(X*D); Y = int(Y*D); return *this; }
  FORCE_INLINE v2 operator / (double D) const noexcept { return v2(int(X/D), int(Y/D)); }
  FORCE_INLINE v2 &operator /= (double D) noexcept { X = int(X/D); Y = int(Y/D); return *this; }
  FORCE_INLINE bool operator == (v2 V) const noexcept { return X == V.X && Y == V.Y; }
  FORCE_INLINE bool operator != (v2 V) const noexcept { return X != V.X || Y != V.Y; }
  FORCE_INLINE bool operator < (v2 V) const noexcept { return (X < V.X || (X == V.X && Y < V.Y)); }
  FORCE_INLINE bool operator <= (v2 V) const noexcept { return (operator<(*this) || operator==(*this)); }
  FORCE_INLINE bool operator > (v2 V) const noexcept { return !operator<=(*this); }
  FORCE_INLINE bool operator >= (v2 V) const noexcept { return (operator>(*this) || operator==(*this)); }
  FORCE_INLINE v2 operator << (int S) const noexcept { return v2(X << S, Y << S); }
  FORCE_INLINE v2 &operator <<= (int S) noexcept { X <<= S; Y <<= S; return *this; }
  FORCE_INLINE v2 operator >> (int S) const noexcept { return v2(X >> S, Y >> S); }
  FORCE_INLINE v2 &operator >>= (int S) noexcept { X >>= S; Y >>= S; return *this; }
  FORCE_INLINE int GetLengthSquare () const noexcept { return X * X + Y * Y; }
  /* Also returns true if V == *this */
  FORCE_INLINE truth IsAdjacent (v2 V) const noexcept { return (abs(V.X - X) <= 1 && abs(V.Y - Y) <= 1); }
  FORCE_INLINE int GetManhattanLength () const noexcept { return Max(abs(X), abs(Y)); }
  FORCE_INLINE int GetSquaredLength () const noexcept { return HypotSquare(X, Y); }
  FORCE_INLINE int GetSquaredDistance (const v2 &other) const noexcept { return HypotSquare(other.X - X, other.Y - Y); }
  FORCE_INLINE int GetManhattanDistance (const v2 &other) const noexcept { return Max(abs(other.X - X), abs(other.Y - Y)); }
  FORCE_INLINE truth IsZero () const noexcept { return (X == 0 && Y == 0); }
  FORCE_INLINE truth IsOrtho () const noexcept { return ((X == 0) != (Y != 0)); }
  FORCE_INLINE truth IsOrthoOne () const noexcept { return ((X == 0 && abs(Y) == 1) || (Y == 0 && abs(X) == 1)); }
  FORCE_INLINE truth IsDirOne () const noexcept { return (Max(abs(X), abs(Y)) == 1); } // (0,0) is not valid
  FORCE_INLINE operator packv2 () const noexcept {
    packv2 V = { (short)X, (short)Y };
    return V;
  }
  //  v2 Randomize() const; Would be a good idea.

  FORCE_INLINE uint32_t Hash () const noexcept {
    uint32_t h = joaatHashBufPart(&X, sizeof(X), 0x29a);
    h = joaatHashBufPart(&Y, sizeof(Y), h);
    return joaatHashBufFinish(h);
  }

  /*
   * Rotates a position Vect of a square map of size
   * Size x Size according to Flags (see felibdef.h)
   */
  void Rotate (/*v2 &Vect, */int Size, int Flags) noexcept {
    cint Limit = Size - 1;
    if (Flags & ROTATE) {
      cint T = this->X;
      this->X = Limit - this->Y;
      this->Y = T;
    }
    if (Flags & MIRROR) this->X = Limit - this->X;
    if (Flags & FLIP) this->Y = Limit - this->Y;
  }
};

FORCE_INLINE packv2::operator v2 () const noexcept { return v2(X, Y); }

MAKE_STD_HASHER(v2)

/*
cv2 ZERO_V2(0, 0);
cv2 ERROR_V2(-0x8000, -0x8000);
cv2 ABORT_V2(-0x7FFF, -0x7FFF);
*/

#define ZERO_V2   (v2(0, 0))
#define ERROR_V2  (v2(-0x8000, -0x8000))
#define ABORT_V2  (v2(-0x7FFF, -0x7FFF))


#endif
