/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include "vwfileio.h"
#include "feerror.h"
#include "femath.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>


//#define VW_DEBUG_SAVE_WRITER

#define MAX_OPENED_FILES  (128)
#define MAX_OPENED_ARCHIVES  (128)


vwad_bool vw_archives_first = 1;

enum Mode {
  Closed = 0,
  Read = 1,
};


struct VFileInfo {
  // for reading
  FILE *fl;
  vwad_fd vfd;
  vwad_handle *archive;
  // actual mode
  Mode mode;
};


struct VArchiveInfo {
  FILE *fl;
  vwad_iostream strm;
  vwad_handle *arch;
};


// #0 is never used
static VFileInfo files[MAX_OPENED_FILES] = {0};

static VArchiveInfo archives[MAX_OPENED_ARCHIVES];
static int archive_count = 0;

static char mybindir[8192] = {0};


//==========================================================================
//
//  ClearFileInfo
//
//==========================================================================
static inline void ClearFileInfo (VFileInfo *nfo) {
  memset(nfo, 0, sizeof(*nfo));
  nfo->mode = Closed; // just in case, lol
}


//==========================================================================
//
//  vw_set_bin_dir
//
//==========================================================================
void vw_set_bin_dir (const char *bindir) {
  if (!bindir || !bindir[0]) {
    strcpy(mybindir, "./");
  } else if (strlen(bindir) > 4096) {
    ABORT("invalid binary dir");
  } else {
    strcpy(mybindir, bindir);
    #ifdef SHITDOZE
    if (mybindir[0]) strcat(mybindir, "\\");
    #else
    if (mybindir[0]) strcat(mybindir, "/");
    #endif
  }
}


//==========================================================================
//
//  vw_is_absolute_path
//
//==========================================================================
int vw_is_absolute_path (const char *fname) {
  if (!fname || !fname[0]) return 0;
  #ifdef SHITDOZE
  if (fname[0] == '/' || fname[0] == '\\') return 1;
  if (fname[0] == '.') {
    return (fname[1] == '/' || fname[1] == '\\');
  }
  if (fname[1] == ':') return 1;
  return 0;
  #else
  return (fname[0] == '/' || (fname[0] == '.' && fname[1] == '/'));
  #endif
}


//==========================================================================
//
//  xf_seek
//
//  return non-zero on failure; failure can be delayed
//  will never be called with negative `pos`
//
//==========================================================================
static vwad_result xf_seek (vwad_iostream *strm, int pos) {
  FILE *fl = (FILE *)strm->udata;
  if (fseek(fl, pos, SEEK_SET) != 0) return -1;
  return 0;
}


//==========================================================================
//
//  xf_read
//
//  read *exactly* bufsize bytes; return 0 on success, negative on failure
//  will never be called with zero or negative `bufsize`
//
//==========================================================================
static vwad_result xf_read (vwad_iostream *strm, void *buf, int bufsize) {
  FILE *fl = (FILE *)strm->udata;
  if (fread(buf, (size_t)bufsize, 1, fl) != 1) return -1;
  return 0;
}


//==========================================================================
//
//  vw_add_archive
//
//==========================================================================
int vw_add_archive (const char *fname) {
  if (archive_count == MAX_OPENED_ARCHIVES) return -1;
  if (!fname || !fname[0]) return -1;
  FILE *fl = fopen(fname, "rb");
  if (!fl) return -1;
  archives[archive_count].fl = fl;
  archives[archive_count].strm.seek = &xf_seek;
  archives[archive_count].strm.read = &xf_read;
  archives[archive_count].strm.udata = fl;
  vwad_handle *arch = vwad_open_archive(&archives[archive_count].strm,
                                        VWAD_OPEN_NO_MAIN_COMMENT, NULL);
  if (!arch) {
    fclose(fl);
    return -1;
  }
  archives[archive_count].arch = arch;
  archive_count += 1;
  // check signature
  if (vwad_is_authenticated(arch) && vwad_has_pubkey(arch)) {
    vwad_public_key pubkey;
    if (vwad_get_pubkey(arch, pubkey) == VWAD_OK) {
      vwad_public_key outkey;
      if (vwad_z85_decode_key("mHYS^gckt0?f&7663u!Zz=zcNkFZPtFxEl*7Dh(4>?N^>", outkey) == VWAD_OK) {
        if (memcmp(pubkey, outkey, sizeof(pubkey)) == 0) {
          ConLogf("added genuine game archive '%s'.", fname);
          return 1;
        }
      }
    }
  }
  ConLogf("added game archive '%s'.", fname);
  return 0;
}


//==========================================================================
//
//  AllocFileId
//
//==========================================================================
static int AllocFileId () {
  int fid = 1;
  while (fid != MAX_OPENED_FILES && files[fid].mode != Closed) fid += 1;
  if (fid == MAX_OPENED_FILES) fid = 0;
  return fid;
}


static int vw_debug_prints = 1;

//==========================================================================
//
//  vwopen_disk
//
//  strictly for reading
//
//==========================================================================
VFile vwopen_disk (const char *fname) {
  if (!fname || !fname[0]) return 0;
  static char xfname[8192];
  if (vw_is_absolute_path(fname)) {
    strcpy(xfname, fname);
  } else {
    strcpy(xfname, mybindir);
    strcat(xfname, fname);
  }
  FILE *fl = fopen(xfname, "rb");
  if (!fl) return 0;
  int fd = AllocFileId();
  if (fd == 0) { fclose(fl); return 0; }
  files[fd].fl = fl;
  files[fd].vfd = -1;
  files[fd].archive = 0;
  files[fd].mode = Read;
  return fd;
}


//==========================================================================
//
//  vwopen_arch
//
//==========================================================================
VFile vwopen_arch (const char *fname) {
  if (!fname || !fname[0]) return 0;
  int vfd = AllocFileId();
  if (vfd == 0) return 0;
  int aidx = archive_count;
  while (aidx != 0) {
    aidx -= 1;
    vwad_fd xfd = vwad_open_file(archives[aidx].arch, fname);
    if (xfd >= 0) {
      #if 0
      ConLogf("FOUND '%s' in archive #%d", fname, aidx);
      #endif
      files[vfd].fl = 0;
      files[vfd].vfd = xfd;
      files[vfd].archive = archives[aidx].arch;
      files[vfd].mode = Read;
      return vfd;
    } else {
      #if 0
      ConLogf("***NOT FOUND '%s' in archive #%d", fname, aidx);
      #endif
    }
  }
  return 0;
}


//==========================================================================
//
//  vwopen
//
//  strictly for reading
//
//==========================================================================
VFile vwopen (const char *fname) {
  if (!fname || !fname[0]) return 0;

  VFile fd = 0;
  if (vw_archives_first) fd = vwopen_arch(fname);
  if (!fd) fd = vwopen_disk(fname);
  if (!fd && !vw_archives_first) fd = vwopen_arch(fname);

  #if 0
  if (vw_debug_prints && fd) {
    ConLogf("OPENED '%s' %s.",
            fname, (files[fd].vfd >= 0 ? "in archive" : "on disk"));
  }
  #else
  # ifndef SHITDOZE
    if (vw_debug_prints && vw_archives_first && fd && files[fd].vfd < 0) {
      ConLogf("WARNING: opened '%s' on disk.", fname);
    }
  # endif
  #endif

  return fd;
}


//==========================================================================
//
//  vwexists
//
//==========================================================================
int vwexists (const char *fname) {
  const int oldpr = vw_debug_prints;
  vw_debug_prints = 0;
  VFile fd = vwopen(fname);
  vwclose(fd);
  vw_debug_prints = oldpr;
  return (fd != 0);
}


//==========================================================================
//
//  vwclose
//
//==========================================================================
void vwclose (VFile fd) {
  if (fd != 0) {
    if (fd > 0 && fd < MAX_OPENED_FILES && files[fd].mode != Closed) {
      if (files[fd].mode == Read) {
        if (files[fd].fl) {
          fclose(files[fd].fl);
        } else {
          vwad_fclose(files[fd].archive, files[fd].vfd);
        }
      }
      ClearFileInfo(&files[fd]);
    } else if (fd > 0 && fd < MAX_OPENED_FILES) {
      ABORT("trying to close non-opened file, fd=%d", fd);
    } else {
      ABORT("trying to close file with invalid fd, fd=%d", fd);
    }
  }
}


//==========================================================================
//
//  vwread
//
//  # of bytes read, -1 on error
//
//==========================================================================
int vwread (VFile fd, void *buf, int size) {
  if (size < 0) return -1;
  if (size > 0 && !buf) return -1;
  if (fd > 0 && fd < MAX_OPENED_FILES && files[fd].mode == Read) {
    if (files[fd].fl) {
      ssize_t rd = fread(buf, 1, (size_t)size, files[fd].fl);
      return (int)rd;
    } else {
      return vwad_read(files[fd].archive, files[fd].vfd, buf, size);
    }
  } else {
    return -1;
  }
}


//==========================================================================
//
//  vwtell
//
//==========================================================================
int vwtell (VFile fd) {
  if (fd > 0 && fd < MAX_OPENED_FILES && files[fd].mode == Read) {
    if (files[fd].fl) {
      return (int)ftell(files[fd].fl);
    } else {
      return vwad_tell(files[fd].archive, files[fd].vfd);
    }
  } else {
    return -1;
  }
}


//==========================================================================
//
//  vwseek
//
//==========================================================================
int vwseek (VFile fd, int offset) {
  if (offset < 0) return -1;
  if (fd > 0 && fd < MAX_OPENED_FILES && files[fd].mode == Read) {
    if (files[fd].fl) {
      return (fseek(files[fd].fl, offset, SEEK_SET) == 0 ? 0 : -1);
    } else {
      return (vwad_seek(files[fd].archive, files[fd].vfd, offset) == VWAD_OK ? 0 : -1);
    }
  } else {
    return -1;
  }
}


//==========================================================================
//
//  vwsize
//
//==========================================================================
int vwsize (VFile fd) {
  if (fd > 0 && fd < MAX_OPENED_FILES && files[fd].mode == Read) {
    if (files[fd].fl) {
      const long oldpos = ftell(files[fd].fl);
      if (oldpos < 0) return -1;
      if (fseek(files[fd].fl, 0, SEEK_END) != 0) {
        fseek(files[fd].fl, oldpos, SEEK_SET); // just in case
        return -1;
      }
      const int res = ftell(files[fd].fl);
      fseek(files[fd].fl, oldpos, SEEK_SET); // just in case
      return (res >= 0 ? (int)res : -1);
    } else {
      vwad_fidx fidx = vwad_fdfidx(files[fd].archive, files[fd].vfd);
      if (fidx < 0) return -1;
      const int res = vwad_get_file_size(files[fd].archive, fidx);
      return (res >= 0 ? (int)res : -1);
    }
  } else {
    return -1;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// SDL2 r/w ops
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  rw_size
//
//==========================================================================
static Sint64 SDLCALL rw_size (struct SDL_RWops *context) {
  VFile fl = (VFile)context->type;
  #if 0
  ConLogf("rw_size: fl=%d; size=%d", fl, vwsize(fl));
  #endif
  return vwsize(fl);
}


//==========================================================================
//
//  rw_seek
//
//==========================================================================
static Sint64 SDLCALL rw_seek (struct SDL_RWops *context, Sint64 offset, int whence) {
  VFile fl = (VFile)context->type;
  #if 0
  ConLogf("rw_seek: fl=%d; ofs=%d; whence=%d", fl, (int)offset, whence);
  #endif
       if (whence == RW_SEEK_CUR) offset += vwtell(fl);
  else if (whence == RW_SEEK_END) offset += vwsize(fl);

  if (vwseek(fl, (int)offset) != 0) return -1;

  return vwtell(fl);
}


//==========================================================================
//
//  rw_read
//
//==========================================================================
static size_t SDLCALL rw_read (struct SDL_RWops *context, void *ptr, size_t size,
                               size_t maxnum)
{
  if (size > 0x10000000 || maxnum > 0x10000000) return 0;
  if (size == 0 || maxnum == 0) return 0;
  VFile fl = (VFile)context->type;
  #if 0
  ConLogf("rw_read: fl=%d; size=%u; maxnum=%u", fl, (unsigned)size, (unsigned)maxnum);
  #endif
  uint8_t *dest = (uint8_t *)ptr;
  size_t res = 0;
  if (size == 1) {
    int rd = vwread(fl, dest, (int)maxnum);
    if (rd < 0) return 0;
    res = (size_t)rd;
  } else {
    while (maxnum != 0) {
      if (vwread(fl, dest, (int)size) != (int)size) break;
      dest += size;
      res += 1;
      maxnum -= 1;
    }
  }
  return res;
}


//==========================================================================
//
//  rw_write
//
//==========================================================================
static size_t SDLCALL rw_write (struct SDL_RWops *context, const void *ptr,
                                size_t size, size_t num)
{
  return 0;
}


//==========================================================================
//
//  rw_close
//
//==========================================================================
static int SDLCALL rw_close (struct SDL_RWops *context) {
  return 0;
}


//==========================================================================
//
//  vwAllocSDLRW
//
//==========================================================================
SDL_RWops *vwAllocSDLRW (VFile fl) {
  SDL_RWops *rw = SDL_AllocRW();
  if (rw) {
    rw->size = &rw_size;
    rw->seek = &rw_seek;
    rw->read = &rw_read;
    rw->write = &rw_write;
    rw->close = &rw_close;
    rw->type = (Uint32)fl;
  }
  return rw;
}


//==========================================================================
//
//  vwFreeSDLRW
//
//==========================================================================
void vwFreeSDLRW (SDL_RWops *&ops) {
  if (ops) {
    SDL_FreeRW(ops);
    ops = 0;
  }
}
