/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef FELIB_VW_FILE_IO_HEADER
#define FELIB_VW_FILE_IO_HEADER

#include "felibdef.h"
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include "feerror.h"
#include "vwadvfs.h"
#include "vwadwrite.h"


typedef int VFile; // file descriptor; 0 on error, >0 is valid

extern vwad_bool vw_archives_first;

void vw_set_bin_dir (const char *bindir);

int vw_is_absolute_path (const char *fname);

// return:
//  <0: error
//   0: normal archive
//   1: genuine (properly signed) archive
int vw_add_archive (const char *fname);

int vwexists (const char *fname);

// strictly for reading
VFile vwopen (const char *fname);
VFile vwopen_disk (const char *fname);
VFile vwopen_arch (const char *fname);
void vwclose (VFile fd);
// # of bytes read, -1 on error
int vwread (VFile fd, void *buf, int size);
int vwtell (VFile fd); // <0 is error
int vwseek (VFile fd, int offset); // return 0 on ok, -1 on error
int vwsize (VFile fd); // <0 is error


// destroying rwops *WILL* *NOT* close the file!
SDL_RWops *vwAllocSDLRW (VFile fl);
void vwFreeSDLRW (SDL_RWops *&ops);

#endif
