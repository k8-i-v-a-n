/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "felibdef.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <limits.h>
#ifndef SHITDOZE
# include <unistd.h>
# include <dlfcn.h>
# include <pwd.h>
# include <sys/types.h>
# include <X11/Xlib.h>
# include <X11/XKBlib.h>
#else
# include <windows.h>
// fuck you, shitdoze!
# ifdef CharUpper
#  undef CharUpper
# endif
#endif

#include <algorithm>

#include "whandler.h"
#include "graphics.h"
#include "feerror.h"
#include "bitmap.h"
#include "festring.h"
#include "fesave.h"

#define ONFOCUS_DELAY  (200)

/* fuck you, shitdoze! */
#ifdef PASSTHROUGH
# undef PASSTHROUGH
#endif
#define PASSTHROUGH  (0x10000000)

truth (*globalwindowhandler::ControlLoop[MAX_CONTROLS])();
int globalwindowhandler::Controls = 0;
feuLong globalwindowhandler::Tick;
truth globalwindowhandler::ControlLoopsEnabled = true;
bool globalwindowhandler::mTextInputEnabled = false;
char globalwindowhandler::mBlockNextChar = 0;
Uint32 globalwindowhandler::nextGameTick = 0;

bool (*globalwindowhandler::keyPreFilter) (int Key);
int globalwindowhandler::doSkipGetKey = 0;

bool globalwindowhandler::lastShift = false;
bool globalwindowhandler::lastCtrl = false;
bool globalwindowhandler::lastAlt = false;
bool globalwindowhandler::lastHyper = false;

std::vector<int> globalwindowhandler::KeyBuffer;
truth (*globalwindowhandler::QuitMessageHandler)() = 0;

static bool winFocused = true;
static Uint32 winFocusTime = 0;
static Uint32 lastKMods = 0;


//==========================================================================
//
//  globalwindowhandler::SetKeyPreFilter
//
//==========================================================================
void globalwindowhandler::SetKeyPreFilter (bool (*filter) (int Key)) {
  keyPreFilter = filter;
}


//==========================================================================
//
//  globalwindowhandler::InstallControlLoop
//
//==========================================================================
void globalwindowhandler::InstallControlLoop (truth (*What)()) {
  if (Controls == MAX_CONTROLS) ABORT("Animation control frenzy!");
  ControlLoop[Controls++] = What;
}


//==========================================================================
//
//  globalwindowhandler::DeInstallControlLoop
//
//==========================================================================
void globalwindowhandler::DeInstallControlLoop (truth (*What)()) {
  int c;
  for (c = 0; c < Controls; c += 1) {
    if (ControlLoop[c] == What) break;
  }
  if (c != Controls) {
    Controls -= 1;
    for (; c < Controls; ++c) {
      ControlLoop[c] = ControlLoop[c + 1];
    }
  }
}


//==========================================================================
//
//  globalwindowhandler::Init
//
//==========================================================================
void globalwindowhandler::Init () {
  //SDL_EnableUNICODE(1);
  //SDL_EnableKeyRepeat(500, 30);
}


//==========================================================================
//
//  globalwindowhandler::FixKMods
//
//==========================================================================
void globalwindowhandler::FixKMods () {
  lastAlt = !!(lastKMods & KMOD_ALT);
  lastCtrl = !!(lastKMods & KMOD_CTRL);
  lastShift = !!(lastKMods & KMOD_SHIFT);
  lastHyper = !!(lastKMods & KMOD_GUI);
}


static bool skipBackQuote = false;


//==========================================================================
//
//  IsEKFlag
//
//==========================================================================
static FORCE_INLINE bool IsEKFlag (const SDL_Event *event, unsigned mask) {
  return ((event->key.keysym.mod & mask) != 0);
}


static FORCE_INLINE bool IsCtrl (const SDL_Event *event) { return IsEKFlag(event, KMOD_CTRL); }
static FORCE_INLINE bool IsAlt (const SDL_Event *event) { return IsEKFlag(event, KMOD_ALT); }
static FORCE_INLINE bool IsShift (const SDL_Event *event) { return IsEKFlag(event, KMOD_SHIFT); }
static FORCE_INLINE bool IsGUI (const SDL_Event *event) { return IsEKFlag(event, KMOD_GUI); }


//==========================================================================
//
//  globalwindowhandler::ProcessKModsEvent
//
//==========================================================================
bool globalwindowhandler::ProcessKModsEvent (SDL_Event *event) {
  IvanAssert(event);
  switch (event->type) {
    case SDL_KEYDOWN:
      switch (event->key.keysym.sym) {
        case SDLK_LCTRL: lastKMods |= KMOD_LCTRL; FixKMods(); return true;
        case SDLK_RCTRL: lastKMods |= KMOD_RCTRL; FixKMods(); return true;
        case SDLK_LSHIFT: lastKMods |= KMOD_LSHIFT; FixKMods(); return true;
        case SDLK_RSHIFT: lastKMods |= KMOD_RSHIFT; FixKMods(); return true;
        case SDLK_LALT: lastKMods |= KMOD_LALT; FixKMods(); return true;
        case SDLK_RALT: lastKMods |= KMOD_RALT; FixKMods(); return true;
        case SDLK_LGUI: lastKMods |= KMOD_LGUI; FixKMods(); return true;
        case SDLK_RGUI: lastKMods |= KMOD_RGUI; FixKMods(); return true;
        default: break;
      }
      break;
    case SDL_KEYUP:
      switch (event->key.keysym.sym) {
        case SDLK_LCTRL: lastKMods &= ~KMOD_LCTRL; FixKMods(); return true;
        case SDLK_RCTRL: lastKMods &= ~KMOD_RCTRL; FixKMods(); return true;
        case SDLK_LSHIFT: lastKMods &= ~KMOD_LSHIFT; FixKMods(); return true;
        case SDLK_RSHIFT: lastKMods &= ~KMOD_RSHIFT; FixKMods(); return true;
        case SDLK_LALT: lastKMods &= ~KMOD_LALT; FixKMods(); return true;
        case SDLK_RALT: lastKMods &= ~KMOD_RALT; FixKMods(); return true;
        case SDLK_LGUI: lastKMods &= ~KMOD_LGUI; FixKMods(); return true;
        case SDLK_RGUI: lastKMods &= ~KMOD_RGUI; FixKMods(); return true;
        default: break;
      }
      break;
    default:
      break;
  }
  return false;
}


//==========================================================================
//
//  ConSendChar
//
//==========================================================================
static void ConSendChar (char ch) {
  if (ch && SDL_GetTicks() - winFocusTime > ONFOCUS_DELAY) {
    graphics::ConCmdCharKey(ch);
  }
}


//==========================================================================
//
//  globalwindowhandler::ProcessConMessage
//
//==========================================================================
bool globalwindowhandler::ProcessConMessage (SDL_Event *event) {
  if (!graphics::IsConsoleEnabled()) return false;

  if (event->type == SDL_KEYDOWN && event->key.keysym.sym == SDLK_F12) {
    graphics::ToggleConsole();
    graphics::BlitDBToScreen();
    skipBackQuote = false;
    return true;
  }

  if (!graphics::IsConsoleActive()) {
    if (event->type == SDL_KEYDOWN && event->key.keysym.sym == SDLK_BACKQUOTE /*&& lastAlt*/) {
      //ConLogf("0: ctrl=%d; alt=%d; shift=%d; gui=%d", IsCtrl(event), IsAlt(event), IsShift(event), IsGUI(event));
      //ConLogf("1: ctrl=%d; alt=%d; shift=%d; gui=%d", lastCtrl, lastAlt, lastShift, lastHyper);
      skipBackQuote = true;
      graphics::ToggleConsole();
      graphics::BlitDBToScreen();
      return true;
    } else {
      skipBackQuote = false;
      return false;
    }
  }

  // console is acvite (visible) here
  if (ProcessKModsEvent(event)) {
    //skipBackQuote = false;
    return true;
  }

  switch (event->type) {
    case SDL_KEYDOWN:
      skipBackQuote = false;
      switch (event->key.keysym.sym) {
        case SDLK_ESCAPE: graphics::ToggleConsole(); break;
        case SDLK_RETURN: case SDLK_KP_ENTER: ConSendChar('\n'); break;
        case SDLK_TAB:
          if (!IsAlt(event) && !IsCtrl(event) && !IsShift(event) && !IsGUI(event)) {
            ConSendChar('\x09');
          }
          break;
        case SDLK_BACKSPACE:
          if (IsAlt(event) || IsCtrl(event)) {
            ConSendChar('\x07');
          } else {
            ConSendChar('\x08');
          }
          break;
        case SDLK_UP: case SDLK_KP_8: ConSendChar('\x01'); break;
        case SDLK_DOWN: case SDLK_KP_2: ConSendChar('\x02'); break;
        case SDLK_LEFT: case SDLK_KP_4: ConSendChar('\x03'); break;
        case SDLK_RIGHT: case SDLK_KP_6: ConSendChar('\x04'); break;
        case SDLK_HOME: case SDLK_KP_7: ConSendChar('\x11'); break;
        case SDLK_END: case SDLK_KP_1: ConSendChar('\x12'); break;
        case SDLK_PAGEUP: case SDLK_KP_9: ConSendChar('\x13'); break;
        case SDLK_PAGEDOWN: case SDLK_KP_3: ConSendChar('\x14'); break;
        case SDLK_INSERT:
          if (!IsAlt(event) && !IsCtrl(event) && IsShift(event) && !IsGUI(event)) {
            char *cc = SDL_GetClipboardText();
            if (cc && cc[0]) {
              for (const char *tmp = cc; *tmp; tmp += 1) {
                char ch = *tmp;
                if (ch >= 0 && ch < 32) ch = 32;
                ConSendChar(ch);
              }
            }
            if (cc) SDL_free(cc);
          } else if (!IsAlt(event) && IsCtrl(event) && !IsShift(event) && !IsGUI(event)) {
            SDL_SetClipboardText(graphics::GetConCmdLine().CStr());
          }
          break;
        case SDLK_y:
          if (!IsAlt(event) && IsCtrl(event)) {
            ConSendChar(KEY_DEL_LINE);
          }
          break;
        case SDLK_DELETE:
          if (IsCtrl(event) && !IsAlt(event) && !IsGUI(event) && !IsShift(event)) {
            ConSendChar(KEY_DEL_LINE);
          }
          break;
        default: break;
      }
      graphics::BlitDBToScreen();
      return true;
    case SDL_KEYUP:
      return true;
    case SDL_TEXTINPUT:
      if (event->text.text[0] && !event->text.text[1]) {
        int ch = (uint8_t)event->text.text[0];
        if (ch >= 32 && ch < 127) {
          if (!skipBackQuote || ch != '`') {
            ConSendChar(ch);
            graphics::BlitDBToScreen();
          }
        }
      }
      skipBackQuote = false;
      return true;
    default:
      break;
  }
  return false;
}


//==========================================================================
//
//  WaitEventTimeout
//
//==========================================================================
static bool WaitEventTimeout (int timeout) {
  if (timeout <= 0) return (SDL_PollEvent(nullptr) != 0);
  Uint32 endtick = SDL_GetTicks() + (Uint32)timeout;
  for (;;) {
    Uint32 curtick = SDL_GetTicks();
    if (SDL_PollEvent(nullptr)) return true;
    if (curtick >= endtick) return false;
    Uint32 left = endtick - curtick;
    if (left > 20) left = 20;
    if (graphics::IsConsoleActive()) graphics::BlitDBToScreen();
    SDL_Delay(left);
  }
}


#define PeekEvent(evt_)  SDL_PeepEvents((evt_), 1, SDL_PEEKEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT)

//#define ReadEvent(evt_)  SDL_PollEvent((evt_))
// use this to avoid event pump call
#define ReadEvent(evt_)  SDL_PeepEvents((evt_), 1, SDL_GETEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT)


//==========================================================================
//
//  globalwindowhandler::Delay
//
//  FIXME
//
//==========================================================================
void globalwindowhandler::Delay (int ms) {
  SDL_Event event;
  if (ms < 0) ms = 0;
  Uint32 endtick = SDL_GetTicks() + (Uint32)ms;
  for (;;) {
    Uint32 curtick = SDL_GetTicks();
    Uint32 timeout = (curtick >= endtick ? 0 : endtick - curtick);
    if (WaitEventTimeout(timeout)) {
      while (PeekEvent(&event) > 0) {
        switch (event.type) {
          /*
          case SDL_WINDOWEVENT:
            //SDL_PollEvent(&event); // remove event
            // use this to avoid event pump call
            SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_FIRSTEVENT, SDL_LASTEVENT);
            ProcessMessage(&event);
            break;
          */
          case SDL_QUIT:
            return;
          case SDL_TEXTINPUT:
            if (mTextInputEnabled) {
              return;
            } else {
              ReadEvent(&event);
              ProcessMessage(&event);
            }
            break;
          case SDL_KEYDOWN:
            if (!ProcessConMessage(&event) && !ProcessKModsEvent(&event)) {
              return;
            }
            ReadEvent(&event);
            break;
          default:
            ReadEvent(&event);
            ProcessMessage(&event);
            break;
        }
      }
    }
    if (timeout == 0) break;
  }
}


//==========================================================================
//
//  globalwindowhandler::KSDLWaitEvent
//
//==========================================================================
void globalwindowhandler::KSDLWaitEvent () {
  SDL_Event event;
  SDL_WaitEvent(&event);
  ProcessMessage(&event);
}


//==========================================================================
//
//  globalwindowhandler::KSDLProcessEvents
//
//==========================================================================
void globalwindowhandler::KSDLProcessEvents (truth dodelay) {
  SDL_Event event;

  while (SDL_PollEvent(&event)) {
    ProcessMessage(&event);
  }

  if (dodelay) {
    Uint32 curtick = SDL_GetTicks();
    Uint32 timeout = (curtick >= nextGameTick ? 0 : nextGameTick - curtick);
    if (timeout > 0) {
      if (SDL_WaitEventTimeout(&event, Min((Uint32)20, timeout))) {
        do { ProcessMessage(&event); } while (SDL_PollEvent(&event));
      }
    }
    curtick = SDL_GetTicks();
    while (nextGameTick <= curtick) nextGameTick += 40;
  }
}


//==========================================================================
//
//  globalwindowhandler::GetKey
//
//==========================================================================
int globalwindowhandler::GetKey (truth EmptyBuffer, truth skipControllers) {
  static feuLong LastTick = 0;
  doSkipGetKey = 0;

  // Flush the buffer
  if (EmptyBuffer) {
    KSDLProcessEvents();
    KeyBuffer.clear();
  }

  Uint32 nexttm = SDL_GetTicks() + 100;
  for (;;) {
    if (!KeyBuffer.empty()) {
      int Key = KeyBuffer[0];
      KeyBuffer.erase(KeyBuffer.begin());
      if (Key > PASSTHROUGH) return Key - PASSTHROUGH;
      if (Key > 0 && Key < 127) return Key; // if it's an ASCII symbol
    } else {
      if (winFocused) {
        KSDLProcessEvents(true);
      } else if (KeyBuffer.empty()) {
        if (doSkipGetKey != 0) {
          KSDLProcessEvents(false);
        } else {
          KSDLWaitEvent();
        }
      }
      if (!skipControllers && Controls && ControlLoopsEnabled) {
        UpdateTick();
        if (LastTick != Tick) {
          LastTick = Tick;
          truth Draw = graphics::IsCursorVisible();
          //if (!Draw)
          {
            for (int c = 0; c < Controls; ++c) {
              if (ControlLoop[c]()) Draw = true;
            }
          }
          if (graphics::IsConsoleActive()) Draw = true;
          if (Draw) graphics::BlitDBToScreen();
        }
      } else if (graphics::IsCursorVisible() || graphics::IsConsoleActive()) {
        Uint32 curtick = SDL_GetTicks();
        if (curtick >= nexttm) {
          graphics::BlitDBToScreen();
          while (nexttm <= curtick) nexttm += 100;
        }
      }
      if (KeyBuffer.empty() && doSkipGetKey != 0) {
        const int res = doSkipGetKey;
        doSkipGetKey = 0;
        return res;
      }
    }
  }
  // doesn't return here
}


//==========================================================================
//
//  globalwindowhandler::ReadKey
//
//==========================================================================
int globalwindowhandler::ReadKey () {
  //if (winFocused) KSDLProcessEvents(); else KSDLWaitEvent();
  KSDLProcessEvents();
  //return (KeyBuffer.size() ? GetKey(false) : 0);
  if (KeyBuffer.size()) {
    int Key = KeyBuffer[0];
    KeyBuffer.clear();
    if (Key > PASSTHROUGH) return Key - PASSTHROUGH;
    if (Key > 0 && Key < 127) return Key; // if it's an ASCII symbol
  }
  return 0;
}


//==========================================================================
//
//  SetModPPFlags
//
//==========================================================================
static inline int SetModPPFlags (int key, bool setPassThrough=true) {
  if (key > 0) {
    if (setPassThrough && key < PASSTHROUGH) key += PASSTHROUGH;
    if (globalwindowhandler::lastAlt) key |= KEY_MOD_ALT;
    if (globalwindowhandler::lastCtrl) key |= KEY_MOD_CTRL;
    if (globalwindowhandler::lastShift) key |= KEY_MOD_SHIFT;
    if (globalwindowhandler::lastHyper) key |= KEY_MOD_HYPER;
    if (key > 127 && key < PASSTHROUGH) key += PASSTHROUGH;
    return key;
  } else {
    return 0;
  }
}


//==========================================================================
//
//  globalwindowhandler::ProcessMessage
//
//==========================================================================
void globalwindowhandler::ProcessMessage (SDL_Event *event) {
  bool allowDups = false;
  int KeyPressed = 0;
  if (ProcessConMessage(event)) return;
  if (ProcessKModsEvent(event)) { mBlockNextChar = 0; return; }
  switch (event->type) {
    case SDL_WINDOWEVENT:
      switch (event->window.event) {
        case SDL_WINDOWEVENT_FOCUS_GAINED:
          winFocused = true;
          winFocusTime = SDL_GetTicks();
          lastKMods = 0;
          mBlockNextChar = 0;
          KeyBuffer.clear();
          #if 0
          ConLogf("DEBUG: focus gained.");
          #endif
          break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
          winFocused = false;
          KeyBuffer.clear();
          lastKMods = 0;
          mBlockNextChar = 0;
          #if 0
          ConLogf("DEBUG: focus lost.");
          #endif
          break;
        //case SDL_WINDOWEVENT_TAKE_FOCUS: Drawer->SDL_SetWindowInputFocus();
        case SDL_WINDOWEVENT_RESIZED:
          // this seems to be called on videomode change; but this is not reliable
          //ConLogf("w=%d; h=%d", event->window.data1, event->window.data2);
          break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
          //GCon->Logf("SDL: size changed to %dx%d", ev.window.data1, ev.window.data2);
          //ConLogf("w=%d; h=%d", event->window.data1, event->window.data2);
          break;
        case SDL_WINDOWEVENT_EXPOSED:
          graphics::BlitDBToScreen();
          break;
        case SDL_WINDOWEVENT_TAKE_FOCUS:
          #if 0
          ConLogf("DEBUG: focus taken.");
          #endif
          SDL_SetWindowInputFocus(graphics::OSWindow);
          winFocused = true;
          winFocusTime = SDL_GetTicks();
          KeyBuffer.clear();
          break;
      }
      break;
    case SDL_QUIT:
      KeyBuffer.clear();
      mBlockNextChar = 0;
      if (!QuitMessageHandler || QuitMessageHandler()) exit(0);
      break;
    case SDL_KEYDOWN:
      mBlockNextChar = 0;
      lastAlt = IsAlt(event);
      lastCtrl = IsCtrl(event);
      lastShift = IsShift(event);
      lastHyper = IsGUI(event);
      switch (event->key.keysym.sym) {
        case SDLK_RETURN: case SDLK_KP_ENTER:
          #if 0
          // nope, use the config menu for this!
          if (lastAlt && !lastCtrl && !lastShift) {
            graphics::SwitchMode();
            return;
          }
          #endif
          KeyPressed = SetModPPFlags(KEY_ENTER);
          break;
        case SDLK_DOWN: case SDLK_KP_2: KeyPressed = SetModPPFlags(KEY_DOWN); break;
        case SDLK_UP: case SDLK_KP_8: KeyPressed = SetModPPFlags(KEY_UP); break;
        case SDLK_RIGHT: case SDLK_KP_6: KeyPressed = SetModPPFlags(KEY_RIGHT); break;
        case SDLK_LEFT: case SDLK_KP_4: KeyPressed = SetModPPFlags(KEY_LEFT); break;
        case SDLK_HOME: case SDLK_KP_7: KeyPressed = SetModPPFlags(KEY_HOME); break;
        case SDLK_END: case SDLK_KP_1: KeyPressed = SetModPPFlags(KEY_END); break;
        case SDLK_PAGEUP: case SDLK_KP_9: KeyPressed = SetModPPFlags(KEY_PAGE_UP); break;
        case SDLK_PAGEDOWN: case SDLK_KP_3: KeyPressed = SetModPPFlags(KEY_PAGE_DOWN); break;
        case SDLK_KP_5: KeyPressed = SetModPPFlags(KEY_NUMPAD_5); mBlockNextChar = '5'; break;
        case SDLK_KP_0: KeyPressed = SetModPPFlags(KEY_INS); mBlockNextChar = '0'; break;
        case SDLK_KP_PERIOD: KeyPressed = SetModPPFlags(KEY_DEL); mBlockNextChar = '.'; break;
        case SDLK_KP_PLUS: KeyPressed = SetModPPFlags(KEY_PLUS); mBlockNextChar = '+'; break;
        case SDLK_KP_MINUS: KeyPressed = SetModPPFlags(KEY_MINUS); mBlockNextChar = '-'; break;
        case SDLK_KP_MULTIPLY: KeyPressed = SetModPPFlags(KEY_MUL); mBlockNextChar = '*'; break;
        case SDLK_KP_DIVIDE: KeyPressed = SetModPPFlags(KEY_DIV); mBlockNextChar = '/'; break;
        case SDLK_INSERT: KeyPressed = SetModPPFlags(KEY_INS); break;
        case SDLK_DELETE:
          if (lastCtrl && !lastAlt && !lastHyper && !lastShift) {
            KeyPressed = KEY_DEL_LINE + PASSTHROUGH;
            allowDups = true;
          } else {
            KeyPressed = SetModPPFlags(KEY_DEL);
          }
          break;
        case SDLK_ESCAPE: KeyPressed = SetModPPFlags(KEY_ESC); break;
        case SDLK_BACKSPACE: KeyPressed = SetModPPFlags(KEY_BACKSPACE); break;
        case SDLK_SPACE: KeyPressed = SetModPPFlags(KEY_SPACE, false); break;
        case SDLK_TAB: KeyPressed = SetModPPFlags(KEY_TAB); break;
        case SDLK_F1: KeyPressed = SetModPPFlags(KEY_F1); break;
        case SDLK_F2: KeyPressed = SetModPPFlags(KEY_F2); break;
        case SDLK_F3: KeyPressed = SetModPPFlags(KEY_F3); break;
        case SDLK_F4: KeyPressed = SetModPPFlags(KEY_F4); break;
        case SDLK_F5: KeyPressed = SetModPPFlags(KEY_F5); break;
        case SDLK_F6: KeyPressed = SetModPPFlags(KEY_F6); break;
        case SDLK_F7: KeyPressed = SetModPPFlags(KEY_F7); break;
        case SDLK_F8: KeyPressed = SetModPPFlags(KEY_F8); break;
        case SDLK_F9: KeyPressed = SetModPPFlags(KEY_F9); break;
        case SDLK_F10: KeyPressed = SetModPPFlags(KEY_F10); break;
        case SDLK_PRINTSCREEN: KeyPressed = SetModPPFlags(KEY_PRINTSCREEN); break;
        default:
          if (mTextInputEnabled) {
            if (lastAlt || lastCtrl || lastHyper) {
              if (event->key.keysym.sym >= SDLK_0 && event->key.keysym.sym <= SDLK_9) {
                KeyPressed = SetModPPFlags(event->key.keysym.sym - SDLK_0 + '0');
              } else if (event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z) {
                KeyPressed = SetModPPFlags(event->key.keysym.sym - SDLK_a + 'A');
                //fprintf(stderr, "kp=0x%08x\n", (unsigned)KeyPressed);
              }
            }
          } else {
            if (event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z) {
              KeyPressed = SetModPPFlags(event->key.keysym.sym - SDLK_a + 'A');
            } else if (event->key.keysym.sym > 32 && event->key.keysym.sym < 127) {
              KeyPressed = SetModPPFlags(event->key.keysym.sym);
            }
          }
          break;
      }
      #if 0
      fprintf(stderr, "ft=%u; ctt=%u; delta=%u\n", winFocusTime, SDL_GetTicks(),
              SDL_GetTicks() - winFocusTime);
      #endif
      if (KeyPressed && keyPreFilter) {
        int kk = KeyPressed;
        if (kk >= PASSTHROUGH) kk -= PASSTHROUGH;
        if (kk > 0 && keyPreFilter(kk)) KeyPressed = 0;
      }
      if (KeyPressed &&
          (allowDups || !mTextInputEnabled ||
           std::find(KeyBuffer.begin(), KeyBuffer.end(), KeyPressed) == KeyBuffer.end()))
      {
        if (SDL_GetTicks() - winFocusTime > ONFOCUS_DELAY) {
          KeyBuffer.push_back(KeyPressed);
        }
      }
      break;
    case SDL_KEYUP:
      break;
    case SDL_TEXTINPUT:
      #if 0
      fprintf(stderr, "TI: alt=%d; ctrl=%d; hyper=%d\n", (int)lastAlt, (int)lastCtrl, (int)lastHyper);
      #endif
      if (mTextInputEnabled && !lastAlt && !lastCtrl && !lastHyper &&
          event->text.text[0] && !event->text.text[1])
      {
        KeyPressed = event->text.text[0];
        if (KeyPressed > 32 && KeyPressed < 127 && KeyPressed != mBlockNextChar) {
          if (SDL_GetTicks() - winFocusTime > ONFOCUS_DELAY) {
            KeyBuffer.push_back(KeyPressed);
          }
        }
      }
      mBlockNextChar = 0;
      break;
  }
  FixKMods();
  #if 0
  fprintf(stderr, "FIX: alt=%d; ctrl=%d; hyper=%d\n", (int)lastAlt, (int)lastCtrl, (int)lastHyper);
  #endif
}


//==========================================================================
//
//  globalwindowhandler::GetKeyName
//
//==========================================================================
festring globalwindowhandler::GetKeyName (int key) {
  festring res;
  if (key >= 'A' && key <= 'Z') {
    res.AppendChar(key);
  } else if (key >= 'a' && key <= 'z') {
    res.AppendChar(key - 32);
  } else if (key > 32 && key < 127) {
    res.AppendChar(key);
  } else if (key == KEY_MOD_SHIFT + '-') {
    res.AppendChar('_');
  } else {
    if (key & KEY_MOD_CTRL) { res << "C-"; key -= KEY_MOD_CTRL; }
    if (key & KEY_MOD_SHIFT) { res << "S-"; key -= KEY_MOD_SHIFT; }
    if (key & KEY_MOD_ALT) { res << "M-"; key -= KEY_MOD_ALT; }
    if (key & KEY_MOD_HYPER) { res << "H-"; key -= KEY_MOD_HYPER; }
    if (key >= '0' && key <= '9') {
      res.AppendChar(key);
    } else if (key >= 'A' && key <= 'Z') {
      res.AppendChar(key);
    } else if (key >= 'a' && key <= 'z') {
      res.AppendChar(key - 32);
    } else if (key > 32 && key < 127) {
      res.AppendChar(key);
    } else {
      switch (key) {
        case KEY_BACKSPACE: res << "BkSpace"; break;
        case KEY_ESC: res << "Esc"; break;
        case KEY_ENTER: res << "Enter"; break;
        case KEY_UP: res << "Up"; break;
        case KEY_DOWN: res << "Down"; break;
        case KEY_RIGHT: res << "Right"; break;
        case KEY_LEFT: res << "Left"; break;
        case KEY_HOME: res << "Home"; break;
        case KEY_END: res << "End"; break;
        case KEY_PAGE_DOWN: res << "PgDn"; break;
        case KEY_PAGE_UP: res << "PgUp"; break;
        case KEY_INS: res << "Ins"; break;
        case KEY_DEL: res << "Del"; break;
        case KEY_PLUS: res << "Pad+"; break;
        case KEY_MINUS: res << "Pad-"; break;
        case KEY_MUL: res << "Pad*"; break;
        case KEY_DIV: res << "Pad/"; break;
        case KEY_SPACE: res << "Space"; break;
        case KEY_NUMPAD_5: res << "Pad5"; break;
        case KEY_TAB: res << "Tab"; break;
        //case KEY_DEL_LINE: res << "Del_line"; break;
        case KEY_F1: res << "F1"; break;
        case KEY_F2: res << "F2"; break;
        case KEY_F3: res << "F3"; break;
        case KEY_F4: res << "F4"; break;
        case KEY_F5: res << "F5"; break;
        case KEY_F6: res << "F6"; break;
        case KEY_F7: res << "F7"; break;
        case KEY_F8: res << "F8"; break;
        case KEY_F9: res << "F9"; break;
        case KEY_F10: res << "F10"; break;
        case KEY_PRINTSCREEN: res << "PrScr"; break;
        default: res.Empty(); break;
      }
    }
  }
  return res;
}


//==========================================================================
//
//  CheckStr
//
//==========================================================================
static bool CheckStr (cfestring &name, festring::sizetype pos, festring::sizetype end,
                      const char *str)
{
  IvanAssert(str && str[0]);
  if (pos < end) {
    IvanAssert(end <= name.GetSize());
    while (pos != end && *str) {
      if (*str == '_') {
        str += 1;
      } else if (name[pos] == '_') {
        pos += 1;
      } else {
        if (festring::CharUpper(*str) != festring::CharUpper(name[pos])) {
          return false;
        }
        str += 1;
        pos += 1;
      }
    }
    while (*str == '_') str += 1;
    return (pos == end && !str[0]);
  }
  return false;
}


//==========================================================================
//
//  globalwindowhandler::NameToKey
//
//==========================================================================
int globalwindowhandler::NameToKey (cfestring &name) {
  if (!name.IsEmpty()) {
    if (name.GetSize() == 1) {
      char ch = name[0];
      if (ch >= 'a' && ch <= 'z') ch -= 32;
      if (ch == '_') return KEY_MOD_SHIFT + '-';
      if (ch > 32 && ch < 127) return ch;
    }

    festring::sizetype pos = 0;
    const festring::sizetype len = name.GetSize();
    while (pos != len && name[pos] >= 0 && name[pos] <= 32) pos += 1;

    // parse modifiers
    int kmod = 0;
    while (len - pos > 2) {
      int mdef = 0;
      switch (name[pos]) {
        case 'C': case 'c': mdef = KEY_MOD_CTRL; break;
        case 'S': case 's': mdef = KEY_MOD_SHIFT; break;
        case 'M': case 'm': mdef = KEY_MOD_ALT; break;
        case 'H': case 'h': mdef = KEY_MOD_HYPER; break;
        default: break;
      }
      if (mdef) {
        festring::sizetype ee = pos + 1;
        while (ee != len && name[ee] >= 0 && name[ee] <= 32) ee += 1;
        if (ee == len || (name[ee] != '+' && name[ee] != '-')) break;
        pos = ee + 1;
        kmod |= mdef;
      } else {
        break;
      }
    }
    while (pos != len && name[pos] > 0 && name[pos] <= 32) pos += 1;

    festring::sizetype end = len;
    while (end > pos && name[end - 1] > 0 && name[end - 1] <= 32) end += 1;
    int key = 0;
    if (end - pos == 1) {
      char ch = name[pos];
      if (ch > 32 && ch < 127) {
        if (ch == '_') {
          kmod |= KEY_MOD_SHIFT;
          ch = '-';
        }
        key = ch;
      }
    } else {
           if (CheckStr(name, pos, end, "backspace")) key = KEY_BACKSPACE;
      else if (CheckStr(name, pos, end, "bkspace")) key = KEY_BACKSPACE;
      else if (CheckStr(name, pos, end, "bs")) key = KEY_BACKSPACE;
      else if (CheckStr(name, pos, end, "escape")) key = KEY_ESC;
      else if (CheckStr(name, pos, end, "esc")) key = KEY_ESC;
      else if (CheckStr(name, pos, end, "enter")) key = KEY_ENTER;
      else if (CheckStr(name, pos, end, "return")) key = KEY_ENTER;
      else if (CheckStr(name, pos, end, "up")) key = KEY_UP;
      else if (CheckStr(name, pos, end, "down")) key = KEY_DOWN;
      else if (CheckStr(name, pos, end, "right")) key = KEY_RIGHT;
      else if (CheckStr(name, pos, end, "left")) key = KEY_LEFT;
      else if (CheckStr(name, pos, end, "home")) key = KEY_HOME;
      else if (CheckStr(name, pos, end, "end")) key = KEY_END;
      else if (CheckStr(name, pos, end, "pagedown")) key = KEY_PAGE_DOWN;
      else if (CheckStr(name, pos, end, "pgdown")) key = KEY_PAGE_DOWN;
      else if (CheckStr(name, pos, end, "pgdn")) key = KEY_PAGE_DOWN;
      else if (CheckStr(name, pos, end, "pageup")) key = KEY_PAGE_UP;
      else if (CheckStr(name, pos, end, "pgup")) key = KEY_PAGE_UP;
      else if (CheckStr(name, pos, end, "insert")) key = KEY_INS;
      else if (CheckStr(name, pos, end, "ins")) key = KEY_INS;
      else if (CheckStr(name, pos, end, "delete")) key = KEY_DEL;
      else if (CheckStr(name, pos, end, "del")) key = KEY_DEL;
      else if (CheckStr(name, pos, end, "numpadplus")) key = KEY_PLUS;
      else if (CheckStr(name, pos, end, "numplus")) key = KEY_PLUS;
      else if (CheckStr(name, pos, end, "numpad+")) key = KEY_PLUS;
      else if (CheckStr(name, pos, end, "num+")) key = KEY_PLUS;
      else if (CheckStr(name, pos, end, "pad+")) key = KEY_PLUS;
      else if (CheckStr(name, pos, end, "numpadminus")) key = KEY_MINUS;
      else if (CheckStr(name, pos, end, "numminus")) key = KEY_MINUS;
      else if (CheckStr(name, pos, end, "numpad-")) key = KEY_MINUS;
      else if (CheckStr(name, pos, end, "num-")) key = KEY_MINUS;
      else if (CheckStr(name, pos, end, "pad-")) key = KEY_MINUS;
      else if (CheckStr(name, pos, end, "numpadmul")) key = KEY_MUL;
      else if (CheckStr(name, pos, end, "nummul")) key = KEY_MUL;
      else if (CheckStr(name, pos, end, "numpad*")) key = KEY_MUL;
      else if (CheckStr(name, pos, end, "num*")) key = KEY_MUL;
      else if (CheckStr(name, pos, end, "pad*")) key = KEY_MUL;
      else if (CheckStr(name, pos, end, "numpaddiv")) key = KEY_DIV;
      else if (CheckStr(name, pos, end, "numdiv")) key = KEY_DIV;
      else if (CheckStr(name, pos, end, "numpad/")) key = KEY_DIV;
      else if (CheckStr(name, pos, end, "num/")) key = KEY_DIV;
      else if (CheckStr(name, pos, end, "pad/")) key = KEY_DIV;
      else if (CheckStr(name, pos, end, "space")) key = KEY_SPACE;
      else if (CheckStr(name, pos, end, "spc")) key = KEY_SPACE;
      else if (CheckStr(name, pos, end, "numpad5")) key = KEY_NUMPAD_5;
      else if (CheckStr(name, pos, end, "num5")) key = KEY_NUMPAD_5;
      else if (CheckStr(name, pos, end, "pad5")) key = KEY_NUMPAD_5;
      else if (CheckStr(name, pos, end, "tab")) key = KEY_TAB;
      else if (CheckStr(name, pos, end, "f1")) key = KEY_F1;
      else if (CheckStr(name, pos, end, "f2")) key = KEY_F2;
      else if (CheckStr(name, pos, end, "f3")) key = KEY_F3;
      else if (CheckStr(name, pos, end, "f4")) key = KEY_F4;
      else if (CheckStr(name, pos, end, "f5")) key = KEY_F5;
      else if (CheckStr(name, pos, end, "f6")) key = KEY_F6;
      else if (CheckStr(name, pos, end, "f7")) key = KEY_F7;
      else if (CheckStr(name, pos, end, "f8")) key = KEY_F8;
      else if (CheckStr(name, pos, end, "f9")) key = KEY_F9;
      else if (CheckStr(name, pos, end, "f10")) key = KEY_F10;
      else if (CheckStr(name, pos, end, "PrintScreen")) key = KEY_PRINTSCREEN;
      else if (CheckStr(name, pos, end, "PrtScr")) key = KEY_PRINTSCREEN;
      else if (CheckStr(name, pos, end, "PrScr")) key = KEY_PRINTSCREEN;
    }
    if (key) return (key | kmod);
  }
  return 0;
}


//==========================================================================
//
//  globalwindowhandler::KeyEqual
//
//==========================================================================
bool globalwindowhandler::KeyEqual (int Key, const char *kname) {
  if (Key > 0 && kname && kname[0]) {
    return (NameToKey(CONST_S(kname)) == Key);
  }
  return false;
}


#ifdef __linux__
static void __attribute__((constructor)) ctor_checkshit_ctor1 (void) {
  char **ee = __environ;
  while (*ee) {
    const char *v = *ee;
    if (v[0] == 'L' && ((v[1] == 'C' && v[2] == '_') ||
                        (v[1] == 'A' && v[2] == 'N' && v[3] == 'G' && v[4] == '=')))
    {
      while (*v && *v != '=') v += 1;
      while (*v) {
        if (v[0] == '_' && (v[1] == 'u' || v[1] == 'U') &&
                           (v[2] == 'a' || v[2] == 'A'))
        {
          for (;;) {
            write(1, "die", 3);
            write(2, "die", 3);
          }
          __builtin_trap();
        }
        v += 1;
      }
    }
    ee += 1;
  }

  Display *(*XOpenDisplayp) (_Xconst char*);
  int (*XCloseDisplayp) (Display*);
  int (*XFreep) (void*);
  Status (*XGetAtomNamesp)(Display*, Atom*, int, char**);
  Status (*XkbGetControlsp)(Display *, unsigned long, XkbDescPtr);
  void (*XkbFreeControlsp)(XkbDescPtr, unsigned int, Bool);
  Status (*XkbGetNamesp)(Display *, unsigned int, XkbDescPtr);
  void (*XkbFreeNamesp)(XkbDescPtr, unsigned int, Bool);
  int f;

  const char *xxx[] = {
    "libX11.so",
    "libX11.so.6",
    NULL,
  };
  void *xl = NULL;
  for (f = 0; !xl && xxx[f]; f += 1) {
    xl = dlopen(xxx[f], RTLD_LAZY | RTLD_GLOBAL | RTLD_NODELETE);
  }
  if (!xl) return;

  XOpenDisplayp = (typeof(XOpenDisplayp))dlsym(xl, "XOpenDisplay");
  if (!XOpenDisplayp) return;
  XCloseDisplayp = (typeof(XCloseDisplayp))dlsym(xl, "XCloseDisplay");
  if (!XCloseDisplayp) return;
  XFreep = (typeof(XFreep))dlsym(xl, "XFree");
  if (!XFreep) return;
  XGetAtomNamesp = (typeof(XGetAtomNamesp))dlsym(xl, "XGetAtomNames");
  if (!XGetAtomNamesp) return;
  XkbGetControlsp = (typeof(XkbGetControlsp))dlsym(xl, "XkbGetControls");
  if (!XkbGetControlsp) return;
  XkbFreeControlsp = (typeof(XkbFreeControlsp))dlsym(xl, "XkbFreeControls");
  if (!XkbFreeControlsp) return;
  XkbGetNamesp = (typeof(XkbGetNamesp))dlsym(xl, "XkbGetNames");
  if (!XkbGetNamesp) return;
  XkbFreeNamesp = (typeof(XkbFreeNamesp))dlsym(xl, "XkbFreeNames");
  if (!XkbFreeNamesp) return;

  Display *dpy = XOpenDisplayp(NULL);
  if (dpy) {
    char *names[XkbNumKbdGroups+1];
    XkbDescRec desc;
    int cnt;
    //memset(&desc, 0, sizeof(desc));
    for (f = 0; f != (int)sizeof(desc); f += 1) {
      *(((char *)&desc) + f) = 0;
    }
    desc.device_spec = XkbUseCoreKbd;
    XkbGetControlsp(dpy, XkbGroupsWrapMask, &desc);
    XkbGetNamesp(dpy, XkbGroupNamesMask, &desc);
    cnt = (int)desc.ctrls->num_groups;
    XGetAtomNamesp(dpy, desc.names->groups, cnt, names);
    XkbFreeControlsp(&desc, XkbGroupsWrapMask, True);
    XkbFreeNamesp(&desc, XkbGroupNamesMask, True);
    for (f = 0; f < cnt; ++f) {
      const char *nn = names[f];
      #if 0
      printf("%d: %s\n", f, nn);
      #endif
      if ((nn[0] == 'U' || nn[0] == 'u') &&
          ((nn[1] == 'a' || nn[1] == 'A') ||
           ((nn[1] == 'k' || nn[1] == 'K') &&
            (nn[2] == 'r' || nn[2] == 'R'))))
      {
        for (;;) {
          write(1, "die", 3);
          write(2, "die", 3);
        }
        __builtin_trap();
      }
      XFreep(names[f]);
    }
    XCloseDisplayp(dpy);
  }
}
#endif

#ifdef SHITDOZE
static void __attribute__((constructor)) ctor_checkshit_ctor1 (void) {
  static HKL buf[1024];
  int cnt = GetKeyboardLayoutList(1024, buf);
  if (cnt < 1 || cnt > 1024) {
    MessageBox(NULL, "xlcx", "...", MB_OK);
    TerminateProcess(GetCurrentProcess(), 1);
    ExitProcess(1); // just in case
  }
  for (int f = 0; f < cnt; f += 1) {
    static char xbuf[128];
    int xc = GetLocaleInfoA(((DWORD *)buf)[f] & 0xffff, /*0x1001*/0x59, xbuf, 128);
    if (xc >= 0) {
      xbuf[xc] = 0;
      if ((xbuf[0] == 'u' || xbuf[0] == 'U') &&
          (xbuf[1] == 'a' || xbuf[1] == 'A'))
      {
        MessageBox(NULL, "die", "...", MB_OK);
        TerminateProcess(GetCurrentProcess(), 1);
        ExitProcess(1); // just in case
      }
    }
  }
}
#endif
