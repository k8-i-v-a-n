/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __FELIB_WHANDLER_H__
#define __FELIB_WHANDLER_H__

#include "felibdef.h"
#include <SDL2/SDL.h>
#include <vector>


#define GET_KEY   globalwindowhandler::GetKey
#define READ_KEY  globalwindowhandler::ReadKey
#define GET_TICK  globalwindowhandler::GetTick
#define DELAY     globalwindowhandler::Delay
// for use in command handlers
#define SKIP_GET_KEY   globalwindowhandler::SkipGetKey

#define CLEAR_KEY_BUFFER  globalwindowhandler::ClearKeyBuffer

// enabling also clears the buffer
#define ENABLE_TEXT_INPUT   globalwindowhandler::EnableTextInput
#define DISABLE_TEXT_INPUT  globalwindowhandler::DisableTextInput

#define KEY_EQU(kint_,kstr_)  globalwindowhandler::KeyEqual(kint_, kstr_)


class globalwindowhandler {
public:
  static void KSDLProcessEvents (truth dodelay=false);
  static void KSDLWaitEvent ();
  static int GetKey (truth EmptyBuffer=true, truth skipControllers=false);
  static int ReadKey ();
  static void InstallControlLoop (truth (*What)());
  static void DeInstallControlLoop (truth (*What)());
  static feuLong GetTick () { return Tick; }
  static truth ControlLoopsInstalled () { return Controls; }
  static void EnableControlLoops () { ControlLoopsEnabled = true; }
  static void DisableControlLoops () { ControlLoopsEnabled = false; }
  static void Init ();
  static void SetQuitMessageHandler (truth (*What)()) { QuitMessageHandler = What; }
  static feuLong UpdateTick () { return Tick = SDL_GetTicks()/40; }

  static void Delay (int ms);

  static void SkipGetKey (int key) { doSkipGetKey = key; }

  static festring GetKeyName (int key);
  static int NameToKey (cfestring &name);

  static void ClearKeyBuffer () { KeyBuffer.clear(); }

  static void EnableTextInput () {
    if (!mTextInputEnabled) {
      ClearKeyBuffer();
      mTextInputEnabled = true;
    }
  }

  static void DisableTextInput () {
    ClearKeyBuffer();
    mTextInputEnabled = false;
    mBlockNextChar = 0;
  }

  static bool IsTextInputEnabled () { return mTextInputEnabled; }

  static bool KeyEqual (int Key, const char *kname);

public:
  // only one!
  static void SetKeyPreFilter (bool (*filter) (int Key));

public:
  static bool lastShift, lastCtrl, lastAlt, lastHyper;

private:
  static void FixKMods ();
  static bool ProcessKModsEvent (SDL_Event *Event);

  static void ProcessMessage (SDL_Event *Event);
  static bool ProcessConMessage (SDL_Event *Event);

private:
  static bool (*keyPreFilter) (int Key);
  static int doSkipGetKey;

  static std::vector<int> KeyBuffer;
  static truth (*QuitMessageHandler)();
  static truth (*ControlLoop[MAX_CONTROLS])();
  static int Controls;
  static feuLong Tick;
  static truth ControlLoopsEnabled;
  static bool mTextInputEnabled;
  static char mBlockNextChar;

  static Uint32 nextGameTick;
};


#endif
