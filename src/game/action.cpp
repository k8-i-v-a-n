/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through actset.cpp */


//==========================================================================
//
//  actionprototype::actionprototype
//
//==========================================================================
actionprototype::actionprototype (actionspawner Spawner, cchar *ClassID)
  : Spawner(Spawner)
  , ClassID(ClassID)
{
  Index = protocontainer<action>::Add(this);
}


//==========================================================================
//
//  action::Terminate
//
//==========================================================================
void action::Terminate (truth) {
  if (this->Actor) {
    this->Actor->SetAction(0);
    if (this->Actor->IsPlayer() && this->Actor->IsEnabled()) {
      this->Actor->UpdateComparisonInfo();
    }
    this->Actor = 0; // just in case
  }
  this->Flags |= TERMINATING; // this should do nothing, but...
  // do not delete the action, we might still be in action handler!
  pool::HellWithAction(this);
  //delete this;
}


//==========================================================================
//
//  action::Save
//
//==========================================================================
void action::Save (outputfile &SaveFile) const {
  SaveFile << Flags;
}


//==========================================================================
//
//  action::Load
//
//==========================================================================
void action::Load (inputfile &SaveFile) {
  SaveFile >> Flags;
}


//==========================================================================
//
//  actionprototype::SpawnAndLoad
//
//==========================================================================
action *actionprototype::SpawnAndLoad (inputfile &SaveFile) const {
  action *Action = Spawner(0);
  Action->Load(SaveFile);
  return Action;
}
