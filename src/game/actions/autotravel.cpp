#ifdef HEADER_PHASE
ACTION(autotravel, action)
{
public:
  autotravel () : terra(0) {}

  virtual void Save (outputfile &SaveFile) const;
  virtual void Load (inputfile &SaveFile);
  virtual void Handle ();
  virtual cchar *GetDescription () const;
  virtual truth ShowEnvironment () const { return false; }
  void SetDestination (owterrain *dest) { terra = dest; }
  owterrain *GetDestination () const { return terra; }

protected:
  owterrain *terra;
};


#else


cchar *autotravel::GetDescription () const { return "traveling"; }


void autotravel::Save (outputfile &SaveFile) const {
  action::Save(SaveFile);
  SaveFile << terra;
}


void autotravel::Load (inputfile &SaveFile) {
  action::Load(SaveFile);
  SaveFile >> terra;
}


void autotravel::Handle () {
  GetActor()->EditAP(GetActor()->GetStateAPGain(100)); // gum solution
  GetActor()->AutoTravel(this);
}


#endif
