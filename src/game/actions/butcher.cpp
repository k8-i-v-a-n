#ifdef HEADER_PHASE
ACTION(butcher, action)
{
public:
  butcher () : RightBackupID(0), LeftBackupID(0) {}

  virtual void Save (outputfile &SaveFile) const;
  virtual void Load (inputfile &SaveFile);
  virtual void Handle ();
  virtual void Terminate (truth);
  void SetCorpseID (feuLong What) { CorpseID = What; }
  void SetRightBackupID (feuLong What) { RightBackupID = What; }
  void SetLeftBackupID (feuLong What) { LeftBackupID = What; }
  virtual cchar *GetDescription () const;
  virtual truth ShowEnvironment () const { return false; }
  void SetMoveTool (truth What) { MoveTool = What; }

protected:
  void RestoreTool (feuLong RightBackupID, feuLong LeftBackupID);

protected:
  feuLong RightBackupID;
  feuLong LeftBackupID;
  truth MoveTool;
  feuLong CorpseID;
};


#else


//==========================================================================
//
//  butcher::GetDescription
//
//==========================================================================
cchar *butcher::GetDescription () const {
  return "butchering";
}


//==========================================================================
//
//  butcher::Save
//
//==========================================================================
void butcher::Save (outputfile &SaveFile) const {
  action::Save(SaveFile);
  SaveFile << MoveTool << CorpseID << RightBackupID << LeftBackupID;
}


//==========================================================================
//
//  butcher::Load
//
//==========================================================================
void butcher::Load (inputfile &SaveFile) {
  action::Load(SaveFile);
  SaveFile >> MoveTool >> CorpseID >> RightBackupID >> LeftBackupID;
}


//==========================================================================
//
//  butcher::Terminate
//
//==========================================================================
void butcher::Terminate (truth Finished) {
  if (Flags & TERMINATING) return;
  Flags |= TERMINATING;
  if (!Finished) {
    if (GetActor()->IsPlayer()) {
      ADD_MESSAGE("You stop butchering.");
    } else if (GetActor()->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s stops butchering.", GetActor()->CHAR_NAME(DEFINITE));
    }
  }
  action::Terminate(Finished);
}


//==========================================================================
//
//  butcher::RestoreTool
//
//==========================================================================
void butcher::RestoreTool (feuLong RightBackupID, feuLong LeftBackupID) {
  character *Actor = GetActor();
  if (Actor && Actor->IsEnabled() && (RightBackupID || LeftBackupID)) {
    if (Actor->GetMainWielded()) {
      Actor->GetMainWielded()->MoveTo(Actor->GetStack());
    }

    item *RightBackup = game::SearchItem(RightBackupID);
    if (RightBackup && RightBackup->Exists() && Actor->IsOver(RightBackup)) {
      RightBackup->RemoveFromSlot();
      Actor->SetRightWielded(RightBackup);
    }

    item *LeftBackup = game::SearchItem(LeftBackupID);
    if (LeftBackup && LeftBackup->Exists() && Actor->IsOver(LeftBackup)) {
      LeftBackup->RemoveFromSlot();
      Actor->SetLeftWielded(LeftBackup);
    }
  }
}


//==========================================================================
//
//  butcher::Handle
//
//==========================================================================
void butcher::Handle () {
  character *Actor = GetActor();

  item *Digger = Actor->GetMainWielded();
  if (!Digger) {
    if (this->MoveTool) RestoreTool(this->RightBackupID, this->LeftBackupID);
    Terminate(false);
    return;
  }

  item *Corpse = game::SearchItem(CorpseID);
  if (!Corpse || !Corpse->IsCorpse() || !Corpse->Exists() ||
      !Actor->IsOver(Corpse)/*Corpse->GetPos() != Actor->GetPos()*/)
  {
    if (this->MoveTool) RestoreTool(this->RightBackupID, this->LeftBackupID);
    Terminate(false);
    return;
  }

  if (Corpse->GetSpoilLevel() > 0) {
    ADD_MESSAGE("The corpse is too spoiled.");
    if (MoveTool) RestoreTool(RightBackupID, LeftBackupID);
    Terminate(false);
    return;
  }

  lsquare *sqr = Actor->GetLSquareUnder();
  if (!sqr) {
    if (this->MoveTool) RestoreTool(this->RightBackupID, this->LeftBackupID);
    Terminate(false);
    return;
  }

  /*
  lsquare *Square = Actor->GetNearLSquare(SquareDug);
  olterrain *Terrain = Square->GetOLTerrain();
  if (!Terrain || !Terrain->CanBeDestroyed() || !Terrain->GetMainMaterial()->CanBeDug(Digger->GetMainMaterial())) {
    Terminate(false);
    return;
  }
  */

  /* Save these here because the EditNP call below can cause 'this' to be terminated */
  truth MoveTool = this->MoveTool;
  feuLong RightBackupID = this->RightBackupID;
  feuLong LeftBackupID = this->LeftBackupID;

  corpse *cc = (corpse *)Corpse;
  character *owner = cc->GetDeceased();
  if (!owner) {
    ConLogf("ERROR: no corpse owner!");
    if (MoveTool) RestoreTool(RightBackupID, LeftBackupID);
    Terminate(false);
    return;
  }

  if (owner->IsZombie()) {
    ADD_MESSAGE("This zombie is too rot to be butchered.");
    if (MoveTool) RestoreTool(RightBackupID, LeftBackupID);
    Terminate(false);
    return;
  }

  int FleshCfg = owner->GetFleshMaterial();
  if (!FleshCfg) {
    ADD_MESSAGE("You don't know how to butch this corpse.");
    if (MoveTool) RestoreTool(RightBackupID, LeftBackupID);
    Terminate(false);
    return;
  }

  int vol = Corpse->GetVolume() / 3;
  if (vol < 500) {
    ADD_MESSAGE("There is nothing to butch in this corpse.");
    //Corpse->RemoveFromSlot();
    //Corpse->SendToHell();
    if (MoveTool) RestoreTool(RightBackupID, LeftBackupID);
    Terminate(false);
    return;
  }

  material *FleshMat = MAKE_MATERIAL(FleshCfg);
  if (!FleshMat->IsFlesh()) {
    ADD_MESSAGE("Only flesh can be butched.");
    delete FleshMat;
    if (MoveTool) RestoreTool(RightBackupID, LeftBackupID);
    Terminate(false);
    return;
  }

  Actor->EditExperience(ARM_STRENGTH, /*200*/100, 1 << /*5*/4);
  Actor->EditAP(/*-200000*/-150000 / APBonus(Actor->GetAttribute(DEXTERITY)));
  Actor->EditNP(-500);
  Actor->DexterityAction(/*40*/30);

  //FIXME: k8: there is prolly a better way to do this, but i don't know it yet.
  int lumpVol;

       if (vol >= 5000) lumpVol = 5000 - 500 + RAND_N(1001);
  else if (vol >= 1000) lumpVol = 1000 - 300 + RAND_N(601);
  else lumpVol = vol - RAND_N(100);

  if (lumpVol > vol) lumpVol = vol;

  item *Lump = lump::Spawn(0, NO_MATERIALS);
  material *Mat = FleshMat->SpawnMore(lumpVol);
  Lump->InitMaterials(Mat);
  sqr->AddItem(Lump);

  delete FleshMat;

  //vol -= lumpVol;
  vol = Corpse->GetVolume() - lumpVol;
  bool finishIt = (vol <= 500);

  #if 1
  ConLogf("corpse volume: %d; lump volume: %d; new volume: %d (finish=%d)",
          Corpse->GetVolume(), lumpVol, vol, (int)finishIt);
  /*
  for (int c = 0; c < Corpse->GetMaterials(); ++c) {
    cmaterial *Material = Corpse->GetMaterial(c);
    if (Material) {
      ConLogf("mat #%d: volume=%d", c, Material->GetVolume());
    }
  }
  */
  #endif

  //FIXME: i need a way to set new corpse volume!
  if (!finishIt) {
    for (int c = 0; lumpVol > 0 && c < Corpse->GetMaterials(); ++c) {
      material *Material = Corpse->GetMaterial(c);
      if (Material) {
        const int mvol = Material->GetVolume();
        if (mvol > 1) {
          const int change = Min(mvol - 1, lumpVol);
          const int nvol = mvol - change;
          ConLogf("mat #%d: volume=%d; new volume=%d; left=%d", c, mvol, nvol, lumpVol - change);
          Material->SetVolume(nvol);
          lumpVol -= change;
        } else {
          ConLogf("mat #%d: volume=%d (skipped)", c, Material->GetVolume());
        }
      }
    }
    if (lumpVol <= 1) {
      Corpse->SignalVolumeAndWeightChange();
      if (Corpse->GetVolume() <= 500) {
        finishIt = true;
      }
    } else {
      finishIt = true;
    }
  }

  if (finishIt) {
    Corpse->RemoveFromSlot();
    Corpse->SendToHell();
    if (MoveTool) RestoreTool(RightBackupID, LeftBackupID);
    // Not already terminated?
    if (Actor->GetAction() == this) Terminate(true);
  } else {
    game::DrawEverything();
    // i don't think that we need more...
    if (MoveTool) RestoreTool(RightBackupID, LeftBackupID);
    if (Actor->GetAction() == this) Terminate(true);
  }
}


#endif
