#ifdef HEADER_PHASE
ACTION(consume, action)
{
public:
  consume ()
    : Description("")
    , ConsumingID(0)
    , Nibbling(0)
    , Gulping(0)
    , Spoiling(0)
    {}

  virtual void Save (outputfile &SaveFile) const;
  virtual void Load (inputfile &SaveFile);
  virtual void Handle ();
  virtual void Terminate (truth);
  void SetConsumingID (feuLong What) { ConsumingID = What; }
  virtual truth AllowUnconsciousness () const { return false; }
  virtual truth AllowFoodConsumption () const { return false; }
  virtual cchar *GetDescription () const;
  virtual void SetDescription (cfestring &);
  // taste a liquid
  inline void SetNibbling (truth What) { Nibbling = (What ? 1 : 0); }
  inline truth IsNibbling () { return !!Nibbling; }
  // eat even if bloated (used internally, but why not...)
  inline void SetGulping (int What) { Gulping = What; }
  inline int IsGulping () { return Gulping; }
  // eat even if spoiled (used internally, but why not...)
  inline void SetSpoiling (truth What) { Spoiling = (What ? 1 : 0); }
  inline truth IsSpoiling () { return !!Spoiling; }
  virtual truth IsConsume () const { return true; }

protected:
  static bool NeedToDropLeftover (ccharacter *Actor, item *Consuming);

protected:
  festring Description;
  feuLong ConsumingID;
  int Nibbling;
  int Gulping;
  int Spoiling;
};


#else


//==========================================================================
//
//  consume::GetDescription
//
//==========================================================================
cchar *consume::GetDescription () const {
  return Description.CStr();
}


//==========================================================================
//
//  consume::SetDescription
//
//==========================================================================
void consume::SetDescription (cfestring &What) {
  Description = What;
}


//==========================================================================
//
//  consume::Save
//
//==========================================================================
void consume::Save (outputfile &SaveFile) const {
  action::Save(SaveFile);
  SaveFile << ConsumingID << Description << Nibbling << Gulping << Spoiling;
}


//==========================================================================
//
//  consume::Load
//
//==========================================================================
void consume::Load (inputfile &SaveFile) {
  action::Load(SaveFile);
  SaveFile >> ConsumingID >> Description >> Nibbling >> Gulping >> Spoiling;
}


//==========================================================================
//
//  consume::Handle
//
//==========================================================================
void consume::Handle () {
  item *Consuming = game::SearchItem(ConsumingID);
  if (!Consuming || !Consuming->Exists() || !Actor->IsOver(Consuming)) {
    Terminate(false);
    return;
  }

  character *Actor = GetActor(); // in case the action is interrupted
  // k8: i've seen this bug...
  if (!Actor) {
    Terminate(false);
    return;
  }

  if (!Spoiling && Consuming->GetSpoilLevel() > 0) {
    if (Actor->IsPlayer()) {
      ADD_MESSAGE("This thing is starting to get spoiled.");
      if (game::TruthQuestion(CONST_S("This thing is starting to get spoiled.\nContinue ") + GetDescription() + "?")) {
        Spoiling = true;
      } else {
        Terminate(false);
        return;
      }
    }
  }

  //TODO: do not let intelligent monsters to eat more than they need?
  bool stopOverfed = false;
  if (!Actor->IsPlayer()) {
    // a monster
    if (Actor->EatToBloatedState()) {
      stopOverfed = (Actor->GetHungerState() >= BLOATED);
    } else {
      stopOverfed = (Actor->GetHungerState() >= SATIATED);
    }
  } else {
    // the player
    if (Gulping >= 0 && Actor->GetHungerState() >= BLOATED) {
      ADD_MESSAGE("You have a really hard time getting all this down your throat.");
      if (game::TruthQuestion(CONST_S("You have a really hard time getting all this down your throat.\nContinue ") + GetDescription() + "?")) {
        Gulping = -1;
      } else {
        stopOverfed = true;
      }
    } else if (Gulping == 0 && !Nibbling && Actor->GetHungerState() >= SATIATED) {
      ADD_MESSAGE("You are satiated.");
      if (game::TruthQuestion(CONST_S("You are satiated.\nContinue ") + GetDescription() + "?")) {
        Gulping = 1;
      } else {
        stopOverfed = true;
      }
    }
  }

  if (stopOverfed) {
    Terminate(false);
    return;
  }

  if (!Actor->IsPlayer() && !Consuming->CanBeEatenByAI(Actor)) {
    // item may be spoiled after action was started
    // but don't terminate if this is a liquid, this is NPC drinking something
    if (Actor->IsDrinkableCanister(Consuming)) {
      #if 0
      ConLogf("CONSUME drinking: keep going.");
      #endif
    } else {
      //ConLogf("CONSUME terminated!");
      Terminate(false);
      return;
    }
  }

  /* Note: if backupped Actor has died of food effect,
     Action is deleted automatically, so we mustn't Terminate it.

     SHIT! this is UB! the action is `delete`d, and, we're accesing
           random memory in this case!
  */

  int consumeAmount = 500;
  if (Nibbling) {
    // the bottle usually contains a liter, so we can "taste" the bottle 4 times.
    // this looks slightly overpowered. a can can be "tasted" two times.
    // so let's introduce some randomness!
    consumeAmount = 250 + RAND_N(Max(50, 130 - Actor->GetAttribute(DEXTERITY)));
  }
  truth stopEating = Consuming->Consume(Actor, consumeAmount);
  if (Actor->IsEnabled()) {
    // if we won't terminate the action on nibbling, we'll drink everything
    if (stopEating || Nibbling) {
      if (GetActor() == Actor && Actor->GetAction() == this) {
        IvanAssert(Actor->Exists());
        Terminate(true);
      }
    } else if (Actor->GetHungerState() == OVER_FED) {
      Actor->DeActivateVoluntaryAction(CONST_S("You are about to choke on this stuff."));
      Actor->Vomit(Actor->GetPos(), 500 + RAND_N(500));
    }
  }
}


//==========================================================================
//
//  consume::NeedToDropLeftover
//
//==========================================================================
bool consume::NeedToDropLeftover (ccharacter *Actor, item *Consuming) {
  if (!Actor || !Consuming || !Consuming->Exists()) return false;
  if (Actor->IsPlayer()) {
    return (!game::IsInWilderness() && game::CheckDropLeftover(Consuming));
  } else {
    if (Consuming->IsBottle() || Consuming->IsCan()) {
      return (!game::IsInWilderness() && !Consuming->GetSecondaryMaterial());
    }
    // any other leftover will be dropped
    return true;
  }
}


//==========================================================================
//
//  consume::Terminate
//
//==========================================================================
void consume::Terminate (truth Finished) {
  if (Flags & TERMINATING) return;
  Flags |= TERMINATING;
  character *Actor = GetActor();
  if (Actor) {
    item *Consuming = game::SearchItem(ConsumingID);
    if (Actor->IsPlayer()) {
      ADD_MESSAGE("You %s %s.", Finished ? "finish" : "stop", Description.CStr());
    } else if (Actor->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s %s %s.", Actor->CHAR_NAME(DEFINITE), Finished ? "finishes" : "stops", Description.CStr());
    }
    if (Finished) {
      if (NeedToDropLeftover(Actor, Consuming)) {
        if (Actor->IsPlayer()) {
          ADD_MESSAGE("%s dropped.", Consuming->GetName(INDEFINITE, 1).CStr());
        } else if (Actor->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s dropped %s.", Actor->CHAR_NAME(DEFINITE),
                      Consuming->GetName(INDEFINITE, 1).CStr());
        }
        Consuming->RemoveFromSlot();
        Actor->GetStackUnder()->AddItem(Consuming);
        Actor->DexterityAction(2);
      }
    } else if (Consuming && Consuming->Exists()) {
      material *ConsumeMaterial = Consuming->GetConsumeMaterial(Actor);
      if (ConsumeMaterial) ConsumeMaterial->FinishConsuming(Actor);
    }
  } else {
    ConLogf("OOPS! consume action has no actor!");
  }
  action::Terminate(Finished);
}


#endif
