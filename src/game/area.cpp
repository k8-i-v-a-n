/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through areaset.cpp */


//==========================================================================
//
//  operator << (EntryInfo)
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const EntryInfo &info) {
  SaveFile << info.Level;
  SaveFile << info.UseTick;
  SaveFile << info.Pos;
  return SaveFile;
}


//==========================================================================
//
//  operator >> (EntryInfo)
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, EntryInfo &info) {
  SaveFile >> info.Level;
  SaveFile >> info.UseTick;
  SaveFile >> info.Pos;
  return SaveFile;
}


//==========================================================================
//
//  operator << (MapMark)
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const area::MapMark &mark) {
  int32_t version = 0;
  SaveFile << version;
  SaveFile << mark.index << mark.pos << mark.color << mark.type << mark.text;
  return SaveFile;
}


//==========================================================================
//
//  operator >> (MapMark)
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, area::MapMark &mark) {
  int32_t version = -1;
  SaveFile >> version;
  if (version != 0) {
    ABORT("invalid map mark version (%d)!", version);
  }
  SaveFile >> mark.index >> mark.pos >> mark.color >> mark.type >> mark.text;
  return SaveFile;
}


// ////////////////////////////////////////////////////////////////////////// //
// area
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  area::area
//
//==========================================================================
area::area () {
}


//==========================================================================
//
//  area::area
//
//==========================================================================
area::area (int InitXSize, int InitYSize) {
  Initialize(InitXSize, InitYSize);
}


//==========================================================================
//
//  area::~area
//
//==========================================================================
area::~area () {
  for (feuLong c = 0; c < XSizeTimesYSize; ++c) delete Map[0][c];
  delete [] Map;
  delete [] FlagMap;
}


//==========================================================================
//
//  area::Initialize
//
//==========================================================================
void area::Initialize (int InitXSize, int InitYSize) {
  XSize = InitXSize;
  YSize = InitYSize;
  XSizeTimesYSize = XSize * YSize;
  Border = rect(0, 0, XSize - 1, YSize - 1);
  Alloc2D(Map, XSize, YSize);
  Alloc2D(FlagMap, XSize, YSize);
  memset(FlagMap[0], 0, XSizeTimesYSize * sizeof(uChar));
}


//==========================================================================
//
//  area::Save
//
//==========================================================================
void area::Save (outputfile &SaveFile) const {
  SaveFile << XSize << YSize << EntryMap << MapMarks;
  SaveFile.WriteBytes(reinterpret_cast<cchar *>(FlagMap[0]), XSizeTimesYSize*sizeof(uChar));
}


//==========================================================================
//
//  area::Load
//
//==========================================================================
void area::Load (inputfile &SaveFile) {
  game::SetAreaInLoad(this);
  XSize = -1; YSize = -1;
  SaveFile >> XSize >> YSize;
  if (SaveFile.Eof()) {
    ABORT("Area file %s corrupted!", SaveFile.GetFileName().CStr());
  }
  if (XSize < 2 || YSize < 2) {
    ABORT("Area file %s corrupted!", SaveFile.GetFileName().CStr());
  }
  SaveFile >> EntryMap >> MapMarks;
  XSizeTimesYSize = XSize * YSize;
  Border = rect(0, 0, XSize - 1, YSize - 1);
  Alloc2D(Map, XSize, YSize);
  Alloc2D(FlagMap, XSize, YSize);
  SaveFile.ReadBytes(reinterpret_cast<char *>(FlagMap[0]), XSizeTimesYSize*sizeof(uChar));
}


//==========================================================================
//
//  area::SendNewDrawRequest
//
//==========================================================================
void area::SendNewDrawRequest () {
  cint XMin = Max(game::GetCamera().X, 0);
  cint YMin = Max(game::GetCamera().Y, 0);
  cint XMax = Min(XSize, game::GetCamera().X + game::GetScreenXSize());
  cint YMax = Min(YSize, game::GetCamera().Y + game::GetScreenYSize());
  for (int x = XMin; x < XSize && x < XMax; ++x) {
    for (int y = YMin; y < YMax; ++y) {
      Map[x][y]->SendStrongNewDrawRequest();
    }
  }

  igraph::GetBackGround()->FastBlit(DOUBLE_BUFFER);

  // map frame
  int xpos, ypos;
  if (ivanconfig::GetStatusOnLeft()) {
    xpos = 110; ypos = 30;
  } else {
    xpos = 14; ypos = 30;
  }

  //DOUBLE_BUFFER->DrawRectangle(14, 30, 17 + (game::GetScreenXSize() << 4), 33 + (game::GetScreenYSize() << 4), DARK_GRAY, true);
  //DOUBLE_BUFFER->Fill(16, 32, game::GetScreenXSize() << 4, game::GetScreenYSize() << 4, BLACK);
  DOUBLE_BUFFER->DrawRectangle(xpos, ypos,
                               xpos + 3 + (game::GetScreenXSize() << 4),
                               ypos + 3 + (game::GetScreenYSize() << 4), DARK_GRAY, true);
  DOUBLE_BUFFER->Fill(xpos + 2, ypos + 2, game::GetScreenXSize() << 4, game::GetScreenYSize() << 4, BLACK);
}


//==========================================================================
//
//  area::GetNeighbourSquare
//
//==========================================================================
square *area::GetNeighbourSquare (v2 Pos, int I) const {
  Pos += game::GetMoveVector(I);
  if (Pos.X >= 0 && Pos.Y >= 0 && Pos.X < XSize && Pos.Y < YSize) return Map[Pos.X][Pos.Y];
  return 0;
}


//==========================================================================
//
//  area::SetEntryPos
//
//==========================================================================
void area::SetEntryPos (int I, v2 Pos) {
  //EntryMap.insert(std::make_pair(I, Pos));
  EntryInfo ee;
  ee.Level = I;
  ee.UseTick = 0;
  ee.Pos = Pos;
  EntryMap.push_back(ee);
}


//==========================================================================
//
//  area::ClearEntryPoints
//
//==========================================================================
void area::ClearEntryPoints () {
  EntryMap.clear();
}


//==========================================================================
//
//  area::MarkEntryAsUsed
//
//==========================================================================
void area::MarkEntryAsUsed (v2 pos) {
  for (auto &&e : EntryMap) {
    if (e.Pos == pos) e.UseTick = Max(0, (int)game::GetTick());
  }
}


//==========================================================================
//
//  area::FindEntryTo
//
//==========================================================================
v2 area::FindEntryTo (int I, truth markUsed) {
  size_t idx = 0;
  sLong bestTick = -1;
  for (size_t f = 0; f != EntryMap.size(); f += 1) {
    if (EntryMap[f].Level == I && EntryMap[f].UseTick > bestTick) {
      bestTick = EntryMap[f].UseTick;
      idx = f;
    }
  }
  if (bestTick >= 0) {
    EntryMap[idx].UseTick = Max(0, (int)game::GetTick());
    return EntryMap[idx].Pos;
  }
  return ERROR_V2;
}


//==========================================================================
//
//  area::WipeMapMarks
//
//==========================================================================
void area::WipeMapMarks () {
  MapMarks.clear();
}


//==========================================================================
//
//  area::FindMapMarkAt
//
//==========================================================================
int area::FindMapMarkAt (v2 pos) const {
  for (size_t f = 0; f != MapMarks.size(); f += 1) {
    const MapMark &mark = MapMarks[f];
    if (mark.pos == pos) {
      return (int)f;
    }
  }
  return -1;
}


//==========================================================================
//
//  area::FindNearestMapMark
//
//==========================================================================
int area::FindNearestMapMark (v2 pos) const {
  int bestIndex = -1;
  int bestDist = 0x7fffffff;
  for (size_t f = 0; f != MapMarks.size(); f += 1) {
    const MapMark &mark = MapMarks[f];
    if (mark.pos != pos) {
      const int dist = pos.GetSquaredDistance(mark.pos);
      if (dist < bestDist) {
        bestIndex = (int)f;
        bestDist = dist;
      }
    }
  }
  return bestIndex;
}


//==========================================================================
//
//  area::GetMapMarkCount
//
//==========================================================================
int area::GetMapMarkCount () const {
  return (int)MapMarks.size();
}


//==========================================================================
//
//  area::GetMapMark
//
//  return `false` if no mark with such index; `mark` is cleared in this case.
//  `mark` can be `nullptr`
//
//==========================================================================
bool area::GetMapMark (int index, MapMark *mark) const {
  if (index >= 0 && index < (int)MapMarks.size()) {
    if (mark) *mark = MapMarks[index];
    return true;
  } else {
    if (mark) mark->Wipe();
    return false;
  }
}


//==========================================================================
//
//  area::RenumberMapMarks
//
//==========================================================================
void area::RenumberMapMarks () {
  // sort by coords, why not?
  std::sort(MapMarks.begin(), MapMarks.end(),
    [&] (const MapMark &a, const MapMark &b) {
      const int dy = a.pos.Y - b.pos.Y;
      if (dy < 0) return true;
      if (dy > 0) return false;
      return (a.pos.X < b.pos.X);
    });
  for (size_t f = 0; f != MapMarks.size(); f += 1) {
    MapMarks[f].index = (int)f;
  }
}


//==========================================================================
//
//  area::AppendMapMark
//
//  index doesn't matter.
//  if there is a mark at the given coords, it will be replaced.
//
//==========================================================================
void area::AppendMapMark (const MapMark *mark) {
  IvanAssert(mark);
  int oldidx = FindMapMarkAt(mark->pos);
  if (oldidx >= 0) {
    // replace
    MapMarks[oldidx].ReplaceWith(*mark);
  } else {
    // append
    if ((int)MapMarks.size() >= XSize * YSize) {
      ABORT("too many map marks. this should not happen!");
    }
    MapMark newmark = *mark;
    newmark.index = (int)MapMarks.size();
    MapMarks.push_back(newmark);
  }
  RenumberMapMarks();
}


//==========================================================================
//
//  area::DeleteMapMark
//
//  delete map mark, renumber marks
//
//==========================================================================
void area::DeleteMapMark (int index) {
  IvanAssert(index >= 0 && index < (int)MapMarks.size());
  MapMarks.erase(MapMarks.begin() + (size_t)index);
  RenumberMapMarks();
}


//==========================================================================
//
//  area::InsetmMapMark
//
//  insert map mark at the given index, renumber marks
//
//==========================================================================
void area::InsertMapMark (const MapMark *mark) {
  IvanAssert(mark);
  IvanAssert(mark->index >= 0 && mark->index <= (int)MapMarks.size());
  if (mark->index == (int)MapMarks.size()) {
    AppendMapMark(mark);
  } else {
    int insidx = mark->index;
    int oldidx = FindMapMarkAt(mark->pos);
    if (oldidx >= 0) {
      // delete old mark
      if (oldidx < insidx) insidx -= 1;
      MapMarks.erase(MapMarks.begin() + (size_t)oldidx);
    }
    MapMarks.insert(MapMarks.begin() + (size_t)insidx, *mark);
    RenumberMapMarks();
  }
}
