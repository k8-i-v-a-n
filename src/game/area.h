/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __AREA_H__
#define __AREA_H__

#include <map>

#include "rect.h"
#include "festring.h"


class character;
class square;
class bitmap;
class outputfile;
class inputfile;


struct EntryInfo {
  sLong Level = 0;
  sLong UseTick = 0;
  v2 Pos = v2(0, 0);
};


outputfile &operator << (outputfile &SaveFile, const EntryInfo &info);
inputfile &operator >> (inputfile &SaveFile, EntryInfo &info);


// ////////////////////////////////////////////////////////////////////////// //
class area {
public:
  area ();
  area (int, int);
  virtual ~area ();

  virtual bool IsLevel () const { return false; }

public:
  virtual void Draw (truth AnimationDraw) const = 0;
  virtual void DrawMiniMap (v2 XY0, v2 Size) const = 0;
  void Save (outputfile &) const;
  void Load (inputfile &);
  int GetFlag (v2 Pos) const { return FlagMap[Pos.X][Pos.Y]; }
  void AddFlag (v2 Pos, int What) { FlagMap[Pos.X][Pos.Y] |= What; }
  square *GetSquare (v2 Pos) const { return Map[Pos.X][Pos.Y]; }
  square *GetSquare (int x, int y) const { return Map[x][y]; }
  int GetXSize () const { return XSize; }
  int GetYSize () const { return YSize; }
  void SendNewDrawRequest ();
  void Initialize (int, int);
  square *GetNeighbourSquare (v2, int) const;
  truth IsValidPos (v2 Pos) const { return Pos.X >= 0 && Pos.Y >= 0 && Pos.X < XSize && Pos.Y < YSize; }
  truth IsValidPos (int X, int Y) const { return X >= 0 && Y >= 0 && X < XSize && Y < YSize; }
  const rect &GetBorder () const { return Border; }
  void SetEntryPos (int, v2);
  void ClearEntryPoints ();

  void MarkEntryAsUsed (v2 pos);

  v2 FindEntryTo (int I, truth markUsed=true);

  virtual truth IsGlobalRainRandom () const { return false; }
  virtual void SetGlobalRainRandom (truth value) {}

public:
  // minimap marks. independent of squares.
  // there could be only one map mark at the given coords.
  // this invariant is respected by the API.

  struct MapMark {
    int32_t index;
    v2 pos;
    col16 color; // `TRANSPARENT_COLOR` if not specified
    festring text;
    int32_t type; // not used yet. can be used in future for icon type.

    inline MapMark () : index(-1), pos(0, 0), color(TRANSPARENT_COLOR), text(), type(0) {}

    inline void Wipe () {
      index = -1; pos = v2(0, 0);
      color = TRANSPARENT_COLOR; text.Empty();
      type = 0;
    }

    inline void ReplaceWith (const MapMark &newmark) {
      pos = newmark.pos;
      color = newmark.color;
      text = newmark.text;
      type = newmark.type;
    }
  };

  void WipeMapMarks ();
  // return `-1` on failure.
  int FindMapMarkAt (v2 pos) const;
  // `pos` itself is ignored.
  // return `-1` on failure.
  int FindNearestMapMark (v2 pos) const;
  int GetMapMarkCount () const;
  // return `false` if no mark with such index; `mark` is cleared in this case.
  // `mark` can be `nullptr`
  bool GetMapMark (int index, MapMark *mark) const;
  // index doesn't matter.
  // if there is a mark at the given coords, it will be replaced.
  void AppendMapMark (const MapMark *mark);
  // delete map mark, renumber marks
  void DeleteMapMark (int index);
  // insert map mark at the given index, renumber marks.
  // if map mark with the given coords already exists, it will be
  // removed *AFTER* inserting this one.
  void InsertMapMark (const MapMark *mark);

protected:
  void RenumberMapMarks ();

protected:
  square ***Map;
  uChar **FlagMap;
  int XSize, YSize;
  feuLong XSizeTimesYSize;
  rect Border;
  //k8: we don't have enough entries to justify using hash table.
  //    besides, there could be more than one for the given index.
  //std::map<int, v2> EntryMap;
  std::vector<EntryInfo> EntryMap;
  // minimap marks
  std::vector<MapMark> MapMarks;
};


#endif
