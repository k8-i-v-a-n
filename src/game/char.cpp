/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through charset.cpp */
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


#define CC_SHOW_FULL_POS_DESC

#ifdef CC_SHOW_FULL_POS_DESC
# define PosDescMethod  AddInventoryEntry
#else
# define PosDescMethod  AddDescriptionEntry
#endif

//#define PRE_CHECK_EQUIP_CHECK
//#define DEBUG_FINDER_SQUARE_CHECK

/*
 * 0: up-left
 * 1: up
 * 2: up-right
 * 3: left
 * 4: right
 * 5: down-left
 * 6: down
 * 7: down-right
 * 8: stand still
 */
enum {
  MDIR_UP_LEFT,
  MDIR_UP,
  MDIR_UP_RIGHT,
  MDIR_LEFT,
  MDIR_RIGHT,
  MDIR_DOWN_LEFT,
  MDIR_DOWN,
  MDIR_DOWN_RIGHT,
  MDIR_STAND,
};


//==========================================================================
//
//  GetHitResultCStr
//
//==========================================================================
const char *GetHitResultCStr (int hitres) {
  switch (hitres) {
    case HAS_HIT: return "hit";
    case HAS_BLOCKED: return "blocked";
    case HAS_DODGED: return "dodged";
    case HAS_DIED: return "died";
    case DID_NO_DAMAGE: return "nodamage";
    case HAS_FAILED: return "failed";
    default: break;
  }
  return "<unknown>";
}


/* These statedata structs contain functions and values used for handling
 * states. Remember to update them. All normal states must have
 * PrintBeginMessage and PrintEndMessage functions and a Description string.
 * BeginHandler, EndHandler, Handler (called each tick) and IsAllowed are
 * optional, enter zero if the state doesn't need one. If the SECRET flag
 * is set, Description is not shown in the panel without magical means.
 * You can also set some source (SRC_*) and duration (DUR_*) flags, which
 * control whether the state can be randomly activated in certain situations.
 * These flags can be found in ivandef.h. RANDOMIZABLE sets all source
 * & duration flags at once. */
struct statedata {
  const char *Description;
  int Flags;
  void (character::*PrintBeginMessage) () const;
  void (character::*PrintEndMessage) () const;
  void (character::*BeginHandler) ();
  void (character::*EndHandler) ();
  void (character::*Handler) ();
  truth (character::*IsAllowed) () const;
  void (character::*SituationDangerModifier) (double &) const;
};


//WARNING! state count and state order MUST be synced with "define.def"
const statedata StateData[STATES] =
{
  {
    "Polymorphed",
    NO_FLAGS,
    0,
    0,
    0,
    &character::EndPolymorph,
    0,
    0,
    0
  }, {
    "Hasted",
    RANDOMIZABLE&~(SRC_MUSHROOM|SRC_EVIL),
    &character::PrintBeginHasteMessage,
    &character::PrintEndHasteMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "Slowed",
    RANDOMIZABLE&~SRC_GOOD,
    &character::PrintBeginSlowMessage,
    &character::PrintEndSlowMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "PolyControl",
    RANDOMIZABLE&~(SRC_MUSHROOM|SRC_EVIL|SRC_GOOD),
    &character::PrintBeginPolymorphControlMessage,
    &character::PrintEndPolymorphControlMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "LifeSaved",
    SECRET,
    &character::PrintBeginLifeSaveMessage,
    &character::PrintEndLifeSaveMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "Lycanthropy",
    SECRET|SRC_FOUNTAIN|SRC_CONFUSE_READ|DUR_FLAGS,
    &character::PrintBeginLycanthropyMessage,
    &character::PrintEndLycanthropyMessage,
    0,
    0,
    &character::LycanthropyHandler,
    0,
    &character::LycanthropySituationDangerModifier
  }, {
    "Invisible",
    RANDOMIZABLE&~(SRC_MUSHROOM|SRC_EVIL),
    &character::PrintBeginInvisibilityMessage,
    &character::PrintEndInvisibilityMessage,
    &character::BeginInvisibility,
    &character::EndInvisibility,
    0,
    0,
    0
  }, {
    "Infravision",
    RANDOMIZABLE&~(SRC_MUSHROOM|SRC_EVIL),
    &character::PrintBeginInfraVisionMessage,
    &character::PrintEndInfraVisionMessage,
    &character::BeginInfraVision,
    &character::EndInfraVision,
    0,
    0,
    0
  }, {
    "ESP",
    RANDOMIZABLE&~SRC_EVIL,
    &character::PrintBeginESPMessage,
    &character::PrintEndESPMessage,
    &character::BeginESP,
    &character::EndESP,
    0,
    0,
    0
  }, {
    "Poisoned",
    DUR_TEMPORARY,
    &character::PrintBeginPoisonedMessage,
    &character::PrintEndPoisonedMessage,
    0,
    0,
    &character::PoisonedHandler,
    &character::CanBePoisoned,
    &character::PoisonedSituationDangerModifier
  }, {
    "Teleporting",
    SECRET|(RANDOMIZABLE&~(SRC_MUSHROOM|SRC_GOOD)),
    &character::PrintBeginTeleportMessage,
    &character::PrintEndTeleportMessage,
    0,
    0,
    &character::TeleportHandler,
    0,
    0
  }, {
    "Polymorphitis",
    SECRET|(RANDOMIZABLE&~(SRC_MUSHROOM|SRC_GOOD)),
    &character::PrintBeginPolymorphMessage,
    &character::PrintEndPolymorphMessage,
    0,
    0,
    &character::PolymorphHandler,
    0,
    &character::PolymorphingSituationDangerModifier
  }, {
    "TeleControl",
    RANDOMIZABLE&~(SRC_MUSHROOM|SRC_EVIL),
    &character::PrintBeginTeleportControlMessage,
    &character::PrintEndTeleportControlMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "Panicked",
    NO_FLAGS,
    &character::PrintBeginPanicMessage,
    &character::PrintEndPanicMessage,
    &character::BeginPanic,
    &character::EndPanic,
    0,
    &character::CanPanic,
    &character::PanicSituationDangerModifier
  }, {
    "Confused",
    SECRET|(RANDOMIZABLE&~(DUR_PERMANENT|SRC_GOOD)),
    &character::PrintBeginConfuseMessage,
    &character::PrintEndConfuseMessage,
    0,
    0,
    0,
    &character::CanBeConfused,
    &character::ConfusedSituationDangerModifier
  }, {
    "Parasite (tapeworm)",
    SECRET|(RANDOMIZABLE&~DUR_TEMPORARY),
    &character::PrintBeginParasitizedMessage,
    &character::PrintEndParasitizedMessage,
    0,
    0,
    &character::ParasitizedHandler,
    &character::CanBeParasitized,
    &character::ParasitizedSituationDangerModifier
  }, {
    "Searching",
    NO_FLAGS,
    &character::PrintBeginSearchingMessage,
    &character::PrintEndSearchingMessage,
    0,
    0,
    &character::SearchingHandler,
    0,
    0
  }, {
    "Gas Immunity",
    SECRET|(RANDOMIZABLE&~(SRC_GOOD|SRC_EVIL)),
    &character::PrintBeginGasImmunityMessage,
    &character::PrintEndGasImmunityMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "Levitating",
    RANDOMIZABLE&~SRC_EVIL,
    &character::PrintBeginLevitationMessage,
    &character::PrintEndLevitationMessage,
    0,
    &character::EndLevitation,
    0,
    0,
    0
  }, {
    "Leprosy",
    SECRET|(RANDOMIZABLE&~DUR_TEMPORARY),
    &character::PrintBeginLeprosyMessage,
    &character::PrintEndLeprosyMessage,
    &character::BeginLeprosy,
    &character::EndLeprosy,
    &character::LeprosyHandler,
    0,
    &character::LeprosySituationDangerModifier
  }, {
    "Hiccups",
    SRC_FOUNTAIN|SRC_CONFUSE_READ|DUR_FLAGS,
    &character::PrintBeginHiccupsMessage,
    &character::PrintEndHiccupsMessage,
    0,
    0,
    &character::HiccupsHandler,
    0,
    &character::HiccupsSituationDangerModifier
  }, {
    "Vampirism",
    DUR_FLAGS, //perhaps no fountain, no secret and no confuse read either: SECRET|SRC_FOUNTAIN|SRC_CONFUSE_READ|
    &character::PrintBeginVampirismMessage,
    &character::PrintEndVampirismMessage,
    0,
    0,
    &character::VampirismHandler,
    0,
    &character::VampirismSituationDangerModifier
  }, {
    "Swimming",
    SECRET,
    &character::PrintBeginSwimmingMessage,
    &character::PrintEndSwimmingMessage,
    &character::BeginSwimming,
    &character::EndSwimming,
    0,
    0,
    0
  }, {
    "Detecting",
    SECRET|(RANDOMIZABLE&~(SRC_MUSHROOM|SRC_EVIL)),
    &character::PrintBeginDetectMessage,
    &character::PrintEndDetectMessage,
    0,
    0,
    &character::DetectHandler,
    0,
    0
  }, {
    "Ethereal",
    NO_FLAGS,
    &character::PrintBeginEtherealityMessage,
    &character::PrintEndEtherealityMessage,
    &character::BeginEthereality,
    &character::EndEthereality,
    0,
    0,
    0
  }, {
    "Fearless",
    RANDOMIZABLE&~SRC_EVIL,
    &character::PrintBeginFearlessMessage,
    &character::PrintEndFearlessMessage,
    &character::BeginFearless,
    &character::EndFearless,
    0,
    0,
    0
  }, {
    "Polymorph Locked",
    SECRET,
    &character::PrintBeginPolymorphLockMessage,
    &character::PrintEndPolymorphLockMessage,
    0,
    0,
    &character::PolymorphLockHandler,
    0,
    0
  }, {
    "Regenerating",
    SECRET|(RANDOMIZABLE&~SRC_EVIL),
    &character::PrintBeginRegenerationMessage,
    &character::PrintEndRegenerationMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "Disease Immunity",
    SECRET|(RANDOMIZABLE&~SRC_EVIL),
    &character::PrintBeginDiseaseImmunityMessage,
    &character::PrintEndDiseaseImmunityMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "Teleport Locked",
    SECRET,
    &character::PrintBeginTeleportLockMessage,
    &character::PrintEndTeleportLockMessage,
    0,
    0,
    &character::TeleportLockHandler,
    0,
    0
  }, {
    "Fasting",
    SECRET|(RANDOMIZABLE&~SRC_EVIL),
    &character::PrintBeginFastingMessage,
    &character::PrintEndFastingMessage,
    0,
    0,
    0,
    0,
    0
  }, {
    "Parasite (mindworm)",
    SECRET|DUR_TEMPORARY|SRC_FOUNTAIN,
    &character::PrintBeginMindwormedMessage,
    &character::PrintEndMindwormedMessage,
    0,
    0,
    &character::MindwormedHandler,
    &character::CanBeParasitized, // We are using TorsoIsAlive right now, but I think it's OK,
                                  // because with unliving torso, your head will not be much better for mind worms.
    &character::ParasitizedSituationDangerModifier
  }
};


//==========================================================================
//
//  character::GetStateDesctiption
//
//==========================================================================
const char *character::GetStateDesctiption (int sidx) const {
  return (sidx >= 0 && sidx < STATES ? StateData[sidx].Description : "<invalid>");
}


void character::CreateInitialEquipment (int SpecialFlags) { AddToInventory(DataBase->Inventory, SpecialFlags); }
void character::EditAP (sLong What) { AP = Limit<sLong>(AP+What, -12000, 1200); }
int character::GetRandomStepperBodyPart () const { return TORSO_INDEX; }
void character::GainIntrinsic (sLong What) { BeginTemporaryState(What, PERMANENT); }
truth character::IsUsingArms () const { return GetAttackStyle() & USE_ARMS; }
truth character::IsUsingLegs () const { return GetAttackStyle() & USE_LEGS; }
truth character::IsUsingHead () const { return GetAttackStyle() & USE_HEAD; }
void character::CalculateAllowedWeaponSkillCategories () { AllowedWeaponSkillCategories = MARTIAL_SKILL_CATEGORIES; }
festring character::GetBeVerb () const { return IsPlayer() ? CONST_S("are") : CONST_S("is"); }
void character::SetEndurance (int What) { BaseExperience[ENDURANCE] = What * EXP_MULTIPLIER; }
void character::SetPerception (int What) { BaseExperience[PERCEPTION] = What * EXP_MULTIPLIER; }
void character::SetIntelligence (int What) { BaseExperience[INTELLIGENCE] = What * EXP_MULTIPLIER; }
void character::SetWisdom (int What) { BaseExperience[WISDOM] = What * EXP_MULTIPLIER; }
void character::SetWillPower (int What) { BaseExperience[WILL_POWER] = What * EXP_MULTIPLIER; }
void character::SetCharisma (int What) { BaseExperience[CHARISMA] = What * EXP_MULTIPLIER; }
//void character::SetMana (int What) { BaseExperience[MANA] = What * EXP_MULTIPLIER; }
truth character::IsOnGround () const { return MotherEntity && MotherEntity->IsOnGround(); }
truth character::LeftOversAreUnique () const { return GetArticleMode() || AssignedName.GetSize(); }
truth character::HomeDataIsValid () const { return (HomeData && HomeData->Level == GetLSquareUnder()->GetLevelIndex() && HomeData->Dungeon == GetLSquareUnder()->GetDungeonIndex()); }
void character::SetHomePos (v2 Pos) { HomeData->Pos = Pos; }
cchar *character::FirstPersonUnarmedHitVerb () const { return "hit"; }
cchar *character::FirstPersonCriticalUnarmedHitVerb () const { return "critically hit"; }
cchar *character::ThirdPersonUnarmedHitVerb () const { return "hits"; }
cchar *character::ThirdPersonCriticalUnarmedHitVerb () const { return "critically hits"; }
cchar *character::FirstPersonKickVerb () const { return "kick"; }
cchar *character::FirstPersonCriticalKickVerb () const { return "critically kick"; }
cchar *character::ThirdPersonKickVerb () const { return "kicks"; }
cchar *character::ThirdPersonCriticalKickVerb () const { return "critically kicks"; }
cchar *character::FirstPersonBiteVerb () const { return "bite"; }
cchar *character::FirstPersonCriticalBiteVerb () const { return "critically bite"; }
cchar *character::ThirdPersonBiteVerb () const { return "bites"; }
cchar *character::ThirdPersonCriticalBiteVerb () const { return "critically bites"; }
cchar *character::UnarmedHitNoun () const { return "attack"; }
cchar *character::KickNoun () const { return "kick"; }
cchar *character::BiteNoun () const { return "attack"; }
cchar *character::GetEquipmentName (int) const { return ""; }
const std::list<feuLong> &character::GetOriginalBodyPartID (int I) const { return OriginalBodyPartID[I]; }
square *character::GetNeighbourSquare (int I) const { return GetSquareUnder()->GetNeighbourSquare(I); }
lsquare *character::GetNeighbourLSquare (int I) const { return static_cast<lsquare *>(GetSquareUnder())->GetNeighbourLSquare(I); }
//wsquare *character::GetNeighbourWSquare (int I) const { return static_cast<wsquare *>(GetSquareUnder())->GetNeighbourWSquare(I); }
god *character::GetMasterGod () const { return game::GetGod(GetConfig()); }
col16 character::GetBodyPartColorA (int, truth) const { return GetSkinColor(); }
col16 character::GetBodyPartColorB (int, truth) const { return GetTorsoMainColor(); }
col16 character::GetBodyPartColorC (int, truth) const { return GetBeltColor(); } // sorry...
col16 character::GetBodyPartColorD (int, truth) const { return GetTorsoSpecialColor(); }
int character::GetRandomApplyBodyPart () const { return TORSO_INDEX; }
truth character::IsPet () const { return GetTeam()->GetID() == PLAYER_TEAM; }
character* character::GetLeader () const { return GetTeam()->GetLeader(); }
festring character::GetZombieDescription () const { return CONST_S(" of ") + GetName(INDEFINITE); }
truth character::BodyPartCanBeSevered (int I) const { return I; }
truth character::HasBeenSeen () const { return (DataBase->Flags & HAS_BEEN_SEEN); }
truth character::IsTemporary () const { return GetTorso()->GetLifeExpectancy(); }
cchar *character::GetNormalDeathMessage () const { return "killed @k"; }


// ////////////////////////////////////////////////////////////////////////// //
static truth isEncryptedScroll (item *i) { return (i && i->IsEncryptedScroll()); }

truth character::HasEncryptedScroll () const {
  return GeneralHasItem(::isEncryptedScroll, false); // not in containers
  //return combineequipmentpredicates()(this, &item::IsEncryptedScroll, 1);
}

truth character::RemoveEncryptedScroll () {
  return GeneralRemoveItem(::isEncryptedScroll, false, true) != 0;
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isElpuriHead (item *i) { return (i && i->IsHeadOfElpuri()); }

truth character::HasHeadOfElpuri () const {
  return GeneralHasItem(::isElpuriHead, true); // Petrus can see into containers
  //return combineequipmentpredicates()(this, &item::IsHeadOfElpuri, 1);
}

truth character::RemoveHeadOfElpuri () {
  return (GeneralRemoveItem(::isElpuriHead, false, false) != 0);
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isPetrussNut (item *i) { return (i && i->IsPetrussNut()); }

truth character::HasPetrussNut () const {
  return GeneralHasItem(::isPetrussNut, true); // gods can see into containers
  //return combineequipmentpredicates()(this, &item::IsPetrussNut, 1);
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isGoldenEagleShirt (item *i) { return (i && i->IsGoldenEagleShirt()); }

truth character::HasGoldenEagleShirt () const {
  return GeneralHasItem(::isGoldenEagleShirt, true); // Petrus can see into containers
  //return combineequipmentpredicates()(this, &item::IsGoldenEagleShirt, 1);
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isShadowVeil (item *i) { return (i && i->IsShadowVeil()); }

truth character::HasShadowVeil () const {
  return GeneralHasItem(::isShadowVeil, false); // not in containers
  //return combineequipmentpredicates()(this, &item::IsShadowVeil, 1);
}

truth character::RemoveShadowVeil (character *ToWhom) {
  return (GeneralRemoveItemTo(::isShadowVeil, ToWhom, false, false) != 0);
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isLostRubyFlamingSword (item *i) { return (i && i->IsLostRubyFlamingSword()); }

truth character::HasLostRubyFlamingSword () const {
  return GeneralHasItem(::isLostRubyFlamingSword, true); // it shines even through a container
  //return combineequipmentpredicates()(this, &item::IsLostRubyFlamingSword, 1);
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isMondedrPass (item *i) { return i->IsMondedrPass(); }

truth character::RemoveMondedrPass () {
  return GeneralRemoveItem(::isMondedrPass, false, false) != 0;
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isRingOfThieves (item *i) { return i->IsRingOfThieves(); }

truth character::RemoveRingOfThieves () {
  return GeneralRemoveItem(::isRingOfThieves, false, true) != 0;
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isNuke (item *i) { return i->IsNuke(); }

truth character::HasNuke () const {
  return GeneralHasItem(::isNuke, false);
}

truth character::RemoveNuke (character *ToWhom) {
  return GeneralRemoveItemTo(::isNuke, ToWhom, false, false) != 0;
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isWeepObsidian (item *i) { return i->IsWeepObsidian(); }

truth character::HasWeepObsidian () const {
  return GeneralHasItem(::isWeepObsidian, false);
}

truth character::RemoveWeepObsidian (character *ToWhom) {
  return GeneralRemoveItemTo(::isWeepObsidian, ToWhom, false, false) != 0;
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isMuramasa (item *i) { return i->IsMuramasa(); }

truth character::HasMuramasa () const {
  return GeneralHasItem(::isMuramasa, false);
}

truth character::RemoveMuramasa (character *ToWhom) {
  return GeneralRemoveItemTo(::isMuramasa, ToWhom, false, false) != 0;
}


// ////////////////////////////////////////////////////////////////////////// //
static truth isMasamune (item *i) { return i->IsMasamune(); }

truth character::HasMasamune () const {
  return GeneralHasItem(::isMasamune, false);
}

truth character::RemoveMasamune (character *ToWhom) {
  return GeneralRemoveItemTo(::isMasamune, ToWhom, false, false) != 0;
}


// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  character::IsHomeLevel
//
//==========================================================================
truth character::IsHomeLevel (level *lvl) const {
  if (!lvl) return false;
  // check level homes
  const fearray<festring> &hlist = DataBase->HomeLevel;
  if (hlist.Size == 0) return false;
  // has "*"?
  for (uInt f = 0; f < hlist.Size; ++f) { if (hlist[f] == "*") return true; }
  // taken from unique characters code
  cfestring *tag = lvl->GetLevelScript()->GetTag();
  if (!tag) return false; // not a possible home level
  // check for home level
  for (uInt f = 0; f < hlist.Size; ++f) { if (hlist[f] == *tag) return true; }
  return false;
}


truth character::MustBeRemovedFromBone () const {
  if (IsUnique() && !CanBeGenerated()) return true;
  // check level homes
  const fearray<festring> &hlist = DataBase->HomeLevel;
  if (hlist.Size == 0) return false;
  // has "*"?
  for (uInt f = 0; f < hlist.Size; ++f) { if (hlist[f] == "*") return true; }
  // taken from unique characters code
  if (!IsEnabled() || GetTeam()->GetID() != DataBase->NaturalTeam) return true;
  return IsHomeLevel(GetLevel());
}


int character::GetMoveType () const {
  // return (!StateIsActivated(LEVITATION) ? DataBase->MoveType : DataBase->MoveType | FLY);
  int res = DataBase->MoveType;
  if (StateIsActivated(LEVITATION)) res |= FLY;
  if (StateIsActivated(ETHEREAL_MOVING)) res |= ETHEREAL;
  if (IsPlayer() && game::IsInWilderness() && game::PlayerHasBoat()) {
    res |= SWIM;
  } else if (StateIsActivated(SWIMMING)) {
    res |= WALK|SWIM;
  }
  return res;
}


square *character::GetSquareUnder (int I) const {
  if (!MotherEntity) {
    if (I >= 0 && I < SquaresUnder && SquareUnder) return SquareUnder[I];
    ConLogf("character::GetSquareUnder() for '%s:(%d)': no square (I=%d; SquaresUnder=%d)",
            GetClassID(), GetConfig(), I, SquaresUnder);
    return nullptr;
  } else {
    return MotherEntity->GetSquareUnderEntity(I);
  }
}


v2 character::GetPos (int I) const {
  square *Sq = GetSquareUnder(I);
  if (!Sq) {
    ABORT("character::GetPos() for '%s:(%d)': no square (I=%d; SquaresUnder=%d)",
          GetClassID(), GetConfig(), I, SquaresUnder);
  }
  return Sq->GetPos();
}


static const char *ExpPtrNames[ATTRIBUTES] = {
  "DefaultEndurance",
  "DefaultPerception",
  "DefaultIntelligence",
  "DefaultWisdom",
  "DefaultWillPower",
  "DefaultCharisma",
  "DefaultMana",
  "DefaultArmStrength",
  "DefaultAgility",
  // not needed for non-humans
  "DefaultLegStrength",
  "DefaultDexterity",
};

int characterdatabase::*ExpPtr[ATTRIBUTES] = {
  &characterdatabase::DefaultEndurance,
  &characterdatabase::DefaultPerception,
  &characterdatabase::DefaultIntelligence,
  &characterdatabase::DefaultWisdom,
  &characterdatabase::DefaultWillPower,
  &characterdatabase::DefaultCharisma,
  &characterdatabase::DefaultMana,
  &characterdatabase::DefaultArmStrength,
  &characterdatabase::DefaultLegStrength,
  &characterdatabase::DefaultDexterity,
  &characterdatabase::DefaultAgility,
};


itemcontentscript characterdatabase::*EquipmentDataPtr[EQUIPMENT_DATAS] = {
  &characterdatabase::Helmet,
  &characterdatabase::Amulet,
  &characterdatabase::Cloak,
  &characterdatabase::BodyArmor,
  &characterdatabase::Belt,
  &characterdatabase::RightWielded,
  &characterdatabase::LeftWielded,
  &characterdatabase::RightRing,
  &characterdatabase::LeftRing,
  &characterdatabase::RightGauntlet,
  &characterdatabase::LeftGauntlet,
  &characterdatabase::RightBoot,
  &characterdatabase::LeftBoot
};


// ////////////////////////////////////////////////////////////////////////// //
// characterdatabase
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  characterdatabase::InitDefaults
//
//==========================================================================
void characterdatabase::InitDefaults (const characterprototype *NewProtoType,
                                      int NewConfig, cfestring &acfgstrname,
                                      const characterdatabase *aParentDB)
{
  IsAbstract = false;
  ProtoType = NewProtoType;
  Config = NewConfig;
  CfgStrName = acfgstrname;
  Alias.Clear();
  CopyFieldInfoFrom(aParentDB);

  if (aParentDB) {
    IsNonHumanoidBase = aParentDB->IsNonHumanoidBase;
  }
  if (!IsNonHumanoidBase && strcmp(ProtoType->GetClassID(), "nonhumanoid") == 0) {
    IsNonHumanoidBase = true;
  }

  /*
  if (aParentDB && IsNonHumanoidBase) {
    IsNonHumanoidBase = true;
  } else {
    IsNonHumanoidBase = false;
    const characterdatabase *bdb = this;
    while (bdb) {
      if (strcmp(bdb->ProtoType->GetClassID(), "nonhumanoid") == 0) {
        IsNonHumanoidBase = true;
        bdb = nullptr;
      } else if (bdb->ProtoType->GetBase()) {
        bdb = (*bdb->ProtoType->GetBase()->GetConfigData());
      } else {
        bdb = 0;
      }
    }
  }
  */
}


//==========================================================================
//
//  characterdatabase::PostProcess
//
//==========================================================================
void characterdatabase::PostProcess (const TextFileLocation &loc) {
  if (Config != 0 && !IsAbstract) {
    IvanAssert(ProtoType->GetBase());
    for (int c = 0; c < ATTRIBUTES - (IsNonHumanoidBase ? 2 : 0); ++c) {
      if (!IsFieldDefined(ExpPtrNames[c])) {
        #if 1
        ConLogf("char '%s:%s': field '%s' is not set!",
                ProtoType->GetClassID(), CfgStrName.CStr(), ExpPtrNames[c]);
        #endif
      }
    }
  }
  double AM = (100 + AttributeBonus) * EXP_MULTIPLIER / 100;
  for (int c = 0; c < ATTRIBUTES; ++c) {
    NaturalExperience[c] = this->*ExpPtr[c] * AM;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// characterprototype
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  characterprototype::characterprototype
//
//==========================================================================
characterprototype::characterprototype (const characterprototype *Base,
                                        characterspawner Spawner,
                                        charactercloner Cloner, cchar *ClassID)
  : Base(Base)
  , Spawner(Spawner)
  , Cloner(Cloner)
  , ClassID(ClassID)
{
  Index = protocontainer<character>::Add(this);
}


//==========================================================================
//
//  characterprototype::SpawnAndLoad
//
//==========================================================================
character *characterprototype::SpawnAndLoad (inputfile &SaveFile) const {
  character *Char = Spawner(0, LOAD);
  Char->Load(SaveFile);
  Char->CalculateAll();
  return Char;
}


//==========================================================================
//
//  characterprototype::CreateSpecialConfigurations
//
//==========================================================================
int characterprototype::CreateSpecialConfigurations (characterdatabase **TempConfig,
                                                     int Configs, int Level)
{
  if (Level == 0 && TempConfig[0]->CreateDivineConfigurations) {
    Configs = databasecreator<character>::CreateDivineConfigurations(this, TempConfig, Configs);
  }

  if (Level == 1 && TempConfig[0]->CreateUndeadConfigurations) {
    for (int c = 1; c < protocontainer<character>::GetSize(); ++c) {
      const character::prototype *Proto = protocontainer<character>::GetProto(c);
      if (!Proto) continue; // missing character
      const character::database *const *CharacterConfigData = Proto->GetConfigData();
      if (!CharacterConfigData) {
        ABORT("No database entry for character <%s>!", Proto->GetClassID());
      }
      const character::database *const *End = CharacterConfigData + Proto->GetConfigSize();
      for (++CharacterConfigData; CharacterConfigData != End; ++CharacterConfigData) {
        const character::database *CharacterDataBase = *CharacterConfigData;
        if (CharacterDataBase->UndeadVersions && Proto->NeedCreateUndead(CharacterDataBase->Config)) {
          character::database *ConfigDataBase = new character::database(**TempConfig);
          festring ucfgname = CONST_S("{");
          //ucfgname << (*TempConfig)->CfgStrName;
          ucfgname << Proto->GetClassID() << ":" << CharacterDataBase->CfgStrName << "}";
          /*
          ucfgname << CharacterDataBase->NameSingular;
          ucfgname << " - ";
          ucfgname << CharacterDataBase->CfgStrName;
          ucfgname << ") ";
          ucfgname << Proto->GetClassID();
          */
          #if 0
          ConLogf("UNDEAD: %s (c=%d; CConfig=%d; ResConfig=%d)", ucfgname.CStr(),
                  c, CharacterDataBase->Config, ((c << 8) | CharacterDataBase->Config));
          #endif
          ConfigDataBase->InitDefaults(this, (c << 8) | CharacterDataBase->Config, ucfgname, *TempConfig);
          //ConfigDataBase->FieldsSet = (*TempConfig)->FieldsSet;
          ConfigDataBase->PostFix << "of ";
          if (CharacterDataBase->Adjective.GetSize()) {
            if (CharacterDataBase->UsesLongAdjectiveArticle) ConfigDataBase->PostFix << "an ";
            else ConfigDataBase->PostFix << "a ";
            ConfigDataBase->PostFix << CharacterDataBase->Adjective << ' ';
          } else {
            if (CharacterDataBase->UsesLongArticle) ConfigDataBase->PostFix << "an ";
            else ConfigDataBase->PostFix << "a ";
          }
          ConfigDataBase->PostFix << CharacterDataBase->NameSingular;
          if (CharacterDataBase->PostFix.GetSize()) ConfigDataBase->PostFix << ' ' << CharacterDataBase->PostFix;
          int P1 = TempConfig[0]->UndeadAttributeModifier;
          int P2 = TempConfig[0]->UndeadVolumeModifier;
          int c2;
          for (c2 = 0; c2 < ATTRIBUTES; ++c2) {
            ConfigDataBase->*ExpPtr[c2] = CharacterDataBase->*ExpPtr[c2] * P1 / 100;
          }
          for (c2 = 0; c2 < EQUIPMENT_DATAS; ++c2) {
            ConfigDataBase->*EquipmentDataPtr[c2] = itemcontentscript ();
          }
          ConfigDataBase->DefaultIntelligence = 5;
          ConfigDataBase->DefaultWisdom = 5;
          ConfigDataBase->DefaultCharisma = 5;
          ConfigDataBase->TotalSize = CharacterDataBase->TotalSize;
          ConfigDataBase->Sex = CharacterDataBase->Sex;
          ConfigDataBase->AttributeBonus = CharacterDataBase->AttributeBonus;
          ConfigDataBase->TotalVolume = CharacterDataBase->TotalVolume * P2 / 100;
          if (TempConfig[0]->UndeadCopyMaterials) {
            // this is wrong for random heads!
            ConfigDataBase->HeadBitmapPos = CharacterDataBase->HeadBitmapPos;
            ConfigDataBase->HairColor = CharacterDataBase->HairColor;
            ConfigDataBase->EyeColor = CharacterDataBase->EyeColor;
            ConfigDataBase->CapColor = CharacterDataBase->CapColor;
            ConfigDataBase->FleshMaterial = CharacterDataBase->FleshMaterial;
            ConfigDataBase->BloodMaterial = CharacterDataBase->BloodMaterial;
            ConfigDataBase->VomitMaterial = CharacterDataBase->VomitMaterial;
            ConfigDataBase->SweatMaterial = CharacterDataBase->SweatMaterial;
          }
          ConfigDataBase->KnownCWeaponSkills = CharacterDataBase->KnownCWeaponSkills;
          ConfigDataBase->CWeaponSkillHits = CharacterDataBase->CWeaponSkillHits;
          ConfigDataBase->PostProcess(TextFileLocation());
          TempConfig[Configs++] = ConfigDataBase;
        }
      }
    }
  }

  if (Level == 0 && TempConfig[0]->CreateGolemMaterialConfigurations) {
    for (int c = 1; c < protocontainer<material>::GetSize(); ++c) {
      const material::prototype *Proto = protocontainer<material>::GetProto(c);
      if (!Proto) continue; // missing matherial
      const material::database *const *MaterialConfigData = Proto->GetConfigData();
      const material::database *const *End = MaterialConfigData + Proto->GetConfigSize();
      for (++MaterialConfigData; MaterialConfigData != End; ++MaterialConfigData) {
        const material::database *MaterialDataBase = *MaterialConfigData;
        if (MaterialDataBase->CategoryFlags & IS_GOLEM_MATERIAL) {
          if (NeedCreateGolem(MaterialDataBase->Config)) {
            character::database *ConfigDataBase = new character::database(**TempConfig);
            festring gcfgname = CONST_S("golem ");
            gcfgname << MaterialDataBase->CfgStrName;
            //ConLogf("GOLEM %d [%s]", MaterialDataBase->Config, gcfgname.CStr());
            #if 0
            ConLogf("GOLEM: %s (Config=%d)", gcfgname.CStr(), MaterialDataBase->Config);
            #endif
            ConfigDataBase->InitDefaults(this, MaterialDataBase->Config, gcfgname, *TempConfig);
            //ConfigDataBase->FieldsSet = (*TempConfig)->FieldsSet;
            ConfigDataBase->Adjective = MaterialDataBase->NameStem;
            ConfigDataBase->UsesLongAdjectiveArticle = MaterialDataBase->NameFlags & USE_AN;
            ConfigDataBase->AttachedGod = MaterialDataBase->AttachedGod;
            TempConfig[Configs++] = ConfigDataBase;
          }
        }
      }
    }
  }

  return Configs;
}


// ////////////////////////////////////////////////////////////////////////// //
// character
// ////////////////////////////////////////////////////////////////////////// //

character::character (ccharacter &Char) :
  entity(Char), id(Char), NP(Char.NP), AP(Char.AP),
  TemporaryState(Char.TemporaryState&~POLYMORPHED),
  Team(Char.Team), GoingTo(ERROR_V2), Money(0),
  AssignedName(Char.AssignedName), Action(0),
  DataBase(Char.DataBase), MotherEntity(0),
  PolymorphBackup(0), EquipmentState(0), SquareUnder(0),
  AllowedWeaponSkillCategories(Char.AllowedWeaponSkillCategories),
  BodyParts(Char.BodyParts),
  RegenerationCounter(Char.RegenerationCounter),
  SquaresUnder(Char.SquaresUnder), LastAcidMsgMin(0),
  Stamina(Char.Stamina), MaxStamina(Char.MaxStamina),
  BlocksSinceLastTurn(0), GenerationDanger(Char.GenerationDanger),
  CommandFlags(Char.CommandFlags), WarnFlags(0),
  ScienceTalks(Char.ScienceTalks), TrapData(0)
  , ItemIgnores(Char.ItemIgnores)
  , mPrevMoveDir(0)
{
  int c;
  Flags &= ~C_PLAYER;
  Flags |= C_INITIALIZING|C_IN_NO_MSG_MODE;
  Stack = new stack(0, this, HIDDEN);
  for (c = 0; c < STATES; ++c) TemporaryStateCounter[c] = Char.TemporaryStateCounter[c];
  if (Team) Team->Add(this);
  for (c = 0; c < BASE_ATTRIBUTES; ++c) BaseExperience[c] = Char.BaseExperience[c];
  BodyPartSlot = new bodypartslot[BodyParts];
  OriginalBodyPartID = new std::list<feuLong>[BodyParts];
  CWeaponSkill = new cweaponskill[AllowedWeaponSkillCategories];
  SquareUnder = new square *[SquaresUnder];
  if (SquaresUnder == 1) *SquareUnder = 0; else memset(SquareUnder, 0, SquaresUnder*sizeof(square *));
  for (c = 0; c < BodyParts; ++c) {
    BodyPartSlot[c].SetMaster(this);
    bodypart *CharBodyPart = Char.GetBodyPart(c);
    OriginalBodyPartID[c] = Char.OriginalBodyPartID[c];
    if (CharBodyPart) {
      bodypart *BodyPart = static_cast<bodypart *>(CharBodyPart->Duplicate());
      SetBodyPart(c, BodyPart);
      BodyPart->CalculateEmitation();
    }
  }
  for (c = 0; c < AllowedWeaponSkillCategories; ++c) CWeaponSkill[c] = Char.CWeaponSkill[c];
  HomeData = Char.HomeData ? new homedata(*Char.HomeData) : 0;
  ID = game::CreateNewCharacterID(this);
}


character::character () :
  entity(HAS_BE), NP(50000), AP(0), TemporaryState(0), Team(0),
  GoingTo(ERROR_V2), Money(0), Action(0), MotherEntity(0),
  PolymorphBackup(0), EquipmentState(0), SquareUnder(0),
  RegenerationCounter(0), HomeData(0), LastAcidMsgMin(0),
  BlocksSinceLastTurn(0), GenerationDanger(DEFAULT_GENERATION_DANGER),
  WarnFlags(0), ScienceTalks(0), TrapData(0)
  , mPrevMoveDir(0)
{
  Stack = new stack(0, this, HIDDEN);
}


character::~character () {
  if (Action) delete Action;
  if (Team) Team->Remove(this);
  delete Stack;
  for (int c = 0; c < BodyParts; ++c) delete GetBodyPart(c);
  delete [] BodyPartSlot;
  delete [] OriginalBodyPartID;
  delete PolymorphBackup;
  delete [] SquareUnder;
  delete [] CWeaponSkill;
  delete HomeData;
  deleteList(TrapData);
  game::RemoveCharacterID(ID);
}


festring character::DebugGetName () {
  festring res;
  res << "char:";
  AddName(res, DEFINITE);
  return res;
}


festring character::GetConfigName () const {
  if (DataBase) {
    return DataBase->CfgStrName;
  }
  /*
  if (GetProtoType() &&
      GetProtoType()->GetConfigData() &&
      GetConfig() < GetProtoType()->GetConfigSize() &&
      GetProtoType()->GetConfigData()[GetConfig()])
  {
    return GetProtoType()->GetConfigData()[GetConfig()]->CfgStrName;
  } else
  */
  {
    festring res;
    res << "WTF<" << GetConfig() << ">";
    return res;
  }
}


festring character::DebugLogNameCfg () const {
  festring res;
  res << GetClassID() << GetConfigName();
  return res;
}


void character::Hunger () {
  auto bst = GetBurdenState();
  if (bst == OVER_LOADED || bst == STRESSED) {
    EditNP(-8);
    EditExperience(LEG_STRENGTH, 150, 1 << 2);
    EditExperience(AGILITY, -50, 1 << 2);
  } else if (bst == BURDENED) {
    EditNP(-2);
    EditExperience(LEG_STRENGTH, 75, 1 << 1);
    EditExperience(AGILITY, -25, 1 << 1);
  } else if (bst == UNBURDENED) {
    EditNP(-1);
  }

  auto hst = GetHungerState();
  if (hst == STARVING) {
    EditExperience(ARM_STRENGTH, -75, 1 << 3);
    EditExperience(LEG_STRENGTH, -75, 1 << 3);
  } else if (hst == VERY_HUNGRY) {
    EditExperience(ARM_STRENGTH, -50, 1 << 2);
    EditExperience(LEG_STRENGTH, -50, 1 << 2);
  } else if (hst == HUNGRY) {
    EditExperience(ARM_STRENGTH, -25, 1 << 1);
    EditExperience(LEG_STRENGTH, -25, 1 << 1);
  } else if (hst == SATIATED) {
    EditExperience(AGILITY, -25, 1 << 1);
  } else if (hst == BLOATED) {
    EditExperience(AGILITY, -50, 1 << 2);
  } else if (hst == OVER_FED) {
    EditExperience(AGILITY, -75, 1 << 3);
  }

  CheckStarvationDeath(CONST_S("starved to death"));
}


int character::TakeHit (character *Enemy, item *Weapon, bodypart *EnemyBodyPart, v2 HitPos, double Damage,
  double ToHitValue, int Success, int Type, int GivenDir, truth Critical, truth ForceHit)
{
  //FIXME: args
  if (!game::RunCharEvent(CONST_S("before_take_hit"), this, Enemy, Weapon)) return DID_NO_DAMAGE;

  int Dir = (Type == BITE_ATTACK ? YOURSELF : GivenDir);
  double DodgeValue = GetDodgeValue();
  if (!Enemy->IsPlayer() && GetAttackWisdomLimit() != NO_LIMIT) Enemy->EditExperience(WISDOM, 75, 1 << 13);
  if (!Enemy->CanBeSeenBy(this)) ToHitValue *= 2;
  if (!CanBeSeenBy(Enemy)) DodgeValue *= 2;
  if (Enemy->StateIsActivated(CONFUSED)) ToHitValue *= 0.75;

  switch (Enemy->GetTirednessState()) {
    case FAINTING: ToHitValue *= 0.50; /* fallthrough */
    case EXHAUSTED: ToHitValue *= 0.75; /* fallthrough */
  }
  switch (GetTirednessState()) {
    case FAINTING: DodgeValue *= 0.50; /* fallthrough */
    case EXHAUSTED: DodgeValue *= 0.75; /* fallthrough */
  }

  if (!ForceHit) {
    if (!IsRetreating()) SetGoingTo(Enemy->GetPos());
    else SetGoingTo(GetPos()-((Enemy->GetPos()-GetPos())<<4));
    if (!Enemy->IsRetreating()) Enemy->SetGoingTo(GetPos());
    else Enemy->SetGoingTo(Enemy->GetPos()-((GetPos()-Enemy->GetPos())<<4));
  }

  /* Effectively, the average chance to hit is 100% / (DV/THV + 1). */
  if (RAND_N((int)(100+ToHitValue/DodgeValue*(100+Success))) < 100 && !Critical && !ForceHit) {
    Enemy->AddMissMessage(this, (Type == WEAPON_ATTACK ? Weapon : 0));
    EditExperience(AGILITY, 150, 1 << 7);
    EditExperience(PERCEPTION, 75, 1 << 7);
    if (Enemy->CanBeSeenByPlayer()) {
      DeActivateVoluntaryAction(CONST_S("The attack of ")+Enemy->GetName(DEFINITE)+CONST_S(" interrupts you."));
    } else {
      DeActivateVoluntaryAction(CONST_S("The attack interrupts you."));
    }
    return HAS_DODGED;
  }

  int TrueDamage = int(Damage*(100+Success)/100)+(RAND_N(3) ? 1 : 0);
  if (Critical) {
    TrueDamage += TrueDamage >> 1;
    ++TrueDamage;
  }

  int BodyPart = ChooseBodyPartToReceiveHit(ToHitValue, DodgeValue);
  if (Critical) {
         if (Type == UNARMED_ATTACK) Enemy->AddPrimitiveHitMessage(this, festring(Enemy->FirstPersonCriticalUnarmedHitVerb()), festring(Enemy->ThirdPersonCriticalUnarmedHitVerb()), BodyPart);
    else if (Type == WEAPON_ATTACK) Enemy->AddWeaponHitMessage(this, Weapon, BodyPart, true);
    else if (Type == KICK_ATTACK) Enemy->AddPrimitiveHitMessage(this, festring(Enemy->FirstPersonCriticalKickVerb()), festring(Enemy->ThirdPersonCriticalKickVerb()), BodyPart);
    else if (Type == BITE_ATTACK) Enemy->AddPrimitiveHitMessage(this, festring(Enemy->FirstPersonCriticalBiteVerb()), festring(Enemy->ThirdPersonCriticalBiteVerb()), BodyPart);
  } else {
         if (Type == UNARMED_ATTACK) Enemy->AddPrimitiveHitMessage(this, festring(Enemy->FirstPersonUnarmedHitVerb()), festring(Enemy->ThirdPersonUnarmedHitVerb()), BodyPart);
    else if (Type == WEAPON_ATTACK) Enemy->AddWeaponHitMessage(this, Weapon, BodyPart, false);
    else if (Type == KICK_ATTACK) Enemy->AddPrimitiveHitMessage(this, festring(Enemy->FirstPersonKickVerb()), festring(Enemy->ThirdPersonKickVerb()), BodyPart);
    else if (Type == BITE_ATTACK) Enemy->AddPrimitiveHitMessage(this, festring(Enemy->FirstPersonBiteVerb()), festring(Enemy->ThirdPersonBiteVerb()), BodyPart);
  }

  if (!Critical && TrueDamage && Enemy->AttackIsBlockable(Type)) {
    TrueDamage = CheckForBlock(Enemy, Weapon, ToHitValue, TrueDamage, Success, Type);
    if (!TrueDamage || (Weapon && !Weapon->Exists())) {
      if (Enemy->CanBeSeenByPlayer()) {
        DeActivateVoluntaryAction(CONST_S("The attack of ")+Enemy->GetName(DEFINITE)+CONST_S(" interrupts you."));
      } else {
        DeActivateVoluntaryAction(CONST_S("The attack interrupts you."));
      }
      return HAS_BLOCKED;
    }
  }

  int WeaponSkillHits = CalculateWeaponSkillHits(Enemy);
  int DoneDamage = ReceiveBodyPartDamage(Enemy, TrueDamage, PHYSICAL_DAMAGE, BodyPart, Dir, false, Critical, true, Type == BITE_ATTACK && Enemy->BiteCapturesBodyPart());
  truth Succeeded = (GetBodyPart(BodyPart) && HitEffect(Enemy, Weapon, HitPos, Type, BodyPart, Dir, !DoneDamage, Critical, DoneDamage)) || DoneDamage;
  if (Succeeded) Enemy->WeaponSkillHit(Weapon, Type, WeaponSkillHits);

  if (Weapon) {
    if (Weapon->Exists() && DoneDamage < TrueDamage) {
      Weapon->ReceiveDamage(Enemy, TrueDamage-DoneDamage, PHYSICAL_DAMAGE);
    }
    if (Weapon->Exists() && DoneDamage && SpillsBlood() && GetBodyPart(BodyPart) &&
        (GetBodyPart(BodyPart)->IsAlive() || GetBodyPart(BodyPart)->GetMainMaterial()->IsLiquid()))
    {
      Weapon->SpillFluid(0, CreateBlood(15+RAND_N(15)));
    }
  }

  if (Enemy->AttackIsBlockable(Type)) SpecialBodyDefenceEffect(Enemy, EnemyBodyPart, Type);

  if (!Succeeded) {
    if (Enemy->CanBeSeenByPlayer()) {
      DeActivateVoluntaryAction(CONST_S("The attack of ")+Enemy->GetName(DEFINITE)+CONST_S(" interrupts you."));
    } else {
      DeActivateVoluntaryAction(CONST_S("The attack interrupts you."));
    }
    return DID_NO_DAMAGE;
  }

  if (CheckDeath(festring(GetNormalDeathMessage()), Enemy, Enemy->IsPlayer() ? FORCE_MSG : 0)) return HAS_DIED;

  if (Enemy->CanBeSeenByPlayer()) {
    DeActivateVoluntaryAction(CONST_S("The attack of ")+Enemy->GetName(DEFINITE)+CONST_S(" interrupts you."));
  } else {
    DeActivateVoluntaryAction(CONST_S("The attack interrupts you."));
  }

  return HAS_HIT;
}


struct svpriorityelement {
  svpriorityelement (int BodyPart, int StrengthValue) : BodyPart(BodyPart), StrengthValue(StrengthValue) {}
  bool operator < (const svpriorityelement &AnotherPair) const { return StrengthValue > AnotherPair.StrengthValue; }
  int BodyPart;
  int StrengthValue;
};


int character::ChooseBodyPartToReceiveHit (double ToHitValue, double DodgeValue) {
  if (BodyParts == 1) return 0;
  std::priority_queue<svpriorityelement> SVQueue;
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && (BodyPart->GetHP() != 1 || BodyPart->CanBeSevered(PHYSICAL_DAMAGE)))
      SVQueue.push(svpriorityelement(c, ModifyBodyPartHitPreference(c, BodyPart->GetStrengthValue()+BodyPart->GetHP())));
  }
  while (SVQueue.size()) {
    svpriorityelement E = SVQueue.top();
    int ToHitPercentage = int(GLOBAL_WEAK_BODYPART_HIT_MODIFIER*ToHitValue*GetBodyPart(E.BodyPart)->GetBodyPartVolume()/(DodgeValue*GetBodyVolume()));
    ToHitPercentage = ModifyBodyPartToHitChance(E.BodyPart, ToHitPercentage);
    if (ToHitPercentage < 1) ToHitPercentage = 1;
    else if (ToHitPercentage > 95) ToHitPercentage = 95;
    if (ToHitPercentage > RAND_N(100)) return E.BodyPart;
    SVQueue.pop();
  }
  return 0;
}


//==========================================================================
//
//  character::Be
//
//==========================================================================
void character::Be () {
  //HACK!
  // this should be done in a separate "pickup handler", but
  // we have more than one way for player to get items...
  if (IsPlayer()) {
    GetStack()->SetScrollInspected();
  }

  if (game::ForceJumpToPlayerBe()) {
    if (!IsPlayer()) return;
    game::SetForceJumpToPlayerBe(false);
  } else {
    truth ForceBe = HP != MaxHP || AllowSpoil();
    for (int c = 0; c < BodyParts; ++c) {
      bodypart *BodyPart = GetBodyPart(c);
      if (BodyPart && (ForceBe || BodyPart->NeedsBe())) BodyPart->Be();
    }
    HandleStates();
    if (!IsEnabled()) return;
    if (GetTeam() == PLAYER->GetTeam()) {
      for (int c = 0; c < AllowedWeaponSkillCategories; ++c) {
        if (CWeaponSkill[c].Tick() && IsPlayer()) {
          CWeaponSkill[c].AddLevelDownMessage(c);
        }
      }
      SWeaponSkillTick();
    }
    if (IsPlayer()) {
      if (GetHungerState() == STARVING && !RAND_N(50)) {
        LoseConsciousness(250+RAND_N(250), true);
      }
      if (!Action || Action->AllowFoodConsumption()) {
        Hunger();
      }
    }
    if (Stamina != MaxStamina) {
      RegenerateStamina();
    }
    if (HP != MaxHP || StateIsActivated(REGENERATION)) {
      Regenerate();
    }
    #if 0
    if (!IsPlayer() && Action) {
      ConLogf("%s action is %s%s (AP=%d).",
              CHAR_NAME(DEFINITE), (Action->IsVoluntary() ? "voluntary " : ""),
              Action->GetDescription(), AP);
    }
    #endif
    if (Action && AP >= 1000) {
      ActionAutoTermination();
    }
    if (Action && AP >= 1000) {
      Action->Handle();
      if (!IsEnabled()) return;
    } else {
      EditAP(GetStateAPGain(100));
    }
  }
  if (AP >= 1000) {
    SpecialTurnHandler();
    BlocksSinceLastTurn = 0;
    if (IsPlayer()) {
      if (!GetAction() && game::CheckAutoSaveTimer()) {
        game::AutoSave();
      }
      game::CalculateNextDanger();
      if (!StateIsActivated(POLYMORPHED)) {
        game::UpdatePlayerAttributeAverage();
      }
      if (!game::IsInWilderness()) {
        Search(GetAttribute(PERCEPTION));
      }
      if (!Action) {
        GetPlayerCommand();
        CLEAR_KEY_BUFFER();
      } else {
        msgsystem::ThyMessagesAreNowOld();

        if (HasComparisonInfo()) {
          game::GetCurrentArea()->SendNewDrawRequest();
          game::DrawEverything();
        } else if (Action->ShowEnvironment()) {
          static int Counter = 0;
          if (++Counter >= 10) {
            game::DrawEverything();
            Counter = 0;
          }
        }

        if (Action->IsVoluntary()) {
          int kk = READ_KEY();
          if (kk) {
            #if 0
              #ifdef _WIN32
              const char *kname = globalwindowhandler::GetKeyName(kk).CStr();
              ConLogf("DEBUG: terminating action '%s' due to pressed key '%s' (%d).",
                      Action->GetDescription(), (kname ? kname : "<?>"), kk);
              #endif
            #endif
            Action->Terminate(false);
          }
        }
      }
    } else {
      if (!Action && !game::IsInWilderness()) {
        GetAICommand();
      }
    }
  }
}


//==========================================================================
//
//  character::Move
//
//==========================================================================
void character::Move (v2 MoveTo, truth TeleportMove, truth Run) {
  ClearComparisonInfo();
  if (!IsEnabled()) return;

  /* Test whether the player is stuck to something */
  if (!TeleportMove && !TryToUnStickTraps(MoveTo-GetPos())) return;
  if (Run && !IsPlayer() && TorsoIsAlive() &&
      (Stamina <= 10000/Max(GetAttribute(LEG_STRENGTH), 1) ||
       (!StateIsActivated(PANIC) && Stamina < MaxStamina>>2)))
  {
    Run = false;
  }

  RemoveTraps();

  if (GetBurdenState() != OVER_LOADED || TeleportMove) {
    lsquare *OldSquareUnder[MAX_SQUARES_UNDER];
    if (!game::IsInWilderness()) {
      if (IsPlayer()) {
        // idiotic code!
        area *ca = GetSquareUnder()->GetArea();
        for (int f = 0; f < MDIR_STAND; ++f) {
          const v2 np = GetPos() + game::GetMoveVector(f);
          if (ca->IsValidPos(np)) {
            lsquare *sq = static_cast<lsquare *>(ca->GetSquare(np.X, np.Y));
            sq->SetGoSeen(true);
          }
        }
      }
      for (int c = 0; c < GetSquaresUnder(); ++c) {
        OldSquareUnder[c] = GetLSquareUnder(c);
      }
    }
    Remove();
    PutTo(MoveTo);

    if (!TeleportMove) {
      /* Multitiled creatures should behave differently, maybe? */
      if (Run) {
        int ED = GetSquareUnder()->GetEntryDifficulty();
        int Base = 10000;
        EditAP(-GetMoveAPRequirement(ED)>>1);
        EditNP(-24 * ED);
        EditExperience(AGILITY, 125, ED<<7);
        if (IsPlayer()) {
          auto hst = GetHungerState();
               if (hst == SATIATED) Base = 11000;
          else if (hst == BLOATED) Base = 12500;
          else if (hst == OVER_FED) Base = 15000;
        }
        EditStamina(-Base/Max(GetAttribute(LEG_STRENGTH), 1), true);
      } else {
        int ED = GetSquareUnder()->GetEntryDifficulty();
        EditAP(-GetMoveAPRequirement(ED));
        EditNP(-12*ED);
        EditExperience(AGILITY, 75, ED<<7);
      }
    }
    if (IsPlayer()) {
      ShowNewPosInfo();
    }
    if (IsPlayer() && !game::IsInWilderness()) {
      GetStackUnder()->SetSteppedOn(true);
    }
    if (!game::IsInWilderness()) {
      SignalStepFrom(OldSquareUnder);
    }
  } else {
    if (IsPlayer()) {
      cchar *CrawlVerb = (StateIsActivated(LEVITATION) ? "float" : "crawl");
      ADD_MESSAGE("You try very hard to %s forward. But your load is too heavy.", CrawlVerb);
    }
    EditAP(-1000);
  }
}


//==========================================================================
//
//  character::GetAICommand
//
//==========================================================================
void character::GetAICommand () {
  //if (strcmp(GetTypeID(), "siren") == 0) ConLogf("siren::GetAICommand; enabled=%d", IsEnabled());
  //ConLogf("char::GetAICommand; class=<%s>", GetTypeID());
  SeekLeader(GetLeader());
  if (CheckDrink()) return;
  if (FollowLeader(GetLeader())) return;
  if (CheckForEnemies(true, true, true)) return;
  if (CheckForUsefulItemsOnGround()) return;
  if (CheckForDoors()) return;
  if (CheckSadism()) return;
  if (MoveRandomly()) return;
  EditAP(-1000);
}


//==========================================================================
//
//  character::MoveTowardsTarget
//
//==========================================================================
truth character::MoveTowardsTarget (truth Run) {
  v2 MoveTo[3];
  v2 TPos;
  v2 Pos = GetPos();

  if (!Route.empty()) {
    TPos = Route.back();
    Route.pop_back();
  } else {
    TPos = GoingTo;
  }

  MoveTo[0] = v2(0, 0);
  MoveTo[1] = v2(0, 0);
  MoveTo[2] = v2(0, 0);

  if (TPos.X < Pos.X) {
    if (TPos.Y < Pos.Y) {
      MoveTo[0] = v2(-1, -1);
      MoveTo[1] = v2(-1,  0);
      MoveTo[2] = v2( 0, -1);
    } else if (TPos.Y == Pos.Y) {
      MoveTo[0] = v2(-1,  0);
      MoveTo[1] = v2(-1, -1);
      MoveTo[2] = v2(-1,  1);
    } else if (TPos.Y > Pos.Y) {
      MoveTo[0] = v2(-1, 1);
      MoveTo[1] = v2(-1, 0);
      MoveTo[2] = v2( 0, 1);
    }
  } else if (TPos.X == Pos.X) {
    if (TPos.Y < Pos.Y) {
      MoveTo[0] = v2( 0, -1);
      MoveTo[1] = v2(-1, -1);
      MoveTo[2] = v2( 1, -1);
    } else if (TPos.Y == Pos.Y) {
      TerminateGoingTo();
      return false;
    } else if (TPos.Y > Pos.Y) {
      MoveTo[0] = v2( 0, 1);
      MoveTo[1] = v2(-1, 1);
      MoveTo[2] = v2( 1, 1);
    }
  } else if (TPos.X > Pos.X) {
    if (TPos.Y < Pos.Y) {
      MoveTo[0] = v2(1, -1);
      MoveTo[1] = v2(1,  0);
      MoveTo[2] = v2(0, -1);
    } else if (TPos.Y == Pos.Y) {
      MoveTo[0] = v2(1,  0);
      MoveTo[1] = v2(1, -1);
      MoveTo[2] = v2(1,  1);
    } else if (TPos.Y > Pos.Y) {
      MoveTo[0] = v2(1, 1);
      MoveTo[1] = v2(1, 0);
      MoveTo[2] = v2(0, 1);
    }
  }

  v2 ModifiedMoveTo = ApplyStateModification(MoveTo[0]);

  if (TryMove(ModifiedMoveTo, true, Run)) return true;

  int L = (Pos-TPos).GetManhattanLength();

  if (RAND_2) Swap(MoveTo[1], MoveTo[2]);

  if (Pos.IsAdjacent(TPos)) {
    TerminateGoingTo();
    return false;
  }

  if ((Pos+MoveTo[1]-TPos).GetManhattanLength() <= L && TryMove(ApplyStateModification(MoveTo[1]), true, Run)) return true;
  if ((Pos+MoveTo[2]-TPos).GetManhattanLength() <= L && TryMove(ApplyStateModification(MoveTo[2]), true, Run)) return true;
  Illegal.insert(Pos+ModifiedMoveTo);
  if (CreateRoute()) return true;
  return false;
}


//==========================================================================
//
//  character::CalculateNewSquaresUnder
//
//==========================================================================
int character::CalculateNewSquaresUnder (lsquare **NewSquare, v2 Pos) const {
  if (GetLevel()->IsValidPos(Pos)) {
    *NewSquare = GetNearLSquare(Pos);
    return 1;
  }
  return 0;
}


//==========================================================================
//
//  character::TryMoveUnlockHelper
//
//  this is used to automatically unlock doors
//
//==========================================================================
truth character::TryMoveUnlockHelper (lsquare *Square) {
  IvanAssert(Square);
  IvanAssert(IsPlayer());
  olterrain *Terrain = Square->GetOLTerrain();
  IvanAssert(Terrain);
  if (!Terrain || !Terrain->IsLocked()) return true;

  int count = GetStack()->GetItems();
  for (int f = 0; f != count; f += 1) {
    item *key = GetStack()->GetItem(f);
    if (key && key->IsKey(this) && Square->KeyCanOpen(key, this)) {
      game::HighlightSquare(Square->GetPos());
      const bool reply = game::TruthQuestion(CONST_S(
          "Do you want to open ")+Terrain->GetName(DEFINITE)+"?",
          true, game::GetMoveCommandKeyBetweenPoints(GetPos(), Square->GetPos()));
      game::UnHighlightSquare();
      if (reply) {
        Square->TryKey(key, this, true); // silent
        Terrain = Square->GetOLTerrain();
        if (Terrain && !Terrain->IsLocked()) return true;
      }
    }
  }

  Terrain = Square->GetOLTerrain();
  if (Terrain && !Terrain->IsLocked()) return true;
  return false;
}


//==========================================================================
//
//  character::TryMoveDropPets
//
//==========================================================================
truth character::TryMoveDropPets (wsquare *dest) {
  IvanAssert(dest);
  charactervector &V = game::GetWorldMap()->GetPlayerGroup();
  truth Discard = false;
  for (size_t c = 0; c != V.size(); c += 1) {
    if (!V[c]->CanMoveOn(dest)) {
      if (!Discard) {
        ADD_MESSAGE("One or more of your team members cannot cross this terrain.");
        if (!game::TruthQuestion(CONST_S("One or more of your team members cannot cross this terrain.\nAbandon them?"))) {
          return false;
        }
        Discard = true;
      }
      if (Discard) delete V[c];
      V.erase(V.begin() + c);
      c -= 1;
    }
  }
  return true;
}


//==========================================================================
//
//  character::DrawVFX
//
//==========================================================================
truth character::DrawVFX (col16 MonoColor) const {
  if (!game::IsInWilderness()) {
    v2 pos = GetPos();
    if (GetLevel()->IsValidPos(pos) && game::OnScreen(pos)) {
      blitdata BlitData = {
        Bitmap: DOUBLE_BUFFER,
        Src: v2(0, 0),
        Dest: v2(0, 0),
        Border: v2(TILE_SIZE, TILE_SIZE),
        { Flags: 0 },
        MaskColor: TRANSPARENT_COLOR,
        CustomData: ALLOW_ANIMATE|ALLOW_ALPHA,
      };
      #if 0
      //FIXME: this doesn't work!
      if (!IsSmall() && MonoColor != TRANSPARENT_COLOR) {
        BlitData.Border = v2(TILE_SIZE * 2, TILE_SIZE * 2);
      }
      #endif
      BlitData.Dest = game::CalculateScreenCoordinates(pos);
      Draw(BlitData, MonoColor);
      if (MonoColor == TRANSPARENT_COLOR) {
        lsquare *sqr = GetLSquareUnder();
        if (sqr) sqr->SendStrongNewDrawRequest();
      }
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::DrawAttackFlash
//
//==========================================================================
void character::DrawAttackFlash (VFXInfo *info, ccharacter *attacker, ccharacter *victim) {
  if (info) {
    memset(info, 0, sizeof(VFXInfo));
    if (ivanconfig::GetAttackIndicator()) {
      /*
      if (attacker && victim && !victim->CanBeSeenBy(attacker)) {
        // attacker cannot see the victim
        return;
      }
      */
      bool rendered = false;
      if (attacker && attacker->IsEnabled() &&
          (attacker->IsPlayer() || attacker->CanBeSeenByPlayer()))
      {
        game::DrawEverythingNoBlit(); // in case the monster moves, but not drawn yet
        rendered = true;
        info->attacker = attacker;
        info->atkSqr = attacker->GetLSquareUnder();
        info->atkDrawn = (attacker->IsPlayer() ? false : attacker->DrawVFX(WHITE));
      }
      if (victim && victim->IsEnabled() &&
          (victim->IsPlayer() || victim->CanBeSeenByPlayer()))
      {
        if (!rendered) {
          game::DrawEverythingNoBlit(); // in case the monster moves, but not drawn yet
          //rendered = true;
        }
        info->victim = victim;
        info->vicSqr = victim->GetLSquareUnder();
        info->vicDrawn = victim->DrawVFX(RED);
      }
    }
  }
}


//==========================================================================
//
//  character::AttackFlashDelay
//
//==========================================================================
void character::AttackFlashDelay (const VFXInfo *info) {
  if (info && (info->atkDrawn || info->vicDrawn)) {
    if (info->atkDrawn && info->vicDrawn) {
      // both are dead?
      if (!info->attacker->IsEnabled() && !info->victim->IsEnabled()) return;
    }
    // do not wait if the victim is dead, and the attacker is the player.
    // this is because there is no flash in this case.
    if (!info->atkDrawn && info->attacker && info->attacker->IsPlayer() &&
        info->vicDrawn && !info->victim->IsEnabled())
    {
      return;
    }
    msgsystem::Draw(); // show attack messages
    graphics::BlitDBToScreen();
    if (DEBUG_ATTACK_FLASH_PAUSE) {
      GET_KEY(true, true);
    } else {
      const bool plrAttacked = (info->victim && info->victim->IsPlayer());
      DELAY(Clamp(ivanconfig::GetAttackIndicatorDelay(), 10, 1000) + (plrAttacked ? 40 : 0));
    }
  }
}


//==========================================================================
//
//  character::EraseAttackFlash
//
//==========================================================================
void character::EraseAttackFlash (VFXInfo *info) {
  if (info && (info->atkDrawn || info->vicDrawn)) {
    //if (sqrOther) sqrOther->SendNewDrawRequest();
    //if (sqrSelf) sqrSelf->SendNewDrawRequest();
    // it is safer this way. much slower, but safer.
    // without this, the explosion erases the map, and everything blinks.
    // this calls `SendStrongNewDrawRequest()` on each square.
    //GetLevel()->SendNewDrawRequest();
    game::GetCurrentArea()->SendNewDrawRequest();
    game::DrawEverythingNoBlit();
  }
}


//==========================================================================
//
//  character::HitWithVFX
//
//  this also checks for fighting in a shop.
//  not the best place to do it, but...
//
//==========================================================================
truth character::HitWithVFX (character *Enemy, v2 HitPos, int Direction, int Flags) {
  VFXInfo vfx;
  room *Room = GetRoom();
  if (Room) {
    Room->BeforeHit(this, Enemy);
    if (!this->IsEnabled() || !Enemy->IsEnabled()) {
      //FIXME!
      return false;
    }
  }
  Room = Enemy->GetRoom();
  if (Room) {
    Room->BeforeHit(this, Enemy);
    if (!this->IsEnabled() || !Enemy->IsEnabled()) {
      //FIXME!
      return false;
    }
  }
  DrawAttackFlash(&vfx, this, Enemy);
  ctruth res = Hit(Enemy, HitPos, Direction, Flags);
  // if not hit, use lighter red for the victim
  if (!res && vfx.vicDrawn && Enemy->IsEnabled()) {
    Enemy->DrawVFX(MakeRGB16(0xff, 0x60, 0x60));
  }
  AttackFlashDelay(&vfx);
  EraseAttackFlash(&vfx);
  return res;
}


//==========================================================================
//
//  character::TryMove
//
//==========================================================================
truth character::TryMove (v2 MoveVector, truth Important, truth Run) {
  lsquare *MoveToSquare[MAX_SQUARES_UNDER];
  character *Pet[MAX_SQUARES_UNDER];
  character *Neutral[MAX_SQUARES_UNDER];
  character *Hostile[MAX_SQUARES_UNDER];
  v2 PetPos[MAX_SQUARES_UNDER];
  v2 NeutralPos[MAX_SQUARES_UNDER];
  v2 HostilePos[MAX_SQUARES_UNDER];
  v2 MoveTo = GetPos() + MoveVector;
  int Direction = game::GetDirectionForVector(MoveVector);
  if (Direction == DIR_ERROR) ABORT("Direction fault.");
  if (!game::IsInWilderness()) {
    int Squares = CalculateNewSquaresUnder(MoveToSquare, MoveTo);
    if (Squares) {
      int Pets = 0;
      int Neutrals = 0;
      int Hostiles = 0;
      for (int c = 0; c < Squares; ++c) {
        character *Char = MoveToSquare[c]->GetCharacter();
        if (Char && Char != this) {
          v2 Pos = MoveToSquare[c]->GetPos();
          if (IsAlly(Char)) {
            Pet[Pets] = Char;
            PetPos[Pets++] = Pos;
          } else if (Char->GetRelation(this) != HOSTILE) {
            Neutral[Neutrals] = Char;
            NeutralPos[Neutrals++] = Pos;
          } else {
            Hostile[Hostiles] = Char;
            HostilePos[Hostiles++] = Pos;
          }
        }
      }

      if (Hostiles == 1) {
        return HitWithVFX(Hostile[0], HostilePos[0], Direction);
      }

      if (Hostiles) {
        int Index = RAND_N(Hostiles);
        return HitWithVFX(Hostile[Index], HostilePos[Index], Direction);
      }

      if (Neutrals == 1) {
        if (!IsPlayer() && !Pets && Important && CanMoveOn(MoveToSquare[0])) {
          return HandleCharacterBlockingTheWay(Neutral[0], NeutralPos[0], Direction);
        } else {
          return IsPlayer() && HitWithVFX(Neutral[0], NeutralPos[0], Direction);
        }
      } else if (Neutrals) {
        if (IsPlayer()) {
          int Index = RAND_N(Neutrals);
          return HitWithVFX(Neutral[Index], NeutralPos[Index], Direction);
        }
        return false;
      }

      if (!IsPlayer()) {
        for (int c = 0; c < Squares; ++c) {
          if (MoveToSquare[c]->IsScary(this)) {
            return false;
          }
        }
      }

      if (Pets == 1) {
        if (IsPlayer() && !ivanconfig::GetBeNice() && Pet[0]->IsMasochist() && HasSadistAttackMode() &&
            game::TruthQuestion(CONST_S("Do you want to punish ") + Pet[0]->GetObjectPronoun() + "?"))
        {
          return HitWithVFX(Pet[0], PetPos[0], Direction, SADIST_HIT);
        } else {
          return (Important && (CanMoveOn(MoveToSquare[0]) ||
                  (IsPlayer() && game::GoThroughWallsCheatIsActive())) && Displace(Pet[0]));
        }
      } else if (Pets) {
        return false;
      }

      if ((CanMove() && CanMoveOn(MoveToSquare[0])) ||
          ((game::GoThroughWallsCheatIsActive() && IsPlayer())))
      {
        Move(MoveTo, false, Run);
        if (IsEnabled() && GetPos() == GoingTo) {
          TerminateGoingTo();
        }
        return true;
      } else {
        for (int c = 0; c < Squares; ++c) {
          olterrain *Terrain = MoveToSquare[c]->GetOLTerrain();
          if (Terrain && Terrain->CanBeOpened()) {
            if (CanOpen()) {
              if (Terrain->IsLocked()) {
                if (IsPlayer()) {
                  /*k8*/
                  if (TryMoveUnlockHelper(MoveToSquare[c])) {
                    // yes, this doesn't spend time for opening. so what.
                    return MoveToSquare[c]->Open(this);
                  } else if (ivanconfig::GetKickDownDoors()) {
                    game::HighlightSquare(MoveToSquare[c]->GetPos());
                    if (game::TruthQuestion(CONST_S("Locked! Do you want to kick ")+Terrain->GetName(DEFINITE)+"?", true, game::GetMoveCommandKeyBetweenPoints(PLAYER->GetPos(), MoveToSquare[0]->GetPos()))) {
                      game::UnHighlightSquare();
                      room *Room = MoveToSquare[c]->GetRoom();
                      if (!Room || Room->CheckDestroyTerrain(this)) {
                        Kick(MoveToSquare[c], Direction);
                        return true;
                      }
                    } else {
                      game::UnHighlightSquare();
                      return false;
                    }
                  }
                  /*k8*/
                  ADD_MESSAGE("The %s is locked.", Terrain->GetNameSingular().CStr()); /* not sure if this is better than "the door is locked", but I guess it _might_ be slighltly better */
                  return false;
                } else if (Important && CheckKick()) {
                  room *Room = MoveToSquare[c]->GetRoom();
                  if (!Room || Room->AllowKick(this, MoveToSquare[c])) {
                    int HP = Terrain->GetHP();
                    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s kicks %s.", CHAR_NAME(DEFINITE), Terrain->CHAR_NAME(DEFINITE));
                    Kick(MoveToSquare[c], Direction);
                    olterrain *NewTerrain = MoveToSquare[c]->GetOLTerrain();
                    if (NewTerrain == Terrain && Terrain->GetHP() == HP) { // BUG!
                      Illegal.insert(MoveTo);
                      CreateRoute();
                    }
                    return true;
                  }
                }
              } else { /* if (Terrain->IsLocked()) */
                /*if(!IsPlayer() || game::TruthQuestion(CONST_S("Do you want to open ")+Terrain->GetName(DEFINITE)+"?", false, game::GetMoveCommandKeyBetweenPoints(PLAYER->GetPos(), MoveToSquare[0]->GetPos()))) return MoveToSquare[c]->Open(this);*/
                /* Non-players always try to open it */
                if (!IsPlayer()) {
                  return MoveToSquare[c]->Open(this);
                }
                game::HighlightSquare(MoveToSquare[c]->GetPos());
                if (game::TruthQuestion(CONST_S("Do you want to open ")+Terrain->GetName(DEFINITE)+"?", false, game::GetMoveCommandKeyBetweenPoints(PLAYER->GetPos(), MoveToSquare[0]->GetPos()))) {
                  game::UnHighlightSquare();
                  return MoveToSquare[c]->Open(this);
                }
                game::UnHighlightSquare();
                return false;
              } /* if (Terrain->IsLocked()) */
            } else { /* if (CanOpen()) */
              if (IsPlayer()) {
                ADD_MESSAGE("This monster type cannot open doors.");
                return false;
              }
              if (Important) {
                Illegal.insert(MoveTo);
                return CreateRoute();
              }
            } /* if (CanOpen()) */
          } /* if (Terrain && Terrain->CanBeOpened()) */
        } /* for */
      } /* if */
      return false;
    } else {
      // leaving on-ground map
      if (IsPlayer() && !IsStuck() && GetLevel()->IsOnGround() &&
          game::TruthQuestion(CONST_S("Do you want to leave ")+game::GetCurrentDungeon()->GetLevelDescription(game::GetCurrentLevelIndex())+"?"))
      {
        ClearComparisonInfo();
        if (HasPetrussNut() && !HasGoldenEagleShirt()) {
          game::TextScreen(CONST_S("An undead and sinister voice greets you as you leave the city behind:\n\n\"MoRtAl! ThOu HaSt SlAuGtHeReD pEtRuS aNd PlEaSeD mE!\nfRoM tHiS dAy On, ThOu ArT tHe DeArEsT sErVaNt Of AlL eViL!\"\n\nYou are victorious!"));
          game::GetCurrentArea()->SendNewDrawRequest();
          game::DrawEverything();
          festring Msg = CONST_S("killed Petrus and became the Avatar of Chaos");
          AddPolymorphedText(Msg, "while");
          AddScoreEntry(Msg, 3, false);
          ShowAdventureInfo(Msg);
          game::End(Msg);
          return true;
        }
        if (game::TryTravel(WORLD_MAP, WORLD_MAP, game::GetCurrentDungeonIndex(),
                            /*AllowHostiles*/false, /*AlliesFollow*/true))
        {
          return true;
        }
      }
      return false;
    }
  } else { // in wilderness
    /** No multitile support */
    if (CanMove() && GetArea()->IsValidPos(MoveTo) &&
        (CanMoveOn(GetNearWSquare(MoveTo)) || game::GoThroughWallsCheatIsActive()))
    {
      if (!game::GoThroughWallsCheatIsActive()) {
        charactervector &V = game::GetWorldMap()->GetPlayerGroup();
        const bool underIsSea = !(GetSquareUnder()->GetSquareWalkability() & WALK);
        const bool newIsSea = !(GetNearWSquare(MoveTo)->GetSquareWalkability() & WALK);
        if (IsPlayer() && game::PlayerHasBoat()) {
          if (underIsSea != newIsSea) {
            if (newIsSea) {
              if (!game::PlayerOnBoat()) {
                if (!game::TruthQuestion(CONST_S("Board your ship?"))) {
                  if (StateIsActivated(LEVITATION)) {
                    // can levitate
                    if (TryMoveDropPets(GetNearWSquare(MoveTo))) {
                      Move(MoveTo, false);
                      return true;
                    }
                  }
                  return false;
                }
                if (V.empty()) {
                  ADD_MESSAGE("You board your ship and prepare to sail.");
                } else {
                  ADD_MESSAGE("Your team boards your ship and prepares to sail.");
                }
                EditStamina(-30000, false);
                game::SetPlayerOnBoat(true);
              }
            } else {
              if (game::PlayerOnBoat()) {
                if (!game::TruthQuestion(CONST_S("Disembark the ship?"))) {
                  return false;
                }
                if (V.empty()) {
                  ADD_MESSAGE("You disembark your ship.");
                } else {
                  ADD_MESSAGE("You and your team disembark your ship.");
                }
                EditStamina(-30000, false);
                game::SetPlayerOnBoat(false);
              }
            }
          }
        } else {
          // Cannot take some pets over the ocean without a ship.
          if (!TryMoveDropPets(GetNearWSquare(MoveTo))) {
            return false;
          }
        }
      }
      Move(MoveTo, false);
      return true;
    } else if (IsPlayer() && CanMove() && !GetArea()->IsValidPos(MoveTo)) {
      // moved off the world map. ok, why not?
      Die(0, CONST_S("performed suicide by jumping out of the world boundaries onto The Great Frog back"),
          FORCE_DEATH | DISALLOW_CORPSE | FORBID_REINCARNATION);
      return false;
    } else {
      return false;
    }
  }
}


//==========================================================================
//
//  character::WildernessTeleport
//
//  wizard mode command
//
//==========================================================================
truth character::WildernessTeleport (v2 MoveTo) {
  if (GetArea()->IsValidPos(MoveTo)) {
    Move(MoveTo, false);
    return true;
  } else {
    return false;
  }
}


//==========================================================================
//
//  character::CreateCorpse
//
//==========================================================================
void character::CreateCorpse (lsquare *Square) {
  if (!BodyPartsDisappearWhenSevered() && !game::AllBodyPartsVanish()) {
    corpse *Corpse = corpse::Spawn(0, NO_MATERIALS);
    Corpse->SetDeceased(this);
    Square->AddItem(Corpse);
    Disable();
  } else {
    SendToHell();
  }
}


//==========================================================================
//
//  character::Die
//
//==========================================================================
void character::Die (character *Killer, cfestring &Msg, feuLong DeathFlags) {
  /* Note: This function musn't delete any objects, since one of these may be
     the one currently processed by pool::Be()! */
  if (!IsEnabled()) return;

  if (!game::RunCharEvent(CONST_S("before_die"), this, Killer)) return;

  RemoveTraps();
  if (IsPlayer()) {
    ClearComparisonInfo();
    ADD_MESSAGE("You die.");
    game::DrawEverything();
    if (game::TruthQuestion(CONST_S("Do you want to save screenshot?"), REQUIRES_ANSWER)) {
      festring dir;
      dir << game::GetDeathshotPath();
      //outputfile::makeDir(dir.CStr());
      festring timestr;
      time_t t = time(NULL);
      struct tm *ts = localtime(&t);
      if (ts) {
        timestr << (int)(ts->tm_year%100);
        int t = ts->tm_mon+1;
        if (t < 10) timestr << '0'; timestr << t;
        t = ts->tm_mday; if (t < 10) timestr << '0'; timestr << t;
        timestr << '_';
        t = ts->tm_hour; if (t < 10) timestr << '0'; timestr << t;
        t = ts->tm_min; if (t < 10) timestr << '0'; timestr << t;
        t = ts->tm_sec; if (t < 10) timestr << '0'; timestr << t;
      } else {
        timestr = "heh";
      }
      festring ext = CONST_S(".png");
      festring fname = dir + "deathshot_" + timestr;
      if (inputfile::fileExists(fname + ext)) {
        for (int f = 0; f < 10000; f += 1) {
          char buf[16];
          sprintf(buf, "%03d", f);
          festring fn = fname+buf;
          if (!inputfile::fileExists(fn+ext)) {
            fname = fn;
            break;
          }
        }
      }
      fname << ext;
      //ConLogf("deathshot: %s", fname.CStr());
      DOUBLE_BUFFER->SavePNG(fname);
    }
    if (game::IsCheating()) {
      game::DrawEverything();
      if (!game::TruthQuestion(CONST_S("Do you want to do this, cheater?"), REQUIRES_ANSWER)) {
        RestoreBodyParts();
        ResetSpoiling();
        RestoreHP();
        RestoreStamina();
        ResetStates();
        SetNP(SATIATED_LEVEL);
        SendNewDrawRequest();
        return;
      }
    }
  } else if (CanBeSeenByPlayer() && !(DeathFlags&DISALLOW_MSG)) {
    ProcessAndAddMessage(GetDeathMessage());
  } else if (DeathFlags&FORCE_MSG) {
    ADD_MESSAGE("You sense the death of something.");
  }

  if (!(DeathFlags&FORBID_REINCARNATION)) {
    if (StateIsActivated(LIFE_SAVED) &&
        CanMoveOn(!game::IsInWilderness() ? GetSquareUnder() : PLAYER->GetSquareUnder()))
    {
      SaveLife();
      return;
    }
    if (SpecialSaveLife()) return;
  } else if (StateIsActivated(LIFE_SAVED)) {
    RemoveLifeSavers();
  }

  Flags |= C_IN_NO_MSG_MODE;
  character *Ghost = 0;
  if (IsPlayer()) {
    game::RemoveSaves();
    if (!game::IsInWilderness()) {
      #if 1
      // creating our ghost will not take a lot of time anymore...
      // ...but still show the message, why not?
      v2 scrpos = game::GetTopAreaTextPos(1);
      game::EraseTopArea();
      // this message is a blatant lie, but meh...
      FONT->PrintStr(DOUBLE_BUFFER, scrpos, ORANGE,
                     CONST_S("Please wait while I calculating some endgame stats..."));
      graphics::BlitDBToScreen();
      #endif
      Ghost = game::CreateGhost(); //WARNING! this may fail!
      if (Ghost) Ghost->Disable();
      #if 1
      game::EraseTopArea();
      graphics::BlitDBToScreen();
      #endif
    }
  }

  square *SquareUnder[MAX_SQUARES_UNDER];
  memset(SquareUnder, 0, sizeof(SquareUnder));
  Disable();
  if (IsPlayer() || !game::IsInWilderness()) {
    for (int c = 0; c < SquaresUnder; ++c) SquareUnder[c] = GetSquareUnder(c);
    Remove();
  } else {
    charactervector &V = game::GetWorldMap()->GetPlayerGroup();
    V.erase(std::find(V.begin(), V.end(), this));
  }
  //lsquare **LSquareUnder = reinterpret_cast<lsquare **>(SquareUnder); /* warning; wtf? */
  lsquare *LSquareUnder[MAX_SQUARES_UNDER];
  memmove(LSquareUnder, SquareUnder, sizeof(SquareUnder));

  if (!game::IsInWilderness()) {
    if (!StateIsActivated(POLYMORPHED)) {
      if (!IsPlayer() && !IsTemporary() && !Msg.IsEmpty()) {
        game::SignalDeath(this, Killer, Msg);
      }
      if (!(DeathFlags&DISALLOW_CORPSE)) {
        CreateCorpse(LSquareUnder[0]);
      } else {
        SendToHell();
      }
    } else {
      if (!IsPlayer() && !IsTemporary() && !Msg.IsEmpty()) {
        game::SignalDeath(GetPolymorphBackup(), Killer, Msg);
      }
      GetPolymorphBackup()->CreateCorpse(LSquareUnder[0]);
      GetPolymorphBackup()->Flags &= ~C_POLYMORPHED;
      SetPolymorphBackup(0);
      SendToHell();
    }
  } else {
    if (!IsPlayer() && !IsTemporary() && !Msg.IsEmpty()) {
      game::SignalDeath(this, Killer, Msg);
    }
    SendToHell();
  }

  if (IsPlayer()) {
    if (!game::IsInWilderness()) {
      for (int c = 0; c < GetSquaresUnder(); ++c) {
        LSquareUnder[c]->SetTemporaryEmitation(GetEmitation());
      }
    }
    AddScoreEntry(Msg);
    ShowAdventureInfo(Msg);
    if (!game::IsInWilderness()) {
      for(int c = 0; c < GetSquaresUnder(); ++c) {
        LSquareUnder[c]->SetTemporaryEmitation(0);
      }
    }
  }

  if (!game::IsInWilderness()) {
    if (GetSquaresUnder() == 1) {
      stack *StackUnder = LSquareUnder[0]->GetStack();
      GetStack()->MoveItemsTo(StackUnder);
      doforbodypartswithparam<stack*>()(this, &bodypart::DropEquipment, StackUnder);
    } else {
      while (GetStack()->GetItems()) GetStack()->GetBottom()->MoveTo(LSquareUnder[RAND_N(GetSquaresUnder())]->GetStack());
      for (int c = 0; c < BodyParts; ++c) {
        bodypart *BodyPart = GetBodyPart(c);
        if (BodyPart) BodyPart->DropEquipment(LSquareUnder[RAND_N(GetSquaresUnder())]->GetStack());
      }
    }
  }

  if (GetTeam()->GetLeader() == this) GetTeam()->SetLeader(0);

  Flags &= ~C_IN_NO_MSG_MODE;

  if (IsPlayer()) {
    if (!game::IsInWilderness() && Ghost) {
      Ghost->PutTo(LSquareUnder[0]->GetPos());
      Ghost->Enable();
      game::CreateBone();
    }
    game::TextScreen(CONST_S("Unfortunately you died."), ZERO_V2, WHITE, true, true,
                     &game::ShowDeathSmiley);
    game::End(Msg);
  }
}


//==========================================================================
//
//  character::AddMissMessage
//
//==========================================================================
void character::AddMissMessage (ccharacter *Enemy, citem *Weapon) const {
  festring Msg;
  bool addWeaponMsg = true;
  if (Enemy->IsPlayer()) {
    Msg = GetDescription(DEFINITE)+" misses you";
    addWeaponMsg = CanBeSeenByPlayer();
  } else if (IsPlayer()) {
    Msg = CONST_S("You miss ") + Enemy->GetDescription(DEFINITE);
  } else if (CanBeSeenByPlayer() || Enemy->CanBeSeenByPlayer()) {
    Msg = GetDescription(DEFINITE) + " misses " + Enemy->GetDescription(DEFINITE);
    addWeaponMsg = CanBeSeenByPlayer();
  } else {
    return;
  }
  if (Weapon && addWeaponMsg) {
    auto ffs = Weapon->TempDisableFluids();
    Msg << " with " << GetPossessivePronoun() << ' ' << Weapon->GetName(UNARTICLED);
  }
  ADD_MESSAGE("%s!", Msg.CStr());
}


//==========================================================================
//
//  character::AddBlockMessage
//
//==========================================================================
void character::AddBlockMessage (ccharacter *Enemy, citem *Blocker, cfestring &HitNoun, truth Partial) const {
  festring Msg;
  festring BlockVerb = CONST_S(Partial ? " to partially block the " : " to block the ") + HitNoun;
  auto ffs = Blocker->TempDisableFluids();
  if (IsPlayer()) {
    Msg << "You manage" << BlockVerb << " with your " << Blocker->GetName(UNARTICLED) << '!';
  } else if (Enemy->IsPlayer() || Enemy->CanBeSeenByPlayer()) {
    if (CanBeSeenByPlayer()) {
      Msg << GetName(DEFINITE) << " manages" << BlockVerb << " with "
          << GetPossessivePronoun() << ' ' << Blocker->GetName(UNARTICLED) << '!';
    } else {
      Msg << "Something manages" << BlockVerb << " with something!";
    }
  } else {
    return;
  }
  ADD_MESSAGE("%s", Msg.CStr());
}


//==========================================================================
//
//  character::AddPrimitiveHitMessage
//
//==========================================================================
void character::AddPrimitiveHitMessage (ccharacter *Enemy, cfestring &FirstPersonHitVerb,
                                        cfestring &ThirdPersonHitVerb, int BodyPart) const
{
  festring Msg;
  festring BodyPartDescription;
  if (BodyPart && (Enemy->CanBeSeenByPlayer() || Enemy->IsPlayer())) {
    BodyPartDescription << " in the " << Enemy->GetBodyPartName(BodyPart);
  }
  if (Enemy->IsPlayer()) {
    Msg << GetDescription(DEFINITE) << ' ' << ThirdPersonHitVerb << " you" << BodyPartDescription << '!';
  } else if (IsPlayer()) {
    Msg << "You " << FirstPersonHitVerb << ' ' << Enemy->GetDescription(DEFINITE) << BodyPartDescription << '!';
  } else if (CanBeSeenByPlayer() || Enemy->CanBeSeenByPlayer()) {
    Msg << GetDescription(DEFINITE) << ' ' << ThirdPersonHitVerb << ' ' << Enemy->GetDescription(DEFINITE) + BodyPartDescription << '!';
  } else {
    return;
  }
  ADD_MESSAGE("%s", Msg.CStr());
}


static cchar *const HitVerb[] = { "strike", "slash", "stab" };
static cchar *const HitVerb3rdPersonEnd[] = { "s", "es", "s" };


//==========================================================================
//
//  character::AddWeaponHitMessage
//
//==========================================================================
void character::AddWeaponHitMessage (ccharacter *Enemy, citem *Weapon, int BodyPart, truth Critical) const {
  festring Msg;
  festring BodyPartDescription;

  if (BodyPart && (Enemy->CanBeSeenByPlayer() || Enemy->IsPlayer())) {
    BodyPartDescription << " in the " << Enemy->GetBodyPartName(BodyPart);
  }

  int FittingTypes = 0;
  int DamageFlags = Weapon->GetDamageFlags();
  int DamageType = 0;

  for (int c = 0; c < DAMAGE_TYPES; ++c) {
    if (1 << c & DamageFlags) {
      if (!FittingTypes || !RAND_N(FittingTypes+1)) DamageType = c;
      ++FittingTypes;
    }
  }

  if (!FittingTypes) {
    ABORT("No damage flags specified for %s!", Weapon->CHAR_NAME(UNARTICLED));
  }

  festring NewHitVerb = CONST_S(Critical ? " critically " : " ");
  NewHitVerb << HitVerb[DamageType];
  cchar *const E = HitVerb3rdPersonEnd[DamageType];

  bool addWeaponMsg = true;
  if (Enemy->IsPlayer()) {
    Msg << GetDescription(DEFINITE) << NewHitVerb << E << " you" << BodyPartDescription;
    addWeaponMsg = CanBeSeenByPlayer();
  } else if (IsPlayer()) {
    Msg << "You" << NewHitVerb << ' ' << Enemy->GetDescription(DEFINITE) << BodyPartDescription;
  } else if (CanBeSeenByPlayer() || Enemy->CanBeSeenByPlayer()) {
    Msg << GetDescription(DEFINITE) << NewHitVerb << E << ' '
        << Enemy->GetDescription(DEFINITE) << BodyPartDescription;
    addWeaponMsg = CanBeSeenByPlayer();
  } else {
    return;
  }
  if (addWeaponMsg) {
    auto ffs = Weapon->TempDisableFluids();
    Msg << " with " << GetPossessivePronoun() << ' ' << Weapon->GetName(UNARTICLED);
  }
  ADD_MESSAGE("%s!", Msg.CStr());
}


//==========================================================================
//
//  character::GeneralHasItem
//
//==========================================================================
truth character::GeneralHasItem (ItemCheckerCB chk, truth checkContainers) const {
  // inventory
  if (GetStack()->GeneralHasItem(this, chk, checkContainers)) return true;
  // equipments
  for (int c = 0; c < GetEquipments(); ++c) {
    item *it = GetEquipment(c);
    if (!it) continue;
    if (chk(it)) return true;
    if (it->IsOpenable(this)) {
      stack *cst = it->GetContained();
      if (cst) {
        if (cst->GeneralHasItem(this, chk, checkContainers)) return true;
      }
    }
  }
  return false;
}


//==========================================================================
//
//  character::HasOmmelBlood
//
//==========================================================================
truth character::HasOmmelBlood () const {
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
    if (i->IsKleinBottle() && i->GetSecondaryMaterial() && i->GetSecondaryMaterial()->GetConfig() == OMMEL_BLOOD) return true;
  }
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Item = GetEquipment(c);
    if (Item && Item->IsKleinBottle() && Item->GetSecondaryMaterial() && Item->GetSecondaryMaterial()->GetConfig() == OMMEL_BLOOD) return true;
  }
  return false; //combineequipmentpredicates()(this, &item::IsKleinBottle, 1);
}


//==========================================================================
//
//  character::HasCurdledBlood
//
//==========================================================================
truth character::HasCurdledBlood () const {
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
    if (i->IsKleinBottle() && i->GetSecondaryMaterial() && i->GetSecondaryMaterial()->GetConfig() == CURDLED_OMMEL_BLOOD) return true;
  }
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Item = GetEquipment(c);
    if (Item && Item->IsKleinBottle() && Item->GetSecondaryMaterial() &&
        Item->GetSecondaryMaterial()->GetConfig() == CURDLED_OMMEL_BLOOD)
    {
      return true;
    }
  }
  return false; //combineequipmentpredicates()(this, &item::IsKleinBottle, 1);
}


//==========================================================================
//
//  character::CurdleOmmelBlood
//
//==========================================================================
truth character::CurdleOmmelBlood () const {
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
    if (i->IsKleinBottle() && i->GetSecondaryMaterial() && i->GetSecondaryMaterial()->GetConfig() == OMMEL_BLOOD) {
      i->ChangeSecondaryMaterial(MAKE_MATERIAL(CURDLED_OMMEL_BLOOD));
      return true;
    }
  }
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Item = GetEquipment(c);
    if (Item && Item->IsKleinBottle() && Item->GetSecondaryMaterial() &&
        Item->GetSecondaryMaterial()->GetConfig() == OMMEL_BLOOD)
    {
      Item->ChangeSecondaryMaterial(MAKE_MATERIAL(CURDLED_OMMEL_BLOOD));
      return true;
    }
  }
  return false; //combineequipmentpredicates()(this, &item::IsKleinBottle, 1);
}


//==========================================================================
//
//  character::RemoveCurdledOmmelBlood
//
//==========================================================================
truth character::RemoveCurdledOmmelBlood () {
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
    if (i->IsKleinBottle() && i->GetSecondaryMaterial() && i->GetSecondaryMaterial()->GetConfig() == CURDLED_OMMEL_BLOOD) {
      (*i)->RemoveFromSlot();
      (*i)->SendToHell();
      return true;
    }
  }
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Item = GetEquipment(c);
    if (Item && Item->IsKleinBottle() && Item->GetSecondaryMaterial() &&
        Item->GetSecondaryMaterial()->GetConfig() == CURDLED_OMMEL_BLOOD)
    {
      Item->RemoveFromSlot();
      Item->SendToHell();
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::GeneralRemoveItem
//
//==========================================================================
int character::GeneralRemoveItem (ItemCheckerCB chk, truth allItems, truth checkContainers) {
  // inventory
  int count = GetStack()->GeneralRemoveItem(this, chk, allItems, checkContainers);
  // equipments
  if (count == 0 || allItems) {
    for (int c = 0; c < GetEquipments(); ++c) {
      item *it = GetEquipment(c);
      if (!it) continue;
      if (chk(it)) {
        it->RemoveFromSlot();
        it->SendToHell();
        ++count;
        if (!allItems) break;
      } else if (it->IsOpenable(this)) {
        stack *cst = it->GetContained();
        if (cst) {
          int rcc = cst->GeneralRemoveItem(this, chk, allItems, checkContainers);
          if (rcc) {
            count += rcc;
            if (!allItems) break;
          }
        }
      }
    }
  }
  return count;
}


//==========================================================================
//
//  character::GeneralRemoveItemTo
//
//==========================================================================
int character::GeneralRemoveItemTo (ItemCheckerCB chk, character *ToWhom,
                                    truth allItems, truth checkContainers)
{
  if (!ToWhom) return GeneralRemoveItem(chk, allItems, checkContainers);
  // inventory
  int count = GetStack()->GeneralRemoveItemTo(this, chk, ToWhom, allItems, checkContainers);
  // equipments
  if (count == 0 || allItems) {
    for (int c = 0; c < GetEquipments(); ++c) {
      item *it = GetEquipment(c);
      if (!it) continue;
      if (chk(it)) {
        it->RemoveFromSlot();
        ToWhom->ReceiveItemAsPresent(it);
        //it->SendToHell();
        ++count;
        if (!allItems) break;
      } else if (it->IsOpenable(this)) {
        stack *cst = it->GetContained();
        if (cst) {
          int rcc = cst->GeneralRemoveItemTo(this, chk, ToWhom, allItems, checkContainers);
          if (rcc) {
            count += rcc;
            if (!allItems) break;
          }
        }
      }
    }
  }
  return count;
}


//==========================================================================
//
//  character::ReadItem
//
//==========================================================================
truth character::ReadItem (item *ToBeRead) {
  if (!ToBeRead->CanBeRead(this)) {
    if (IsPlayer()) ADD_MESSAGE("You can't read this.");
    return false;
  }

  if (!GetLSquareUnder()->IsDark() || game::GetSeeWholeMapCheatMode()) {
    if (StateIsActivated(CONFUSED) && !RAND_8) {
      if (!ToBeRead->IsDestroyable(this)) {
        ADD_MESSAGE("You read some words of %s and understand exactly nothing.", ToBeRead->CHAR_NAME(DEFINITE));
      } else {
        ADD_MESSAGE("%s is very confusing. Or perhaps you are just too confused?", ToBeRead->CHAR_NAME(DEFINITE));
        ActivateRandomState(SRC_CONFUSE_READ, 1000+RAND_N(1500));
        ToBeRead->RemoveFromSlot();
        ToBeRead->SendToHell();
      }
      EditAP(-1000);
      return true;
    }
    if (ToBeRead->Read(this)) {
      /* This AP is used to take the stuff out of backpack */
      DexterityAction(5);
      return true;
    }
    return false;
  }

  if (IsPlayer()) ADD_MESSAGE("It's too dark here to read.");
  return false;
}


//==========================================================================
//
//  character::CalculateBurdenState
//
//==========================================================================
void character::CalculateBurdenState () {
  int OldBurdenState = BurdenState;
  sLong SumOfMasses = GetCarriedWeight();
  sLong CarryingStrengthUnits = sLong(GetCarryingStrength())*2500;
       if (SumOfMasses > (CarryingStrengthUnits << 1) + CarryingStrengthUnits) BurdenState = OVER_LOADED;
  else if (SumOfMasses > CarryingStrengthUnits << 1) BurdenState = STRESSED;
  else if (SumOfMasses > CarryingStrengthUnits) BurdenState = BURDENED;
  else BurdenState = UNBURDENED;
  if (!IsInitializing() && BurdenState != OldBurdenState) CalculateBattleInfo();
}


//==========================================================================
//
//  character::Save
//
//==========================================================================
void character::Save (outputfile &SaveFile) const {
  SaveFile << DataBase->CfgStrName;

  Stack->Save(SaveFile);
  SaveFile << ID;
  for (int c = 0; c < BASE_ATTRIBUTES; ++c) SaveFile << BaseExperience[c];

  //SaveFile << ExpModifierMap;/*k8: wtf is this?*/
  SaveFile << NP << AP << Stamina << GenerationDanger << ScienceTalks;
  SaveFile << TemporaryState << EquipmentState << Money << GoingTo << RegenerationCounter << Route << Illegal;
  SaveFile << CurrentSweatMaterial;
  SaveFile.Put(!!IsEnabled());
  SaveFile << HomeData << BlocksSinceLastTurn << CommandFlags;
  SaveFile << WarnFlags << (uShort)Flags;

  for (int c = 0; c < BodyParts; ++c) {
    SaveFile << BodyPartSlot[c] << OriginalBodyPartID[c];
  }

  SaveLinkedList(SaveFile, TrapData);
  SaveFile << Action;

  for (int c = 0; c < STATES; ++c) {
    SaveFile << TemporaryStateCounter[c];
  }

  if (GetTeam()) {
    SaveFile.Put(true);
    SaveFile << (feuLong)Team->GetID(); // feuLong
  } else {
    SaveFile.Put(false);
  }

  if (GetTeam() && GetTeam()->GetLeader() == this) SaveFile.Put(true); else SaveFile.Put(false);

  SaveFile << AssignedName << PolymorphBackup;

  for (int c = 0; c < AllowedWeaponSkillCategories; ++c) {
    SaveFile << CWeaponSkill[c];
  }

  SaveFile << ItemIgnores;
}


//==========================================================================
//
//  character::Load
//
//==========================================================================
void character::Load (inputfile &SaveFile) {
  LoadSquaresUnder();

  festring acfgname;
  SaveFile >> acfgname;
  int acfgid = databasecreator<character>::FindConfigByName(FindProtoType(), acfgname);
  if (acfgid == -1) {
    ABORT("Cannot find '%s' config '%s'!", FindProtoType()->GetClassID(), acfgname.CStr());
  }

  Stack->Load(SaveFile);
  SaveFile >> ID;
  game::AddCharacterID(this, ID);

  for (int c = 0; c < BASE_ATTRIBUTES; ++c) SaveFile >> BaseExperience[c];

  //SaveFile >> ExpModifierMap;/*k8: wtf is this?*/
  SaveFile >> NP >> AP >> Stamina >> GenerationDanger >> ScienceTalks;
  SaveFile >> TemporaryState >> EquipmentState >> Money >> GoingTo >> RegenerationCounter >> Route >> Illegal;
  SaveFile >> CurrentSweatMaterial;

  if (!SaveFile.Get()) Disable();

  SaveFile >> HomeData >> BlocksSinceLastTurn >> CommandFlags;
  SaveFile >> WarnFlags;
  WarnFlags &= ~WARNED;
  Flags |= ReadType(uShort, SaveFile) & ~ENTITY_FLAGS;

  for (int c = 0; c < BodyParts; ++c) {
    SaveFile >> BodyPartSlot[c] >> OriginalBodyPartID[c];
    item *BodyPart = *BodyPartSlot[c];
    if (BodyPart) BodyPart->Disable();
  }

  LoadLinkedList(SaveFile, TrapData);
  SaveFile >> Action;

  if (Action) Action->SetActor(this);

  for (int c = 0; c < STATES; ++c) SaveFile >> TemporaryStateCounter[c];

  if (SaveFile.Get()) {
    feuLong tid = ReadType(feuLong, SaveFile);
    #if 0
    ConLogf("char want to go to team %d", (int)tid);
    #endif
    SetTeam(game::GetTeam(tid));
  }

  if (SaveFile.Get()) {
    GetTeam()->SetLeader(this);
  }

  SaveFile >> AssignedName >> PolymorphBackup;

  for (int c = 0; c < AllowedWeaponSkillCategories; ++c) {
    SaveFile >> CWeaponSkill[c];
  }

  SaveFile >> ItemIgnores;


  //databasecreator<character>::InstallDataBase(this, ReadType(uShort, SaveFile));
  databasecreator<character>::InstallDataBase(this, acfgid);

  if (IsEnabled() && !game::IsInWilderness()) {
    for (int c = 1; c < GetSquaresUnder(); ++c) {
      GetSquareUnder(c)->SetCharacter(this);
    }
  }
}


//==========================================================================
//
//  character::Engrave
//
//==========================================================================
truth character::Engrave (cfestring &What) {
  GetLSquareUnder()->Engrave(What);
  return true;
}


//==========================================================================
//
//  character::RemoveItemFromIgnoreList
//
//==========================================================================
void character::RemoveItemFromIgnoreList (citem *Item) {
  if (Item) {
    auto it = ItemIgnores.find(Item->GetID());
    if (it != ItemIgnores.end()) ItemIgnores.erase(it);
  }
}


//==========================================================================
//
//  character::CheckItemIgnoreCooldown
//
//==========================================================================
truth character::CheckItemIgnoreCooldown (citem *Item) {
  bool res = true;
  if (Item) {
    auto it = ItemIgnores.find(Item->GetID());
    if (it != ItemIgnores.end()) {
      if (it->second <= (feuLong)game::GetTurn()) {
        // debug output
        if (DEBUG_PICKUP_AI != 0) {
          ConLogf("char #%u:'%s' is (again) interested in item '%s' at (%d, %d)",
                  GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                  Item->GetPos().X, Item->GetPos().Y);
        }
        ItemIgnores.erase(it);
      } else {
        res = false;
      }
    }
  } else {
    res = false;
  }
  return res;
}


//==========================================================================
//
//  character::AddItemToIgnoreList
//
//==========================================================================
void character::AddItemToIgnoreList (citem *Item, cchar *debugWhy) {
  if (Item) {
    const int nextTurn = RAND_BETWEEN(16, 24);
    // debug output
    if (debugWhy && DEBUG_PICKUP_AI != 0) {
      if (ItemIgnores.find(Item->GetID()) == ItemIgnores.end()) {
        ConLogf("char #%u:'%s' gives up reaching item '%s' at (%d, %d) "
                "from (%d, %d) for %d turns (%s)",
                GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                Item->GetPos().X, Item->GetPos().Y,
                GetPos().X, GetPos().Y, nextTurn,
                debugWhy);
      }
    }
    // done debug output
    ItemIgnores[Item->GetID()] = (feuLong)(game::GetTurn() + nextTurn);
  }
}


//==========================================================================
//
//  character::MoveToInterestingItem
//
//  this is automatically called from `MoveRandomly()`
//
//==========================================================================
truth character::MoveToInterestingItem (truth SameRoom) {
  feuLong iid;
  v2 interestingPOI = FindUsefulItemAround(SameRoom, &iid);
  if (interestingPOI != ERROR_V2) {
    // if the item is near, and our stamina is high, run to the item, why not?
    const bool run = (GetTirednessState() == UNTIRED || GetAttribute(INTELLIGENCE) < 6);
    // debug output
    if (interestingPOI != GoingTo && DEBUG_PICKUP_AI != 0) {
      item *Item = (iid ? game::SearchItem(iid) : 0);
      if (Item) {
        ConLogf("char #%u:'%s' is %s to item '%s' at (%d, %d) from (%d, %d)...",
                GetID(), DebugLogName().CStr(),
                (run ? "running" : "going"),
                Item->DebugLogName().CStr(),
                interestingPOI.X, interestingPOI.Y,
                GetPos().X, GetPos().Y);
      } else {
        ConLogf("char #%u:'%s' is %s to item at (%d, %d) from (%d, %d)...",
                GetID(), DebugLogName().CStr(),
                (run ? "running" : "going"),
                interestingPOI.X, interestingPOI.Y,
                GetPos().X, GetPos().Y);
      }
    }
    // done debug output
    // temporarily change the route.
    const v2 oldGT = GoingTo;
    SetGoingTo(interestingPOI);
    ctruth res = MoveTowardsTarget(run);
    if (!res || oldGT != ERROR_V2) {
      if (oldGT != ERROR_V2 && oldGT != interestingPOI) {
        SetGoingTo(oldGT);
      } else {
        TerminateGoingTo();
      }
    }
    if (res) return true;
    // can't get to item, give up for some time
    if (iid) {
      item *Item = game::SearchItem(iid);
      AddItemToIgnoreList(Item, "cannot reach");
    }
  }
  return false;
}


//==========================================================================
//
//  character::MoveRandomly
//
//==========================================================================
truth character::MoveRandomly (truth allowInterestingItems) {
  if (!IsEnabled()) return false;

  if (allowInterestingItems && MoveToInterestingItem(false)) return true;

  // chaotic movement
  for (int c = 0; c < 10; ++c) {
    v2 ToTry = game::GetMoveVector(RAND_8);
    if (GetLevel()->IsValidPos(GetPos()+ToTry)) {
      lsquare *Square = GetNearLSquare(GetPos()+ToTry);
      if (!Square->IsDangerous(this) && !Square->IsScary(this) &&
          TryMove(ToTry, false, false))
      {
        return true;
      }
    }
  }

  return false;
}


//==========================================================================
//
//  character::TestForPickup
//
//==========================================================================
truth character::TestForPickup (item *ToBeTested) const {
  if (MakesBurdened(ToBeTested->GetWeight() + GetCarriedWeight())) return false;
  return true;
}


//==========================================================================
//
//  character::FillHiScoreInfo
//
//==========================================================================
void character::FillHiScoreInfo (highscore::Info &info) const {
  game::GetGameHash(info.hash);
  info.plrName = game::GetPlayerName();
  info.score = game::GetScore();
  info.quited = false;
  info.ticks = (sLong)game::GetTick();
  info.turns = (sLong)game::GetTurn();
  info.rtime = (sLong)game::GetTimeSpent();
  ivantime Time;
  game::GetTime(Time);
  info.gday = (int)Time.Day;
  info.ghour = (int)Time.Hour;
  info.gmin = (int)Time.Min;
}


//==========================================================================
//
//  character::AddScoreEntry
//
//==========================================================================
void character::AddScoreEntry (cfestring &Description, double Multiplier, truth AddEndLevel) const {
  if (!game::WizardModeIsReallyActive() && !game::IsCheating()) {
    festring where;
    if (AddEndLevel) {
      where << (game::IsInWilderness()
                  ? CONST_S("the world map")
                  : game::GetCurrentDungeon()->GetLevelDescription(game::GetCurrentLevelIndex()));
    }
    highscore::Info info;
    FillHiScoreInfo(info);
    info.deathDesc = Description;
    info.multiplier = Multiplier;
    info.place = where;
    highscore::Add(info, ivanconfig::GetSaveQuitScore());
    SaveAdventureInfo();
  } else {
    highscore::SetLastAddFailed();
  }
}


//==========================================================================
//
//  character::AddQuitedScoreEntry
//
//==========================================================================
void character::AddQuitedScoreEntry (cfestring &Description, double Multiplier) const {
  if (!game::WizardModeIsReallyActive() && !game::IsCheating()) {
    festring where;
    where << (game::IsInWilderness()
                ? CONST_S("the world map")
                : game::GetCurrentDungeon()->GetLevelDescription(game::GetCurrentLevelIndex()));
    highscore::Info info;
    FillHiScoreInfo(info);
    info.deathDesc = Description;
    info.multiplier = Multiplier;
    info.quited = true;
    info.place = where;
    highscore::Add(info, ivanconfig::GetSaveQuitScore());
    SaveAdventureInfo();
  } else {
    highscore::SetLastAddFailed();
  }
}


//==========================================================================
//
//  character::AddPolymorphedText
//
//==========================================================================
bool character::AddPolymorphedText (festring &Msg, cchar *pfx) {
  if (GetPolymorphBackup()) {
    if (!Msg.IsEmpty()) Msg << " ";
    if (pfx && pfx[0]) {
      if (pfx[0] == 32 && !pfx[1]) {
        // special
        Msg << " ";
      } else {
        Msg << pfx << " ";
      }
    }
    Msg << "polymorphed into ";
    id::AddName(Msg, INDEFINITE);
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::CheckDeath
//
//==========================================================================
truth character::CheckDeath (cfestring &Msg, character *Murderer, feuLong DeathFlags) {
  if (!IsEnabled()) return true;
  if (game::IsSumoWrestling() && IsDead()) {
    game::EndSumoWrestling(!!IsPlayer());
    return true;
  }
  if ((DeathFlags&FORCE_DEATH) || IsDead()) {
    if (Murderer && Murderer->IsPlayer() && GetTeam()->GetKillEvilness()) {
      game::DoEvilDeed(GetTeam()->GetKillEvilness());
    }
    festring SpecifierMsg;
    int SpecifierParts = 0;
    /*
    if (GetPolymorphBackup()) {
      SpecifierMsg << " polymorphed into ";
      id::AddName(SpecifierMsg, INDEFINITE);
      ++SpecifierParts;
    }
    */
    if (AddPolymorphedText(SpecifierMsg, " ")) {
      SpecifierParts += 1;
    }
    if (!(DeathFlags&IGNORE_TRAPS) && IsStuck()) {
      if (SpecifierParts++) SpecifierMsg << " and";
      SpecifierMsg << " caught in " << GetTrapDescription();
    }
    if (GetAction() && !((DeathFlags&IGNORE_UNCONSCIOUSNESS) && GetAction()->IsUnconsciousness())) {
      festring ActionMsg(GetAction()->GetDeathExplanation());
      if (!ActionMsg.IsEmpty()) {
        if (SpecifierParts > 1) {
          SpecifierMsg = ActionMsg << ',' << SpecifierMsg;
        } else {
          if (SpecifierParts) SpecifierMsg << " and";
          SpecifierMsg << ActionMsg;
        }
        ++SpecifierParts;
      }
    }
    festring NewMsg = Msg;
    if (Murderer == this) {
      SEARCH_N_REPLACE(NewMsg, CONST_S("@bkp"), CONST_S("by ") + GetPossessivePronoun(false) + " own");
      SEARCH_N_REPLACE(NewMsg, CONST_S("@bk"), CONST_S("by ") + GetObjectPronoun(false) + "self");
      SEARCH_N_REPLACE(NewMsg, CONST_S("@k"), GetObjectPronoun(false) + "self");
    } else if (Murderer) {
      SEARCH_N_REPLACE(NewMsg, CONST_S("@bkp"), CONST_S("by ") + Murderer->GetName(INDEFINITE) + "'s");
      SEARCH_N_REPLACE(NewMsg, CONST_S("@bk"), CONST_S("by ") + Murderer->GetName(INDEFINITE));
      SEARCH_N_REPLACE(NewMsg, CONST_S("@k"), CONST_S("by ") + Murderer->GetName(INDEFINITE));
    }
    if (SpecifierParts) NewMsg << " while" << SpecifierMsg;
    if (IsPlayer() && game::IsCheating()) {
      ADD_MESSAGE("Death message: %s. Score: %d.", NewMsg.CStr(), game::GetScore());
    }
    Die(Murderer, NewMsg, DeathFlags);
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::CheckStarvationDeath
//
//==========================================================================
truth character::CheckStarvationDeath (cfestring &Msg) {
  if (GetNP() < 1 && UsesNutrition() && !StateIsActivated(FASTING)) {
    return CheckDeath(Msg, 0, FORCE_DEATH);
  }
  return false;
}


//==========================================================================
//
//  character::ThrowItem
//
//==========================================================================
void character::ThrowItem (int Direction, item *ToBeThrown) {
  if (Direction > 7) ABORT("Throw in TOO odd direction...");
  ToBeThrown->Fly(this, Direction, GetAttribute(ARM_STRENGTH));
}


//==========================================================================
//
//  character::HasBeenHitByItem
//
//==========================================================================
void character::HasBeenHitByItem (character *Thrower, item *Thingy, int Damage,
                                  double ToHitValue, int Direction)
{
  if (IsPlayer()) ADD_MESSAGE("%s hits you.", Thingy->CHAR_NAME(DEFINITE));
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s hits %s.", Thingy->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
  int BodyPart = ChooseBodyPartToReceiveHit(ToHitValue, DodgeValue);
  int WeaponSkillHits = Thrower ? CalculateWeaponSkillHits(Thrower) : 0;
  int DoneDamage = ReceiveBodyPartDamage(Thrower, Damage, PHYSICAL_DAMAGE, BodyPart, Direction);
  truth Succeeded = (GetBodyPart(BodyPart) && HitEffect(Thrower, Thingy, Thingy->GetPos(), THROW_ATTACK, BodyPart, Direction, !DoneDamage, false, DoneDamage)) || DoneDamage;
  if (Succeeded && Thrower) Thrower->WeaponSkillHit(Thingy, THROW_ATTACK, WeaponSkillHits);
  festring DeathMsg = CONST_S("killed by a flying ")+Thingy->GetName(UNARTICLED);
  if (CheckDeath(DeathMsg, Thrower)) return;
  if (Thrower) {
    if (Thrower->CanBeSeenByPlayer())
      DeActivateVoluntaryAction(CONST_S("The attack of ")+Thrower->GetName(DEFINITE)+CONST_S(" interrupts you."));
    else
      DeActivateVoluntaryAction(CONST_S("The attack interrupts you."));
  } else {
    DeActivateVoluntaryAction(CONST_S("The hit interrupts you."));
  }
}


//==========================================================================
//
//  character::DodgesFlyingItem
//
//==========================================================================
truth character::DodgesFlyingItem (item *Item, double ToHitValue) {
  return !Item->EffectIsGood() && RAND_N((int)(100+ToHitValue/DodgeValue*100)) < 100;
}


struct XXInCommand {
public:
  inline XXInCommand () { game::InGetPlayerCommand = true; }
  inline ~XXInCommand () { game::InGetPlayerCommand = false; }
};


//==========================================================================
//
//  character::GetPlayerCommand
//
//==========================================================================
void character::GetPlayerCommand () {
  command *cmd;
  bool HasActed = false;
  bool popupDisabled = false;

  while (!HasActed) {
    game::DrawEverything();

    if (game::GetDangerFound() && !StateIsActivated(FEARLESS)) {
      if (game::GetDangerFound() > 500.0) {
        if (game::GetCausePanicFlag()) {
          game::SetCausePanicFlag(false);
          BeginTemporaryState(PANIC, 500+RAND_N(500));
        }
        game::AskForEscPress(CONST_S("You are horrified by your situation!"));
      } else if (ivanconfig::GetWarnAboutDanger()) {
        if (game::GetDangerFound() > 50.0) {
          game::AskForEscPress(CONST_S("You sense great danger!"));
        } else {
          game::AskForEscPress(CONST_S("You sense danger!"));
        }
      }
      game::SetDangerFound(0);
    }

    int Key;
    {
      //game::SetIsInGetCommand(true);
      auto guard = XXInCommand();
      Key = GET_KEY();
      //game::SetIsInGetCommand(false);
    }

    // gum!
    bool msgAreOld = true;
    if (msgAreOld) {
      command *xcmd;
      if (msgAreOld) {
        xcmd = commandsystem::FindCommand("DrawMessageHistory");
        if (xcmd && xcmd->IsKey(Key)) msgAreOld = false;
      }
      if (msgAreOld) {
        xcmd = commandsystem::FindCommand("ScrollMessagesDown");
        if (xcmd && xcmd->IsKey(Key)) msgAreOld = false;
      }
      if (msgAreOld) {
        xcmd = commandsystem::FindCommand("ScrollMessagesUp");
        if (xcmd && xcmd->IsKey(Key)) msgAreOld = false;
      }
    }
    if (msgAreOld) {
      msgsystem::ThyMessagesAreNowOld();
    }

    bool ValidKeyPressed = false;
    int Dir = game::MoveKeyToDir(Key, /*allowFastExt*/!game::IsInWilderness());
    if (Dir >= 0 && (Dir % 100) < DIRECTION_COMMAND_KEYS) {
      if (Dir >= 100 && !game::IsInWilderness()) {
        // C-dir: Go
        Dir %= 100;
        IvanAssert(Dir >= 0 && Dir <= 7);
        go *Go = go::Spawn(this);
        Go->SetDirection(Dir);
        Go->SetPrevWasTurn(false);
        SetAction(Go);
        EditAP(GetStateAPGain(100)); // gum solution
        GoOn(Go, true);
        HasActed = true;
      } else if (Dir < 100) {
        HasActed = TryMove(ApplyStateModification(game::GetMoveVector(Dir)), true,
                           game::PlayerIsRunning());
      }
      ValidKeyPressed = true;
    }

    if (!ValidKeyPressed && Key != KEY_CC_REDRAW && Key != KEY_CC_ACTED) {
      cmd = commandsystem::FindCommandByKey(Key);
      if (cmd) {
        if (game::IsInWilderness() && !cmd->IsUsableInWilderness()) {
          ADD_MESSAGE("This function cannot be used while in wilderness.");
        } else if (/*!game::WizardModeIsActive() &&*/ cmd->IsWizardModeFunction()) {
          //ConLogf("Activate wizardmode to use this function.");
        } else {
          HasActed = cmd->GetLinkedFunction()(this);
          if (popupDisabled) {
            ClearComparisonInfo();
          } else {
            UpdateComparisonInfo();
          }
        }
        ValidKeyPressed = true;
      } else if (Key == KEY_ESC && HasComparisonInfo()) {
        ClearComparisonInfo();
        popupDisabled = true;
        ValidKeyPressed = true;
      }
    }

    if (!ValidKeyPressed && Key != KEY_ESC && Key != KEY_CC_REDRAW && Key != KEY_CC_ACTED) {
      festring kname = commandsystem::GetHelpKeyNameFor("ShowKeyLayout");
      if (!kname.IsEmpty()) {
        ADD_MESSAGE("Unknown key. Press \1Y%s\2 for a list of commands.", kname.CStr());
      } else {
        ADD_MESSAGE("Unknown command key.");
      }
    }

    // aborted by console command
    if (Key == KEY_CC_REDRAW || Key == KEY_CC_ACTED) {
      game::GetCurrentArea()->SendNewDrawRequest();
      if (Key == KEY_CC_ACTED) HasActed = true;
    }
  }

  /*
  if (wasPopupChange) {
    game::GetCurrentArea()->SendNewDrawRequest();
    game::DrawEverything();
  }
  */
  if (HasComparisonInfo()) {
    game::DrawEverything(); // meh
  }

  game::IncreaseTurn();
}


//==========================================================================
//
//  character::Vomit
//
//==========================================================================
void character::Vomit (v2 Pos, int Amount, truth ShowMsg) {
  if (!CanVomit()) return;
  if (ShowMsg) {
    if (IsPlayer()) ADD_MESSAGE("You vomit.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s vomits.", CHAR_NAME(DEFINITE));
  }
  if (VomittingIsUnhealthy()) {
    EditExperience(ARM_STRENGTH, -75, 1 << 9);
    EditExperience(LEG_STRENGTH, -75, 1 << 9);
  }
  if (IsPlayer()) {
    EditNP(-2500-RAND_N(2501));
    CheckStarvationDeath(CONST_S("vomited himself to death"));
  }
  if (StateIsActivated(PARASITE_TAPE_WORM) && !RAND_8) {
    if (IsPlayer()) ADD_MESSAGE("You notice a dead broad tapeworm among your former stomach contents.");
    DeActivateTemporaryState(PARASITE_TAPE_WORM);
  }
  if (!game::IsInWilderness()) {
    GetNearLSquare(Pos)->ReceiveVomit(this, liquid::Spawn(GetVomitMaterial(), sLong(sqrt(GetBodyVolume())*Amount/1000)));
  }
}


//==========================================================================
//
//  character::Polymorph
//
//==========================================================================
truth character::Polymorph (character *NewForm, int Counter) {
  if (!NewForm) return false; // just in case
  if (!IsPolymorphable() || (!IsPlayer() && game::IsInWilderness())) {
    delete NewForm;
    return false;
  }

  RemoveTraps();
  if (GetAction()) {
    GetAction()->Terminate(false);
  }

  NewForm->SetAssignedName(festring::EmptyStr());
  if (IsPlayer()) {
    ADD_MESSAGE("Your body glows in a crimson light. You transform into %s!",
                NewForm->CHAR_NAME(INDEFINITE));
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s glows in a crimson light and %s transforms into %s!",
                CHAR_NAME(DEFINITE), GetPersonalPronoun().CStr(),
                NewForm->CHAR_NAME(INDEFINITE));
  }

  Flags |= C_IN_NO_MSG_MODE;
  NewForm->Flags |= C_IN_NO_MSG_MODE;
  NewForm->ChangeTeam(GetTeam());
  NewForm->GenerationDanger = GenerationDanger;
  NewForm->mOnEvents = this->mOnEvents;

  bool restoreLeader = false;
  if (GetTeam()->GetLeader() == this) {
    GetTeam()->SetLeader(NewForm);
    restoreLeader = true;
  }

  v2 Pos = GetPos();
  Remove();

  //k8: it is possible (why?!) to poly into something which cannot be placed.
  //    check for this;
  /*
  nope. this is wrong due to `PutToOrNear()` call to `CanMoveOn()`. fix it there.
  */

  if (!NewForm->PutToOrNear(Pos, /*allowFail*/true)) {
    // failed to put, revert
    ConLogf("ERROR: refused to poly into '%s` (no room)", NewForm->GetName(DEFINITE).CStr());
    Flags &= ~C_IN_NO_MSG_MODE;
    NewForm->Flags &= ~C_IN_NO_MSG_MODE;
    ADD_MESSAGE("But something prevented you to finish polymorphing, pushing you back into "
                "your original form.");
    if (restoreLeader) {
      GetTeam()->SetLeader(this);
    }
    PutToOrNear(Pos);
    delete NewForm;
    return false;
  }

  NewForm->SetAssignedName(GetAssignedName());
  NewForm->ActivateTemporaryState(POLYMORPHED);
  NewForm->SetTemporaryStateCounter(POLYMORPHED, Counter);

  if (TemporaryStateIsActivated(POLYMORPHED)) {
    NewForm->SetPolymorphBackup(GetPolymorphBackup());
    SetPolymorphBackup(0);
    SendToHell();
  } else {
    NewForm->SetPolymorphBackup(this);
    Flags |= C_POLYMORPHED;
    Disable();
  }

  GetStack()->MoveItemsTo(NewForm->GetStack());
  NewForm->SetMoney(GetMoney());
  DonateEquipmentTo(NewForm);
  Flags &= ~C_IN_NO_MSG_MODE;
  NewForm->Flags &= ~C_IN_NO_MSG_MODE;
  NewForm->CalculateAll();

  if (IsPlayer()) {
    Flags &= ~C_PLAYER;
    game::SetPlayer(NewForm);
    game::SendLOSUpdateRequest();
    UpdateESPLOS();
  }

  NewForm->TestWalkability();
  return true;
}


//==========================================================================
//
//  character::BeKicked
//
//==========================================================================
void character::BeKicked (character *Kicker, item *Boot, bodypart *Leg, v2 HitPos,
                          double KickDamage, double ToHitValue, int Success,
                          int Direction, truth Critical, truth ForceHit)
{
  //FIXME: other args
  if (!game::RunCharEvent(CONST_S("before_kicked_by"), this, Kicker, Boot)) return;

  VFXInfo vfx;
  DrawAttackFlash(&vfx, Kicker, this);
  auto hitres = TakeHit(Kicker, Boot, Leg, HitPos, KickDamage, ToHitValue, Success, KICK_ATTACK, Direction, Critical, ForceHit);
  AttackFlashDelay(&vfx);
  EraseAttackFlash(&vfx);
  if (hitres == HAS_HIT || hitres == HAS_BLOCKED || hitres == DID_NO_DAMAGE) {
    if (IsEnabled()) {
      if (Boot && (Boot->GetConfig() == BOOT_OF_DISPLACEMENT) && Kicker->Displace(this, true)) {
        return;
      }
      if (!CheckBalance(KickDamage) || (Boot && (Boot->GetConfig() == BOOT_OF_KICKING))) {
        if (IsPlayer()) {
          ADD_MESSAGE("The kick throws you off balance.");
        } else if (Kicker->IsPlayer()) {
          ADD_MESSAGE("The kick throws %s off balance.", CHAR_DESCRIPTION(DEFINITE));
        }
        v2 FallToPos = GetPos() + game::GetMoveVector(Direction);
        FallTo(Kicker, FallToPos);
      }
    }
  }
}


//==========================================================================
//
//  character::CheckBalance
//
//  Return true if still in balance
//
//==========================================================================
truth character::CheckBalance (double KickDamage) {
  return !CanMove() || IsStuck() || !KickDamage || (!IsFlying() && KickDamage*5 < RAND_N(GetSize()));
}


//==========================================================================
//
//  character::FallTo
//
//==========================================================================
void character::FallTo (character *GuiltyGuy, v2 Where) {
  EditAP(-500);
  lsquare *MoveToSquare[MAX_SQUARES_UNDER];
  int Squares = CalculateNewSquaresUnder(MoveToSquare, Where);
  if (Squares) {
    truth NoRoom = false;
    for (int c = 0; c < Squares; ++c) {
      olterrain *Terrain = MoveToSquare[c]->GetOLTerrain();
      if (Terrain && !CanMoveOn(Terrain)) { NoRoom = true; break; }
    }
    if (NoRoom) {
      if (HasHead()) {
        if (IsPlayer()) {
          ADD_MESSAGE("You hit your head on the wall.");
        } else if (CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s hits %s head on the wall.",
                      CHAR_NAME(DEFINITE), GetPossessivePronoun().CStr());
        }
      }
      ReceiveDamage(GuiltyGuy, 1+RAND_N(5), PHYSICAL_DAMAGE, HEAD);
      CheckDeath(CONST_S("killed by hitting a wall due to being kicked @bk"), GuiltyGuy);
    } else {
      if (IsFreeForMe(MoveToSquare[0])) Move(Where, true);
      // Place code that handles characters bouncing to each other here
    }
  }
}


//==========================================================================
//
//  character::CheckCannibalism
//
//==========================================================================
truth character::CheckCannibalism (cmaterial *What) const {
  return GetTorso()->GetMainMaterial()->IsSameAs(What);
}


//==========================================================================
//
//  character::StandIdleAI
//
//==========================================================================
void character::StandIdleAI () {
  SeekLeader(GetLeader());
  if (CheckDrink()) return;
  if (CheckForEnemies(true, true, true)) return;
  if (CheckForUsefulItemsOnGround()) return;
  if (FollowLeader(GetLeader())) return;
  if (CheckForDoors()) return;
  if (MoveTowardsHomePos()) return;
  if (CheckSadism()) return;
  EditAP(-1000);
}


//==========================================================================
//
//  character::LoseConsciousness
//
//==========================================================================
truth character::LoseConsciousness (int Counter, truth HungerFaint) {
  if (!AllowUnconsciousness()) return false;
  action *Action = GetAction();
  if (Action) {
    if (HungerFaint && !Action->AllowUnconsciousness()) return false;
    if (Action->IsUnconsciousness()) {
      static_cast<unconsciousness *>(Action)->RaiseCounterTo(Counter);
      return true;
    }
    Action->Terminate(false);
  }
  if (IsPlayer()) ADD_MESSAGE("You lose consciousness.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s loses consciousness.", CHAR_NAME(DEFINITE));
  unconsciousness *Unconsciousness = unconsciousness::Spawn(this);
  Unconsciousness->SetCounter(Counter);
  SetAction(Unconsciousness);
  return true;
}


//==========================================================================
//
//  character::DeActivateVoluntaryAction
//
//==========================================================================
void character::DeActivateVoluntaryAction (cfestring &Reason) {
  if (GetAction() && GetAction()->IsVoluntary()) {
    truth goon = false;
    if (IsPlayer()) {
      festring msg;
      if (!Reason.IsEmpty()) {
        msg << Reason << "\n";
        ADD_MESSAGE("%s", Reason.CStr());
      }
      goon = game::TruthQuestion(msg + "Continue " + GetAction()->GetDescription() + "?");
      // without this, attack flash causes the black screen
      GetLevel()->SendNewDrawRequest();
      game::DrawEverythingNoBlit();
    }
    if (goon) {
      GetAction()->ActivateInDNDMode();
    } else {
      GetAction()->Terminate(false);
    }
  }
}


//==========================================================================
//
//  character::ActionAutoTermination
//
//==========================================================================
void character::ActionAutoTermination () {
  if (!GetAction() || !GetAction()->IsVoluntary() || GetAction()->InDNDMode()) return;
  v2 Pos = GetPos();
  for (int c = 0; c != game::GetTeams(); c += 1) {
    if (GetTeam()->GetRelation(game::GetTeam(c)) == HOSTILE) {
      for (std::list<character *>::const_iterator i = game::GetTeam(c)->GetMember().begin();
            i != game::GetTeam(c)->GetMember().end(); ++i)
      {
        character *XChar = *i;
        //if (XChar->IsEnabled() && XChar->CanBeSeenBy(this, false, true) &&
        //    (XChar->CanMove() || XChar->GetPos().IsAdjacent(Pos)) && XChar->CanAttack())
        if (XChar->IsEnabled() && XChar->CanAttack() &&
            (XChar->CanMove() || XChar->GetPos().IsAdjacent(Pos)) &&
            XChar->CanBeSeenBy(this, false, true))
        {
          if (IsPlayer()) {
            festring msg;
            msg << XChar->CHAR_NAME(DEFINITE) << " seems to be hostile.";
            ADD_MESSAGE("%s", msg.CStr());
            if (game::TruthQuestion(msg + "\nContinue " + GetAction()->GetDescription() + "?")) {
              GetAction()->ActivateInDNDMode();
            } else {
              GetAction()->Terminate(false);
            }
          } else {
            /*
            ConLogf("%s action is %s%s (autoterminated).",
                    CHAR_NAME(DEFINITE), (GetAction()->IsVoluntary() ? "voluntary " : ""),
                    GetAction()->GetDescription());
            */
            GetAction()->Terminate(false);
          }
          return;
        }
      }
    }
  }
}


//==========================================================================
//
//  character::CheckForEnemies
//
//==========================================================================
truth character::CheckForEnemies (truth CheckDoors, truth CheckGround, truth MayMoveRandomly,
                                  truth RunTowardsTarget)
{
  if (!IsEnabled()) return false;
  truth HostileCharsNear = false;
  character *NearestChar = 0;
  sLong NearestDistance = 0x7FFFFFFF;
  v2 Pos = GetPos();
  for (int c = 0; c < game::GetTeams(); ++c) {
    if (GetTeam()->GetRelation(game::GetTeam(c)) == HOSTILE) {
      for (std::list<character*>::const_iterator i = game::GetTeam(c)->GetMember().begin(); i != game::GetTeam(c)->GetMember().end(); ++i) {
        character *XChar = *i;
        if (XChar->IsEnabled() && GetAttribute(WISDOM) < XChar->GetAttackWisdomLimit()) {
          sLong ThisDistance = Max<sLong>(abs(XChar->GetPos().X - Pos.X), abs(XChar->GetPos().Y - Pos.Y));
          if (ThisDistance <= GetLOSRangeSquare()) HostileCharsNear = true;
          if ((ThisDistance < NearestDistance || (ThisDistance == NearestDistance && !RAND_N(3))) &&
              XChar->CanBeSeenBy(this, false, IsGoingSomeWhere()) &&
              (!IsGoingSomeWhere() || HasClearRouteTo(XChar->GetPos())))
          {
            NearestChar = XChar;
            NearestDistance = ThisDistance;
          }
        }
      }
    }
  }

  if (NearestChar) {
    if (GetAttribute(INTELLIGENCE) >= 10 || IsSpy()) game::CallForAttention(GetPos(), 100);
    if (SpecialEnemySightedReaction(NearestChar)) return true;
    if (IsExtraCoward() && !StateIsActivated(PANIC) && NearestChar->GetRelativeDanger(this) >= 0.5 && !StateIsActivated(FEARLESS)) {
      if (CanBeSeenByPlayer()) ADD_MESSAGE("%s sees %s.", CHAR_NAME(DEFINITE), NearestChar->CHAR_DESCRIPTION(DEFINITE));
      BeginTemporaryState(PANIC, 500+RAND_N(500));
    }
    if (!IsRetreating()) {
      if (CheckGround && NearestDistance > 2 && CheckForUsefulItemsOnGround(false)) {
        return true;
      }
      SetGoingTo(NearestChar->GetPos());
    } else {
      SetGoingTo(Pos-((NearestChar->GetPos()-Pos)<<4));
    }
    return MoveTowardsTarget(true);
  } else {
    character *Leader = GetLeader();
    if (Leader == this) Leader = 0;
    if (!Leader && IsGoingSomeWhere()) {
      if (!MoveTowardsTarget(RunTowardsTarget)) {
        TerminateGoingTo();
        return false;
      } else {
        if (!IsEnabled()) return true;
        if (GetPos() == GoingTo) {
          TerminateGoingTo();
        }
        return true;
      }
    } else {
      if ((!Leader || (Leader && !IsGoingSomeWhere())) && HostileCharsNear) {
        if (CheckDoors && CheckForDoors()) {
          return true;
        }
        if (CheckGround && CheckForUsefulItemsOnGround()) {
          return true;
        }
        if (MayMoveRandomly && MoveRandomly()) {
          return true; // one has heard that an enemy is near but doesn't know where
        }
      }
      return false;
    }
  }
}


//==========================================================================
//
//  character::CheckForDoors
//
//==========================================================================
truth character::CheckForDoors () {
  if (!CanOpen() || !IsEnabled()) return false;
  for (int d = 0; d < GetNeighbourSquares(); ++d) {
    lsquare *Square = GetNeighbourLSquare(d);
    if (Square && Square->GetOLTerrain() && Square->GetOLTerrain()->Open(this)) return true;
  }
  return false;
}


//==========================================================================
//
//  character::IsDrinkableCanister
//
//==========================================================================
material *character::IsDrinkableCanister (item *Item) const {
  if (Item && (Item->IsBottle() || Item->IsCan()) && /*!Item->IsBroken() &&*/
      !Item->IsKleinBottle())
  {
    //material *mat = Item->GetSecondaryMaterial();
    material *mat = Item->GetConsumeMaterial(this);
    if (mat && mat->IsLiquid()) return mat;
  }
  return 0;
}


//==========================================================================
//
//  character::ConsumeRandomPotion
//
//==========================================================================
truth character::ConsumeRandomPotion (itemvector &foundList) {
  while (foundList.size() != 0) {
    const size_t idx = (size_t)RAND_N((int)foundList.size());
    item *Item = foundList[idx];
    if (/*Item->CanBeEatenByAI(this) &&*/
        ConsumeItem(Item, festring(Item->GetConsumeMaterial(this)->GetConsumeVerb())))
    {
      return true;
    }
    foundList.erase(foundList.begin() + idx);
  }
  return false;
}


//==========================================================================
//
//  character::FindGoodPotions
//
//==========================================================================
void character::FindGoodPotions (itemvector &foundList,
                                 bool (*checkCB) (character *Char, material *mat, void *udata),
                                 void *udata)
{
  itemvector ItemVector;
  GetStack()->FillItemVector(ItemVector);
  for (size_t c = 0; c != ItemVector.size(); c += 1) {
    item *Item = ItemVector[c];
    material *mat = IsDrinkableCanister(Item);
    if (mat && checkCB(this, mat, udata)) {
      foundList.push_back(Item);
    }
  }
}


//==========================================================================
//
//  GoodHealingPotion
//
//==========================================================================
static bool GoodHealingPotion (character *Char, material *mat, void *udata) {
  return (mat->GetEffect() == EFFECT_HEAL);
}


//==========================================================================
//
//  GoodAntidotePotion
//
//==========================================================================
static bool GoodAntidotePotion (character *Char, material *mat, void *udata) {
  return (mat->GetEffect() == EFFECT_ANTIDOTE);
}


//==========================================================================
//
//  GoodAlcoholPotion
//
//==========================================================================
static bool GoodAlcoholPotion (character *Char, material *mat, void *udata) {
  return mat->DisablesPanicWhenConsumed();
}


//==========================================================================
//
//  GoodWaterPotion
//
//==========================================================================
static bool GoodWaterPotion (character *Char, material *mat, void *udata) {
  return (mat->GetConfig() == WATER);
}


//==========================================================================
//
//  character::CheckDrink
//
//  consume healing potion, if necessary.
//  consume some alcohol if paniced.
//
//==========================================================================
truth character::CheckDrink () {
  if (!IsEnabled() || IsPlayer()) return false;
  if (GetAttribute(INTELLIGENCE) <= 5) return false; // too dumb
  if (IsZombie() || CannotDrinkPotions() || IsMushroom()) return false; // no reason

  const int intl = GetAttribute(INTELLIGENCE);

  itemvector foundList;

  // drink antidote?
  if (IsHumanoid() && intl >= 15) {
    // StateIsActivated(PARASITE_TAPE_WORM)
    if (StateIsActivated(PARASITE_MIND_WORM) || StateIsActivated(LEPROSY)) {
      // look for antidote potion
      FindGoodPotions(foundList, &GoodAntidotePotion, 0);
      if (foundList.size() != 0) {
        ConLogf("%s wants to drink some antidote due to %s...",
                CHAR_NAME(DEFINITE),
                (StateIsActivated(PARASITE_MIND_WORM) && StateIsActivated(LEPROSY) ? "mindworm+leprosy"
                 : StateIsActivated(PARASITE_MIND_WORM) ? "mindworm" : "leprosy"));
      }
    }
  }

  int needBPHealing = -1;
  int missBP = -1;
  // check if some vital body part in danger
  if (intl >= 10) {
    for (int c = 0; c != BodyParts && needBPHealing < 0; c += 1) {
      bodypart *BodyPart = GetBodyPart(c);
      if (BodyPart && BodyPartIsVital(c)) {
        int ConditionColorIndex = BodyPart->GetConditionColorIndex();
        if (ConditionColorIndex <= 1) {
          needBPHealing = c;
        }
      } else if (!BodyPart && IsHumanoid() && intl >= 14) {
        // if missing a body part, try to consume healing liquid.
        // it may grow the body part back.
        needBPHealing = c;
        missBP = c; // for debug
      }
    }
  }

  if (needBPHealing >= 0 || (GetHP() < GetMaxHP() / 2 && intl >= 6)) {
    const size_t oldSize = foundList.size();
    FindGoodPotions(foundList, &GoodHealingPotion, 0);
    if (foundList.size() != oldSize) {
      ConLogf("%s: hp=%d; max=%d; needBP=%d (%s); missBP=%d (%s)",
              CHAR_NAME(DEFINITE), GetHP(), GetMaxHP(),
              needBPHealing, (needBPHealing >= 0 ? GetBodyPartName(needBPHealing).CStr() : "<none>"),
              missBP, (missBP >= 0 ? GetBodyPartName(missBP).CStr() : "<none>"));
    }
  }

  // drink random potion from the list
  if (foundList.size()) {
    if (GetAction() && GetAction()->IsConsume()) {
      return true; // still consuming something
    }
    if (ConsumeRandomPotion(foundList)) return true;
  }

  // if paniced, try to drink some vodka, why not?
  // actually, nobody needs a lot of Int to drink vodka, i guess...
  if (IsHumanoid() && StateIsActivated(PANIC) && intl >= 6) {
    foundList.clear();
    FindGoodPotions(foundList, &GoodAlcoholPotion, 0);
    if (foundList.size()) {
      if (GetAction() && GetAction()->IsConsume()) {
        return true; // still consuming something
      }
      if (ConsumeRandomPotion(foundList)) return true;
    }
  }

  // if exhausted, drink some water
  if (IsHumanoid() && intl >= 10) {
    auto trst = GetTirednessState();
    if (trst == FAINTING || trst == EXHAUSTED) {
      foundList.clear();
      FindGoodPotions(foundList, &GoodWaterPotion, 0);
      if (foundList.size()) {
        if (GetAction() && GetAction()->IsConsume()) {
          return true; // still consuming something
        }
        if (ConsumeRandomPotion(foundList)) return true;
      }
    }
  }

  return false;
}


//==========================================================================
//
//  character::CheckForUsefulItemsOnGround
//
//==========================================================================
truth character::CheckForUsefulItemsOnGround (truth CheckFood) {
  if (StateIsActivated(PANIC) || !IsEnabled()) {
    return false;
  }
  itemvector ItemVector;
  GetStackUnder()->FillItemVector(ItemVector);
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    item *Item = ItemVector[c];
    if (Item->CanBeSeenBy(this) && Item->IsPickable(this)) {
      if ((!IsPet() || !(CommandFlags & DONT_CHANGE_EQUIPMENT)) && TryToEquip(Item)) {
        return true;
      }
      if (CheckFood && UsesNutrition() && !CheckIfSatiated() && TryToConsume(Item)) {
        return true;
      }
      if (IsRangedAttacker() && CanThrowItem(Item) && TryToAddToInventory(Item)) {
        return true;
      }
    }
  }
  return false;
}


//==========================================================================
//
//  FinderDebugSquareReject
//
//==========================================================================
static void FinderDebugSquareReject (lsquare *Square, ccharacter *Char, item *Item, cchar *reason) {
  #ifdef DEBUG_FINDER_SQUARE_CHECK
  if (Char->IsPet() || true) {
    // debug output
    if (DEBUG_PICKUP_AI < 0) {
      ConLogf("char #%u:'%s' rejected item '%s' at (%d, %d) due to %s.",
              Char->GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
              Item->GetPos().X, Item->GetPos().Y,
              reason);
    }
    // done debug output
  }
  #endif
}


#define FAI_CHECK_SQUARE  \
  /* item visibility check */ \
  if (!sqVisChecked) { \
    /* ignore dangerous squares */ \
    if (Square->IsDangerous(this) || Square->IsScary(this)) { \
      FinderDebugSquareReject(Square, this, Item, "dangerous or scary"); \
      break; \
    } \
    if (!Square->CanBeSeenByEx(this, /*ignore darkness*/!IsPlayer())) { \
      FinderDebugSquareReject(Square, this, Item, "square can't be seen"); \
      break; \
    } \
    sqVisChecked = true; \
  } \
  if (!visChecked) { \
    if (IsPlayer() && !Item->CanBeSeenBy(this)) { \
      FinderDebugSquareReject(Square, this, Item, "invisible item"); \
      continue; \
    } \
    visChecked = true; \
  }


//==========================================================================
//
//  character::FindUsefulItemAround
//
//  return ERROR_V2 or position
//
//==========================================================================
v2 character::FindUsefulItemAround (truth SameRoom, feuLong *itemID) {
  if (itemID) *itemID = 0;

  if (!IsEnabled() || StateIsActivated(PANIC) || !NeedCheckNearbyItems() ||
      !CanMove() || SquaresUnder != 1/*big*/)
  {
    return ERROR_V2;
  }

  // this prolly means gameover...
  if (!PLAYER || !PLAYER->IsEnabled()) {
    return ERROR_V2;
  }

  // cheat: do not use this Very Advanced AI for NPCs which are neither friendly
  // to the player, nor hostile. this is so town population will not try to pick
  // up items, it is mostly useless.
  if (this != PLAYER) {
    if (GetRelation(PLAYER) == UNCARING &&
        PLAYER->GetRelation(this) == UNCARING)
    {
      return ERROR_V2;
    }
  }

  room *myRoom = GetRoom();
  // do not allow shopkeepers to pick their own items
  if (myRoom && myRoom->GetMaster() == this) {
    return ERROR_V2;
  }

  // humanoids without a head should not seek items
  if (IsHumanoid()) {
    //if (!GetHead()) return ERROR_V2;
    if (BodyParts <= HEAD_INDEX || !GetBodyPart(HEAD_INDEX)) return ERROR_V2;
  }

  double newDanger = -1;
  v2 newEquipPos = ERROR_V2;
  feuLong equipID = 0;
  v2 nearestFood = ERROR_V2;
  feuLong foodID = 0;
  v2 nearestPickup = ERROR_V2;
  feuLong pickupID = 0;
  bool preferFood = false;

  // if not hungry enough, ignore food
  bool CheckFood = NeedCheckNearbyFood() && UsesNutrition();
  if (CheckFood) {
    int np = GetNP();
    if (np <= HUNGER_LEVEL) {
      preferFood = true;
    } else if (np > NOT_HUNGER_LEVEL && np < HUNGER_LEVEL) {
      auto hprc = GetCurrentHungerStatePercent();
      if (hprc > 30) CheckFood = false; // not "almost hungry"
    } else {
      CheckFood = false;
    }
  }

  bool CheckEquip =
    (!IsPet() || !(CommandFlags & DONT_CHANGE_EQUIPMENT)) &&
    NeedCheckNearbyEquipment();

  int doDebug = -1;

  // do not go too far away
  int range = Max(1, GetLOSRange());
  const int IntStat = GetAttribute(INTELLIGENCE);

       if (IntStat < 5) range = Min(range, 3); // kobolds! ;-)
  else if (IntStat < 10) range = Min(range, 4);
  else if (IntStat < 14) range = Min(range, 6);
  else if (IntStat < 16) range = Min(range, 7);
  else if (IntStat < 20) range = Min(range, 8);
  else if (IntStat < 22) range = Min(range, 11);
  else range = Min(range, 14);
  const int rangeSq = range * range;

  #if 0
  if (IsPet() || true) {
    ConLogf("**** char #%u:'%s' finder ai: CheckFood=%d; CheckEquip=%d; range=%d ****",
            GetID(), DebugLogName().CStr(),
            (int)CheckFood, (int)CheckEquip, range);
  }
  #endif

  for (int dx = -range; dx <= range; dx += 1) {
    for (int dy = -range; dy <= range; dy += 1) {
      v2 pos = GetPos() + v2(dx, dy);
      // postpone visibility check
      if ((dx | dy) != 0 && GetLevel()->IsValidPos(pos) &&
          GetPos().GetSquaredDistance(pos) <= rangeSq)
      {
        lsquare *MoveToSquare[MAX_SQUARES_UNDER];
        const int Squares = CalculateNewSquaresUnder(MoveToSquare, pos);
        for (int c = 0; c < Squares; c += 1) {
          lsquare *Square = MoveToSquare[c];
          //FIXME: some chars CAN go to walls, but meh...
          if (!Square->IsTransparent()) continue; // don't go to walls
          // check if someone is standing at the square
          character *cc = Square->GetCharacter();
          if (cc) {
            #if 0
            // debug output
            if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
            if (doDebug) {
              ConLogf("char #%u:'%s' rejected position (%d, %d) due to char '%s'.",
                      GetID(), DebugLogName().CStr(),
                      pos.X, pos.Y,
                      cc->GetClassID(), cc->DebugLogName().CStr());
            }
            // done debug output
            #endif
            continue;
          }
          // has any items?
          if (!Square->GetStack()->HasAnyItem()) continue;
          // ignore dangerous squares
          //if (Square->IsDangerous(this) || Square->IsScary(this)) continue;
          // get item list
          itemvector ItemVector;
          Square->GetStack()->FillItemVector(ItemVector);
          if (ItemVector.size() == 0) continue;
          bool sqVisChecked = false;
          // check for a good item
          double myDanger = 0; // to avoid recalculating
          for (uInt c = 0; c < ItemVector.size(); ++c) {
            item *Item = ItemVector[c];
            if (!Item->IsPickable(this)) continue;
            //IvanAssert(Item->GetPos() == Square->GetPos());
            // this is -- i believe -- for 32x32 corpses only.

            if (Item->GetPos() != Square->GetPos()) {
              if (!Item->IsLargeCorpse()) {
                ConLogf("WARNING: `FindUsefulItemAround()` #%u:`%s` got bad square: "
                        "square at (%d, %d); item '%s' at (%d, %d)",
                        GetID(), DebugLogName().CStr(),
                        Square->GetPos().X, Square->GetPos().Y,
                        Item->DebugLogName().CStr(),
                        Item->GetPos().X, Item->GetPos().Y);
              }
              continue;
            }

            if (Item->GetPos() != pos) {
              if (!Item->IsLargeCorpse()) {
                ConLogf("WARNING: `FindUsefulItemAround()` #%u:`%s` got bad item: "
                        "pos=(%d, %d); item '%s' at (%d, %d)",
                        GetID(), DebugLogName().CStr(),
                        pos.X, pos.Y,
                        Item->DebugLogName().CStr(),
                        Item->GetPos().X, Item->GetPos().Y);
              }
              continue;
            }

            #if 0
            // debug output
            if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
            if (doDebug && IsPet()) {
              ConLogf("char #%u:'%s' checking item '%s' at (%d, %d)...",
                      GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                      pos.X, pos.Y);
            }
            // done debug output
            #endif

            // don't bother with items in another room if asked to stay in the same room
            if (SameRoom && myRoom && Item->GetRoom() != myRoom) {
              #if 0
              // debug output
              if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
              if (doDebug) {
                ConLogf("char #%u:'%s' rejected item '%s' at (%d, %d) due to different room.",
                        GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                        pos.X, pos.Y);
              }
              // done debug output
              #endif
              continue;
            }

            bool visChecked = false;
            // equipment check
            if (CheckEquip && PreCheckEquip(Item)) {
              FAI_CHECK_SQUARE
              #if 0
              // debug output
              if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
              if (doDebug) {
                ConLogf("char #%u:'%s' is checking equipment item '%s' at (%d, %d)...",
                        GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                        pos.X, pos.Y);
              }
              // done debug output
              #endif
              double ndg = CheckWantToEquip(Item, &myDanger);
              if (ndg > 0) {
                if (ndg > newDanger) {
                  newDanger = ndg;
                  newEquipPos = pos;
                  equipID = Item->GetID();
                } else if (ndg == newDanger) {
                  // go to the closest one
                  IvanAssert(newEquipPos != ERROR_V2);
                  int oldDist = GetPos().GetManhattanDistance(newEquipPos);
                  int newDist = GetPos().GetManhattanDistance(pos);
                  if (newDist < oldDist) {
                    newDanger = ndg;
                    newEquipPos = pos;
                    equipID = Item->GetID();
                  }
                }
                continue;
              }
            }

            // food check
            if (CheckFood && PreCheckConsume(Item)) {
              FAI_CHECK_SQUARE
              // debug output
              if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
              if (doDebug) {
                ConLogf("char #%u:'%s' is checking food item '%s' at (%d, %d)...",
                        GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                        pos.X, pos.Y);
              }
              // done debug output
              if (nearestFood == ERROR_V2) {
                nearestFood = pos;
                foodID = Item->GetID();
              } else {
                int oldDist = GetPos().GetManhattanDistance(nearestFood);
                int newDist = GetPos().GetManhattanDistance(pos);
                if (newDist < oldDist) {
                  nearestFood = pos;
                  foodID = Item->GetID();
                }
              }
              continue;
            }

            // ranged attackers may want to get something to throw
            if (IsRangedAttacker() && CanThrowItem(Item) && PreCheckPickup(Item)) {
              FAI_CHECK_SQUARE
              // debug output
              if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
              if (doDebug) {
                ConLogf("char #%u:'%s' is checking inventory item '%s' at (%d, %d)...",
                        GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                        pos.X, pos.Y);
              }
              // done debug output
              if (TryToAddToInventory(Item, /*check*/true)) {
                if (nearestPickup == ERROR_V2) {
                  nearestPickup = pos;
                  pickupID = Item->GetID();
                } else {
                  int oldDist = GetPos().GetManhattanDistance(nearestPickup);
                  int newDist = GetPos().GetManhattanDistance(pos);
                  if (newDist < oldDist) {
                    nearestPickup = pos;
                    pickupID = Item->GetID();
                  }
                }
                continue;
              }
            }
          }
        }
      }
    }
  }

  // preferences: gear, pickup, food
  if (preferFood) {
    // too hungry, prefer running for food
    if (nearestFood != ERROR_V2) {
      if (itemID) *itemID = foodID;
      return nearestFood;
    }
  }
  if (newEquipPos != ERROR_V2) {
    if (itemID) *itemID = equipID;
    return newEquipPos;
  }
  if (nearestPickup != ERROR_V2) {
    if (itemID) *itemID = pickupID;
    return nearestPickup;
  }
  if (nearestFood != ERROR_V2) {
    if (itemID) *itemID = foodID;
    return nearestFood;
  }

  return ERROR_V2;
}


//==========================================================================
//
//  character::CanThrowItem
//
//==========================================================================
truth character::CanThrowItem (item *ToBeChecked) {
  return
    ToBeChecked->IsThrowingWeapon() &&
    (ToBeChecked->GetThrowItemTypes() & GetWhatThrowItemTypesToThrow());
}


//==========================================================================
//
//  character::CheckThrowItemOpportunity
//
//==========================================================================
truth character::CheckThrowItemOpportunity () {
  if (!IsEnabled() || !IsRangedAttacker() || !CanThrow() ||
      !IsHumanoid() || !IsSmall())
  {
    return false; // total gum
  }
  //ConLogf("character::CheckThrowItemOpportunity...");
  // Steps:
  // (1) - Acquire target as nearest enemy
  // (2) - Check that this enemy is in range, and is in appropriate direction; no friendly fire!
  // (3) - check inventory for throwing weapon, select this weapon
  // (4) - throw item in direction where the enemy is

  //Check the visible area for hostiles
  int ThrowDirection = 0;
  int TargetFound = 0;
  v2 Pos = GetPos();
  v2 TestPos;
  int RangeMax = GetLOSRange();
  int CandidateDirections[7] = {0, 0, 0, 0, 0, 0, 0};
  int HostileFound = 0;
  item *ToBeThrown = 0;
  level *Level = GetLevel();

  for (int r = 1; r <= RangeMax; r += 1) {
    for (int dir = 0; dir < MDIR_STAND; dir += 1) {
      switch (dir) {
        case 0: TestPos = v2(Pos.X-r, Pos.Y-r); break;
        case 1: TestPos = v2(Pos.X, Pos.Y-r); break;
        case 2: TestPos = v2(Pos.X+r, Pos.Y-r); break;
        case 3: TestPos = v2(Pos.X-r, Pos.Y); break;
        case 4: TestPos = v2(Pos.X+r, Pos.Y); break;
        case 5: TestPos = v2(Pos.X-r, Pos.Y+r); break;
        case 6: TestPos = v2(Pos.X, Pos.Y+r); break;
        case 7: TestPos = v2(Pos.X+r, Pos.Y+r); break;
      }
      if (Level->IsValidPos(TestPos)) {
        square *TestSquare = GetNearSquare(TestPos);
        character *Dude = TestSquare->GetCharacter();
        if (Dude && Dude->IsEnabled() && Dude->CanBeSeenBy(this, false, true)) {
          if (GetRelation(Dude) != HOSTILE) {
            CandidateDirections[dir] = BLOCKED;
          } else if (GetRelation(Dude) == HOSTILE && CandidateDirections[dir] != BLOCKED) {
            //then load this candidate position direction into the vector of possible throw directions
            CandidateDirections[dir] = SUCCESS;
            HostileFound = 1;
          }
        }
      }
    }
  }

  if (HostileFound) {
    for (int dir = 0; dir < MDIR_STAND; dir += 1) {
      if (CandidateDirections[dir] == SUCCESS && !TargetFound) {
        ThrowDirection = dir;
        TargetFound = 1;
        break;
      }
    }
    if (!TargetFound) return false;
  } else {
    return false;
  }
  //ConLogf("throw: has target.");

  // check inventory for throwing weapon
  itemvector ItemVector;
  GetStack()->FillItemVector(ItemVector);
  // shuffle vector
  if (ItemVector.size() > 1) {
    // shuffle: https://en.wikipedia.org/wiki/Fisher-Yates_shuffle
    for (size_t f = (size_t)ItemVector.size() - 1; f != 0; f -= 1) {
      const size_t swp = (size_t)RAND_GOOD((int)f + 1);
      if (swp != f) {
        item *tmp = ItemVector[f];
        ItemVector[f] = ItemVector[swp];
        ItemVector[swp] = tmp;
      }
    }
  }

  for (size_t c = 0; !ToBeThrown && c != ItemVector.size(); c += 1) {
    if (CanThrowItem(ItemVector[c])) {
      ToBeThrown = ItemVector[c];
    }
  }
  if (!ToBeThrown) return false;

  //ConLogf("throw: has throwing weapon.");
  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s throws %s.", CHAR_NAME(DEFINITE), ToBeThrown->CHAR_NAME(INDEFINITE));
  }

  ThrowItem(ThrowDirection, ToBeThrown);
  EditExperience(ARM_STRENGTH, 75, 1<<8);
  EditExperience(DEXTERITY, 75, 1<<8);
  EditExperience(PERCEPTION, 75, 1<<8);
  EditNP(-50);
  DexterityAction(5);

  //k8: why? because of attack?
  TerminateGoingTo();
  return true;
}


//==========================================================================
//
//  character::CheckAIZapOpportunity
//
//==========================================================================
truth character::CheckAIZapOpportunity () {
  if (/*!IsRangedAttacker() || */ !IsEnabled() || !CanZap() ||
      !IsHumanoid() || !IsSmall())
  {
    return false; // total gum
  }
  // Steps:
  // (1) - Acquire target as nearest enemy
  // (2) - Check that this enemy is in range, and is in appropriate direction; no friendly fire!
  // (3) - check inventory for zappable item
  // (4) - zap item in direction where the enemy is
  //Check the rest of the visible area for hostiles
  v2 Pos = GetPos();
  v2 TestPos;
  int SensibleRange = 5;
  int RangeMax = GetLOSRange();
  if (RangeMax < SensibleRange) SensibleRange = RangeMax;
  int CandidateDirections[7] = {0, 0, 0, 0, 0, 0, 0};
  int HostileFound = 0;
  int ZapDirection = 0;
  int TargetFound = 0;
  item *ToBeZapped = 0;
  level *Level = GetLevel();

  for (int r = 2; r <= SensibleRange; r += 1) {
    for (int dir = 0; dir < MDIR_STAND; dir += 1) {
      switch (dir) {
        case 0: TestPos = v2(Pos.X-r, Pos.Y-r); break;
        case 1: TestPos = v2(Pos.X, Pos.Y-r); break;
        case 2: TestPos = v2(Pos.X+r, Pos.Y-r); break;
        case 3: TestPos = v2(Pos.X-r, Pos.Y); break;
        case 4: TestPos = v2(Pos.X+r, Pos.Y); break;
        case 5: TestPos = v2(Pos.X-r, Pos.Y+r); break;
        case 6: TestPos = v2(Pos.X, Pos.Y+r); break;
        case 7: TestPos = v2(Pos.X+r, Pos.Y+r); break;
      }
      if (Level->IsValidPos(TestPos)) {
        square *TestSquare = GetNearSquare(TestPos);
        character *Dude = TestSquare->GetCharacter();
        if (Dude && Dude->IsEnabled() && Dude->CanBeSeenBy(this, false, true)) {
          if (GetRelation(Dude) != HOSTILE) {
            CandidateDirections[dir] = BLOCKED;
          } else if (GetRelation(Dude) == HOSTILE && CandidateDirections[dir] != BLOCKED) {
            //then load this candidate position direction into the vector of possible zap directions
            CandidateDirections[dir] = SUCCESS;
            HostileFound = 1;
          }
        }
      }
    }
  }

  if (HostileFound) {
    for (int dir = 0; dir < MDIR_STAND; dir += 1) {
      if (CandidateDirections[dir] == SUCCESS && !TargetFound) {
        ZapDirection = dir;
        TargetFound = 1;
        break;
      }
    }
    if (!TargetFound) return false;
  } else {
    return false;
  }

  // check inventory for zappable item
  itemvector ItemVector;
  GetStack()->FillItemVector(ItemVector);
  for (size_t c = 0; c != ItemVector.size(); c += 1) {
    if (ItemVector[c]->GetMinCharges() > 0 && ItemVector[c]->GetPrice()) {
      // bald-faced gum solution for choosing zappables that have shots left.
      // MinCharges needs to be replaced. Empty wands have zero price!
      ToBeZapped = ItemVector[c];
      break;
    }
  }
  if (!ToBeZapped) return false;

  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s zaps %s.", CHAR_NAME(DEFINITE), ToBeZapped->CHAR_NAME(INDEFINITE));
  }

  //k8: why? because of attack?
  TerminateGoingTo();

  if (ToBeZapped->Zap(this, GetPos(), ZapDirection)) {
    EditAP(-100000 / APBonus(GetAttribute(PERCEPTION)));
    return true;
  } else {
    return false;
  }

  // unreachable code
  //TerminateGoingTo();
  //return true;
}


//==========================================================================
//
//  character::GetAdjustedStaminaCost
//
//  GetAdjustedStaminaCost(-1000, GetAttribute(ARM_STRENGTH))
//  is
//  (-10000 / GetAttribute(ARM_STRENGTH);
//
//==========================================================================
int character::GetAdjustedStaminaCost (int BaseCost, int Attribute) {
  if (Attribute > 1) {
    return BaseCost / log10(Attribute);
  } else {
    return BaseCost / 0.20;
  }
}


//==========================================================================
//
//  character::FollowLeader
//
//==========================================================================
truth character::FollowLeader (character *Leader) {
  if (!Leader || Leader == this || !IsEnabled()) {
    return false;
  }

  if ((CommandFlags & FOLLOW_LEADER) && Leader->CanBeSeenBy(this) &&
      Leader->SquareUnderCanBeSeenBy(this, true))
  {
    v2 Distance = GetPos() - GoingTo;
    if (abs(Distance.X) <= 2 && abs(Distance.Y) <= 2) return false;
    // try to pickup items first
    if (NeedCheckNearbyItems() && CheckForUsefulItemsOnGround(false)) return true;
    // allow going for items
    if (MoveToInterestingItem(false)) return true;
    return MoveTowardsTarget(false);
  }

  if (IsGoingSomeWhere()) {
    // try to pickup items first
    if (NeedCheckNearbyItems() && CheckForUsefulItemsOnGround(false)) return true;
    if (!MoveTowardsTarget(true)) {
      TerminateGoingTo();
      return false;
    }
    return true;
  }

  return false;
}


//==========================================================================
//
//  character::SeekLeader
//
//==========================================================================
void character::SeekLeader (ccharacter *Leader) {
  if (Leader && Leader != this) {
    if (Leader->CanBeSeenBy(this) &&
        (Leader->SquareUnderCanBeSeenBy(this, true) || !IsGoingSomeWhere()))
    {
      if (CommandFlags & FOLLOW_LEADER) {
        SetGoingTo(Leader->GetPos());
      }
    } else if (!IsGoingSomeWhere()) {
      team *Team = GetTeam();
      for (std::list<character *>::const_iterator i = Team->GetMember().begin();
              i != Team->GetMember().end(); ++i)
      {
        character *XChar = *i;
        if (XChar->IsEnabled() && XChar->GetID() != GetID() &&
            (CommandFlags & FOLLOW_LEADER) == (XChar->CommandFlags & FOLLOW_LEADER) &&
            XChar->CanBeSeenBy(this))
        {
          v2 Pos = XChar->GetPos();
          v2 Distance = GetPos()-Pos;
          if (abs(Distance.X) > 2 && abs(Distance.Y) > 2) {
            SetGoingTo(Pos);
            break;
          }
        }
      }
    }
  }
}


//==========================================================================
//
//  character::GetMoveEase
//
//==========================================================================
int character::GetMoveEase () const {
  if (BurdenState == OVER_LOADED || BurdenState == STRESSED) return 50;
  if (BurdenState == BURDENED) return 75;
  if (BurdenState == UNBURDENED) return 100;
  return 666;
}


//==========================================================================
//
//  character::GetLOSRange
//
//==========================================================================
int character::GetLOSRange () const {
  if (!game::IsInWilderness()) return GetAttribute(PERCEPTION)*GetLevel()->GetLOSModifier()/48;
  return 3;
}


//==========================================================================
//
//  character::Displace
//
//==========================================================================
truth character::Displace (character *Who, truth Forced) {
  ClearComparisonInfo();
  if (Who) Who->ClearComparisonInfo();

  if (GetBurdenState() == OVER_LOADED) {
    if (IsPlayer()) {
      cchar *CrawlVerb = StateIsActivated(LEVITATION) ? "float" : "crawl";
      ADD_MESSAGE("You try very hard to %s forward. But your load is too heavy.", CrawlVerb);
      EditAP(-1000);
      return true;
    }
    return false;
  }

  double Danger = GetRelativeDanger(Who);
  int PriorityDifference = Limit(GetDisplacePriority()-Who->GetDisplacePriority(), -31, 31);

  if (IsPlayer()) ++PriorityDifference;
  else if (Who->IsPlayer()) --PriorityDifference;

  if (PriorityDifference >= 0) Danger *= 1 << PriorityDifference;
  else Danger /= 1 << -PriorityDifference;

  if (IsSmall() && Who->IsSmall() &&
      (Forced || Danger > 1.0 || !(Who->IsPlayer() || Who->IsBadPath(GetPos()))) &&
      !IsStuck() && !Who->IsStuck() && (!Who->GetAction() || Who->GetAction()->TryDisplace()) &&
      CanMove() && Who->CanMove() && Who->CanMoveOn(GetLSquareUnder()))
  {
         if (IsPlayer()) ADD_MESSAGE("You displace %s!", Who->CHAR_DESCRIPTION(DEFINITE));
    else if (Who->IsPlayer()) ADD_MESSAGE("%s displaces you!", CHAR_DESCRIPTION(DEFINITE));
    else if (CanBeSeenByPlayer() || Who->CanBeSeenByPlayer()) ADD_MESSAGE("%s displaces %s!", CHAR_DESCRIPTION(DEFINITE), Who->CHAR_DESCRIPTION(DEFINITE));
    lsquare *OldSquareUnder1[MAX_SQUARES_UNDER];
    lsquare *OldSquareUnder2[MAX_SQUARES_UNDER];
    for (int c = 0; c < GetSquaresUnder(); ++c) OldSquareUnder1[c] = GetLSquareUnder(c);
    for (int c = 0; c < Who->GetSquaresUnder(); ++c) OldSquareUnder2[c] = Who->GetLSquareUnder(c);
    v2 Pos = GetPos();
    v2 WhoPos = Who->GetPos();
    Remove();
    Who->Remove();
    PutTo(WhoPos);
    Who->PutTo(Pos);
    EditAP(-GetMoveAPRequirement(GetSquareUnder()->GetEntryDifficulty()) - 500);
    EditNP(-12*GetSquareUnder()->GetEntryDifficulty());
    EditExperience(AGILITY, 75, GetSquareUnder()->GetEntryDifficulty() << 7);
    if (IsPlayer()) ShowNewPosInfo();
    if (Who->IsPlayer()) Who->ShowNewPosInfo();
    SignalStepFrom(OldSquareUnder1);
    Who->SignalStepFrom(OldSquareUnder2);
    return true;
  } else {
    if (IsPlayer()) {
      ADD_MESSAGE("%s resists!", Who->CHAR_DESCRIPTION(DEFINITE));
      EditAP(-1000);
      return true;
    }
    return false;
  }
}


//==========================================================================
//
//  character::SetNP
//
//==========================================================================
void character::SetNP (sLong What) {
  int OldState = GetHungerState();
  NP = What;
  if (IsPlayer()) {
    int NewState = GetHungerState();
    if (NewState == STARVING && OldState > STARVING) DeActivateVoluntaryAction(CONST_S("You are getting really hungry."));
    else if (NewState == VERY_HUNGRY && OldState > VERY_HUNGRY) DeActivateVoluntaryAction(CONST_S("You are getting very hungry."));
    else if (NewState == HUNGRY && OldState > HUNGRY) DeActivateVoluntaryAction(CONST_S("You are getting hungry."));
  }
}


//==========================================================================
//
//  GetWieldIndexForItem
//
//==========================================================================
static bool GetWieldIndexForItem (ccharacter *Char, const item *Item, int *wld0p, int *wld1p) {
  int wld0 = -1, wld1 = -1;
       if (Item->IsHelmet(Char)) { wld0 = HELMET_INDEX; }
  else if (Item->IsAmulet(Char)) { wld0 = AMULET_INDEX; }
  else if (Item->IsCloak(Char)) { wld0 = CLOAK_INDEX; }
  else if (Item->IsBodyArmor(Char)) { wld0 = BODY_ARMOR_INDEX; }
  else if (Item->IsBelt(Char)) { wld0 = BELT_INDEX; }
  else if (Item->IsRing(Char)) { wld0 = RIGHT_RING_INDEX; wld1 = LEFT_RING_INDEX; }
  else if (Item->IsGauntlet(Char)) { wld0 = RIGHT_GAUNTLET_INDEX; wld1 = LEFT_GAUNTLET_INDEX; }
  else if (Item->IsBoot(Char)) { wld0 = RIGHT_BOOT_INDEX; wld1 = LEFT_BOOT_INDEX; }
  else if (Item->IsShield(Char)) { wld0 = RIGHT_WIELDED_INDEX; wld1 = LEFT_WIELDED_INDEX; }
  else if (Item->IsWeapon(Char)) { wld0 = RIGHT_WIELDED_INDEX; wld1 = LEFT_WIELDED_INDEX; }
  //else if (Item->IsArmor(this)) { wld0 = RIGHT_WIELDED_INDEX; wld1 = LEFT_WIELDED_INDEX; }
  else {
    if (wld0p) *wld0p = -1;
    if (wld1p) *wld1p = -1;
    return false;
  }
  if (wld0p) *wld0p = wld0;
  if (wld1p) *wld1p = wld1;
  return true;
}


//==========================================================================
//
//  IsSameItemClass
//
//==========================================================================
static OK_UNUSED bool IsSameItemClass (const character *Char,
                                       const item *Item0, const item *Item1)
{
  if (!Char || !Item0 || !Item1) return false;
  if (Item0 == Item1) return true;

  if (Item0->IsHelmet(Char)) return Item1->IsHelmet(Char);
  if (Item0->IsAmulet(Char)) return Item1->IsAmulet(Char);
  if (Item0->IsCloak(Char)) return Item1->IsCloak(Char);
  if (Item0->IsBodyArmor(Char)) return Item1->IsBodyArmor(Char);
  if (Item0->IsBelt(Char)) return Item1->IsBelt(Char);
  if (Item0->IsRing(Char)) return Item1->IsRing(Char);
  if (Item0->IsGauntlet(Char)) return Item1->IsGauntlet(Char);
  if (Item0->IsBoot(Char)) return Item1->IsBoot(Char);
  if (Item0->IsShield(Char)) return Item1->IsShield(Char);
  if (Item0->IsWeapon(Char)) return Item1->IsWeapon(Char);
  //if (Item0->IsArmor(Char)) return Item1->IsArmor(Char);

  return false;
}


//==========================================================================
//
//  character::ItemComparisonWieldIdx
//
//==========================================================================
int character::ItemComparisonWieldIdx (const item *Item, int *countp) {
  if (countp) *countp = 0;
  if (!Item || Item->IsASkull()) return -1;

  int slot = -1, slot1 = -1;
  int count = 1;

  if (!GetWieldIndexForItem(this, Item, &slot, &slot1)) return -1;
  if (slot1 != -1) count = 2;

  for (int c = slot; c < slot + count; c += 1) {
    if (!GetBodyPartOfEquipment(c)) continue;
    //item *Equipment = GetEquipment(c);
    //if (!Equipment) continue;
    //if (!IsSameItemClass(this, Item, Equipment)) continue;
    if (countp) *countp = count;
    return slot;
  }

  return -1;
}


//==========================================================================
//
//  character::CreateItemComparisonInfo
//
//==========================================================================
void character::CreateItemComparisonInfo (const item *Item, int pileCount, bool &renderEqu) {
  if (!Item) return;

  int slot = -1;
  int count = 1;

  slot = ItemComparisonWieldIdx(Item, &count);
  if (slot < 0) return;

  bool needShow = false;
  bool equRendered = false;

  struct IInfo {
    festring dsc;
    int amount;
    item *Item;
    int weight;

    IInfo (cfestring &adesc, item *it)
      : dsc(adesc)
      , amount(1)
      , Item(it)
      , weight(it->GetWeight())
    {}
  };
  std::vector<IInfo> elist;

  for (int c = slot; c < slot + count; c += 1) {
    if (!GetBodyPartOfEquipment(c)) continue;
    //item *Equipment = GetEquipment(c);
    //if (!Equipment) continue;
    //if (!IsSameItemClass(this, Item, Equipment)) continue;
    // equipped item
    if (!needShow) {
      needShow = true;
      if (mComparisonInfo.size() > 16) {
        if (mComparisonInfo[mComparisonInfo.size() - 1] != "\1#888|...more...") {
          mComparisonInfo.push_back(CONST_S("\1#888|...more..."));
        }
        return;
      }
      // insert delimiter
      if (renderEqu && !equRendered && !mComparisonInfo.empty()) {
        // special string
        mComparisonInfo.push_back(CONST_S("--- --- ---"));
      }
    }
    if (renderEqu) {
      item *Equipment = GetEquipment(c);
      if (Equipment) {
        festring eqs;
        Equipment->AddName(eqs, INDEFINE_BIT | STRIP_STATES | NO_FLUIDS);
        Equipment->AddSpecialInfo(nullptr, eqs, 1, false);
        if (!elist.empty() && elist[elist.size() - 1].dsc == eqs) {
          elist[elist.size() - 1].amount += 1;
          elist[elist.size() - 1].weight += Equipment->GetWeight();
        } else {
          elist.push_back(IInfo(eqs, Equipment));
        }
        equRendered = true;
      }
    }
  }

  if (needShow) {
    if (renderEqu && !equRendered) {
      // nothing wielded
      IvanAssert(elist.empty());
      mComparisonInfo.push_back(CONST_S("\1#fff|nothing"));
    } else {
      for (size_t f = 0; f != elist.size(); f += 1) {
        elist[f].dsc = CONST_S("\1#fff!");
        if (elist[f].amount == 1) {
          elist[f].Item->AddName(elist[f].dsc, INDEFINE_BIT | STRIP_STATES | NO_FLUIDS);
        } else {
          elist[f].dsc << elist[f].amount << " ";
          elist[f].Item->AddName(elist[f].dsc, PLURAL | STRIP_STATES | NO_FLUIDS);
        }
        elist[f].dsc << "\1#b7b7b7!";
        elist[f].Item->AddSpecialInfo(nullptr, elist[f].dsc, elist[f].amount, false);
        elist[f].dsc << " [";
        elist[f].dsc.PutWeight(elist[f].weight, "\1C", "\2");
        elist[f].dsc << "]";
        FONT->WordWrap(elist[f].dsc, mComparisonInfo, (game::GetScreenXSize() - 1) * 16, 4);
      }
    }

    {
      festring nis = CONST_S("\1#e70!") + NBSP;
      if (pileCount == 1) {
        Item->AddName(nis, INDEFINE_BIT | STRIP_STATES | NO_FLUIDS);
      } else {
        nis << pileCount << " ";
        Item->AddName(nis, PLURAL | STRIP_STATES | NO_FLUIDS);
      }
      nis << "\1#b60!";
      Item->AddSpecialInfo(nullptr, nis, pileCount, false);
      nis << " [";
      nis.PutWeight(Item->GetWeight() * pileCount, "\1C", "\2");
      nis << "]";
      FONT->WordWrap(nis, mComparisonInfo, (game::GetScreenXSize() - 1) * 16, 4);
    }
    renderEqu = false;
  }
}


//==========================================================================
//
//  IsUninterestingItemForInfo
//
//==========================================================================
static inline bool IsUninterestingItemForInfo (const item *Item) {
  if (/*Item->IsCorpse() || Item->IsBodyPart() ||*/ Item->IsLanternOnWall() ||
      Item->IsABone() || Item->IsASkull() || Item->IsBananaPeel())
  {
    return true;
  }

  if (Item->IsFish()) {
    return (Item->GetConfig() == BONE_FISH);
  }

  if (Item->IsBottle()) {
    return Item->IsBroken();
  }

  return false;
}


//==========================================================================
//
//  character::CreateOtherItemInfo
//
//==========================================================================
void character::CreateOtherItemInfo (const item *Item, int pileCount, bool &renderMore) {
  if (!Item || !renderMore) return;
  if (!Item->IsQuestItem() && IsUninterestingItemForInfo(Item)) return;

  if (mOtherItemInfo.size() < 12) {
    festring ss = CONST_S("\1#e70!");
    if (pileCount == 1) {
      Item->AddName(ss, INDEFINE_BIT | STRIP_STATES | NO_FLUIDS);
    } else {
      ss << pileCount << " ";
      Item->AddName(ss, PLURAL | STRIP_STATES | NO_FLUIDS);
    }
    ss << "\1#b60!";
    Item->AddSpecialInfo(nullptr, ss, pileCount, false);
    ss << " [";
    ss.PutWeight(Item->GetWeight() * pileCount, "\1C", "\2");
    ss << "]";
    FONT->WordWrap(ss, mOtherItemInfo, (game::GetScreenXSize() - 1) * 16, 4);
  } else {
    renderMore = false;
    mOtherItemInfo.push_back(CONST_S("\1#888|...more..."));
  }
}


//==========================================================================
//
//  character::ClearComparisonInfo
//
//==========================================================================
void character::ClearComparisonInfo () {
  if (HasComparisonInfo()) {
    game::GetCurrentArea()->SendNewDrawRequest();
    //game::DrawEverything();
  }
  mComparisonInfo.clear();
  mOtherItemInfo.clear();
}


//==========================================================================
//
//  character::UpdateComparisonInfo
//
//==========================================================================
void character::UpdateComparisonInfo () {
  ClearComparisonInfo();

  if (!ivanconfig::GetShowItemComparison() && !ivanconfig::GetShowOtherItemPopups()) {
    return;
  }

  if (IsPlayer() && IsEnabled() && !game::IsInWilderness()) {
    if (!GetLSquareUnder()->IsTransparent()) {
      return;
    }
    if (GetLSquareUnder()->IsDark() && !game::GetSeeWholeMapCheatMode()) {
      return;
    }

    itemvectorvector PileVector;
    GetStackUnder()->Pile(PileVector, this, CENTER, 0, true);

    if (PileVector.size()) {
      // create item comparisons last
      if (ivanconfig::GetShowItemComparison()) {
        for (int eqslot = 0; eqslot <= 12; eqslot += 1) {
          bool renderSlot = true;
          for (uInt c = 0; c != PileVector.size(); c += 1) {
            item *it = PileVector[c][0];
            int slot, count;
            slot = ItemComparisonWieldIdx(it, &count);
            if (slot == eqslot) {
              CreateItemComparisonInfo(it, (int)PileVector[c].size(), renderSlot);
            }
          }
        }
      }

      if (ivanconfig::GetShowOtherItemPopups()) {
        bool renderMore = true;
        for (uInt c = 0; renderMore && c != PileVector.size(); c += 1) {
          bool other = true;
          item *it = PileVector[c][0];
          for (int eqslot = 0; other && eqslot <= 12; eqslot += 1) {
            int slot, count;
            slot = ItemComparisonWieldIdx(it, &count);
            if (slot >= 0) other = false;
          }
          if (other) {
            CreateOtherItemInfo(it, (int)PileVector[c].size(), renderMore);
          }
        }
      }

      if (HasComparisonInfo()) {
        game::GetCurrentArea()->SendNewDrawRequest();
      }
    }
  }
}


//==========================================================================
//
//  character::ShowNewPosInfo
//
//==========================================================================
void character::ShowNewPosInfo () {
  UpdateComparisonInfo();
  v2 Pos = GetPos();

  if (ivanconfig::GetAutoCenterMap()) {
    game::UpdateCameraX();
    game::UpdateCameraY();
  } else {
    if (Pos.X < game::GetCamera().X+3 || Pos.X >= game::GetCamera().X+game::GetScreenXSize()-3) game::UpdateCameraX();
    if (Pos.Y < game::GetCamera().Y+3 || Pos.Y >= game::GetCamera().Y+game::GetScreenYSize()-3) game::UpdateCameraY();
  }

  game::SendLOSUpdateRequest();
  game::DrawEverythingNoBlit();
  UpdateESPLOS();

  if (!game::IsInWilderness()) {
    msgsystem::EnterBigMessageMode();

    if (GetLSquareUnder()->IsDark() && !game::GetSeeWholeMapCheatMode()) {
      ADD_MESSAGE("It's dark in here!");
    }

    GetLSquareUnder()->ShowSmokeMessage();
    itemvectorvector PileVector;
    #ifdef CC_SHOW_FULL_POS_DESC
    GetStackUnder()->SetScrollInspected();
    #endif
    GetStackUnder()->Pile(PileVector, this, CENTER, 0, true);
    truth Feel = true; // for now

    if (PileVector.size()) {
      Feel = (!GetLSquareUnder()->IsTransparent() || GetLSquareUnder()->IsDark());
      if (PileVector.size() == 1) {
        if (Feel) {
          #ifndef CC_SHOW_FULL_POS_DESC
          if (PileVector[0][0]->IsGenericScrollDescr()) {
            if (PileVector[0].size() == 1) {
              ADD_MESSAGE("You feel a scroll is lying here.");
            } else {
              ADD_MESSAGE("You feel several scrolls are lying here.");
            }
          } else
          #endif
          {
            ADD_MESSAGE("You feel %s lying here.",
                        PileVector[0][0]->GetName(INDEFINITE, PileVector[0].size()).CStr());
          }
        } else {
          #ifndef CC_SHOW_FULL_POS_DESC
          if (PileVector[0][0]->IsGenericScrollDescr()) {
            if (PileVector[0].size() == 1) {
              ADD_MESSAGE("You feel a scroll is lying here.");
            } else {
              ADD_MESSAGE("You feel several scrolls are lying here.");
            }
          } else
          #endif
          {
            auto ffs = PileVector[0][0]->TempDisableFluids();
            if (ivanconfig::GetShowFullItemDesc() && PileVector[0][0]->AllowDetailedDescription()) {
              festring text;
              // `nullptr` as viewer removes skill info
              PileVector[0][0]->PosDescMethod(/*PLAYER*/nullptr, text, PileVector[0].size(), true);
              //ConLogf("invdsc     : [%s]", text.CStr());
              ADD_MESSAGE("%s %s lying here.", text.CStr(), PileVector[0].size() == 1 ? "is" : "are");
            } else {
              ADD_MESSAGE("%s %s lying here.",
                          PileVector[0][0]->GetName(INDEFINITE, PileVector[0].size()).CStr(),
                                                    PileVector[0].size() == 1 ? "is" : "are");
            }
          }
          #if 0
          if (game::WizardModeIsActive()) {
            //ConLogf("description: [%s]", PileVector[0][0]->GetDescription(INDEFINITE).CStr());
            ADD_MESSAGE("\nstrength: [%s]\n", PileVector[0][0]->GetStrengthValueDescription());
            ADD_MESSAGE("basetohit: [%s]\n", PileVector[0][0]->GetBaseToHitValueDescription());
            ADD_MESSAGE("baseblock: [%s]\n", PileVector[0][0]->GetBaseBlockValueDescription());
            ADD_MESSAGE("extdsc: [%s]\n", PileVector[0][0]->GetExtendedDescription().CStr());
          }
          #endif
        }
      } else {
        int Items = 0;
        for (uInt c = 0; c < PileVector.size(); ++c) {
          if ((Items += PileVector[c].size()) > 3) break;
        }
        if (Items > 3) {
          if (Feel) {
            ADD_MESSAGE("You feel several items lying here.");
          } else {
            ADD_MESSAGE("Several items are lying here.");
          }
        } else if (Items) {
          if (Feel) {
            ADD_MESSAGE("You feel a few items lying here.");
          } else {
            ADD_MESSAGE("A few items are lying here.");
          }
        }
      }
    }

    festring SideItems;
    GetLSquareUnder()->GetSideItemDescription(SideItems);

    if (!SideItems.IsEmpty()) ADD_MESSAGE("There is %s.", SideItems.CStr());

    if (GetLSquareUnder()->HasEngravings()) {
      if (CanRead()) {
        ADD_MESSAGE("Something has been engraved here: \"%s\"", GetLSquareUnder()->GetEngraved());
      } else {
        ADD_MESSAGE("Something has been engraved here.");
      }
    }

    msgsystem::LeaveBigMessageMode();
  }
}


//==========================================================================
//
//  character::Hostility
//
//==========================================================================
void character::Hostility (character *Enemy) {
  if (Enemy == this || !Enemy || !Team || !Enemy->Team) return;
  if (Enemy->IsMasochist() && GetRelation(Enemy) == FRIEND) return;
  if (!IsAlly(Enemy)) {
    //k8: HACK! petruss wife #3 and petruss wife #7 will side with the Attnam attacker.
    // see their chat text to know why.
    // if the player attacks Petrus...
    if (IsPlayer() && Enemy->IsPetrus() && GetRelation(Enemy) != HOSTILE) {
      // ...two warrior princesses should join the player
      team *att = Enemy->GetTeam(); //game::GetTeam(ATTNAM_TEAM);
      IvanAssert(att);
      for (auto &&it : att->GetMember()) {
        if (it->IsPetrussWife() && it->GetRelation(PLAYER) != HOSTILE) {
          if (it->GetConfig() == 3 || it->GetConfig() == 7) {
            // move her to the player team
            it->ChangeTeam(GetTeam());
          }
        }
      }
    }
    // normal hostility
    GetTeam()->Hostility(Enemy->GetTeam());
  } else if (IsPlayer() && !Enemy->IsPlayer()) {
    // I believe both may be players due to polymorph feature...
    if (Enemy->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s becomes enraged.", Enemy->CHAR_NAME(DEFINITE));
    }
    Enemy->ChangeTeam(game::GetTeam(BETRAYED_TEAM));
  }
}


//==========================================================================
//
//  character::GetGiftStack
//
//==========================================================================
stack *character::GetGiftStack () const {
  if (GetLSquareUnder()->GetRoomIndex() && !GetLSquareUnder()->GetRoom()->AllowDropGifts()) {
    return GetStack();
  }
  return GetStackUnder();
}


//==========================================================================
//
//  character::MoveRandomlyInRoom
//
//  ignore interesting items
//
//==========================================================================
truth character::MoveRandomlyInRoom (truth allowInterestingItems) {
  if (allowInterestingItems && MoveToInterestingItem(true)) return true;

  for (int c = 0; c < 10; c += 1) {
    v2 ToTry = game::GetMoveVector(RAND_8);
    if (GetLevel()->IsValidPos(GetPos() + ToTry)) {
      lsquare *Square = GetNearLSquare(GetPos() + ToTry);
      if (!Square->IsDangerous(this) && !Square->IsScary(this) &&
          (!Square->GetOLTerrain() || !Square->GetOLTerrain()->IsDoor()) &&
          TryMove(ToTry, false, false))
      {
        return true;
      }
    }
  }
  return false;
}


//#define dirlogf(...)  do { if (DEBUG_GO) ConLogf(__VA_ARGS__); } while (0)
#define dirlogf(...)  (void)0


static const int revDir[MDIR_STAND] = { MDIR_DOWN_RIGHT, MDIR_DOWN, MDIR_DOWN_LEFT, MDIR_RIGHT, MDIR_LEFT, MDIR_UP_RIGHT, MDIR_UP, MDIR_UP_LEFT };
static const bool orthoDir[MDIR_STAND] = { false, true, false, true, true, false, true, false };


//==========================================================================
//
//  IsDirExcluded
//
//  only for ortho moveDir
//
//==========================================================================
static inline truth IsDirExcluded (int moveDir, int dir) {
  if (moveDir == dir) return true;
  switch (moveDir) {
    case MDIR_UP: return (dir == MDIR_UP_LEFT || dir == MDIR_UP_RIGHT);
    case MDIR_LEFT: return (dir == MDIR_UP_LEFT || dir == MDIR_DOWN_LEFT);
    case MDIR_RIGHT: return (dir == MDIR_UP_RIGHT || dir == MDIR_DOWN_RIGHT);
    case MDIR_DOWN: return (dir == MDIR_DOWN_LEFT || dir == MDIR_DOWN_RIGHT);
  }
  return false;
}


//==========================================================================
//
//  character::IsPassableSquare
//
//==========================================================================
truth character::IsPassableSquare (int x, int y) const {
  if (x >= 0 && y >= 0) {
    area *ca = GetSquareUnder()->GetArea();
    lsquare *sq;
    if (x >= ca->GetXSize() || y >= ca->GetYSize()) return false;
    sq = static_cast<lsquare *>(ca->GetSquare(x, y));
    return sq && CanMoveOn(sq);
  }
  return false;
}


//==========================================================================
//
//  character::CountPossibleMoveDirs
//
//==========================================================================
void character::CountPossibleMoveDirs (cv2 pos, int *odirs, int *ndirs, int exclideDir) const {
  if (odirs) *odirs = 0;
  if (ndirs) *ndirs = 0;
  for (int f = 0; f != MDIR_STAND; ++f) {
    if (!IsDirExcluded(exclideDir, f)) {
      if (IsPassableSquare(pos + game::GetMoveVector(f))) {
        if (orthoDir[f]) {
          if (odirs) ++(*odirs);
        } else {
          if (ndirs) ++(*ndirs);
        }
      }
    }
  }
}


//==========================================================================
//
//  character::IsInCorridor
//
//  in corridor (for orto-dirs):
//  count dirs excluding ortho-dir we going:
//  if there is one or less ortho-dirs and one or less non-ortho-dirs, we are in corridor
//
//  only for ortho-dirs
//
//==========================================================================
truth character::IsInCorridor (int x, int y, int moveDir) const {
  int od = 0, nd = 0;
  dirlogf("IsInCorridor(%d,%d,%d)\n", x, y, moveDir);
  // reverse moveDir
  moveDir = (moveDir >= 0 && moveDir < MDIR_STAND ? revDir[moveDir] : -1);
  dirlogf(" reversedDir: %d\n", moveDir);
  CountPossibleMoveDirs(v2(x, y), &od, &nd, moveDir);
  dirlogf(" possibleDirs: (%d:%d)\n", od, nd);
  dirlogf(" IsInCorridor: %s\n", ((od <= 1 && nd <= 1) ? "yes" : "no"));
  return (od <= 1 && nd <= 1);
}


//==========================================================================
//
//  character::GetDiagonalForDirs
//
//==========================================================================
cv2 character::GetDiagonalForDirs (int moveDir, int newDir) const {
  switch (moveDir) {
    case MDIR_UP:
      switch (newDir) {
        case MDIR_LEFT: return game::GetMoveVector(MDIR_UP_LEFT);
        case MDIR_RIGHT: return game::GetMoveVector(MDIR_UP_RIGHT);
      }
      break;
    case MDIR_DOWN:
      switch (newDir) {
        case MDIR_LEFT: return game::GetMoveVector(MDIR_DOWN_LEFT);
        case MDIR_RIGHT: return game::GetMoveVector(MDIR_DOWN_RIGHT);
      }
      break;
    case MDIR_LEFT:
      switch (newDir) {
        case MDIR_UP: return game::GetMoveVector(MDIR_UP_LEFT);
        case MDIR_DOWN: return game::GetMoveVector(MDIR_DOWN_LEFT);
      }
      break;
    case MDIR_RIGHT:
      switch (newDir) {
        case MDIR_UP: return game::GetMoveVector(MDIR_UP_RIGHT);
        case MDIR_DOWN: return game::GetMoveVector(MDIR_DOWN_RIGHT);
      }
      break;
  }
  ABORT("wtf in character::GetDiagonalForDirs()");
}


//==========================================================================
//
//  character::CheckWindowInDir
//
//  check if there is anybody there
//
//==========================================================================
character *character::CheckWindowInDir (int dir) const {
  if (dir < 0 || dir >= 8) return 0;

  v2 pos = GetPos() + game::GetMoveVector(dir);
  if (!GetLevel()->IsValidPos(pos)) return 0;

  lsquare *lsq = GetNaturalNeighbourLSquare(dir);
  if (!lsq) return 0;
  if (!lsq->IsTransparent()) return 0;

  // if it is a window, check if there is somebody over there
  /*
  olterrain *terra = lsq->GetOLTerrain();
  if (!terra) return 0;
  if (!(terra->GetConfig() & WINDOW) && !terra->IsBarWall()) return 0;
  //if (terra->CanBeOpened()) return 0;
  */

  pos += game::GetMoveVector(dir);
  if (!GetLevel()->IsValidPos(pos)) return 0;
  lsq = GetLevel()->GetLSquare(pos);
  if (!lsq) return 0;

  return lsq->GetCharacter();
}


//==========================================================================
//
//  character::CanSeeAnyNonFriendlyMonsters
//
//  FIXME: this prolly could be done better!
//  FIXME: disallow resting in darkness
//
//==========================================================================
truth character::CanSeeAnyNonFriendlyMonsters () const {
  int LOSRange = GetLOSRange();
  if (LOSRange < 2) LOSRange = 2;
  for (int dx = -LOSRange; dx <= LOSRange; dx += 1) {
    for (int dy = -LOSRange; dy <= LOSRange; dy += 1) {
      v2 pos = GetPos() + v2(dx, dy);
      if ((dx | dy) != 0 && GetLevel()->IsValidPos(pos) /*&& IsPassableSquare(pos)*/ &&
          SquareUnderCanBeSeenBy(this, false))
      {
        lsquare *MoveToSquare[MAX_SQUARES_UNDER];
        auto Squares = CalculateNewSquaresUnder(MoveToSquare, pos);
        for (decltype(Squares) c = 0; c < Squares; ++c) {
          lsquare *Square = MoveToSquare[c];
          #if 0
          if (Square->IsDangerous(this)) return true;
          #endif
          // check if someone is standing at the square
          character *cc = Square->GetCharacter();
          if (cc && GetTeam() != Square->GetCharacter()->GetTeam() &&
              cc->GetRelation(this) == HOSTILE)
          {
            if (cc->CanBeSeenBy(this)) {
              #if 0
              ConLogf("[HS: %s (me:%d,%d); (%d,%d)]",
                      cc->DebugLogName().CStr(),
                      GetPos().X, GetPos().Y, pos.X, pos.Y);
              #endif
              return true;
            }
          }
        }
      }
    }
  }
  return false;
}


//==========================================================================
//
//  character::IsInSafeRoom
//
//  it is small, all doors are closed, and there are no monsters inside.
//  yep, friendly monsters counts too.
//
//  FIXME: check memorized monsters on the squares we cannot see!
//
//==========================================================================
truth character::IsInSafeRoom () const {
  room *Room = GetRoom();
  if (!Room || Room->GetSize().X > 7 || Room->GetSize().Y > 7) {
    return false;
  }
  const uInt thisRoomIndex = Room->GetIndex();
  // floodfill the room, check if all doors are closed, and there are no monsters inside
  std::vector<v2> scanned; // don't bother with hashtable
  std::list<v2> queue;
  queue.push_back(GetPos());
  while (!queue.empty()) {
    v2 pos = queue.front();
    queue.pop_front();
    bool found = false;
    for (size_t vidx = 0; !found && vidx != scanned.size(); vidx += 1) {
      found = (scanned[vidx] == pos);
    }
    if (!found && GetLevel()->IsValidPos(pos)) {
      if (pos.X < Room->GetPos().X || pos.Y < Room->GetPos().Y ||
          pos.X >= Room->GetPos().X + Room->GetSize().X ||
          pos.Y >= Room->GetPos().Y + Room->GetSize().Y)
      {
        return false; // moved out of the room
      }
      scanned.push_back(pos);
      lsquare *lsq = GetLevel()->GetLSquare(pos);
      if (lsq) {
        // if we haven't seen the square, we cannot know if there is something there
        if (!lsq->HasBeenSeen()) return false;
        // early exit if the room is destroyed, or we leaked out of it
        if (lsq->GetRoomIndex() != thisRoomIndex) return false;
        bool addNg = true;
        olterrain *terra = lsq->GetOLTerrain();
        // this checks if there is no character there, or if the character is `this`
        if (!IsFreeForMe(lsq)) return false;
        if (terra) {
          // we will eventually leak over windows due to `IsTransparent()`
          //if (terra->GetConfig() & WINDOW) return false; // there should be no windows
          if (terra->CanBeClosed()) return false; // open door
          addNg = (lsq->IsTransparent() || CanMoveOn(lsq) || CanMoveOn(terra));
        }
        if (addNg) {
          for (int dy = -1; dy <= 1; dy += 1) {
            for (int dx = -1; dx <= 1; dx += 1) {
              if ((dx | dy) != 0) {
                queue.push_back(pos + v2(dx, dy));
              }
            }
          }
        }
      } else {
        return false;
      }
    }
  }
  return true;
}


//==========================================================================
//
//  character::CanRestHere
//
//  can rest in (but only if there are no hostile monsters):
//    dead ends
//    small rooms with closed doors
//
//==========================================================================
truth character::CanRestHere () const {
  int od, nd;
  v2 pos = GetPos();
  if (game::IsInWilderness()) return true; // why not?
  CountPossibleMoveDirs(pos, &od, &nd, -1);
  if (od <= 1 && nd == 0) return !CanSeeAnyNonFriendlyMonsters();
  // allow to rest in semi-dead ends, `[`-shaped
  if (od == 1) {
    int xdir = -1;
    for (int f = 0; xdir == -1 && f != MDIR_STAND; ++f) {
      if (orthoDir[f] && IsPassableSquare(pos + game::GetMoveVector(f))) {
        xdir = f;
      }
    }
    if (xdir != -1) {
      v2 diag;
      switch (xdir) {
        case MDIR_LEFT:
          if (!IsPassableSquare(pos + game::GetMoveVector(MDIR_UP_RIGHT)) &&
              !IsPassableSquare(pos + game::GetMoveVector(MDIR_DOWN_RIGHT)))
          {
            return !CanSeeAnyNonFriendlyMonsters();
          }
          break;
        case MDIR_RIGHT:
          if (!IsPassableSquare(pos + game::GetMoveVector(MDIR_UP_LEFT)) &&
              !IsPassableSquare(pos + game::GetMoveVector(MDIR_DOWN_LEFT)))
          {
            return !CanSeeAnyNonFriendlyMonsters();
          }
          break;
        case MDIR_UP:
          if (!IsPassableSquare(pos + game::GetMoveVector(MDIR_DOWN_LEFT)) &&
              !IsPassableSquare(pos + game::GetMoveVector(MDIR_DOWN_RIGHT)))
          {
            return !CanSeeAnyNonFriendlyMonsters();
          }
          break;
        case MDIR_DOWN:
          if (!IsPassableSquare(pos + game::GetMoveVector(MDIR_UP_LEFT)) &&
              !IsPassableSquare(pos + game::GetMoveVector(MDIR_UP_RIGHT)))
          {
            return !CanSeeAnyNonFriendlyMonsters();
          }
          break;
        default:
          break;
      }
    }
  /* corridors; nope, they are not safe
  } else if (od == 2 && nd == 0) {
    return !CanSeeAnyNonFriendlyMonsters();
  */
  } else if (IsInSafeRoom()) {
    // small rooms are safe to rest
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::CheckCorridorMove
//
//  try to walk in the given dir
//  can do two steps without a turn and still in corridor?
//  yes:
//   just go
//  no:
//   go in non-ortho dir, set prevdir to last ortho-dir from corridor tracing
//
//  only for ortho-dirs; assume that the char is in corridor
//
//==========================================================================
int character::CheckCorridorMove (v2 &moveVector, cv2 pos, int moveDir, truth *markAsTurn) const {
  moveVector = game::GetMoveVector(moveDir);
  v2 ps1(pos+moveVector);

  if (markAsTurn) *markAsTurn = true;

  if (IsPassableSquare(ps1)) {
    // we can do first step in the given dir
    // check if we will be in corridor after it
    dirlogf("CheckCorridorMove: can do first step\n");
    if (IsInCorridor(ps1, moveDir)) {
      // check second step
      v2 ps2(ps1+moveVector);
      dirlogf("CheckCorridorMove: still in corridor after the first step\n");
      if (IsPassableSquare(ps2)) {
        // can do second step
        dirlogf("CheckCorridorMove: can do second step\n");
        return moveDir;
      } else {
        // can't do second step; but we still in corridor, so we should make a turn
        int newDir = -1; // direction to turn
        for (int f = 0; f < MDIR_STAND; ++f) {
          if (f != moveDir && orthoDir[f] && f != revDir[moveDir] && IsPassableSquare(ps1+game::GetMoveVector(f))) {
            newDir = f;
            break;
          }
        }
        dirlogf("CheckCorridorMove: can't do second step; moveDir=%d; newDir=%d\n", moveDir, newDir);
        if (newDir < 0) {
          // dead end, will stop
          return moveDir;
        }
        // we should do diagonal move
        moveVector = GetDiagonalForDirs(moveDir, newDir);
        // if this is 'one-tile-turn', we should not change the direction to newDir
        if (IsPassableSquare(ps1+game::GetMoveVector(newDir)+game::GetMoveVector(moveDir))) {
          // yes, this is 'one-tile-turn'
          dirlogf("CheckCorridorMove: one-tile-turn, don't change dir\n");
          /* 'g'o bug:
           *
           * ####.######
           * ####*......
           * ..@..######
           * ######
           *
           * 'g'o right: should stop at '*', but it just goes right
           */
          if (markAsTurn) *markAsTurn = IsInCorridor(ps1+game::GetMoveVector(newDir), newDir);
          newDir = moveDir;
        }
        return newDir;
      }
    }
    dirlogf("CheckCorridorMove: can do one or two steps; move forward\n");
    // can do one or two steps: check for T-junction
    // we should stop if we have more than two open dirs, or one of open dirs is not moveDir
    int dcount = 0;
    for (int f = 0; f < MDIR_STAND; ++f) {
      if (f == revDir[moveDir]) continue; // skip "reverse dir" check
      v2 ps2(pos+game::GetMoveVector(f));
      if (IsPassableSquare(ps2)) {
        ++dcount;
        if (dcount > 2) return -1; // more than two open dirs, stop
        if (f != moveDir) return -1; // one of open dirs is not moveDir
      }
    }
    // just move forward
    return moveDir;
  }
  dirlogf("CheckCorridorMove: dead end\n");
  // can't go, assume invalid direction
  return -1;
}


//==========================================================================
//
//  character::IsDangerousSquareChars
//
//==========================================================================
truth character::IsDangerousSquareChars (v2 pos) const {
  if (!IsPassableSquare(pos)) return false;
  lsquare *MoveToSquare[MAX_SQUARES_UNDER];
  auto Squares = CalculateNewSquaresUnder(MoveToSquare, pos);
  for (decltype(Squares) c = 0; c < Squares; ++c) {
    lsquare *Square = MoveToSquare[c];
    if (IsPlayer()) {
      if (!Square->HasBeenSeen()) continue;
    } else {
      if (!Square->CanBeSeenBy(this)) continue;
    }
    // check if someone is standing at the square
    if (Square->GetCharacter() && GetTeam() != Square->GetCharacter()->GetTeam() &&
        Square->GetCharacter()->CanBeSeenBy(this))
    {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::IsDangerousSquareOther
//
//==========================================================================
truth character::IsDangerousSquareOther (v2 pos) const {
  if (!IsPassableSquare(pos)) return false;
  lsquare *MoveToSquare[MAX_SQUARES_UNDER];
  auto Squares = CalculateNewSquaresUnder(MoveToSquare, pos);
  for (decltype(Squares) c = 0; c < Squares; ++c) {
    lsquare *Square = MoveToSquare[c];
    if (IsPlayer()) {
      if (!Square->HasBeenSeen()) continue;
    } else {
      if (!Square->CanBeSeenBy(this)) continue;
    }
    if (Square->IsDangerous(this)) {
      if (IsPlayer() && Square->HasBeenSeen()) return true;
      if (Square->CanBeSeenBy(this)) return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::MarkAdjacentItemsAsSeen
//
//==========================================================================
void character::MarkAdjacentItemsAsSeen (v2 pos) {
  lsquare *sqlist[MAX_SQUARES_UNDER];
  for (int d = 0; d < MDIR_STAND; ++d) {
    auto np = pos+game::GetMoveVector(d);
    if (!IsPassableSquare(np)) continue;
    auto sqcount = CalculateNewSquaresUnder(sqlist, np);
    for (int n = 0; n < sqcount; ++n) {
      lsquare *sq = sqlist[n];
      if ((IsPlayer() && sq->HasBeenSeen()) || sq->CanBeSeenBy(this)) {
        sq->GetStack()->SetSteppedOn(true);
      }
    }
  }
}


//==========================================================================
//
//  character::IsFunnyItem
//
//  used to determine if the item is "fun enough" to stop "G"oing.
//
//==========================================================================
truth character::IsFunnyItem (item *Item) const {
  if (!Item->CanBePickedUp()) return false;
  if (!ivanconfig::GetStopOnSeenItems() && Item->IsSteppedOn()) return false;
  if (Item->IsCorpse() || Item->IsBodyPart()) return ivanconfig::GetStopOnCorpses();
  if (Item->IsABone() || Item->IsASkull()) return false;
  if (Item->IsLanternOnWall()) return false;
  if (strcmp(Item->GetClassID(), "fish") == 0) {
    return (Item->GetConfig() != BONE_FISH);
  }
  return true;
}


//==========================================================================
//
//  character::HasInterestingItemsAt
//
//==========================================================================
truth character::HasInterestingItemsAt (v2 pos) {
  if (!IsPassableSquare(pos)) return false;
  lsquare *MoveToSquare[MAX_SQUARES_UNDER];
  int sq2 = CalculateNewSquaresUnder(MoveToSquare, pos);
  for (int c = 0; c < sq2; ++c) {
    lsquare *sq = MoveToSquare[c];
    if (IsPlayer()) {
      if (!sq->HasBeenSeen()) continue;
    } else {
      if (!sq->CanBeSeenBy(this)) continue;
    }
    if (sq->GetStack()->HasSomethingFunnyFor(this)) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::GoOn
//
//==========================================================================
void character::GoOn (go *Go, truth FirstStep) {
  const bool debugGo = (DEBUG_GO != 0);
  if (debugGo) {
    ConLogf("=== character::GoOn; dir=%d; pos=(%d,%d) ===", Go->GetDirection(), GetPos().X, GetPos().Y);
  }

  if (FirstStep) {
    if (debugGo) ConLogf("GO: FirstStep");
    mPrevMoveDir = Go->GetDirection();
    // this should also be set *after* making a move (and it is)
    Go->SetIsWalkingInOpen(!IsInCorridor(Go->GetDirection()));
  }

  v2 MoveVector = ApplyStateModification(game::GetMoveVector(Go->GetDirection()));
  lsquare *MoveToSquare[MAX_SQUARES_UNDER];
  int Squares = CalculateNewSquaresUnder(MoveToSquare, GetPos()+MoveVector);
  int moveDir = game::MoveVectorToDirection(MoveVector);

  if (Squares == 0 || !CanMoveOn(MoveToSquare[0])) {
    if (debugGo) ConLogf("GO: just can't move");
    Go->Terminate(false);
    return;
  }

  if (FirstStep) {
    // first step: mark all adjacent items as seen
    MarkAdjacentItemsAsSeen(GetPos());
  } else {
    // not a first step

    // check for corridor<->open place
    if (!Go->GetPrevWasTurn() && Go->IsWalkingInOpen() != !IsInCorridor(GetPos(), moveDir)) {
      if (debugGo) ConLogf("..moved to/from open place");
      Go->Terminate(false);
      return;
    }

    // check for room change
    uInt OldRoomIndex = GetLSquareUnder()->GetRoomIndex();
    uInt CurrentRoomIndex = MoveToSquare[0]->GetRoomIndex();
    if (OldRoomIndex && (CurrentRoomIndex != OldRoomIndex)) {
      // room is about to be changed, stop here
      if (debugGo) ConLogf("..room about to be changed");
      Go->Terminate(false);
      return;
    }

    // stop near a dangerous square
    if (IsDangerousSquareChars(GetPos() + MoveVector) ||
        IsDangerousSquareOther(GetPos() + MoveVector))
    {
      if (debugGo) ConLogf("..sense a danger (fwd)");
      Go->Terminate(false);
      return;
    }
  }

  // if the state modified the direction, move and stop
  if (moveDir != Go->GetDirection()) {
    if (debugGo) ConLogf("..move affected by state");
    if (TryMove(MoveVector, true, game::PlayerIsRunning())) {
      game::DrawEverything();
      if (ivanconfig::GetGoingDelay()) {
        DELAY(ivanconfig::GetGoingDelay());
      }
    }
    Go->Terminate(false);
    return;
  }

  truth doStop = false, markAsTurn = false;
  if (!FirstStep) {
    // continuous walking
    if (Go->IsWalkingInOpen() || !orthoDir[moveDir]) {
      // walking in open space or diagonal walking
      v2 newPos(GetPos() + MoveVector);
      int ood, ond, nod, nnd;
      /*
       * open: stop if # of possible dirs in next step != # of possible dirs in current step
       *       (or next step is in corridor)
       */
      if (IsInCorridor(newPos, moveDir)) {
        // trying to enter the corridor, stop right here
        if (debugGo) {
          ConLogf("..entering the corridor (open walking)");
        }
        Go->Terminate(false);
        return;
      }
      if (debugGo) ConLogf("...open walking");
      CountPossibleMoveDirs(GetPos(), &ood, &ond);
      CountPossibleMoveDirs(newPos, &nod, &nnd);
      if (ood != nod || ond != nnd) {
        // # of directions to walk to changed, stop right here
        if (debugGo) {
          ConLogf("..# of directions changed from (%d:%d) to (%d:%d)\n", ood, ond, nod, nnd);
        }
        //Go->Terminate(false);
        //return;
        doStop = true;
      }
      // ok, we can do this move
    } else {
      // ortho-walking thru the corridor
      int newDir = CheckCorridorMove(MoveVector, GetPos(), moveDir, &markAsTurn);
      if (newDir < 0) {
        // ah, something weird; stop right here
        Go->Terminate(false);
        return;
      }
      // if this is diagonal move: check if we have something interesting in previous dir
      if (MoveVector.X && MoveVector.Y) {
        if (HasInterestingItemsAt(GetPos()+game::GetMoveVector(moveDir))) {
          // move to item and stop
          newDir = moveDir;
          MoveVector = game::GetMoveVector(moveDir);
          doStop = true;
        }
      }
      // as `MoveVector` can change here, recalc squares
      Squares = CalculateNewSquaresUnder(MoveToSquare, GetPos()+MoveVector);
      // perform possible turn
      Go->SetDirection(newDir);
    }

    // stop near the dangerous square (but only chars)
    for (int mdv = 0; mdv < MDIR_STAND; ++mdv) {
      if (IsDangerousSquareChars(GetPos() + MoveVector + game::GetMoveVector(mdv))) {
        if (debugGo) ConLogf("..danger square!");
        Go->Terminate(false);
        return;
      }
    }
  }

  // now try to perform the move
  dirlogf("trying to make the move\n");

  square *BeginSquare = GetSquareUnder();
  uInt OldRoomIndex = GetLSquareUnder()->GetRoomIndex();
  uInt CurrentRoomIndex = MoveToSquare[0]->GetRoomIndex();

  // stop on the square with something interesting
  if (!doStop && !FirstStep) {
    // idiotic code!
    area *ca = GetSquareUnder()->GetArea();
    v2 npos = GetPos() + MoveVector;
    for (int f = 0; f != MDIR_STAND; f += 1) {
      v2 np = npos + game::GetMoveVector(f);
      if (ca->IsValidPos(np)) {
        lsquare *sq = static_cast<lsquare *>(ca->GetSquare(np.X, np.Y));
        if (IsPlayer()) {
          if (!sq->HasBeenSeen()) continue;
        } else {
          if (!sq->CanBeSeenBy(this)) continue;
        }
        //if (!sq->CanBeSeenBy(this)) continue;
        olterrain *terra = sq->GetOLTerrain();
        if (terra) {
          dirlogf("** OK terra at %d; door: %s; seen: %s\n", f, (terra->IsDoor() ? "yes" : "no"), (sq->IsGoSeen() ? "yes" : "no"));
          if (terra->IsDoor()) {
            if (ivanconfig::GetStopOnSeenDoors() || !sq->IsGoSeen()) {
              if (debugGo) {
                ConLogf("*** stopped near the door");
              }
              doStop = true;
              break;
            }
          }
        }
      }
    }

    // check items
    if (!doStop) {
      for (int c = 0; c < Squares; ++c) {
        lsquare *Square = MoveToSquare[c];
        if (IsPlayer()) {
          if (!Square->HasBeenSeen()) continue;
        } else {
          if (!Square->CanBeSeenBy(this)) continue;
        }
        if (Square->GetStack()->HasSomethingFunnyFor(this)) {
          if (debugGo) ConLogf("*** stopped on something interesting");
          doStop = true;
          break;
        }
      }
    }

    // check items in adjacent squares too, so diagonal move won't miss any
    if (!doStop) {
      for (int f = 0; f < MDIR_STAND && !doStop; ++f) {
        v2 np = game::GetMoveVector(f);
        if (np == MoveVector) continue; // this will be checked on the next move
        if (HasInterestingItemsAt(GetPos()+np)) {
          if (debugGo) ConLogf("*** stopped on something interesting (xdiag)");
          // stop
          Go->Terminate(false);
          return;
        }
      }
    }
  }

  Go->SetPrevWasTurn(markAsTurn && MoveVector.X && MoveVector.Y); // diagonal move?

  truth moveOk;
  if (!Go->IsTerminated()) {
    // just in case: check for moving int something dangerous
    if (IsDangerousSquareChars(GetPos() + MoveVector) ||
        IsDangerousSquareOther(GetPos() + MoveVector))
    {
      if (debugGo) ConLogf("..sense a danger (beforemove)");
      Go->Terminate(false);
      return;
    }
    moveOk = TryMove(MoveVector, true, game::PlayerIsRunning());
  } else {
    moveOk = false;
  }

  if (!moveOk || BeginSquare == GetSquareUnder() || (CurrentRoomIndex && (OldRoomIndex != CurrentRoomIndex))) {
    if (debugGo) {
      if (!moveOk) {
        ConLogf("..stopped (can't move)");
      } else if (BeginSquare == GetSquareUnder()) {
        ConLogf("..stopped (same begin square)");
      } else if (CurrentRoomIndex && OldRoomIndex != CurrentRoomIndex) {
        ConLogf("..stopped (different room)");
      } else {
        ConLogf("..stopped (other reason)");
      }
    }
    if (moveOk) {
      if (HasComparisonInfo()) {
        game::GetCurrentArea()->SendNewDrawRequest();
      }
      game::DrawEverything();
      // we're stopped, why delay?
      //if (ivanconfig::GetGoingDelay()) DELAY(ivanconfig::GetGoingDelay());
    }
    Go->Terminate(false);
    return;
  }

  if (FirstStep) {
    mPrevMoveDir = Go->GetDirection();
    Go->SetIsWalkingInOpen(!IsInCorridor(moveDir));
  }

  game::DrawEverything();
  if (doStop) {
    Go->Terminate(false);
  } else {
    if (ivanconfig::GetGoingDelay()) DELAY(ivanconfig::GetGoingDelay());
  }
}


//==========================================================================
//
//  character::AutoTravel
//
//==========================================================================
void character::AutoTravel (autotravel *Go, truth FirstStep) {
  worldmap *wmap = game::GetWorldMap();
  if (wmap) {
    v2 step = wmap->CalculateStepDir(this, GetPos(), Go->GetDestination()->GetPosition());
    #if 0
    ConLogf("AT: from=(%d,%d); to=(%d,%d); step=(%d,%d)",
            GetPos().X, GetPos().Y,
            Go->GetDestination()->GetPosition().X, Go->GetDestination()->GetPosition().Y,
            step.X, step.Y);
    #endif
    if (step == v2(0, 0)) {
      Go->Terminate(false);
    } else if (step != ERROR_V2) {
      if (!TryMove(step, true, game::PlayerIsRunning())) {
        Go->Terminate(false);
      } else {
        game::DrawEverything();
        if (ivanconfig::GetGoingDelay()) DELAY(ivanconfig::GetGoingDelay());
      }
    } else {
      Go->Terminate(false);
    }
  } else {
    Go->Terminate(false);
  }
}


//==========================================================================
//
//  character::SetTeam
//
//==========================================================================
void character::SetTeam (team *What) {
  Team = What;
  What->Add(this);
}


//==========================================================================
//
//  character::ChangeTeam
//
//==========================================================================
void character::ChangeTeam (team *What) {
  if (Team) Team->Remove(this);
  Team = What;
  SendNewDrawRequest();
  if (Team) Team->Add(this);
}


//==========================================================================
//
//  character::ChangeRandomAttribute
//
//==========================================================================
truth character::ChangeRandomAttribute (int HowMuch) {
  for (int c = 0; c < 50; ++c) {
    int AttribID = RAND_N(ATTRIBUTES);
    if (EditAttribute(AttribID, HowMuch)) return true;
  }
  return false;
}


//==========================================================================
//
//  character::RandomizeReply
//
//==========================================================================
int character::RandomizeReply (sLong &Said, int Replies) {
  truth NotSaid = false;
  for (int c = 0; c < Replies; ++c) {
    if (!(Said & (1 << c))) {
      NotSaid = true;
      break;
    }
  }
  if (!NotSaid) Said = 0;
  sLong ToSay;
  while (Said & 1 << (ToSay = RAND_N(Replies)));
  Said |= 1 << ToSay;
  return ToSay;
}


//==========================================================================
//
//  character::DisplayInfo
//
//==========================================================================
void character::DisplayInfo (festring &Msg) {
  if (IsPlayer()) {
    Msg << " You are " << GetStandVerb() << " here.";
  } else {
    Msg << ' ' << GetName(INDEFINITE).CapitalizeCopy() << " is " << GetStandVerb()
        << " here. " << GetPersonalPronoun().CapitalizeCopy();
    cchar *Separator1 = GetAction() ? "," : " and";
    cchar *Separator2 = " and";
    if (GetTeam() == PLAYER->GetTeam()) {
      Msg << " is tame";
    } else {
      int Relation = GetRelation(PLAYER);
      if (Relation == HOSTILE) Msg << " is hostile";
      else if (Relation == UNCARING) {
        Msg << " does not care about you";
        Separator1 = Separator2 = " and is";
      } else {
        Msg << " is friendly";
      }
    }
    if (StateIsActivated(PANIC)) {
      Msg << Separator1 << " panicked";
      Separator2 = " and";
    }
    if (GetAction()) Msg << Separator2 << ' ' << GetAction()->GetDescription();
    Msg << '.';
  }
}


//==========================================================================
//
//  character::TestWalkability
//
//==========================================================================
void character::TestWalkability () {
  if (!IsEnabled()) return;
  square *SquareUnder = !game::IsInWilderness() ? GetSquareUnder() : PLAYER->GetSquareUnder();
  if (SquareUnder->IsFatalToStay() && !CanMoveOn(SquareUnder)) {
    truth Alive = false;
    if (!game::IsInWilderness() || IsPlayer()) {
      for (int d = 0; d < GetNeighbourSquares(); ++d) {
        square *Square = GetNeighbourSquare(d);
        if (Square && CanMoveOn(Square) && IsFreeForMe(Square)) {
          if (IsPlayer()) {
            ADD_MESSAGE("%s.", SquareUnder->SurviveMessage(this));
          } else if (CanBeSeenByPlayer()) {
            ADD_MESSAGE("%s %s.", CHAR_NAME(DEFINITE), SquareUnder->MonsterSurviveMessage(this));
          }
          Move(Square->GetPos(), true); // actually, this shouldn't be a teleport move
          SquareUnder->SurviveEffect(this);
          Alive = true;
          break;
        }
      }
    }
    if (!Alive) {
      if (IsPlayer()) {
        Remove();
        SendToHell();
        festring DeathMsg = festring(SquareUnder->DeathMessage(this));
        game::AskForEscPress(DeathMsg + ".");
        festring Msg(SquareUnder->ScoreEntry(this));
        AddScoreEntry(Msg);
        game::End(Msg);
      } else {
        if (CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s %s.", CHAR_NAME(DEFINITE), SquareUnder->MonsterDeathVerb(this));
        }
        Die(0, festring(SquareUnder->ScoreEntry(this)), DISALLOW_MSG);
      }
    }
  }
}


//==========================================================================
//
//  character::GetSize
//
//==========================================================================
int character::GetSize () const {
  const int res = GetTorso()->GetSize();
  if (res < 1) {
    ConLogf("WARNING: character::GetSize() is %d for %s!",
            GetTorso()->GetSize(), GetNameSingular().CStr());
    return 1;
  }
  return res;
}


//==========================================================================
//
//  character::SetMainMaterial
//
//==========================================================================
void character::SetMainMaterial (material *NewMaterial, int SpecialFlags) {
  NewMaterial->SetVolume(GetBodyPart(0)->GetMainMaterial()->GetVolume());
  GetBodyPart(0)->SetMainMaterial(NewMaterial, SpecialFlags);
  for (int c = 1; c < BodyParts; ++c) {
    NewMaterial = NewMaterial->SpawnMore(GetBodyPart(c)->GetMainMaterial()->GetVolume());
    GetBodyPart(c)->SetMainMaterial(NewMaterial, SpecialFlags);
  }
}


//==========================================================================
//
//  character::ChangeMainMaterial
//
//==========================================================================
void character::ChangeMainMaterial (material *NewMaterial, int SpecialFlags) {
  NewMaterial->SetVolume(GetBodyPart(0)->GetMainMaterial()->GetVolume());
  GetBodyPart(0)->ChangeMainMaterial(NewMaterial, SpecialFlags);
  for (int c = 1; c < BodyParts; ++c) {
    NewMaterial = NewMaterial->SpawnMore(GetBodyPart(c)->GetMainMaterial()->GetVolume());
    GetBodyPart(c)->ChangeMainMaterial(NewMaterial, SpecialFlags);
  }
}


//==========================================================================
//
//  character::SetSecondaryMaterial
//
//==========================================================================
void character::SetSecondaryMaterial (material *, int) {
  ABORT("Illegal character::SetSecondaryMaterial call!");
}


//==========================================================================
//
//  character::ChangeSecondaryMaterial
//
//==========================================================================
void character::ChangeSecondaryMaterial (material *, int) {
  ABORT("Illegal character::ChangeSecondaryMaterial call!");
}


//==========================================================================
//
//  character::TeleportRandomly
//
//==========================================================================
void character::TeleportRandomly (truth Intentional) {
  v2 TelePos = ERROR_V2;
  if (StateIsActivated(TELEPORT_LOCK)) { ADD_MESSAGE("You flicker for a second."); return; }
  if (StateIsActivated(TELEPORT_CONTROL)) {
    if (IsPlayer()) {
      v2 Input = game::PositionQuestion(CONST_S("Where do you wish to teleport? "
                                                "[direction keys move cursor, space accepts]"), GetPos(),
                                        &game::TeleportHandler, 0, ivanconfig::GetLookZoom());
      if (Input == ERROR_V2) Input = GetPos(); // esc pressed
      lsquare *Square = GetNearLSquare(Input);
      if (CanMoveOn(Square) || game::GoThroughWallsCheatIsActive()) {
        if (Square->GetPos() == GetPos()) {
          ADD_MESSAGE("You disappear and reappear.");
          return;
        }
        if (IsFreeForMe(Square)) {
          if ((Input-GetPos()).GetLengthSquare() <= GetTeleportRangeSquare()) {
            EditExperience(INTELLIGENCE, 100, 1 << 10);
            TelePos = Input;
          } else {
            ADD_MESSAGE("You cannot concentrate yourself enough to control a teleport that far.");
          }
        } else {
          character *C = Square->GetCharacter();
          if (C) ADD_MESSAGE("For a moment you feel very much like %s.", C->CHAR_NAME(INDEFINITE));
          else ADD_MESSAGE("You feel that something weird has happened, but can't really tell what exactly.");
        }
      } else {
        ADD_MESSAGE("You feel like having been hit by something really hard from the inside.");
      }
    } else if (!Intentional) {
      if (IsGoingSomeWhere() && GetLevel()->IsValidPos(GoingTo)) {
        v2 Where = GetLevel()->GetNearestFreeSquare(this, GoingTo);
        if (Where != ERROR_V2 && (Where-GetPos()).GetLengthSquare() <= GetTeleportRangeSquare()) {
          EditExperience(INTELLIGENCE, 100, 1 << 10);
          Where = TelePos;
        }
      }
    }
  }

  if (TelePos == ERROR_V2) {
    TelePos = GetLevel()->GetRandomSquare(this);
    if (TelePos == ERROR_V2) return;
  }

  if (IsPlayer()) {
    ADD_MESSAGE("A rainbow-colored whirlpool twists the existence around you. You are sucked through a tunnel piercing a myriad of surreal universes. Luckily you return to this dimension in one piece.");
  }

  room *PossibleRoom = game::GetCurrentLevel()->GetLSquare(TelePos)->GetRoom();
  if (!PossibleRoom) {
    //if it's outside of a room
    Move(TelePos, true);
    if (!IsPlayer() && CanBeSeenByPlayer()) ADD_MESSAGE("%s appears.", CHAR_NAME(INDEFINITE));
    if (GetAction() && GetAction()->IsVoluntary()) GetAction()->Terminate(false);
  } else if (PossibleRoom && PossibleRoom->IsOKToTeleportInto()) {
    // If it's inside of a room, check whether a ward is active that might impede the player
    Move(TelePos, true);
    if (!IsPlayer() && CanBeSeenByPlayer()) ADD_MESSAGE("%s appears.", CHAR_NAME(INDEFINITE));
    if (GetAction() && GetAction()->IsVoluntary()) GetAction()->Terminate(false);
  } else {
    if (IsPlayer()) {
      ADD_MESSAGE("A mighty force blasts you back to where you were standing. A ward prevents you from teleporting.");
    }
    game::GetCurrentLevel()->Explosion(this, CONST_S("killed by an explosion triggered when attempting to teleport into room protected by a ward"), PLAYER->GetPos(), 300 >> 3, false);
    /*
    beamdata Beam
    (
      this,
      CONST_S("killed by an explosion triggered when attempting to teleport into room protected by a ward"),
      YOURSELF,
      3 // or 0 ?
    );
    lsquare* Square = GetNearLSquare(GetPos());
    Square->DrawParticles(RED);
    Square->FireBall(Beam);*/
  }
}


//==========================================================================
//
//  character::DoDetecting
//
//==========================================================================
void character::DoDetecting () {
  material *TempMaterial;
  for (;;) {
    festring Temp = game::DefaultQuestion(CONST_S("What material do you want to detect?"), game::GetDefaultDetectMaterial());
    TempMaterial = protosystem::CreateMaterial(Temp);
    if (TempMaterial) break;
    game::DrawEverythingNoBlit();
  }

  level *Level = GetLevel();
  int Squares = Level->DetectMaterial(TempMaterial);

  if (Squares > GetAttribute(INTELLIGENCE) * (25+RAND_N(51))) {
    ADD_MESSAGE("An enormous burst of geographical information overwhelms your consciousness. Your mind cannot cope with it and your memories blur.");
    if (!game::IsInWilderness()) {
      Level->BlurMemory();
    }
    BeginTemporaryState(CONFUSED, 1000 + RAND_N(1000));
    EditExperience(INTELLIGENCE, -100, 1 << 12);
  } else if (!Squares) {
    ADD_MESSAGE("You feel a sudden urge to imagine the dark void of a starless night sky.");
    EditExperience(INTELLIGENCE, 200, 1 << 12);
  } else {
    ADD_MESSAGE("You feel attracted to all things made of %s.", TempMaterial->GetName(false, false).CStr());
    game::PositionQuestion(CONST_S("Detecting material [direction keys move cursor, space exits]"),
                           GetPos(), 0, 0, ivanconfig::GetLookZoom());
    EditExperience(INTELLIGENCE, 300, 1 << 12);
  }

  delete TempMaterial;
  Level->CalculateLuminances();
  game::SendLOSUpdateRequest();
}


//==========================================================================
//
//  character::RestoreHP
//
//==========================================================================
void character::RestoreHP () {
  doforbodyparts()(this, &bodypart::FastRestoreHP);
  HP = MaxHP;
}


//==========================================================================
//
//  character::RestoreLivingHP
//
//==========================================================================
void character::RestoreLivingHP () {
  HP = 0;
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && BodyPart->CanRegenerate()) {
      BodyPart->FastRestoreHP();
      HP += BodyPart->GetHP();
    }
  }
}


//==========================================================================
//
//  character::AllowDamageTypeBloodSpill
//
//==========================================================================
truth character::AllowDamageTypeBloodSpill (int Type) {
  int ddt = Type&0xFFF;
  if (ddt == PHYSICAL_DAMAGE) return true;
  if (ddt == SOUND) return true;
  if (ddt == ENERGY) return true;
  if (ddt == ACID) return false;
  if (ddt == FIRE) return false;
  if (ddt == DRAIN) return false;
  if (ddt == POISON) return false;
  if (ddt == ELECTRICITY) return false;
  if (ddt == MUSTARD_GAS_DAMAGE) return false;
  if (ddt == PSI) return false;
  ABORT("Unknown blood effect destroyed the dungeon!  (type is %d [masked:%d])", Type, ddt);
  return false;
}


//==========================================================================
//
//  character::ReceiveBodyPartDamage
//
//  Returns truly done damage
//
//==========================================================================
int character::ReceiveBodyPartDamage (character *Damager, int Damage, int Type, int BodyPartIndex,
                                      int Direction, truth PenetrateResistance, truth Critical,
                                      truth ShowNoDamageMsg, truth CaptureBodyPart)
{
  bodypart *BodyPart = GetBodyPart(BodyPartIndex);

  if (!Damager || Damager->AttackMayDamageArmor()) {
    BodyPart->DamageArmor(Damager, Damage, Type);
  }

  if (!PenetrateResistance) {
    Damage -= (BodyPart->GetTotalResistance(Type)>>1)+RAND_N((BodyPart->GetTotalResistance(Type)>>1)+1);
  }

  if (int(Damage) < 1) {
    if (Critical) {
      Damage = 1;
    } else {
      if (ShowNoDamageMsg) {
        if (IsPlayer()) ADD_MESSAGE("You are not hurt.");
        else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s is not hurt.", GetPersonalPronoun().CStr());
      }
      return 0;
    }
  }

  if (Critical && AllowDamageTypeBloodSpill(Type) && !game::IsInWilderness()) {
    BodyPart->SpillBlood(2+RAND_2);
    for (int d = 0; d < GetNeighbourSquares(); ++d) {
      lsquare *Square = GetNeighbourLSquare(d);
      if (Square && Square->IsFlyable()) BodyPart->SpillBlood(1, Square->GetPos());
    }
  }

  if (BodyPart->ReceiveDamage(Damager, Damage, Type, Direction) && BodyPartCanBeSevered(BodyPartIndex)) {
    if (DamageTypeDestroysBodyPart(Type)) {
      if (IsPlayer()) {
        ADD_MESSAGE("Your %s is destroyed!", BodyPart->GetBodyPartName().CStr());
      } else if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s %s is destroyed!",
                    GetPossessivePronoun().CStr(), BodyPart->GetBodyPartName().CStr());
      }
      GetBodyPart(BodyPartIndex)->DropEquipment();
      item *Severed = SevereBodyPart(BodyPartIndex);
      if (Severed) {
        Severed->DestroyBodyPart(!game::IsInWilderness() ? GetStackUnder() : GetStack());
      }
      SendNewDrawRequest();
      if (IsPlayer()) {
        game::AskForEscPress(CONST_S("Bodypart destroyed!"));
      }
    } else {
      if (IsPlayer()) {
        ADD_MESSAGE("Your %s is severed off!", BodyPart->GetBodyPartName().CStr());
      } else if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s %s is severed off!",
                    GetPossessivePronoun().CStr(), BodyPart->GetBodyPartName().CStr());
      }
      item *Severed = SevereBodyPart(BodyPartIndex);
      SendNewDrawRequest();
      if (Severed) {
        if (CaptureBodyPart) {
          if (Damager) Damager->GetLSquareUnder()->AddItem(Severed);
        } else if (!game::IsInWilderness()) {
          /** No multi-tile humanoid support! */
          GetStackUnder()->AddItem(Severed);
          if (Direction != YOURSELF) {
            Severed->Fly(0, Direction, Damage);
          }
        } else {
          GetStack()->AddItem(Severed);
        }
        Severed->DropEquipment();
      } else if (IsPlayer() || CanBeSeenByPlayer()) {
        ADD_MESSAGE("It vanishes.");
      }
      if (IsPlayer()) {
        game::AskForEscPress(CONST_S("Bodypart severed!"));
      }
    }
    if (CanPanicFromSeveredBodyPart() && !IsDead() &&
        !StateIsActivated(PANIC) && !StateIsActivated(FEARLESS) &&
        RAND_N(100) < GetPanicLevel())
    {
      BeginTemporaryState(PANIC, 1000+RAND_N(1001));
    }
    SpecialBodyPartSeverReaction();
  }

  if (!IsDead()) CheckPanic(500);

  return Damage;
}


//==========================================================================
//
//  character::SevereBodyPart
//
//  Returns 0 if bodypart disappears
//
//==========================================================================
item *character::SevereBodyPart (int BodyPartIndex, truth ForceDisappearance, stack *EquipmentDropStack) {
  bodypart *BodyPart = GetBodyPart(BodyPartIndex);

  if (StateIsActivated(LEPROSY)) {
    BodyPart->GetMainMaterial()->SetIsInfectedByLeprosy(true);
  }

  if (BodyPartIndex == HEAD_INDEX && StateIsActivated(PARASITE_MIND_WORM)) {
    DeActivateTemporaryState(PARASITE_MIND_WORM);
  }

  if (ForceDisappearance || BodyPartsDisappearWhenSevered() ||
      StateIsActivated(POLYMORPHED) || game::AllBodyPartsVanish())
  {
    BodyPart->DropEquipment(EquipmentDropStack);
    BodyPart->RemoveFromSlot();
    CalculateAttributeBonuses();
    CalculateBattleInfo();
    BodyPart->SendToHell();
    SignalPossibleTransparencyChange();
    RemoveTraps(BodyPartIndex);
    return 0;
  }

  BodyPart->SetOwnerDescription(CONST_S("of ") + GetName(INDEFINITE));
  BodyPart->SetIsUnique(LeftOversAreUnique());
  UpdateBodyPartPicture(BodyPartIndex, true);
  BodyPart->RemoveFromSlot();
  BodyPart->RandomizePosition();
  CalculateAttributeBonuses();
  CalculateBattleInfo();
  BodyPart->Enable();
  SignalPossibleTransparencyChange();
  RemoveTraps(BodyPartIndex);
  return BodyPart;
}


//==========================================================================
//
//  character::ReceiveDamage
//
//  The second int is actually TargetFlags, which is not used here,
//  but seems to be used in humanoid::ReceiveDamage.
//  Returns true if the character really receives damage.
//  note that `Damager` can be 0 for parazite/god damage, for example.
//
//==========================================================================
truth character::ReceiveDamage (character *Damager, int Damage, int Type, int TargetFlags, int Direction,
                                truth Divide, truth PenetrateArmor, truth Critical, truth ShowMsg)
{
  truth Affected = ReceiveBodyPartDamage(Damager, Damage, Type, 0, Direction, PenetrateArmor, Critical, ShowMsg);
  if (DamageTypeAffectsInventory(Type)) {
    for (int c = 0; c < GetEquipments(); ++c) {
      item *Equipment = GetEquipment(c);
      if (Equipment) Equipment->ReceiveDamage(Damager, Damage, Type);
    }
    GetStack()->ReceiveDamage(Damager, Damage, Type);
  }
  return Affected;
}


//==========================================================================
//
//  character::GetDescription
//
//==========================================================================
festring character::GetDescription (int Case) const {
  if (IsPlayer()) return CONST_S("you");
  if (CanBeSeenByPlayer()) return GetName(Case);
  return CONST_S("something");
}


//==========================================================================
//
//  character::GetPersonalPronoun
//
//==========================================================================
festring character::GetPersonalPronoun (truth PlayersView) const {
  if (IsPlayer() && PlayersView) return CONST_S("you");
  if (GetSex() == UNDEFINED || (PlayersView && !CanBeSeenByPlayer() &&
                                !game::GetSeeWholeMapCheatMode()))
  {
    return CONST_S("it");
  }
  if (GetSex() == MALE) return CONST_S("he");
  return CONST_S("she");
}


//==========================================================================
//
//  character::GetPossessivePronoun
//
//==========================================================================
festring character::GetPossessivePronoun (truth PlayersView) const {
  if (IsPlayer() && PlayersView) return CONST_S("your");
  if (GetSex() == UNDEFINED || (PlayersView && !CanBeSeenByPlayer() &&
                                !game::GetSeeWholeMapCheatMode()))
  {
    return CONST_S("its");
  }
  if (GetSex() == MALE) return CONST_S("his");
  return CONST_S("her");
}


//==========================================================================
//
//  character::GetObjectPronoun
//
//==========================================================================
festring character::GetObjectPronoun (truth PlayersView) const {
  if (IsPlayer() && PlayersView) return CONST_S("you");
  if (GetSex() == UNDEFINED || (PlayersView && !CanBeSeenByPlayer() &&
                                !game::GetSeeWholeMapCheatMode()))
  {
    return CONST_S("it");
  }
  if (GetSex() == MALE) return CONST_S("him");
  return CONST_S("her");
}


//==========================================================================
//
//  character::AddName
//
//==========================================================================
void character::AddName (festring &String, int Case) const {
  if (AssignedName.IsEmpty()) {
    id::AddName(String, Case);
  } else if (!(Case & PLURAL)) {
    if (!ShowClassDescription()) {
      String << AssignedName;
    } else {
      String << AssignedName << ' ';
      id::AddName(String, (Case|ARTICLE_BIT)&~INDEFINE_BIT);
    }
  } else {
    id::AddName(String, Case);
    String << " named " << AssignedName;
  }
}


//==========================================================================
//
//  character::GetHungerState
//
//==========================================================================
int character::GetHungerState () const {
  if (!UsesNutrition()) return NOT_HUNGRY;
  const sLong np = GetNP();
  if (np > OVER_FED_LEVEL) return OVER_FED;
  if (np > BLOATED_LEVEL) return BLOATED;
  if (np > SATIATED_LEVEL) return SATIATED;
  if (np > NOT_HUNGER_LEVEL) return NOT_HUNGRY;
  if (np > HUNGER_LEVEL) return HUNGRY;
  if (np > VERY_HUNGER_LEVEL) return VERY_HUNGRY;
  return STARVING;
}


//==========================================================================
//
//  RoughPercent
//
//==========================================================================
static int RoughPercent (sLong curr, sLong lo, sLong hi) {
  if (lo >= hi) return -1;
  if (curr <= lo) return 0;
  if (curr >= hi) return 100;
  sLong prc = 100 * (curr - lo) / (hi - lo + 1);
  return Clamp(prc / 30 * 30, 10, 90);
}


//==========================================================================
//
//  character::GetCurrentHungerStatePercent
//
//  towards satiation
//
//==========================================================================
int character::GetCurrentHungerStatePercent () const {
  if (!UsesNutrition()) return -1;
  const sLong np = GetNP();
  if (np > OVER_FED_LEVEL) return -1;
  if (np > BLOATED_LEVEL) return -1; //RoughPercent(np, BLOATED_LEVEL, OVER_FED_LEVEL);
  if (np > SATIATED_LEVEL) return RoughPercent(np, SATIATED_LEVEL, BLOATED_LEVEL);
  if (np > NOT_HUNGER_LEVEL) return RoughPercent(np, NOT_HUNGER_LEVEL, SATIATED_LEVEL);
  if (np > HUNGER_LEVEL) return RoughPercent(np, HUNGER_LEVEL, NOT_HUNGER_LEVEL);
  if (np > VERY_HUNGER_LEVEL) return -1; //RoughPercent(np, VERY_HUNGER_LEVEL, NOT_HUNGER_LEVEL);
  return -1;
}


//==========================================================================
//
//  character::CanConsume
//
//==========================================================================
truth character::CanConsume (material *Material) const {
  return GetConsumeFlags() & Material->GetConsumeType();
}


//==========================================================================
//
//  character::SetTemporaryStateCounter
//
//==========================================================================
void character::SetTemporaryStateCounter (sLong State, int What) {
  for (int c = 0; c < STATES; ++c) {
    if ((1 << c) & State) TemporaryStateCounter[c] = What;
  }
}


//==========================================================================
//
//  character::EditTemporaryStateCounter
//
//==========================================================================
void character::EditTemporaryStateCounter (sLong State, int What) {
  for (int c = 0; c < STATES; ++c) {
    if ((1 << c) & State) {
      // k8
      // it is possible to boost it beyond PERMANENT, yeah.
      // this is legitimage game mechanics now.
      // i added clamping, tho, because there is no reason to go higher anyway.
      TemporaryStateCounter[c] = Min(TemporaryStateCounter[c] + What, PERMANENT);
    }
  }
}


//==========================================================================
//
//  character::GetTemporaryStateCounter
//
//==========================================================================
int character::GetTemporaryStateCounter (sLong State) const {
  for (int c = 0; c < STATES; ++c) {
    if ((1 << c) & State) return TemporaryStateCounter[c];
  }
  ABORT("Illegal GetTemporaryStateCounter request!");
  return 0;
}


//==========================================================================
//
//  character::CheckKick
//
//==========================================================================
truth character::CheckKick () const {
  if (!CanKick()) {
    if (IsPlayer()) ADD_MESSAGE("This race can't kick.");
    return false;
  }
  return true;
}


//==========================================================================
//
//  character::GetResistance
//
//==========================================================================
int character::GetResistance (int Type) const {
  int ddt = Type&0xFFF;
  if (ddt == PHYSICAL_DAMAGE) return 0;
  if (ddt == DRAIN) return 0;
  if (ddt == MUSTARD_GAS_DAMAGE) return 0;
  if (ddt == PSI) return 0;
  if (ddt == ENERGY) return GetEnergyResistance();
  if (ddt == FIRE) return GetFireResistance();
  if (ddt == POISON) return GetPoisonResistance();
  if (ddt == ELECTRICITY) return GetElectricityResistance();
  if (ddt == ACID) return GetAcidResistance();
  if (ddt == SOUND) return GetSoundResistance();
  ABORT("Resistance lack detected! (type is %d [masked:%d])", Type, ddt);
  return 0;
}


//==========================================================================
//
//  character::Regenerate
//
//==========================================================================
void character::Regenerate () {
  if (HP == MaxHP) {
    if (StateIsActivated(REGENERATION) && !RAND_N(3000)) {
      bodypart *NewBodyPart = GenerateRandomBodyPart();
      if (!NewBodyPart) return;
      NewBodyPart->SetHP(1);
           if (IsPlayer()) ADD_MESSAGE("You grow a new %s.", NewBodyPart->GetBodyPartName().CStr());
      else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s grows a new %s.", CHAR_NAME(DEFINITE), NewBodyPart->GetBodyPartName().CStr());
    }
    return;
  }

  sLong RegenerationBonus = 0;
  truth NoHealableBodyParts = true;
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && BodyPart->CanRegenerate()) {
      RegenerationBonus += BodyPart->GetMaxHP();
      if (NoHealableBodyParts && BodyPart->GetHP() < BodyPart->GetMaxHP()) NoHealableBodyParts = false;
    }
  }

  if (!RegenerationBonus || NoHealableBodyParts) return;

  RegenerationBonus *= (50+GetAttribute(ENDURANCE));

  if (StateIsActivated(REGENERATION)) {
    RegenerationBonus *= GetAttribute(ENDURANCE) /*<< 1*/;
  }

  if (Action && Action->IsRest()) {
    if (SquaresUnder == 1) {
      RegenerationBonus *= GetSquareUnder()->GetRestModifier() << 1;
    } else {
      int Lowest = GetSquareUnder(0)->GetRestModifier();
      for (int c = 1; c < GetSquaresUnder(); ++c) {
        int Mod = GetSquareUnder(c)->GetRestModifier();
        if (Mod < Lowest) Lowest = Mod;
      }
      RegenerationBonus *= Lowest << 1;
    }
  }

  RegenerationCounter += RegenerationBonus;

  while (RegenerationCounter > 1250000) {
    bodypart *BodyPart = HealHitPoint();
    if (!BodyPart) break;
    EditNP(-Max(7500/MaxHP, 1));
    RegenerationCounter -= 1250000;
    int HP = BodyPart->GetHP();
    EditExperience(ENDURANCE, Min(1000*BodyPart->GetMaxHP()/(HP*HP), 300), 1000);
  }
}


//==========================================================================
//
//  character::PrintInfo
//
//==========================================================================
void character::PrintInfo () const {
  felist Info(CONST_S("Information about ")+GetName(DEFINITE));
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment && (EquipmentEasilyRecognized(c) || game::WizardModeIsActive())) {
      int ImageKey = game::AddToItemDrawVector(itemvector(1, Equipment));
      Info.AddEntry(festring(GetEquipmentName(c))+": "+Equipment->GetName(INDEFINITE),
                    LIGHT_GRAY, 0, ImageKey, true);
      Info.AddLastEntryHelp(Equipment->GetDescriptiveInfo());
    }
  }
  if (Info.IsEmpty()) {
    ADD_MESSAGE("There's nothing special to tell about %s.", CHAR_NAME(DEFINITE));
  } else {
    game::SetStandardListAttributes(Info);
    Info.SetEntryDrawer(game::ItemEntryDrawer);
    Info.Draw();
  }
  game::ClearItemDrawVector();
}


//==========================================================================
//
//  character::TryToRiseFromTheDead
//
//==========================================================================
truth character::TryToRiseFromTheDead () {
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart) {
      BodyPart->ResetSpoiling();
      if (BodyPart->CanRegenerate() || BodyPart->GetHP() < 1) BodyPart->SetHP(1);
    }
  }
  ResetStates();
  return true;
}


//==========================================================================
//
//  character::RaiseTheDead
//
//==========================================================================
truth character::RaiseTheDead (character *) {
  truth Useful = false;
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (!BodyPart && CanCreateBodyPart(c)) {
      CreateBodyPart(c)->SetHP(1);
      if (IsPlayer()) ADD_MESSAGE("Suddenly you grow a new %s.", GetBodyPartName(c).CStr());
      else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s grows a new %s.", CHAR_NAME(DEFINITE), GetBodyPartName(c).CStr());
      Useful = true;
    } else if (BodyPart && BodyPart->CanRegenerate() && BodyPart->GetHP() < 1) {
      BodyPart->SetHP(1);
    }
  }
  if (!Useful) {
    if (IsPlayer()) ADD_MESSAGE("You shudder.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s shudders.", CHAR_NAME(DEFINITE));
  }
  return Useful;
}


//==========================================================================
//
//  character::SetSize
//
//==========================================================================
void character::SetSize (int Size) {
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart) BodyPart->SetSize(GetBodyPartSize(c, Size));
  }
}


//==========================================================================
//
//  character::GetBodyPartSize
//
//==========================================================================
sLong character::GetBodyPartSize (int I, int TotalSize) const {
  if (I == TORSO_INDEX) return TotalSize;
  ABORT("Weird bodypart size request for a character!");
  return 0;
}


//==========================================================================
//
//  character::GetBodyPartVolume
//
//==========================================================================
sLong character::GetBodyPartVolume (int I) const {
  if (I == TORSO_INDEX) return GetTotalVolume();
  ABORT("Weird bodypart volume request for a character!");
  return 0;
}


//==========================================================================
//
//  character::CreateBodyParts
//
//==========================================================================
void character::CreateBodyParts (int SpecialFlags) {
  for (int c = 0; c < BodyParts; ++c) {
    if (CanCreateBodyPart(c)) {
      CreateBodyPart(c, SpecialFlags);
    }
  }
}


//==========================================================================
//
//  character::RestoreBodyParts
//
//==========================================================================
void character::RestoreBodyParts () {
  for (int c = 0; c < BodyParts; ++c) {
    if (!GetBodyPart(c) && CanCreateBodyPart(c)) {
      CreateBodyPart(c);
    }
  }
}


//==========================================================================
//
//  character::UpdatePictures
//
//==========================================================================
void character::UpdatePictures () {
  if (!PictureUpdatesAreForbidden()) {
    for (int c = 0; c < BodyParts; ++c) {
      UpdateBodyPartPicture(c, false);
    }
  }
}


//==========================================================================
//
//  character::MakeBodyPart
//
//==========================================================================
bodypart *character::MakeBodyPart (int I) const {
  if (I == TORSO_INDEX) return normaltorso::Spawn(0, NO_MATERIALS);
  ABORT("Weird bodypart to make for a character!");
  return 0;
}


//==========================================================================
//
//  character::CreateBodyPart
//
//==========================================================================
bodypart *character::CreateBodyPart (int I, int SpecialFlags) {
  #if 0
  ConLogf("character(%s:%s)::CreateBodyPart: I=%d; flags=0x%08x",
          GetClassID(),
          /*GetConfigName.CStr(),*/
          DataBase->CfgStrName.CStr(),
          I, SpecialFlags);
  #endif
  bodypart *BodyPart = MakeBodyPart(I);
  material *Material = CreateBodyPartMaterial(I, GetBodyPartVolume(I));
  BodyPart->InitMaterials(Material, false);
  BodyPart->SetSize(GetBodyPartSize(I, GetTotalSize()));
  BodyPart->SetBloodMaterial(GetBloodMaterial());
  BodyPart->SetNormalMaterial(Material->GetConfig());
  BodyPart->SetHP(1);
  SetBodyPart(I, BodyPart);
  BodyPart->InitSpecialAttributes();
  if (!(SpecialFlags & NO_PIC_UPDATE)) {
    UpdateBodyPartPicture(I, false);
  }
  if (!IsInitializing()) {
    CalculateBattleInfo();
    SendNewDrawRequest();
    SignalPossibleTransparencyChange();
  }
  return BodyPart;
}


//==========================================================================
//
//  character::GetBodyPartBitmapPos
//
//==========================================================================
v2 character::GetBodyPartBitmapPos (int I, truth) const {
  if (I == TORSO_INDEX) return GetTorsoBitmapPos();
  ABORT("Weird bodypart BitmapPos request for a character!");
  return v2();
}


//==========================================================================
//
//  character::UpdateBodyPartPicture
//
//==========================================================================
void character::UpdateBodyPartPicture (int I, truth Severed) {
  bodypart *BP = GetBodyPart(I);
  if (BP) {
    BP->SetBitmapPos(GetBodyPartBitmapPos(I, Severed));
    BP->GetMainMaterial()->SetSkinColor(GetBodyPartColorA(I, Severed));
    BP->GetMainMaterial()->SetSkinColorIsSparkling(GetBodyPartSparkleFlags(I) & SPARKLING_A);
    BP->SetMaterialColorB(GetBodyPartColorB(I, Severed));
    BP->SetMaterialColorC(GetBodyPartColorC(I, Severed));
    BP->SetMaterialColorD(GetBodyPartColorD(I, Severed));
    BP->SetSparkleFlags(GetBodyPartSparkleFlags(I));
    BP->SetSpecialFlags(GetSpecialBodyPartFlags(I));
    BP->SetWobbleData(GetBodyPartWobbleData(I));
    BP->UpdatePictures();
  }
}


//==========================================================================
//
//  character::LoadDataBaseStats
//
//==========================================================================
void character::LoadDataBaseStats () {
  for (int c = 0; c < BASE_ATTRIBUTES; ++c) {
    BaseExperience[c] = DataBase->NaturalExperience[c];
    if (BaseExperience[c]) LimitRef(BaseExperience[c], MIN_EXP, MAX_EXP);
  }
  SetMoney(GetDefaultMoney());
  SetInitialSweatMaterial(GetSweatMaterial());
  const fearray<sLong> &Skills = GetKnownCWeaponSkills();
  if (Skills.Size) {
    const fearray<sLong> &Hits = GetCWeaponSkillHits();
    if (Hits.Size == 1) {
      for (uInt c = 0; c < Skills.Size; ++c) {
        if (Skills[c] < AllowedWeaponSkillCategories) {
          CWeaponSkill[Skills[c]].AddHit(Hits[0]*100);
        }
      }
    } else if (Hits.Size == Skills.Size) {
      for (uInt c = 0; c < Skills.Size; ++c) {
        if (Skills[c] < AllowedWeaponSkillCategories) {
          CWeaponSkill[Skills[c]].AddHit(Hits[c]*100);
        }
      }
    } else {
      ABORT("Illegal weapon skill hit array size detected!");
    }
  }
}


//==========================================================================
//
//  character::Initialize
//
//==========================================================================
void character::Initialize (int NewConfig, int SpecialFlags) {
  Flags |= C_INITIALIZING|C_IN_NO_MSG_MODE;
  CalculateBodyParts();
  CalculateAllowedWeaponSkillCategories();
  CalculateSquaresUnder();
  BodyPartSlot = new bodypartslot[BodyParts];
  OriginalBodyPartID = new std::list<feuLong>[BodyParts];
  CWeaponSkill = new cweaponskill[AllowedWeaponSkillCategories];
  SquareUnder = new square*[SquaresUnder];

  if (SquaresUnder == 1) {
    *SquareUnder = 0;
  } else {
    memset(SquareUnder, 0, SquaresUnder * sizeof(square *));
  }

  for (int c = 0; c < BodyParts; ++c) {
    BodyPartSlot[c].SetMaster(this);
  }

  if (!(SpecialFlags & LOAD)) {
    ID = game::CreateNewCharacterID(this);
    databasecreator<character>::InstallDataBase(this, NewConfig);
    LoadDataBaseStats();
    TemporaryState |= GetClassStates();
    if (TemporaryState) {
      for (int c = 0; c < STATES; ++c) {
        if (TemporaryState & (1 << c)) {
          TemporaryStateCounter[c] = PERMANENT;
        }
      }
    }

    CreateBodyParts(SpecialFlags | NO_PIC_UPDATE);
    InitSpecialAttributes();
    CommandFlags = GetDefaultCommandFlags();

    if (GetAttribute(INTELLIGENCE, false) < 8) {
      CommandFlags &= ~DONT_CONSUME_ANYTHING_VALUABLE; // gum
    }

    if (!GetDefaultName().IsEmpty()) {
      SetAssignedName(GetDefaultName());
    }
  }

  if (!(SpecialFlags & LOAD)) PostConstruct();

  if (!(SpecialFlags & LOAD)) {
    if (!(SpecialFlags & NO_EQUIPMENT)) {
      CreateInitialEquipment((SpecialFlags & NO_EQUIPMENT_PIC_UPDATE) >> 1);
    }
    if (!(SpecialFlags & NO_PIC_UPDATE)) {
      UpdatePictures();
    }
    CalculateAll();
    RestoreHP();
    RestoreStamina();
  }

  Flags &= ~(C_INITIALIZING|C_IN_NO_MSG_MODE);
}


//==========================================================================
//
//  character::TeleportNear
//
//==========================================================================
truth character::TeleportNear (character *Caller) {
  v2 Where = GetLevel()->GetNearestFreeSquare(this, Caller->GetPos());
  if (Where == ERROR_V2) return false;
  Move(Where, true);
  return true;
}


//==========================================================================
//
//  character::ReceiveHeal
//
//==========================================================================
void character::ReceiveHeal (sLong Amount) {
  int c;
  for (c = 0; c < Amount / 10; ++c) if (!HealHitPoint()) break;
  Amount -= c*10;
  if (RAND_N(10) < Amount) HealHitPoint();
  if (Amount >= 250 || RAND_N(250) < Amount) {
    bodypart *NewBodyPart = GenerateRandomBodyPart();
    if (!NewBodyPart) return;
    NewBodyPart->SetHP(1);
    if (IsPlayer()) ADD_MESSAGE("You grow a new %s.", NewBodyPart->GetBodyPartName().CStr());
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s grows a new %s.", CHAR_NAME(DEFINITE), NewBodyPart->GetBodyPartName().CStr());
  }
}


//==========================================================================
//
//  character::AddHealingLiquidConsumeEndMessage
//
//==========================================================================
void character::AddHealingLiquidConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel better.");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s looks healthier.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::ReceiveSchoolFood
//
//==========================================================================
void character::ReceiveSchoolFood (sLong SizeOfEffect) {
  SizeOfEffect += RAND_N(SizeOfEffect);
  if (SizeOfEffect >= 250) VomitAtRandomDirection(SizeOfEffect);
  if (!RAND_N(3) && SizeOfEffect >= 500 && EditAttribute(ENDURANCE, SizeOfEffect/500)) {
    if (IsPlayer()) ADD_MESSAGE("You gain a little bit of toughness for surviving this stuff.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("Suddenly %s looks tougher.", CHAR_NAME(DEFINITE));
  }
  BeginTemporaryState(POISONED, (SizeOfEffect>>1));
}


//==========================================================================
//
//  character::AddSchoolFoodConsumeEndMessage
//
//==========================================================================
void character::AddSchoolFoodConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("Yuck! This stuff tasted like vomit and old mousepads.");
  }
}


//==========================================================================
//
//  character::AddSchoolFoodHitMessage
//
//==========================================================================
void character::AddSchoolFoodHitMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Yuck! This stuff feels like vomit and old mousepads.");
}


//==========================================================================
//
//  character::ReceiveNutrition
//
//==========================================================================
void character::ReceiveNutrition (sLong SizeOfEffect) {
  EditNP(SizeOfEffect);
}


//==========================================================================
//
//  character::ReceiveOmmelUrine
//
//==========================================================================
void character::ReceiveOmmelUrine (sLong Amount) {
  EditExperience(ARM_STRENGTH, 500, Amount<<4);
  EditExperience(LEG_STRENGTH, 500, Amount<<4);
  if (IsPlayer()) game::DoEvilDeed(Amount/25);
}


//==========================================================================
//
//  character::ReceiveOmmelCerumen
//
//==========================================================================
void character::ReceiveOmmelCerumen (sLong Amount) {
  EditExperience(INTELLIGENCE, 500, Amount << 5);
  EditExperience(WISDOM, 500, Amount << 5);
  if (IsPlayer()) game::DoEvilDeed(Amount / 25);
}


//==========================================================================
//
//  character::ReceiveOmmelSweat
//
//==========================================================================
void character::ReceiveOmmelSweat (sLong Amount) {
  EditExperience(AGILITY, 500, Amount << 4);
  EditExperience(DEXTERITY, 500, Amount << 4);
  RestoreStamina();
  if (IsPlayer()) game::DoEvilDeed(Amount / 25);
}


//==========================================================================
//
//  character::ReceiveOmmelTears
//
//==========================================================================
void character::ReceiveOmmelTears (sLong Amount) {
  EditExperience(PERCEPTION, 500, Amount << 4);
  EditExperience(CHARISMA, 500, Amount << 4);
  if (IsPlayer()) game::DoEvilDeed(Amount / 25);
}


//==========================================================================
//
//  character::ReceiveOmmelSnot
//
//==========================================================================
void character::ReceiveOmmelSnot (sLong Amount) {
  EditExperience(ENDURANCE, 500, Amount << 5);
  RestoreLivingHP();
  if (IsPlayer()) game::DoEvilDeed(Amount / 25);
}


//==========================================================================
//
//  character::ReceiveOmmelBone
//
//==========================================================================
void character::ReceiveOmmelBone (sLong Amount) {
  EditExperience(ARM_STRENGTH, 500, Amount << 6);
  EditExperience(LEG_STRENGTH, 500, Amount << 6);
  EditExperience(DEXTERITY, 500, Amount << 6);
  EditExperience(AGILITY, 500, Amount << 6);
  EditExperience(ENDURANCE, 500, Amount << 6);
  EditExperience(PERCEPTION, 500, Amount << 6);
  EditExperience(INTELLIGENCE, 500, Amount << 6);
  EditExperience(WISDOM, 500, Amount << 6);
  EditExperience(CHARISMA, 500, Amount << 6);
  RestoreLivingHP();
  RestoreStamina();
  if (IsPlayer()) game::DoEvilDeed(Amount / 25);
}


//==========================================================================
//
//  character::ReceiveOmmelBlood
//
//==========================================================================
void character::ReceiveOmmelBlood (sLong Amount) {
  EditExperience(WILL_POWER, 500, Amount << 4);
  EditExperience(MANA, 500, Amount << 4);
  //k8: we don't have the above attrs. sigh.
  EditExperience(INTELLIGENCE, 500, Amount << 3);
  EditExperience(WISDOM, 500, Amount << 3);
  if (IsPlayer()) game::DoEvilDeed(Amount / 25);
}


//==========================================================================
//
//  character::AddOmmelConsumeEndMessage
//
//==========================================================================
void character::AddOmmelConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel a primitive force coursing through your veins.");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("Suddenly %s looks more powerful.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::ReceivePepsi
//
//==========================================================================
void character::ReceivePepsi (sLong Amount) {
  ReceiveDamage(0, Amount / 100, POISON, TORSO);
  EditExperience(PERCEPTION, Amount, 1 << 14);
  if (CheckDeath(CONST_S("was poisoned by pepsi"), 0)) return;
  if (IsPlayer()) game::DoEvilDeed(Amount / 10);
}


//==========================================================================
//
//  character::AddPepsiConsumeEndMessage
//
//==========================================================================
void character::AddPepsiConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("Urgh. You feel your guruism fading away.");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s looks very lame.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::AddPepsiConsumeEndMessage
//
//==========================================================================
void character::AddCocaColaConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel your guruism rising!");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s looks awesome.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::ReceiveDarkness
//
//==========================================================================
void character::ReceiveDarkness (sLong Amount) {
  EditExperience(INTELLIGENCE, -Amount / 5, 1 << 13);
  EditExperience(WISDOM, -Amount / 5, 1 << 13);
  EditExperience(CHARISMA, -Amount / 5, 1 << 13);
  if (IsPlayer()) game::DoEvilDeed(int(Amount / 50));
}


//==========================================================================
//
//  character::AddFrogFleshConsumeEndMessage
//
//==========================================================================
void character::AddFrogFleshConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("Arg. You feel the fate of a navastater placed upon you...");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("Suddenly %s looks like a navastater.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::ReceiveKoboldFlesh
//
//==========================================================================
void character::ReceiveKoboldFlesh (sLong) {
  /* As it is commonly known, the possibility of fainting per 500 cubic
     centimeters of kobold flesh is exactly 5%. */
  if (!RAND_N(20)) {
    if (IsPlayer()) ADD_MESSAGE("You lose control of your legs and fall down.");
    LoseConsciousness(250 + RAND_N(250));
  }
}


//==========================================================================
//
//  character::AddKoboldFleshConsumeEndMessage
//
//==========================================================================
void character::AddKoboldFleshConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("This stuff tasted really funny.");
  }
}


//==========================================================================
//
//  character::AddKoboldFleshHitMessage
//
//==========================================================================
void character::AddKoboldFleshHitMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel very funny.");
  }
}


//==========================================================================
//
//  character::AddBoneConsumeEndMessage
//
//==========================================================================
void character::AddBoneConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel like a hippie.");
  } else if (CanBeSeenByPlayer()) {
    // this suspects that nobody except dogs can eat bones
    ADD_MESSAGE("%s barks happily.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::RawEditAttribute
//
//==========================================================================
truth character::RawEditAttribute (double &Experience, int Amount) const {
  /* Check if the attribute is disabled for creature */
  if (!Experience) return false;
  if ((Amount < 0 && Experience < 2 * EXP_MULTIPLIER) || (Amount > 0 && Experience > 999 * EXP_MULTIPLIER)) return false;
  Experience += Amount * EXP_MULTIPLIER;
  LimitRef<double>(Experience, MIN_EXP, MAX_EXP);
  return true;
}


//==========================================================================
//
//  Darken
//
//==========================================================================
static inline col16 Darken (col16 rgb, int darken) {
  int r = GetRed16(rgb);
  int g = GetGreen16(rgb);
  int b = GetBlue16(rgb);
  r = Clamp(r - darken, 0, 255);
  g = Clamp(g - darken, 0, 255);
  b = Clamp(b - darken, 0, 255);
  return MakeRGB16(r, g, b);
}


#define XNAME_DARKEN  (64)

//==========================================================================
//
//  character::PrintAttribute
//
//==========================================================================
void character::PrintAttribute (cchar *Desc, int I, int PanelPosX, int PanelPosY) const {
  const int Attribute = GetAttribute(I);
  const int NoBonusAttribute = GetAttribute(I, false);
  const int AtrX = FONT->FontWidth() * 5;
  col16 C = game::GetAttributeColor(I);
  //festring String;// = Desc;
  //String.Resize(5);
  //String << Attribute;
  //String.Resize(8);
  /*
  FONT->PrintStr(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), Darken(C, XNAME_DARKEN), CONST_S(Desc));
  FONT->Printf(DOUBLE_BUFFER, v2(PanelPosX + AtrX, PanelPosY), C, "%d", Attribute);
  if (Attribute != NoBonusAttribute) {
    int Where = PanelPosX + AtrX + FONT->FontWidth() * 9;
    FONT->Printf(DOUBLE_BUFFER, v2(Where, PanelPosY), LIGHT_GRAY, "%d", NoBonusAttribute);
  }
  */
  FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), Darken(C, XNAME_DARKEN), CONST_S(Desc));
  FONT->PrintfOutline(DOUBLE_BUFFER, v2(PanelPosX + AtrX, PanelPosY), C, "%d", Attribute);
  if (Attribute != NoBonusAttribute) {
    int Where = PanelPosX + AtrX + FONT->FontWidth() * 9;
    FONT->PrintfOutline(DOUBLE_BUFFER, v2(Where, PanelPosY), LIGHT_GRAY, "%d", NoBonusAttribute);
  }
}


//==========================================================================
//
//  character::DrawPanel
//
//  draw paperdoll and stats
//
//==========================================================================
void character::DrawPanel (truth AnimationDraw) const {
  if (AnimationDraw) {
    DrawStats(true);
    return;
  }

  int PanelPosX, PanelPosY;

  // player name
  if (ivanconfig::GetStatusOnLeft()) {
    igraph::BlitBackGround(v2(0, 0),
                           v2(109, RES.Y));
    igraph::BlitBackGround(v2(16 + 96, 45 + (game::GetScreenYSize() << 4)),
                           v2(game::GetScreenXSize() << 4, 9));
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(16 + 96, 45 - 4 + (game::GetScreenYSize() << 4)),
                          /*WHITE*/LIGHT_GRAY,
                          GetPanelName());
    PanelPosX = game::GetLeftStatsPos();
  } else {
    igraph::BlitBackGround(v2(19 + (game::GetScreenXSize() << 4), 0),
                           v2(RES.X - 19 - (game::GetScreenXSize() << 4), RES.Y));
    igraph::BlitBackGround(v2(16, 45 + (game::GetScreenYSize() << 4)),
                           v2(game::GetScreenXSize() << 4, 9));
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(16, 45 - 4 + (game::GetScreenYSize() << 4)),
                          /*WHITE*/LIGHT_GRAY,
                          GetPanelName());
    PanelPosX = RES.X - 96;
  }
  game::UpdateAttributeMemory();

  const int lineHeight = 10;
  PanelPosY = DrawStats(false);

  PrintAttribute("End", ENDURANCE, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  PrintAttribute("Per", PERCEPTION, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  PrintAttribute("Int", INTELLIGENCE, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  PrintAttribute("Wis", WISDOM, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  #if 0
  // willpower was added by devs in CVS, but never really used anywhere.
  // it is now used in two comm. fork monsters, but meh...
  PrintAttribute("Wil", WILL_POWER, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  #endif
  PrintAttribute("Cha", CHARISMA, PanelPosX, PanelPosY); PanelPosY += lineHeight;

  #if 0
  // this is actually height, in cm. not useful.
  FONT->PrintOutline(DOUBLE_BUFFER, v2(PanelPosX + FONT->FontWidth() * 5, PanelPosY), WHITE, "%d", GetSize());
  FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), Darken(WHITE, XNAME_DARKEN), "Siz");
  PanelPosY += lineHeight;
  #endif

  const bool inbad = IsInBadCondition();
  const int hp = GetHP();
  const int maxhp = GetMaxHP();
  PanelPosY += lineHeight / 4;
  FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), (inbad ? RED : Darken(WHITE, XNAME_DARKEN)),
                        CONST_S("HP"));
  FONT->PrintfOutline(DOUBLE_BUFFER, v2(PanelPosX + FONT->FontWidth() * 3, PanelPosY),
                      (inbad ? RED : WHITE),
                      "%s%d\2/%d", (inbad || hp >= maxhp ? "" : "\1Y"), hp, maxhp);
  PanelPosY += lineHeight + lineHeight / 2;

  FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), WHITE, CONST_S("Gold"));
  FONT->PrintfOutline(DOUBLE_BUFFER, v2(PanelPosX + FONT->FontWidth() * 5, PanelPosY),
                      YELLOW, "%d", GetMoney());
  PanelPosY += lineHeight * 2;

  if (game::IsInWilderness()) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), MakeRGB16(170, 240, 0),
                          CONST_S("Worldmap"));
  } else {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), MakeRGB16(170, 240, 0),
                          game::GetCurrentDungeon()->GetShortLevelDescription(game::GetCurrentLevelIndex()));
  }
  PanelPosY += lineHeight + lineHeight / 4;

  ivantime Time;
  game::GetTime(Time);

  FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), Darken(WHITE, XNAME_DARKEN),
                        CONST_S("Day"));
  FONT->PrintfOutline(DOUBLE_BUFFER, v2(PanelPosX + FONT->FontWidth() * 5, PanelPosY),
                      WHITE, "%d", Time.Day);
  PanelPosY += lineHeight;

  FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), Darken(WHITE, XNAME_DARKEN),
                        CONST_S("Time"));
  FONT->PrintfOutline(DOUBLE_BUFFER, v2(PanelPosX + FONT->FontWidth() * 5, PanelPosY),
                      WHITE, "%d:%s%d", Time.Hour, Time.Min < 10 ? "0" : "", Time.Min);
  PanelPosY += lineHeight;

  FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), Darken(WHITE, XNAME_DARKEN),
                        CONST_S("Turn"));
  FONT->PrintfOutline(DOUBLE_BUFFER, v2(PanelPosX + FONT->FontWidth() * 5, PanelPosY),
                      WHITE, "%d", game::GetTurn());
  PanelPosY += lineHeight + lineHeight / 2;

  if (GetAction()) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY),
                          MakeRGB16(60, 206, 99),
                          festring(GetAction()->GetDescription()).CapitalizeCopy());
    PanelPosY += lineHeight;
  }

  const packcol16 clrperm = MakeRGB16(0, 188, 128); //ORANGE; //BLUE;
  const packcol16 clrtemp = MakeRGB16(200, 0, 200); //WHITE;
  for (int c = 0; c < STATES; ++c) {
    if (!(StateData[c].Flags & SECRET) && StateIsActivated(1 << c) &&
        ((1 << c) != HASTE || !StateIsActivated(SLOW)) &&
        ((1 << c) != SLOW || !StateIsActivated(HASTE)))
    {
      FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY),
                            ((1 << c) & EquipmentState) || TemporaryStateCounter[c] >= PERMANENT ? clrperm : clrtemp,
                            CONST_S(StateData[c].Description));
      PanelPosY += lineHeight;
    }
  }

  auto hst = GetHungerState();
  auto hprc = GetCurrentHungerStatePercent();
  festring hll;
  packcol16 hllColor = LIGHT_GRAY;
       if (hst == STARVING) { hllColor = RED; hll << "Starving"; }
  else if (hst == VERY_HUNGRY) { hllColor = RED; hll << "Very hungry"; }
  else if (hst == HUNGRY) { hllColor = ORANGE; hll << "Hungry"; }
  else if (hst == SATIATED) { hllColor = WHITE; hll << "Satiated"; }
  else if (hst == BLOATED) { hllColor = WHITE; hll << "Bloated"; }
  else if (hst == OVER_FED) { hllColor = WHITE; hll << "Overfed!"; }
  else if (hst == NOT_HUNGRY) {
    if (hprc <= 30) {
      hllColor = LIGHT_GRAY; hll << "Alm.\x12hungry";
    } else if (hprc >= 80) {
      hllColor = LIGHT_GRAY; hll << "Alm.\x12satiated";
    }
  }
  if (!hll.IsEmpty()) {
    #if 0
    if (hprc > 0) {
      festring ss; ss << " \1#999|(" << hprc << ")";
      FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), hllColor, ss); PanelPosY += 1;
      PanelPosY += lineHeight;
    }
    #endif
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), hllColor, hll); PanelPosY += 1;
    PanelPosY += lineHeight;
  }

  auto bst = GetBurdenState();
  if (bst == OVER_LOADED) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), RED, CONST_S("Overload!"));
    PanelPosY += lineHeight;
  } else if (bst == STRESSED) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), ORANGE, CONST_S("Stressed"));
    PanelPosY += lineHeight;
  } else if (bst == BURDENED) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), clrtemp, CONST_S("Burdened"));
    PanelPosY += lineHeight;
  }

  auto trst = GetTirednessState();
  if (trst == FAINTING) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), RED, CONST_S("Fainting"));
    PanelPosY += lineHeight;
  } else if (trst == EXHAUSTED) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), ORANGE, CONST_S("Exhausted"));
    PanelPosY += lineHeight;
  }

  if (game::IsInWilderness() && game::PlayerHasBoat() && IsSwimming()) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), WHITE, CONST_S("On Ship"));
    PanelPosY += lineHeight;
  }

  if (game::PlayerIsRunning()) {
    FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), WHITE, CONST_S(GetRunDescriptionLine(0)));
    PanelPosY += lineHeight;
    cchar *SecondLine = GetRunDescriptionLine(1);
    if (SecondLine && SecondLine[0]) {
      FONT->PrintStrOutline(DOUBLE_BUFFER, v2(PanelPosX, PanelPosY), WHITE, CONST_S(SecondLine));
      PanelPosY += lineHeight;
    }
  }
}


//==========================================================================
//
//  character::IsInBadCondition
//
//  original code: truth IsInBadCondition () const { return HP * 3 < MaxHP; }
//
//==========================================================================
truth character::IsInBadCondition () const {
  for (int c = 0; c < BodyParts; c += 1) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && BodyPartIsVital(c) &&
        (BodyPart->GetHP() * 3 < BodyPart->GetMaxHP() ||
         (BodyPart->GetHP() == 1 && BodyPart->GetMaxHP() > 1)))
    {
      return true;
    }
  }

  return HP * 3 < MaxHP;
}


//==========================================================================
//
//  character::TryToStealFromShop
//
//==========================================================================
truth character::TryToStealFromShop (character *Shopkeeper, item *ToSteal) {
  if (!IsPlayer()) {
    // this doesn't matter, as only the player can steal for now.
    return RAND_2; // Plain 50% chance for monsters.
  }

  double perception_check;
  if (Shopkeeper) {
    perception_check = 100 - (1000 / (10 + Shopkeeper->GetAttribute(PERCEPTION)));
  } else {
    perception_check = 0;
  }

  const double base_chance = 100 - (100000 / (2000 + game::GetGod(CLEPTIA)->GetRelation()));
  const double size_mod = ::pow(0.99999, ((ToSteal->GetWeight() * ToSteal->GetSize()) / GetAttribute(ARM_STRENGTH)));
  const double stat_mod = ::pow(1.01, ((100 - (1000 / (10 + GetAttribute(DEXTERITY)))) - perception_check));
  //const int normalized_chance = Max(5, Min(95, int(base_chance * size_mod * stat_mod)));
  const int normalized_chance = Clamp((int)(base_chance * size_mod * stat_mod), 5, 95);

  const int stealx = 1 + RAND_N(100);

  ConLogf("shoplifting '%s'; perception=%g; base_chance=%g; size_mod=%g; "
          "stat_mod=%g; chance=%d (raw:%d); stx=%d",
          ToSteal->DebugLogName().CStr(),
          perception_check, base_chance, size_mod, stat_mod,
          normalized_chance, (int)(base_chance * size_mod * stat_mod),
          stealx);

  game::DoEvilDeed(25);
  game::GetGod(CLEPTIA)->AdjustRelation(50);

  return (stealx < normalized_chance);
}


//==========================================================================
//
//  character::CalculateDodgeValue
//
//==========================================================================
void character::CalculateDodgeValue () {
  DodgeValue = 0.05 * GetMoveEase() * GetAttribute(AGILITY) / sqrt(GetSize());
  if (IsFlying()) DodgeValue *= 2;
  if (DodgeValue < 1) DodgeValue = 1;
}


//==========================================================================
//
//  character::DamageTypeAffectsInventory
//
//==========================================================================
truth character::DamageTypeAffectsInventory (int Type) {
  int ddt = Type&0xFFF;
  if (ddt == SOUND) return true;
  if (ddt == ENERGY) return true;
  if (ddt == ACID) return true;
  if (ddt == FIRE) return true;
  if (ddt == ELECTRICITY) return true;
  if (ddt == PHYSICAL_DAMAGE) return false;
  if (ddt == POISON) return false;
  if (ddt == DRAIN) return false;
  if (ddt == MUSTARD_GAS_DAMAGE) return false;
  if (ddt == PSI) return false;
  ABORT("Unknown reaping effect destroyed dungeon! (type is %d [masked:%d])", Type, ddt);
  return false;
}


//==========================================================================
//
//  character::CheckForBlockWithArm
//
//==========================================================================
int character::CheckForBlockWithArm (character *Enemy, item *Weapon, arm *Arm,
                                     double WeaponToHitValue, int Damage, int Success, int Type)
{
  int BlockStrength = Arm->GetBlockCapability();
  double BlockValue = Arm->GetBlockValue();
  if (BlockStrength && BlockValue) {
    item *Blocker = Arm->GetWielded();
    if (RAND_N((int)(100+WeaponToHitValue/BlockValue/(1<<BlocksSinceLastTurn)*(100+Success))) < 100) {
      int NewDamage = BlockStrength < Damage ? Damage-BlockStrength : 0;
           if (Type == UNARMED_ATTACK) AddBlockMessage(Enemy, Blocker, festring(Enemy->UnarmedHitNoun()), NewDamage);
      else if (Type == WEAPON_ATTACK) AddBlockMessage(Enemy, Blocker, CONST_S("attack"), NewDamage);
      else if (Type == KICK_ATTACK) AddBlockMessage(Enemy, Blocker, festring(Enemy->KickNoun()), NewDamage);
      else if (Type == BITE_ATTACK) AddBlockMessage(Enemy, Blocker, festring(Enemy->BiteNoun()), NewDamage);
      sLong Weight = Blocker->GetWeight();
      sLong StrExp = Limit(15 * Weight / 200, 75, 300);
      sLong DexExp = Weight ? Limit(75000 / Weight, 75, 300) : 300;
      Arm->EditExperience(ARM_STRENGTH, StrExp, 1 << 8);
      Arm->EditExperience(DEXTERITY, DexExp, 1 << 8);
      #if 0 /* old code */
        EditStamina(-10000 / GetAttribute(ARM_STRENGTH), false);
      #else
        if (Blocker && Blocker->IsShield(this)) {
          // shields are made for blocking! ;-)
          #if 0
          ConLogf("SHIELD stamina penalty: %d (%d)", -10000 / GetAttribute(ARM_STRENGTH),
                  -10000 / GetAttribute(ARM_STRENGTH) / 5);
          #endif
          EditStamina(-10000 / GetAttribute(ARM_STRENGTH) / 5, false);
        } else {
          #if 0
          ConLogf("NORMAL stamina penalty: %d", -10000 / GetAttribute(ARM_STRENGTH));
          #endif
          EditStamina(-10000 / GetAttribute(ARM_STRENGTH), false);
        }
      #endif
      if (Arm->TwoHandWieldIsActive()) {
        arm *PairArm = Arm->GetPairArm();
        PairArm->EditExperience(ARM_STRENGTH, StrExp, 1 << 8);
        PairArm->EditExperience(DEXTERITY, DexExp, 1 << 8);
      }
      Blocker->WeaponSkillHit(Enemy->CalculateWeaponSkillHits(this));
      Blocker->ReceiveDamage(this, Damage, PHYSICAL_DAMAGE);
      Blocker->BlockEffect(this, Enemy, Weapon, Type);
      if (Weapon) Weapon->ReceiveDamage(Enemy, Damage - NewDamage, PHYSICAL_DAMAGE);
      if (BlocksSinceLastTurn < 16) ++BlocksSinceLastTurn;
      return NewDamage;
    }
  }
  return Damage;
}


//==========================================================================
//
//  character::GetStateAPGain
//
//==========================================================================
sLong character::GetStateAPGain (sLong BaseAPGain) const {
  if (!StateIsActivated(HASTE) == !StateIsActivated(SLOW)) return BaseAPGain;
  if (StateIsActivated(HASTE)) return (BaseAPGain * 5) >> 2;
  return (BaseAPGain << 2) / 5;
}


//==========================================================================
//
//  character::SignalEquipmentAdd
//
//==========================================================================
void character::SignalEquipmentAdd (int EquipmentIndex) {
  item *Equipment = GetEquipment(EquipmentIndex);
  if (Equipment->IsInCorrectSlot(EquipmentIndex)) {
    sLong AddedStates = Equipment->GetGearStates();
    if (AddedStates) {
      for (int c = 0; c < STATES; ++c) {
        if (AddedStates & (1 << c)) {
          if (!StateIsActivated(1 << c)) {
            if (!IsInNoMsgMode()) (this->*StateData[c].PrintBeginMessage)();
            EquipmentState |= 1 << c;
            if (StateData[c].BeginHandler) (this->*StateData[c].BeginHandler)();
          } else {
            EquipmentState |= 1 << c;
          }
        }
      }
    }
  }
  if (!IsInitializing() && Equipment->IsInCorrectSlot(EquipmentIndex)) ApplyEquipmentAttributeBonuses(Equipment);
}


//==========================================================================
//
//  character::SignalEquipmentRemoval
//
//==========================================================================
void character::SignalEquipmentRemoval (int, citem *Item) {
  CalculateEquipmentState();
  if (CalculateAttributeBonuses()) {
    CheckDeath(CONST_S("lost ") + GetPossessivePronoun(false) +
               " vital " + Item->GetName(INDEFINITE));
  }
}


//==========================================================================
//
//  character::CalculateEquipmentState
//
//==========================================================================
void character::CalculateEquipmentState () {
  sLong Back = EquipmentState;
  EquipmentState = 0;
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment && Equipment->IsInCorrectSlot(c)) EquipmentState |= Equipment->GetGearStates();
  }
  for (int c = 0; c < STATES; ++c) {
    if (Back & (1 << c) && !StateIsActivated(1 << c)) {
      if (StateData[c].EndHandler) {
        (this->*StateData[c].EndHandler)();
        if (!IsEnabled()) return;
      }
      if (!IsInNoMsgMode()) (this->*StateData[c].PrintEndMessage)();
    }
  }
}


//==========================================================================
//
//  character::BeginTemporaryState
//
//  Counter = duration in ticks
//
//==========================================================================
void character::BeginTemporaryState (sLong State, int Counter) {
  if (!Counter) return;
  if (State & POLYMORPHED) ABORT("No Polymorphing with BeginTemporaryState!");
  while (State != 0) {
    sLong st = 0, sidx;
    for (sidx = 0; sidx < STATES; ++sidx) {
      if (State&(1<<sidx)) {
        st = (1<<sidx);
        break;
      }
    }
    if (!st) {
      break; /*ABORT("BeginTemporaryState works only when State == 2^n!");*/
    }
    State &= ~st;
    if (TemporaryStateIsActivated(st)) {
      int OldCounter = GetTemporaryStateCounter(st);
      if (OldCounter != PERMANENT) {
        EditTemporaryStateCounter(st, Max(Counter, 50-OldCounter));
      }
    } else if (StateData[sidx].IsAllowed == 0 || (this->*StateData[sidx].IsAllowed)()) {
      SetTemporaryStateCounter(st, Max(Counter, 50));
      if (!EquipmentStateIsActivated(st)) {
        if (!IsInNoMsgMode()) (this->*StateData[sidx].PrintBeginMessage)();
        ActivateTemporaryState(st);
        if (StateData[sidx].BeginHandler) (this->*StateData[sidx].BeginHandler)();
      } else {
        ActivateTemporaryState(st);
      }
    }
  }
}


//==========================================================================
//
//  character::HandleStates
//
//==========================================================================
void character::HandleStates () {
  if (!TemporaryState && !EquipmentState) return;
  for (int c = 0; c < STATES; ++c) {
    if (TemporaryState & (1 << c) && TemporaryStateCounter[c] != PERMANENT) {
      if (!--TemporaryStateCounter[c]) {
        TemporaryState &= ~(1 << c);
        if (!(EquipmentState & (1 << c))) {
          if (StateData[c].EndHandler) {
            (this->*StateData[c].EndHandler)();
            if (!IsEnabled()) return;
          }
          if (!TemporaryStateCounter[c]) (this->*StateData[c].PrintEndMessage)();
        }
      }
    }
    if (StateIsActivated(1 << c)) {
      if (StateData[c].Handler) (this->*StateData[c].Handler)();
    }
    if (!IsEnabled()) return;
  }
}


//==========================================================================
//
//  character::PrintBeginPolymorphControlMessage
//
//==========================================================================
void character::PrintBeginPolymorphControlMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel your mind has total control over your body.");
}


//==========================================================================
//
//  character::PrintEndPolymorphControlMessage
//
//==========================================================================
void character::PrintEndPolymorphControlMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You are somehow uncertain of your willpower.");
}


//==========================================================================
//
//  character::PrintBeginLifeSaveMessage
//
//==========================================================================
void character::PrintBeginLifeSaveMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You hear Hell's gates being locked just now.");
}


//==========================================================================
//
//  character::PrintEndLifeSaveMessage
//
//==========================================================================
void character::PrintEndLifeSaveMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel the Afterlife is welcoming you once again.");
}


//==========================================================================
//
//  character::PrintBeginLycanthropyMessage
//
//==========================================================================
void character::PrintBeginLycanthropyMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You suddenly notice you've always loved full moons.");
}


//==========================================================================
//
//  character::PrintEndLycanthropyMessage
//
//==========================================================================
void character::PrintEndLycanthropyMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel the wolf inside you has had enough of your bad habits.");
}


//==========================================================================
//
//  character::PrintBeginVampirismMessage
//
//==========================================================================
void character::PrintBeginVampirismMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You suddenly decide you have always hated garlic.");
}


//==========================================================================
//
//  character::PrintEndVampirismMessage
//
//==========================================================================
void character::PrintEndVampirismMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You recall your delight of the morning sunshine back in New Attnam. You are a vampire no longer.");
}


//==========================================================================
//
//  character::PrintBeginInvisibilityMessage
//
//==========================================================================
void character::PrintBeginInvisibilityMessage () const {
  if ((PLAYER->StateIsActivated(INFRA_VISION) && IsWarm()) ||
      (PLAYER->StateIsActivated(ESP) && GetAttribute(INTELLIGENCE) >= 5))
  {
    if (IsPlayer()) ADD_MESSAGE("You seem somehow transparent.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s seems somehow transparent.", CHAR_NAME(DEFINITE));
  } else {
    if (IsPlayer()) ADD_MESSAGE("You fade away.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s disappears!", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::PrintEndInvisibilityMessage
//
//==========================================================================
void character::PrintEndInvisibilityMessage () const {
  if ((PLAYER->StateIsActivated(INFRA_VISION) && IsWarm()) ||
      (PLAYER->StateIsActivated(ESP) && GetAttribute(INTELLIGENCE) >= 5))
  {
    if (IsPlayer()) ADD_MESSAGE("Your notice your transparency has ended.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("The appearance of %s seems far more solid now.", CHAR_NAME(INDEFINITE));
  } else {
    if (IsPlayer()) ADD_MESSAGE("You reappear.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("Suddenly %s appears from nowhere!", CHAR_NAME(INDEFINITE));
  }
}


//==========================================================================
//
//  character::PrintBeginInfraVisionMessage
//
//==========================================================================
void character::PrintBeginInfraVisionMessage () const {
  if (IsPlayer()) {
    if (StateIsActivated(INVISIBLE) && IsWarm() &&
        !(StateIsActivated(ESP) && GetAttribute(INTELLIGENCE) >= 5))
    {
      ADD_MESSAGE("You reappear.");
    } else {
      ADD_MESSAGE("You feel your perception being magically altered.");
    }
  }
}


//==========================================================================
//
//  character::PrintEndInfraVisionMessage
//
//==========================================================================
void character::PrintEndInfraVisionMessage () const {
  if (IsPlayer()) {
    if (StateIsActivated(INVISIBLE) && IsWarm() &&
        !(StateIsActivated(ESP) && GetAttribute(INTELLIGENCE) >= 5))
    {
      ADD_MESSAGE("You disappear.");
    } else {
      ADD_MESSAGE("You feel your perception returning to normal.");
    }
  }
}


//==========================================================================
//
//  character::PrintBeginESPMessage
//
//==========================================================================
void character::PrintBeginESPMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You suddenly feel like being only a tiny part of a great network of intelligent minds.");
}


//==========================================================================
//
//  character::PrintEndESPMessage
//
//==========================================================================
void character::PrintEndESPMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You are filled with desire to be just yourself from now on.");
}


//==========================================================================
//
//  character::PrintBeginHasteMessage
//
//==========================================================================
void character::PrintBeginHasteMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Time slows down to a crawl.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s looks faster!", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::PrintEndHasteMessage
//
//==========================================================================
void character::PrintEndHasteMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Everything seems to move much faster now.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s looks slower!", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::PrintBeginSlowMessage
//
//==========================================================================
void character::PrintBeginSlowMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Everything seems to move much faster now.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s looks slower!", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::PrintEndSlowMessage
//
//==========================================================================
void character::PrintEndSlowMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Time slows down to a crawl.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s looks faster!", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::EndPolymorph
//
//==========================================================================
void character::EndPolymorph () {
  ForceEndPolymorph();
}


//==========================================================================
//
//  character::ForceEndPolymorph
//
//==========================================================================
character *character::ForceEndPolymorph () {
  if (IsPlayer()) {
    ADD_MESSAGE("You return to your true form.");
  } else if (game::IsInWilderness()) {
    ActivateTemporaryState(POLYMORPHED);
    SetTemporaryStateCounter(POLYMORPHED, 10);
    return this; // fast gum solution, state ends when the player enters a dungeon
  }
  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s returns to %s true form.",
                CHAR_NAME(DEFINITE), GetPossessivePronoun().CStr());
  }
  RemoveTraps();
  if (GetAction()) GetAction()->Terminate(false);
  v2 Pos = GetPos();
  SendToHell();
  Remove();
  character *Char = GetPolymorphBackup();
  Flags |= C_IN_NO_MSG_MODE;
  Char->Flags |= C_IN_NO_MSG_MODE;
  Char->PutToOrNear(Pos);
  Char->ChangeTeam(GetTeam());
  if (GetTeam()->GetLeader() == this) GetTeam()->SetLeader(Char);
  SetPolymorphBackup(0);
  Char->Enable();
  Char->Flags &= ~C_POLYMORPHED;
  GetStack()->MoveItemsTo(Char->GetStack());
  DonateEquipmentTo(Char);
  Char->SetMoney(GetMoney());
  Flags &= ~C_IN_NO_MSG_MODE;
  Char->Flags &= ~C_IN_NO_MSG_MODE;
  Char->CalculateAll();
  Char->SetAssignedName(GetAssignedName());
  if (IsPlayer()) {
    Flags &= ~C_PLAYER;
    game::SetPlayer(Char);
    game::SendLOSUpdateRequest();
    UpdateESPLOS();
  }
  Char->TestWalkability();
  return Char;
}


//==========================================================================
//
//  character::LycanthropyHandler
//
//==========================================================================
void character::LycanthropyHandler () {
  if (StateIsActivated(POLYMORPH_LOCK)) return;
  if (IsOfType("werewolfwolf")) return;
  if (!RAND_N(2000)) {
    if (StateIsActivated(POLYMORPH_CONTROL)) {
      // assume that other characters do not want to became werewolves
      if (!IsPlayer() || !game::TruthQuestion(CONST_S("Do you wish to change into a werewolf?"))) {
        return;
      }
    }
    Polymorph(werewolfwolf::Spawn(), 1000 + RAND_N(2000));
  }
}


//==========================================================================
//
//  character::SaveLife
//
//==========================================================================
void character::SaveLife () {
  if (TemporaryStateIsActivated(LIFE_SAVED)) {
    if (IsPlayer()) {
      ADD_MESSAGE("But wait! You glow briefly red and seem to be in a better shape!");
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("But wait, suddenly %s glows briefly red and seems to be in a better shape!",
                  GetPersonalPronoun().CStr());
    }
    DeActivateTemporaryState(LIFE_SAVED);
  } else {
    item *LifeSaver = 0;
    for (int c = 0; c < GetEquipments(); ++c) {
      item *Equipment = GetEquipment(c);
      if (Equipment && Equipment->IsInCorrectSlot(c) && Equipment->GetGearStates() & LIFE_SAVED) LifeSaver = Equipment;
    }
    if (!LifeSaver) ABORT("The Universe can only kill you once!");
    if (IsPlayer()) {
      ADD_MESSAGE("But wait! Your %s glows briefly red and disappears and you seem "
                  "to be in a better shape!",
                  LifeSaver->CHAR_NAME(UNARTICLED));
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("But wait, suddenly %s %s glows briefly red and disappears "
                  "and %s seems to be in a better shape!",
                  GetPossessivePronoun().CStr(), LifeSaver->CHAR_NAME(UNARTICLED),
                  GetPersonalPronoun().CStr());
    }
    LifeSaver->RemoveFromSlot();
    LifeSaver->SendToHell();
  }

  if (IsPlayer()) {
    game::AskForEscPress(CONST_S("Life saved!"));
  }

  RestoreBodyParts();
  ResetSpoiling();
  RestoreHP();
  RestoreStamina();
  ResetStates();

  if (GetNP() < SATIATED_LEVEL) {
    SetNP(SATIATED_LEVEL);
  }

  SendNewDrawRequest();

  if (GetAction()) {
    GetAction()->Terminate(false);
  }
}


//==========================================================================
//
//  character::PolymorphRandomly
//
//  returned value is never used; it is a success flag, actually
//
//==========================================================================
character *character::PolymorphRandomly (int MinDanger, int MaxDanger, int Time) {
  character *NewForm = 0;
  if (StateIsActivated(POLYMORPH_LOCK)) {
    ADD_MESSAGE("You feel uncertain about your body for a moment.");
    return NewForm;
  }
  if (StateIsActivated(POLYMORPH_CONTROL)) {
    if (IsPlayer()) {
      if (!GetNewFormForPolymorphWithControl(NewForm)) {
        return 0;
      }
    } else {
      NewForm = protosystem::CreateMonster(MinDanger*10, MaxDanger*10, NO_EQUIPMENT);
    }
  } else {
    NewForm = protosystem::CreateMonster(MinDanger, MaxDanger, NO_EQUIPMENT);
  }
  if (Polymorph(NewForm, Time)) {
    return NewForm;
  }
  return 0;
}


//==========================================================================
//
//  character::StartReading
//
//  In reality, the reading takes Time / (Intelligence * 10) turns
//
//==========================================================================
void character::StartReading (item *Item, sLong Time) {
  study *Read = study::Spawn(this);
  Read->SetLiteratureID(Item->GetID());
  //!!if (game::WizardModeIsActive()) Time = 1;
  Read->SetCounter(Time);
  SetAction(Read);
  if (IsPlayer()) {
    ADD_MESSAGE("You start reading %s.", Item->CHAR_NAME(DEFINITE));
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s starts reading %s.", CHAR_NAME(DEFINITE), Item->CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::DexterityAction
//
//  Call when one makes something with his/her/its hands.
//  Difficulty of 5 takes about one turn, so it's the most common to use.
//
//==========================================================================
void character::DexterityAction (int Difficulty) {
  EditAP(-20000 * Difficulty / APBonus(GetAttribute(DEXTERITY)));
  EditExperience(DEXTERITY, Difficulty * 15, 1 << 7);
}


//==========================================================================
//
//  character::CanBeSeenWithESPBy
//
//==========================================================================
truth character::CanBeSeenWithESPBy (ccharacter *Char) const {
  if (!Char || !Char->IsEnabled() || !IsEnabled()) return false;
  if (GetAttribute(INTELLIGENCE) < 5) return false; // too dumb, has no working brains
  if (!Char->StateIsActivated(ESP)) return false;
  if (IsESPBlockedByEquipment() || Char->IsESPBlockedByEquipment()) return false;
  return true;
}


//==========================================================================
//
//  character::CanBeSeenWithInfraBy
//
//==========================================================================
truth character::CanBeSeenWithInfraBy (ccharacter *Char) const {
  if (!Char || !Char->IsEnabled() || !IsEnabled()) return false;
  if (!IsWarm()) return false; // no blood, or cold-blooded
  if (!Char->StateIsActivated(INFRA_VISION)) return false;
  return true;
}


//==========================================================================
//
//  character::CanBeSeenBy
//
//  If Theoretically != false, range is not a factor.
//
//==========================================================================
truth character::CanBeSeenBy (ccharacter *Who, truth Theoretically, truth IgnoreESP) const {
  if (Who && IsEnabled() && !game::IsGenerating() && Who->IsEnabled() &&
      (Theoretically || GetSquareUnder()))
  {
    if (!StateIsActivated(INVISIBLE)) {
      if (Theoretically || game::IsInWilderness()) return true;
      if (Who->IsPlayer()) {
        return SquareUnderCanBeSeenByPlayer(/*CanBeSeenWithInfraBy(Who)*/
                                            IsWarm() && Who->StateIsActivated(INFRA_VISION));
      } else {
        return SquareUnderCanBeSeenBy(Who, /*CanBeSeenWithInfraBy(Who)*/
                                           IsWarm() && Who->StateIsActivated(INFRA_VISION));
      }
    } else {
      // invisible, check states
      // ESP
      if (!IgnoreESP && /*CanBeSeenWithESPBy(Who)*/
          // inlined
          Who->GetAttribute(INTELLIGENCE) >= 5 && /* otherwised is too dumb, has no working brains */
          Who->StateIsActivated(ESP) &&
          !IsESPBlockedByEquipment() &&
          !Who->IsESPBlockedByEquipment())
      {
        // can be seen with ESP
        if (Theoretically || game::IsInWilderness()) return true;
        if (GetDistanceSquareFrom(Who) <= Who->GetESPRangeSquare()) return true;
        // ESP failed
      }
      // infravision
      //if (CanBeSeenWithInfraBy(Who))
      // inlined
      if (IsWarm() && Who->StateIsActivated(INFRA_VISION))
      {
        if (Theoretically || game::IsInWilderness()) return true;
        if (Who->IsPlayer()) {
          if (SquareUnderCanBeSeenByPlayer(true)) return true;
        } else {
          if (SquareUnderCanBeSeenBy(Who, true)) return true;
        }
        // infravision failed
      }
      // invisible, and cannot be seen with ESP & infravision
      return false;
    }
  }

  return false;
}


//==========================================================================
//
//  character::SquareUnderCanBeSeenByPlayer
//
//==========================================================================
truth character::SquareUnderCanBeSeenByPlayer (truth IgnoreDarkness) const {
  if (!GetSquareUnder()) return false;
  int S1 = SquaresUnder, S2 = PLAYER->SquaresUnder;
  if (S1 == 1 && S2 == 1) {
    if (GetSquareUnder()->CanBeSeenByPlayer(IgnoreDarkness)) return true;
    if (IgnoreDarkness) {
      int LOSRangeSquare = PLAYER->GetLOSRangeSquare();
      if ((GetPos() - PLAYER->GetPos()).GetLengthSquare() <= LOSRangeSquare) {
        eyecontroller::Map = GetLevel()->GetMap();
        return mapmath<eyecontroller>::DoLine(PLAYER->GetPos().X, PLAYER->GetPos().Y,
                                              GetPos().X, GetPos().Y, SKIP_FIRST|LINE_BOTH_DIRS);
      }
    }
    return false;
  } else {
    for (int c1 = 0; c1 < S1; ++c1) {
      lsquare *Square = GetLSquareUnder(c1);
      if (Square->CanBeSeenByPlayer(IgnoreDarkness)) return true;
      else if (IgnoreDarkness) {
        v2 Pos = Square->GetPos();
        int LOSRangeSquare = PLAYER->GetLOSRangeSquare();
        for (int c2 = 0; c2 < S2; ++c2) {
          v2 PlayerPos = PLAYER->GetPos(c2);
          if ((Pos-PlayerPos).GetLengthSquare() <= LOSRangeSquare) {
            eyecontroller::Map = GetLevel()->GetMap();
            if (mapmath<eyecontroller>::DoLine(PlayerPos.X, PlayerPos.Y, Pos.X, Pos.Y, SKIP_FIRST|LINE_BOTH_DIRS)) return true;
          }
        }
      }
    }
    return false;
  }
}


//==========================================================================
//
//  character::SquareUnderCanBeSeenBy
//
//==========================================================================
truth character::SquareUnderCanBeSeenBy (ccharacter *Who, truth IgnoreDarkness) const {
  int S1 = SquaresUnder, S2 = Who->SquaresUnder;
  int LOSRangeSquare = Who->GetLOSRangeSquare();
  if (S1 == 1 && S2 == 1) {
    return GetSquareUnder()->CanBeSeenFrom(Who->GetPos(), LOSRangeSquare, IgnoreDarkness);
    //return GetSquareUnder()->CanBeSeenBy(Who, IgnoreDarkness);
  }
  for (int c1 = 0; c1 < S1; ++c1) {
    lsquare *Square = GetLSquareUnder(c1);
    for (int c2 = 0; c2 < S2; ++c2) {
      if (Square->CanBeSeenFrom(Who->GetPos(c2), LOSRangeSquare, IgnoreDarkness)) return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::GetDistanceSquareFrom
//
//==========================================================================
int character::GetDistanceSquareFrom (ccharacter *Who) const {
  int S1 = SquaresUnder, S2 = Who->SquaresUnder;
  if (S1 == 1 && S2 == 1) return (GetPos() - Who->GetPos()).GetLengthSquare();
  v2 MinDist(0x7FFF, 0x7FFF);
  int MinLength = 0xFFFF;
  for (int c1 = 0; c1 < S1; ++c1) {
    for (int c2 = 0; c2 < S2; ++c2) {
      v2 Dist = GetPos(c1)-Who->GetPos(c2);
      if (Dist.X < 0) Dist.X = -Dist.X;
      if (Dist.Y < 0) Dist.Y = -Dist.Y;
      if (Dist.X <= MinDist.X && Dist.Y <= MinDist.Y) {
        MinDist = Dist;
        MinLength = Dist.GetLengthSquare();
      } else if (Dist.X < MinDist.X || Dist.Y < MinDist.Y) {
        int Length = Dist.GetLengthSquare();
        if (Length < MinLength) {
          MinDist = Dist;
          MinLength = Length;
        }
      }
    }
  }
  return MinLength;
}


//==========================================================================
//
//  character::AttachBodyPart
//
//==========================================================================
void character::AttachBodyPart (bodypart *BodyPart) {
  SetBodyPart(BodyPart->GetBodyPartIndex(), BodyPart);
  if (!AllowSpoil()) BodyPart->ResetSpoiling();
  BodyPart->ResetPosition();
  BodyPart->UpdatePictures();
  CalculateAttributeBonuses();
  CalculateBattleInfo();
  SendNewDrawRequest();
  SignalPossibleTransparencyChange();
}


//==========================================================================
//
//  character::HasAllBodyParts
//
//  Returns true if the character has all bodyparts, false if not.
//
//==========================================================================
truth character::HasAllBodyParts () const {
  for (int c = 0; c < BodyParts; ++c) if (!GetBodyPart(c) && CanCreateBodyPart(c)) return false;
  return true;
}


//==========================================================================
//
//  character::GenerateRandomBodyPart
//
//==========================================================================
bodypart *character::GenerateRandomBodyPart () {
  int NeededBodyPart[MAX_BODYPARTS];
  int Index = 0;
  for (int c = 0; c < BodyParts; ++c) if (!GetBodyPart(c) && CanCreateBodyPart(c)) NeededBodyPart[Index++] = c;
  return Index ? CreateBodyPart(NeededBodyPart[RAND_N(Index)]) : 0;
}


//==========================================================================
//
//  character::FindRandomOwnBodyPart
//
//  Searches the character's Stack and if it find some bodyparts there
//  that are the character's old bodyparts returns a stackiterator to
//  one of them (choosen in random).
//  If no fitting bodyparts are found the function returns 0.
//
//==========================================================================
bodypart *character::FindRandomOwnBodyPart (truth AllowNonLiving) const {
  itemvector LostAndFound;
  for (int c = 0; c < BodyParts; ++c) {
    if (!GetBodyPart(c)) {
      for (std::list<feuLong>::iterator i = OriginalBodyPartID[c].begin(); i != OriginalBodyPartID[c].end(); ++i) {
        bodypart *Found = static_cast<bodypart *>(SearchForItem(*i));
        if (Found && (AllowNonLiving || Found->CanRegenerate())) LostAndFound.push_back(Found);
      }
    }
  }
  if (LostAndFound.empty()) return 0;
  return static_cast<bodypart *>(LostAndFound[RAND_N((int)LostAndFound.size())]);
}


//==========================================================================
//
//  character::PrintBeginPoisonedMessage
//
//==========================================================================
void character::PrintBeginPoisonedMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You seem to be very ill.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s looks very ill.", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::PrintEndPoisonedMessage
//
//==========================================================================
void character::PrintEndPoisonedMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel better again.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s looks better.", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::PoisonedHandler
//
//==========================================================================
void character::PoisonedHandler () {
  if (!RAND_N(100)) VomitAtRandomDirection(500 + RAND_N(250));
  int Damage = 0;
  for (int Used = 0; Used < GetTemporaryStateCounter(POISONED); Used += 100) {
    if (!RAND_N(100)) {
      Damage += 1;
    }
  }
  if (Damage) {
    ReceiveDamage(0, Damage, POISON, ALL, 8, false, false, false, false);
    CheckDeath(CONST_S("died of acute poisoning"), 0);
  }
}


//==========================================================================
//
//  character::IsWarm
//
//==========================================================================
truth character::IsWarm () const {
  return combinebodypartpredicates()(this, &bodypart::IsWarm, 1);
}


//==========================================================================
//
//  character::IsWarmBlooded
//
//==========================================================================
truth character::IsWarmBlooded () const {
  return combinebodypartpredicates()(this, &bodypart::IsWarmBlooded, 1);
}


//==========================================================================
//
//  character::BeginInvisibility
//
//==========================================================================
void character::BeginInvisibility () {
  UpdatePictures();
  SendNewDrawRequest();
  SignalPossibleTransparencyChange();
}


//==========================================================================
//
//  character::BeginInfraVision
//
//==========================================================================
void character::BeginInfraVision () {
  if (IsPlayer()) GetArea()->SendNewDrawRequest();
}


//==========================================================================
//
//  character::BeginESP
//
//==========================================================================
void character::BeginESP () {
  if (IsPlayer()) GetArea()->SendNewDrawRequest();
}


//==========================================================================
//
//  character::EndInvisibility
//
//==========================================================================
void character::EndInvisibility () {
  UpdatePictures();
  SendNewDrawRequest();
  SignalPossibleTransparencyChange();
}


//==========================================================================
//
//  character::EndInfraVision
//
//==========================================================================
void character::EndInfraVision () {
  if (IsPlayer() && IsEnabled()) GetArea()->SendNewDrawRequest();
}


//==========================================================================
//
//  character::EndESP
//
//==========================================================================
void character::EndESP () {
  if (IsPlayer() && IsEnabled()) GetArea()->SendNewDrawRequest();
}


//==========================================================================
//
//  character::Draw
//
//==========================================================================
void character::Draw (blitdata &BlitData, col16 MonoColor) const {
  col24 L = BlitData.Luminance;

  if (PLAYER->IsEnabled() && (CanBeSeenWithESPBy(PLAYER) || CanBeSeenWithInfraBy(PLAYER))) {
    BlitData.Luminance = ivanconfig::GetContrastLuminance();
  }

  const bool onboat = IsPlayerOnBoat();

  if (!onboat) {
    DrawBodyParts(BlitData, MonoColor);
  }

  BlitData.Luminance = ivanconfig::GetContrastLuminance();
  cint SquareIndex = BlitData.CustomData & SQUARE_INDEX_MASK;

  if (onboat) {
    // draw ship icon
    BlitData.Src.X = 128;
    BlitData.Src.Y = 240; //256
    igraph::GetHumanoidGraphic()->LuminanceMaskedBlit(BlitData);
  }

  BlitData.Src.Y = 16;

  if (GetTeam() == PLAYER->GetTeam() && !IsPlayer() && SquareIndex == GetTameSymbolSquareIndex()) {
    BlitData.Src.X = 32;
    igraph::GetSymbolGraphic()->LuminanceMaskedBlit(BlitData);
  }

  if (IsFlying() && SquareIndex == GetFlySymbolSquareIndex()) {
    BlitData.Src.X = 128;
    igraph::GetSymbolGraphic()->LuminanceMaskedBlit(BlitData);
  }

  if (!onboat && IsSwimming() && SquareIndex == GetSwimmingSymbolSquareIndex()) {
    BlitData.Src.X = 240;
    igraph::GetSymbolGraphic()->LuminanceMaskedBlit(BlitData);
  }

  if (GetAction() && GetAction()->IsUnconsciousness() && SquareIndex == GetUnconsciousSymbolSquareIndex()) {
    BlitData.Src.X = 224;
    igraph::GetSymbolGraphic()->LuminanceMaskedBlit(BlitData);
  }

  BlitData.Src.X = BlitData.Src.Y = 0;
  BlitData.Luminance = L;
}


//==========================================================================
//
//  character::DrawBodyParts
//
//==========================================================================
void character::DrawBodyParts (blitdata &BlitData, col16 MonoColor) const {
  GetTorso()->Draw(BlitData, MonoColor);
}


//==========================================================================
//
//  character::PrintBeginTeleportMessage
//
//==========================================================================
void character::PrintBeginTeleportMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel jumpy.");
}


//==========================================================================
//
//  character::PrintEndTeleportMessage
//
//==========================================================================
void character::PrintEndTeleportMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You suddenly realize you've always preferred walking to jumping.");
}


//==========================================================================
//
//  character::PrintBeginDetectMessage
//
//==========================================================================
void character::PrintBeginDetectMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel curious about your surroundings.");
}


//==========================================================================
//
//  character::PrintEndDetectMessage
//
//==========================================================================
void character::PrintEndDetectMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You decide to rely on your intuition from now on.");
}


//==========================================================================
//
//  character::TeleportHandler
//
//==========================================================================
void character::TeleportHandler () {
  if (!RAND_N(1500) && !game::IsInWilderness()) {
    if (IsPlayer()) ADD_MESSAGE("You feel an urgent spatial relocation is now appropriate.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s disappears.", CHAR_NAME(DEFINITE));
    TeleportRandomly();
  }
}


//==========================================================================
//
//  character::DetectHandler
//
//==========================================================================
void character::DetectHandler () {
  if (IsPlayer()) {
    //the AI can't be asked position questions! So only the player can hav this state really :/ a bit daft of me
    if (!RAND_N(3000) && !game::IsInWilderness()) {
      ADD_MESSAGE("Your mind wanders in search of something.");
      DoDetecting(); //in fact, who knows what would happen if a dark frog had the detecting state?
    }
  }
}


//==========================================================================
//
//  character::PrintBeginPolymorphMessage
//
//==========================================================================
void character::PrintBeginPolymorphMessage () const {
  if (IsPlayer()) ADD_MESSAGE("An uncomfortable uncertainty of who you really are overwhelms you.");
}


//==========================================================================
//
//  character::PrintEndPolymorphMessage
//
//==========================================================================
void character::PrintEndPolymorphMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel you are you and no one else.");
}


//==========================================================================
//
//  character::PolymorphHandler
//
//==========================================================================
void character::PolymorphHandler () {
  if (!RAND_N(1500)) {
    PolymorphRandomly(1, 999999, 200 + RAND_N(800));
  }
}


//==========================================================================
//
//  character::PrintBeginTeleportControlMessage
//
//==========================================================================
void character::PrintBeginTeleportControlMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel very controlled.");
}


//==========================================================================
//
//  character::PrintEndTeleportControlMessage
//
//==========================================================================
void character::PrintEndTeleportControlMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel your control slipping.");
}


//==========================================================================
//
//  character::DisplayStethoscopeInfo
//
//==========================================================================
void character::DisplayStethoscopeInfo (character *) const {
  felist Info(CONST_S("Information about ") + GetDescription(DEFINITE));
  AddSpecialStethoscopeInfo(Info);
  Info.AddEntry(CONST_S("Endurance: \1W") + GetAttribute(ENDURANCE), LIGHT_GRAY);
  Info.AddEntry(CONST_S("Perception: \1W") + GetAttribute(PERCEPTION), LIGHT_GRAY);
  Info.AddEntry(CONST_S("Intelligence: \1W") + GetAttribute(INTELLIGENCE), LIGHT_GRAY);
  Info.AddEntry(CONST_S("Wisdom: \1W") + GetAttribute(WISDOM), LIGHT_GRAY);
  //Info.AddEntry(CONST_S("Willpower: \1W") + GetAttribute(WILL_POWER), LIGHT_GRAY);
  Info.AddEntry(CONST_S("Charisma: \1W") + GetAttribute(CHARISMA), LIGHT_GRAY);

  if (IsInBadCondition()) {
    Info.AddEntry(CONST_S("HP: ") + GetHP() + "/\1W" + GetMaxHP(), RED);
  } else if (GetHP() != GetMaxHP()) {
    Info.AddEntry(CONST_S("HP: \1Y") + GetHP() + "\2/\1W" + GetMaxHP(), LIGHT_GRAY);
  } else {
    Info.AddEntry(CONST_S("HP: \1W") + GetHP() + "\2/\1W" + GetMaxHP(), LIGHT_GRAY);
  }

  if (GetAction()) {
    Info.AddEntry(festring(GetAction()->GetDescription()).CapitalizeCopy(), LIGHT_GRAY);
  }

  const bool showCounters = (game::WizardModeIsActive() || DEBUG_SHOW_STETHOSCOPE_COUNTERS);
  for (int c = 0; c < STATES; ++c) {
    if (StateIsActivated(1 << c) &&
        ((1 << c) != HASTE || !StateIsActivated(SLOW)) &&
        ((1 << c) != SLOW || !StateIsActivated(HASTE)))
    {
      if (showCounters) {
        festring ss;
        ss << StateData[c].Description << " \1O[" << TemporaryStateCounter[c] << "]\2";
        Info.AddEntry(ss, LIGHT_GRAY);
      } else {
        Info.AddEntry(CONST_S(StateData[c].Description), LIGHT_GRAY);
      }
    }
  }
  switch (GetTirednessState()) {
    case FAINTING: Info.AddEntry(CONST_S("Fainting"), RED); break;
    case EXHAUSTED: Info.AddEntry(CONST_S("Exhausted"), LIGHT_GRAY); break;
  }

  if (IsPlayer() && game::PlayerHasBoat()) {
    Info.AddEntry(CONST_S("Ship Owned"), LIGHT_GRAY);
  }

  game::SetStandardListAttributes(Info);
  Info.Draw();
}


//==========================================================================
//
//  character::CanUseStethoscope
//
//==========================================================================
truth character::CanUseStethoscope (truth PrintReason) const {
  if (PrintReason) ADD_MESSAGE("This type of monster can't use a stethoscope.");
  return false;
}


//==========================================================================
//
//  character::TeleportSomePartsAway
//
//  Effect used by at least Sophos.
//  NOTICE: Doesn't check for death!
//
//==========================================================================
void character::TeleportSomePartsAway (int NumberToTeleport) {
  if (StateIsActivated(TELEPORT_LOCK)) {
    if (IsPlayer()) ADD_MESSAGE("You feel very itchy for a moment.");
    return;
  }
  for (int c = 0; c < NumberToTeleport; ++c) {
    int RandomBodyPart = GetRandomNonVitalBodyPart();
    if (RandomBodyPart == NONE_INDEX) {
      for (; c < NumberToTeleport; ++c) {
        GetTorso()->SetHP((GetTorso()->GetHP() << 2) / 5);
        sLong TorsosVolume = GetTorso()->GetMainMaterial()->GetVolume() / 10;
        if (!TorsosVolume) break;
        sLong Amount = RAND_N(TorsosVolume) + 1;
        item *Lump = GetTorso()->GetMainMaterial()->CreateNaturalForm(Amount);
        GetTorso()->GetMainMaterial()->EditVolume(-Amount);
        auto pos = GetLevel()->GetRandomSquare();
        if (pos == ERROR_V2) {
          ConLogf("oops. `character::TeleportSomePartsAway()` failed.");
          delete Lump;
        } else {
          Lump->MoveTo(GetNearLSquare(pos)->GetStack());
        }
             if (IsPlayer()) ADD_MESSAGE("Parts of you teleport away.");
        else if (CanBeSeenByPlayer()) ADD_MESSAGE("Parts of %s teleport away.", CHAR_NAME(DEFINITE));
      }
    } else {
      item *SeveredBodyPart = SevereBodyPart(RandomBodyPart);
      if (SeveredBodyPart) {
        auto pos = GetLevel()->GetRandomSquare();
        if (pos == ERROR_V2) {
          ConLogf("oops. `character::TeleportSomePartsAway()` failed.");
          //SeveredBodyPart->SendToHell(); //???
          delete SeveredBodyPart;
          if (IsPlayer()) {
            ADD_MESSAGE("Your %s disappears.", GetBodyPartName(RandomBodyPart).CStr());
          } else if (CanBeSeenByPlayer()) {
            ADD_MESSAGE("%s %s disappears.",
                        GetPossessivePronoun().CStr(), GetBodyPartName(RandomBodyPart).CStr());
          }
          return;
        }
        GetNearLSquare(pos)->AddItem(SeveredBodyPart);
        SeveredBodyPart->DropEquipment();
        if (IsPlayer()) {
          ADD_MESSAGE("Your %s teleports away.", GetBodyPartName(RandomBodyPart).CStr());
        } else if (CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s %s teleports away.",
                      GetPossessivePronoun().CStr(), GetBodyPartName(RandomBodyPart).CStr());
        }
      } else {
        if (IsPlayer()) {
          ADD_MESSAGE("Your %s disappears.", GetBodyPartName(RandomBodyPart).CStr());
        } else if (CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s %s disappears.",
                      GetPossessivePronoun().CStr(), GetBodyPartName(RandomBodyPart).CStr());
        }
      }
    }
  }
}


//==========================================================================
//
//  character::GetRandomNonVitalBodyPart
//
//  Returns an index of a random bodypart that is not vital.
//  If no non-vital bodypart is found returns NONE_INDEX.
//
//==========================================================================
int character::GetRandomNonVitalBodyPart () const {
  int OKBodyPart[MAX_BODYPARTS];
  int OKBodyParts = 0;
  for (int c = 0; c < BodyParts; ++c) {
    if (GetBodyPart(c) && !BodyPartIsVital(c)) {
      OKBodyPart[OKBodyParts++] = c;
    }
  }
  return OKBodyParts ? OKBodyPart[RAND_N(OKBodyParts)] : NONE_INDEX;
}


//==========================================================================
//
//  character::CalculateVolumeAndWeight
//
//==========================================================================
void character::CalculateVolumeAndWeight () {
  Volume = Stack->GetVolume();
  Weight = Stack->GetWeight();
  BodyVolume = 0;
  CarriedWeight = Weight;
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart) {
      BodyVolume += BodyPart->GetBodyPartVolume();
      Volume += BodyPart->GetVolume();
      CarriedWeight += BodyPart->GetCarriedWeight();
      Weight += BodyPart->GetWeight();
    }
  }
}


//==========================================================================
//
//  character::SignalVolumeAndWeightChange
//
//==========================================================================
void character::SignalVolumeAndWeightChange () {
  if (!IsInitializing()) {
    CalculateVolumeAndWeight();
    if (IsEnabled()) CalculateBurdenState();
    if (MotherEntity) MotherEntity->SignalVolumeAndWeightChange();
  }
}


//==========================================================================
//
//  character::SignalEmitationIncrease
//
//==========================================================================
void character::SignalEmitationIncrease (col24 EmitationUpdate) {
  if (game::CompareLights(EmitationUpdate, Emitation) > 0) {
    game::CombineLights(Emitation, EmitationUpdate);
    if (MotherEntity) MotherEntity->SignalEmitationIncrease(EmitationUpdate);
    else if (SquareUnder[0] && !game::IsInWilderness()) {
      for(int c = 0; c < GetSquaresUnder(); ++c) GetLSquareUnder()->SignalEmitationIncrease(EmitationUpdate);
    }
  }
}


//==========================================================================
//
//  character::SignalEmitationDecrease
//
//==========================================================================
void character::SignalEmitationDecrease (col24 EmitationUpdate) {
  if (game::CompareLights(EmitationUpdate, Emitation) >= 0 && Emitation) {
    col24 Backup = Emitation;
    CalculateEmitation();
    if (Backup != Emitation) {
      if (MotherEntity) MotherEntity->SignalEmitationDecrease(EmitationUpdate);
      else if (SquareUnder[0] && !game::IsInWilderness()) {
        for (int c = 0; c < GetSquaresUnder(); ++c) GetLSquareUnder(c)->SignalEmitationDecrease(EmitationUpdate);
      }
    }
  }
}


//==========================================================================
//
//  character::CalculateEmitation
//
//==========================================================================
void character::CalculateEmitation () {
  Emitation = GetBaseEmitation();
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart) game::CombineLights(Emitation, BodyPart->GetEmitation());
  }
  game::CombineLights(Emitation, Stack->GetEmitation());
}


//==========================================================================
//
//  character::CalculateAll
//
//==========================================================================
void character::CalculateAll () {
  Flags |= C_INITIALIZING;
  CalculateAttributeBonuses();
  CalculateVolumeAndWeight();
  CalculateEmitation();
  CalculateBodyPartMaxHPs(0);
  CalculateMaxStamina();
  CalculateBurdenState();
  CalculateBattleInfo();
  Flags &= ~C_INITIALIZING;
}


//==========================================================================
//
//  character::CalculateHP
//
//==========================================================================
void character::CalculateHP () {
  HP = sumbodypartproperties()(this, &bodypart::GetHP);
}


//==========================================================================
//
//  character::CalculateMaxHP
//
//==========================================================================
void character::CalculateMaxHP () {
  MaxHP = sumbodypartproperties()(this, &bodypart::GetMaxHP);
}


//==========================================================================
//
//  character::CalculateBodyPartMaxHPs
//
//==========================================================================
void character::CalculateBodyPartMaxHPs (feuLong Flags) {
  doforbodypartswithparam<feuLong>()(this, &bodypart::CalculateMaxHP, Flags);
  CalculateMaxHP();
  CalculateHP();
}


//==========================================================================
//
//  character::EditAttribute
//
//==========================================================================
truth character::EditAttribute (int Identifier, int Value) {
  if (Identifier == ENDURANCE && UseMaterialAttributes()) return false;
  if (RawEditAttribute(BaseExperience[Identifier], Value)) {
    if (!IsInitializing()) {
      if (Identifier == LEG_STRENGTH) CalculateBurdenState();
      else if (Identifier == ENDURANCE) CalculateBodyPartMaxHPs();
      else if (IsPlayer() && Identifier == PERCEPTION) game::SendLOSUpdateRequest();
      else if (IsPlayerKind() && (Identifier == INTELLIGENCE || Identifier == WISDOM || Identifier == CHARISMA)) UpdatePictures();
      CalculateBattleInfo();
    }
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::ActivateRandomState
//
//==========================================================================
truth character::ActivateRandomState (int Flags, int Time, sLong Seed) {
  sLong ToBeActivated;
  {
    /*
    auto saviour = femath::SeedSaviour();
    //femath::SaveSeed();
    if (Seed) femath::SetSeed(Seed);
    ToBeActivated = GetRandomState(Flags|DUR_TEMPORARY);
    //femath::LoadSeed();
    */
    if (Seed) {
      // this is used in `material::EffectXX()` only,
      // to get the same effect from the same square.
      PRNG prng((uint32_t)Seed);
      ToBeActivated = GetRandomStateWithPRNG(prng, Flags | DUR_TEMPORARY);
    } else {
      ToBeActivated = GetRandomState(Flags | DUR_TEMPORARY);
    }
  }
  if (!ToBeActivated) return false;
  BeginTemporaryState(ToBeActivated, Time);
  return true;
}


//==========================================================================
//
//  character::GainRandomIntrinsic
//
//==========================================================================
truth character::GainRandomIntrinsic (int Flags) {
  sLong ToBeActivated = GetRandomState(Flags|DUR_PERMANENT);
  if (!ToBeActivated) return false;
  GainIntrinsic(ToBeActivated);
  return true;
}


//==========================================================================
//
//  character::GetRandomStateWithPRNG
//
//==========================================================================
sLong character::GetRandomStateWithPRNG (PRNG &prng, int Flags) const {
  sLong OKStates[STATES];
  int NumberOfOKStates = 0;
  for (int c = 0; c < STATES; ++c) {
    if ((StateData[c].Flags & Flags & DUR_FLAGS) &&
        (StateData[c].Flags & Flags & SRC_FLAGS))
    {
      OKStates[NumberOfOKStates++] = 1 << c;
    }
  }
  return (NumberOfOKStates ? OKStates[I32Uniform(prng, NumberOfOKStates)] : 0);
}


//==========================================================================
//
//  character::GetRandomState
//
//  Returns 0 if state not found
//
//==========================================================================
sLong character::GetRandomState (int Flags) const {
  sLong OKStates[STATES];
  int NumberOfOKStates = 0;
  for (int c = 0; c < STATES; ++c) {
    if ((StateData[c].Flags & Flags & DUR_FLAGS) &&
        (StateData[c].Flags & Flags & SRC_FLAGS))
    {
      OKStates[NumberOfOKStates++] = 1 << c;
    }
  }
  return (NumberOfOKStates ? OKStates[RAND_N(NumberOfOKStates)] : 0);
}


//==========================================================================
//
//  character::GetTimeToDie
//
//==========================================================================
double character::GetTimeToDie (ccharacter *Enemy, int Damage, double ToHitValue,
                                truth AttackIsBlockable, truth UseMaxHP) const
{
  IvanAssert(Enemy);
  double DodgeValue = GetDodgeValue();
  if (!Enemy->CanBeSeenBy(this, true)) ToHitValue *= 2;
  if (!CanBeSeenBy(Enemy, true)) DodgeValue *= 2;
  double MinHits = 1000;
  truth First = true;
  for (int c = 0; c != BodyParts; c += 1) {
    if (BodyPartIsVital(c) && GetBodyPart(c)) {
      double Hits = GetBodyPart(c)->GetTimeToDie(Damage, ToHitValue, DodgeValue, AttackIsBlockable, UseMaxHP);
      if (First) {
        MinHits = Hits;
        First = false;
      } else {
        MinHits = 1.0 / (1.0 / MinHits + 1.0 / Hits);
      }
    }
  }
  return MinHits;
}


//==========================================================================
//
//  character::GetRelativeDanger
//
//  this uses polymorph form. it's not right, we need to skip it.
//  but not simply skip: calculate values for both forms, and use the higher one.
//
//==========================================================================
double character::GetRelativeDanger (ccharacter *Enemy, truth UseMaxHP,
                                     truth ignoreEnemyPolymorph) const
{
  double Danger = Enemy->GetTimeToKill(this, UseMaxHP) / GetTimeToKill(Enemy, UseMaxHP);
  int EnemyAP = Enemy->GetMoveAPRequirement(1);
  int ThisAP = GetMoveAPRequirement(1);
  if (EnemyAP > ThisAP) Danger *= 1.25; else if (ThisAP > EnemyAP) Danger *= 0.80;
  if (!Enemy->CanBeSeenBy(this, true)) Danger *= (Enemy->IsPlayer() ? 0.2 : 0.5);
  if (!CanBeSeenBy(Enemy, true)) Danger *= (IsPlayer() ? 5.0 : 2.0);
  if (GetAttribute(INTELLIGENCE) < 10 && !IsPlayer()) Danger *= 0.80;
  if (Enemy->GetAttribute(INTELLIGENCE) < 10 && !Enemy->IsPlayer()) Danger *= 1.25;
  Danger = Limit(Danger, 0.001, 1000.0);
  if (ignoreEnemyPolymorph && Enemy->PolymorphBackup) {
    Danger = Max(Danger, GetRelativeDanger(Enemy->PolymorphBackup, UseMaxHP, false));
  }
  return Danger;
}


//==========================================================================
//
//  character::GetBodyPartName
//
//==========================================================================
festring character::GetBodyPartName (int I, truth Articled) const {
  if (I == TORSO_INDEX) return Articled ? CONST_S("a torso") : CONST_S("torso");
  ABORT("Illegal character bodypart name request!");
  return festring();
}


//==========================================================================
//
//  character::SearchForItem
//
//==========================================================================
item *character::SearchForItem (feuLong ID) const {
  item *Equipment = findequipment<feuLong>()(this, &item::HasID, ID);
  if (Equipment) return Equipment;
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) if (i->GetID() == ID) return *i;
  return 0;
}


//==========================================================================
//
//  character::ContentsCanBeSeenBy
//
//==========================================================================
truth character::ContentsCanBeSeenBy (ccharacter *Viewer) const {
  return (Viewer == this);
}


//==========================================================================
//
//  character::HitEffect
//
//==========================================================================
truth character::HitEffect (character *Enemy, item* Weapon, v2 HitPos, int Type,
                            int BodyPartIndex, int Direction, truth BlockedByArmour,
                            truth Critical, int DoneDamage)
{
  if (Weapon) return Weapon->HitEffect(this, Enemy, HitPos, BodyPartIndex, Direction, BlockedByArmour);
  if (Type == UNARMED_ATTACK) return Enemy->SpecialUnarmedEffect(this, HitPos, BodyPartIndex, Direction, BlockedByArmour);
  if (Type == KICK_ATTACK) return Enemy->SpecialKickEffect(this, HitPos, BodyPartIndex, Direction, BlockedByArmour);
  if (Type == BITE_ATTACK) return Enemy->SpecialBiteEffect(this, HitPos, BodyPartIndex, Direction, BlockedByArmour, Critical, DoneDamage);
  return false;
}


//==========================================================================
//
//  character::WeaponSkillHit
//
//==========================================================================
void character::WeaponSkillHit (item *Weapon, int Type, int Hits) {
  int Category;
       if (Type == UNARMED_ATTACK) Category = UNARMED;
  else if (Type == WEAPON_ATTACK) { Weapon->WeaponSkillHit(Hits); return; }
  else if (Type == KICK_ATTACK) Category = KICK;
  else if (Type == BITE_ATTACK) Category = BITE;
  else if (Type == THROW_ATTACK) { if (!IsHumanoid()) return; Category = Weapon->GetWeaponCategory(); }
  else { ABORT("Illegal Type %d passed to character::WeaponSkillHit()!", Type); return; }
  if (GetCWeaponSkill(Category)->AddHit(Hits)) {
    CalculateBattleInfo();
    if (IsPlayer()) {
      GetCWeaponSkill(Category)->AddLevelUpMessage(Category);
    }
  }
}


//==========================================================================
//
//  character::Duplicate
//
//  Returns 0 if character cannot be duplicated
//
//==========================================================================
character *character::Duplicate (feuLong Flags) {
  if (!(Flags & IGNORE_PROHIBITIONS) && !CanBeCloned()) return 0;
  character *Char = GetProtoType()->Clone(this);
  if (Flags & MIRROR_IMAGE) {
    DuplicateEquipment(Char, Flags & ~IGNORE_PROHIBITIONS);
    Char->SetLifeExpectancy(Flags >> LE_BASE_SHIFT & LE_BASE_RANGE, Flags >> LE_RAND_SHIFT & LE_RAND_RANGE);
  }
  Char->CalculateAll();
  Char->CalculateEmitation();
  Char->UpdatePictures();
  Char->Flags &= ~(C_INITIALIZING|C_IN_NO_MSG_MODE);
  return Char;
}


//==========================================================================
//
//  character::TryToAddToInventory
//
//==========================================================================
truth character::TryToAddToInventory (item *Item, bool checkOnly) {
  if (!(GetBurdenState() > STRESSED) || !CanUseEquipment() || Item->GetSquaresUnder() != 1) {
    return false;
  }

  if (checkOnly) {
    room *Room = Item->GetRoom();
    if (Room && !Room->PickupItem(this, Item, 1, true)) return false;
  }

  if (!checkOnly) {
    RemoveItemFromIgnoreList(Item);
  }

  // do not pickup item which will make the monster burdened
  if (!TestForPickup(Item)) {
    AddItemToIgnoreList(Item, "cannot pickup");
    return false;
  }

  room *Room = Item->GetRoom();
  if (!Room || Room->PickupItem(this, Item, 1, checkOnly)) {
    if (!checkOnly) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s picks up %s from the ground.", CHAR_NAME(DEFINITE),
                    Item->CHAR_NAME(INDEFINITE));
      }
      Item->MoveTo(GetStack());
      DexterityAction(5);
    }
    return true;
  }

  return false;
}


//==========================================================================
//
//  character::PreCheckPickup
//
//==========================================================================
bool character::PreCheckPickup (item *Item) {
  if (!(GetBurdenState() > STRESSED) || !CanUseEquipment() || Item->GetSquaresUnder() != 1) {
    return false;
  }

  room *Room = Item->GetRoom();
  if (Room && !Room->PickupItem(this, Item, 1, true)) return false;

  // do not pickup item which will make the monster burdened
  if (!TestForPickup(Item)) return false;

  if (!CheckItemIgnoreCooldown(Item)) return false;

  return true;
}


//==========================================================================
//
//  character::PreCheckConsume
//
//==========================================================================
bool character::PreCheckConsume (item *Item) {
  if (!Item || !Item->CanBeEatenByAI(this)) return false;

  room *Room = Item->GetRoom();
  if (Room) {
    if (!Room->AllowFoodSearch()) return false;
    if (!Room->ConsumeItem(this, Item, 1, true)) return false;
  }

  //HACK: only bunnies will go for carrots
  if (Item->IsCarrot() && !IsBunny()) return false;

  if (!CheckItemIgnoreCooldown(Item)) return false;

  return true;
}


//==========================================================================
//
//  character::PreCheckEquipWield
//
//==========================================================================
bool character::PreCheckEquipWield (item *Item, int wldidx) {
  if (!Item || wldidx < 0) return false;
  if (!GetBodyPartOfEquipment(wldidx)) return false;
  if (!EquipmentIsAllowed(wldidx)) return false;
  if (BoundToUse(GetEquipment(wldidx), wldidx)) return false;
  if (!AllowEquipment(Item, wldidx)) return false;
  return true;
}


//==========================================================================
//
//  character::IsForbiddenItem
//
//  do not wear some items...
//
//==========================================================================
bool character::IsForbiddenItem (citem *Item) const {
  if (Item->IsRing(this) && GetAttribute(INTELLIGENCE) >= 14) {
    // do not wear ring of polymorph without polycontrol
    if (Item->GetConfig() == RING_OF_POLYMORPH) {
      if (!StateIsActivated(POLYMORPH_LOCK) &&
          !StateIsActivated(POLYMORPH_CONTROL))
      {
        return true;
      }
    }

    // do not wear ring of teleportation without telecontrol
    // why not? it's fun.
    /*
    if (Item->GetConfig() == RING_OF_TELEPORTATION) {
      if (!StateIsActivated(TELEPORT_LOCK) &&
          !StateIsActivated(TELEPORT_CONTROL))
      {
        return true;
      }
    }
    */

    // don't bother
    if (Item->GetConfig() == RING_OF_WORM) {
      return true;
    }
  }

  // do not go for amulet of life saving... not very IVAN-style, but...
  if (Item->IsAmulet(this) && Item->GetConfig() == AMULET_OF_LIFE_SAVING) {
    return true;
  }

  return false;
}


//==========================================================================
//
//  character::PreCheckEquip
//
//==========================================================================
bool character::PreCheckEquip (item *Item) {
  #ifdef PRE_CHECK_EQUIP_CHECK
  int doDebug = -1;
  #endif

  /* allow broken items, why not?
  // don't go for broken items
  if (Item->IsBroken()) {
    #ifdef PRE_CHECK_EQUIP_CHECK
    if (IsPet()) {
      // debug output
      if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
      if (doDebug) {
        ConLogf("char #%u:'%s:%s' rejected equipment item '%s:%s' at (%d, %d) due to being broken.",
                GetID(), GetClassID(), GetConfigName().CStr(),
                Item->GetClassID(), Item->GetConfigName().CStr(),
                Item->GetPos().X, Item->GetPos().Y);
      }
      // done debug output
    }
    #endif
    return false;
  }
  */

  if (!Item->AllowEquip() || !CanUseEquipment() ||
      GetAttribute(WISDOM) >= Item->GetWearWisdomLimit() ||
      Item->GetSquaresUnder() != 1)
  {
    #ifdef PRE_CHECK_EQUIP_CHECK
    if (IsPet()) {
      // debug output
      if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
      if (doDebug) {
        ConLogf("char #%u:'%s' rejected equipment item '%s' at (%d, %d) due to check set #0.",
                GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                Item->GetPos().X, Item->GetPos().Y);
      }
      // done debug output
    }
    #endif
    return false;
  }

  room *Room = Item->GetRoom();
  if (Room && !Room->PickupItem(this, Item, 1, true)) {
    #ifdef PRE_CHECK_EQUIP_CHECK
    if (IsPet()) {
      // debug output
      if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
      if (doDebug) {
        ConLogf("char #%u:'%s' rejected equipment item '%s' at (%d, %d) due to room.",
                GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                Item->GetPos().X, Item->GetPos().Y);
      }
      // done debug output
    }
    #endif
    return false;
  }

  int wld0 = -1, wld1 = -1;

  if (!GetWieldIndexForItem(this, Item, &wld0, &wld1)) {
    #ifdef PRE_CHECK_EQUIP_CHECK
    if (IsPet()) {
      // debug output
      if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
      if (doDebug) {
        ConLogf("char #%u:'%s' rejected equipment item '%s' at (%d, %d) due to unknown slot.",
                GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                Item->GetPos().X, Item->GetPos().Y);
      }
      // done debug output
    }
    #endif
    return false;
  }

  if (!CheckItemIgnoreCooldown(Item)) return false;

  const bool rej0 = !PreCheckEquipWield(Item, wld0);
  const bool rej1 = !PreCheckEquipWield(Item, wld1);

  if (rej0 && rej1) {
    #ifdef PRE_CHECK_EQUIP_CHECK
    if (IsPet()) {
      // debug output
      if (doDebug < 0) doDebug = (DEBUG_PICKUP_AI < 0 ? 1 : 0);
      if (doDebug) {
        ConLogf("char #%u:'%s' rejected equipment item '%s' at (%d, %d) due to wield inability.",
                GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                Item->GetPos().X, Item->GetPos().Y);
      }
      // done debug output
    }
    #endif
    return false;
  }

  if (IsForbiddenItem(Item)) return false;

  return true;
}


//==========================================================================
//
//  character::CheckWantToEquip
//
//  return new danger level (always > 0), or 0 if cannot pickup.
//  the logic is slightly different from `TryToEquip()`.
//
//==========================================================================
double character::CheckWantToEquip (item *Item, double *originalDanger) {
  if (IsPlayer() || !IsEnabled()) return 0;

  if (!Item->AllowEquip() || !CanUseEquipment() ||
      GetAttribute(WISDOM) >= Item->GetWearWisdomLimit() ||
      Item->GetSquaresUnder() != 1)
  {
    return 0;
  }

  if (IsForbiddenItem(Item)) {
    return 0;
  }

  room *Room = Item->GetRoom();
  if (Room && !Room->PickupItem(this, Item, 1, true)) {
    return 0;
  }

  double myDanger = (originalDanger ? *originalDanger : 0);
  double bestDanger = 0;

  for (int e = 0; e != GetEquipments(); e += 1) {
    if (GetBodyPartOfEquipment(e) && EquipmentIsAllowed(e)) {
      sorter Sorter = EquipmentSorter(e);
      if (AllowEquipment(Item, e) &&
          (Sorter == 0 || (Item->*Sorter)(this)) &&
          ((e != RIGHT_WIELDED_INDEX && e != LEFT_WIELDED_INDEX) ||
           Item->IsWeapon(this) || Item->IsShield(this)))
      {
        item *OldEquipment = GetEquipment(e);
        // can unequip?
        if (BoundToUse(OldEquipment, e)) continue;

        // if bound to use the item, don't bother calculating the proper danger level
        if (BoundToUse(Item, e)) {
          bestDanger = 10000.0;
          break;
        }

        // calculate it once
        if (myDanger <= 0) {
          myDanger = GetRelativeDangerMaxHPNoEnemyPoly(PLAYER);
          if (originalDanger) *originalDanger = myDanger;
          #if 0
          const int dbgFlags = DEBUG_PICKUP_AI;
          if (dbgFlags < 0 && ((-dbgFlags) & 2) != 0) {
            ConLogf("char #%u:'%s' danger level=%g (checking '%s')",
                    GetID(), DebugLogName().CStr(), myDanger,
                    Item->DebugLogName().CStr());
          }
          #endif
        }

        // use item position, because this method is called in finder AI.
        // without explicit item position use, tried items will teleport to the char. ;-)
        //FIXME: this will change item order in square stack. this is mostly
        // visual inconvenience, but i should fix it nevertheless. later.
        lsquare *LSquareUnder = Item->GetLSquareUnder();
        stack *StackUnder = LSquareUnder->GetStack();

        // calculate new danger level (relative to the player).
        // this is because PC is The Choosen One.
        msgsystem::DisableMessages();
        Flags |= C_PICTURE_UPDATES_FORBIDDEN;
        LSquareUnder->Freeze();
        StackUnder->Freeze();
        if (OldEquipment) OldEquipment->RemoveFromSlot();
        Item->RemoveFromSlot();
        SetEquipment(e, Item);
        const double NewDanger = GetRelativeDangerMaxHPNoEnemyPoly(PLAYER);
        Item->RemoveFromSlot();
        StackUnder->AddItem(Item);
        if (OldEquipment) SetEquipment(e, OldEquipment);
        msgsystem::EnableMessages();
        Flags &= ~C_PICTURE_UPDATES_FORBIDDEN;
        LSquareUnder->UnFreeze();
        StackUnder->UnFreeze();

        // check if we want to equip it.
        // conditions:
        //   if bound to use, equip unconditionally.
        //   if nothing is equipped in the slot, and not a weapon:
        //        equip if new danger is >= old danger.
        //   otherwise, equip if new danger > old danger.
        // note that we cannot skip danger calculations, because we need new danger value.
        if (NewDanger > bestDanger) {
          if (NewDanger > myDanger ||
              (!OldEquipment && NewDanger == myDanger &&
               e != RIGHT_WIELDED_INDEX && e != LEFT_WIELDED_INDEX))
          {
            #if 0
            const int dbgFlags = DEBUG_PICKUP_AI;
            if (dbgFlags < 0 && ((-dbgFlags) & 2) != 0) {
              ConLogf("  char #%u:'%s' tried equipment item '%s' at (%d, %d); "
                      "d=%g; nd=%g (%g)",
                      GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                      Item->GetPos().X, Item->GetPos().Y,
                      myDanger, NewDanger, bestDanger);
            }
            #endif
            bestDanger = NewDanger;
          } else {
            #if 0
            const int dbgFlags = DEBUG_PICKUP_AI;
            if (dbgFlags < 0 && ((-dbgFlags) & 2) != 0) {
              ConLogf("  char #%u:'%s' rejected (newdanger) item '%s' at (%d, %d); "
                      "d=%g; nd=%g (%g)",
                      GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                      Item->GetPos().X, Item->GetPos().Y,
                      myDanger, NewDanger, bestDanger);
            }
            #endif
          }
        } else {
          #if 0
          const int dbgFlags = DEBUG_PICKUP_AI;
          if (dbgFlags < 0 && ((-dbgFlags) & 2) != 0) {
            ConLogf("  char #%u:'%s' rejected (bestdanger) item '%s' at (%d, %d); "
                    "d=%g; nd=%g (%g)",
                    GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                    Item->GetPos().X, Item->GetPos().Y,
                    myDanger, NewDanger, bestDanger);
          }
          #endif
        }
      }
    }
  }

  if (bestDanger > 0) {
    const int dbgFlags = DEBUG_PICKUP_AI;
    if (dbgFlags < 0 && ((-dbgFlags) & 2) != 0) {
      ConLogf("char #%u:'%s' found good equipment item '%s' at (%d, %d); d=%g; nd=%g",
              GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
              Item->GetPos().X, Item->GetPos().Y,
              myDanger, bestDanger);
    }
    return bestDanger;
  }

  return 0;
}


//==========================================================================
//
//  character::TryToEquip
//
//==========================================================================
truth character::TryToEquip (item *Item) {
  if (IsPlayer() || !IsEnabled()) return false;

  if (!Item->AllowEquip() || !CanUseEquipment() ||
      GetAttribute(WISDOM) >= Item->GetWearWisdomLimit() ||
      Item->GetSquaresUnder() != 1)
  {
    return false;
  }

  if (IsForbiddenItem(Item)) {
    return false;
  }

  // anyway
  room *Room = Item->GetRoom();
  if (Room && !Room->PickupItem(this, Item, 1, true)) {
    // do not bother
    //AddItemToIgnoreList(Item, "bad room");
    return false;
  }

  double myDanger = 0;

  // dumb monsters will not realise that they can use the empty hand ;-)
  const bool skipFirstPass = (GetAttribute(INTELLIGENCE) < 8);

  // two passes.
  // on the first pass, try the empty equipment slot.
  // on the second pass, try any equipment slot.

  #if 0
  ConLogf("char #%u:'%s' trying to equip item '%s' at (%d, %d)",
          GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
          Item->GetPos().X, Item->GetPos().Y);
  #endif

  for (int pass = (skipFirstPass ? 1 : 0); pass <= 1; pass += 1) {
    for (int e = 0; e != GetEquipments(); e += 1) {
      if (GetBodyPartOfEquipment(e) && EquipmentIsAllowed(e)) {
        item *OldEquipment = GetEquipment(e);
        if (pass == 0 && OldEquipment) continue;
        if (BoundToUse(OldEquipment, e)) continue;
        if (!AllowEquipment(Item, e)) continue;

        sorter Sorter = EquipmentSorter(e);
        if ((Sorter == 0 || (Item->*Sorter)(this)) &&
            ((e != RIGHT_WIELDED_INDEX && e != LEFT_WIELDED_INDEX) ||
             Item->IsWeapon(this) || Item->IsShield(this)))
        {
          // use item position, because this method is called in finder AI.
          // without explicit item position use, tried items will teleport to the char. ;-)
          //FIXME: this will change item order in square stack. this is mostly
          // visual inconvenience, but i should fix it nevertheless. later.
          lsquare *LSquareUnder = Item->GetLSquareUnder();
          stack *StackUnder = LSquareUnder->GetStack();
          //lsquare *LSquareUnder = GetLSquareUnder();
          //stack *StackUnder = LSquareUnder->GetStack();

          // if bound to use, don't bother with danger levels
          bool doEquip = BoundToUse(Item, e);
          if (!doEquip) {
            // calculate new danger level (relative to the player).
            // this is because PC is The Choosen One.
            msgsystem::DisableMessages();
            Flags |= C_PICTURE_UPDATES_FORBIDDEN;
            LSquareUnder->Freeze();
            StackUnder->Freeze();
            if (myDanger <= 0) {
              myDanger = GetRelativeDangerMaxHPNoEnemyPoly(PLAYER);
            }
            if (OldEquipment) OldEquipment->RemoveFromSlot();
            Item->RemoveFromSlot();
            SetEquipment(e, Item);
            const double NewDanger = GetRelativeDangerMaxHPNoEnemyPoly(PLAYER);
            Item->RemoveFromSlot();
            StackUnder->AddItem(Item);
            if (OldEquipment) SetEquipment(e, OldEquipment);
            msgsystem::EnableMessages();
            Flags &= ~C_PICTURE_UPDATES_FORBIDDEN;
            LSquareUnder->UnFreeze();
            StackUnder->UnFreeze();

            // check if we want to equip it.
            // conditions:
            //   if bound to use, equip unconditionally.
            //   if nothing is equipped in the slot, equip if new danger is >= old danger.
            //   otherwise, equip if new danger > old danger.
            // note that we cannot skip danger calculations, because we need new danger value.
            doEquip = (NewDanger > myDanger || BoundToUse(Item, e));
            if (!doEquip && !OldEquipment && NewDanger == myDanger &&
                e != RIGHT_WIELDED_INDEX && e != LEFT_WIELDED_INDEX)
            {
              doEquip = true;
            }

            // debug output
            if (doEquip) {
              const int dbgFlags = DEBUG_PICKUP_AI;
              if (dbgFlags < 0 && ((-dbgFlags) & 4) != 0) {
                if (OldEquipment) {
                  ConLogf("char #%u:'%s' equipped item '%s' at (%d, %d); d=%g; nd=%g "
                          "instead of '%s'",
                          GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                          Item->GetPos().X, Item->GetPos().Y,
                          myDanger, NewDanger,
                          OldEquipment->DebugLogName().CStr());
                } else {
                  ConLogf("char #%u:'%s' equipped item '%s' at (%d, %d); d=%g; nd=%g",
                          GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                          Item->GetPos().X, Item->GetPos().Y,
                          myDanger, NewDanger);
                }
              }
            }
            #if 0
            else {
              ConLogf("char #%u:'%s' rejected to wield item '%s' at (%d, %d); d=%g; nd=%g",
                      GetID(), DebugLogName().CStr(), Item->DebugLogName().CStr(),
                      Item->GetPos().X, Item->GetPos().Y,
                      myDanger, NewDanger);
            }
            #endif
          }

          if (doEquip) {
            // this should never fail!
            if (Room && !Room->PickupItem(this, Item, 1)) {
              ABORT("Room->PickupItem() for item `%s` and char `%s` returned "
                    "different result from item check. This should not happen!",
                    Item->DebugLogName().CStr(), DebugLogName().CStr());
            }
            RemoveItemFromIgnoreList(Item);
            const bool dropped = OldEquipment && (!Room || Room->DropItem(this, OldEquipment, 1));
            if (dropped) {
              IvanAssert(OldEquipment);
              OldEquipment->MoveTo(StackUnder);
            }
            if (CanBeSeenByPlayer()) {
              if (dropped) {
                ADD_MESSAGE("%s drops %s %s and equips %s instead.",
                            CHAR_NAME(DEFINITE),
                            CHAR_POSSESSIVE_PRONOUN, OldEquipment->CHAR_NAME(UNARTICLED),
                            Item->CHAR_NAME(INDEFINITE));
              } else if (OldEquipment) {
                ADD_MESSAGE("%s picks up and equips %s instead of %s %s.",
                            CHAR_NAME(DEFINITE), Item->CHAR_NAME(INDEFINITE),
                            CHAR_POSSESSIVE_PRONOUN, OldEquipment->CHAR_NAME(UNARTICLED));
              } else {
                ADD_MESSAGE("%s picks up and equips %s.",
                            CHAR_NAME(DEFINITE), Item->CHAR_NAME(INDEFINITE));
              }
            }
            Item->RemoveFromSlot();
            SetEquipment(e, Item);
            DexterityAction(5);
            return true;
          }
        }
      }
    }
  }

  AddItemToIgnoreList(Item, 0/*"cannot equip"*/);
  return false;
}


//==========================================================================
//
//  character::TryToConsume
//
//==========================================================================
truth character::TryToConsume (item *Item) {
  if (!Item || !Item->CanBeEatenByAI(this)) return false;
  room *Room = Item->GetRoom();
  if (Room && !Room->ConsumeItem(this, Item, 1, true)) {
    // do not bother
    //AddItemToIgnoreList(Item, "cannot consume (room)");
    return false;
  }
  return ConsumeItem(Item, festring(Item->GetConsumeMaterial(this)->GetConsumeVerb()));
}


//==========================================================================
//
//  character::UpdateESPLOS
//
//==========================================================================
void character::UpdateESPLOS () const {
  if (StateIsActivated(ESP) && !game::IsInWilderness()) {
    for (int c = 0; c < game::GetTeams(); ++c) {
      for (std::list<character *>::const_iterator i = game::GetTeam(c)->GetMember().begin();
             i != game::GetTeam(c)->GetMember().end(); ++i)
      {
        const character *XChar = *i;
        if (XChar->IsEnabled()) {
          XChar->SendNewDrawRequest();
        }
      }
    }
  }
}


//==========================================================================
//
//  character::GetCWeaponSkillLevel
//
//==========================================================================
int character::GetCWeaponSkillLevel (citem *Item) const {
  if (Item->GetWeaponCategory() < GetAllowedWeaponSkillCategories()) {
    return GetCWeaponSkill(Item->GetWeaponCategory())->GetLevel();
  }
  return 0;
}


//==========================================================================
//
//  character::PrintBeginPanicMessage
//
//==========================================================================
void character::PrintBeginPanicMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You panic!");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s panics.", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::PrintEndPanicMessage
//
//==========================================================================
void character::PrintEndPanicMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You finally calm down.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s calms down.", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::CheckPanic
//
//==========================================================================
void character::CheckPanic (int Ticks) {
  if (GetPanicLevel() > 1 && !StateIsActivated(PANIC) && !StateIsActivated(FEARLESS) &&
      GetHP()*100 < RAND_N(GetPanicLevel()*GetMaxHP()<<1))
  {
    BeginTemporaryState(PANIC, ((Ticks * 3) >> 2) + RAND_N((Ticks >> 1) + 1)); // 25% randomness to ticks...
  }
}


//==========================================================================
//
//  character::DuplicateToNearestSquare
//
//  Returns 0 if fails else the newly created character
//
//==========================================================================
character *character::DuplicateToNearestSquare (character *Cloner, feuLong Flags) {
  character *NewlyCreated = Duplicate(Flags);
  if (!NewlyCreated) return 0;
  if (Flags & CHANGE_TEAM && Cloner) NewlyCreated->ChangeTeam(Cloner->GetTeam());
  if (!NewlyCreated->PutNear(GetPos(), true)) {
    //NewlyCreated->Disable();
    //NewlyCreated->SendToHell();
    delete NewlyCreated;
    return 0;
  }
  return NewlyCreated;
}


//==========================================================================
//
//  character::SignalSpoil
//
//==========================================================================
void character::SignalSpoil (material *m) {
  if (GetMotherEntity()) {
    GetMotherEntity()->SignalSpoil(m);
  } else {
    Disappear(0, "spoil", &item::IsVeryCloseToSpoiling);
  }
}


//==========================================================================
//
//  character::CanHeal
//
//==========================================================================
truth character::CanHeal () const {
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && BodyPart->CanRegenerate() && BodyPart->GetHP() < BodyPart->GetMaxHP()) return true;
  }
  return false;
}


//==========================================================================
//
//  character::GetRelation
//
//==========================================================================
int character::GetRelation (ccharacter *Who) const {
  return GetTeam()->GetRelation(Who->GetTeam());
}


truth (item::*AffectTest[BASE_ATTRIBUTES])() const = {
  &item::AffectsEndurance,
  &item::AffectsPerception,
  &item::AffectsIntelligence,
  &item::AffectsWisdom,
  &item::AffectsWillPower,
  &item::AffectsCharisma,
  &item::AffectsMana
};


//==========================================================================
//
//  character::CalculateAttributeBonuses
//
//  Returns nonzero if endurance has decreased and death may occur
//
//==========================================================================
truth character::CalculateAttributeBonuses () {
  doforbodyparts()(this, &bodypart::CalculateAttributeBonuses);
  int BackupBonus[BASE_ATTRIBUTES];
  int BackupCarryingBonus = CarryingBonus;
  CarryingBonus = 0;
  int c1;
  for (c1 = 0; c1 < BASE_ATTRIBUTES; ++c1) {
    BackupBonus[c1] = AttributeBonus[c1];
    AttributeBonus[c1] = 0;
  }
  for (c1 = 0; c1 < GetEquipments(); ++c1) {
    item *Equipment = GetEquipment(c1);
    if (!Equipment || !Equipment->IsInCorrectSlot(c1)) continue;
    for (int c2 = 0; c2 < BASE_ATTRIBUTES; ++c2) {
      if ((Equipment->*AffectTest[c2])()) AttributeBonus[c2] += Equipment->GetEnchantment();
    }
    if (Equipment->AffectsCarryingCapacity()) CarryingBonus += Equipment->GetCarryingBonus();
  }

  ApplySpecialAttributeBonuses();

  if (IsPlayer() && !IsInitializing() && AttributeBonus[PERCEPTION] != BackupBonus[PERCEPTION]) game::SendLOSUpdateRequest();
  if (IsPlayer() && !IsInitializing() && AttributeBonus[INTELLIGENCE] != BackupBonus[INTELLIGENCE]) UpdateESPLOS();

  if (!IsInitializing() && CarryingBonus != BackupCarryingBonus) CalculateBurdenState();

  if (!IsInitializing() && AttributeBonus[ENDURANCE] != BackupBonus[ENDURANCE]) {
    CalculateBodyPartMaxHPs();
    CalculateMaxStamina();
    return AttributeBonus[ENDURANCE] < BackupBonus[ENDURANCE];
  }

  return false;
}


//==========================================================================
//
//  character::ApplyEquipmentAttributeBonuses
//
//==========================================================================
void character::ApplyEquipmentAttributeBonuses (item *Equipment) {
  if (Equipment->AffectsEndurance()) {
    AttributeBonus[ENDURANCE] += Equipment->GetEnchantment();
    CalculateBodyPartMaxHPs();
    CalculateMaxStamina();
  }
  if (Equipment->AffectsPerception()) {
    AttributeBonus[PERCEPTION] += Equipment->GetEnchantment();
    if (IsPlayer()) game::SendLOSUpdateRequest();
  }
  if (Equipment->AffectsIntelligence()) {
    AttributeBonus[INTELLIGENCE] += Equipment->GetEnchantment();
    if (IsPlayer()) UpdateESPLOS();
  }
  if (Equipment->AffectsWisdom()) AttributeBonus[WISDOM] += Equipment->GetEnchantment();
  if (Equipment->AffectsWillPower()) AttributeBonus[WILL_POWER] += Equipment->GetEnchantment();
  if (Equipment->AffectsCharisma()) AttributeBonus[CHARISMA] += Equipment->GetEnchantment();
  if (Equipment->AffectsMana()) AttributeBonus[MANA] += Equipment->GetEnchantment();
  if (Equipment->AffectsCarryingCapacity()) {
    CarryingBonus += Equipment->GetCarryingBonus();
    CalculateBurdenState();
  }
}


//==========================================================================
//
//  character::ReceiveAntidote
//
//==========================================================================
void character::ReceiveAntidote (sLong Amount, bool forceHeal) {
  if (StateIsActivated(POISONED)) {
    if (!forceHeal && GetTemporaryStateCounter(POISONED) > Amount) {
      EditTemporaryStateCounter(POISONED, -Amount);
      Amount = 0;
    } else {
      if (IsPlayer()) {
        ADD_MESSAGE("Aaaah... You feel much better.");
      } else if (CanBeSeenByPlayer() && GetRelation(PLAYER) == FRIEND) {
        ADD_MESSAGE("%s feels much better.", CHAR_NAME(DEFINITE));
      }
      Amount -= GetTemporaryStateCounter(POISONED);
      DeActivateTemporaryState(POISONED);
    }
  }

  if (StateIsActivated(PARASITE_MIND_WORM) && (forceHeal || Amount >= 500 || RAND_N(1000) < Amount)) {
    if (IsPlayer()) {
      ADD_MESSAGE("Something in your head screeches in pain.");
    } else if (CanBeSeenByPlayer() && GetRelation(PLAYER) == FRIEND) {
      //FIXME: better message!
      ADD_MESSAGE("%s can think much better.", CHAR_NAME(DEFINITE));
    }
    DeActivateTemporaryState(PARASITE_MIND_WORM);
    Amount -= Min(500, Amount);
  }

  if (StateIsActivated(LEPROSY) && (forceHeal || Amount >= 100 || RAND_N(100) < Amount)) {
    if (IsPlayer()) {
      ADD_MESSAGE("You are not falling to pieces anymore.");
    } else if (CanBeSeenByPlayer() && GetRelation(PLAYER) == FRIEND) {
      ADD_MESSAGE("%s is not falling to pieces anymore.", CHAR_NAME(DEFINITE));
    }
    DeActivateTemporaryState(LEPROSY);
    Amount -= Min(100, Amount);
  }

  if (StateIsActivated(PARASITE_TAPE_WORM) && (forceHeal || Amount >= 100 || RAND_N(100) < Amount)) {
    if (IsPlayer()) {
      ADD_MESSAGE("Something in your belly didn't seem to like this stuff.");
    } else if (CanBeSeenByPlayer() && GetRelation(PLAYER) == FRIEND) {
      //FIXME: better message!
      ADD_MESSAGE("%s stomach is better now.", CHAR_NAME(DEFINITE));
    }
    DeActivateTemporaryState(PARASITE_TAPE_WORM);
    Amount -= Min(100, Amount);
  }

  if (StateIsActivated(LYCANTHROPY) && (forceHeal || Amount >= 300 || RAND_N(900) < Amount)) {
    if (IsPlayer()) {
      ADD_MESSAGE("You don't want to howl to the moon anymore.");
    } else if (CanBeSeenByPlayer() && GetRelation(PLAYER) == FRIEND) {
      //FIXME: better message!
      ADD_MESSAGE("%s is not roaring anymore.", CHAR_NAME(DEFINITE));
    }
    DeActivateTemporaryState(LYCANTHROPY);
    Amount -= Min(300, Amount);
  }

  if (StateIsActivated(VAMPIRISM) && (forceHeal || Amount >= 300 || RAND_N(900) < Amount)) {
    if (IsPlayer()) {
      ADD_MESSAGE("You don't want to drink blood anymore.");
    } else if (CanBeSeenByPlayer() && GetRelation(PLAYER) == FRIEND) {
      //FIXME: better message!
      ADD_MESSAGE("%s is not thristy for blood anymore.", CHAR_NAME(DEFINITE));
    }
    DeActivateTemporaryState(VAMPIRISM);
    Amount -= Min(300, Amount);
  }
}


//==========================================================================
//
//  character::AddAntidoteConsumeEndMessage
//
//==========================================================================
void character::AddAntidoteConsumeEndMessage () const {
  if (StateIsActivated(POISONED)) {
    // true only if the antidote didn't cure the poison completely
    if (IsPlayer()) {
      ADD_MESSAGE("Your body processes the poison in your veins with rapid speed.");
    } else {
      ADD_MESSAGE("%s body processes the poison in their veins with rapid speed.",
                  GetPossessivePronoun().CStr());
    }
  }
}


//==========================================================================
//
//  character::IsDead
//
//==========================================================================
truth character::IsDead () const {
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPartIsVital(c) && (!BodyPart || BodyPart->GetHP() < 1)) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::SignalSpoilLevelChange
//
//==========================================================================
void character::SignalSpoilLevelChange (material *m) {
  if (GetMotherEntity()) {
    GetMotherEntity()->SignalSpoilLevelChange(m);
  } else {
    UpdatePictures();
  }
}


//==========================================================================
//
//  character::AddOriginalBodyPartID
//
//==========================================================================
void character::AddOriginalBodyPartID (int I, feuLong What) {
  if (std::find(OriginalBodyPartID[I].begin(), OriginalBodyPartID[I].end(), What) == OriginalBodyPartID[I].end()) {
    OriginalBodyPartID[I].push_back(What);
    if (OriginalBodyPartID[I].size() > 100) OriginalBodyPartID[I].erase(OriginalBodyPartID[I].begin());
  }
}


//==========================================================================
//
//  character::AddToInventory
//
//==========================================================================
void character::AddToInventory (const fearray<itemcontentscript> &ItemArray, int SpecialFlags) {
  for (uInt c1 = 0; c1 < ItemArray.Size; ++c1) {
    if (ItemArray[c1].IsValid()) {
      const interval *TimesPtr = ItemArray[c1].GetTimes();
      int Times = TimesPtr ? TimesPtr->Randomize() : 1;
      for (int c2 = 0; c2 < Times; ++c2) {
        item *Item = ItemArray[c1].Instantiate(SpecialFlags);
        if (Item) {
          Stack->AddItem(Item);
          Item->SpecialGenerationHandler();
        }
      }
    }
  }
}


//==========================================================================
//
//  character::HasHadBodyPart
//
//==========================================================================
truth character::HasHadBodyPart (citem *Item) const {
  for (int c = 0; c < BodyParts; ++c) {
    if (std::find(OriginalBodyPartID[c].begin(), OriginalBodyPartID[c].end(), Item->GetID()) != OriginalBodyPartID[c].end()) {
      return true;
    }
  }
  return GetPolymorphBackup() && GetPolymorphBackup()->HasHadBodyPart(Item);
}


//==========================================================================
//
//  character::ProcessMessage
//
//==========================================================================
festring &character::ProcessMessage (festring &Msg) const {
  SEARCH_N_REPLACE(Msg, CONST_S("@nu"), GetName(UNARTICLED));
  SEARCH_N_REPLACE(Msg, CONST_S("@ni"), GetName(INDEFINITE));
  SEARCH_N_REPLACE(Msg, CONST_S("@nd"), GetName(DEFINITE));
  SEARCH_N_REPLACE(Msg, CONST_S("@du"), GetDescription(UNARTICLED));
  SEARCH_N_REPLACE(Msg, CONST_S("@di"), GetDescription(INDEFINITE));
  SEARCH_N_REPLACE(Msg, CONST_S("@dd"), GetDescription(DEFINITE));
  SEARCH_N_REPLACE(Msg, CONST_S("@pp"), GetPersonalPronoun());
  SEARCH_N_REPLACE(Msg, CONST_S("@sp"), GetPossessivePronoun());
  SEARCH_N_REPLACE(Msg, CONST_S("@op"), GetObjectPronoun());
  SEARCH_N_REPLACE(Msg, CONST_S("@Nu"), GetName(UNARTICLED).CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Ni"), GetName(INDEFINITE).CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Nd"), GetName(DEFINITE).CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Du"), GetDescription(UNARTICLED).CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Di"), GetDescription(INDEFINITE).CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Dd"), GetDescription(DEFINITE).CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Pp"), GetPersonalPronoun().CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Sp"), GetPossessivePronoun().CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Op"), GetObjectPronoun().CapitalizeCopy());
  SEARCH_N_REPLACE(Msg, CONST_S("@Gd"), CONST_S(GetMasterGod()->GetName()));
  return Msg;
}


//==========================================================================
//
//  character::ProcessAndAddMessage
//
//==========================================================================
void character::ProcessAndAddMessage (festring Msg) const {
  ADD_MESSAGE("%s", ProcessMessage(Msg).CStr());
}


//==========================================================================
//
//  character::BeTalkedTo
//
//==========================================================================
void character::BeTalkedTo () {
  static sLong Said;
  if (GetRelation(PLAYER) == HOSTILE) {
    ProcessAndAddMessage(GetHostileReplies()[RandomizeReply(Said, GetHostileReplies().Size)]);
  } else {
    ProcessAndAddMessage(GetFriendlyReplies()[RandomizeReply(Said, GetFriendlyReplies().Size)]);
  }
}


//==========================================================================
//
//  character::CheckZap
//
//==========================================================================
truth character::CheckZap () {
  if (!CanZap()) {
    ADD_MESSAGE("This monster type can't zap.");
    return false;
  }
  return true;
}


//==========================================================================
//
//  character::DamageAllItems
//
//==========================================================================
void character::DamageAllItems (character *Damager, int Damage, int Type) {
  GetStack()->ReceiveDamage(Damager, Damage, Type);
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment) Equipment->ReceiveDamage(Damager, Damage, Type);
  }
}


//==========================================================================
//
//  character::Equips
//
//==========================================================================
truth character::Equips (citem *Item) const {
  return combineequipmentpredicateswithparam<feuLong>()(this, &item::HasID, Item->GetID(), 1);
}


//==========================================================================
//
//  character::PrintBeginConfuseMessage
//
//==========================================================================
void character::PrintBeginConfuseMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel quite happy.");
}


//==========================================================================
//
//  character::PrintEndConfuseMessage
//
//==========================================================================
void character::PrintEndConfuseMessage () const {
  if (IsPlayer()) ADD_MESSAGE("The world is boring again.");
}


//==========================================================================
//
//  character::ApplyStateModification
//
//==========================================================================
v2 character::ApplyStateModification (v2 TryDirection) const {
  if (!StateIsActivated(CONFUSED) || RAND_16 || game::IsInWilderness()) return TryDirection;
  v2 To = GetLevel()->GetFreeAdjacentSquare(this, GetPos(), true);
  if (To == ERROR_V2) return TryDirection;
  To -= GetPos();
  if (To != TryDirection && IsPlayer()) ADD_MESSAGE("Whoa! You somehow don't manage to walk straight.");
  return To;
}


//==========================================================================
//
//  character::AddConfuseHitMessage
//
//==========================================================================
void character::AddConfuseHitMessage () const {
  if (IsPlayer()) ADD_MESSAGE("This stuff is confusing.");
}


//==========================================================================
//
//  character::SelectFromPossessions
//
//==========================================================================
item *character::SelectFromPossessions (cfestring &Topic, sorter Sorter) {
  itemvector ReturnVector;
  SelectFromPossessions(ReturnVector, Topic, NO_MULTI_SELECT | STACK_ALLOW_NAMING, Sorter);
  return !ReturnVector.empty() ? ReturnVector[0] : 0;
}


//==========================================================================
//
//  character::SelectFromPossessions
//
//==========================================================================
truth character::SelectFromPossessions (itemvector &ReturnVector, cfestring &Topic,
                                        int Flags, sorter Sorter)
{
  felist List(Topic);
  truth InventoryPossible = GetStack()->SortedItems(this, Sorter);
  if (InventoryPossible) {
    List.AddEntry(CONST_S("choose from inventory"), LIGHT_GRAY, 20,
                  game::AddToItemDrawVector(itemvector()));
  }
  truth Any = false;
  itemvector Item;
  festring Entry;
  int c;

  int maxEqNameWidth = 0;
  for (int c = 0; c < GetEquipments(); ++c) {
    festring ss(GetEquipmentName(c));
    ss << ":";
    const int ww = FONT->TextWidth(ss);
    if (maxEqNameWidth < ww) maxEqNameWidth = ww;
  }
  maxEqNameWidth += 6;

  for (c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && (Sorter == 0 || (BodyPart->*Sorter)(this))) {
      Item.push_back(BodyPart);
      Entry.Empty();
      BodyPart->AddName(Entry, UNARTICLED);
      int ImageKey = game::AddToItemDrawVector(itemvector(1, BodyPart));
      List.AddEntry(Entry, LIGHT_GRAY, 20, ImageKey, true);
      Any = true;
    }
  }

  for (c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment && (Sorter == 0 || (Equipment->*Sorter)(this))) {
      Item.push_back(Equipment);
      Entry = GetEquipmentName(c);
      Entry << ':';
      FONT->RPadToPixWidth(Entry, maxEqNameWidth); //Entry.Resize(20);
      Equipment->AddInventoryEntry(this, Entry, 1, true);
      AddSpecialEquipmentInfo(Entry, c);
      int ImageKey = game::AddToItemDrawVector(itemvector(1, Equipment));
      List.AddEntry(Entry, LIGHT_GRAY, 20, ImageKey, true);
      List.AddLastEntryHelp(Equipment->GetDescriptiveInfo());
      Any = true;
    }
  }

  if (Any) {
    game::SetStandardListAttributes(List);
    List.SetFlags(SELECTABLE|DRAW_BACKGROUND_AFTERWARDS);
    List.SetEntryDrawer(game::ItemEntryDrawer);
    game::DrawEverythingNoBlit();
    int Chosen = List.Draw();
    game::ClearItemDrawVector();
    if (Chosen != ESCAPED) {
      if ((InventoryPossible && !Chosen) || (Chosen & FELIST_ERROR_BIT)) {
        GetStack()->DrawContents(ReturnVector, this, Topic, Flags, Sorter);
      } else {
        ReturnVector.push_back(Item[InventoryPossible ? Chosen - 1 : Chosen]);
        if ((Flags & SELECT_PAIR) && ReturnVector[0]->HandleInPairs()) {
          item *PairEquipment = GetPairEquipment(ReturnVector[0]->GetEquipmentIndex());
          if (PairEquipment && PairEquipment->CanBePiledWith(ReturnVector[0], this)) ReturnVector.push_back(PairEquipment);
        }
      }
    }
  } else {
    if (!GetStack()->SortedItems(this, Sorter)) return false;
    game::ClearItemDrawVector();
    GetStack()->DrawContents(ReturnVector, this, Topic, Flags, Sorter);
  }

  return true;
}


//==========================================================================
//
//  character::EquipsSomething
//
//==========================================================================
truth character::EquipsSomething (sorter Sorter) {
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment && (Sorter == 0 || (Equipment->*Sorter)(this))) return true;
  }
  return false;
}


//==========================================================================
//
//  character::CreateBodyPartMaterial
//
//==========================================================================
material *character::CreateBodyPartMaterial (int, sLong Volume) const {
  return MAKE_MATERIAL(GetFleshMaterial(), Volume);
}


//==========================================================================
//
//  character::CheckTalk
//
//==========================================================================
truth character::CheckTalk () {
  if (!CanTalk()) {
    ADD_MESSAGE("This monster does not know the art of talking.");
    return false;
  }
  return true;
}


//==========================================================================
//
//  character::MoveTowardsHomePos
//
//==========================================================================
truth character::MoveTowardsHomePos () {
  if (HomeDataIsValid() && IsEnabled()) {
    SetGoingTo(HomeData->Pos);
    return
      MoveTowardsTarget(false) ||
      (!GetPos().IsAdjacent(HomeData->Pos) && MoveRandomly());
  }
  return false;
}


//==========================================================================
//
//  character::TryToChangeEquipment
//
//==========================================================================
int character::TryToChangeEquipment (stack *MainStack, stack *SecStack, int Chosen) {
  if (!GetBodyPartOfEquipment(Chosen)) {
    ADD_MESSAGE("Bodypart missing!");
    return 0;
  }

  item *OldEquipment = GetEquipment(Chosen);
  if (!IsPlayer() && BoundToUse(OldEquipment, Chosen)) {
    ADD_MESSAGE("%s refuses to unequip %s.",
                CHAR_DESCRIPTION(DEFINITE), OldEquipment->CHAR_NAME(DEFINITE));
    return 0;
  }

  // unequip
  if (OldEquipment) {
    OldEquipment->MoveTo(MainStack);
  }

  sorter Sorter = EquipmentSorter(Chosen);
  if (!MainStack->SortedItems(this, Sorter) && (!SecStack || !SecStack->SortedItems(this, Sorter))) {
    ADD_MESSAGE("You haven't got any item that could be used for this purpose.");
    // equip back
    if (OldEquipment) {
      OldEquipment->RemoveFromSlot();
      SetEquipment(Chosen, OldEquipment);
    }
    return 0;
  }

  game::DrawEverythingNoBlit();

  itemvector ItemVector;
  int Return = MainStack->DrawContents(ItemVector, SecStack, this,
    CONST_S("Choose ")+GetEquipmentName(Chosen)+':',
    (SecStack ? CONST_S("Items in your inventory") : CONST_S("")),
    (SecStack ? festring(CONST_S("Items in ")+GetPossessivePronoun()+" inventory") : CONST_S("")),
    (SecStack ? festring(GetDescription(DEFINITE)+" is "+GetVerbalBurdenState()) : CONST_S("")),
    GetVerbalBurdenStateColor(),
    NONE_AS_CHOICE|NO_MULTI_SELECT|SELECT_PAIR|SKIP_FIRST_IF_NO_OLD|SELECT_MOST_RECENT,
    Sorter, OldEquipment);
  if (Return == ESCAPED) {
    // equip back
    if (OldEquipment) {
      OldEquipment->RemoveFromSlot();
      SetEquipment(Chosen, OldEquipment);
    }
    return 0;
  }

  item *Item = (ItemVector.empty() ? 0 : ItemVector[0]);
  int otherChosen = -1;

  if (Item) {
    if (!IsPlayer() && !AllowEquipment(Item, Chosen)) {
      ADD_MESSAGE("%s refuses to equip %s.",
                  CHAR_DESCRIPTION(DEFINITE), Item->CHAR_NAME(DEFINITE));
      // equip back
      if (OldEquipment) {
        OldEquipment->RemoveFromSlot();
        SetEquipment(Chosen, OldEquipment);
      }
      return 0;
    }

    if (ItemVector[0]->HandleInPairs() && ItemVector.size() > 1) {
      switch (Chosen) {
        case RIGHT_GAUNTLET_INDEX: otherChosen = LEFT_GAUNTLET_INDEX; break;
        case LEFT_GAUNTLET_INDEX: otherChosen = RIGHT_GAUNTLET_INDEX; break;
        case RIGHT_BOOT_INDEX: otherChosen = LEFT_BOOT_INDEX; break;
        case LEFT_BOOT_INDEX: otherChosen = RIGHT_BOOT_INDEX; break;
        default: break;
      }
      if (otherChosen != -1) {
        if (GetBodyPartOfEquipment(otherChosen)) {
          if (!game::TruthQuestion(CONST_S("Do you want to wear both items?"), YES)) {
            otherChosen = -1;
          }
        } else {
          otherChosen = -1;
        }
      }
    }

    // wear/wield first item
    Item->RemoveFromSlot();
    SetEquipment(Chosen, Item);
    if (CheckIfEquipmentIsNotUsable(Chosen)) {
      Item->MoveTo(MainStack); Item = 0; otherChosen = -1; // small bug?
      // equip back
      if (OldEquipment) {
        OldEquipment->RemoveFromSlot();
        SetEquipment(Chosen, OldEquipment);
      }
    }

    // wear/wield possible second item
    if (Item && otherChosen != -1 && ItemVector[0]->HandleInPairs() &&
        ItemVector.size() > 1 && GetBodyPartOfEquipment(otherChosen))
    {
      item *otherOld = GetEquipment(otherChosen);
      if (otherOld && !IsPlayer() && BoundToUse(otherOld, otherChosen)) {
        ADD_MESSAGE("%s refuses to unequip %s.", CHAR_DESCRIPTION(DEFINITE), otherOld->CHAR_NAME(DEFINITE));
        otherChosen = -1;
      } else if (otherOld) {
        otherOld->MoveTo(MainStack);
      }
      if (otherChosen != -1) {
        ItemVector[1]->RemoveFromSlot();
        SetEquipment(otherChosen, ItemVector[1]);
        if (CheckIfEquipmentIsNotUsable(otherChosen)) { ItemVector[1]->MoveTo(MainStack); otherChosen = -1; } // small bug?
      }
    }
  }

  return (Item != OldEquipment ? (otherChosen != -1 ? 2 : 1) : 0);
}


//==========================================================================
//
//  character::PrintBeginParasitizedMessage
//
//==========================================================================
void character::PrintBeginParasitizedMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel you are no longer alone.");
}


//==========================================================================
//
//  character::PrintEndParasitizedMessage
//
//==========================================================================
void character::PrintEndParasitizedMessage () const {
  if (IsPlayer()) ADD_MESSAGE("A feeling of long welcome emptiness overwhelms you.");
}


//==========================================================================
//
//  character::ParasitizedHandler
//
//==========================================================================
void character::ParasitizedHandler () {
  EditNP(-5); // `-10` in commfork
  if (!RAND_N(250)) {
    if (IsPlayer()) ADD_MESSAGE("Ugh. You feel something violently carving its way through your intestines.");
    // use DRAIN here so that resistances do not apply (commfork)
    ReceiveDamage(0, 1, /*POISON*/DRAIN, TORSO, 8, false, false, false, false);
    CheckDeath(CONST_S("killed by a vile parasite"), 0);
  }
}


//==========================================================================
//
//  character::PrintBeginFastingMessage
//
//==========================================================================
void character::PrintBeginFastingMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel your life-force invigorating your entire body.");
}


//==========================================================================
//
//  character::PrintEndFastingMessage
//
//==========================================================================
void character::PrintEndFastingMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Your stomach growls in discontent.");
}


//==========================================================================
//
//  character::PrintBeginMindwormedMessage
//
//==========================================================================
void character::PrintBeginMindwormedMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You have a killer headache.");
}


//==========================================================================
//
//  character::PrintEndMindwormedMessage
//
//==========================================================================
void character::PrintEndMindwormedMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Your headache finally subsides.");
}


//==========================================================================
//
//  character::MindWormCanPenetrateSkull
//
//==========================================================================
truth character::MindWormCanPenetrateSkull (mindworm *) const {
  if (!RAND_N(2)) {
    return true;
  } else {
    return false;
  }
}


//==========================================================================
//
//  character::MindwormedHandler
//
//==========================================================================
void character::MindwormedHandler () {
  if (!RAND_N(200)) {
    if (IsPlayer()) ADD_MESSAGE("Your brain hurts!");
    BeginTemporaryState(CONFUSED, 100 + RAND_N(100));
    return;
  }

  // Multiple mind worm hatchlings can hatch, because multiple eggs could have been implanted.
  if (!game::IsInWilderness() && !RAND_N(500)) {
    character *Spawned = mindworm::Spawn(HATCHLING);
    v2 Pos = game::GetCurrentLevel()->GetNearestFreeSquare(Spawned, GetPos());
    if (Pos == ERROR_V2) {
      delete Spawned;
      return;
    }

    Spawned->SetTeam(game::GetTeam(MONSTER_TEAM));
    Spawned->PutTo(Pos);

    if (IsPlayer()) {
      ADD_MESSAGE("%s suddenly digs out of your skull.", Spawned->CHAR_NAME(INDEFINITE));
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s suddenly digs out of %s's skull.", Spawned->CHAR_NAME(INDEFINITE),
                  CHAR_NAME(DEFINITE));
    }

    ReceiveDamage(0, 1, DRAIN, HEAD, 8/*dir*/, false, false, false, false); // Use DRAIN here so that AV does not apply.
    CheckDeath(CONST_S("killed by giving birth to ") + Spawned->GetName(INDEFINITE));
  }
}


//==========================================================================
//
//  character::CanFollow
//
//==========================================================================
truth character::CanFollow () const {
  return CanMove() && !StateIsActivated(PANIC) && !IsStuck();
}


//==========================================================================
//
//  character::GetKillName
//
//==========================================================================
festring character::GetKillName () const {
  if (!GetPolymorphBackup()) return GetName(INDEFINITE);
  festring KillName;
  GetPolymorphBackup()->AddName(KillName, INDEFINITE);
  KillName << " polymorphed into ";
  id::AddName(KillName, INDEFINITE);
  return KillName;
}


//==========================================================================
//
//  character::GetPanelName
//
//==========================================================================
festring character::GetPanelName () const {
  festring Name;
  Name << AssignedName << " the " << game::GetVerbalPlayerAlignment() << ' ';
  id::AddName(Name, UNARTICLED);
  if (game::IsCheating()) {
    Name << " \1R(cheater!)\2";
  }
  return Name;
}


//==========================================================================
//
//  character::GetMoveAPRequirement
//
//==========================================================================
sLong character::GetMoveAPRequirement (int Difficulty) const {
  return (!StateIsActivated(PANIC) ? 10000000 : 8000000) * Difficulty /
            (APBonus(GetAttribute(AGILITY)) * GetMoveEase());
}


//==========================================================================
//
//  character::HealHitPoint
//
//==========================================================================
bodypart *character::HealHitPoint () {
  int NeedHeal = 0, NeedHealIndex[MAX_BODYPARTS];
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && BodyPart->CanRegenerate() && BodyPart->GetHP() < BodyPart->GetMaxHP()) {
      NeedHealIndex[NeedHeal++] = c;
    }
  }
  if (NeedHeal) {
    bodypart *BodyPart = GetBodyPart(NeedHealIndex[RAND_N(NeedHeal)]);
    BodyPart->IncreaseHP();
    ++HP;
    return BodyPart;
  }
  return 0;
}


//==========================================================================
//
//  character::CreateHomeData
//
//==========================================================================
void character::CreateHomeData () {
  HomeData = new homedata;
  lsquare *Square = GetLSquareUnder();
  HomeData->Pos = Square->GetPos();
  HomeData->Dungeon = Square->GetDungeonIndex();
  HomeData->Level = Square->GetLevelIndex();
  HomeData->Room = Square->GetRoomIndex();
}


//==========================================================================
//
//  character::GetHomeRoom
//
//==========================================================================
room *character::GetHomeRoom () const {
  if (HomeDataIsValid() && HomeData->Room) {
    return GetLevel()->GetRoom(HomeData->Room);
  }
  return 0;
}


//==========================================================================
//
//  character::RemoveHomeData
//
//==========================================================================
void character::RemoveHomeData () {
  delete HomeData;
  HomeData = 0;
}


//==========================================================================
//
//  character::AddESPConsumeMessage
//
//==========================================================================
void character::AddESPConsumeMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel a strange mental activity.");
}


//==========================================================================
//
//  character::SetBodyPart
//
//==========================================================================
void character::SetBodyPart (int I, bodypart *What) {
  BodyPartSlot[I].PutInItem(What);
  if (What) {
    What->SignalPossibleUsabilityChange();
    What->Disable();
    AddOriginalBodyPartID(I, What->GetID());
         if (What->GetMainMaterial()->IsInfectedByLeprosy()) GainIntrinsic(LEPROSY);
    else if (StateIsActivated(LEPROSY)) What->GetMainMaterial()->SetIsInfectedByLeprosy(true);
  }
}


//==========================================================================
//
//  character::ConsumeItem
//
//==========================================================================
truth character::ConsumeItem (item *Item, cfestring &ConsumeVerb, truth nibbling) {
  if (Item->IsQuestItem() /*|| !Item->IsDestroyable(this):k8:nope, cannot eat spider*/) {
    if (IsPlayer()) ADD_MESSAGE("You cannot eat that!");
    return false;
  }

  if (IsPlayer() && HasHadBodyPart(Item) &&
      !game::TruthQuestion(CONST_S("Are you sure? You may be able to put it back...")))
  {
    return false;
  }

  if (Item->IsOnGround() && Item->GetRoom() && !Item->GetRoom()->ConsumeItem(this, Item, 1)) {
    return false;
  }

  if (IsPlayer()) {
    ADD_MESSAGE("You begin %s %s.", ConsumeVerb.CStr(), Item->CHAR_NAME(DEFINITE));
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s begins %s %s.", CHAR_NAME(DEFINITE), ConsumeVerb.CStr(), Item->CHAR_NAME(DEFINITE));
  }

  consume *Consume = consume::Spawn(this);
  Consume->SetDescription(ConsumeVerb);
  Consume->SetConsumingID(Item->GetID());
  Consume->SetNibbling(nibbling);
  SetAction(Consume);
  DexterityAction(5);

  return true;
}


//==========================================================================
//
//  character::CheckThrow
//
//==========================================================================
truth character::CheckThrow () const {
  if (!CanThrow()) {
    ADD_MESSAGE("This monster type cannot throw.");
    return false;
  }
  return true;
}


//==========================================================================
//
//  character::GetHitByExplosion
//
//==========================================================================
void character::GetHitByExplosion (const explosion *Explosion, int Damage) {
  if (!Damage) return; // just in case

  int DamageDirection = (GetPos() == Explosion->Pos
                          ? RANDOM_DIR
                          : game::CalculateRoughDirection(GetPos() - Explosion->Pos));

  if (!IsPet() && Explosion->Terrorist && Explosion->Terrorist->IsPet()) {
    Explosion->Terrorist->Hostility(this);
  }

  GetTorso()->SpillBlood((8 - Explosion->Size + RAND_N(8 - Explosion->Size)) >> 1);
  if (DamageDirection == RANDOM_DIR) DamageDirection = RAND_8;

  v2 SpillPos = GetPos() + game::GetMoveVector(DamageDirection);
  if (SquareUnder[0] && GetArea()->IsValidPos(SpillPos)) {
    GetTorso()->SpillBlood((8-Explosion->Size + RAND_N(8-Explosion->Size)) >> 1, SpillPos);
  }

  if (IsPlayer()) {
    ADD_MESSAGE("You are hit by the explosion!");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s is hit by the explosion.", CHAR_NAME(DEFINITE));
  }

  // this is so `1 / 2` is `1`, not `0`
  const int halfDamage = Div2Ass(Damage);

  truth WasUnconscious = (GetAction() && GetAction()->IsUnconsciousness());
  ReceiveDamage(Explosion->Terrorist, halfDamage, FIRE, ALL, DamageDirection, true, false, false, false);

  if (IsEnabled()) {
    ReceiveDamage(Explosion->Terrorist, halfDamage, PHYSICAL_DAMAGE, ALL, DamageDirection, true, false, false, false);
    CheckDeath(Explosion->DeathMsg, Explosion->Terrorist, !WasUnconscious ? IGNORE_UNCONSCIOUSNESS : 0);
  }
}


//==========================================================================
//
//  character::SortAllItems
//
//==========================================================================
void character::SortAllItems (const sortdata &SortData) {
  GetStack()->SortAllItems(SortData);
  doforequipmentswithparam<const sortdata&>()(this, &item::SortAllItems, SortData);
}


//==========================================================================
//
//  character::PrintBeginSearchingMessage
//
//==========================================================================
void character::PrintBeginSearchingMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel you can now notice even the very smallest details around you.");
}


//==========================================================================
//
//  character::PrintEndSearchingMessage
//
//==========================================================================
void character::PrintEndSearchingMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel less perceptive.");
}


//==========================================================================
//
//  character::SearchingHandler
//
//==========================================================================
void character::SearchingHandler () {
  if (!game::IsInWilderness()) Search(15);
}


//==========================================================================
//
//  character::Search
//
//==========================================================================
void character::Search (int Perception) {
  for (int d = 0; d < GetExtendedNeighbourSquares(); ++d) {
    if (!GetSquareUnder()) {
      ConLogf("character '%s' is nowhere!", CHAR_DESCRIPTION(DEFINITE));
      return;
    }
    lsquare *LSquare = GetNeighbourLSquare(d);
    if (LSquare) LSquare->GetStack()->Search(this, Min(Perception, 200));
  }
}


//==========================================================================
//
//  character::GetRandomNeighbour
//
//  surprisingly returns 0 if fails
//
//==========================================================================
character *character::GetRandomNeighbour (int RelationFlags, bool anybody) const {
  character *Chars[MAX_NEIGHBOUR_SQUARES];
  int Index = 0;
  for (int d = 0; d < GetNeighbourSquares(); ++d) {
    lsquare *LSquare = GetNeighbourLSquare(d);
    if (LSquare) {
      character *Char = LSquare->GetCharacter();
      if (Char && (GetRelation(Char) & RelationFlags)) {
        // check if we can see invisibles
        if (anybody || Char->CanBeSeenBy(this)) {
          Chars[Index++] = Char;
        }
      }
    }
  }
  return (Index ? Chars[RAND_N(Index)] : 0);
}


//==========================================================================
//
//  character::ResetStates
//
//==========================================================================
void character::ResetStates () {
  for (int c = 0; c < STATES; ++c) {
    if (1 << c != POLYMORPHED && TemporaryStateIsActivated(1 << c) && TemporaryStateCounter[c] != PERMANENT) {
      TemporaryState &= ~(1 << c);
      if (StateData[c].EndHandler) {
        (this->*StateData[c].EndHandler)();
        if (!IsEnabled())return;
      }
    }
  }
}


//==========================================================================
//
//  character::PrintBeginGasImmunityMessage
//
//==========================================================================
void character::PrintBeginGasImmunityMessage () const {
  if (IsPlayer()) ADD_MESSAGE("All smells fade away.");
}


//==========================================================================
//
//  character::PrintEndGasImmunityMessage
//
//==========================================================================
void character::PrintEndGasImmunityMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Yuck! The world smells bad again.");
}


//==========================================================================
//
//  character::ShowAdventureInfo
//
//==========================================================================
void character::ShowAdventureInfo (cfestring &Msg) const {
  const char *lists[4][4] = {
    { "Show massacre history",
      "Show inventory",
      "Show message history",
      NULL },
    { "Show inventory",
      "Show message history",
      NULL,
      NULL },
    { "Show message history",
      NULL,
      NULL,
      NULL },
    { "Show massacre history",
      "Show message history",
      NULL,
      NULL }
  };
  // massacre, inventory, messages
  const int nums[4][3] = {
    { 0, 1, 2},
    {-1, 0, 1},
    {-1,-1, 0},
    { 0,-1, 0}
  };
  int idx = 0;
  if (GetStack()->GetItems()) {
    idx = game::MassacreListsEmpty() ? 1 : 0;
  } else {
    idx = game::MassacreListsEmpty() ? 2 : 3;
  }
  int sel = -1;
  for (;;) {
    sel = game::ListSelectorArray(sel, CONST_S("Do you want to see some funny history?"),
                                  Msg, lists[idx]);
    if (sel < 0) break;
    if (sel == nums[idx][0] && !game::MassacreListsEmpty()) {
      game::DisplayMassacreLists();
    }
    if (sel == nums[idx][1] && GetStack()->GetItems()) {
      GetStack()->DrawContents(this, CONST_S("Your inventory"), NO_SELECT);
      for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
        i->DrawContents(this);
      }
      doforequipmentswithparam<ccharacter *>()(this, &item::DrawContents, this);
    }
    if (sel == nums[idx][2]) {
      msgsystem::DrawMessageHistory();
    }
  }
}


//==========================================================================
//
//  character::SaveAdventureInfo
//
//  this should be called after adding a high-score!
//
//==========================================================================
void character::SaveAdventureInfo () const {
  if (highscore::BeginLastInfo()) {
    /*
    if (GetStack()->GetItems()) {
      // save items
      for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
        i->SaveContents(this);
      }
    }
    */
    if (!game::MassacreListsEmpty()) {
      game::SaveMassacreLists();
    }
    msgsystem::SaveLastMessages();
    highscore::EndLastInfo();
  }
}



//==========================================================================
//
//  character::EditAllAttributes
//
//==========================================================================
truth character::EditAllAttributes (int Amount) {
  if (!Amount) return true;
  int c;
  truth MayEditMore = false;
  for (c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && BodyPart->EditAllAttributes(Amount)) {
      MayEditMore = true;
    }
  }
  for (c = 0; c < BASE_ATTRIBUTES; ++c) {
    if (BaseExperience[c]) {
      BaseExperience[c] += Amount * EXP_MULTIPLIER;
      LimitRef(BaseExperience[c], MIN_EXP, MAX_EXP);
      if ((Amount < 0 && BaseExperience[c] != MIN_EXP) ||
          (Amount > 0 && BaseExperience[c] != MAX_EXP))
      {
        MayEditMore = true;
      }
    }
  }
  CalculateAll();
  RestoreHP();
  RestoreStamina();
  if (IsPlayer()) {
    game::SendLOSUpdateRequest();
    UpdateESPLOS();
  }
  if (IsPlayerKind()) {
    UpdatePictures();
  }
  return MayEditMore;
}


//==========================================================================
//
//  character::AddAttributeInfo
//
//==========================================================================
void character::AddAttributeInfo (festring &Entry) const {
  Entry.Resize(57);
  Entry << GetAttribute(ENDURANCE);
  Entry.Resize(60);
  Entry << GetAttribute(PERCEPTION);
  Entry.Resize(63);
  Entry << GetAttribute(INTELLIGENCE);
  Entry.Resize(66);
  Entry << GetAttribute(WISDOM);
  Entry.Resize(69);
  Entry << GetAttribute(CHARISMA);
  Entry.Resize(72);
  Entry << GetAttribute(MANA);
}


//==========================================================================
//
//  character::AddDefenceInfo
//
//==========================================================================
void character::AddDefenceInfo (felist &List) const {
  festring Entry;
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart) {
      Entry = CONST_S("   ");
      BodyPart->AddName(Entry, UNARTICLED);
      Entry.Resize(60);
      Entry << BodyPart->GetMaxHP();
      Entry.Resize(70);
      Entry << BodyPart->GetTotalResistance(PHYSICAL_DAMAGE);
      List.AddEntry(Entry, LIGHT_GRAY);
    }
  }
}


//==========================================================================
//
//  character::DetachBodyPart
//
//==========================================================================
void character::DetachBodyPart () {
  ADD_MESSAGE("You haven't got any extra bodyparts.");
}


//==========================================================================
//
//  character::ReceiveHolyBanana
//
//==========================================================================
void character::ReceiveHolyBanana (sLong Amount) {
  Amount <<= 1;
  EditExperience(ARM_STRENGTH, Amount, 1 << 13);
  EditExperience(LEG_STRENGTH, Amount, 1 << 13);
  EditExperience(DEXTERITY, Amount, 1 << 13);
  EditExperience(AGILITY, Amount, 1 << 13);
  EditExperience(ENDURANCE, Amount, 1 << 13);
  EditExperience(PERCEPTION, Amount, 1 << 13);
  EditExperience(INTELLIGENCE, Amount, 1 << 13);
  EditExperience(WISDOM, Amount, 1 << 13);
  EditExperience(CHARISMA, Amount, 1 << 13);
  RestoreLivingHP();
}


//==========================================================================
//
//  character::AddHolyBananaConsumeEndMessage
//
//==========================================================================
void character::AddHolyBananaConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel a mysterious strengthening fire coursing through your body.");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("For a moment %s is surrounded by a swirling fire aura.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::ReceiveHolyMango
//
//==========================================================================
void character::ReceiveHolyMango (sLong Amount) {
  Amount <<= 1;
  EditExperience(ARM_STRENGTH, Amount, 1 << 13);
  EditExperience(LEG_STRENGTH, Amount, 1 << 13);
  EditExperience(DEXTERITY, Amount, 1 << 13);
  EditExperience(AGILITY, Amount, 1 << 13);
  EditExperience(ENDURANCE, Amount, 1 << 13);
  EditExperience(PERCEPTION, Amount, 1 << 13);
  EditExperience(INTELLIGENCE, Amount, 1 << 13);
  EditExperience(WISDOM, Amount, 1 << 13);
  EditExperience(CHARISMA, Amount, 1 << 13);
  RestoreLivingHP();
}


//==========================================================================
//
//  character::AddHolyMangoConsumeEndMessage
//
//==========================================================================
void character::AddHolyMangoConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel a mysterious strengthening fire coursing through your body.");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("For a moment %s is surrounded by a swirling fire aura.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::PreProcessForBone
//
//==========================================================================
truth character::PreProcessForBone () {
  if (IsPet() && IsEnabled()) {
    Die(0, CONST_S(""), FORBID_REINCARNATION);
    return true;
  }
  if (GetAction()) GetAction()->Terminate(false);
  if (TemporaryStateIsActivated(POLYMORPHED)) {
    character *PolymorphBackup = GetPolymorphBackup();
    EndPolymorph();
    PolymorphBackup->PreProcessForBone();
    return true;
  }
  if (MustBeRemovedFromBone()) return false;
  if (IsUnique() && !CanBeGenerated()) {
    game::SignalQuestMonsterFound();
  }
  RestoreLivingHP();
  ResetStates();
  RemoveTraps();
  GetStack()->PreProcessForBone();
  doforequipments()(this, &item::PreProcessForBone);
  doforbodyparts()(this, &bodypart::PreProcessForBone);
  game::RemoveCharacterID(ID);
  ID = -ID;
  game::AddCharacterID(this, ID);
  return true;
}


//==========================================================================
//
//  character::PostProcessForBone
//
//==========================================================================
truth character::PostProcessForBone (double &DangerSum, int &Enemies) {
  if (PostProcessForBone()) {
    if (GetRelation(PLAYER) == HOSTILE) {
      double Danger = GetRelativeDanger(PLAYER, true);
      if (Danger > 99.0) game::SetTooGreatDangerFound(true);
      else if (!IsUnique() && !IgnoreDanger()) {
        DangerSum += Danger;
        ++Enemies;
      }
    }
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::PostProcessForBone
//
//==========================================================================
truth character::PostProcessForBone () {
  feuLong NewID = game::CreateNewCharacterID(this);
  game::GetBoneCharacterIDMap().insert(std::make_pair(-ID, NewID));
  game::RemoveCharacterID(ID);
  ID = NewID;
  ItemIgnores.clear();
  if (IsUnique() && CanBeGenerated()) {
    if (DataBase->Flags & HAS_BEEN_GENERATED) {
      return false;
    }
    SignalGeneration();
  }
  GetStack()->PostProcessForBone();
  doforequipments()(this, &item::PostProcessForBone);
  doforbodyparts()(this, &bodypart::PostProcessForBone);
  return true;
}


//==========================================================================
//
//  character::FinalProcessForBone
//
//==========================================================================
void character::FinalProcessForBone () {
  Flags &= ~C_PLAYER;
  GetStack()->FinalProcessForBone();
  doforequipments()(this, &item::FinalProcessForBone);
  int c;
  for (c = 0; c < BodyParts; ++c) {
    for (std::list<feuLong>::iterator i = OriginalBodyPartID[c].begin(); i != OriginalBodyPartID[c].end();) {
      boneidmap::iterator BI = game::GetBoneItemIDMap().find(*i);
      if (BI == game::GetBoneItemIDMap().end()) {
        std::list<feuLong>::iterator Dirt = i++;
        OriginalBodyPartID[c].erase(Dirt);
      } else {
        *i = BI->second;
        ++i;
      }
    }
  }
}


//==========================================================================
//
//  character::SetSoulID
//
//==========================================================================
void character::SetSoulID (feuLong What) {
  if (GetPolymorphBackup()) GetPolymorphBackup()->SetSoulID(What);
}


//==========================================================================
//
//  character::SearchForItem
//
//==========================================================================
truth character::SearchForItem (citem *Item) const {
  if (combineequipmentpredicateswithparam<feuLong>()(this, &item::HasID, Item->GetID(), 1)) return true;
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) if (*i == Item) return true;
  return false;
}


//==========================================================================
//
//  character::SearchForItem
//
//==========================================================================
item *character::SearchForItem (const sweaponskill *SWeaponSkill) const {
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment && SWeaponSkill->IsSkillOf(Equipment)) return Equipment;
  }
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) if (SWeaponSkill->IsSkillOf(*i)) return *i;
  return 0;
}


//==========================================================================
//
//  character::PutNear
//
//==========================================================================
truth character::PutNear (v2 Pos, truth allowFail) {
  v2 NewPos = game::GetCurrentLevel()->GetNearestFreeSquare(this, Pos, false);
  if (NewPos == ERROR_V2) {
    do {
      NewPos = game::GetCurrentLevel()->GetRandomSquare(this, 0, nullptr, NewPos);
      if (NewPos == ERROR_V2) {
        if (allowFail) return false;
        ABORT("Oops... Out of free squares in `character::PutNear()`!\n"
              "the char is `%s`\n", GetName(DEFINITE).CStr());
      }
    } while (NewPos == Pos);
  }
  PutTo(NewPos);
  return true;
}


//==========================================================================
//
//  character::PutToOrNearNoTeleport
//
//==========================================================================
truth character::PutToOrNearNoTeleport (v2 pos) {
  if (!game::IsInWilderness()) {
    // get resulting position; starting position is allowed
    pos = game::GetCurrentLevel()->GetNearestFreeSquare(this, pos, true);
  }
  if (pos == ERROR_V2) return false;
  PutTo(pos);
  return true;
}


//==========================================================================
//
//  character::PutNearNoTeleport
//
//==========================================================================
truth character::PutNearNoTeleport (v2 pos) {
  if (!game::IsInWilderness()) {
    // get resulting position; starting position is allowed
    pos = game::GetCurrentLevel()->GetNearestFreeSquare(this, pos, false);
  }
  if (pos == ERROR_V2) return false;
  PutTo(pos);
  return true;
}


//==========================================================================
//
//  character::PutToOrNear
//
//==========================================================================
truth character::PutToOrNear (v2 Pos, truth allowFail) {
  bool ok = game::IsInWilderness();
  if (!ok) {
      // it was `&&`, but i believe that it should be `||`
      //(CanMoveOn(game::GetCurrentLevel()->GetLSquare(Pos)) ||
      // IsFreeForMe(game::GetCurrentLevel()->GetLSquare(Pos))))
    ok = IsFreeForMe(game::GetCurrentLevel()->GetLSquare(Pos));
    // just in case
    if (ok && GetMoveType() != 0) {
      ok = CanMoveOn(game::GetCurrentLevel()->GetLSquare(Pos));
    } else if (GetMoveType() == 0) {
      ConLogf("PutToOrNear: skipped `CanMoveOn` check for '%s'", CHAR_NAME(DEFINITE));
    }
  }
  if (ok) {
    PutTo(Pos);
    return true;
  } else {
    return PutNear(Pos, allowFail);
  }
}


//==========================================================================
//
//  character::PutTo
//
//==========================================================================
void character::PutTo (v2 Pos) {
  SquareUnder[0] = game::GetCurrentArea()->GetSquare(Pos);
  SquareUnder[0]->AddCharacter(this);
}


//==========================================================================
//
//  character::Remove
//
//==========================================================================
void character::Remove () {
  SquareUnder[0]->RemoveCharacter();
  SquareUnder[0] = 0;
}


//==========================================================================
//
//  character::SendNewDrawRequest
//
//==========================================================================
void character::SendNewDrawRequest () const {
  for (int c = 0; c < SquaresUnder; ++c) {
    square *Square = GetSquareUnder(c);
    if (Square) Square->SendNewDrawRequest();
  }
}


//==========================================================================
//
//  character::IsOver
//
//==========================================================================
truth character::IsOver (v2 Pos) const {
  for (int c = 0; c < SquaresUnder; ++c) {
    square *Square = GetSquareUnder(c);
    if (Square && Square->GetPos() == Pos) return true;
  }
  return false;
}


truth character::CanTheoreticallyMoveOn (const lsquare *LSquare) const { return !!(GetMoveType() & LSquare->GetTheoreticalWalkability()); }
truth character::CanMoveOn (const lsquare *LSquare) const { return !!(GetMoveType() & LSquare->GetWalkability()); }
truth character::CanMoveOn (const square *Square) const { return !!(GetMoveType() & Square->GetSquareWalkability()); }
truth character::CanMoveOn (const olterrain *OLTerrain) const { return !!(GetMoveType() & OLTerrain->GetWalkability()); }
truth character::CanMoveOn (const oterrain *OTerrain) const { return !!(GetMoveType() & OTerrain->GetWalkability()); }
truth character::IsFreeForMe (square *Square) const { return !Square->GetCharacter() || Square->GetCharacter() == this; }
void character::LoadSquaresUnder () { SquareUnder[0] = game::GetSquareInLoad(); }


//==========================================================================
//
//  character::AttackAdjacentEnemyAI
//
//==========================================================================
truth character::AttackAdjacentEnemyAI () {
  if (!IsEnabled()) return false;
  character *Char[MAX_NEIGHBOUR_SQUARES];
  v2 Pos[MAX_NEIGHBOUR_SQUARES];
  int Dir[MAX_NEIGHBOUR_SQUARES];
  int Index = 0;
  for (int d = 0; d < GetNeighbourSquares(); ++d) {
    square *Square = GetNeighbourSquare(d);
    if (Square) {
      character *Enemy = Square->GetCharacter();
      if (Enemy && (GetRelation(Enemy) == HOSTILE || StateIsActivated(CONFUSED))) {
        Dir[Index] = d;
        Pos[Index] = Square->GetPos();
        Char[Index++] = Enemy;
      }
    }
  }
  if (Index) {
    int ChosenIndex = RAND_N(Index);
    HitWithVFX(Char[ChosenIndex], Pos[ChosenIndex], Dir[ChosenIndex]);
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::SignalStepFrom
//
//==========================================================================
void character::SignalStepFrom (lsquare **OldSquareUnder) {
  int c;
  lsquare *NewSquareUnder[MAX_SQUARES_UNDER];
  for (c = 0; c < GetSquaresUnder(); ++c) NewSquareUnder[c] = GetLSquareUnder(c);
  for (c = 0; c < GetSquaresUnder(); ++c) {
    if (IsEnabled() && GetLSquareUnder(c) == NewSquareUnder[c]) NewSquareUnder[c]->StepOn(this, OldSquareUnder);
  }
}


//==========================================================================
//
//  character::GetSumOfAttributes
//
//==========================================================================
int character::GetSumOfAttributes () const {
  return GetAttribute(ENDURANCE) + GetAttribute(PERCEPTION) +
         GetAttribute(INTELLIGENCE) + GetAttribute(WISDOM) +
         GetAttribute(CHARISMA) + GetAttribute(ARM_STRENGTH) +
         GetAttribute(AGILITY);
}


//==========================================================================
//
//  character::IntelligenceAction
//
//==========================================================================
void character::IntelligenceAction (int Difficulty) {
  EditAP(-20000 * Difficulty / APBonus(GetAttribute(INTELLIGENCE)));
  EditExperience(INTELLIGENCE, Difficulty * 15, 1 << 7);
}


struct walkabilitycontroller {
  static truth Handler (int x, int y) {
    return x >= 0 && y >= 0 && x < LevelXSize && y < LevelYSize && Map[x][y]->GetTheoreticalWalkability() & MoveType;
  }
  static lsquare ***Map;
  static int LevelXSize, LevelYSize;
  static int MoveType;
};


lsquare ***walkabilitycontroller::Map;
int walkabilitycontroller::LevelXSize, walkabilitycontroller::LevelYSize;
int walkabilitycontroller::MoveType;


//==========================================================================
//
//  character::CreateRoute
//
//==========================================================================
truth character::CreateRoute () {
  Route.clear();
  if (GetAttribute(INTELLIGENCE) >= 10 && !StateIsActivated(CONFUSED)) {
    v2 Pos = GetPos();
    walkabilitycontroller::Map = GetLevel()->GetMap();
    walkabilitycontroller::LevelXSize = GetLevel()->GetXSize();
    walkabilitycontroller::LevelYSize = GetLevel()->GetYSize();
    walkabilitycontroller::MoveType = GetMoveType();
    node *Node;
    for (int c = 0; c < game::GetTeams(); ++c) {
      for (std::list<character *>::const_iterator i = game::GetTeam(c)->GetMember().begin();
            i != game::GetTeam(c)->GetMember().end(); ++i)
      {
        character *Char = *i;
        //k8: it seems that this code is trying to reuse the route already built by some other char
        if (Char->IsEnabled() && !Char->Route.empty() &&
            //FIXME: this condition might be relaxed, i (k8) think...
            (Char->GetMoveType() & GetMoveType()) == Char->GetMoveType())
        {
          v2 CharGoingTo = Char->Route[0];
          v2 iPos = Char->Route.back();
          if ((GoingTo-CharGoingTo).GetLengthSquare() <= 100 && (Pos - iPos).GetLengthSquare() <= 100 &&
              mapmath<walkabilitycontroller>::DoLine(CharGoingTo.X, CharGoingTo.Y, GoingTo.X, GoingTo.Y, SKIP_FIRST|LINE_BOTH_DIRS) &&
              mapmath<walkabilitycontroller>::DoLine(Pos.X, Pos.Y, iPos.X, iPos.Y, SKIP_FIRST|LINE_BOTH_DIRS))
          {
            if (!Illegal.empty() && Illegal.find(Char->Route.back()) != Illegal.end()) continue;
            Node = GetLevel()->FindRoute(CharGoingTo, GoingTo, Illegal, GetMoveType());
            if (Node) {
              while (Node->Last) { Route.push_back(Node->Pos); Node = Node->Last; }
            } else {
              Route.clear();
              continue;
            }
            Route.insert(Route.end(), Char->Route.begin(), Char->Route.end());
            Node = GetLevel()->FindRoute(Pos, iPos, Illegal, GetMoveType());
            if (Node) {
              while (Node->Last) { Route.push_back(Node->Pos); Node = Node->Last; }
            } else {
              Route.clear();
              continue;
            }
            IntelligenceAction(1);
            return true;
          }
        }
      }
    }
    Node = GetLevel()->FindRoute(Pos, GoingTo, Illegal, GetMoveType());
    if (Node) {
      while (Node->Last) { Route.push_back(Node->Pos); Node = Node->Last; }
    } else {
      TerminateGoingTo();
    }
    IntelligenceAction(5);
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::SetGoingTo
//
//==========================================================================
void character::SetGoingTo (v2 What) {
  if (GoingTo != What) {
    GoingTo = What;
    Route.clear();
    Illegal.clear();
  }
}


//==========================================================================
//
//  character::TerminateGoingTo
//
//==========================================================================
void character::TerminateGoingTo () {
  GoingTo = ERROR_V2;
  Route.clear();
  Illegal.clear();
}


//==========================================================================
//
//  character::CheckForFood
//
//==========================================================================
truth character::CheckForFood (int Radius) {
  if (StateIsActivated(PANIC) || !UsesNutrition() || !IsEnabled()) return false;
  v2 Pos = GetPos();
  int x, y;
  for (int r = 1; r <= Radius; ++r) {
    x = Pos.X-r;
    if (x >= 0) {
      for (y = Pos.Y-r; y <= Pos.Y+r; ++y) {
        if (CheckForFoodInSquare(v2(x, y))) return true;
      }
    }
    x = Pos.X+r;
    if (x < GetLevel()->GetXSize()) {
      for (y = Pos.Y-r; y <= Pos.Y+r; ++y) {
        if (CheckForFoodInSquare(v2(x, y))) return true;
      }
    }
    y = Pos.Y-r;
    if (y >= 0) {
      for (x = Pos.X-r; x <= Pos.X+r; ++x) {
        if (CheckForFoodInSquare(v2(x, y))) return true;
      }
    }
    y = Pos.Y+r;
    if (y < GetLevel()->GetYSize()) {
      for (x = Pos.X-r; x <= Pos.X+r; ++x) {
        if (CheckForFoodInSquare(v2(x, y))) return true;
      }
    }
  }
  return false;
}


//==========================================================================
//
//  character::CheckForFoodInSquare
//
//==========================================================================
truth character::CheckForFoodInSquare (v2 Pos) {
  level *Level = GetLevel();
  if (Level->IsValidPos(Pos)) {
    lsquare *Square = Level->GetLSquare(Pos);
    stack *Stack = Square->GetStack();
    if (Stack->GetItems()) {
      for (stackiterator i = Stack->GetBottom(); i.HasItem(); ++i) {
        if (i->IsPickable(this) && i->CanBeSeenBy(this) &&
            i->CanBeEatenByAI(this) &&
            (!Square->GetRoomIndex() || Square->GetRoom()->AllowFoodSearch()))
        {
          SetGoingTo(Pos);
          return MoveTowardsTarget(false);
        }
      }
    }
  }
  return false;
}


//==========================================================================
//
//  character::SetConfig
//
//==========================================================================
void character::SetConfig (int NewConfig, int SpecialFlags) {
  databasecreator<character>::InstallDataBase(this, NewConfig);
  CalculateAll();
  CheckIfSeen();
  if (!(SpecialFlags & NO_PIC_UPDATE)) {
    UpdatePictures();
  }
}


//==========================================================================
//
//  character::IsOver
//
//==========================================================================
truth character::IsOver (citem *Item) const {
  for (int c1 = 0; c1 < Item->GetSquaresUnder(); ++c1) {
    for (int c2 = 0; c2 < SquaresUnder; ++c2) {
      if (Item->GetPos(c1) == GetPos(c2)) return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::CheckConsume
//
//==========================================================================
truth character::CheckConsume (cfestring &Verb) const {
  if (!UsesNutrition()) {
    if (IsPlayer()) ADD_MESSAGE("In this form you can't and don't need to %s.", Verb.CStr());
    return false;
  }
  return true;
}


//==========================================================================
//
//  character::PutTo
//
//==========================================================================
void character::PutTo (lsquare *To) {
  PutTo(To->GetPos());
}


//==========================================================================
//
//  character::RandomizeBabyExperience
//
//==========================================================================
double character::RandomizeBabyExperience (double SumE) {
  if (!SumE) return 0;
  //double E = (SumE / 4) - (SumE / 32) + (double(RAND()) / MAX_RAND) * (SumE / 16 + 1);
  // k8: slightly less biased this way
  double E = (SumE / 4) - (SumE / 32) + ((double)RANDU32() / (double)0xffffffffU) * (SumE / 16 + 1);
  return Limit(E, MIN_EXP, MAX_EXP);
}


//==========================================================================
//
//  character::CreateBlood
//
//==========================================================================
liquid *character::CreateBlood (sLong Volume) const {
  return liquid::Spawn(GetBloodMaterial(), Volume);
}


//==========================================================================
//
//  character::SpillFluid
//
//==========================================================================
void character::SpillFluid (character *Spiller, liquid *Liquid, int SquareIndex) {
  //ConLogf("character(%s:%d)::SpillFluid: Liquid->GetName(0):<%s>", GetTypeID(), GetConfig(), Liquid->GetName(false, false).CStr());
  sLong ReserveVolume = Liquid->GetVolume() >> 1;
  Liquid->EditVolume(-ReserveVolume);
  //ConLogf("character::SpillFluid: Liquid->GetName(1):<%s>", Liquid->GetName(false, false).CStr());
  GetStack()->SpillFluid(Spiller, Liquid, sLong(Liquid->GetVolume() * sqrt(double(GetStack()->GetVolume()) / GetVolume())));
  //ConLogf("character::SpillFluid: Liquid->GetName(2):<%s>", Liquid->GetName(false, false).CStr());
  Liquid->EditVolume(ReserveVolume);
  //ConLogf("character::SpillFluid: Liquid->GetName(3):<%s>", Liquid->GetName(false, false).CStr());
  sLong Modifier[MAX_BODYPARTS], ModifierSum = 0;
  for (int c = 0; c < BodyParts; ++c) {
    if (GetBodyPart(c)) {
      Modifier[c] = sLong(sqrt(GetBodyPart(c)->GetVolume()));
      if (Modifier[c]) Modifier[c] *= 1 + RAND_4;
      ModifierSum += Modifier[c];
    } else {
      Modifier[c] = 0;
    }
  }
  for (int c = 1; c < GetBodyParts(); ++c) {
    if (GetBodyPart(c) && IsEnabled()) {
      //ConLogf("character::SpillFluid: Liquid->GetName(4:%d):<%s>", c, Liquid->GetName(false, false).CStr());
      GetBodyPart(c)->SpillFluid(Spiller, Liquid->SpawnMoreLiquid(Liquid->GetVolume() * Modifier[c] / ModifierSum), SquareIndex);
      //ConLogf("character::SpillFluid: Liquid->GetName(5:%d):<%s>", c, Liquid->GetName(false, false).CStr());
    }
  }
  if (IsEnabled()) {
    Liquid->SetVolume(Liquid->GetVolume() * Modifier[TORSO_INDEX] / ModifierSum);
    //ConLogf("character::SpillFluid: Liquid->GetName(6):<%s>", Liquid->GetName(false, false).CStr());
    GetTorso()->SpillFluid(Spiller, Liquid, SquareIndex);
    //ConLogf("character::SpillFluid: Liquid->GetName(7):<%s>", Liquid->GetName(false, false).CStr());
  }
  //ConLogf("character::SpillFluid: Liquid->GetName(8):<%s>", Liquid->GetName(false, false).CStr());
}


//==========================================================================
//
//  character::StayOn
//
//==========================================================================
void character::StayOn (liquid *Liquid) {
  Liquid->TouchEffect(this, TORSO_INDEX);
}


//==========================================================================
//
//  character::IsAlly
//
//==========================================================================
truth character::IsAlly (ccharacter *Char) const {
  return Char->GetTeam()->GetID() == GetTeam()->GetID();
}


//==========================================================================
//
//  character::ResetSpoiling
//
//==========================================================================
void character::ResetSpoiling () {
  doforbodyparts()(this, &bodypart::ResetSpoiling);
}


//==========================================================================
//
//  character::SearchForItem
//
//==========================================================================
item *character::SearchForItem (ccharacter *Char, sorter Sorter) const {
  item *Equipment = findequipment<ccharacter *>()(this, Sorter, Char);
  if (Equipment) return Equipment;
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) if (((*i)->*Sorter)(Char)) return *i;
  return 0;
}


//==========================================================================
//
//  character::DetectMaterial
//
//==========================================================================
truth character::DetectMaterial (cmaterial *Material) const {
  return GetStack()->DetectMaterial(Material) ||
    combinebodypartpredicateswithparam<cmaterial*>()(this, &bodypart::DetectMaterial, Material, 1) ||
    combineequipmentpredicateswithparam<cmaterial*>()(this, &item::DetectMaterial, Material, 1);
}


//==========================================================================
//
//  character::DamageTypeDestroysBodyPart
//
//==========================================================================
truth character::DamageTypeDestroysBodyPart (int Type) {
  return (Type&0xFFF) != PHYSICAL_DAMAGE;
}


//==========================================================================
//
//  character::CheckIfTooScaredToHit
//
//==========================================================================
truth character::CheckIfTooScaredToHit (ccharacter *Enemy) const {
  if (IsPlayer() && StateIsActivated(PANIC)) {
    for (int d = 0; d < GetNeighbourSquares(); ++d) {
      square *Square = GetNeighbourSquare(d);
      if (Square) {
        if(CanMoveOn(Square) && (!Square->GetCharacter() || Square->GetCharacter()->IsPet())) {
          ADD_MESSAGE("You are too scared to attack %s.", Enemy->CHAR_DESCRIPTION(DEFINITE));
          return true;
        }
      }
    }
  }
  return false;
}


//==========================================================================
//
//  character::PrintBeginLevitationMessage
//
//==========================================================================
void character::PrintBeginLevitationMessage () const {
  if (!IsFlying()) {
    if (IsPlayer()) ADD_MESSAGE("You rise into the air like a small hot-air balloon.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s begins to float.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::PrintEndLevitationMessage
//
//==========================================================================
void character::PrintEndLevitationMessage () const {
  if (!IsFlying()) {
    if (IsPlayer()) ADD_MESSAGE("You descend gently onto the ground.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s drops onto the ground.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::IsLimbIndex
//
//==========================================================================
truth character::IsLimbIndex (int I) {
  switch (I) {
    case RIGHT_ARM_INDEX:
    case LEFT_ARM_INDEX:
    case RIGHT_LEG_INDEX:
    case LEFT_LEG_INDEX:
      return true;
  }
  return false;
}


//==========================================================================
//
//  character::EditExperience
//
//==========================================================================
void character::EditExperience (int Identifier, double Value, double Speed) {
  if (!AllowExperience() || (Identifier == ENDURANCE && UseMaterialAttributes())) return;
  int Change = RawEditExperience(BaseExperience[Identifier], GetNaturalExperience(Identifier), Value, Speed);
  if (!Change) return;
  cchar *PlayerMsg = 0, *NPCMsg = 0;
  switch (Identifier) {
    case ENDURANCE:
      if (Change > 0) {
        PlayerMsg = "You feel tougher than anything!";
        if (IsPet()) NPCMsg = "Suddenly %s looks tougher.";
      } else {
        PlayerMsg = "You feel less healthy.";
        if (IsPet()) NPCMsg = "Suddenly %s looks less healthy.";
      }
      CalculateBodyPartMaxHPs();
      CalculateMaxStamina();
      break;
    case PERCEPTION:
      if (IsPlayer()) {
        if (Change > 0) {
          PlayerMsg = "You now see the world in much better detail than before.";
        } else {
          PlayerMsg = "You feel very guru.";
          game::GetGod(VALPURUS)->AdjustRelation(100);
        }
        game::SendLOSUpdateRequest();
      }
      break;
    case INTELLIGENCE:
      if (IsPlayer()) {
        if (Change > 0) PlayerMsg = "Suddenly the inner structure of the Multiverse around you looks quite simple.";
        else PlayerMsg = "It surely is hard to think today.";
        UpdateESPLOS();
      }
      if (IsPlayerKind()) UpdatePictures();
      break;
    case WISDOM:
      if (IsPlayer()) {
        if (Change > 0) PlayerMsg = "You feel your life experience increasing all the time.";
        else PlayerMsg = "You feel like having done something unwise.";
      }
      if (IsPlayerKind()) UpdatePictures();
      break;
    case CHARISMA:
      if (Change > 0) {
        PlayerMsg = "You feel very confident of your social skills.";
        if (IsPet()) {
          if (GetAttribute(CHARISMA) <= 15) NPCMsg = "%s looks less ugly.";
          else NPCMsg = "%s looks more attractive.";
        }
      } else {
        PlayerMsg = "You feel somehow disliked.";
        if (IsPet()) {
          if (GetAttribute(CHARISMA) < 15) NPCMsg = "%s looks more ugly.";
          else NPCMsg = "%s looks less attractive.";
        }
      }
      if (IsPlayerKind()) UpdatePictures();
      break;
    case MANA:
      if (Change > 0) {
        PlayerMsg = "You feel magical forces coursing through your body!";
        NPCMsg = "You notice an odd glow around %s.";
      } else {
        PlayerMsg = "You feel your magical abilities withering slowly.";
        NPCMsg = "You notice strange vibrations in the air around %s. But they disappear rapidly.";
      }
      break;
  }

  if (IsPlayer()) ADD_MESSAGE("%s", PlayerMsg);
  else if (NPCMsg && CanBeSeenByPlayer()) ADD_MESSAGE(NPCMsg, CHAR_NAME(DEFINITE));

  CalculateBattleInfo();
}


//==========================================================================
//
//  character::RawEditExperience
//
//==========================================================================
int character::RawEditExperience (double &Exp, double NaturalExp, double Value, double Speed) const {
  double OldExp = Exp;
  if (Speed < 0) {
    Speed = -Speed;
    Value = -Value;
  }

  if (!OldExp || !Value || (Value > 0 && OldExp >= NaturalExp * (100 + Value) / 100) ||
      (Value < 0 && OldExp <= NaturalExp * (100 + Value) / 100))
  {
    return 0;
  }

  //if (!IsPlayer()) Speed *= 1.5;
  if (IsPlayer() && !IsPlayerKind()) {
    Speed *= 0.5; // slow down attribute gain of polymorphed player
  } else if (!IsPlayer()) {
    Speed *= 1.5;
  }

  Exp += (NaturalExp * (100 + Value) - 100 * OldExp) * Speed * EXP_DIVISOR;
  LimitRef(Exp, MIN_EXP, MAX_EXP);
  int NewA = int(Exp * EXP_DIVISOR);
  int OldA = int(OldExp * EXP_DIVISOR);
  int Delta = NewA - OldA;
  if (Delta > 0) {
    Exp = Max(Exp, (NewA + 0.05) * EXP_MULTIPLIER);
  } else if (Delta < 0) {
    Exp = Min(Exp, (NewA + 0.95) * EXP_MULTIPLIER);
  }
  LimitRef(Exp, MIN_EXP, MAX_EXP);

  return Delta;
}


//==========================================================================
//
//  character::GetAttribute
//
//==========================================================================
int character::GetAttribute (int Identifier, truth AllowBonus) const {
  int A = int(BaseExperience[Identifier] * EXP_DIVISOR);
  if (AllowBonus && Identifier == INTELLIGENCE && BrainsHurt()) {
    return Max((A + AttributeBonus[INTELLIGENCE]) / 3, 1);
  }
  //k8: it is used for divisions, so avoid returning 0 here.
  //    this should not happen, ever, but...
  //return A && AllowBonus ? Max(A + AttributeBonus[Identifier], 1) : A;
  A = (A && AllowBonus ? Max(A + AttributeBonus[Identifier], 1) : A);
  if (A == 0) A = 1;
  return A;
}


//==========================================================================
//
//  character::EditDealExperience
//
//==========================================================================
void character::EditDealExperience (sLong Price) {
  //EditExperience(CHARISMA, sqrt(Price) / 5, 1 << 9);
  //k8: see comm. fork commit e20db7eb1108416fbe542eb0245748d0e63a0e86
  EditExperience(CHARISMA, sqrt(Price) * 10, 15000);
}


//==========================================================================
//
//  character::PrintBeginLeprosyMessage
//
//==========================================================================
void character::PrintBeginLeprosyMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel you're falling in pieces.");
}


//==========================================================================
//
//  character::PrintEndLeprosyMessage
//
//==========================================================================
void character::PrintEndLeprosyMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel your limbs are stuck in place tightly."); // CHANGE OR DIE
}


//==========================================================================
//
//  character::TryToInfectWithLeprosy
//
//==========================================================================
void character::TryToInfectWithLeprosy (ccharacter *Infector) {
  if (!IsImmuneToLeprosy() &&
      ((GetRelation(Infector) == HOSTILE && !RAND_N(50 * GetAttribute(ENDURANCE))) ||
       !RAND_N(500 * GetAttribute(ENDURANCE))))
  {
    GainIntrinsic(LEPROSY);
  }
}


//==========================================================================
//
//  character::SignalGeneration
//
//==========================================================================
void character::SignalGeneration () {
  const_cast<database *>(DataBase)->Flags |= HAS_BEEN_GENERATED;
}


//==========================================================================
//
//  character::CheckIfSeen
//
//==========================================================================
void character::CheckIfSeen () {
  if (IsPlayer() || CanBeSeenByPlayer()) SignalSeen();
}


//==========================================================================
//
//  character::SignalSeen
//
//==========================================================================
void character::SignalSeen () {
  if (!(WarnFlags & WARNED) && GetRelation(PLAYER) == HOSTILE && !StateIsActivated(FEARLESS)) {
    double Danger = GetRelativeDanger(PLAYER);
    if (Danger > 5.0) {
      game::SetDangerFound(Max(game::GetDangerFound(), Danger));
      if (Danger > 500.0 && !(WarnFlags & HAS_CAUSED_PANIC)) {
        WarnFlags |= HAS_CAUSED_PANIC;
        game::SetCausePanicFlag(true);
      }
      WarnFlags |= WARNED;
    }
  }
  const_cast<database *>(DataBase)->Flags |= HAS_BEEN_SEEN;
}


//==========================================================================
//
//  character::GetPolymorphIntelligenceRequirement
//
//==========================================================================
int character::GetPolymorphIntelligenceRequirement () const {
  if (DataBase->PolymorphIntelligenceRequirement == DEPENDS_ON_ATTRIBUTES) {
    return Max(GetAttributeAverage() - 5, 0);
  }
  return DataBase->PolymorphIntelligenceRequirement;
}


//==========================================================================
//
//  character::RemoveAllItems
//
//==========================================================================
void character::RemoveAllItems () {
  GetStack()->Clean();
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment) {
      Equipment->RemoveFromSlot();
      Equipment->SendToHell();
    }
  }
}


//==========================================================================
//
//  character::CalculateWeaponSkillHits
//
//==========================================================================
int character::CalculateWeaponSkillHits (ccharacter *Enemy) const {
  if (Enemy->IsPlayer()) {
    configid ConfigID(GetType(), GetConfig());
    const dangerid& DangerID = game::GetDangerMap().find(ConfigID)->second;
    return Min(int(DangerID.EquippedDanger * 2000), 1000);
  }
  return Min(int(GetRelativeDanger(Enemy, true) * 2000), 1000);
}


//==========================================================================
//
//  character::CanUseEquipment
//
//==========================================================================
truth character::CanUseEquipment (int I) const {
  return CanUseEquipment() && I < GetEquipments() &&
         GetBodyPartOfEquipment(I) && EquipmentIsAllowed(I);
}


//==========================================================================
//
//  character::DonateEquipmentTo
//
//  Target mustn't have any equipment
//
//==========================================================================
void character::DonateEquipmentTo (character *Character) {
  if (IsPlayer()) {
    feuLong *EquipmentMemory = game::GetEquipmentMemory();
    for (int c = 0; c < MAX_EQUIPMENT_SLOTS; ++c) {
      item *Item = GetEquipment(c);
      if (Item) {
        if (Character->CanUseEquipment(c)) {
          Item->RemoveFromSlot();
          Character->SetEquipment(c, Item);
        } else {
          EquipmentMemory[c] = Item->GetID();
          Item->MoveTo(Character->GetStack());
        }
      } else if (CanUseEquipment(c)) {
        EquipmentMemory[c] = 0;
      } else if (EquipmentMemory[c] && Character->CanUseEquipment(c)) {
        for (stackiterator i = Character->GetStack()->GetBottom(); i.HasItem(); ++i) {
          if (i->GetID() == EquipmentMemory[c]) {
            item *Item = *i;
            Item->RemoveFromSlot();
            Character->SetEquipment(c, Item);
            break;
          }
        }
        EquipmentMemory[c] = 0;
      }
    }
  } else {
    for (int c = 0; c < GetEquipments(); ++c) {
      item *Item = GetEquipment(c);
      if (Item) {
        if (Character->CanUseEquipment(c)) {
          Item->RemoveFromSlot();
          Character->SetEquipment(c, Item);
        } else {
          Item->MoveTo(Character->GetStackUnder());
        }
      }
    }
  }
}


//==========================================================================
//
//  character::ReceivePeaSoup
//
//==========================================================================
void character::ReceivePeaSoup (sLong) {
  if (!game::IsInWilderness()) {
    lsquare *Square = GetLSquareUnder();
    if (Square->IsFlyable()) Square->AddSmoke(gas::Spawn(FART, 250));
  }
}


//==========================================================================
//
//  character::AddPeaSoupConsumeEndMessage
//
//==========================================================================
void character::AddPeaSoupConsumeEndMessage () const {
  if (IsPlayer()) {
    if (CanHear()) {
      ADD_MESSAGE("Mmmh! The soup is very tasty. You hear a small puff.");
    } else {
      ADD_MESSAGE("Mmmh! The soup is very tasty.");
    }
  } else if (PLAYER->CanHear() && CanBeSeenByPlayer()) {
    // change someday
    ADD_MESSAGE("You hear a small puff.");
  }
}


//==========================================================================
//
//  character::CalculateMaxStamina
//
//==========================================================================
void character::CalculateMaxStamina () {
  MaxStamina = TorsoIsAlive() ? GetAttribute(ENDURANCE) * 10000 : 0;
}


//==========================================================================
//
//  character::EditStamina
//
//==========================================================================
void character::EditStamina (int Amount, truth CanCauseUnconsciousness) {
  if (!TorsoIsAlive()) return;
  int UnconsciousnessStamina = MaxStamina >> 3;
  if (!CanCauseUnconsciousness && Amount < 0) {
    if (Stamina > UnconsciousnessStamina) {
      Stamina += Amount;
      if (Stamina < UnconsciousnessStamina) Stamina = UnconsciousnessStamina;
    }
    return;
  }
  int OldStamina = Stamina;
  Stamina += Amount;
  if (Stamina > MaxStamina) {
    Stamina = MaxStamina;
  } else if (Stamina < 0) {
    Stamina = 0;
    LoseConsciousness(250 + RAND_N(250));
  } else if (IsPlayer()) {
    if (OldStamina >= MaxStamina >> 2 && Stamina < MaxStamina >> 2) {
      ADD_MESSAGE("You are getting a little tired.");
    } else if(OldStamina >= UnconsciousnessStamina && Stamina < UnconsciousnessStamina) {
      ADD_MESSAGE("You are seriously out of breath!");
      game::SetPlayerIsRunning(false);
    }
  }
  if (IsPlayer() && StateIsActivated(PANIC) && GetTirednessState() != FAINTING) {
    game::SetPlayerIsRunning(true);
  }
}


//==========================================================================
//
//  character::RegenerateStamina
//
//==========================================================================
void character::RegenerateStamina () {
  if (GetTirednessState() != UNTIRED) {
    EditExperience(ENDURANCE, 50, 1);
    if (Sweats() && TorsoIsAlive() && !RAND_N(30) && !game::IsInWilderness()) {
      // Sweat amount proportional to endurance also
      //sLong Volume = sLong(0.05 * sqrt(GetBodyVolume()));
      sLong Volume = long(0.05*sqrt(GetBodyVolume()*GetAttribute(ENDURANCE)/10));
      if (GetTirednessState() == FAINTING) Volume <<= 1;
      for (int c = 0; c < SquaresUnder; ++c) {
        GetLSquareUnder(c)->SpillFluid(0, CreateSweat(Volume), false, false);
      }
    }
  }
  int Bonus = 1;
  if (Action) {
    if (Action->IsRest()) {
      if (SquaresUnder == 1) {
        Bonus = GetSquareUnder()->GetRestModifier() << 1;
      } else {
        int Lowest = GetSquareUnder(0)->GetRestModifier();
        for (int c = 1; c < GetSquaresUnder(); ++c) {
          int Mod = GetSquareUnder(c)->GetRestModifier();
          if (Mod < Lowest) Lowest = Mod;
        }
        Bonus = Lowest << 1;
      }
    } else if (Action->IsUnconsciousness()) Bonus = 2;
  }
  int Plus1 = 100;
  auto bst = GetBurdenState();
       if (bst == OVER_LOADED) Plus1 = 25;
  else if (bst == STRESSED) Plus1 = 50;
  else if (bst == BURDENED) Plus1 = 75;
  int Plus2 = 100;
  if (IsPlayer()) {
    auto hst = GetHungerState();
         if (hst == STARVING) Plus2 = 25;
    else if (hst == VERY_HUNGRY) Plus2 = 50;
    else if (hst == HUNGRY) Plus2 = 75;
  }
  Stamina += Plus1 * Plus2 * Bonus / 1000;
  if (Stamina > MaxStamina) Stamina = MaxStamina;
  if (IsPlayer() && StateIsActivated(PANIC) && GetTirednessState() != FAINTING) {
    game::SetPlayerIsRunning(true);
  }
}


//==========================================================================
//
//  character::BeginPanic
//
//==========================================================================
void character::BeginPanic () {
  if (IsPlayer() && GetTirednessState() != FAINTING) {
    game::SetPlayerIsRunning(true);
  }
  DeActivateVoluntaryAction();
}


//==========================================================================
//
//  character::EndPanic
//
//==========================================================================
void character::EndPanic () {
  if (IsPlayer()) game::SetPlayerIsRunning(false);
}


//==========================================================================
//
//  character::GetTirednessState
//
//==========================================================================
int character::GetTirednessState () const {
  if (Stamina >= MaxStamina >> 2) return UNTIRED;
  if (Stamina >= MaxStamina >> 3) return EXHAUSTED;
  return FAINTING;
}


//==========================================================================
//
//  character::ReceiveBlackUnicorn
//
//==========================================================================
void character::ReceiveBlackUnicorn (sLong Amount) {
  if (!RAND_N(160)) game::DoEvilDeed(Amount / 50);
  BeginTemporaryState(TELEPORT, Amount / 100);
  for (int c = 0; c < STATES; ++c) {
    if (StateData[c].Flags & DUR_TEMPORARY) {
      BeginTemporaryState(1 << c, Amount / 100);
      if (!IsEnabled()) return;
    } else if (StateData[c].Flags & DUR_PERMANENT) {
      GainIntrinsic(1 << c);
      if (!IsEnabled()) return;
    }
  }
}


//==========================================================================
//
//  character::ReceiveGrayUnicorn
//
//==========================================================================
void character::ReceiveGrayUnicorn (sLong Amount) {
  if (!RAND_N(80)) game::DoEvilDeed(Amount / 50);
  BeginTemporaryState(TELEPORT, Amount / 100);
  for (int c = 0; c < STATES; ++c) {
    if (1 << c != TELEPORT) {
      DecreaseStateCounter(1 << c, -Amount / 100);
      if (!IsEnabled()) return;
    }
  }
}


//==========================================================================
//
//  character::ReceiveWhiteUnicorn
//
//==========================================================================
void character::ReceiveWhiteUnicorn (sLong Amount) {
  if (!RAND_N(40)) game::DoEvilDeed(Amount / 50);
  BeginTemporaryState(TELEPORT, Amount / 100);
  DecreaseStateCounter(LYCANTHROPY, -Amount / 100);
  DecreaseStateCounter(POISONED, -Amount / 100);
  DecreaseStateCounter(PARASITE_TAPE_WORM, -Amount / 100);
  DecreaseStateCounter(PARASITE_MIND_WORM, -Amount / 100);
  DecreaseStateCounter(LEPROSY, -Amount / 100);
  DecreaseStateCounter(VAMPIRISM, -Amount / 100);
}


//==========================================================================
//
//  character::DecreaseStateCounter
//
//  Counter should be negative. Removes intrinsics.
//
//==========================================================================
void character::DecreaseStateCounter (sLong State, int Counter) {
  while (State != 0) {
    sLong st = 0, sidx;
    for (sidx = 0; sidx < STATES; ++sidx) if (State&(1<<sidx)) { st = (1<<sidx); break; }
    if (!st) { break; /*ABORT("BeginTemporaryState works only when State == 2^n!");*/ }
    State &= ~st;
    if (TemporaryState&st) {
      if (TemporaryStateCounter[sidx] == PERMANENT || (TemporaryStateCounter[sidx] += Counter) <= 0) {
        TemporaryState &= ~st;
        if (!(EquipmentState & st)) {
          if (StateData[sidx].EndHandler) {
            (this->*StateData[sidx].EndHandler)();
            if (!IsEnabled()) return;
          }
          (this->*StateData[sidx].PrintEndMessage)();
        }
      }
    }
  }
}


//==========================================================================
//
//  character::IsImmuneToLeprosy
//
//==========================================================================
truth character::IsImmuneToLeprosy () const {
  return (DataBase->IsImmuneToLeprosy || UseMaterialAttributes() || StateIsActivated(DISEASE_IMMUNITY));
}


//==========================================================================
//
//  character::LeprosyHandler
//
//==========================================================================
void character::LeprosyHandler () {
  EditExperience(ARM_STRENGTH, -25, 1 << 1);
  EditExperience(LEG_STRENGTH, -25, 1 << 1);
  EditExperience(DEXTERITY, -25, 1 << 1);
  EditExperience(AGILITY, -25, 1 << 1);
  EditExperience(ENDURANCE, -25, 1 << 1);
  EditExperience(CHARISMA, -25, 1 << 1);
  CheckDeath(CONST_S("killed by leprosy"));
}


//==========================================================================
//
//  character::SearchForOriginalBodyPart
//
//==========================================================================
bodypart *character::SearchForOriginalBodyPart (int I) const {
  for (stackiterator i1 = GetStackUnder()->GetBottom(); i1.HasItem(); ++i1) {
    for (std::list<feuLong>::iterator i2 = OriginalBodyPartID[I].begin();
          i2 != OriginalBodyPartID[I].end(); ++i2)
    {
      if (i1->GetID() == *i2) return static_cast<bodypart*>(*i1);
    }
  }
  return 0;
}


//==========================================================================
//
//  character::SetLifeExpectancy
//
//==========================================================================
void character::SetLifeExpectancy (int Base, int RandPlus) {
  int c;
  for (c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart) BodyPart->SetLifeExpectancy(Base, RandPlus);
  }
  for (c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment) Equipment->SetLifeExpectancy(Base, RandPlus);
  }
}


//==========================================================================
//
//  character::DuplicateEquipment
//
//  Receiver should be a fresh duplicate of this
//
//==========================================================================
void character::DuplicateEquipment (character *Receiver, feuLong Flags) {
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment) {
      item *Duplicate = Equipment->Duplicate(Flags);
      Receiver->SetEquipment(c, Duplicate);
    }
  }
}


//==========================================================================
//
//  character::Disappear
//
//==========================================================================
void character::Disappear (corpse *Corpse, cchar *Verb, truth (item::*ClosePredicate)() const) {
  truth TorsoDisappeared = false;
  truth CanBeSeen = Corpse ? Corpse->CanBeSeenByPlayer() : IsPlayer() || CanBeSeenByPlayer();
  int c;

  if ((GetTorso()->*ClosePredicate)()) {
    if (CanBeSeen) {
      if (Corpse) ADD_MESSAGE("%s %ss.", Corpse->CHAR_NAME(DEFINITE), Verb);
      else if (IsPlayer()) ADD_MESSAGE("You %s.", Verb);
      else ADD_MESSAGE("%s %ss.", CHAR_NAME(DEFINITE), Verb);
    }
    TorsoDisappeared = true;
    for (c = 0; c < GetEquipments(); ++c) {
      item *Equipment = GetEquipment(c);
      if (Equipment && (Equipment->*ClosePredicate)()) {
        Equipment->RemoveFromSlot();
        Equipment->SendToHell();
      }
    }
    itemvector ItemVector;
    GetStack()->FillItemVector(ItemVector);
    for (uInt c = 0; c < ItemVector.size(); ++c) {
      if (ItemVector[c] && (ItemVector[c]->*ClosePredicate)()) {
        ItemVector[c]->RemoveFromSlot();
        ItemVector[c]->SendToHell();
      }
    }
  }

  for (c = 1; c < GetBodyParts(); ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart) {
      if ((BodyPart->*ClosePredicate)()) {
        if (!TorsoDisappeared && CanBeSeen) {
          if(IsPlayer()) ADD_MESSAGE("Your %s %ss.", GetBodyPartName(c).CStr(), Verb);
          else ADD_MESSAGE("The %s of %s %ss.", GetBodyPartName(c).CStr(), CHAR_NAME(DEFINITE), Verb);
        }
        BodyPart->DropEquipment();
        item *BodyPart = SevereBodyPart(c);
        if (BodyPart) BodyPart->SendToHell();
      } else if (TorsoDisappeared) {
        BodyPart->DropEquipment();
        item *BodyPart = SevereBodyPart(c);
        if (BodyPart) {
               if (Corpse) Corpse->GetSlot()->AddFriendItem(BodyPart);
          else if (!game::IsInWilderness()) GetStackUnder()->AddItem(BodyPart);
          else BodyPart->SendToHell();
        }
      }
    }
  }

  if (TorsoDisappeared) {
    if (Corpse) {
      Corpse->RemoveFromSlot();
      Corpse->SendToHell();
    } else {
      CheckDeath(festring(Verb) + "ed", 0, FORCE_DEATH|DISALLOW_CORPSE|DISALLOW_MSG);
    }
  } else {
    CheckDeath(festring(Verb) + "ed", 0, DISALLOW_MSG);
  }
}


//==========================================================================
//
//  character::SignalDisappearance
//
//==========================================================================
void character::SignalDisappearance () {
  if (GetMotherEntity()) {
    GetMotherEntity()->SignalDisappearance();
  } else {
    Disappear(0, "disappear", &item::IsVeryCloseToDisappearance);
  }
}


//==========================================================================
//
//  character::HornOfFearWorks
//
//==========================================================================
truth character::HornOfFearWorks () const {
  return CanHear() && GetPanicLevel() > RAND_N(33) && !StateIsActivated(FEARLESS);
}


//==========================================================================
//
//  character::BeginLeprosy
//
//==========================================================================
void character::BeginLeprosy () {
  doforbodypartswithparam<truth>()(this, &bodypart::SetIsInfectedByLeprosy, true);
}


//==========================================================================
//
//  character::EndLeprosy
//
//==========================================================================
void character::EndLeprosy () {
  doforbodypartswithparam<truth>()(this, &bodypart::SetIsInfectedByLeprosy, false);
}


//==========================================================================
//
//  character::IsSameAs
//
//==========================================================================
truth character::IsSameAs (ccharacter *What) const {
  return What->GetType() == GetType() && What->GetConfig() == GetConfig();
}


//==========================================================================
//
//  character::GetCommandFlags
//
//==========================================================================
feuLong character::GetCommandFlags () const {
  return !StateIsActivated(PANIC) ? CommandFlags : CommandFlags|FLEE_FROM_ENEMIES;
}


//==========================================================================
//
//  character::GetConstantCommandFlags
//
//==========================================================================
feuLong character::GetConstantCommandFlags () const {
  return !StateIsActivated(PANIC) ? DataBase->ConstantCommandFlags
                                  : DataBase->ConstantCommandFlags|FLEE_FROM_ENEMIES;
}


//==========================================================================
//
//  character::GetPossibleCommandFlags
//
//==========================================================================
feuLong character::GetPossibleCommandFlags () const {
  int Int = GetAttribute(INTELLIGENCE);
  feuLong Flags = ALL_COMMAND_FLAGS;
  if (!CanMove() || Int < 4) Flags &= ~FOLLOW_LEADER;
  if (!CanMove() || Int < 6) Flags &= ~FLEE_FROM_ENEMIES;
  if (!CanUseEquipment() || Int < 8) Flags &= ~DONT_CHANGE_EQUIPMENT;
  if (!UsesNutrition() || Int < 8) Flags &= ~DONT_CONSUME_ANYTHING_VALUABLE;
  return Flags;
}


//==========================================================================
//
//  character::IsRetreating
//
//==========================================================================
truth character::IsRetreating () const {
  return StateIsActivated(PANIC) || ((CommandFlags & FLEE_FROM_ENEMIES) && IsPet());
}


//==========================================================================
//
//  character::ChatMenu
//
//==========================================================================
truth character::ChatMenu () {
  if (GetAction() && !GetAction()->CanBeTalkedTo()) {
    ADD_MESSAGE("%s is silent.", CHAR_DESCRIPTION(DEFINITE));
    PLAYER->EditAP(-200);
    return true;
  }

  feuLong ManagementFlags = GetManagementFlags();
  if (ManagementFlags == CHAT_IDLY || !IsPet()) {
    return ChatIdly();
  }

  cchar *const ChatMenuEntry[CHAT_MENU_ENTRIES] = {
    "Change equipment",
    "Take items",
    "Give items",
    "Issue commands",
    "Chat idly",
  };

  const petmanagementfunction PMF[CHAT_MENU_ENTRIES] = {
    &character::ChangePetEquipment,
    &character::TakePetItems,
    &character::GivePetItems,
    &character::IssuePetCommands,
    &character::ChatIdly
  };

  felist List(CONST_S("Choose action:"));
  game::SetStandardListAttributes(List);
  List.AddFlags(SELECTABLE);
  int c, i;
  for (c = 0; c < CHAT_MENU_ENTRIES; ++c) {
    if (1 << c & ManagementFlags) {
      List.AddEntry(CONST_S(ChatMenuEntry[c]), LIGHT_GRAY);
    }
  }

  int Chosen = List.Draw();
  if (Chosen & FELIST_ERROR_BIT) return false;
  for (c = 0, i = 0; c < CHAT_MENU_ENTRIES; ++c) {
    if (1 << c & ManagementFlags && i++ == Chosen) return (this->*PMF[c])();
  }

  return false; // dummy
}


//==========================================================================
//
//  character::ChangePetEquipment
//
//==========================================================================
truth character::ChangePetEquipment () {
  if (EquipmentScreen(PLAYER->GetStack(), GetStack())) {
    DexterityAction(3);
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::TakePetItems
//
//==========================================================================
truth character::TakePetItems () {
  truth Success = false;
  stack::SetSelected(0);
  for (;;) {
    itemvector ToTake;
    game::DrawEverythingNoBlit();
    GetStack()->DrawContents(
      ToTake,
      0,
      PLAYER,
      CONST_S("What do you want to take from ") + CHAR_DESCRIPTION(DEFINITE) + '?',
      CONST_S(""),
      CONST_S(""),
      GetDescription(DEFINITE) + " is " + GetVerbalBurdenState(),
      GetVerbalBurdenStateColor(),
      REMEMBER_SELECTED);
    if (ToTake.empty()) break;
    for (uInt c = 0; c < ToTake.size(); ++c) ToTake[c]->MoveTo(PLAYER->GetStack());
    ADD_MESSAGE("You take %s.", ToTake[0]->GetName(DEFINITE, ToTake.size()).CStr());
    Success = true;
  }
  if (Success) {
    DexterityAction(2);
    PLAYER->DexterityAction(2);
  }
  return Success;
}


//==========================================================================
//
//  character::GivePetItems
//
//==========================================================================
truth character::GivePetItems () {
  truth Success = false;
  stack::SetSelected(0);
  for (;;) {
    itemvector ToGive;
    game::DrawEverythingNoBlit();
    PLAYER->GetStack()->DrawContents(
      ToGive,
      0,
      this,
      CONST_S("What do you want to give to ") + CHAR_DESCRIPTION(DEFINITE) + '?',
      CONST_S(""),
      CONST_S(""),
      GetDescription(DEFINITE) + " is " + GetVerbalBurdenState(),
      GetVerbalBurdenStateColor(),
      REMEMBER_SELECTED);
    if (ToGive.empty()) break;
    for (uInt c = 0; c < ToGive.size(); ++c) ToGive[c]->MoveTo(GetStack());
    ADD_MESSAGE("You give %s to %s.", ToGive[0]->GetName(DEFINITE, ToGive.size()).CStr(), CHAR_DESCRIPTION(DEFINITE));
    Success = true;
  }
  if (Success) {
    DexterityAction(2);
    PLAYER->DexterityAction(2);
  }
  return Success;
}


//==========================================================================
//
//  character::IssuePetCommands
//
//==========================================================================
truth character::IssuePetCommands () {
  if (!IsConscious()) {
    ADD_MESSAGE("%s is unconscious.", CHAR_DESCRIPTION(DEFINITE));
    return false;
  }
  feuLong PossibleC = GetPossibleCommandFlags();
  if (!PossibleC) {
    ADD_MESSAGE("%s cannot be commanded.", CHAR_DESCRIPTION(DEFINITE));
    return false;
  }
  feuLong OldC = GetCommandFlags();
  feuLong NewC = OldC, VaryFlags = 0;
  game::CommandScreen(CONST_S("Issue commands to ")+GetDescription(DEFINITE), PossibleC, GetConstantCommandFlags(), VaryFlags, NewC);
  if (NewC == OldC) return false;
  SetCommandFlags(NewC);
  PLAYER->EditAP(-500);
  PLAYER->EditExperience(CHARISMA, 25, 1 << 7);
  return true;
}


//==========================================================================
//
//  character::ChatIdly
//
//==========================================================================
truth character::ChatIdly () {
  if (!TryQuestTalks() && !TryToTalkAboutScience()) {
    #if 0
    ConLogf("ChatIdly(%s): BeTalkedTo!", GetNameSingular().CStr());
    #endif
    BeTalkedTo();
    PLAYER->EditExperience(CHARISMA, 75, 1 << 7);
  }
  PLAYER->EditAP(-1000);
  return true;
}


//==========================================================================
//
//  character::HasSomethingToEquipAt
//
//==========================================================================
int character::HasSomethingToEquipAt (int chosen, truth equippedIsTrue) {
  if (!GetBodyPartOfEquipment(chosen)) return 0;

  item *oldEquipment = GetEquipment(chosen);
  if (!IsPlayer() && oldEquipment && BoundToUse(oldEquipment, chosen)) return 0;

  stack *mainStack = GetStack();
  sorter Sorter = EquipmentSorter(chosen);
  auto count = mainStack->SortedItemsCount(this, Sorter);

  if (equippedIsTrue && oldEquipment) ++count;

  return count;
}


//==========================================================================
//
//  character::HasSomethingToEquipAtRecentTime
//
//  returns 0, 1 or pickup time
//
//==========================================================================
feuLong character::HasSomethingToEquipAtRecentTime (int chosen, truth equippedIsTrue) {
  if (!GetBodyPartOfEquipment(chosen)) return 0;

  item *oldEquipment = GetEquipment(chosen);
  if (!IsPlayer() && oldEquipment && BoundToUse(oldEquipment, chosen)) return 0;

  stack *mainStack = GetStack();
  sorter Sorter = EquipmentSorter(chosen);
  feuLong highestTime = mainStack->SortedItemsRecentTime(this, Sorter);

  if (equippedIsTrue && oldEquipment && oldEquipment->pickupTime > highestTime) highestTime = oldEquipment->pickupTime;

  return highestTime;
}


//==========================================================================
//
//  character::FixLeftRightSelected
//
//==========================================================================
int character::FixLeftRightSelected (int selected) {
  int ileft, iright;
  bool wield = false;
  switch (selected) {
    case RIGHT_RING_INDEX: case LEFT_RING_INDEX:
      iright = RIGHT_RING_INDEX; ileft = LEFT_RING_INDEX;
      break;
    case RIGHT_WIELDED_INDEX: case LEFT_WIELDED_INDEX:
      iright = RIGHT_WIELDED_INDEX; ileft = LEFT_WIELDED_INDEX;
      wield = true;
      break;
    case RIGHT_GAUNTLET_INDEX: case LEFT_GAUNTLET_INDEX:
      iright = RIGHT_GAUNTLET_INDEX; ileft = LEFT_GAUNTLET_INDEX;
      break;
    case RIGHT_BOOT_INDEX: case LEFT_BOOT_INDEX:
      iright = RIGHT_BOOT_INDEX; ileft = LEFT_BOOT_INDEX;
      break;
    default:
      return selected;
  }

  if (GetBodyPartOfEquipment(ileft) && GetBodyPartOfEquipment(iright)) {
    // has both body parts
    const bool hasLeft = !!GetEquipment(ileft);
    const bool hasRight = !!GetEquipment(iright);
    if (!hasLeft || !hasRight) {
      if (wield) {
        selected = ileft; // i prefer the left hand
      } else {
        if (!hasLeft && HasSomethingToEquipAt(ileft, false) > 0) {
          selected = ileft;
        } else if (!hasRight && HasSomethingToEquipAt(iright, false) > 0) {
          selected = iright;
        }
      }
    } else {
      // both slots are equipped, prefer left
      selected = ileft;
    }
  }

  return selected;
}


//==========================================================================
//
//  character::EquipmentScreen
//
//==========================================================================
truth character::EquipmentScreen (stack *MainStack, stack *SecStack) {
  if (!CanUseEquipment()) {
    ADD_MESSAGE("%s cannot use equipment.", CHAR_DESCRIPTION(DEFINITE));
    return false;
  }

  int Chosen = 0;
  truth EquipmentChanged = false;
  felist List(CONST_S("Equipment menu [\1RESC\2 exits]"));
  festring Entry;
  for (;;) {
    List.Empty();
    List.EmptyDescription();
    if (!IsPlayer()) {
      List.AddDescription(CONST_S(""));
      List.AddDescription(festring(GetDescription(DEFINITE) + " is " + GetVerbalBurdenState()).CapitalizeCopy(),
                          GetVerbalBurdenStateColor());
    } else {
      int totalWeight = 0;
      for (int c = 0; c < GetEquipments(); ++c) {
        item *Equipment = GetEquipment(c);
        totalWeight += (Equipment ? Equipment->GetWeight() : 0);
      }
      festring Total("Total weight: "); //\1Y");
      //Total << totalWeight;
      //Total << "\2g";
      Total.PutWeight(totalWeight, "\1Y", "\2");
      List.AddDescription(CONST_S(""));
      List.AddDescription(Total);
    }
    int firstEmpty = -1, firstNonEmpty = -1, selected = -1;
    feuLong selPickTime = 1;
    feuLong armPickTime = 0;
    int armFirst = -1;
    //truth selectedIsEmpty = false;

    int maxEqNameWidth = 0;
    for (int c = 0; c < GetEquipments(); ++c) {
      festring ss(GetEquipmentName(c));
      ss << ":";
      const int ww = FONT->TextWidth(ss);
      if (maxEqNameWidth < ww) maxEqNameWidth = ww;
    }
    maxEqNameWidth += 6;
    #if 0
    ConLogf("xww=%d", maxEqNameWidth);
    #endif

    for (int c = 0; c < GetEquipments(); ++c) {
      truth isArm = (c == RIGHT_WIELDED_INDEX || c == LEFT_WIELDED_INDEX);
      //int bpidx = (GetBodyPartOfEquipment(c) ? GetBodyPartOfEquipment(c)->GetBodyPartIndex() : -1);
      truth equippable = !!GetBodyPartOfEquipment(c);
      Entry = GetEquipmentName(c);
      Entry << ':';
      FONT->RPadToPixWidth(Entry, maxEqNameWidth); //Entry.Resize(20);
      item *Equipment = GetEquipment(c);
      feuLong pickTm = (equippable ? HasSomethingToEquipAtRecentTime(c, false) : 0);
      int availEquipCount = (equippable ? HasSomethingToEquipAt(c, false) : 0);
      if (pickTm > 1 && game::GetTick()-pickTm > game::PickTimeout) pickTm = 0;
      //ConLogf("c=%d; equippable=%d; availcount=%d; pickTm=%u; tick=%u", c, (int)equippable, availEquipCount, pickTm, game::GetTick());
      if (Equipment) {
        {
          auto ffs = Equipment->TempDisableFluids();
          Equipment->AddInventoryEntry(this, Entry, 1, true);
        }
        AddSpecialEquipmentInfo(Entry, c);
        int ImageKey = game::AddToItemDrawVector(itemvector(1, Equipment));
        if (firstNonEmpty < 0 && equippable && !isArm && availEquipCount > 0) firstNonEmpty = c;
        if (equippable) {
          if (availEquipCount > 0 && isArm && armPickTime < pickTm) { armFirst = c; armPickTime = pickTm; }
          if (selPickTime < pickTm && equippable && !isArm) { selected = c; selPickTime = pickTm; }
        }
        // build help
        festring help = Equipment->GetDescriptiveInfo();
        if (isArm) {
          bodypart *arm = GetBodyPartOfEquipment(c);
          IvanAssert(arm);
          const int rate = GetWeaponTooHeavyRate(c);
          const int hitval = (rate != WEAPON_UNUSABLE ? arm->GetToHitValueRoundedMul10() : 0);
          festring ratestr;
          festring hitstr;
          switch (rate) {
            case WEAPON_SOMEWHAT_DIFFICULT:
              ratestr = CONST_S("\1WIt is somewhat difficult for you to use this item.\2");
              break;
            case WEAPON_MUCH_TROUBLE:
              ratestr = CONST_S("\1#a00|You have much trouble using this item.\2");
              break;
            case WEAPON_EXTREMLY_DIFFICULT:
              ratestr = CONST_S("\1#c00|It is extremly difficult for you to use this item.\2");
              break;
            case WEAPON_UNUSABLE:
              ratestr = CONST_S("\1RYou cannot use this item.\2");
              break;
          }
          if (hitval > 0 && GetAttribute(INTELLIGENCE) >= 8) {
            #if 0
            hitstr = CONST_S("\1OHit value: ~") + hitval / 10 + "." + hitval % 10 + "\2";
            #else
            Entry << " \1#880|[HV \1#CC0|~" << hitval / 10 << "." << hitval % 10 << "\1#880|]\2";
            #endif
          }
          if (!ratestr.IsEmpty() || !hitstr.IsEmpty()) {
            if (!help.IsEmpty()) help = CONST_S("\n\n") + help;
            if (!ratestr.IsEmpty() && !hitstr.IsEmpty()) ratestr += "\n";
            help = ratestr + hitstr + help;
          }
        }
        List.AddEntry(Entry, (availEquipCount ? ORANGE : LIGHT_GRAY), 20, ImageKey, true);
        List.AddLastEntryHelp(help);
      } else {
        truth canUse = !!GetBodyPartOfEquipment(c);
        Entry << (canUse ? "-" : "can't use");
        col16 color = RED;
        if (canUse && availEquipCount > 0) {
          if (firstEmpty < 0 && equippable && !isArm && availEquipCount > 0) firstEmpty = c;
          if (equippable && isArm && armFirst < 0) armFirst = c;
          switch (availEquipCount) {
            case 0: color = RED; break;
            case 1: color = LIGHT_GRAY; break;
            default: color = ORANGE; break;
          }
        }
        if (color != RED && equippable) {
          if (pickTm > selPickTime && !isArm) { selected = c; selPickTime = pickTm; }
        }
        List.AddEntry(Entry, color, 20, game::AddToItemDrawVector(itemvector()));
      }
    }
    game::DrawEverythingNoBlit();
    game::SetStandardListAttributes(List);

    //ConLogf("  selected=%d; firstEmpty=%d; firstNonEmpty=%d; armFirst=%d; armPickTime=%u", selected, firstEmpty, firstNonEmpty, armFirst, armPickTime);
    if (selected < 0) {
           if (armPickTime > 0) selected = armFirst;
      else if (firstEmpty >= 0) selected = firstEmpty;
      else if (firstNonEmpty >= 0) selected = firstNonEmpty;
    }

    if (selected >= 0) {
      selected = FixLeftRightSelected(selected);
      List.SetSelected(selected);
    }

    List.SetFlags(SELECTABLE|DRAW_BACKGROUND_AFTERWARDS|FE_COLON_HEADER);
    List.SetEntryDrawer(game::ItemEntryDrawer);
    Chosen = List.Draw();
    game::ClearItemDrawVector();
    if (Chosen >= GetEquipments()) break;
    EquipmentChanged = TryToChangeEquipment(MainStack, SecStack, Chosen);
  }

  if (EquipmentChanged) DexterityAction(5);
  return EquipmentChanged;
}


//==========================================================================
//
//  character::GetManagementFlags
//
//==========================================================================
feuLong character::GetManagementFlags () const {
  feuLong Flags = ALL_MANAGEMENT_FLAGS;
  if (!CanUseEquipment() || !AllowPlayerToChangeEquipment()) Flags &= ~CHANGE_EQUIPMENT;
  if (!GetStack()->GetItems()) Flags &= ~TAKE_ITEMS;
  if (!WillCarryItems()) Flags &= ~GIVE_ITEMS;
  if (!GetPossibleCommandFlags()) Flags &= ~ISSUE_COMMANDS;
  return Flags;
}


static cchar *VerbalBurdenState[] = { "overloaded", "stressed", "burdened", "unburdened" };
static col16 VerbalBurdenStateColor[] = { RED, BLUE, BLUE, WHITE };

cchar *character::GetVerbalBurdenState () const { return VerbalBurdenState[BurdenState]; }
col16 character::GetVerbalBurdenStateColor () const { return VerbalBurdenStateColor[BurdenState]; }
int character::GetAttributeAverage () const { return GetSumOfAttributes()/7; }


//==========================================================================
//
//  character::GetStandVerb
//
//==========================================================================
cfestring &character::GetStandVerb () const {
  if (ForceCustomStandVerb()) return DataBase->StandVerb;
  static festring Hovering = CONST_S("hovering");
  static festring Swimming = CONST_S("swimming");
  if (StateIsActivated(LEVITATION)) return Hovering;
  if (IsSwimming()) return Swimming;
  return DataBase->StandVerb;
}


//==========================================================================
//
//  character::CheckApply
//
//==========================================================================
truth character::CheckApply () const {
  if (!CanApply()) {
    ADD_MESSAGE("This monster type cannot apply.");
    return false;
  }
  return true;
}


//==========================================================================
//
//  character::EndLevitation
//
//==========================================================================
void character::EndLevitation () {
  if (!IsFlying() && GetSquareUnder()) {
    if (!game::IsInWilderness()) SignalStepFrom(0);
    if (game::IsInWilderness() || !GetLSquareUnder()->IsFreezed()) TestWalkability();
  }
}


//==========================================================================
//
//  character::CanMove
//
//==========================================================================
truth character::CanMove () const {
  return !IsRooted() || StateIsActivated(LEVITATION);
}


//==========================================================================
//
//  character::CalculateEnchantments
//
//==========================================================================
void character::CalculateEnchantments () {
  doforequipments()(this, &item::CalculateEnchantment);
  GetStack()->CalculateEnchantments();
}


//==========================================================================
//
//  character::GetNewFormForPolymorphWithControl
//
//==========================================================================
truth character::GetNewFormForPolymorphWithControl (character *&NewForm) {
  if (StateIsActivated(POLYMORPH_LOCK)) {
    ADD_MESSAGE("You feel uncertain about your body for a moment.");
    return false;
  }
  festring Topic, Temp;
  NewForm = 0;
  while (!NewForm) {
    festring Temp = game::DefaultQuestion(CONST_S("What do you want to become? [press \1YF1\2 for a list]"), game::GetDefaultPolymorphTo(), &game::PolymorphControlKeyHandler);
    NewForm = protosystem::CreateMonster(Temp);
    if (NewForm) {
      if (NewForm->IsSameAs(this)) {
        delete NewForm;
        ADD_MESSAGE("You choose not to polymorph.");
        NewForm = this;
        return false;
      }
      if (PolymorphBackup && NewForm->IsSameAs(PolymorphBackup)) {
        delete NewForm;
        NewForm = ForceEndPolymorph();
        return false;
      }
      if (NewForm->GetPolymorphIntelligenceRequirement() > GetAttribute(INTELLIGENCE) &&
          !game::WizardModeIsActive())
      {
        ADD_MESSAGE("You feel your mind isn't yet powerful enough to call forth the form of %s.",
                    NewForm->CHAR_NAME(INDEFINITE));
        delete NewForm;
        NewForm = 0;
      } else {
        NewForm->RemoveAllItems();
      }
    }
  }
  return true;
}


//==========================================================================
//
//  character::CreateSweat
//
//==========================================================================
liquid *character::CreateSweat (sLong Volume) const {
  //return liquid::Spawn(GetSweatMaterial(), Volume);
  return liquid::Spawn(GetCurrentSweatMaterial(), Volume);
}


//==========================================================================
//
//  character::TeleportRandomItem
//
//==========================================================================
truth character::TeleportRandomItem (truth TryToHinderVisibility) {
  if (IsImmuneToItemTeleport() || StateIsActivated(TELEPORT_LOCK)) return false;
  itemvector ItemVector;
  std::vector<sLong> PossibilityVector;
  int TotalPossibility = 0;
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
    ItemVector.push_back(*i);
    int Possibility = i->GetTeleportPriority();
    if (TryToHinderVisibility) Possibility += i->GetHinderVisibilityBonus(this);
    PossibilityVector.push_back(Possibility);
    TotalPossibility += Possibility;
  }
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment) {
      ItemVector.push_back(Equipment);
      int Possibility = Equipment->GetTeleportPriority();
      if (TryToHinderVisibility) Possibility += Equipment->GetHinderVisibilityBonus(this);
      PossibilityVector.push_back(Possibility <<= 1);
      TotalPossibility += Possibility;
    }
  }
  if (!TotalPossibility) return false;
  int Chosen = femath::WeightedRand(PossibilityVector, TotalPossibility);
  item *Item = ItemVector[Chosen];
  truth Equipped = (IsPlayer() ? PLAYER->Equips(Item) : false);
  truth Seen = Item->CanBeSeenByPlayer();
  Item->RemoveFromSlot();
  if (Seen) {
    ADD_MESSAGE("%s disappears.", Item->CHAR_NAME(DEFINITE));
  }
  if (Equipped) {
    festring msg = Item->ProcessMessage(CONST_S("!F:@Hsp @nu is lost!"), this);
    game::AskForEscPress(msg);
    //game::AskForEscPress(CONST_S("Equipment lost! (") + Item->CHAR_NAME(DEFINITE) + ")");
  }
  v2 Pos = GetPos();
  int Range = (Item->GetEmitation() && TryToHinderVisibility ? 25 : 5);
  rect Border(Pos + v2(-Range, -Range), Pos + v2(Range, Range));
  Pos = GetLevel()->GetRandomSquare(this, 0, &Border);
  if (Pos == ERROR_V2) {
    Pos = GetLevel()->GetRandomSquare();
    if (Pos == ERROR_V2) {
      ConLogf("oops. `character::TeleportRandomItem()` failed.");
      Item->SendToHell(); //???
      return true;
    }
  }
  GetNearLSquare(Pos)->GetStack()->AddItem(Item);
  if (Item->CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s appears.", Item->CHAR_NAME(INDEFINITE));
  }
  return true;
}


//==========================================================================
//
//  character::HasClearRouteTo
//
//==========================================================================
truth character::HasClearRouteTo (v2 Pos) const {
  pathcontroller::Map = GetLevel()->GetMap();
  pathcontroller::Character = this;
  v2 ThisPos = GetPos();
  return mapmath<pathcontroller>::DoLine(ThisPos.X, ThisPos.Y, Pos.X, Pos.Y, SKIP_FIRST|LINE_BOTH_DIRS);
}


//==========================================================================
//
//  character::IsTransparent
//
//==========================================================================
truth character::IsTransparent () const {
  return !IsEnormous() || GetTorso()->GetMainMaterial()->IsTransparent() || StateIsActivated(INVISIBLE);
}


//==========================================================================
//
//  character::SignalPossibleTransparencyChange
//
//==========================================================================
void character::SignalPossibleTransparencyChange () {
  if (!game::IsInWilderness()) {
    for (int c = 0; c < SquaresUnder; ++c) {
      lsquare *Square = GetLSquareUnder(c);
      if (Square) Square->SignalPossibleTransparencyChange();
    }
  }
}


//==========================================================================
//
//  character::GetCursorData
//
//==========================================================================
int character::GetCursorData () const {
  int Bad = 0;
  int Color = (game::PlayerIsRunning() ? BLUE_CURSOR : DARK_CURSOR);
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPart && BodyPart->IsUsable()) {
      int ConditionColorIndex = BodyPart->GetConditionColorIndex();
      if ((BodyPartIsVital(c) && !ConditionColorIndex) ||
          (ConditionColorIndex <= 1 && ++Bad == 2))
      {
        return Color|CURSOR_FLASH;
      }
    } else if (++Bad == 2) {
      return Color|CURSOR_FLASH;
    }
  }
  Color = (game::PlayerIsRunning() ? YELLOW_CURSOR : RED_CURSOR);
  return (Bad ? Color|CURSOR_FLASH : Color);
}


//==========================================================================
//
//  character::TryToName
//
//==========================================================================
void character::TryToName () {
  if (!IsPet()) {
    ADD_MESSAGE("%s refuses to let YOU decide what %s's called.", CHAR_NAME(DEFINITE), CHAR_PERSONAL_PRONOUN);
  } else if (IsPlayer()) {
    ADD_MESSAGE("You can't rename yourself.");
  } else if (!IsNameable()) {
    ADD_MESSAGE("%s refuses to be called anything else but %s.", CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
  } else {
    festring Topic = CONST_S("What name will you give to ")+GetName(DEFINITE)+'?';
    festring Name = game::StringQuestionEx(Topic, GetAssignedName(), WHITE, 0, 40, true);
    if (Name.GetSize()) SetAssignedName(Name);
  }
}


//==========================================================================
//
//  character::GetSituationDanger
//
//==========================================================================
double character::GetSituationDanger (ccharacter *Enemy, v2 ThisPos,
                                      v2 EnemyPos, truth SeesEnemy) const
{
  double Danger;
  if (IgnoreDanger() && !IsPlayer()) {
    if (Enemy->IgnoreDanger() && !Enemy->IsPlayer()) {
      Danger = double(GetHP())*GetHPRequirementForGeneration()/(Enemy->GetHP()*Enemy->GetHPRequirementForGeneration());
    }
    else {
      Danger = 0.25*GetHPRequirementForGeneration()/Enemy->GetHP();
    }
  } else if (Enemy->IgnoreDanger() && !Enemy->IsPlayer()) {
    Danger = 4.0*GetHP()/Enemy->GetHPRequirementForGeneration();
  } else {
    Danger = GetRelativeDanger(Enemy);
  }
  Danger *= 3.0/((EnemyPos-ThisPos).GetManhattanLength()+2);
  if (!SeesEnemy) Danger *= 0.2;
  if (StateIsActivated(PANIC)) Danger *= 0.2;
  Danger *= double(GetHP())*Enemy->GetMaxHP()/(Enemy->GetHP()*GetMaxHP());
  return Danger;
}


//==========================================================================
//
//  character::ModifySituationDanger
//
//==========================================================================
void character::ModifySituationDanger (double &Danger) const {
  switch (GetTirednessState()) {
    case FAINTING: Danger *= 1.5; /* fallthrough */
    case EXHAUSTED: Danger *= 1.25; /* fallthrough */
  }
  for (int c = 0; c < STATES; ++c) {
    if (StateIsActivated(1 << c) && StateData[c].SituationDangerModifier != 0) {
      (this->*StateData[c].SituationDangerModifier)(Danger);
    }
  }
}


//==========================================================================
//
//  character::LycanthropySituationDangerModifier
//
//==========================================================================
void character::LycanthropySituationDangerModifier (double &Danger) const {
  character *Wolf = werewolfwolf::Spawn();
  double DangerToWolf = GetRelativeDanger(Wolf);
  Danger *= pow(DangerToWolf, 0.1);
  delete Wolf;
}


//==========================================================================
//
//  character::PoisonedSituationDangerModifier
//
//==========================================================================
void character::PoisonedSituationDangerModifier (double &Danger) const {
  int C = GetTemporaryStateCounter(POISONED);
  Danger *= (1+(C*C)/(GetHP()*10000.0*(GetGlobalResistance(POISON)+1)));
}


//==========================================================================
//
//  character::PolymorphingSituationDangerModifier
//
//==========================================================================
void character::PolymorphingSituationDangerModifier (double &Danger) const {
  if (!StateIsActivated(POLYMORPH_CONTROL) && !StateIsActivated(POLYMORPH_LOCK)) {
    Danger *= 1.5;
  }
}


//==========================================================================
//
//  character::PanicSituationDangerModifier
//
//==========================================================================
void character::PanicSituationDangerModifier (double &Danger) const {
  Danger *= 1.5;
}


//==========================================================================
//
//  character::ConfusedSituationDangerModifier
//
//==========================================================================
void character::ConfusedSituationDangerModifier (double &Danger) const {
  Danger *= 1.5;
}


//==========================================================================
//
//  character::ParasitizedSituationDangerModifier
//
//==========================================================================
void character::ParasitizedSituationDangerModifier (double &Danger) const {
  Danger *= 1.25;
}


//==========================================================================
//
//  character::LeprosySituationDangerModifier
//
//==========================================================================
void character::LeprosySituationDangerModifier (double &Danger) const {
  Danger *= 1.5;
}


//==========================================================================
//
//  character::AddRandomScienceName
//
//==========================================================================
void character::AddRandomScienceName (festring &String) const {
  festring Science = GetScienceTalkName().GetRandomElement();
  if (Science[0] == '!') {
    String << Science.CStr()+1;
    return;
  }
  festring Attribute = GetScienceTalkAdjectiveAttribute().GetRandomElement();
  festring Prefix;
  truth NoAttrib = Attribute.IsEmpty(), NoSecondAdjective = false;
  if (!Attribute.IsEmpty() && Attribute[0] == '!') {
    NoSecondAdjective = true;
    Attribute.Erase(0, 1);
  }
  if (!Science.Find("the ")) {
    Science.Erase(0, 4);
    if (!Attribute.Find("the ", 0, 4)) Attribute << " the"; else Attribute.Insert(0, "the ", 4);
  }
  if (islower(Science[0]) && Science.Find(' ') == festring::NPos &&
      Science.Find('-') == festring::NPos &&
      Science.Find("phobia") == festring::NPos)
  {
    Prefix = GetScienceTalkPrefix().GetRandomElement();
    if (!Prefix.IsEmpty() && Science.Find(Prefix) != festring::NPos) Prefix.Empty();
  }
  int L = Prefix.GetSize();
  if (L && Prefix[L-1] == Science[0]) Science.Erase(0, 1);
  if (!NoAttrib && !NoSecondAdjective == !RAND_GOOD(3)) {
    int S1 = NoSecondAdjective ? 0 : GetScienceTalkAdjectiveAttribute().Size;
    int S2 = GetScienceTalkSubstantiveAttribute().Size;
    festring OtherAttribute;
    int Chosen = RAND_GOOD(S1+S2);
    if (Chosen < S1) OtherAttribute = GetScienceTalkAdjectiveAttribute()[Chosen];
    else OtherAttribute = GetScienceTalkSubstantiveAttribute()[Chosen - S1];
    if (!OtherAttribute.IsEmpty() && OtherAttribute.Find("the ", 0, 4) &&
        Attribute.Find(OtherAttribute) == festring::NPos)
    {
      String << Attribute << ' ' << OtherAttribute << ' ' << Prefix << Science;
      return;
    }
  }
  String << Attribute;
  if (!NoAttrib) String << ' ';
  String << Prefix << Science;
}


//==========================================================================
//
//  character::TryToTalkAboutScience
//
//==========================================================================
truth character::TryToTalkAboutScience () {
  #if 0
  ConLogf("TryToTalkAboutScience(%s)...", GetNameSingular().CStr());
  #endif
  if (GetRelation(PLAYER) == HOSTILE ||
      GetScienceTalkPossibility() <= RAND_GOOD(100) ||
      PLAYER->GetAttribute(INTELLIGENCE) < GetScienceTalkIntelligenceRequirement() ||
      PLAYER->GetAttribute(WISDOM) < GetScienceTalkWisdomRequirement() ||
      PLAYER->GetAttribute(CHARISMA) < GetScienceTalkCharismaRequirement())
  {
    return false;
  }

  #if 0
  ConLogf("...passed.");
  #endif

  festring Science;
  if (RAND_GOOD(3)) {
    AddRandomScienceName(Science);
  } else {
    festring S1, S2;
    AddRandomScienceName(S1);
    AddRandomScienceName(S2);
    if (S1.Find(S2) == festring::NPos && S2.Find(S1) == festring::NPos) {
      switch (RAND_GOOD(3)) {
        case 0: Science = "the relation of "; break;
        case 1: Science = "the differences of "; break;
        case 2: Science = "the similarities of "; break;
      }
      Science << S1 << " and " << S2;
    } else {
      AddRandomScienceName(Science);
    }
  }

  // this is totally UB. fuck you standards again.
  // k8:
  // i added `&`, so `%` will DEFINITELY generate something non-negative.
  // but tbh, i cannot see why it is done this way at all. if it was done
  // to add more randomness, then it definitely doesn't. this is no more
  // random than `RAND_N(10)` (and maybe even worse). it has absolutely
  // no dependence of the tick too.
  // also, i must admit that it is quite bad when you cannot trust your
  // PRNG to produce good enough random numbers, and try to help it with
  // some other "random" numbers. this will never work as you expect it to.
  // ORIGINAL CODE: switch (((RAND() + GET_TICK())&0x7fffffff) % 10)
  // p.s.: i believe that it was something like `GET_TICK() % 10` once, hence the mess.
  switch (RAND_N(10)) {
    case 0:
      ADD_MESSAGE("You have a rather pleasant chat about %s with %s.",
                  Science.CStr(), CHAR_DESCRIPTION(DEFINITE));
      break;
    case 1:
      ADD_MESSAGE("%s explains a few of %s opinions regarding %s to you.",
                  CHAR_DESCRIPTION(DEFINITE), CHAR_POSSESSIVE_PRONOUN, Science.CStr());
      break;
    case 2:
      ADD_MESSAGE("%s reveals a number of %s insightful views of %s to you.",
                  CHAR_DESCRIPTION(DEFINITE), CHAR_POSSESSIVE_PRONOUN, Science.CStr());
      break;
    case 3:
      ADD_MESSAGE("You exchange some information pertaining to %s with %s.",
                  Science.CStr(), CHAR_DESCRIPTION(DEFINITE));
      break;
    case 4:
      ADD_MESSAGE("You engage in a pretty intriguing conversation about %s with %s.",
                  Science.CStr(), CHAR_DESCRIPTION(DEFINITE));
      break;
    case 5:
      ADD_MESSAGE("You discuss at length about %s with %s.",
                  Science.CStr(), CHAR_DESCRIPTION(DEFINITE));
      break;
    case 6:
      ADD_MESSAGE("You have a somewhat boring talk concerning %s with %s.",
                  Science.CStr(), CHAR_DESCRIPTION(DEFINITE));
      break;
    case 7:
      ADD_MESSAGE("You are drawn into a heated argument regarding %s with %s.",
                  Science.CStr(), CHAR_DESCRIPTION(DEFINITE));
      break;
    case 8:
      ADD_MESSAGE("%s delivers a long monologue concerning eg. %s.",
                  CHAR_DESCRIPTION(DEFINITE), Science.CStr());
      break;
    case 9:
      ADD_MESSAGE("You dive into a brief but thought-provoking debate over %s with %s",
                  Science.CStr(), CHAR_DESCRIPTION(DEFINITE));
      break;
  }

  PLAYER->EditExperience(INTELLIGENCE, 1000, 50.0 * GetScienceTalkIntelligenceModifier() / ++ScienceTalks);
  PLAYER->EditExperience(WISDOM, 1000, 50.0 * GetScienceTalkWisdomModifier() / ++ScienceTalks);
  PLAYER->EditExperience(CHARISMA, 1000, 50.0 * GetScienceTalkCharismaModifier() / ++ScienceTalks);

  return true;
}


//==========================================================================
//
//  character::IsUsingWeaponOfCategory
//
//==========================================================================
truth character::IsUsingWeaponOfCategory (int Category) const {
  return
    ((GetMainWielded() && GetMainWielded()->GetWeaponCategory() == Category) ||
     (GetSecondaryWielded() && GetSecondaryWielded()->GetWeaponCategory() == Category));
}


//==========================================================================
//
//  character::TryToUnStickTraps
//
//==========================================================================
truth character::TryToUnStickTraps (v2 Dir) {
  if (!TrapData) return true;
  std::vector<trapdata> TrapVector;
  for (const trapdata *T = TrapData; T; T = T->Next) TrapVector.push_back(*TrapData);
  for (uInt c = 0; c < TrapVector.size(); ++c) {
    if (IsEnabled()) {
      entity *Trap = game::SearchTrap(TrapVector[c].TrapID);
      /*k8:??? if(!Trap->Exists()) int esko = esko = 2; */
      if (!Trap->Exists()) continue; /*k8: ??? added by me; what this means? */
      if (Trap->GetVictimID() == GetID() && Trap->TryToUnStick(this, Dir)) break;
    }
  }
  return !TrapData && IsEnabled();
}


struct trapidcomparer {
  trapidcomparer (feuLong ID) : ID(ID) {}
  truth operator () (const trapdata *T) const { return T->TrapID == ID; }
  feuLong ID;
};


//==========================================================================
//
//  character::RemoveTrap
//
//==========================================================================
void character::RemoveTrap (feuLong ID) {
  trapdata *&T = ListFind(TrapData, trapidcomparer(ID));
  T = T->Next;
  doforbodyparts()(this, &bodypart::SignalPossibleUsabilityChange);
}


//==========================================================================
//
//  character::AddTrap
//
//==========================================================================
void character::AddTrap (feuLong ID, feuLong BodyParts) {
  trapdata *&T = ListFind(TrapData, trapidcomparer(ID));
  if (T) T->BodyParts |= BodyParts;
  else T = new trapdata(ID, GetID(), BodyParts);
  doforbodyparts()(this, &bodypart::SignalPossibleUsabilityChange);
}


//==========================================================================
//
//  character::IsStuckToTrap
//
//==========================================================================
truth character::IsStuckToTrap (feuLong ID) const {
  for (const trapdata *T = TrapData; T; T = T->Next) if (T->TrapID == ID) return true;
  return false;
}


//==========================================================================
//
//  character::RemoveTraps
//
//==========================================================================
void character::RemoveTraps () {
  for (trapdata *T = TrapData; T; T = T->Next) {
    entity *Trap = game::SearchTrap(T->TrapID);
    if (Trap) Trap->UnStick();
  }
  deleteList(TrapData);
  doforbodyparts()(this, &bodypart::SignalPossibleUsabilityChange);
}


//==========================================================================
//
//  character::RemoveTraps
//
//==========================================================================
void character::RemoveTraps (int BodyPartIndex) {
  feuLong Flag = 1 << BodyPartIndex;
  for (trapdata **T = &TrapData; *T;) {
    if ((*T)->BodyParts & Flag) {
      entity *Trap = game::SearchTrap((*T)->TrapID);
      if (!((*T)->BodyParts &= ~Flag)) {
        if (Trap) Trap->UnStick();
        trapdata *ToDel = *T;
        *T = (*T)->Next;
        delete ToDel;
      } else {
        if (Trap) Trap->UnStick(BodyPartIndex);
        T = &(*T)->Next;
      }
    }
    else {
      T = &(*T)->Next;
    }
  }
  if (GetBodyPart(BodyPartIndex)) GetBodyPart(BodyPartIndex)->SignalPossibleUsabilityChange();
}


//==========================================================================
//
//  character::GetTrapDescription
//
//==========================================================================
festring character::GetTrapDescription () const {
  festring Desc;
  std::pair<entity *, int> TrapStack[3];
  int Index = 0;
  for (const trapdata *T = TrapData; T; T = T->Next) {
    if (Index < 3) {
      entity *Trap = game::SearchTrap(T->TrapID);
      if (Trap) {
        int c;
        for (c = 0; c < Index; ++c) if (TrapStack[c].first->GetTrapType() == Trap->GetTrapType()) ++TrapStack[c].second;
        if (c == Index) TrapStack[Index++] = std::make_pair(Trap, 1);
      }
    } else {
      ++Index;
      break;
    }
  }
  if (Index <= 3) {
    TrapStack[0].first->AddTrapName(Desc, TrapStack[0].second);
    if (Index == 2) {
      Desc << " and ";
      TrapStack[1].first->AddTrapName(Desc, TrapStack[1].second);
    } else if (Index == 3) {
      Desc << ", ";
      TrapStack[1].first->AddTrapName(Desc, TrapStack[1].second);
      Desc << " and ";
      TrapStack[2].first->AddTrapName(Desc, TrapStack[2].second);
    }
  } else {
    Desc << "lots of traps";
  }
  return Desc;
}


//==========================================================================
//
//  character::RandomizeHurtBodyPart
//
//==========================================================================
int character::RandomizeHurtBodyPart (feuLong BodyParts) const {
  int BodyPartIndex[MAX_BODYPARTS];
  int Index = 0;
  for (int c = 0; c < GetBodyParts(); ++c) {
    if (1 << c & BodyParts) {
      /*k8: ??? if(!GetBodyPart(c)) int esko = esko = 2; */
      if (!GetBodyPart(c)) continue;
      BodyPartIndex[Index++] = c;
    }
    /*k8: ??? if(!Index) int esko = esko = 2;*/
  }
  if (!Index) {
    ConLogf("FATAL: RandomizeHurtBodyPart -- Index==0");
    abort();
  }
  return BodyPartIndex[RAND_N(Index)];
}


//==========================================================================
//
//  character::BodyPartIsStuck
//
//==========================================================================
truth character::BodyPartIsStuck (int I) const {
  for (const trapdata *T = TrapData; T; T = T->Next) if (1 << I & T->BodyParts) return true;
  return false;
}


//==========================================================================
//
//  character::AllowUnconsciousness
//
//==========================================================================
truth character::AllowUnconsciousness () const {
  return (DataBase->AllowUnconsciousness && TorsoIsAlive());
}


//==========================================================================
//
//  character::CanPanic
//
//==========================================================================
truth character::CanPanic () const {
  return (!Action || !Action->IsUnconsciousness() || !StateIsActivated(FEARLESS));
}


//==========================================================================
//
//  character::GetRandomBodyPart
//
//==========================================================================
int character::GetRandomBodyPart (feuLong Possible) const {
  int OKBodyPart[MAX_BODYPARTS];
  int OKBodyParts = 0;
  for (int c = 0; c < BodyParts; ++c) if (1 << c & Possible && GetBodyPart(c)) OKBodyPart[OKBodyParts++] = c;
  return OKBodyParts ? OKBodyPart[RAND_N(OKBodyParts)] : NONE_INDEX;
}


//==========================================================================
//
//  character::EditNP
//
//==========================================================================
void character::EditNP (sLong What) {
  int OldState = GetHungerState();
  NP += What;
  int NewState = GetHungerState();
  if (OldState > VERY_HUNGRY && NewState == VERY_HUNGRY) DeActivateVoluntaryAction(CONST_S("You are getting really hungry."));
  if (OldState > STARVING && NewState == STARVING) DeActivateVoluntaryAction(CONST_S("You are getting extremely hungry."));
}


//==========================================================================
//
//  character::IsSwimming
//
//==========================================================================
truth character::IsSwimming () const {
  return !IsFlying() && GetSquareUnder() && GetSquareUnder()->GetSquareWalkability() & SWIM;
}


//==========================================================================
//
//  character::AddBlackUnicornConsumeEndMessage
//
//==========================================================================
void character::AddBlackUnicornConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel dirty and loathsome.");
  }
}


//==========================================================================
//
//  character::AddGrayUnicornConsumeEndMessage
//
//==========================================================================
void character::AddGrayUnicornConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel neutralized.");
  }
}


//==========================================================================
//
//  character::AddWhiteUnicornConsumeEndMessage
//
//==========================================================================
void character::AddWhiteUnicornConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel purified.");
  }
}


//==========================================================================
//
//  character::AddOmmelBoneConsumeEndMessage
//
//==========================================================================
void character::AddOmmelBoneConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel the power of all your canine ancestors combining in your body.");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("For a moment %s looks extremely ferocious. You shudder.",
                CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::AddLiquidHorrorConsumeEndMessage
//
//==========================================================================
void character::AddLiquidHorrorConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("Untold horrors flash before your eyes. The melancholy of the world "
                "is on your shoulders!");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s looks as if the melancholy of the world "
                "is on %s shoulders!.", CHAR_NAME(DEFINITE),
                GetPossessivePronoun().CStr());
  }
}


//==========================================================================
//
//  character::AddAlienFleshConsumeEndMessage
//
//==========================================================================
void character::AddAlienFleshConsumeEndMessage () const {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel somehow sick by eating such acidic corpse...");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s looks like he eat something bad.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::GetBodyPartSparkleFlags
//
//==========================================================================
int character::GetBodyPartSparkleFlags (int) const {
  return
    ((GetNaturalSparkleFlags() & SKIN_COLOR ? SPARKLING_A : 0) |
     (GetNaturalSparkleFlags() & TORSO_MAIN_COLOR ? SPARKLING_B : 0) |
     (GetNaturalSparkleFlags() & TORSO_SPECIAL_COLOR ? SPARKLING_D : 0));
}


//==========================================================================
//
//  character::IsAnimated
//
//==========================================================================
truth character::IsAnimated () const {
  return combinebodypartpredicates()(this, &bodypart::IsAnimated, 1);
}


//==========================================================================
//
//  character::GetNaturalExperience
//
//==========================================================================
double character::GetNaturalExperience (int Identifier) const {
  return DataBase->NaturalExperience[Identifier];
}


//==========================================================================
//
//  character::HasBodyPart
//
//==========================================================================
truth character::HasBodyPart (sorter Sorter) const {
  if (Sorter == 0) return true;
  return combinebodypartpredicateswithparam<ccharacter*>()(this, Sorter, this, 1);
}


//==========================================================================
//
//  character::PossessesItem
//
//==========================================================================
truth character::PossessesItem (sorter Sorter) const {
  if (Sorter == 0) return true;
  return
    (GetStack()->SortedItems(this, Sorter) ||
     combinebodypartpredicateswithparam<ccharacter*>()(this, Sorter, this, 1) ||
     combineequipmentpredicateswithparam<ccharacter*>()(this, Sorter, this, 1));
}


//==========================================================================
//
//  character::MoreThanOnePossessesItem
//
//==========================================================================
truth character::MoreThanOnePossessesItem (sorter Sorter) const {
  if (Sorter) {
    int count = 0;
    for (int c = 0; c < BodyParts; ++c) {
      bodypart *BodyPart = GetBodyPart(c);
      if (BodyPart && (Sorter == 0 || (BodyPart->*Sorter)(this))) {
        if (++count > 1) return true;
      }
    }
    for (int c = 0; c < GetEquipments(); ++c) {
      item *Equipment = GetEquipment(c);
      if (Equipment && (Sorter == 0 || (Equipment->*Sorter)(this))) {
        if (++count > 1) return true;
      }
    }
    for (int c = 0; c < GetStack()->GetItems(); ++c) {
      item *Stk = GetStack()->GetItem(c);
      if (Stk && (Sorter == 0 || (Stk->*Sorter)(this))) {
        if (++count > 1) return true;
      }
    }
    return false;
  }
  return false;
}


//==========================================================================
//
//  character::FirstPossessesItem
//
//==========================================================================
item *character::FirstPossessesItem (sorter Sorter) const {
  if (Sorter) {
    for (int c = 0; c < BodyParts; ++c) {
      bodypart *BodyPart = GetBodyPart(c);
      if (BodyPart && (Sorter == 0 || (BodyPart->*Sorter)(this))) return BodyPart;
    }
    for (int c = 0; c < GetEquipments(); ++c) {
      item *Equipment = GetEquipment(c);
      if (Equipment && (Sorter == 0 || (Equipment->*Sorter)(this))) return Equipment;
    }
    for (int c = 0; c < GetStack()->GetItems(); ++c) {
      item *Stk = GetStack()->GetItem(c);
      if (Stk && (Sorter == 0 || (Stk->*Sorter)(this))) return Stk;
    }
  }
  return 0;
}


//==========================================================================
//
//  character::GetRunDescriptionLine
//
//  0 <= I <= 1
//
//==========================================================================
cchar *character::GetRunDescriptionLine (int I) const {
  if (!GetRunDescriptionLineOne().IsEmpty()) {
    return (!I ? GetRunDescriptionLineOne().CStr() : GetRunDescriptionLineTwo().CStr());
  }
  if (IsFlying()) return (!I ? "Flying" : "very fast");
  if (IsSwimming()) {
    if (IsPlayer() && game::IsInWilderness() && game::PlayerHasBoat()) {
      return (!I ? "Sailing" : " very fast");
    }
    return (!I ? "Swimming" : "very fast");
  }
  return (!I ? "Running" : "");
}


//==========================================================================
//
//  character::VomitAtRandomDirection
//
//==========================================================================
void character::VomitAtRandomDirection (int Amount) {
  if (game::IsInWilderness() || IsLarge() || Amount <= 0) return;
  /* Lacks support of multitile monsters */
  v2 Possible[9];
  int Index = 0;
  for (int d = 0; d < 9; ++d) {
    lsquare *Square = GetLSquareUnder()->GetNeighbourLSquare(d);
    if (Square && !Square->VomitingIsDangerous(this)) {
      Possible[Index++] = Square->GetPos();
    }
  }
  if (Index) {
    Vomit(Possible[RAND_N(Index)], Amount);
  } else {
    Vomit(GetPos(), Amount);
  }
}


//==========================================================================
//
//  character::RemoveLifeSavers
//
//==========================================================================
void character::RemoveLifeSavers () {
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment && Equipment->IsInCorrectSlot(c) && Equipment->GetGearStates() & LIFE_SAVED) {
      Equipment->SendToHell();
      Equipment->RemoveFromSlot();
    }
  }
}


//==========================================================================
//
//  character::FindCarrier
//
//==========================================================================
ccharacter *character::FindCarrier () const {
  return this; //check
}


//==========================================================================
//
//  character::PrintBeginHiccupsMessage
//
//==========================================================================
void character::PrintBeginHiccupsMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Your diaphragm is spasming vehemently.");
}


//==========================================================================
//
//  character::PrintEndHiccupsMessage
//
//==========================================================================
void character::PrintEndHiccupsMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel your annoying hiccoughs have finally subsided.");
}


//==========================================================================
//
//  character::HiccupsHandler
//
//==========================================================================
void character::HiccupsHandler () {
  /*
  if (!RAND_N(2000)) {
    if (IsPlayer()) ADD_MESSAGE("");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("");
    else if ((PLAYER->GetPos()-GetPos()).GetLengthSquare() <= 400) ADD_MESSAGE("");
    game::CallForAttention(GetPos(), 400);
  }
  */
}


//==========================================================================
//
//  character::VampirismHandler
//
//==========================================================================
void character::VampirismHandler () {
  //EditExperience(ARM_STRENGTH, -25, 1 << 1);
  //EditExperience(LEG_STRENGTH, -25, 1 << 1);
  //EditExperience(DEXTERITY, -25, 1 << 1);
  //EditExperience(AGILITY, -25, 1 << 1);
  //EditExperience(ENDURANCE, -25, 1 << 1);
  EditExperience(CHARISMA, -25, 1 << 1);
  EditExperience(WISDOM, -25, 1 << 1);
  EditExperience(INTELLIGENCE, -25, 1 << 1);
  CheckDeath(CONST_S("killed by vampirism"));
}


//==========================================================================
//
//  character::HiccupsSituationDangerModifier
//
//==========================================================================
void character::HiccupsSituationDangerModifier (double &Danger) const {
  Danger *= 1.25;
}


//==========================================================================
//
//  character::VampirismSituationDangerModifier
//
//==========================================================================
void character::VampirismSituationDangerModifier (double &Danger) const {
  character *Vampire = vampire::Spawn();
  double DangerToVampire = GetRelativeDanger(Vampire);
  Danger *= pow(DangerToVampire, 0.1);
  delete Vampire;
}


//==========================================================================
//
//  character::IsConscious
//
//==========================================================================
bool character::IsConscious () const {
  return !Action || !Action->IsUnconsciousness();
}


//==========================================================================
//
//  character::GetNearWSquare
//
//==========================================================================
wsquare *character::GetNearWSquare (v2 Pos) const {
  return static_cast<wsquare *>(GetSquareUnder()->GetArea()->GetSquare(Pos));
}


//==========================================================================
//
//  character::GetNearWSquare
//
//==========================================================================
wsquare *character::GetNearWSquare (int x, int y) const {
  return static_cast<wsquare *>(GetSquareUnder()->GetArea()->GetSquare(x, y));
}


//==========================================================================
//
//  character::ForcePutNear
//
//==========================================================================
void character::ForcePutNear (v2 Pos) {
  /* GUM SOLUTION!!! */
  v2 NewPos = game::GetCurrentLevel()->GetNearestFreeSquare(PLAYER, Pos, false);
  if (NewPos == ERROR_V2) {
    do {
      NewPos = game::GetCurrentLevel()->GetRandomSquare(this, 0, nullptr, NewPos);
      if (NewPos == ERROR_V2) ABORT("No country for the old fart...");
    } while (NewPos == Pos);
  }
  PutTo(NewPos);
}


//==========================================================================
//
//  character::ReceiveMustardGas
//
//==========================================================================
void character::ReceiveMustardGas (int BodyPart, sLong Volume) {
  if (Volume && GetBodyPart(BodyPart)) {
    bodypart *bp = GetBodyPart(BodyPart);
    if (bp) {
      bp->AddFluid(liquid::Spawn(MUSTARD_GAS_LIQUID, Volume), CONST_S("skin"), 0, true);
    }
  }
}


//==========================================================================
//
//  character::ReceiveMustardGasLiquid
//
//==========================================================================
void character::ReceiveMustardGasLiquid (int BodyPartIndex, sLong Modifier) {
  bodypart *BodyPart = GetBodyPart(BodyPartIndex);
  if (BodyPart && BodyPart->GetMainMaterial() &&
      (BodyPart->GetMainMaterial()->GetInteractionFlags() & IS_AFFECTED_BY_MUSTARD_GAS))
  {
    sLong Tries = Modifier;
    Modifier -= Tries; //opt%?
    int Damage = 0;
    for (sLong c = 0; c < Tries; ++c) {
      if (!RAND_N(100)) {
        Damage += 1;
      }
    }
    if (Modifier && !(RAND_N(1000) / Modifier)) {
      Damage += 1;
    }
    if (Damage) {
      feuLong Minute = game::GetTotalMinutes();
      if (GetLastAcidMsgMin() != Minute && (CanBeSeenByPlayer() || IsPlayer())) {
        SetLastAcidMsgMin(Minute);
        if (IsPlayer()) {
          ADD_MESSAGE("Mustard gas dissolves the skin of your %s.", BodyPart->GetBodyPartName().CStr());
        } else {
          ADD_MESSAGE("Mustard gas dissolves %s.", CHAR_NAME(DEFINITE));
        }
      }
      ReceiveBodyPartDamage(0, Damage, MUSTARD_GAS_DAMAGE, BodyPartIndex, YOURSELF, false, false, false);
      CheckDeath(CONST_S("killed by a fatal exposure to mustard gas"));
    }

    if (IsPlayer()) {
      action *Action = GetAction();
      if (Action && Action->IsRest() && !Action->InDNDMode() &&
          BodyPartIsVital(BodyPartIndex) && BodyPart->IsBadlyHurt())
      {
        ADD_MESSAGE("You're about to die from mustard gas.");
        if (game::TruthQuestion(CONST_S("Mustard gas is dissolving your flesh.\n"
                                        "Continue resting?"), NO))
        {
          Action->ActivateInDNDMode();
        } else {
          Action->Terminate(false);
        }
      }
    }
  }
}


//==========================================================================
//
//  character::IsBadPath
//
//==========================================================================
truth character::IsBadPath (v2 Pos) const {
  if (!IsGoingSomeWhere()) return false;
  v2 TPos = !Route.empty() ? Route.back() : GoingTo;
  return ((TPos - Pos).GetManhattanLength() > (TPos - GetPos()).GetManhattanLength());
}


//==========================================================================
//
//  character::GetExpModifierRef
//
//==========================================================================
/*k8: wtf is this?
double &character::GetExpModifierRef (expid E) {
  return ExpModifierMap.insert(std::make_pair(E, 1.0)).first->second;
}
*/


//==========================================================================
//
//  character::ForgetRandomThing
//
//  Should probably do more. Now only makes Player forget gods.
//
//==========================================================================
truth character::ForgetRandomThing () {
  if (IsPlayer()) {
    /* hopefully this code isn't somewhere else */
    std::vector<god *> Known;
    for (int c = 1; c <= GODS; ++c) {
      if (game::GetGod(c)->IsKnown()) {
        Known.push_back(game::GetGod(c));
      }
    }
    if (Known.empty()) return false;
    int RandomGod = RAND_N(Known.size());
    Known.at(RAND_N(Known.size()))->SetIsKnown(false);
    ADD_MESSAGE("You forget how to pray to %s.", Known.at(RandomGod)->GetName());
    return true;
  }
  return false;
}


//==========================================================================
//
//  character::CheckForBlock
//
//==========================================================================
int character::CheckForBlock (character *Enemy, item *Weapon, double ToHitValue,
                              int Damage, int Success, int Type)
{
  return Damage;
};


//==========================================================================
//
//  character::ApplyAllGodsKnownBonus
//
//==========================================================================
void character::ApplyAllGodsKnownBonus () {
  stack *AddPlace = GetStackUnder();
  if (game::IsInWilderness()) AddPlace = GetStack(); else AddPlace = GetStackUnder();
  pantheonbook *NewBook = pantheonbook::Spawn();
  AddPlace->AddItem(NewBook);
  ADD_MESSAGE("\"MORTAL! BEHOLD THE HOLY SAGA\"");
  ADD_MESSAGE("%s materializes near your feet.", NewBook->CHAR_NAME(INDEFINITE));
}


//==========================================================================
//
//  character::ReceiveSirenSong
//
//==========================================================================
truth character::ReceiveSirenSong (character *Siren) {
  // if it cannot hear, the song has no effect
  if (!CanHear()) return false;

  //if (Siren->GetTeam() == GetTeam()) return false;
  // from comm. fork -- so elven emissary will not steal your equipment...
  // ...or won't try to steal equipment of Attnam citizens.
  //FIXME: i'm not sure if i want this. that bitch was nasty in IVAN way, i believe...
  if (Siren->GetRelation(this) != HOSTILE) {
    // let Elianise to steal stuff from the player. nobody cares if
    // The Ambassador takes some goodies from a dirty peasant.
    if (!IsPlayer() || Siren->GetConfig() != AMBASSADOR_SIREN) {
      return false;
    }
  }

  // full helmets has SoundRes 1, Aegis shield has SoundRes 2.
  // ESP items usually have SongRes 8, and the effect is cumulative.
  // broken items provide half of the protection.
  int chance = 0;
  int sresist = 0;
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Item = GetEquipment(c);
    if (Item) {
      //k8: dunno why i restricted it, there is no reson for doing that.
      //k8: Aegis provides sound resistance, why not? and weapons could have song resistance.
      //if (Item->IsHelmet(this) || Item->IsRing(this) || Item->IsAmulet(this))
      {
        // song resistance is cumulative
        int songres = Item->GetSirenSongResistance();
        if (Item->IsBroken()) {
          songres = Div2Ass(songres);
        } else {
          songres = Max(0, songres + Item->GetEnchantment() * 2);
        }
        chance += songres; // the effect is cumulative; i may revise this later
        // sound resistance is the one which is greater
        int soundres = Item->GetResistance(SOUND) * 10;
        if (Item->IsBroken()) {
          soundres = Div2Ass(soundres);
        } else {
          soundres = Max(0, soundres + Item->GetEnchantment() * 4);
        }
        sresist = Max(sresist, soundres);
      }
    }
  }
  #if 0
  ConLogf("chance=%d; sresist=%d", chance, sresist);
  #endif

  /*
  if (chance <= 0 && sresist > 0) {
    chance = sresist * 10;
  } else if (chance > 0 && sresist > 0) {
    chance += Div2Ass(10 * sresist);
  }
  */
  // if we have some resistance from ESP items, nerf sound resistance
  if (chance > 4) sresist = Div2Ass(sresist);
  // now sum them
  chance = Max(chance + sresist, 4/*default*/);

  if (!RAND_N(chance)) {
    if (IsPlayer()) {
      ADD_MESSAGE("The beautiful melody of %s makes you feel sleepy.",
                  Siren->CHAR_NAME(DEFINITE));
    } else if (PLAYER->CanHear() && CanBeSeenByPlayer()) {
      ADD_MESSAGE("The beautiful melody of %s makes %s look sleepy.",
                  Siren->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
    }
    EditStamina(-((1 + RAND_N(4)) * 10000), true); // from comm. fork
    return true;
  }

  if (!IsPlayer() && IsCharmable() && CanTameWithDulcis(Siren) &&
      /*!RAND_N(5)*/!RAND_N(chance + chance / 3))
  {
    ChangeTeam(Siren->GetTeam());
    if (PLAYER->CanHear()) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s seems to be totally brainwashed by %s melodies.",
                    CHAR_NAME(DEFINITE), Siren->CHAR_NAME(DEFINITE));
      } else {
        ADD_MESSAGE("You hear a beautiful song.");
      }
    }
    return true;
  }

  if (!IsImmuneToWhipOfThievery() && !RAND_N(chance)) {
    item *What = GiveMostExpensiveItem(Siren);
    if (What) {
      if (IsPlayer()) {
        ADD_MESSAGE("%s music persuades you to give %s to %s as a present.",
                    Siren->CHAR_NAME(DEFINITE), What->CHAR_NAME(DEFINITE),
                    Siren->CHAR_OBJECT_PRONOUN);
      } else if (PLAYER->CanHear() && CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s is persuated to give %s to %s because of %s beautiful singing.",
                    CHAR_NAME(DEFINITE), What->CHAR_NAME(INDEFINITE),
                    Siren->CHAR_NAME(DEFINITE), Siren->CHAR_OBJECT_PRONOUN);
      } else if (PLAYER->CanHear()) {
        ADD_MESSAGE("You hear a beautiful song.");
      }
    } else {
      if (IsPlayer()) {
        ADD_MESSAGE("You would like to give something to %s.", Siren->CHAR_NAME(DEFINITE));
      } else if (PLAYER->CanHear() && CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s looks longingly at %s.", CHAR_NAME(DEFINITE), Siren->CHAR_NAME(DEFINITE));
      } else if (PLAYER->CanHear()) {
        ADD_MESSAGE("You hear a beautiful song.");
      }
    }
    return true;
  }

  return false;
}


//==========================================================================
//
//  character::ReceiveSickness
//
//==========================================================================
void character::ReceiveSickness (sLong Amount) {
  if (IsPlayer() && !RAND_N(10)) {
    ADD_MESSAGE("You don't feel so good.");
  }

  if (!StateIsActivated(DISEASE_IMMUNITY) && !RAND_N(10)) {
    switch (RAND_N(5)) {
     case 0: BeginTemporaryState(LYCANTHROPY, Amount); break;
     case 1: BeginTemporaryState(VAMPIRISM, Amount); break;
     case 2: BeginTemporaryState(PARASITE_TAPE_WORM, Amount); break;
     case 3: BeginTemporaryState(PARASITE_MIND_WORM, Amount); break;
     case 4: GainIntrinsic(LEPROSY); break;
    }
  }

  if (!RAND_N(10)) {
    BeginTemporaryState(POISONED, Amount + RAND_N(Amount));
  }
}


//==========================================================================
//
//  character::FindFirstEquippedItem
//
//==========================================================================
item *character::FindFirstEquippedItem (cfestring &aclassname, int aconfig) const {
  for (int f = 0; f < GetEquipments(); ++f) {
    item *it = GetEquipment(f);
    if (!it) continue;
    if (aclassname.EquCI(it->GetClassID())) {
      if (aconfig == -1 || it->GetConfig() == aconfig) return it;
    }
    /*
    for (uInt c = 0; c < it->GetDataBase()->Alias.Size; ++c) {
      if (s.CompareIgnoreCase(it->GetDataBase()->Alias[c]) == 0) return it;
    }
    */
  }
  return nullptr;
}


//==========================================================================
//
//  character::FindFirstInventoryItem
//
//==========================================================================
item *character::FindFirstInventoryItem (cfestring &aclassname, int aconfig) const {
  itemvector items;
  GetStack()->FillItemVector(items);
  for (int f = 0; f < (int)items.size(); ++f) {
    item *it = items[f];
    if (!it) continue;
    if (aclassname.EquCI(it->GetClassID())) {
      if (aconfig == -1 || it->GetConfig() == aconfig) return it;
    }
  }
  return nullptr;
}


//==========================================================================
//
//  character::FindFirstItem
//
//==========================================================================
item *character::FindFirstItem (cfestring &aclassname, int aconfig) const {
  item *it = FindFirstInventoryItem(aclassname, aconfig);
  if (!it) it = FindFirstEquippedItem(aclassname, aconfig);
  return it;
}


//==========================================================================
//
//  character::FindMostExpensiveItem
//
//  return 0, if no item found
//
//==========================================================================
item *character::FindMostExpensiveItem () const {
  int MaxPrice = -1;
  item *MostExpensive = 0;
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
    if ((*i)->GetPrice() > MaxPrice) {
      MaxPrice = (*i)->GetPrice();
      MostExpensive = (*i);
    }
  }
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment && Equipment->GetPrice() > MaxPrice) {
      MaxPrice = Equipment->GetPrice();
      MostExpensive = Equipment;
    }
  }
  return MostExpensive;
}


//==========================================================================
//
//  character::GiveMostExpensiveItem
//
//  returns 0 if no items available
//
//==========================================================================
item *character::GiveMostExpensiveItem (character *ToWhom) {
  item *ToGive = FindMostExpensiveItem();
  if (!ToGive) return 0;
  //truth Equipped = (IsPlayer() ? PLAYER->Equips(ToGive) : false);
  ToGive->RemoveFromSlot();
  //if (Equipped)
  if (IsPlayer()) {
    festring msg = ToGive->ProcessMessage(CONST_S("!F:@Hsp @nu is lost!"), this);
    game::AskForEscPress(msg);
    //game::AskForEscPress(CONST_S("Equipment lost! (") + ToGive->CHAR_NAME(DEFINITE) + ")");
  }
  ToWhom->ReceiveItemAsPresent(ToGive);
  EditAP(-1000);
  return ToGive;
}


//==========================================================================
//
//  character::StealItemFrom
//
//==========================================================================
void character::StealItemFrom (character *Victim, item *ToSteal) {
  if (ToSteal) {
    ToSteal->RemoveFromSlot();
    GetStack()->AddItem(ToSteal);
    if (Victim) {
      if (Victim->IsPlayer()) {
        if (CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s steals your %s.", CHAR_NAME(DEFINITE), ToSteal->CHAR_NAME(UNARTICLED));
        } else {
          ADD_MESSAGE("Something steals your %s.", ToSteal->CHAR_NAME(UNARTICLED));
        }
        festring msg = ToSteal->ProcessMessage(CONST_S("!F:@Hsp @nu is lost!"), Victim);
        game::AskForEscPress(msg);
      } else if (CanBeSeenByPlayer() && Victim->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s steals %s %s.",
                    CHAR_NAME(DEFINITE), Victim->GetPossessivePronoun().CStr(),
                    ToSteal->CHAR_NAME(UNARTICLED));
      }
    }
  }
}


//==========================================================================
//
//  character::ReceiveItemAsPresent
//
//==========================================================================
void character::ReceiveItemAsPresent (item *Present) {
  if (TestForPickup(Present)) {
    GetStack()->AddItem(Present);
  } else {
    GetStackUnder()->AddItem(Present);
  }
}


//==========================================================================
//
//  character::GetNearestEnemy
//
//  returns 0 if no enemies in sight
//
//==========================================================================
character *character::GetNearestEnemy () const {
  character *NearestEnemy = 0;
  sLong NearestEnemyDistance = 0x7FFFFFFF;
  v2 Pos = GetPos();
  for (int c = 0; c < game::GetTeams(); ++c) {
    if (GetTeam()->GetRelation(game::GetTeam(c)) == HOSTILE) {
      for (std::list<character*>::const_iterator i = game::GetTeam(c)->GetMember().begin(); i != game::GetTeam(c)->GetMember().end(); ++i) {
        if ((*i)->IsEnabled()) {
          sLong ThisDistance = Max<sLong>(abs((*i)->GetPos().X - Pos.X), abs((*i)->GetPos().Y - Pos.Y));
          if ((ThisDistance < NearestEnemyDistance ||
               (ThisDistance == NearestEnemyDistance && !RAND_N(3))) &&
              (*i)->CanBeSeenBy(this))
          {
            NearestEnemy = *i;
            NearestEnemyDistance = ThisDistance;
          }
        }
      }
    }
  }
  return NearestEnemy;
}


//==========================================================================
//
//  character::CanTameWithDulcis
//
//==========================================================================
truth character::CanTameWithDulcis (const character *Tamer) const {
  int TamingDifficulty = GetTamingDifficulty();
  if (TamingDifficulty == NO_TAMING) return false;
  if (GetAttachedGod() == DULCIS) return true;
  int Modifier = Tamer->GetAttribute(WISDOM) + Tamer->GetAttribute(CHARISMA);
  if (Tamer->IsPlayer()) Modifier += game::GetGod(DULCIS)->GetRelation() / 20;
  else if (Tamer->GetAttachedGod() == DULCIS) Modifier += 50;
  if (TamingDifficulty == 0) {
    if (!IgnoreDanger()) TamingDifficulty = int(10 * GetRelativeDanger(Tamer));
    else TamingDifficulty = 10 * GetHPRequirementForGeneration()/Max(Tamer->GetHP(), 1);
  }
  return Modifier >= TamingDifficulty * 3;
}


//==========================================================================
//
//  character::CanTameWithLyre
//
//==========================================================================
truth character::CanTameWithLyre (const character *Tamer) const {
  int TamingDifficulty = GetTamingDifficulty();
  if (TamingDifficulty == NO_TAMING) return false;
  if (TamingDifficulty == 0) {
    if (!IgnoreDanger()) TamingDifficulty = int(10 * GetRelativeDanger(Tamer));
    else TamingDifficulty = 10*GetHPRequirementForGeneration()/Max(Tamer->GetHP(), 1);
  }
  return Tamer->GetAttribute(CHARISMA) >= TamingDifficulty;
}


//==========================================================================
//
//  character::CanTameWithScroll
//
//==========================================================================
truth character::CanTameWithScroll (const character *Tamer) const {
  int TamingDifficulty = GetTamingDifficulty();
  return
    (TamingDifficulty != NO_TAMING &&
     (TamingDifficulty == 0 ||
      Tamer->GetAttribute(INTELLIGENCE) * 4 + Tamer->GetAttribute(CHARISMA) >= TamingDifficulty * 5));
}


//==========================================================================
//
//  character::CanTameWithResurrection
//
//==========================================================================
truth character::CanTameWithResurrection (const character *Tamer) const {
  int TamingDifficulty = GetTamingDifficulty();
  if (TamingDifficulty == NO_TAMING) return false;
  if (TamingDifficulty == 0) return true;
  return (Tamer->GetAttribute(CHARISMA) >= TamingDifficulty/2);
}


//==========================================================================
//
//  character::CheckSadism
//
//==========================================================================
truth character::CheckSadism () {
  if (!IsSadist() || !HasSadistAttackMode() || !IsSmall()) return false; // gum
  if (!RAND_N(10)) {
    for (int d = 0; d < MDIR_STAND; ++d) {
      square *Square = GetNeighbourSquare(d);
      if (Square) {
        character *Char = Square->GetCharacter();
        if (Char && Char->IsMasochist() && GetRelation(Char) == FRIEND &&
            Char->GetHP() * 3 >= Char->GetMaxHP() * 2 &&
            HitWithVFX(Char, Square->GetPos(), d, SADIST_HIT))
        {
          TerminateGoingTo();
          return true;
        }
      }
    }
  }
  return false;
}


//==========================================================================
//
//  character::CheckForBeverage
//
//==========================================================================
truth character::CheckForBeverage () {
  if (StateIsActivated(PANIC) || !IsEnabled() || !UsesNutrition() || CheckIfSatiated()) return false;
  itemvector ItemVector;
  GetStack()->FillItemVector(ItemVector);
  for (size_t c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->IsBeverage(this) && TryToConsume(ItemVector[c])) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  character::Haste
//
//==========================================================================
void character::Haste () {
  doforbodyparts()(this, &bodypart::Haste);
  doforequipments()(this, &item::Haste);
  BeginTemporaryState(HASTE, 500 + RAND_N(1000));
}


//==========================================================================
//
//  character::Slow
//
//==========================================================================
void character::Slow () {
  doforbodyparts()(this, &bodypart::Slow);
  doforequipments()(this, &item::Slow);
  BeginTemporaryState(SLOW, 500 + RAND_N(1000));
}


//==========================================================================
//
//  character::SurgicallyDetachBodyPart
//
//==========================================================================
void character::SurgicallyDetachBodyPart () {
  ADD_MESSAGE("You haven't got any extra bodyparts.");
}


//==========================================================================
//
//  character::CanHear
//
//==========================================================================
truth character::CanHear () const {
  return DataBase->CanHear && HasHead();
}


//==========================================================================
//
//  character::IsAllowedInDungeon
//
//==========================================================================
truth character::IsAllowedInDungeon (int dunIndex) {
  const fearray<int> &dlist = GetAllowedDungeons();
  for (uInt f = 0; f < dlist.Size; ++f) {
    if (dlist[f] == ALL_DUNGEONS || dlist[f] == dunIndex) {
      //ConLogf("OK!");
      return true;
    }
  }
  //ConLogf("NO!");
  return false;
}


//==========================================================================
//
//  character::IsESPBlockedByEquipment
//
//==========================================================================
truth character::IsESPBlockedByEquipment () const {
  for (int c = 0; c < GetEquipments(); ++c) {
    item *Item = GetEquipment(c);
    if (Item && Item->IsHelmet(this) &&
        ((Item->GetMainMaterial() && Item->GetMainMaterial()->BlockESP()) ||
         (Item->GetSecondaryMaterial() && Item->GetSecondaryMaterial()->BlockESP()))) return true;
  }
  return false;
}


//==========================================================================
//
//  character::ActivateTemporaryState
//
//==========================================================================
void character::ActivateTemporaryState (sLong What) {
  TemporaryState |= What;
}


//==========================================================================
//
//  character::DeActivateTemporaryState
//
//==========================================================================
void character::DeActivateTemporaryState (sLong What) {
  if (PolymorphBackup) {
    PolymorphBackup->TemporaryState &= ~What;
  }
  TemporaryState &= ~What;
}


//==========================================================================
//
//  character::ActivateEquipmentState
//
//==========================================================================
void character::ActivateEquipmentState (sLong What) {
  EquipmentState |= What;
}


//==========================================================================
//
//  character::DeActivateEquipmentState
//
//==========================================================================
void character::DeActivateEquipmentState (sLong What) {
  EquipmentState &= ~What;
}


//==========================================================================
//
//  character::TemporaryStateIsActivated
//
//==========================================================================
truth character::TemporaryStateIsActivated (sLong What) const {
  if ((What&PANIC) && StateIsActivated(FEARLESS)) What &= ~PANIC;
  if ((What&ESP) && (TemporaryState&ESP) && IsESPBlockedByEquipment()) What &= ~ESP;
  return !!(TemporaryState & What);
}


//==========================================================================
//
//  character::EquipmentStateIsActivated
//
//==========================================================================
truth character::EquipmentStateIsActivated (sLong What) const {
  return !!(EquipmentState & What);
}


//==========================================================================
//
//  character::StateIsActivated
//
//==========================================================================
truth character::StateIsActivated (sLong What) const {
  if ((What&PANIC) && ((TemporaryState|EquipmentState)&FEARLESS)) What &= ~PANIC;
  if ((What&ESP) && ((TemporaryState|EquipmentState)&ESP) && IsESPBlockedByEquipment()) What &= ~ESP;
  return (TemporaryState & What) || (EquipmentState & What);
}


//==========================================================================
//
//  character::PrintBeginFearlessMessage
//
//==========================================================================
void character::PrintBeginFearlessMessage () const {
  if (!StateIsActivated(FEARLESS)) {
         if (IsPlayer()) ADD_MESSAGE("You feel very comfortable.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s seems very comfortable.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::PrintEndFearlessMessage
//
//==========================================================================
void character::PrintEndFearlessMessage () const {
  if (!StateIsActivated(FEARLESS)) {
         if (IsPlayer()) ADD_MESSAGE("Everything looks more dangerous now.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s seems to have lost his confidence.", CHAR_NAME(DEFINITE));
  }
}


//==========================================================================
//
//  character::BeginFearless
//
//==========================================================================
void character::BeginFearless () {
  DeActivateTemporaryState(PANIC);
}


//==========================================================================
//
//  character::EndFearless
//
//==========================================================================
void character::EndFearless () {
  CheckPanic(500);
}


//==========================================================================
//
//  character::PrintBeginEtherealityMessage
//
//==========================================================================
void character::PrintBeginEtherealityMessage () const {
       if (IsPlayer()) ADD_MESSAGE("You feel like many miscible droplets of ether.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s melds into the surroundings.", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::PrintEndEtherealityMessage
//
//==========================================================================
void character::PrintEndEtherealityMessage () const {
       if (IsPlayer()) ADD_MESSAGE("You drop out of the firmament, feeling suddenly quite dense.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("Suddenly %s displaces the air with a puff.", CHAR_NAME(INDEFINITE));
}


//==========================================================================
//
//  character::BeginEthereality
//
//==========================================================================
void character::BeginEthereality () {
  if (IsPlayer()) {
    ADD_MESSAGE("You feel extraordinarily thin.");
  }
}


//==========================================================================
//
//  character::EndEthereality
//
//==========================================================================
void character::EndEthereality () {
  if (IsPlayer()) {
    ADD_MESSAGE("You are not two-dimensional anymore.");
  }
}


//==========================================================================
//
//  character::PrintBeginPolymorphLockMessage
//
//==========================================================================
void character::PrintBeginPolymorphLockMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel incredibly stubborn about who you are.");
}


//==========================================================================
//
//  character::PrintEndPolymorphLockMessage
//
//==========================================================================
void character::PrintEndPolymorphLockMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel more open to new ideas.");
}


//==========================================================================
//
//  character::PolymorphLockHandler
//
//==========================================================================
void character::PolymorphLockHandler () {
  if (TemporaryStateIsActivated(POLYMORPHED)) {
    EditTemporaryStateCounter(POLYMORPHED, 1);
    if (GetTemporaryStateCounter(POLYMORPHED) < 1000) {
      EditTemporaryStateCounter(POLYMORPHED, 1);
    }
  }
}


//==========================================================================
//
//  character::PrintBeginRegenerationMessage
//
//==========================================================================
void character::PrintBeginRegenerationMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Your heart races.");
}


//==========================================================================
//
//  character::PrintEndRegenerationMessage
//
//==========================================================================
void character::PrintEndRegenerationMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Your rapid heartbeat calms down.");
}


//==========================================================================
//
//  character::PrintBeginDiseaseImmunityMessage
//
//==========================================================================
void character::PrintBeginDiseaseImmunityMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel especially healthy.");
}


//==========================================================================
//
//  character::PrintEndDiseaseImmunityMessage
//
//==========================================================================
void character::PrintEndDiseaseImmunityMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You develop a sudden fear of germs.");
}


//==========================================================================
//
//  character::PrintBeginTeleportLockMessage
//
//==========================================================================
void character::PrintBeginTeleportLockMessage () const {
  if (IsPlayer()) ADD_MESSAGE("You feel firmly planted in reality.");
}


//==========================================================================
//
//  character::PrintEndTeleportLockMessage
//
//==========================================================================
void character::PrintEndTeleportLockMessage () const {
  if (IsPlayer()) ADD_MESSAGE("Your mind soars far and wide.");
}


//==========================================================================
//
//  character::TeleportLockHandler
//
//==========================================================================
void character::TeleportLockHandler () {
  if (StateIsActivated(TELEPORT_LOCK)) {
    EditTemporaryStateCounter(TELEPORT_LOCK, 1);
    if (GetTemporaryStateCounter(TELEPORT_LOCK) < 1000) EditTemporaryStateCounter(TELEPORT_LOCK, 1);
  }
}


//==========================================================================
//
//  character::PrintBeginSwimmingMessage
//
//==========================================================================
void character::PrintBeginSwimmingMessage () const {
       if (IsPlayer())  ADD_MESSAGE("You fondly remember the sound of ocean waves.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s looks wet.", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  character::PrintEndSwimmingMessage
//
//==========================================================================
void character::PrintEndSwimmingMessage () const {
       if (IsPlayer()) ADD_MESSAGE("You suddenly remember how you nearly drowned as a child.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("Suddenly %s looks less wet.", CHAR_NAME(INDEFINITE));
}


//==========================================================================
//
//  character::BeginSwimming
//
//==========================================================================
void character::BeginSwimming () {
}


//==========================================================================
//
//  character::EndSwimming
//
//==========================================================================
void character::EndSwimming () {
}
