/*
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __CHAR_H__
#define __CHAR_H__

#include "itemset.h"
#include "script.h"
#include "wskill.h"
#include "fesave.h"
#include "feparse.h"
#include "festring.h"
#include "hscore.h"


#define CHAR_PERSONAL_PRONOUN  GetPersonalPronoun(true).CStr()
#define CHAR_POSSESSIVE_PRONOUN  GetPossessivePronoun(true).CStr()
#define CHAR_OBJECT_PRONOUN  GetObjectPronoun(true).CStr()
#define CHAR_PERSONAL_PRONOUN_THIRD_PERSON_VIEW  GetPersonalPronoun(false).CStr()
#define CHAR_POSSESSIVE_PRONOUN_THIRD_PERSON_VIEW  GetPossessivePronoun(false).CStr()
#define CHAR_OBJECT_PRONOUN_THIRD_PERSON_VIEW  GetObjectPronoun(false).CStr()

class go;
class autotravel;
class team;
class wsquare;
class cweaponskill;
class action;
class characterprototype;
class web;
class mindworm;

struct homedata;
struct trapdata;
struct blitdata;


typedef std::vector<std::pair<double, int> > blockvector;
typedef truth (item::*sorter)(ccharacter *) const;
typedef truth (character::*petmanagementfunction) ();
typedef character *(*characterspawner) (int, int);
typedef character *(*charactercloner) (ccharacter *);

// for GeneralHasItem
typedef truth (*ItemCheckerCB) (item *i);


static FORCE_INLINE int APBonus (int Attribute) {
  return Attribute >= 10 ? 90 + Attribute : 50 + Attribute * 5;
}


/*k8: wtf is this?
struct expid {
  FORCE_INLINE bool operator < (expid E) const {
    return ActID != E.ActID ? ActID < E.ActID : SrcID < E.SrcID;
  }
  int ActID, SrcID;
};


//inline bool expid::operator < (expid E) const { return ActID != E.ActID ? ActID < E.ActID : SrcID < E.SrcID; }

inline outputfile &operator << (outputfile &SaveFile, const expid &Value) {
  //SaveFile.writeSimple<decltype(Value)>(Value);
  SaveFile << Value.ActID << Value.SrcID;
  return SaveFile;
}


inline inputfile &operator >> (inputfile &SaveFile, expid &Value) {
  //SaveFile.readSimple<decltype(Value)>(Value);
  SaveFile >> Value.ActID >> Value.SrcID;
  return SaveFile;
}
*/


//typedef std::map<expid, double> expmodifiermap;


struct characterdatabase : public databasebase {
  typedef characterprototype prototype;

  void InitDefaults (const characterprototype *NewProtoType, int NewConfig,
                     cfestring &acfgstrname, const characterdatabase *aParentDB);
  truth AllowRandomInstantiation () const { return CanBeGenerated && !IsUnique; }
  void PostProcess (const TextFileLocation &loc);

  const prototype *ProtoType;
  double NaturalExperience[ATTRIBUTES]; // calculated
  truth IsNonHumanoidBase; // set in `InitDefaults()`
  feuLong Flags;
  truth IsAbstract;
  truth CanRead;
  truth CanBeGenerated;
  truth CanOpen;
  truth IsUnique;
  truth IsNameable;
  truth IsPolymorphable;
  truth CanUseEquipment;
  truth CanKick;
  truth CanTalk;
  truth CanBeWished;
  truth CreateDivineConfigurations;
  truth CreateGolemMaterialConfigurations;
  truth CanBeCloned;
  truth CanZap;
  truth HasALeg;
  truth IgnoreDanger;
  truth IsExtraCoward;
  truth SpillsBlood;
  truth HasEyes;
  truth HasHead;
  truth CanThrow;
  truth UsesNutrition;
  truth BodyPartsDisappearWhenSevered;
  truth CanBeConfused;
  truth CanApply;
  truth BiteCapturesBodyPart;
  truth IsPlant;
  truth DestroysWalls;
  truth IsRooted;
  truth HasSecondaryMaterial;
  truth IsImmuneToLeprosy;
  truth AutomaticallySeen;
  truth CanHear;
  truth WillCarryItems;
  truth Sweats;
  truth IsImmuneToItemTeleport;
  truth AlwaysUseMaterialAttributes;
  truth IsEnormous;
  truth IsExtraFragile;
  truth AllowUnconsciousness;
  truth CanChoke;
  truth IsImmuneToStickiness;
  truth ForceCustomStandVerb;
  truth VomittingIsUnhealthy;
  int DefaultEndurance;
  int DefaultPerception;
  int DefaultIntelligence;
  int DefaultWisdom;
  int DefaultWillPower;
  int DefaultCharisma;
  int DefaultMana;
  int DefaultArmStrength;
  int DefaultLegStrength;
  int DefaultDexterity;
  int DefaultAgility;
  sLong DefaultMoney;
  int TotalSize;
  int Sex;
  int CriticalModifier;
  festring StandVerb;
  int Frequency;
  int EnergyResistance;
  int FireResistance;
  int PoisonResistance;
  int ElectricityResistance;
  int AcidResistance;
  int SoundResistance;
  int ConsumeFlags;
  sLong TotalVolume;
  packv2 HeadBitmapPos;
  packv2 TorsoBitmapPos;
  packv2 ArmBitmapPos;
  packv2 LegBitmapPos;
  packv2 RightArmBitmapPos;
  packv2 LeftArmBitmapPos;
  packv2 RightLegBitmapPos;
  packv2 LeftLegBitmapPos;
  packv2 GroinBitmapPos;
  packcol16 ClothColor;
  packcol16 SkinColor;
  packcol16 CapColor;
  packcol16 HairColor;
  packcol16 EyeColor;
  packcol16 TorsoMainColor;
  packcol16 BeltColor;
  packcol16 BootColor;
  packcol16 TorsoSpecialColor;
  packcol16 ArmMainColor;
  packcol16 GauntletColor;
  packcol16 ArmSpecialColor;
  packcol16 LegMainColor;
  packcol16 LegSpecialColor;
  col24 BaseEmitation;
  truth UsesLongArticle;
  festring Adjective;
  truth UsesLongAdjectiveArticle;
  festring NameSingular;
  festring NamePlural;
  festring PostFix;
  int ArticleMode;
  int BaseUnarmedStrength;
  int BaseBiteStrength;
  int BaseKickStrength;
  int AttackStyle;
  sLong ClassStates;
  sLong WhatThrowItemTypesToThrow;
  fearray<festring> Alias;
  itemcontentscript Helmet;
  itemcontentscript Amulet;
  itemcontentscript Cloak;
  itemcontentscript BodyArmor;
  itemcontentscript Belt;
  itemcontentscript RightWielded;
  itemcontentscript LeftWielded;
  itemcontentscript RightRing;
  itemcontentscript LeftRing;
  itemcontentscript RightGauntlet;
  itemcontentscript LeftGauntlet;
  itemcontentscript RightBoot;
  itemcontentscript LeftBoot;
  int AttributeBonus;
  fearray<sLong> KnownCWeaponSkills;
  fearray<sLong> CWeaponSkillHits;
  int RightSWeaponSkillHits;
  int LeftSWeaponSkillHits;
  int PanicLevel;
  fearray<itemcontentscript> Inventory;
  int DangerModifier;
  festring DefaultName;
  fearray<festring> FriendlyReplies;
  fearray<festring> HostileReplies;
  int FleshMaterial;
  festring DeathMessage;
  int HPRequirementForGeneration;
  int DayRequirementForGeneration;
  int AttackWisdomLimit;
  int AttachedGod;
  packv2 WieldedPosition;
  int NaturalSparkleFlags;
  int MoveType;
  int BloodMaterial;
  int VomitMaterial;
  int PolymorphIntelligenceRequirement;
  feuLong DefaultCommandFlags;
  feuLong ConstantCommandFlags;
  festring ForceVomitMessage;
  int SweatMaterial;
  fearray<festring> ScienceTalkAdjectiveAttribute;
  fearray<festring> ScienceTalkSubstantiveAttribute;
  fearray<festring> ScienceTalkPrefix;
  fearray<festring> ScienceTalkName;
  int ScienceTalkPossibility;
  int ScienceTalkIntelligenceModifier;
  int ScienceTalkWisdomModifier;
  int ScienceTalkCharismaModifier;
  int ScienceTalkIntelligenceRequirement;
  int ScienceTalkWisdomRequirement;
  int ScienceTalkCharismaRequirement;
  int DisplacePriority;
  festring RunDescriptionLineOne;
  festring RunDescriptionLineTwo;
  truth AllowPlayerToChangeEquipment;
  int TamingDifficulty;
  truth IsMasochist;
  truth IsSadist;
  truth IsCatacombCreature;
  truth CreateUndeadConfigurations;
  truth UndeadVersions;
  int UndeadAttributeModifier;
  int UndeadVolumeModifier;
  truth UndeadCopyMaterials;
  truth CanBeGeneratedOnlyInTheCatacombs;
  truth IsAlcoholic;
  truth IsUndead;
  truth IsImmuneToWhipOfThievery;
  truth IsRangedAttacker;
  int WhatCategoryToThrow;
  int WhatWeaponConfigToThrow;
  fearray<int> AllowedDungeons;
  fearray<festring> LevelTags;
  fearray<festring> HomeLevel;
  int NaturalTeam;
};


class characterprototype {
  friend class databasecreator<character>;
  friend class protosystem;
public:
  characterprototype (const characterprototype *, characterspawner, charactercloner, cchar *);
  virtual ~characterprototype () {}

  character *Spawn (int Config=0, int SpecialFlags=0) const { return Spawner(Config, SpecialFlags); }
  character *SpawnAndLoad (inputfile &) const;
  character *Clone (ccharacter *Char) const { return Cloner(Char); }
  int GetIndex () const { return Index; }
  inline cchar *GetTypeID () const { return ClassID; }
  inline truth IsOfType (cchar *tname) const { return (tname ? (strcmp(tname, ClassID) == 0) : false); }
  inline truth IsOfType (cfestring &tname) const { return (tname.Compare(ClassID) == 0); }
  const characterprototype *GetBase () const { return Base; }
  cchar *GetClassID () const { return ClassID; }
  int CreateSpecialConfigurations (characterdatabase **, int, int);
  const characterdatabase *ChooseBaseForConfig (characterdatabase **TempConfig, int, int) { return *TempConfig; }
  const characterdatabase *const *GetConfigData () const { return ConfigData; }
  int GetConfigSize () const { return ConfigSize; }

  bool NeedCreateUndead (int Config) const {
    /*
    auto it = createdUndead.find(Config);
    if (it != createdUndead.end()) {
      return false;
    } else {
      createdUndead.insert(Config);
      return true;
    }
    */
    return true;
  }

  bool NeedCreateGolem (int Config) const {
    /*
    auto it = createdGolem.find(Config);
    if (it != createdGolem.end()) {
      return false;
    } else {
      createdGolem.insert(Config);
      return true;
    }
    */
    return true;
  }

private:
  int Index;
  const characterprototype *Base;
  characterdatabase **ConfigData;
  characterdatabase **ConfigTable[CONFIG_TABLE_SIZE];
  int ConfigSize;
  characterspawner Spawner;
  charactercloner Cloner;
  cchar *ClassID;

  // we may call `CreateSpecialConfigurations()` several times for modules.
  // there is no need to recreate undead configs each time
  /*
  mutable std::set<int> createdUndead;
  // and for golems
  mutable std::set<int> createdGolem;
  */

public:
  EventHandlerMap mOnEvents;
};


class character : public entity, public id {
  friend class databasecreator<character>;
  friend class corpse;

public:
  typedef characterprototype prototype;
  typedef characterdatabase database;

public:
  character ();
  character (ccharacter &);
  virtual ~character ();

public:
  virtual truth IsCharacter () const override { return true; }

  virtual void Save (outputfile &) const;
  virtual void Load (inputfile &);
  virtual truth CanWield () const { return false; }
  virtual truth Catches(item *) { return false; }
  bool AddPolymorphedText (festring &Msg, cchar *pfx);
  truth CheckDeath (cfestring &, character * =0, feuLong =0);
  truth DodgesFlyingItem (item *, double);
  virtual truth Hit (character *Enemy, v2 HitPos, int Direction, int Flags=0) = 0;
  truth ReadItem (item *);
  truth TestForPickup (item *) const;
  void ThrowItem (int, item *);

  truth HitWithVFX (character *Enemy, v2 HitPos, int Direction, int Flags=0);

  truth TryMoveUnlockHelper (lsquare *Square); // this is used to automatically unlock doors
  truth TryMove (v2 MoveVector, truth Important, truth Run);

  // helper for `TryMove()`
  truth TryMoveDropPets (wsquare *dest);

  // wizard mode command
  truth WildernessTeleport (v2 pos);

  truth IsPlayerOnBoat () const {
    return (IsPlayer() && game::IsInWilderness() && game::PlayerHasBoat() && game::PlayerOnBoat());
  }

  truth HasEncryptedScroll () const;
  truth RemoveEncryptedScroll ();
  truth HasHeadOfElpuri () const;
  truth RemoveHeadOfElpuri ();
  truth HasGoldenEagleShirt () const;
  truth HasPetrussNut () const;
  truth HasOmmelBlood () const;
  truth HasCurdledBlood () const;
  truth CurdleOmmelBlood () const;
  truth RemoveCurdledOmmelBlood ();

  truth RemoveMondedrPass ();
  truth RemoveRingOfThieves ();
  truth HasShadowVeil () const;
  truth RemoveShadowVeil (character *ToWhom);
  truth HasLostRubyFlamingSword () const;

  truth HasNuke () const;
  truth RemoveNuke (character *ToWhom);
  truth HasWeepObsidian () const;
  truth RemoveWeepObsidian (character *ToWhom);
  truth HasMuramasa () const;
  truth RemoveMuramasa (character *ToWhom);
  truth HasMasamune () const;
  truth RemoveMasamune (character *ToWhom);

  // return secondary material, or `0`
  material *IsDrinkableCanister (item *Item) const;

  truth IsPlayer () const { return Flags & C_PLAYER; }
  virtual truth IsPetrussWife () const { return false; }
  virtual truth IsPetrus () const { return false; }
  truth Engrave (cfestring &What);
  void AddScoreEntry (cfestring &Description, double Multiplier=1.0, truth AddEndLevel=true) const;
  void AddQuitedScoreEntry (cfestring &Description, double Multiplier=1.0) const;
  sLong GetAP () const { return AP; }
  sLong GetNP () const { return NP; }
  stack *GetStack () const { return Stack; }
  int GetBurdenState () const { return BurdenState; }
  truth MakesBurdened (sLong What) const { return sLong(GetCarryingStrength()) * 2500 < What; }
  virtual int TakeHit (character *, item *, bodypart *, v2, double, double, int, int, int, truth, truth);
  int GetLOSRange () const;
  int GetLOSRangeSquare () const { return GetLOSRange() * GetLOSRange(); }
  int GetESPRange () const { return GetAttribute(INTELLIGENCE) / 3; }
  int GetESPRangeSquare () const { return GetESPRange() * GetESPRange(); }
  int GetTeleportRange () const { return GetAttribute(INTELLIGENCE); }
  int GetTeleportRangeSquare () const { return GetTeleportRange() * GetTeleportRange(); }
  void AddMissMessage (ccharacter *Enemy, citem *Weapon) const;
  void AddPrimitiveHitMessage (ccharacter *, cfestring &, cfestring &, int) const;
  void AddWeaponHitMessage (ccharacter *, citem *, int, truth = false) const;
  virtual void BeTalkedTo ();
  void ReceiveDarkness (sLong);
  void Die (character *Killer=0, cfestring &Msg=CONST_S(""), feuLong DeathFlags=0);
  void HasBeenHitByItem (character *, item *, int, double, int);
  void Hunger ();
  void Move (v2, truth, truth = false);
  virtual truth MoveToInterestingItem (truth SameRoom); // this is automatically called from `MoveRandomly()`
  virtual truth MoveRandomly (truth allowInterestingItems=true);
  void ReceiveNutrition (sLong);
  void ReceiveOmmelUrine (sLong);
  void ReceivePepsi (sLong);
  void ReceiveSchoolFood (sLong);
  void ReceiveSickness (sLong Amount);
  void Regenerate ();
  void SetAP (sLong What) { AP = What; }
  void SetNP (sLong);
  void Vomit (v2, int, truth = true);
  virtual void Be ();
  truth Polymorph (character *, int);
  void BeKicked (character *, item *, bodypart *, v2, double, double, int, int, truth, truth);
  void FallTo (character *, v2);
  truth CheckCannibalism (cmaterial *) const;
  void ActivateTemporaryState (sLong What);
  void DeActivateTemporaryState (sLong What);
  void ActivateEquipmentState (sLong What);
  void DeActivateEquipmentState (sLong What);
  truth TemporaryStateIsActivated (sLong What) const;
  truth EquipmentStateIsActivated (sLong What) const;
  truth StateIsActivated (sLong What) const;
  truth LoseConsciousness (int, truth = false);
  void SetTemporaryStateCounter (sLong, int);
  void DeActivateVoluntaryAction (cfestring & = CONST_S(""));
  void ActionAutoTermination ();
  team *GetTeam () const { return Team; }
  void SetTeam (team *); // WARNING! should be called only for new monsters w/o team set!
  void ChangeTeam (team *);
  virtual int GetMoveEase () const;
  double GetDodgeValue () const { return DodgeValue; }
  sLong GetMoney () const { return Money; }
  void SetMoney (sLong What) { Money = What; }
  void EditMoney (sLong What) { Money += What; }
  int GetCurrentSweatMaterial() const { return CurrentSweatMaterial; }
  void SetInitialSweatMaterial(int What) { CurrentSweatMaterial = What; }
  void EditCurrentSweatMaterial(int What) { CurrentSweatMaterial = What; }
  truth Displace (character *, truth = false);
  truth CheckStarvationDeath (cfestring &);
  void ShowNewPosInfo ();
  void Hostility (character *);
  stack *GetGiftStack () const;
  truth MoveRandomlyInRoom (truth allowInterestingItems=true);
  void ReceiveKoboldFlesh (sLong);
  truth ChangeRandomAttribute (int);
  int RandomizeReply (sLong &, int);
  virtual void CreateInitialEquipment (int);
  void DisplayInfo (festring &);
  virtual truth SpecialEnemySightedReaction (character *) { return false; }
  void TestWalkability ();
  void EditAP (sLong);
  void EditNP (sLong);
  void SetSize (int);
  virtual int GetSize () const;
  torso *GetTorso () const { return static_cast<torso *>(*BodyPartSlot[TORSO_INDEX]); }
  humanoidtorso *GetHumanoidTorso () const { return static_cast<humanoidtorso *>(*BodyPartSlot[TORSO_INDEX]); }
  void SetTorso (torso *What) { SetBodyPart(TORSO_INDEX, What); }
  inline bodypart *GetBodyPart (int I) const {
    if (I >= 0 && I < BodyParts) {
      return static_cast<bodypart *>(*BodyPartSlot[I]);
    } else {
      return 0;
    }
  }
  void SetBodyPart (int, bodypart *);
  void SetMainMaterial (material *, int = 0);
  void ChangeMainMaterial (material *, int = 0);
  void SetSecondaryMaterial (material *, int = 0);
  void ChangeSecondaryMaterial (material *, int = 0);
  void DoDetecting ();
  void RestoreHP ();
  void RestoreLivingHP ();
  void RestoreStamina () { Stamina = MaxStamina; }
  virtual truth ReceiveDamage (character *Damager, int Damage, int Type, int TargetFlags=ALL, int Direction=8,
                               truth Divide=false, truth PenetrateArmor=false, truth Critical=false, truth ShowMsg=true);
  virtual int ReceiveBodyPartDamage (character *, int, int, int, int = 8, truth = false, truth = false, truth = true, truth = false);
  virtual truth BodyPartIsVital (int) const { return true; }
  void RestoreBodyParts ();
  cfestring &GetAssignedName () const { return AssignedName; }
  void SetAssignedName (cfestring &What) { AssignedName = What; }
  festring GetDescription (int) const;
  festring GetPersonalPronoun (truth PlayersView=true) const;
  festring GetPossessivePronoun (truth PlayersView=true) const;
  festring GetObjectPronoun (truth PlayersView=true) const;
  virtual truth BodyPartCanBeSevered (int) const;
  virtual void AddName (festring &, int) const;
  void ReceiveHeal (sLong);
  virtual item *GetMainWielded () const { return 0; }
  virtual item *GetSecondaryWielded () const { return 0; }
  int GetHungerState () const;
  int GetCurrentHungerStatePercent () const;
  truth ConsumeItem (item *Item, cfestring &ConsumeVerb, truth nibbling=false);
  virtual truth CanConsume (material *) const;
  virtual truth EatToBloatedState () const { return false; }
  action *GetAction () const { return Action; }
  void SetAction (action *What) { Action = What; }
  virtual void SwitchToDig (item *, v2) {}
  virtual void SwitchToButchery (item *DigItem, item *Corpse) {}
  virtual void SetRightWielded (item *) {}
  virtual void SetLeftWielded (item *) {}
  truth IsPassableSquare (int x, int y) const;
  truth IsPassableSquare (cv2 xy) const { return IsPassableSquare(xy.X, xy.Y); }
  truth IsInCorridor (int x, int y, int moveDir) const;
  inline truth IsInCorridor (cv2 pos, int moveDir) const { return IsInCorridor(pos.X, pos.Y, moveDir); }
  inline truth IsInCorridor (int moveDir) const { return IsInCorridor(GetPos(), moveDir); }
  void CountPossibleMoveDirs (cv2 pos, int *odirs, int *ndirs, int exclideDir=-1) const;
  int CheckCorridorMove (v2 &moveVector, cv2 pos, int moveDir, truth *markAsTurn) const; // -1: not in corridor; return new moveDir and moveVector
  cv2 GetDiagonalForDirs (int moveDir, int newDir) const;
  truth IsInSafeRoom () const;
  truth CanRestHere () const;
  truth CanSeeAnyNonFriendlyMonsters () const;
  truth IsDangerousSquareChars (v2 pos) const;
  truth IsDangerousSquareOther (v2 pos) const;
  void MarkAdjacentItemsAsSeen (v2 pos);
  virtual truth IsFunnyItem (item *Item) const;
  truth HasInterestingItemsAt (v2 pos);
  void GoOn (go *Go, truth FirstStep=false);
  void AutoTravel (autotravel *Go, truth FirstStep=false);
  character *CheckWindowInDir (int dir) const; // check if there is somebody there
  virtual truth CheckKick () const;
  virtual int OpenMultiplier () const { return 2; }
  virtual int CloseMultiplier () const { return 2; }
  virtual truth CheckThrow () const;
  virtual truth CheckOffer () const { return true; }
  int GetTemporaryStateCounter (sLong) const;
  void EditTemporaryStateCounter (sLong, int);
  static truth AllowDamageTypeBloodSpill (int);
  int GetResistance (int) const;
  virtual int GetGlobalResistance (int Type) const { return GetResistance(Type); }
  virtual cchar *GetEquipmentName (int) const;
  virtual bodypart *GetBodyPartOfEquipment (int) const { return 0; }
  virtual item *GetEquipment (int) const { return 0; }
  virtual int GetEquipments () const { return 0; }
  virtual sorter EquipmentSorter (int) const { return 0; }
  virtual void SetEquipment (int, item *) {}
  /*not virtual!*/truth CanBeSeenWithESPBy (ccharacter *Char) const; // doesn't check LOS
  /*not virtual!*/truth CanBeSeenWithInfraBy (ccharacter *Char) const; // doesn't check LOS
  /*not virtual!*/truth IsESPBlockedByEquipment () const;
  void AddHealingLiquidConsumeEndMessage () const;
  void AddSchoolFoodConsumeEndMessage () const;
  void AddSchoolFoodHitMessage () const;
  void AddOmmelConsumeEndMessage () const;
  void AddCocaColaConsumeEndMessage () const;
  void AddPepsiConsumeEndMessage () const;
  void AddFrogFleshConsumeEndMessage () const;
  void AddKoboldFleshConsumeEndMessage () const;
  void AddKoboldFleshHitMessage () const;
  void AddBoneConsumeEndMessage () const;
  void AddBlackUnicornConsumeEndMessage () const;
  void AddGrayUnicornConsumeEndMessage () const;
  void AddWhiteUnicornConsumeEndMessage () const;
  void AddOmmelBoneConsumeEndMessage () const;
  void AddLiquidHorrorConsumeEndMessage() const;
  void AddAlienFleshConsumeEndMessage() const;
  void PrintInfo () const;
  virtual item *SevereBodyPart (int, truth = false, stack * = 0);
  virtual truth TryToRiseFromTheDead ();
  virtual truth RaiseTheDead (character *);
  bodypart *CreateBodyPart (int, int = 0);
  virtual truth EquipmentIsAllowed (int) const { return true; }
  truth CanUseEquipment (int) const;
  const database *GetDataBase () const { return DataBase; }
  void SetParameters (int) {}
  virtual double GetNaturalExperience (int) const;
  virtual truth TryToStealFromShop (character *, item *);

  DATA_BASE_VALUE(const prototype *, ProtoType);
  DATA_BASE_VALUE(int, Config);
  DATA_BASE_VALUE(int, DefaultEndurance);
  DATA_BASE_VALUE(int, DefaultPerception);
  DATA_BASE_VALUE(int, DefaultIntelligence);
  DATA_BASE_VALUE(int, DefaultWisdom);
  DATA_BASE_VALUE(int, DefaultWillPower);
  DATA_BASE_VALUE(int, DefaultCharisma);
  DATA_BASE_VALUE(int, DefaultMana);
  DATA_BASE_VALUE(int, DefaultArmStrength);
  DATA_BASE_VALUE(int, DefaultLegStrength);
  DATA_BASE_VALUE(int, DefaultDexterity);
  DATA_BASE_VALUE(int, DefaultAgility);
  DATA_BASE_VALUE(sLong, DefaultMoney);
  DATA_BASE_VALUE(int, TotalSize);
  DATA_BASE_TRUTH(CanRead);
  DATA_BASE_VALUE(int, Sex);
  DATA_BASE_TRUTH(CanBeGenerated);
  DATA_BASE_VALUE(int, CriticalModifier);
  DATA_BASE_TRUTH(CanOpen);
  DATA_BASE_VALUE(int, EnergyResistance);
  DATA_BASE_VALUE(int, FireResistance);
  DATA_BASE_VALUE(int, PoisonResistance);
  DATA_BASE_VALUE(int, ElectricityResistance);
  DATA_BASE_VALUE(int, AcidResistance);
  DATA_BASE_VALUE(int, SoundResistance);
  DATA_BASE_VALUE(int, ConsumeFlags);
  DATA_BASE_VALUE(sLong, TotalVolume);
  virtual DATA_BASE_VALUE(v2, HeadBitmapPos);
  virtual DATA_BASE_VALUE(v2, TorsoBitmapPos);
  virtual DATA_BASE_VALUE(v2, ArmBitmapPos);
  virtual DATA_BASE_VALUE(v2, LegBitmapPos);
  virtual DATA_BASE_VALUE(v2, RightArmBitmapPos);
  virtual DATA_BASE_VALUE(v2, LeftArmBitmapPos);
  virtual DATA_BASE_VALUE(v2, RightLegBitmapPos);
  virtual DATA_BASE_VALUE(v2, LeftLegBitmapPos);
  virtual DATA_BASE_VALUE(v2, GroinBitmapPos);
  virtual DATA_BASE_VALUE(col16, ClothColor);
  virtual DATA_BASE_VALUE(col16, SkinColor);
  virtual DATA_BASE_VALUE(col16, CapColor);
  virtual DATA_BASE_VALUE(col16, HairColor);
  virtual DATA_BASE_VALUE(col16, EyeColor);
  virtual DATA_BASE_VALUE(col16, TorsoMainColor);
  virtual DATA_BASE_VALUE(col16, BeltColor);
  virtual DATA_BASE_VALUE(col16, BootColor);
  virtual DATA_BASE_VALUE(col16, TorsoSpecialColor);
  virtual DATA_BASE_VALUE(col16, ArmMainColor);
  virtual DATA_BASE_VALUE(col16, GauntletColor);
  virtual DATA_BASE_VALUE(col16, ArmSpecialColor);
  virtual DATA_BASE_VALUE(col16, LegMainColor);
  virtual DATA_BASE_VALUE(col16, LegSpecialColor);
  virtual DATA_BASE_TRUTH(IsNameable);
  virtual DATA_BASE_VALUE(col24, BaseEmitation); // devirtualize ASAP
  DATA_BASE_TRUTH(UsesLongArticle);
  DATA_BASE_VALUE(cfestring&, Adjective);
  DATA_BASE_TRUTH(UsesLongAdjectiveArticle);
  DATA_BASE_VALUE(cfestring&, NameSingular);
  DATA_BASE_VALUE(cfestring&, NamePlural);
  DATA_BASE_VALUE(cfestring&, PostFix);
  DATA_BASE_VALUE(int, ArticleMode);
  DATA_BASE_TRUTH(CanZap);
  virtual DATA_BASE_TRUTH(IsPolymorphable);
  DATA_BASE_VALUE(int, BaseUnarmedStrength);
  DATA_BASE_VALUE(int, BaseBiteStrength);
  DATA_BASE_VALUE(int, BaseKickStrength);
  DATA_BASE_VALUE(int, AttackStyle);
  DATA_BASE_TRUTH(CanUseEquipment);
  DATA_BASE_TRUTH(CanKick);
  DATA_BASE_TRUTH(CanTalk);
  DATA_BASE_TRUTH(CanBeWished);
  DATA_BASE_VALUE(sLong, ClassStates);
  DATA_BASE_VALUE(sLong, WhatThrowItemTypesToThrow);
  DATA_BASE_VALUE(const fearray<festring>&, Alias);
  DATA_BASE_TRUTH(CreateGolemMaterialConfigurations);
  DATA_BASE_VALUE(const fearray<sLong>&, KnownCWeaponSkills);
  DATA_BASE_VALUE(const fearray<sLong>&, CWeaponSkillHits);
  DATA_BASE_VALUE(int, RightSWeaponSkillHits);
  DATA_BASE_VALUE(int, LeftSWeaponSkillHits);
  DATA_BASE_VALUE(int, PanicLevel);
  DATA_BASE_TRUTH(CanBeCloned);
  DATA_BASE_VALUE(cfestring&, DefaultName);
  DATA_BASE_VALUE(const fearray<festring>&, FriendlyReplies);
  DATA_BASE_VALUE(const fearray<festring>&, HostileReplies);
  DATA_BASE_VALUE(int, FleshMaterial);
  virtual DATA_BASE_TRUTH(HasALeg);
  virtual DATA_BASE_VALUE(cfestring&, DeathMessage);
  DATA_BASE_VALUE(int, HPRequirementForGeneration);
  DATA_BASE_TRUTH(IsExtraCoward);
  DATA_BASE_TRUTH(SpillsBlood);
  DATA_BASE_TRUTH(HasEyes);
  virtual DATA_BASE_TRUTH(HasHead);
  DATA_BASE_TRUTH(CanThrow);
  DATA_BASE_TRUTH(UsesNutrition);
  DATA_BASE_VALUE(int, AttackWisdomLimit);
  DATA_BASE_TRUTH(IsUnique);
  DATA_BASE_VALUE(int, AttachedGod);
  DATA_BASE_TRUTH(BodyPartsDisappearWhenSevered);
  DATA_BASE_VALUE(int, Frequency);
  DATA_BASE_TRUTH(CanBeConfused);
  DATA_BASE_TRUTH(CanApply);
  DATA_BASE_VALUE(v2, WieldedPosition);
  virtual DATA_BASE_VALUE(int, NaturalSparkleFlags);
  DATA_BASE_TRUTH(IgnoreDanger);
  DATA_BASE_TRUTH(BiteCapturesBodyPart);
  DATA_BASE_TRUTH(IsPlant);
  DATA_BASE_TRUTH(DestroysWalls);
  DATA_BASE_TRUTH(IsRooted);
  DATA_BASE_VALUE(int, BloodMaterial);
  DATA_BASE_VALUE(int, VomitMaterial);
  DATA_BASE_TRUTH(AutomaticallySeen);
  DATA_BASE_VALUE(feuLong, DefaultCommandFlags);
  DATA_BASE_TRUTH(WillCarryItems);
  DATA_BASE_VALUE(int, SweatMaterial);
  DATA_BASE_TRUTH(Sweats);
  DATA_BASE_TRUTH(IsImmuneToItemTeleport);
  DATA_BASE_TRUTH(AlwaysUseMaterialAttributes);
  DATA_BASE_TRUTH(IsEnormous);
  DATA_BASE_VALUE(const fearray<festring>&, ScienceTalkAdjectiveAttribute);
  DATA_BASE_VALUE(const fearray<festring>&, ScienceTalkSubstantiveAttribute);
  DATA_BASE_VALUE(const fearray<festring>&, ScienceTalkPrefix);
  DATA_BASE_VALUE(const fearray<festring>&, ScienceTalkName);
  DATA_BASE_VALUE(int, ScienceTalkPossibility);
  DATA_BASE_VALUE(int, ScienceTalkIntelligenceModifier);
  DATA_BASE_VALUE(int, ScienceTalkWisdomModifier);
  DATA_BASE_VALUE(int, ScienceTalkCharismaModifier);
  DATA_BASE_VALUE(int, ScienceTalkIntelligenceRequirement);
  DATA_BASE_VALUE(int, ScienceTalkWisdomRequirement);
  DATA_BASE_VALUE(int, ScienceTalkCharismaRequirement);
  DATA_BASE_TRUTH(IsExtraFragile);
  DATA_BASE_TRUTH(IsImmuneToStickiness);
  DATA_BASE_VALUE(festring, ForceVomitMessage);
  DATA_BASE_TRUTH(CanChoke);
  DATA_BASE_VALUE(int, DisplacePriority);
  DATA_BASE_VALUE(cfestring&, RunDescriptionLineOne);
  DATA_BASE_VALUE(cfestring&, RunDescriptionLineTwo);
  DATA_BASE_TRUTH(ForceCustomStandVerb);
  DATA_BASE_TRUTH(VomittingIsUnhealthy);
  DATA_BASE_TRUTH(AllowPlayerToChangeEquipment);
  DATA_BASE_VALUE(int, TamingDifficulty);
  DATA_BASE_TRUTH(IsMasochist);
  DATA_BASE_TRUTH(IsSadist);
  DATA_BASE_TRUTH(IsCatacombCreature);
  DATA_BASE_TRUTH(CreateUndeadConfigurations);
  DATA_BASE_TRUTH(UndeadVersions);
  DATA_BASE_VALUE(int, UndeadAttributeModifier);
  DATA_BASE_VALUE(int, UndeadVolumeModifier);
  DATA_BASE_TRUTH(UndeadCopyMaterials);
  DATA_BASE_TRUTH(CanBeGeneratedOnlyInTheCatacombs);
  DATA_BASE_TRUTH(IsAlcoholic);
  DATA_BASE_TRUTH(IsUndead);
  DATA_BASE_TRUTH(IsImmuneToWhipOfThievery);
  DATA_BASE_TRUTH(IsRangedAttacker);
  DATA_BASE_VALUE(int, WhatCategoryToThrow);
  DATA_BASE_VALUE(int, WhatWeaponConfigToThrow);
  DATA_BASE_VALUE(const fearray<int>&, AllowedDungeons);
  DATA_BASE_VALUE(const fearray<festring>&, LevelTags);
  DATA_BASE_VALUE(const fearray<festring>&, HomeLevel);
  DATA_BASE_VALUE(int, NaturalTeam);

  int GetType () const { return GetProtoType()->GetIndex(); }
  inline cchar *GetTypeID () const { return GetProtoType()->GetClassID(); }
  inline truth IsOfType (cchar *tname) const { return (tname ? (strcmp(tname, GetProtoType()->GetClassID()) == 0) : false); }
  inline truth IsOfType (cfestring &tname) const { return (tname.Compare(GetProtoType()->GetClassID()) == 0); }
  festring GetConfigName () const;

  festring DebugLogNameCfg () const;

  void TeleportRandomly (truth = false);
  truth TeleportNear (character *);
  virtual void InitSpecialAttributes () {}
  virtual void Kick (lsquare *Square, int Direction, truth ForceHit=false) = 0;
  virtual int GetAttribute (int Identifier, truth AllowBonus=true) const;
  // k8: i am going to remove MANA, so let's substitute something for it
  virtual int GetMana () {
    //FIXME: this is wrong, stupid, etc.
    return ((GetAttribute(INTELLIGENCE) + GetAttribute(WISDOM)) >> 1) + 1;
  }
  virtual truth EditAttribute (int Identifier, int Value);
  virtual void EditExperience (int, double, double);
  truth RawEditAttribute (double &, int) const;
  void DrawPanel (truth) const;
  virtual int DrawStats (truth) const = 0;
  virtual int GetCarryingStrength () const = 0;
  static truth DamageTypeAffectsInventory (int);
  virtual int GetRandomStepperBodyPart () const;
  entity *GetMotherEntity () const { return MotherEntity; }
  void SetMotherEntity (entity *What) { MotherEntity = What; }
  virtual int CheckForBlock (character *, item *, double, int, int, int);
  int CheckForBlockWithArm (character *, item *, arm *, double, int, int, int);
  void AddBlockMessage (ccharacter *, citem *, cfestring &, truth) const;
  character *GetPolymorphBackup () const { return PolymorphBackup; }
  void SetPolymorphBackup (character *What) { PolymorphBackup = What; }
  cweaponskill *GetCWeaponSkill (int I) const { return &CWeaponSkill[I]; }
  virtual truth AddSpecialSkillInfo (felist &List, int maxNameWdt, int maxLevelWdt,
                                     int maxPointsWdt, int maxNeededWdt) const { return false; }
  virtual truth CheckBalance (double);
  sLong GetStateAPGain (sLong) const;
  virtual sLong GetMoveAPRequirement (int) const;
  virtual void SignalEquipmentAdd (int);
  virtual void SignalEquipmentRemoval (int, citem *);
  const char *GetStateDesctiption (int sidx) const;
  void BeginTemporaryState (sLong, int);
  void GainIntrinsic (sLong);
  void HandleStates ();
  void PrintBeginPolymorphControlMessage () const;
  void PrintEndPolymorphControlMessage () const;
  void PrintBeginLifeSaveMessage () const;
  void PrintEndLifeSaveMessage () const;
  void PrintBeginLycanthropyMessage () const;
  void PrintEndLycanthropyMessage () const;
  void PrintBeginVampirismMessage () const;
  void PrintEndVampirismMessage () const;
  void PrintBeginHasteMessage () const;
  void PrintEndHasteMessage () const;
  void PrintBeginSlowMessage () const;
  void PrintEndSlowMessage () const;
  void PrintBeginSearchingMessage () const;
  void PrintEndSearchingMessage () const;
  void PrintBeginHiccupsMessage () const;
  void PrintEndHiccupsMessage () const;
  void EndPolymorph ();
  void PrintBeginPolymorphLockMessage () const;
  void PrintEndPolymorphLockMessage () const;
  void PolymorphLockHandler ();
  void PrintBeginRegenerationMessage () const;
  void PrintEndRegenerationMessage () const;
  void PrintBeginDiseaseImmunityMessage () const;
  void PrintEndDiseaseImmunityMessage () const;
  void PrintBeginTeleportLockMessage () const;
  void PrintEndTeleportLockMessage () const;
  void PrintBeginSwimmingMessage () const;
  void PrintEndSwimmingMessage () const;
  void PrintBeginFastingMessage () const;
  void PrintEndFastingMessage () const;
  void PrintBeginMindwormedMessage () const;
  void PrintEndMindwormedMessage () const;
  void MindwormedHandler ();
  void TeleportLockHandler ();
  character *ForceEndPolymorph ();
  void LycanthropyHandler ();
  void SearchingHandler ();
  void SaveLife ();
  void BeginInvisibility ();
  void BeginInfraVision ();
  void BeginESP ();
  void EndInvisibility ();
  void EndInfraVision ();
  void EndESP ();
  void HiccupsHandler ();
  void VampirismHandler ();
  void BeginEthereality();
  void EndEthereality ();
  void BeginSwimming ();
  void EndSwimming ();
  character *PolymorphRandomly (int, int, int);
  virtual truth EquipmentEasilyRecognized (int) const { return true; }
  void StartReading (item *, sLong);
  void DexterityAction (int);
  void IntelligenceAction (int);
  virtual void SWeaponSkillTick () {}
  void PrintBeginInvisibilityMessage () const;
  void PrintEndInvisibilityMessage () const;
  void PrintBeginInfraVisionMessage () const;
  void PrintEndInfraVisionMessage () const;
  void PrintBeginESPMessage () const;
  void PrintEndESPMessage () const;
  void PrintBeginEtherealityMessage() const;
  void PrintEndEtherealityMessage() const;
  // If Theoretically != false, range is not a factor.
  truth CanBeSeenBy (ccharacter *Who, truth Theoretically=false, truth IgnoreESP=false) const;
  FORCE_INLINE truth CanBeSeenByPlayer (truth Theoretically=false, truth IgnoreESP=false) const {
    return CanBeSeenBy(PLAYER, Theoretically, IgnoreESP);
  }
  void AttachBodyPart (bodypart *);
  truth HasAllBodyParts () const;
  bodypart *FindRandomOwnBodyPart (truth) const;
  bodypart *GenerateRandomBodyPart ();
  void PrintBeginPoisonedMessage () const;
  void PrintEndPoisonedMessage () const;
  truth IsWarm () const;
  truth IsWarmBlooded() const;
  void CalculateEquipmentState ();
  void Draw (blitdata &BlitData, col16 MonoColor=TRANSPARENT_COLOR) const;
  virtual void DrawBodyParts (blitdata &BlitData, col16 MonoColor=TRANSPARENT_COLOR) const;
  god *GetMasterGod () const;
  void PoisonedHandler ();
  void PrintBeginTeleportMessage () const;
  void PrintEndTeleportMessage () const;
  void TeleportHandler ();
  void DetectHandler ();
  void PrintEndTeleportControlMessage () const;
  void PrintBeginTeleportControlMessage () const;
  void PrintBeginDetectMessage () const;
  void PrintEndDetectMessage () const;
  void PolymorphHandler ();
  void PrintEndPolymorphMessage () const;
  void PrintBeginPolymorphMessage () const;
  virtual void DisplayStethoscopeInfo (character *) const;
  virtual truth CanUseStethoscope (truth) const;
  virtual truth IsUsingArms () const;
  virtual truth IsUsingLegs () const;
  virtual truth IsUsingHead () const;
  dungeon *GetDungeon () const { return static_cast<level *>(GetSquareUnder()->GetArea())->GetDungeon(); }
  level *GetLevel () const { return static_cast<level *>(GetSquareUnder()->GetArea()); }
  area *GetArea () const { return GetSquareUnder()->GetArea(); }
  virtual square *GetNeighbourSquare (int) const;
  virtual lsquare *GetNeighbourLSquare (int) const;
  //virtual wsquare *GetNeighbourWSquare (int) const;
  stack *GetStackUnder (int I = 0) const { return static_cast<lsquare *>(GetSquareUnder(I))->GetStack(); }
  square *GetNearSquare (v2 Pos) const { return GetSquareUnder()->GetArea()->GetSquare(Pos); }
  square *GetNearSquare (int x, int y) const { return GetSquareUnder()->GetArea()->GetSquare(x, y); }
  lsquare *GetNearLSquare (v2 Pos) const { return static_cast<lsquare *>(GetSquareUnder()->GetArea()->GetSquare(Pos)); }
  lsquare *GetNearLSquare (int x, int y) const { return static_cast<lsquare *>(GetSquareUnder()->GetArea()->GetSquare(x, y)); }
  wsquare *GetNearWSquare (v2) const;
  wsquare *GetNearWSquare (int, int) const;
  v2 GetPos (int I = 0) const;
  square *GetSquareUnder (int I = 0) const;
  virtual square *GetSquareUnderEntity (int I = 0) const { return GetSquareUnder(I); }
  lsquare *GetLSquareUnder (int I = 0) const { return static_cast<lsquare *>(GetSquareUnder(I)); }
  int GetRandomNonVitalBodyPart () const;
  void TeleportSomePartsAway (int);
  virtual void SignalVolumeAndWeightChange ();
  virtual void SignalBodyPartVolumeAndWeightChange () {}
  void CalculateVolumeAndWeight ();
  sLong GetVolume () const { return Volume; }
  sLong GetBodyVolume () const { return BodyVolume; }
  sLong GetWeight () const { return Weight; }
  sLong GetCarriedWeight () const { return CarriedWeight; }
  virtual void SignalEmitationIncrease (col24);
  virtual void SignalEmitationDecrease (col24);
  void CalculateEmitation ();
  void CalculateAll ();
  void CalculateHP ();
  void CalculateMaxHP ();
  int GetHP () const { return HP; }
  int GetMaxHP () const { return MaxHP; }
  void CalculateBodyPartMaxHPs (feuLong = MAY_CHANGE_HPS|CHECK_USABILITY);
  truth IsInitializing () const { return !!(Flags & C_INITIALIZING); }
  truth IsInNoMsgMode () const { return !!(Flags & C_IN_NO_MSG_MODE); }
  truth ActivateRandomState (int, int, sLong = 0);
  sLong GetRandomState (int Flags) const;
  sLong GetRandomStateWithPRNG (PRNG &prng, int Flags) const;
  truth GainRandomIntrinsic (int);
  virtual void CalculateBattleInfo () = 0;
  void CalculateBurdenState ();
  virtual void CalculateDodgeValue ();
  virtual void CalculateBodyParts () { BodyParts = 1; }
  virtual void CalculateAllowedWeaponSkillCategories ();
  int GetBodyParts () const { return BodyParts; }
  int GetAllowedWeaponSkillCategories () const { return AllowedWeaponSkillCategories; }

  // this uses polymorph form. it's not right, we need to skip it.
  // but not simply skip: calculate values for both forms, and use the higher one.
  double GetRelativeDanger (ccharacter *Enemy, truth UseMaxHP=false,
                            truth ignoreEnemyPolymorph=false) const;
  FORCE_INLINE double GetRelativeDangerMaxHP (ccharacter *Enemy) const { return GetRelativeDanger(Enemy, true); }
  FORCE_INLINE double GetRelativeDangerNoEnemyPoly (ccharacter *Enemy) const { return GetRelativeDanger(Enemy, false, true); }
  FORCE_INLINE double GetRelativeDangerMaxHPNoEnemyPoly (ccharacter *Enemy) const { return GetRelativeDanger(Enemy, true, true); }
  double GetTimeToDie (ccharacter *Enemy, int Damage, double ToHitValue,
                       truth AttackIsBlockable, truth UseMaxHP) const;
  virtual double GetTimeToKill (ccharacter *Enemy, truth UseMaxHP) const = 0;

  virtual void AddSpecialEquipmentInfo (festring &, int) const {}
  virtual festring GetBodyPartName (int, truth = false) const;
  item *SearchForItem (feuLong) const;
  truth SearchForItem (citem *) const;
  item *SearchForItem (const sweaponskill *) const;
  truth ContentsCanBeSeenBy (ccharacter *) const;
  festring GetBeVerb () const;
  virtual void CreateBlockPossibilityVector (blockvector &, double) const {}
  virtual truth SpecialUnarmedEffect (character *, v2, int, int, truth) { return false; }
  virtual truth SpecialKickEffect (character *, v2, int, int, truth) { return false; }
  virtual truth SpecialBiteEffect (character* Victim, v2 HitPos, int BodyPartIndex,
                                   int Direction, truth BlockedByArmour, truth Critical,
                                   int DoneDamage) { return false; }
  truth HitEffect (character *, item *, v2, int, int, int, truth, truth Critical, int DoneDamage);
  void WeaponSkillHit (item *, int, int);
  character *Duplicate (feuLong = 0);
  room *GetRoom (int I=0) const { return GetLSquareUnder(I)->GetRoom(); }

  // return new danger level (always > 0), or 0 if cannot pickup.
  // the logic is slightly different from `TryToEquip()`.
  // cannot be called for player character (always returns 0).
  // if `originalDanger` is set:
  //   if <= 0: calculate original danger level, store there.
  //   if >0: use this danger level for comparisons.
  // note that if this method returned `0`, `originalDanger` may not be properly set.
  double CheckWantToEquip (item *Item, double *originalDanger);
  // return `true` if equipped.
  // cannot be called for player character (always returns `false`).
  truth TryToEquip (item *Item);

  // use `Item->CanBeEatenByAI()`
  truth TryToConsume (item *Item);
  truth TryToAddToInventory (item *Item, bool checkOnly=false);

  // fast rejects
  virtual bool PreCheckEquipWield (item *Item, int wldidx);
  virtual bool PreCheckEquip (item *Item);
  virtual bool PreCheckPickup (item *Item);
  virtual bool PreCheckConsume (item *Item);

  // do not wear some items...
  virtual bool IsForbiddenItem (citem *Item) const;

  void UpdateESPLOS () const;
  int GetCWeaponSkillLevel (citem *) const;
  virtual int GetSWeaponSkillLevel (citem *) const { return 0; }
  void PrintBeginPanicMessage () const;
  void PrintEndPanicMessage () const;
  void CheckPanic (int);
  character *DuplicateToNearestSquare (character *, feuLong = 0);
  virtual void SignalSpoil (material *m = 0);
  virtual void SignalSpoilLevelChange (material *m = 0);
  virtual truth UseMaterialAttributes () const = 0;
  truth IsPolymorphed () const { return Flags & C_POLYMORPHED; }
  truth IsInBadCondition () const;
  truth IsInBadCondition (int HP) const { return HP * 3 < MaxHP; }
  int GetCondition () const;
  void UpdatePictures ();
  truth CanHeal () const;
  void SetGoingTo (v2);
  int GetRelation (ccharacter *) const;
  truth CalculateAttributeBonuses ();
  void ApplyEquipmentAttributeBonuses (item *);
  void ReceiveAntidote (sLong Amount, bool forceHeal=false);
  void AddAntidoteConsumeEndMessage () const;
  truth IsDead () const;
  void AddOriginalBodyPartID (int, feuLong);
  void AddToInventory (const fearray<itemcontentscript> &, int);
  truth HasHadBodyPart (citem *) const;
  void ProcessAndAddMessage (festring) const;
  virtual truth CheckZap ();
  void SetEndurance (int);
  void SetPerception (int);
  void SetIntelligence (int);
  void SetWisdom (int);
  void SetWillPower (int);
  void SetCharisma (int);
  //void SetMana (int);
  void DamageAllItems (character *, int, int);
  truth Equips (citem *) const;
  void PrintBeginConfuseMessage () const;
  void PrintEndConfuseMessage () const;
  v2 ApplyStateModification (v2) const;
  void AddConfuseHitMessage () const;
  item *SelectFromPossessions (cfestring &, sorter = 0);
  truth SelectFromPossessions (itemvector &, cfestring &, int, sorter = 0);
  truth EquipsSomething (sorter = 0);
  truth CheckTalk ();
  virtual truth CanCreateBodyPart (int) const { return true; }
  virtual truth HandleCharacterBlockingTheWay (character *, v2, int) { return false; }
  virtual festring& ProcessMessage (festring &) const;
  virtual truth IsHumanoid () const { return false; }
  virtual truth IsHuman () const { return false; }
  truth IsOnGround () const;
  virtual truth CheckIfEquipmentIsNotUsable (int) const { return false; }
  virtual int GetWeaponTooHeavyRate (int) const { return WEAPON_UNUSABLE; }
  virtual truth MoveTowardsHomePos ();
  virtual void SetWayPoints (const fearray<packv2> &) {}
  int HasSomethingToEquipAt (int chosen, truth equippedIsTrue); // counter
  feuLong HasSomethingToEquipAtRecentTime (int chosen, truth equippedIsTrue); // returns 0, 1 or pickup time
  int TryToChangeEquipment (stack *, stack *, int); // number of items worn (is this the right word?)
  void PrintBeginParasitizedMessage () const;
  void PrintEndParasitizedMessage () const;
  void ParasitizedHandler ();
  truth CanFollow () const;
  truth LeftOversAreUnique () const;
  virtual festring GetKillName () const;
  festring GetPanelName () const;
  virtual void AddSpecialStethoscopeInfo (felist &) const = 0;
  virtual item *GetPairEquipment (int) const { return 0; }
  bodypart *HealHitPoint ();
  void CreateHomeData ();
  room *GetHomeRoom () const;
  truth HomeDataIsValid () const;
  void SetHomePos (v2);
  void RemoveHomeData ();
  feuLong GetID () const { return ID; }
  void AddESPConsumeMessage () const;
  const std::list<feuLong> &GetOriginalBodyPartID (int) const;
  void GetHitByExplosion (const explosion *, int);
  truth CanBePoisoned () const { return TorsoIsAlive(); }
  truth CanBeParasitized () const { return TorsoIsAlive(); }
  void SortAllItems (const sortdata &);
  character *GetRandomNeighbourEnemy () const;
  void Search(int);
  character *GetRandomNeighbour (int RelationFlags=(HOSTILE|UNCARING|FRIEND), bool anybody=false) const;
  virtual truth IsRetreating () const;
  virtual truth IsMushroom () const { return false; }
  virtual truth IsMagicDrinker () const { return false; }
  virtual truth DrinkMagic (const beamdata&) { return IsMagicDrinker(); }
  void ResetStates ();
  virtual head *Behead () { return 0; }
  void PrintBeginGasImmunityMessage () const;
  void PrintEndGasImmunityMessage () const;
  void ShowAdventureInfo (cfestring &Msg) const;
  void SaveAdventureInfo () const; // this should be called after adding a high-score!
  virtual truth BoundToUse (citem *, int) const { return false; }
  virtual truth IsBananaGrower () const { return false; }
  virtual int GetRandomApplyBodyPart () const;

  virtual void AddAttributeInfo (festring &) const;
  virtual void AddAttackInfo (felist &) const = 0;
  virtual void AddDefenceInfo (felist &) const;
  virtual void DetachBodyPart ();

  void ReceiveHolyBanana (sLong);
  void AddHolyBananaConsumeEndMessage () const;
  void ReceiveHolyMango (sLong);
  void AddHolyMangoConsumeEndMessage () const;
  truth IsHomeLevel (level *lvl) const;
  virtual truth PreProcessForBone ();
  truth PostProcessForBone (double &, int &);
  truth PostProcessForBone ();
  virtual void FinalProcessForBone ();
  virtual truth EditAllAttributes (int Amount);
  virtual void SetSoulID (feuLong);
  virtual truth SuckSoul (character *) { return false; }
  virtual truth MustBeRemovedFromBone () const;
  truth TorsoIsAlive () const { return GetTorso()->IsAlive(); }
  truth PictureUpdatesAreForbidden () const { return Flags & C_PICTURE_UPDATES_FORBIDDEN; }
  virtual int GetUsableArms () const { return 0; }
  truth IsPet () const;
  virtual void PutTo (v2);
  void PutTo (lsquare *);
  truth PutNear (v2, truth allowFail=false);
  truth PutNearNoTeleport (v2);
  truth PutToOrNear (v2, truth allowFail=false);
  truth PutToOrNearNoTeleport (v2);
  virtual void Remove ();
  truth IsSmall () const { return SquaresUnder == 1; }
  truth IsOver (v2) const;
  truth IsOver (citem *) const;
  truth SquareUnderCanBeSeenByPlayer (truth IgnoreDarkness) const;
  truth SquareUnderCanBeSeenBy (ccharacter *Who, truth IgnoreDarkness) const;
  int GetDistanceSquareFrom (ccharacter *) const;
  virtual truth CanTheoreticallyMoveOn (const lsquare *) const;
  virtual truth CanMoveOn (const lsquare *) const;
  virtual truth CanMoveOn (const square *) const;
  truth CanMoveOn (const olterrain *) const;
  truth CanMoveOn (const oterrain *) const;
  truth IsMainPos (v2 What) const { return GetPos() == What; }
  virtual void CalculateSquaresUnder () { SquaresUnder = 1; }
  int GetSquaresUnder () const { return SquaresUnder; }
  virtual int GetSquareIndex (v2) const { return 0; }
  virtual int GetNeighbourSquares () const { return 8; }
  virtual int GetExtendedNeighbourSquares () const { return 9; }
  virtual int CalculateNewSquaresUnder (lsquare **, v2) const;
  virtual truth IsFreeForMe (square *) const;
  void SendNewDrawRequest () const;
  square *GetNaturalNeighbourSquare (int I) const { return character::GetNeighbourSquare(I); }
  lsquare *GetNaturalNeighbourLSquare (int I) const { return character::GetNeighbourLSquare(I); }
  void SignalStepFrom (lsquare **);
  virtual void SpecialBodyDefenceEffect (character *, bodypart *, int) {}
  virtual int GetSumOfAttributes () const;
  truth IsGoingSomeWhere () const { return GoingTo != ERROR_V2; }
  virtual truth CreateRoute ();
  void TerminateGoingTo ();
  virtual truth IsSpy () const { return false; } // does CallForAttention() if true
  virtual truth IsKing () const { return false; } // lost king of Aslona
  virtual truth IsLarge () const { return false; }
  truth CheckForFood (int);
  truth CheckForFoodInSquare (v2);
  // this is used by AI to decide if it needs to eat
  virtual truth CheckIfSatiated () { return GetNP() > SATIATED_LEVEL; }
  virtual void SignalNaturalGeneration () {}
  virtual truth IsBunny () const { return false; }
  virtual truth IsSpider () const { return false; }
  void SetConfig (int, int = 0);
  bodypartslot *GetBodyPartSlot (int I) { return &BodyPartSlot[I]; }
  virtual truth CheckConsume (cfestring &) const;
  virtual int GetTameSymbolSquareIndex () const { return 0; }
  virtual int GetFlySymbolSquareIndex () const { return 0; }
  virtual int GetSwimmingSymbolSquareIndex () const { return 0; }
  virtual int GetUnconsciousSymbolSquareIndex () const { return 0; }
  virtual truth PlaceIsIllegal (v2 Pos, v2 Illegal) const { return Pos == Illegal; }
  liquid *CreateBlood (sLong) const;
  void SpillFluid (character *, liquid*, int = 0);
  virtual void StayOn (liquid *);
  virtual head *GetVirtualHead () const { return 0; }
  truth IsAlly (ccharacter *) const;
  virtual truth CanVomit () const { return TorsoIsAlive(); }
  feuLong GetLastAcidMsgMin () const { return LastAcidMsgMin; }
  void SetLastAcidMsgMin (feuLong What) { LastAcidMsgMin = What; }
  virtual truth AllowSpoil () const { return false; }
  void Disappear (corpse *, cchar *, truth (item::*)() const);
  void ResetSpoiling ();
  virtual character *GetLeader () const;
  int GetMoveType () const;
  virtual truth IsSumoWrestler () const { return false; }
  void InitMaterials (const materialscript *, const materialscript *, truth) {}
  item *SearchForItem (ccharacter *, sorter) const;
  virtual character *CreateZombie () const { return 0; }
  virtual festring GetZombieDescription () const;
  virtual truth IsZombie () const { return false; }
  virtual truth CannotDrinkPotions () const { return false; }
  virtual truth CanAttack () const { return true; }
  truth DetectMaterial (cmaterial *) const;
  truth CheckIfTooScaredToHit (ccharacter *) const;
  void PrintBeginLevitationMessage () const;
  void PrintEndLevitationMessage () const;
  void EditDealExperience (sLong);
  int RawEditExperience (double &, double, double, double) const;
  virtual void LeprosyHandler ();
  virtual void TryToInfectWithLeprosy (ccharacter *);
  void PrintBeginLeprosyMessage () const;
  void PrintEndLeprosyMessage () const;
  void SignalGeneration ();
  void CheckIfSeen ();
  void SignalSeen ();
  truth HasBeenSeen () const;
  int GetPolymorphIntelligenceRequirement () const;
  void RemoveAllItems ();
  int CalculateWeaponSkillHits (ccharacter *) const;
  void DonateEquipmentTo (character *);
  void ReceivePeaSoup (sLong);
  void AddPeaSoupConsumeEndMessage () const;
  void CalculateMaxStamina ();
  void EditStamina (int Amount, truth CanCauseUnconsciousness);
  void RegenerateStamina ();
  void BeginPanic ();
  void EndPanic ();
  int GetTirednessState () const;
  int GetStamina () const { return Stamina; }
  int GetMaxStamina () const { return MaxStamina; }
  void SetGenerationDanger (double What) { GenerationDanger = What; }
  double GetGenerationDanger () const { return GenerationDanger; }
  void ReceiveBlackUnicorn (sLong);
  void ReceiveGrayUnicorn (sLong);
  void ReceiveWhiteUnicorn (sLong);
  void DecreaseStateCounter (sLong, int);
  truth IsImmuneToLeprosy () const;
  bodypart *SearchForOriginalBodyPart (int) const;
  void SetLifeExpectancy (int, int);
  virtual void DuplicateEquipment (character *, feuLong);
  virtual void SignalDisappearance ();
  virtual truth HornOfFearWorks () const;
  virtual truth CanHear () const;
  void BeginLeprosy ();
  void EndLeprosy ();
  void ReceiveOmmelCerumen (sLong);
  void ReceiveOmmelSweat (sLong);
  void ReceiveOmmelTears (sLong);
  void ReceiveOmmelSnot (sLong);
  void ReceiveOmmelBone (sLong);
  void ReceiveOmmelBlood (sLong);
  truth IsSameAs (ccharacter *) const;
  feuLong GetCommandFlags () const;
  void SetCommandFlags (feuLong What) { CommandFlags = What; }
  feuLong GetPossibleCommandFlags () const;
  feuLong GetConstantCommandFlags () const;
  virtual truth AllowEquipment (citem *Item, int EquipmentIndex) const { return true; }
  truth ChatMenu ();
  truth ChangePetEquipment ();
  truth TakePetItems ();
  truth GivePetItems ();
  truth IssuePetCommands ();
  truth ChatIdly ();
  truth EquipmentScreen (stack *, stack *);
  feuLong GetManagementFlags () const;
  cchar *GetVerbalBurdenState () const;
  col16 GetVerbalBurdenStateColor () const;
  virtual int GetAttributeAverage () const;
  virtual cfestring& GetStandVerb () const;
  virtual truth CheckApply () const;
  virtual truth CanForceVomit () const { return CanVomit(); }
  void EndLevitation ();
  virtual truth CanMove () const;
  void CalculateEnchantments ();
  truth GetNewFormForPolymorphWithControl (character *&);
  liquid *CreateSweat (sLong) const;
  truth IsTemporary () const;
  truth TeleportRandomItem (truth);
  truth HasClearRouteTo (v2) const;
  virtual truth IsTransparent () const;
  void SignalPossibleTransparencyChange ();
  int GetCursorData () const;
  void TryToName ();
  double GetSituationDanger (ccharacter *, v2, v2, truth) const;
  virtual void ModifySituationDanger (double &) const;
  void LycanthropySituationDangerModifier (double &) const;
  void PoisonedSituationDangerModifier (double &) const;
  void PolymorphingSituationDangerModifier (double &) const;
  void PanicSituationDangerModifier (double &) const;
  void ConfusedSituationDangerModifier (double &) const;
  void ParasitizedSituationDangerModifier (double &) const;
  void LeprosySituationDangerModifier (double &) const;
  void HiccupsSituationDangerModifier (double &) const;
  void VampirismSituationDangerModifier (double &) const;
  virtual truth TryQuestTalks () { return false; }
  virtual truth TryToTalkAboutScience ();
  virtual truth IsUsingWeaponOfCategory (int) const;
  virtual truth IsKamikazeDwarf () const { return false; }
  void AddRandomScienceName (festring &) const;
  truth IsStuck () const { return !!TrapData; }
  festring GetTrapDescription () const;
  truth TryToUnStickTraps (v2);
  void RemoveTrap (feuLong);
  void AddTrap (feuLong, feuLong);
  truth IsStuckToTrap (feuLong) const;
  void RemoveTraps ();
  void RemoveTraps (int);
  int RandomizeHurtBodyPart (feuLong) const;
  virtual int RandomizeTryToUnStickBodyPart (feuLong) const { return NONE_INDEX; }
  truth BodyPartIsStuck (int) const;
  void PrintAttribute (cchar *, int, int, int) const;
  virtual truth AllowUnconsciousness () const;
  truth CanPanic () const;
  int GetRandomBodyPart (feuLong = ALL_BODYPART_FLAGS) const;
  virtual truth CanChokeOnWeb (web *) const { return CanChoke(); }
  virtual truth BrainsHurt () const { return false; }
  truth IsSwimming () const;
  truth IsAnimated () const;
  virtual truth IsPlayerKind () const { return false; }
  truth HasBodyPart (sorter) const;
  truth PossessesItem (sorter) const;
  truth MoreThanOnePossessesItem (sorter) const;
  item *FirstPossessesItem (sorter Sorter) const;
  truth IsFlying () const { return GetMoveType() & FLY; }
  virtual cchar *GetRunDescriptionLine (int) const;
  void VomitAtRandomDirection (int);
  virtual truth SpecialSaveLife () { return false; }
  void RemoveLifeSavers ();
  virtual ccharacter *FindCarrier () const;
  virtual cchar *GetNormalDeathMessage () const;
  virtual bool IsConscious () const;
  void ForcePutNear (v2);
  void PrintBeginFearlessMessage() const;
  void PrintEndFearlessMessage() const;
  void BeginFearless();
  void EndFearless();
  virtual void ApplySpecialAttributeBonuses () {}
  void ReceiveMustardGas (int, sLong);
  void ReceiveMustardGasLiquid (int, sLong);
  truth IsBadPath (v2) const;
  //double &GetExpModifierRef (expid);/*k8: wtf is this?*/
  truth ForgetRandomThing ();
  void ApplyAllGodsKnownBonus ();
  item *GiveMostExpensiveItem (character *);
  void ReceiveItemAsPresent (item *);
  item *FindMostExpensiveItem () const;
  truth ReceiveSirenSong (character *Siren);
  void StealItemFrom (character *Victim, item *ToSteal); // does no checks!
  character *GetNearestEnemy () const;
  //truth IsInfectedByMindWorm () const { return !CounterToMindWormHatch; } // replaced by state
  //void SetCounterToMindWormHatch (int What) { CounterToMindWormHatch = What; } // replaced by state
  virtual truth MindWormCanPenetrateSkull (mindworm *) const;
  virtual truth CanTameWithDulcis (const character *) const;
  virtual truth CanTameWithLyre (const character *) const;
  virtual truth CanTameWithScroll (const character *) const;
  virtual truth CanTameWithResurrection (const character*) const;
  virtual truth IsCharmable () const { return GetTamingDifficulty() != NO_TAMING; }
  virtual truth CheckSadism ();
  virtual truth CheckThrowItemOpportunity ();
  virtual truth CheckAIZapOpportunity ();
  virtual int GetAdjustedStaminaCost (int BaseCost, int Attribute); // from comm. fork
  virtual truth CanThrowItem (item *Item);
  virtual truth HasSadistAttackMode () const { return IsUsingLegs(); }
  truth CheckForBeverage ();
  void Haste ();
  void Slow ();
  virtual void SurgicallyDetachBodyPart ();
  truth IsAllowedInDungeon (int dunIndex);

  inline cchar *GetClassID () const { return (FindProtoType() ? FindProtoType()->GetClassID() : ""); }
  virtual const prototype *FindProtoType () const { return &ProtoType; }

  // aconfig == -1: any
  virtual item *FindFirstEquippedItem (cfestring &aclassname, int aconfig=-1) const;
  virtual item *FindFirstInventoryItem (cfestring &aclassname, int aconfig=-1) const;
  virtual item *FindFirstItem (cfestring &aclassname, int aconfig=-1) const;

public:
  // attack flash code
  truth DrawVFX (col16 MonoColor=TRANSPARENT_COLOR) const;

  struct VFXInfo {
    ccharacter *attacker;
    ccharacter *victim;
    // char might die, so we need to save its square
    lsquare *atkSqr;
    lsquare *vicSqr;
    bool atkDrawn;
    bool vicDrawn;
  };

  static void DrawAttackFlash (VFXInfo *info, ccharacter *attacker, ccharacter *victim);
  static void AttackFlashDelay (const VFXInfo *info);
  static void EraseAttackFlash (VFXInfo *info);

protected:
  virtual festring DebugGetName () override;
  int ItemComparisonWieldIdx (const item *Item, int *countp);
  void CreateItemComparisonInfo (const item *Item, int pileCount, bool &renderEqu);
  void CreateOtherItemInfo (const item *Item, int pileCount, bool &renderMore);

  void FillHiScoreInfo (highscore::Info &info) const;

  // helper; will *NOT* clear `foundList`!
  void FindGoodPotions (itemvector &foundList,
                        bool (*checkCB) (character *Char, material *mat, void *udata),
                        void *udata);
  truth ConsumeRandomPotion (itemvector &foundList);

  static truth DamageTypeDestroysBodyPart (int);
  int FixLeftRightSelected (int selected);
  virtual void LoadSquaresUnder ();
  virtual bodypart *MakeBodyPart (int) const;
  virtual void SpecialTurnHandler () {}
  void Initialize (int, int);
  virtual void PostConstruct () {}
  void LoadDataBaseStats ();
  virtual v2 GetBodyPartBitmapPos (int, truth = false) const;
  virtual col16 GetBodyPartColorA (int, truth = false) const;
  virtual col16 GetBodyPartColorB (int, truth = false) const;
  virtual col16 GetBodyPartColorC (int, truth = false) const;
  virtual col16 GetBodyPartColorD (int, truth = false) const;
  virtual int GetBodyPartSparkleFlags (int) const;
  virtual sLong GetBodyPartSize (int, int) const;
  virtual sLong GetBodyPartVolume (int) const;
  void UpdateBodyPartPicture (int I, truth);
  int ChooseBodyPartToReceiveHit (double, double);
  virtual void CreateBodyParts (int);
  virtual material *CreateBodyPartMaterial (int, sLong) const;
  virtual truth ShowClassDescription () const { return true; }
  void SeekLeader (ccharacter *);
  virtual v2 FindUsefulItemAround (truth SameRoom, feuLong *itemID=0); // return ERROR_V2 or position
  virtual truth CheckForUsefulItemsOnGround (truth CheckFood=true);
  virtual truth NeedCheckNearbyItems () const { return true; } // for AI; disables finder AI
  virtual truth NeedCheckNearbyEquipment () const { return true; } // for AI
  virtual truth NeedCheckNearbyFood () const { return true; } // for AI
  virtual truth CheckDrink ();
  truth CheckForDoors ();
  truth CheckForEnemies (truth CheckDoors, truth CheckGround, truth MayMoveRandomly,
                        truth RunTowardsTarget=false);
  truth FollowLeader (character *);
  void StandIdleAI ();
  virtual void CreateCorpse (lsquare *);
  void GetPlayerCommand ();
  virtual void GetAICommand ();
  truth MoveTowardsTarget (truth Run);
  virtual cchar *FirstPersonUnarmedHitVerb () const;
  virtual cchar *FirstPersonCriticalUnarmedHitVerb () const;
  virtual cchar *ThirdPersonUnarmedHitVerb () const;
  virtual cchar *ThirdPersonCriticalUnarmedHitVerb () const;
  virtual cchar *FirstPersonKickVerb () const;
  virtual cchar *FirstPersonCriticalKickVerb () const;
  virtual cchar *ThirdPersonKickVerb () const;
  virtual cchar *ThirdPersonCriticalKickVerb () const;
  virtual cchar *FirstPersonBiteVerb () const;
  virtual cchar *FirstPersonCriticalBiteVerb () const;
  virtual cchar *ThirdPersonBiteVerb () const;
  virtual cchar *ThirdPersonCriticalBiteVerb () const;
  virtual cchar *UnarmedHitNoun () const;
  virtual cchar *KickNoun () const;
  virtual cchar *BiteNoun () const;
  virtual truth AttackIsBlockable (int) const { return true; }
  virtual truth AttackMayDamageArmor () const { return true; }
  virtual int GetSpecialBodyPartFlags (int) const { return ST_NORMAL; }
  virtual int GetBodyPartWobbleData (int) const { return 0; }
  virtual int ModifyBodyPartHitPreference (int, int Modifier) const { return Modifier; }
  virtual int ModifyBodyPartToHitChance (int, int Chance) const { return Chance; }
  virtual truth CanPanicFromSeveredBodyPart () const { return true; }
  virtual void SpecialBodyPartSeverReaction () {}
  truth AttackAdjacentEnemyAI ();
  double RandomizeBabyExperience (double);
  static truth IsLimbIndex (int);
  virtual truth AllowExperience () const { return true; }

  truth GeneralHasItem (ItemCheckerCB chk, truth checkContainers=false) const;
  int GeneralRemoveItem (ItemCheckerCB chk, truth allItems=false, truth checkContainers=false); // return count
  int GeneralRemoveItemTo (ItemCheckerCB chk, character *ToWhom, truth allItems=false, truth checkContainers=false); // return count

public:
  // for console
  int GetTempStateCounterInternal (int idx) const {
    return (idx >= 0 && idx < STATES ? TemporaryStateCounter[idx] : 0);
  }

  void RemoveItemFromIgnoreList (citem *Item);
  void AddItemToIgnoreList (citem *Item, cchar *debugWhy);
  truth CheckItemIgnoreCooldown (citem *Item); // return `true` if allowed

public:
  static const prototype ProtoType;
  stack *Stack;
  sLong NP, AP;
  sLong TemporaryState;
  int TemporaryStateCounter[STATES];
  team *Team;
  v2 GoingTo;
  sLong Money;
  int CurrentSweatMaterial;
  bodypartslot *BodyPartSlot;
  festring AssignedName;
  action *Action;
  const database *DataBase;
  double BaseExperience[BASE_ATTRIBUTES];
  std::list<feuLong> *OriginalBodyPartID;
  entity *MotherEntity;
  character *PolymorphBackup;
  cweaponskill *CWeaponSkill;
  sLong EquipmentState;
  square **SquareUnder;
  sLong Volume;
  sLong Weight;
  sLong CarriedWeight;
  sLong BodyVolume;
  int HP;
  int MaxHP;
  int BurdenState;
  double DodgeValue;
  int AllowedWeaponSkillCategories;
  int BodyParts;
  sLong RegenerationCounter;
  int AttributeBonus[BASE_ATTRIBUTES];
  int CarryingBonus;
  homedata *HomeData;
  feuLong ID;
  int SquaresUnder;
  std::vector<v2> Route;
  std::unordered_set<v2> Illegal;
  feuLong LastAcidMsgMin;
  int Stamina;
  int MaxStamina;
  int BlocksSinceLastTurn;
  double GenerationDanger;
  feuLong CommandFlags;
  feuLong WarnFlags;
  int ScienceTalks;
  trapdata *TrapData;
  //expmodifiermap ExpModifierMap;/*k8: wtf is this?*/
  //int CounterToMindWormHatch; // replaced by state

  // for finded AI; give up if we can't get to the item for some time.
  // the key is item ID, the value is cooldown time (turns).
  std::unordered_map<feuLong, feuLong> ItemIgnores;

public:
  void ClearComparisonInfo ();
  void UpdateComparisonInfo ();

  FORCE_INLINE bool HasComparisonInfo () const {
    return (!mComparisonInfo.empty() || !mOtherItemInfo.empty());
  }

  FORCE_INLINE const std::vector<festring> &GetGearComparisonInfo () const { return mComparisonInfo; }
  FORCE_INLINE const std::vector<festring> &GetOtherComparisonInfo () const { return mOtherItemInfo; }

protected:
  int mPrevMoveDir;
  std::vector<festring> mComparisonInfo;
  // other interesting items for popup
  std::vector<festring> mOtherItemInfo;
};


#ifdef __FILE_OF_STATIC_CHARACTER_PROTOTYPE_DEFINITIONS__
#define CHARACTER_PROTO(name, base)\
template<> const characterprototype\
  name##sysbase::ProtoType(&base::ProtoType,\
         (characterspawner)(&name##sysbase::Spawn),\
         (charactercloner)(&name##sysbase::Clone), #name);
#else
#define CHARACTER_PROTO(name, base)
#endif


#define CHARACTER(name, base)\
class name;\
typedef sysbase<name, base, characterprototype> name##sysbase;\
CHARACTER_PROTO(name, base)\
class name : public name##sysbase


#endif
