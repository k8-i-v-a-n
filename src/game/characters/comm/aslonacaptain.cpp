#ifdef HEADER_PHASE
CHARACTER(aslonacaptain, guard) {
public:
  aslonacaptain ();
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth TryQuestTalks () override;
  virtual void BeTalkedTo () override;

protected:
  truth HasBeenSpokenTo;
};


#else


aslonacaptain::aslonacaptain () : HasBeenSpokenTo(false) {}


void aslonacaptain::Save (outputfile &SaveFile) const {
  humanoid::Save(SaveFile);
  SaveFile << HasBeenSpokenTo;
}


void aslonacaptain::Load (inputfile &SaveFile) {
  humanoid::Load(SaveFile);
  SaveFile >> HasBeenSpokenTo;
}


truth aslonacaptain::TryQuestTalks () {
  if (GetRelation(PLAYER) != HOSTILE) {
    if (!HasBeenSpokenTo && game::GetAslonaStoryState() > 1) {
      game::TextScreen(
        CONST_S("\"Finally someone not tangled up in their responsibilities! I was almost ready to drop\n"
                "everything and go myself, but thankfully you are here.\"\n\n"
                "\"Let me explain. When king Othyr died, Seges rest his soul, and Harvan fled justice to\n"
                "start the rebellion, the crown prince, His Highness Artorius, was away on a visit to Castle\n"
                "Noth. I sent for him to immediately return back to the Castle of Aslona, both for safety\n"
                "and to pay respects to his father. But his retinue never arrived. My scouts found marks\n"
                "of an ambush and tracked the responsible raiding party of goblins back to their lair\n"
                "in a nearby ruined fort, where we also believe they imprisoned the young prince.\"\n\n"
                "\"I would have loved to throw the whole army of Aslona at the goblins, but with\n"
                "the ceaseless attacks of rebel squads, that would mean sacrificing the kingdom.\n"
                "I was forced to wait for a momentary respite from the fighting, or for some trustworthy\n"
                "outsider willing to go on a rescue mission.\"\n\n"
                "\"It tears at my heart that poor prince languishes in some jail cell, as if his father's\n"
                "death wasn't hard on him already. Save prince Artorius, I beg of you, and bring him to me\n"
                "so that I need to worry no more.\""));
      game::RevealPOI(game::goblinfortPOI());
      GetArea()->SendNewDrawRequest();
      ADD_MESSAGE("%s says: \"Hurry, please.\"", CHAR_NAME(DEFINITE));
      HasBeenSpokenTo = true;
      return true;
    }

    if (HasBeenSpokenTo) {
      // Does the player have prince Artorius in his team?
      character *CrownPrince = 0;
      for (character *p : game::GetTeam(PLAYER_TEAM)->GetMember()) {
        if (p->IsEnabled() && !p->IsPlayer() && p->IsKing()) {
          CrownPrince = p;
        }
      }

      if (CrownPrince) {
        if (game::TruthQuestion(CONST_S("Entrust young prince to Lord Mittrars' care?"), REQUIRES_ANSWER)) {
          team *Team = game::GetTeam(ASLONA_TEAM);
          CrownPrince->ChangeTeam(Team);
          ADD_MESSAGE("\"Uncle Mittrars!\"");
          ADD_MESSAGE("\"My prince! Thanks Seges and all the gods of Law, I was already loosing any hope that I will see you again.\"");
          game::SetAslonaStoryState(game::GetAslonaStoryState() + 1);
          return true;
        }
      }
    }
  }

  return false;
}


void aslonacaptain::BeTalkedTo () {
  // we don't want "do you have something to give me?" questions here
  humanoid::BeTalkedTo();
}


#endif
