#ifdef HEADER_PHASE
CHARACTER(aslonapriest, priest) {
public:
  aslonapriest ();
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth TryQuestTalks () override;

protected:
  truth HasBeenSpokenTo;
};


#else


aslonapriest::aslonapriest () : HasBeenSpokenTo(false) {}


void aslonapriest::Save (outputfile &SaveFile) const {
  humanoid::Save(SaveFile);
  SaveFile << HasBeenSpokenTo;
}


void aslonapriest::Load (inputfile &SaveFile) {
  humanoid::Load(SaveFile);
  SaveFile >> HasBeenSpokenTo;
}


truth aslonapriest::TryQuestTalks () {
  if (GetRelation(PLAYER) != HOSTILE) {
    if (!HasBeenSpokenTo && game::GetAslonaStoryState() > 1) {
      game::TextScreen(
        CONST_S("\"Yes, I can most definitely put your skills to good use. You see, this senseless\n"
                "war has already claimed many lives by the blades of the soldiers, but a more insidious\n"
                "enemy is rearing her ugly head. I'm talking about Scabies. With many injured and access\n"
                "to supplies limited, diseases are starting to spread, and even my skills and magic\n"
                "are not enough without proper medical care and hygiene. Hygiene is essential to health,\n"
                "but essential to hygiene is access to clean water, which is one of the things this castle\n"
                "lacks right now.\"\n\n"
                "\"I have found some adventurer's journal describing an artifact that might help solve\n"
                "our troubles. A single tear of Silva, petrified into the form of an obsidian shard, yet still\n"
                "weeping with rains of freshwater. I would ask of you to retrieve this shard for me. It should\n"
                "be located in a nearby coal cave, though you should know the journal mentioned strange fungal\n"
                "growths appearing in the cave, nourished by the life-giving water.\""));
      game::RevealPOI(game::fungalcavePOI());
      GetArea()->SendNewDrawRequest();
      ADD_MESSAGE("%s says: \"Thank you very much for your kind help.\"", CHAR_NAME(DEFINITE));
      HasBeenSpokenTo = true;
      return true;
    }

    if (PLAYER->HasWeepObsidian()) {
      if (game::TruthQuestion(CONST_S("Turn in the weeping obsidian?"), REQUIRES_ANSWER)) {
        PLAYER->RemoveWeepObsidian(this);
        ADD_MESSAGE("%s says: \"Wonderful! Let me get to work right away.\"", CHAR_NAME(DEFINITE));
        game::SetAslonaStoryState(game::GetAslonaStoryState() + 1);
        return true;
      }
    }
  }

  return false;
}


#endif
