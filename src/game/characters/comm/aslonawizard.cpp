#ifdef HEADER_PHASE
CHARACTER(aslonawizard, humanoid) {
public:
  aslonawizard ();
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth TryQuestTalks () override;

protected:
  int GetSpellAPCost () const;
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;

protected:
  truth HasBeenSpokenTo;
};


#else


aslonawizard::aslonawizard () : HasBeenSpokenTo(false) {}


void aslonawizard::Save (outputfile &SaveFile) const {
  humanoid::Save(SaveFile);
  SaveFile << HasBeenSpokenTo;
}


void aslonawizard::Load (inputfile &SaveFile) {
  humanoid::Load(SaveFile);
  SaveFile >> HasBeenSpokenTo;
}


int aslonawizard::GetSpellAPCost () const {
  return 1000; // elder mage
}


void aslonawizard::GetAICommand () {
  SeekLeader(GetLeader());

  if (FollowLeader(GetLeader())) return;

  /*
   * Teleports when in danger, otherwise either blinks his allies close to
   * an enemy, or summons a gas golem.
   */

  character *NearestEnemy = 0;
  sLong NearestEnemyDistance = 0x7FFFFFFF;
  character *RandomFriend = 0;
  charactervector Friend;
  v2 Pos = GetPos();

  for (int c = 0; c < game::GetTeams(); c += 1) {
    if (GetTeam()->GetRelation(game::GetTeam(c)) == HOSTILE) {
      for (character *p : game::GetTeam(c)->GetMember()) {
        if (p->IsEnabled()) {
          sLong ThisDistance = Max(abs(p->GetPos().X - Pos.X), abs(p->GetPos().Y - Pos.Y));
          if ((ThisDistance < NearestEnemyDistance ||
               (ThisDistance == NearestEnemyDistance && !RAND_N(3))) && p->CanBeSeenBy(this))
          {
            NearestEnemy = p;
            NearestEnemyDistance = ThisDistance;
          }
        }
      }
    } else if (GetTeam()->GetRelation(game::GetTeam(c)) == FRIEND) {
      for (character *p : game::GetTeam(c)->GetMember()) {
        if (p->IsEnabled() && p->CanBeSeenBy(this)) {
          Friend.push_back(p);
        }
      }
    }
  }

  //k8: i believe that it meant to be `RAND_4`.
  //k8: confirmed by red_kangaroo here: https://attnam.com/posts/31364
  if (NearestEnemy && NearestEnemy->GetPos().IsAdjacent(Pos) &&
      (StateIsActivated(PANIC) || !RAND_4))
  {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s invokes a spell and disappears.", CHAR_NAME(DEFINITE));
    }
    TeleportRandomly(true);
    EditAP(-GetSpellAPCost());
    return;
  }

  if (!RAND_2 && CheckAIZapOpportunity()) return;

  if (NearestEnemy && (NearestEnemyDistance < 10 || StateIsActivated(PANIC)) && RAND_4) {
    SetGoingTo((Pos << 1) - NearestEnemy->GetPos());
    if (MoveTowardsTarget(true)) return;
  }

  if (Friend.size() && !RAND_4) {
    RandomFriend = Friend[RAND_N((int)Friend.size())];
    NearestEnemy = 0;
  }

  if (GetRelation(PLAYER) == HOSTILE && PLAYER->CanBeSeenBy(this)) {
    NearestEnemy = PLAYER;
  }

  beamdata Beam(
    this,
    CONST_S("killed by the spells of ") + GetName(INDEFINITE),
    YOURSELF,
    0
  );

  if (NearestEnemy) {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s invokes a spell!", CHAR_NAME(DEFINITE));
    }

    if (RandomFriend && !RAND_N(4)) {
      EditAP(-GetSpellAPCost());
      RandomFriend->GetLSquareUnder()->DrawParticles(RED);
      RandomFriend->TeleportNear(NearestEnemy);
      return;
    } else {
      lsquare *Square = NearestEnemy->GetLSquareUnder();
      character *ToBeCalled = 0;

      EditAP(-GetSpellAPCost());

      const int GasMaterial[] = { MUSTARD_GAS, MAGIC_VAPOUR, SLEEPING_GAS, TELEPORT_GAS,
                                  EVIL_WONDER_STAFF_VAPOUR, EVIL_WONDER_STAFF_VAPOUR };
      ToBeCalled = golem::Spawn(GasMaterial[RAND_N(6)]);
      v2 Where = GetLevel()->GetNearestFreeSquare(ToBeCalled, Square->GetPos());

      if (Where == ERROR_V2) {
        if (CanBeSeenByPlayer()) ADD_MESSAGE("Nothing happens.");
        delete ToBeCalled;
      } else {
        ToBeCalled->SetGenerationDanger(GetGenerationDanger());
        ToBeCalled->SetTeam(GetTeam());
        ToBeCalled->PutTo(Where);
        if (ToBeCalled->CanBeSeenByPlayer()) {
          ADD_MESSAGE("Suddenly %s materializes!", ToBeCalled->CHAR_NAME(INDEFINITE));
        }
        ToBeCalled->GetLSquareUnder()->DrawParticles(RED);
      }

      if (CanBeSeenByPlayer()) {
        NearestEnemy->DeActivateVoluntaryAction(CONST_S("The spell of ") + GetName(DEFINITE) +
                                                CONST_S(" interrupts you."));
      } else {
        NearestEnemy->DeActivateVoluntaryAction(CONST_S("The spell interrupts you."));
      }
      return;
    }
  }

  StandIdleAI();
}


truth aslonawizard::TryQuestTalks () {
  if (GetRelation(PLAYER) != HOSTILE) {
    if (!HasBeenSpokenTo && game::GetAslonaStoryState() > 1) {
      game::TextScreen(
        CONST_S("\"You have been sent by Lord Regent? Excellent! As the only protector of Aslona from\n"
                "supernatural incursions, I cannot leave the castle, so I shall welcome any help you\n"
                "can provide.\"\n\n"
                "\"My request for you might cause some to brand me a coward. But no matter. We must win\n"
                "the war quickly, or Aslona looses even if we eventually achieve victory. Swords and\n"
                "spells may have won earlier battles, but now we need to end the rebels in one fell swoop.\n"
                "Lord Mittrars is already working hard on locating the command centre of Harvan's forces,\n"
                "so my task is to arrange for the weapon.\"\n\n"
                "\"My research shows that such a weapon capable of complete obliteration appears in many\n"
                "tales from the long lost empire of Otoul'iv Ik-Omit. I have uncovered the site of\n"
                "a pyramid dating back to that era, and all sources indicate that deep within, an untouched\n"
                "arms depot should be located.\"\n\n"
                "\"Please fetch me this mighty weapon post haste. I'm sure you will recognize it once you see it.\""));
      game::RevealPOI(game::pyramidPOI());
      GetArea()->SendNewDrawRequest();
      ADD_MESSAGE("%s says with a concern: \"Be careful, the pyramid is said to be a dangerous place. Better be prepared when you go there.\"", CHAR_NAME(DEFINITE));
      HasBeenSpokenTo = true;
      return true;
    }

    if (PLAYER->HasNuke()) {
      if (game::TruthQuestion(CONST_S("Turn in the thaumic bomb?"), REQUIRES_ANSWER)) {
        PLAYER->RemoveNuke(this);
        ADD_MESSAGE("%s beams: \"Yes! Thank you, thank you! With this, we can blow the rebels to tiny bits, we can end the war!\"", CHAR_NAME(DEFINITE));
        game::SetAslonaStoryState(game::GetAslonaStoryState() + 1);
        return true;
      }
    }
  }

  return false;
}


void aslonawizard::CreateCorpse (lsquare *Square) {
  game::GetCurrentLevel()->GasExplosion(gas::Spawn(MAGIC_VAPOUR, 100), Square, this);
  SendToHell();
}


#endif
