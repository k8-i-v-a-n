#ifdef HEADER_PHASE
CHARACTER(gasghoul, zombie) {
public:
  virtual int TakeHit (character*, item*, bodypart*, v2, double, double, int, int, int, truth, truth) override;
  virtual truth AllowSpoil() const override;
};


#else


truth gasghoul::AllowSpoil () const { return false; }


int gasghoul::TakeHit (character *Enemy, item *Weapon, bodypart *EnemyBodyPart,
                       v2 HitPos, double Damage, double ToHitValue,
                       int Success, int Type, int Direction, truth Critical, truth ForceHit)
{
  int res = humanoid::TakeHit(Enemy, Weapon, EnemyBodyPart, HitPos, Damage,
                              ToHitValue, Success, Type, Direction, Critical, ForceHit);

  if (res != HAS_DODGED && res != HAS_BLOCKED && GetLSquareUnder()->IsFlyable()) {
    if (Enemy->IsPlayer()) {
      ADD_MESSAGE("%s releases a cloud of fumes as you strike %s.",
                  CHAR_DESCRIPTION(DEFINITE), GetObjectPronoun().CStr());
    } else if (IsPlayer()) {
      ADD_MESSAGE("You release a cloud of fumes as %s strikes you.",
                  Enemy->CHAR_DESCRIPTION(DEFINITE));
    } else if (CanBeSeenByPlayer() && Enemy->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s releases a cloud of fumes as %s strikes %s.",
                  CHAR_DESCRIPTION(DEFINITE), Enemy->CHAR_DESCRIPTION(DEFINITE),
                  GetObjectPronoun().CStr());
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s releases a cloud of fumes as something strikes %s.",
                  CHAR_DESCRIPTION(DEFINITE), GetObjectPronoun().CStr());
    }

    int GasMaterial[] = { MUSTARD_GAS, SKUNK_SMELL, ACID_GAS/*, FIRE_GAS*/ };

    if (Critical) {
      GetLevel()->GasExplosion(gas::Spawn(GasMaterial[RAND_N(3)], 100), GetLSquareUnder(), this);
    } else if (RAND_N(10) > 5) {
      // mustard gas is rarer
      GetLSquareUnder()->AddSmoke(gas::Spawn(GasMaterial[RAND_N(3)], 100));
    } else {
      GetLSquareUnder()->AddSmoke(gas::Spawn(GasMaterial[1 + RAND_N(2)], 100));
    }
  }

  return res;
}

#endif
