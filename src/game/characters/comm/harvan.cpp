#ifdef HEADER_PHASE
CHARACTER(harvan, humanoid) {
public:
  virtual truth SpecialEnemySightedReaction (character *) override;
  virtual truth TryQuestTalks () override;

protected:
  virtual void GetAICommand () override;
};


#else


void harvan::GetAICommand () {
  StandIdleAI();
}


truth harvan::TryQuestTalks () {
  if (GetRelation(PLAYER) != HOSTILE) {
    if (!game::GetRebelStoryState()) {
      game::TextScreen(
        CONST_S("\"Well met, adventurer! It's always nice to talk to someone unaffected by\n"
                "the unfortunate quarrels of our kingdom.\"\n\n"
                "\"I don't know how much have you heard, but our old king Othyr was murdered\n"
                "and then even the crown prince Artorius disappeared in an alleged goblin raid.\n"
                "Immediately, Lord Efra Peredivall named himself the Lord Regent of Aslona and\n"
                "made it known that he intends to \1Yuse\2 his newfound power. I have known\n"
                "Lord Peredivall for most of my life, I used to call him my friend, but I cannot\n"
                "stand for this high treason, and the people of Aslona support me. But now we must\n"
                "hide in the woods like brigands, hunted by those corrupt or mislead by\n"
                "Lord Regent's lies. I fear we will need some edge if we want to win this war\n"
                "without drowning in blood.\""));
      game::TextScreen(
        CONST_S("\"Ah-hah! And here I'm talking without realizing you could be of a great help!\n"
                "You are an obvious foreigner, hardly suspected of any connections to us. You could\n"
                "infiltrate Lord Regent's forces and try to glean his plans. Maybe even offer\n"
                "your services and whatever he asks of you to acquire, bring to us instead!\n"
                "Yes, go to the Castle of Aslona, help our cause and once we achieve victory,\n"
                "I will reward you handsomely.\""));
      GetArea()->SendNewDrawRequest();
      ADD_MESSAGE("%s pats you on the back. \"Good luck and return soon, my friend.\"", CHAR_NAME(DEFINITE));
      game::SetRebelStoryState(2); // To have same StoryState values as Aslona.
      return true;
    }

    if (PLAYER->HasMasamune()) {
      if (game::TruthQuestion(CONST_S("Turn in the noble katana named E-numa sa-am?"), REQUIRES_ANSWER)) {
        //game::PlayVictoryMusic();
        game::TextScreen(
          CONST_S("You hand over the ancient katana and thus the regalia necessary to crown a new\n"
                  "king of Aslona are together again.\n\n"
                  "\"Thank you, my friend,\" Harvan says and then turns to his army. \"Comrades,\n"
                  "this is our hour of victory. You have fought well. You are heroes, all of you.\n"
                  "Our country and the very soul of our nation was saved only thanks to you, thanks\n"
                  "to your courage, loyalty, selflessness and resolve. But now, the war is over.\n"
                  "The traitors shall face justice and peace will return to our homes. Gods bless Aslona!\"\n\n"
                  "A deafening cheer echoes his words.\n\nYou are victorious!"));
        game::GetCurrentArea()->SendNewDrawRequest();
        game::DrawEverything();
        // Did the player do all quests just for rebels?
        festring Msg;
        if (game::GetRebelStoryState() == 5) {
          Msg = CONST_S("helped the rebels to an overwhelming victory");
          PLAYER->AddPolymorphedText(Msg, "while");
          PLAYER->AddScoreEntry(Msg, 4, false);
        } else {
          Msg = CONST_S("helped the rebels to an uneasy victory");
          PLAYER->AddPolymorphedText(Msg, "while");
          PLAYER->AddScoreEntry(Msg, 2, false);
        }
        PLAYER->ShowAdventureInfo(Msg);
        game::End(Msg);
        return true;
      }
    }

    if (PLAYER->HasNuke()) {
      if (game::TruthQuestion(CONST_S("Turn in the thaumic bomb?"), REQUIRES_ANSWER)) {
        PLAYER->RemoveNuke(this);
        ADD_MESSAGE("\"So this is the fate Lord Regent had planned for me and my people. "
                    "Thank you for saving all of our lives, %s.\"",
                    PLAYER->GetAssignedName().CStr());
        game::SetRebelStoryState(game::GetRebelStoryState() + 1);
        return true;
      }
    }

    if (PLAYER->HasWeepObsidian()) {
      if (game::TruthQuestion(CONST_S("Turn in the weeping obsidian?"), REQUIRES_ANSWER)) {
        PLAYER->RemoveWeepObsidian(this);
        ADD_MESSAGE("\"The royalists are starting to feel their isolation in the castle, "
                    "it would seem. Sooner or later, they will loose their strength to oppose us.\"");
        game::SetRebelStoryState(game::GetRebelStoryState() + 1);
        return true;
      }
    }

    if (game::GetRebelStoryState() == 5 && game::GetStoryState() < 3) {
      game::TextScreen(
        CONST_S("\"You are a hero to me, you should know that. You have already done so much,\n"
                "yet I must ask for one last favor.\"\n\n"
                "\"This war is costly in both innocent lives and money we cannot spare,\n"
                "it needs to end right now. Thanks to your help, we have the thaumic bomb and\n"
                "could level the whole castle and wipe Lord Regent and his royalists from the surface\n"
                "of the world, but I'm unwilling to sacrifice everyone in the castle and the castle\n"
                "with itself. It would be a hollow victory to kill the traitor, but loose the kingdom\n"
                "when the symbol of our proud history was in ruins.\"\n\n"
                "\"Yet there is a third option. It is my sincere belief that many of Lord Peredivall's\n"
                "troops would change sides if prince Artorius took on the title of his father as\n"
                "the rightful king of Aslona. But for His Highness to be crowned the king without\n"
                "any doubts or dispute, we need the regalia of Aslona, two ancient, masterwork katanas,\n"
                "Asamarum and E-numa sa-am.\""));
      game::TextScreen(
        CONST_S("\"I have managed to take Asamarum with me during Lord Regent's coup, but he still\n"
                "holds E-numa sa-am. One last time, I would ask you to steal into the Castle of Aslona\n"
                "and retrieve the sword from Lord Regent, so that we might claim victory without\n"
                "bloodshed.\""));
      GetArea()->SendNewDrawRequest();
      PLAYER->GetTeam()->Hostility(game::GetTeam(ASLONA_TEAM)); // Too easy otherwise.
      ADD_MESSAGE("%s hugs you tightly. \"Godspeed, my friend.\"", CHAR_NAME(DEFINITE));
      game::SetStoryState(3);
      return true;
    }

    {
      // Does the player have prince Artorius in his team?
      character *CrownPrince = 0;
      for (character *p : game::GetTeam(PLAYER_TEAM)->GetMember()) {
        if (p->IsEnabled() && !p->IsPlayer() && p->IsKing()) {
          CrownPrince = p;
        }
      }

      if (CrownPrince) {
        if (game::TruthQuestion(CONST_S("Entrust young prince to Harvan's care?"), REQUIRES_ANSWER)) {
          team *Team = game::GetTeam(REBEL_TEAM);
          CrownPrince->ChangeTeam(Team);
          ADD_MESSAGE("\"Hi, uncle Harvan! Where are we? When are we gonna go home?\"");
          ADD_MESSAGE("\"Your Highness, I'm so very glad to see you. Don't worry, I will take you home soon.\"");
          game::SetRebelStoryState(game::GetRebelStoryState() + 1);
          return true;
        }
      }
    }
  }

  return false;
}


truth harvan::SpecialEnemySightedReaction (character *Char) {
  if (!Char->IsPlayer() || GetPos().IsAdjacent(Char->GetPos())) {
    return false;
  }

  if (GetHP() > (GetMaxHP() / 2) && !RAND_N(10)) {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s screams at you: \"Get over here!\"", CHAR_NAME(DEFINITE));
    } else {
      ADD_MESSAGE("\"Get over here!\"");
    }
    Char->TeleportNear(this);
  }

  return false;
}

#endif
