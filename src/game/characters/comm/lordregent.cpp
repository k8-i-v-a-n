#ifdef HEADER_PHASE
CHARACTER(lordregent, humanoid) {
public:
  virtual truth TryQuestTalks () override;

protected:
  virtual void GetAICommand () override;
  virtual void SpecialBodyPartSeverReaction () override;
};


#else


void lordregent::GetAICommand () {
  StandIdleAI();
}


truth lordregent::TryQuestTalks () {
  if (GetRelation(PLAYER) != HOSTILE) {
    if (game::GetAslonaStoryState() == 1) {
      game::TextScreen(CONST_S(
        "\"Sir Lancelyn sent you? He thought you can be of any use to me? I doubt that,\n"
        "but then again maybe there is something in you, we shall see.\"\n\n"
        "\"Several of my advisors have been complaining lately that we don't have enough\n"
        "expendable personnel to send on special missions. Go see Lord Mittrars,\n"
        "Myrddin the wizard and Senex of Seges, and report back once they are happy.\"\n\n"
        "\"Now away with you, I have other things to worry about.\""));
      // reveal rebel camp here, why not.
      // the camp is still on the map, so the player may find it by himself.
      game::RevealPOI(game::rebelCampPOI());
      GetArea()->SendNewDrawRequest();
      ADD_MESSAGE("%s sighs: \"I don't have time for this.\"", CHAR_NAME(DEFINITE));
      game::SetAslonaStoryState(2);
      return true;
    } else if (PLAYER->HasMuramasa()) {
      if (game::TruthQuestion(CONST_S("Turn in the wicked katana named Asa'marum?"), REQUIRES_ANSWER)) {
        //game::PlayVictoryMusic();
        game::TextScreen(CONST_S(
          "You hand over the ancient katana and thus the regalia necessary to crown a new\n"
          "king of Aslona are together again.\n\n"
          "\"I am in your debt,\" Lord Peredivall says and then turns to his army. \"Citizens,\n"
          "this is our hour of victory. You have fought well. You are heroes, all of you.\n"
          "Our country and the very soul of our nation was saved only thanks to you, thanks\n"
          "to your bravery, honor, devotion and determination. But now, the war is over.\n"
          "The traitors shall face justice and peace will return to our homes. Gods bless Aslona!\"\n\n"
          "A deafening cheer echoes his words.\n\nYou are victorious!"));
        game::GetCurrentArea()->SendNewDrawRequest();
        game::DrawEverything();
        // Did the player do all quests just for the royalists?
        festring Msg;
        if (game::GetAslonaStoryState() == 5) {
          Msg = CONST_S("helped the royalists to an overwhelming victory");
          PLAYER->AddPolymorphedText(Msg, "while");
          PLAYER->AddScoreEntry(Msg, 4, false);
        } else {
          Msg = CONST_S("helped the royalists to an uneasy victory");
          PLAYER->AddPolymorphedText(Msg, "while");
          PLAYER->AddScoreEntry(Msg, 2, false);
        }
        PLAYER->ShowAdventureInfo(Msg);
        game::End(Msg);
        return true;
      }
    } else if (game::GetAslonaStoryState() == 5 && game::GetStoryState() < 3) {
      game::TextScreen(
        CONST_S("\"Ah, you are back? I guess I owe you an apology. I expected you to try and leverage\n"
                "some money from us, and then scram. But you really showed what you are made of!\n"
                "I hope you could lend your services to Aslona one last time.\"\n\n"
                "\"This civil war needs to end right now, before our kingdom tears itself apart, but\n"
                "I don't like the options I see. The scouts of Lord Mittrars finally discovered\n"
                "the location of Harvan's headquarters, but attacking them head-on in a difficult\n"
                "terrain would be foolish and costly. Myrddin informed me he has a magical item\n"
                "that could destroy the rebels once and for all, but I'm hesitant to condemn\n"
                "every single one of those misguided souls to death.\""));
      game::TextScreen(
        CONST_S("\"Yet there is a third option. It is my sincere belief that many of Harvan's troops\n"
                "would change sides if prince Artorius took the throne of his father as the rightful\n"
                "king of Aslona. But for His Highness to be crowned the king without any doubts or\n"
                "dispute, we need the regalia of Aslona, two ancient katanas of immaculate craft,\n"
                "Asamarum and E-numa sa-am.\"\n\n"
                "\"E-numa sa-am I already have, but Harvan absconded with Asamarum when he fled after\n"
                "the old king's death. And where a direct assault would be hard-pressed for victory,\n"
                "I think you as an outlander could slip through the sentries of the rebels' camp\n"
                "unaccosted and steal Asamarum from Harvan.\"\n\n"
                "\"A stealth mission, if you will.\""));
      GetArea()->SendNewDrawRequest();
      PLAYER->GetTeam()->Hostility(game::GetTeam(REBEL_TEAM)); // So much for a stealth mission. ;)
      ADD_MESSAGE("%s smiles at you: \"Together, we'll bring Harvan to justice.\"", CHAR_NAME(DEFINITE));
      game::SetStoryState(3);
      return true;
    } else {
      // Does the player have prince Artorius in his team?
      character *CrownPrince = 0;
      for (character *p : game::GetTeam(PLAYER_TEAM)->GetMember()) {
        if (p->IsEnabled() && !p->IsPlayer() && p->IsKing()) {
          CrownPrince = p;
        }
      }

      if (CrownPrince) {
        ADD_MESSAGE("%s bows his head slightly. \"Your Highness, it is excellent to see you safe and sound. Please, hurry to tell Lord Mittrars about your return. He was truly sick with worry.\"", CHAR_NAME(DEFINITE));
        return true;
      }

      if (PLAYER->HasNuke() || PLAYER->HasWeepObsidian()) {
        ADD_MESSAGE("\"Ah, seems like you were not idle. I'm sure my advisors will be thrilled.\"");
        return true;
      }
    }
  }

  return false;
}


void lordregent::SpecialBodyPartSeverReaction () {
  struct distancepair {
    distancepair(sLong aDistance, character *aChar) : Distance(aDistance), Char(aChar) { }
    bool operator<(const distancepair& D) const { return Distance > D.Distance; }
    sLong Distance;
    character *Char;
  };

  if (HasHead()) {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s prays to Seges. You feel the sudden presence of enemies.", CHAR_NAME(DEFINITE));
    }

    // Summons allies and then teleports away.
    std::vector<distancepair> ToSort;
    v2 Pos = GetPos();

    for (character *p : GetTeam()->GetMember()) {
      if (p->IsEnabled() && p != this) {
        ToSort.push_back(distancepair((Pos - p->GetPos()).GetLengthSquare(), p));
      }
    }

    if (ToSort.size() > 5) {
      std::sort(ToSort.begin(), ToSort.end());
    }

    for (uInt c = 0; c < 5 && c < ToSort.size(); ++c) {
      ToSort[c].Char->TeleportNear(this);
    }

    /* Teleport away, but rather than doing it the simple way, we're going to use
     * a teleport beam to also remove any items on the square. In effect, if the
     * player cuts off Lord Efra's sword arm, he will teleport and so will
     * Masamune, rather than for it to stay lying on the ground.
     */
    beamdata Beam (
      this,
      CONST_S("killed by the fickle favor of Seges"),
      YOURSELF,
      0
    );
    GetLSquareUnder()->Teleport(Beam);
    //TeleportRandomly(true);
  }
}


#endif
