#ifdef HEADER_PHASE
// this seems to be something unfinished in comm. fork
CHARACTER(nihil, humanoid) {
public:
  virtual truth BodyPartIsVital (int) const override;
  virtual truth CanCreateBodyPart (int) const override;
  virtual int GetAttribute (int, truth = true) const override;
  virtual col24 GetBaseEmitation () const override;
  virtual cfestring &GetStandVerb () const override;
};


#else


col24 nihil::GetBaseEmitation () const { return MakeRGB24(150, 110, 110); }
cfestring &nihil::GetStandVerb() const { return character::GetStandVerb(); }
truth nihil::BodyPartIsVital (int I) const { return I == TORSO_INDEX || I == HEAD_INDEX; }


int nihil::GetAttribute (int Identifier, truth AllowBonus) const {
  if (Identifier == LEG_STRENGTH) return GetDefaultLegStrength();
  if (Identifier == AGILITY) return GetDefaultAgility();
  return humanoid::GetAttribute(Identifier, AllowBonus);
}


truth nihil::CanCreateBodyPart (int I) const {
  return (I == TORSO_INDEX || I == HEAD_INDEX || I == RIGHT_ARM_INDEX || I == LEFT_ARM_INDEX);
}


#endif
