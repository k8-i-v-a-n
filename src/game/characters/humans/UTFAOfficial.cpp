#ifdef HEADER_PHASE
CHARACTER(UTFAOfficial, humanoid)
{
public:
  virtual void GetAICommand () override;
};


#else


void UTFAOfficial::GetAICommand () {
  StandIdleAI();
}


#endif
