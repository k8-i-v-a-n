#ifdef HEADER_PHASE
CHARACTER(assassin, humanoid)
{
public:
  virtual truth TryQuestTalks () override;
};


#else


truth assassin::TryQuestTalks () {
  if (GetRelation(PLAYER) == HOSTILE) {
    if (PLAYER->GetMoney() >= 1500) {
      festring msg;
      msg << CHAR_DESCRIPTION(DEFINITE);
      msg << " talks: \"If you shell out \1Y1500\2 gold pieces I'll join your side.\"";
      ADD_MESSAGE("%s", msg.CStr());
      if (game::TruthQuestion(msg + "\nDo you want to bribe him?")) {
        PLAYER->SetMoney(PLAYER->GetMoney() - 1500);
        ChangeTeam(PLAYER->GetTeam());
        RemoveHomeData();
      }
    } else {
      ADD_MESSAGE("\"Trying to reason me with diplomancy won't work on me.\"");
    }
    return true;
  }
  return false;
}


#endif
