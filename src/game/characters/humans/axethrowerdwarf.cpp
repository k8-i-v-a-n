#ifdef HEADER_PHASE
CHARACTER(axethrowerdwarf, humanoid)
{
public:
  virtual void GetAICommand () override;
  virtual void CreateInitialEquipment (int) override;

protected:
  virtual col16 GetTorsoMainColor () const override;
  virtual col16 GetGauntletColor () const override;
  virtual col16 GetLegMainColor () const override;
  virtual v2 GetDrawDisplacement (int) const;
  virtual int GetWSkillHits () const;
  virtual truth IsElite () const;
};


#else


truth axethrowerdwarf::IsElite () const { return false; }

int axethrowerdwarf::GetWSkillHits () const { return 10000; }
col16 axethrowerdwarf::GetTorsoMainColor () const { return GetMasterGod()->GetColor(); }
col16 axethrowerdwarf::GetGauntletColor () const { return GetMasterGod()->GetColor(); }
col16 axethrowerdwarf::GetLegMainColor () const { return GetMasterGod()->GetColor(); }


void axethrowerdwarf::CreateInitialEquipment (int SpecialFlags) {
  SetRightWielded(meleeweapon::Spawn(AXE, SpecialFlags));
  SetLeftWielded(meleeweapon::Spawn(AXE, SpecialFlags));
  for (int k = 0; k < 6; ++k) GetStack()->AddItem(meleeweapon::Spawn(AXE));
  GetStack()->AddItem(lantern::Spawn());
  GetCWeaponSkill(AXES)->AddHit(GetWSkillHits());
  GetCurrentRightSWeaponSkill()->AddHit(GetWSkillHits());
}


void axethrowerdwarf::GetAICommand () {
  if (CheckThrowItemOpportunity()) return;
  humanoid::GetAICommand();
}


// the argument is body part index
v2 axethrowerdwarf::GetDrawDisplacement (int I) const {
  const v2 DrawDisplacement[] = {
    v2(0, 0),
    v2(0, 1),
    v2(0, -1),
    v2(0, -1),
    v2(0, -1),
    v2(0, 0),
    v2(0, 0),
  };
  return DrawDisplacement[I];
}


#endif
