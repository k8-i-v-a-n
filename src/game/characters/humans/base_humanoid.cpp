#ifdef HEADER_PHASE
CHARACTER(humanoid, character)
{
public:
  friend class rightarm;
  friend class leftarm;

public:
  humanoid () : CurrentRightSWeaponSkill(0), CurrentLeftSWeaponSkill(0) { }
  humanoid (const humanoid &);
  virtual ~humanoid ();

  virtual truth CanWield () const override;
  virtual truth Hit (character *, v2, int, int = 0) override;
  virtual int GetSize () const override;

  head *GetHead () const { return static_cast<head*>(*BodyPartSlot[HEAD_INDEX]); }
  arm *GetRightArm () const { return static_cast<arm*>(*BodyPartSlot[RIGHT_ARM_INDEX]); }
  arm *GetLeftArm () const { return static_cast<arm*>(*BodyPartSlot[LEFT_ARM_INDEX]); }
  groin *GetGroin () const { return static_cast<groin*>(*BodyPartSlot[GROIN_INDEX]); }
  leg *GetRightLeg () const { return static_cast<leg*>(*BodyPartSlot[RIGHT_LEG_INDEX]); }
  leg *GetLeftLeg () const { return static_cast<leg*>(*BodyPartSlot[LEFT_LEG_INDEX]); }
  item *GetHelmet () const { return GetHead() ? GetHead()->GetHelmet() : 0; }
  item *GetAmulet () const { return GetHead() ? GetHead()->GetAmulet() : 0; }
  item *GetCloak () const { return GetHumanoidTorso() ? GetHumanoidTorso()->GetCloak() : 0; }
  item *GetBodyArmor () const { return GetHumanoidTorso() ? GetHumanoidTorso()->GetBodyArmor() : 0; }
  item *GetBelt () const { return GetHumanoidTorso() ? GetHumanoidTorso()->GetBelt() : 0; }
  item *GetRightWielded () const { return GetRightArm() ? GetRightArm()->GetWielded() : 0; }
  item *GetLeftWielded () const { return GetLeftArm() ? GetLeftArm()->GetWielded() : 0; }
  item *GetRightRing () const { return GetRightArm() ? GetRightArm()->GetRing() : 0; }
  item *GetLeftRing () const { return GetLeftArm() ? GetLeftArm()->GetRing() : 0; }
  item *GetRightGauntlet () const { return GetRightArm() ? GetRightArm()->GetGauntlet() : 0; }
  item *GetLeftGauntlet () const { return GetLeftArm() ? GetLeftArm()->GetGauntlet() : 0; }
  item *GetRightBoot () const { return GetRightLeg() ? GetRightLeg()->GetBoot() : 0; }
  item *GetLeftBoot () const { return GetLeftLeg() ? GetLeftLeg()->GetBoot() : 0; }
  void SetHelmet (item *What) { GetHead()->SetHelmet(What); }
  void SetAmulet (item *What) { GetHead()->SetAmulet(What); }
  void SetCloak (item *What) { GetHumanoidTorso()->SetCloak(What); }
  void SetBodyArmor (item *What) { GetHumanoidTorso()->SetBodyArmor(What); }
  void SetBelt (item *What) { GetHumanoidTorso()->SetBelt(What); }
  void SetRightWielded (item *What) { GetRightArm()->SetWielded(What); }
  void SetLeftWielded (item *What) { GetLeftArm()->SetWielded(What); }
  void SetRightRing (item *What) { GetRightArm()->SetRing(What); }
  void SetLeftRing (item *What) { GetLeftArm()->SetRing(What); }
  void SetRightGauntlet (item *What) { GetRightArm()->SetGauntlet(What); }
  void SetLeftGauntlet (item *What) { GetLeftArm()->SetGauntlet(What); }
  void SetRightBoot (item *What) { GetRightLeg()->SetBoot(What); }
  void SetLeftBoot (item *What) { GetLeftLeg()->SetBoot(What); }
  arm *GetMainArm () const;
  arm *GetSecondaryArm () const;

  virtual truth ReceiveDamage (character*, int, int, int = ALL, int = 8, truth = false, truth = false, truth = false, truth = true) override;
  virtual truth BodyPartIsVital (int) const override;
  virtual truth BodyPartCanBeSevered (int) const override;
  virtual item *GetMainWielded () const override;
  virtual item *GetSecondaryWielded () const override;
  virtual cchar *GetEquipmentName (int) const override;
  virtual bodypart *GetBodyPartOfEquipment (int) const override;
  virtual item *GetEquipment (int) const override;
  virtual int GetEquipments () const override;
  virtual void SwitchToDig (item *, v2) override;
  virtual void SwitchToButchery (item *DigItem, item *Corpse) override;
  virtual int GetUsableLegs () const;
  virtual int GetUsableArms () const override;
  virtual truth CheckKick () const override;
  virtual int OpenMultiplier () const override;
  virtual int CloseMultiplier () const override;
  virtual truth CheckThrow () const override;
  virtual truth CheckThrowItemOpportunity () override;
  virtual truth CheckAIZapOpportunity () override;
  virtual truth CheckOffer () const override;
  virtual sorter EquipmentSorter (int) const override;
  virtual void SetEquipment (int, item *) override;
  virtual void DrawSilhouette (truth) const;
  virtual int GetGlobalResistance (int) const override;
  virtual truth TryToRiseFromTheDead () override;
  virtual truth HandleNoBodyPart (int);
  virtual void Kick (lsquare *, int, truth=false) override;
  virtual double GetTimeToKill (ccharacter *, truth) const override;
  virtual int GetAttribute (int, truth=true) const override;
  virtual truth EditAttribute (int, int) override;
  virtual void EditExperience (int, double, double) override;
  virtual int DrawStats (truth) const override;
  virtual void Bite (character *, v2, int, truth=false);
  virtual int GetCarryingStrength () const override;
  virtual int GetRandomStepperBodyPart () const override;
  virtual int CheckForBlock (character *, item *, double, int, int, int) override;
  virtual truth AddSpecialSkillInfo (felist &List, int maxNameWdt, int maxLevelWdt,
                                     int maxPointsWdt, int maxNeededWdt) const override;
  virtual truth CheckBalance (double) override;
  virtual sLong GetMoveAPRequirement (int) const override;
  virtual v2 GetEquipmentPanelPos (int) const;
  virtual truth EquipmentEasilyRecognized (int) const override;

  sweaponskill *GetCurrentRightSWeaponSkill () const { return CurrentRightSWeaponSkill; }
  void SetCurrentRightSWeaponSkill (sweaponskill *What) { CurrentRightSWeaponSkill = What; }
  sweaponskill *GetCurrentLeftSWeaponSkill () const { return CurrentLeftSWeaponSkill; }
  void SetCurrentLeftSWeaponSkill (sweaponskill *What) { CurrentLeftSWeaponSkill = What; }

  virtual void SWeaponSkillTick () override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void SignalEquipmentAdd (int) override;
  virtual void SignalEquipmentRemoval (int, citem *) override;
  virtual void DrawBodyParts (blitdata &BlitData, col16 MonoColor=TRANSPARENT_COLOR) const override;
  virtual truth CanUseStethoscope (truth) const override;
  virtual truth IsUsingArms () const override;
  virtual truth IsUsingLegs () const override;
  virtual truth IsUsingHead () const override;
  virtual void CalculateBattleInfo () override;
  virtual void CalculateBodyParts () override;
  virtual void CalculateAllowedWeaponSkillCategories () override;
  virtual truth HasALeg () const override;
  virtual void AddSpecialEquipmentInfo (festring &, int) const override;
  virtual void CreateInitialEquipment (int) override;
  virtual festring GetBodyPartName (int, truth=false) const override;
  virtual void CreateBlockPossibilityVector (blockvector &, double) const override;
  virtual item *SevereBodyPart (int, truth=false, stack * = 0) override;
  virtual int GetSWeaponSkillLevel (citem *) const override;
  virtual truth UseMaterialAttributes () const override;
  virtual void CalculateDodgeValue () override;
  virtual truth CheckZap () override;
  virtual truth IsHumanoid () const override;
  virtual truth CheckTalk ();
  virtual int GetWeaponTooHeavyRate (int I) const override;
  virtual truth CheckIfEquipmentIsNotUsable (int I) const override;
  virtual void AddSpecialStethoscopeInfo (felist &) const override;
  virtual item *GetPairEquipment (int) const override;
  virtual truth HasHead () const override;
  virtual cfestring &GetStandVerb () const override;
  virtual head *Behead () override;
  virtual void AddAttributeInfo (festring &) const override;
  virtual void AddAttackInfo (felist &) const override;
  virtual void AddDefenceInfo (felist &) const override;
  virtual void DetachBodyPart () override;
  virtual int GetRandomApplyBodyPart () const override;

  void EnsureCurrentSWeaponSkillIsCorrect (sweaponskill*&, citem*);

  virtual int GetSumOfAttributes () const override;
  virtual truth CheckConsume (cfestring&) const override;
  virtual truth CanConsume (material*) const override;
  virtual truth PreProcessForBone () override;
  virtual void FinalProcessForBone () override;
  virtual void StayOn (liquid*) override;
  virtual head *GetVirtualHead () const override;
  virtual character *CreateZombie () const override;
  virtual void LeprosyHandler () override;
  virtual void DropRandomNonVitalBodypart ();
  virtual void DropBodyPart (int);
  virtual void DuplicateEquipment (character*, feuLong) override;
  virtual int GetAttributeAverage () const override;
  virtual truth CanVomit () const override;
  virtual truth CheckApply () const override;
  virtual truth CanForceVomit () const override;
  virtual truth IsTransparent () const override;
  virtual void ModifySituationDanger (double&) const override;
  virtual int RandomizeTryToUnStickBodyPart (feuLong) const override;
  virtual truth AllowUnconsciousness () const override;
  virtual truth CanChokeOnWeb (web*) const override;
  virtual truth BrainsHurt () const override;
  virtual cchar *GetRunDescriptionLine (int) const override;
  virtual cchar *GetNormalDeathMessage () const override;
  virtual void ApplySpecialAttributeBonuses () override;
  virtual truth MindWormCanPenetrateSkull (mindworm *) const override;
  truth HasSadistWeapon () const;
  virtual truth HasSadistAttackMode () const override;
  virtual void SurgicallyDetachBodyPart () override;
  virtual truth SpecialBiteEffect (character*, v2, int, int, truth, truth Critical, int DoneDamage) override;

protected:
  virtual v2 GetBodyPartBitmapPos (int, truth = false) const override;
  virtual col16 GetBodyPartColorB (int, truth = false) const override;
  virtual col16 GetBodyPartColorC (int, truth = false) const override;
  virtual col16 GetBodyPartColorD (int, truth = false) const override;
  virtual int GetBodyPartSparkleFlags (int) const override;
  virtual sLong GetBodyPartSize (int, int) const override;
  virtual sLong GetBodyPartVolume (int) const override;
  virtual bodypart *MakeBodyPart (int) const override;
  virtual cfestring &GetDeathMessage () const override;
  virtual v2 GetDrawDisplacement (int) const;

protected:
  truth HasAUsableArm () const;
  truth HasAUsableLeg () const;
  truth HasTwoUsableLegs () const;
  truth CanAttackWithAnArm () const;
  truth RightArmIsUsable () const;
  truth LeftArmIsUsable () const;
  truth RightLegIsUsable () const;
  truth LeftLegIsUsable () const;

  truth TryToHardenItem (itemvector &Item);

protected:
  std::list<sweaponskill *> SWeaponSkill;
  sweaponskill *CurrentRightSWeaponSkill;
  sweaponskill *CurrentLeftSWeaponSkill;
  static const cint DrawOrder[];
};


#else


const cint humanoid::DrawOrder[] = {
  TORSO_INDEX,
  GROIN_INDEX,
  RIGHT_LEG_INDEX,
  LEFT_LEG_INDEX,
  RIGHT_ARM_INDEX,
  LEFT_ARM_INDEX,
  HEAD_INDEX
};


//==========================================================================
//
//  humanoid::humanoid
//
//==========================================================================
humanoid::humanoid (const humanoid &Humanoid) :
  mybase(Humanoid),
  CurrentRightSWeaponSkill(0),
  CurrentLeftSWeaponSkill(0)
{
  SWeaponSkill.resize(Humanoid.SWeaponSkill.size());
  std::list<sweaponskill*>::iterator i1 = SWeaponSkill.begin();
  std::list<sweaponskill*>::const_iterator i2 = Humanoid.SWeaponSkill.begin();
  for (; i1 != SWeaponSkill.end(); ++i1, ++i2) *i1 = new sweaponskill(**i2);
}


//==========================================================================
//
//  humanoid::~humanoid
//
//==========================================================================
humanoid::~humanoid () {
  for (std::list<sweaponskill*>::iterator i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) {
    delete *i;
  }
}


truth humanoid::IsHumanoid () const { return true; }

int humanoid::GetEquipments () const { return 13; }
truth humanoid::BodyPartIsVital (int I) const { return I == TORSO_INDEX || I == HEAD_INDEX || I == GROIN_INDEX; }
truth humanoid::BodyPartCanBeSevered (int I) const { return I != TORSO_INDEX && I != GROIN_INDEX; }
int humanoid::OpenMultiplier () const { return HasAUsableArm() ? 1 : 3; }
int humanoid::CloseMultiplier () const { return HasAUsableArm() ? 1 : 2; }
int humanoid::GetCarryingStrength () const { return Max(GetAttribute(LEG_STRENGTH), 1)+CarryingBonus; }
void humanoid::CalculateBodyParts () { BodyParts = HUMANOID_BODYPARTS; }
void humanoid::CalculateAllowedWeaponSkillCategories () { AllowedWeaponSkillCategories = WEAPON_SKILL_CATEGORIES; }

truth humanoid::HasALeg () const { return HasAUsableLeg(); }
truth humanoid::HasHead () const { return !!GetHead(); }
head *humanoid::GetVirtualHead () const { return GetHead(); }
truth humanoid::CanForceVomit () const { return TorsoIsAlive() && HasAUsableArm(); }

v2 humanoid::GetDrawDisplacement (int) const { return ZERO_V2; }


//==========================================================================
//
//  humanoid::Save
//
//==========================================================================
void humanoid::Save (outputfile &SaveFile) const {
  character::Save(SaveFile);
  SaveFile << SWeaponSkill;
}


//==========================================================================
//
//  humanoid::Load
//
//==========================================================================
void humanoid::Load (inputfile &SaveFile) {
  character::Load(SaveFile);
  SaveFile >> SWeaponSkill;
  if (GetRightWielded()) {
    for (std::list<sweaponskill*>::iterator i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) {
      if ((*i)->IsSkillOf(GetRightWielded())) {
        SetCurrentRightSWeaponSkill(*i);
        break;
      }
    }
  }
  if (GetLeftWielded()) {
    for (std::list<sweaponskill*>::iterator i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) {
      if ((*i)->IsSkillOf(GetLeftWielded())) {
        SetCurrentLeftSWeaponSkill(*i);
        break;
      }
    }
  }
}


//==========================================================================
//
//  humanoid::DrawSilhouette
//
//==========================================================================
void humanoid::DrawSilhouette (truth AnimationDraw) const {
  int c;
  blitdata B1 = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { 0, 0 },
    { TILE_SIZE, TILE_SIZE },
    { (int)ivanconfig::GetContrastLuminance() }, //k8:UB, i don't fuckin' care
    TRANSPARENT_COLOR,
    ALLOW_ANIMATE
  };

  v2 Where;
  if (ivanconfig::GetStatusOnLeft()) {
    Where = v2(18, 53);
  } else {
    Where = v2(RES.X-SILHOUETTE_SIZE.X-39, 53);
  }
  if (ivanconfig::GetStatusOnBottom()) {
    Where = v2(Where.X, Where.Y + game::GetVertDollOfs());
  }

  cint Equipments = GetEquipments();
  if (CanUseEquipment()) {
    for (c = 0; c != Equipments; c += 1) {
      if (GetBodyPartOfEquipment(c) && EquipmentIsAllowed(c)) {
        v2 Pos = Where + GetEquipmentPanelPos(c);
        if (!AnimationDraw) {
          DOUBLE_BUFFER->DrawRectangle(Pos + v2(-1, -1), Pos + TILE_V2, DARK_GRAY);
        }
        item *Equipment = GetEquipment(c);
        if (Equipment && (!AnimationDraw || Equipment->IsAnimated())) {
          igraph::BlitBackGround(Pos, TILE_V2);
          if (c == RIGHT_WIELDED_INDEX || c == LEFT_WIELDED_INDEX) {
            const int rate = GetWeaponTooHeavyRate(c);
            col16 back = TRANSPARENT_COLOR;
            switch (rate) {
              case WEAPON_SOMEWHAT_DIFFICULT: back = MakeRGB16(0x30, 0x20, 0x00); break;
              case WEAPON_MUCH_TROUBLE: back = MakeRGB16(0x70, 0x00, 0x00); break;
              case WEAPON_EXTREMLY_DIFFICULT: back = MakeRGB16(0xa0, 0x00, 0x00); break;
              case WEAPON_UNUSABLE: back = MakeRGB16(0xd0, 0x00, 0x00); break;
            }
            if (back != TRANSPARENT_COLOR) {
              DOUBLE_BUFFER->Fill(Pos, TILE_V2, back);
            }
          }
          B1.Dest = Pos;
          if (Equipment->AllowAlphaEverywhere()) {
            B1.CustomData |= ALLOW_ALPHA;
          }
          Equipment->Draw(B1);
          B1.CustomData &= ~ALLOW_ALPHA;
        }
      }
    }
  }

  if (!AnimationDraw) {
    blitdata B2 = {
      DOUBLE_BUFFER,
      { 0, 0 },
      { Where.X+8, Where.Y },
      { SILHOUETTE_SIZE.X, SILHOUETTE_SIZE.Y },
      { 0 },
      0,
      0
    };
    for (int c = 0; c < BodyParts; ++c) {
      bodypart *BodyPart = GetBodyPart(c);
      if (BodyPart) {
        int Type = (BodyPart->IsUsable() ? SILHOUETTE_NORMAL : SILHOUETTE_INTER_LACED);
        bitmap *Cache = igraph::GetSilhouetteCache(c, BodyPart->GetConditionColorIndex(), Type);
        Cache->NormalMaskedBlit(B2);
        BodyPart->DrawScars(B2);
      }
    }
  }
}


//==========================================================================
//
//  humanoid::DrawStats
//
//==========================================================================
int humanoid::DrawStats (truth AnimationDraw) const {
  DrawSilhouette(AnimationDraw);
  if (AnimationDraw) return 15;
  int PanelPosX;
  if (ivanconfig::GetStatusOnLeft()) {
    PanelPosX = game::GetLeftStatsPos();
  } else {
    PanelPosX = RES.X-96;
  }
  const int lineHeight = 10;
  int PanelPosY = 15;
  if (ivanconfig::GetStatusOnBottom()) {
    PanelPosY += game::GetVertStatOfs();
  }
  PanelPosY *= lineHeight;
  PrintAttribute("AStr", ARM_STRENGTH, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  PrintAttribute("LStr", LEG_STRENGTH, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  PrintAttribute("Dex", DEXTERITY, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  PrintAttribute("Agi", AGILITY, PanelPosX, PanelPosY); PanelPosY += lineHeight;
  return PanelPosY;
}


//==========================================================================
//
//  humanoid::GetMainWielded
//
//==========================================================================
item *humanoid::GetMainWielded () const {
  if (GetMainArm() && GetMainArm()->GetWielded()) return GetMainArm()->GetWielded();
  if (GetSecondaryArm()) return GetSecondaryArm()->GetWielded();
  return 0;
}


//==========================================================================
//
//  humanoid::GetSecondaryWielded
//
//==========================================================================
item *humanoid::GetSecondaryWielded () const {
  if (GetMainArm() && GetMainArm()->GetWielded() && GetSecondaryArm()) return GetSecondaryArm()->GetWielded();
  return 0;
}


//==========================================================================
//
//  humanoid::Hit
//
//==========================================================================
truth humanoid::Hit (character *Enemy, v2 HitPos, int Direction, int Flags) {
  if (CheckIfTooScaredToHit(Enemy)) return false;

  if (IsPlayer()) {
    if (!(Enemy->IsMasochist() && GetRelation(Enemy) == FRIEND) && GetRelation(Enemy) != HOSTILE &&
        !game::TruthQuestion(CONST_S("This might cause a hostile reaction. Are you sure?")))
    {
      return false;
    }
  } else if (GetAttribute(WISDOM) >= Enemy->GetAttackWisdomLimit()) {
    return false;
  }

  if (GetBurdenState() == OVER_LOADED) {
    if (IsPlayer()) ADD_MESSAGE("You cannot fight while carrying so much.");
    return false;
  }

  int c, AttackStyles;
  for (c = 0, AttackStyles = 0; c < 8; ++c) {
    if (GetAttackStyle()&(1<<c)) ++AttackStyles;
  }

  int Chosen = RAND_N(AttackStyles);
  for (c = 0, AttackStyles = 0; c < 8; ++c) {
    if (GetAttackStyle()&(1<<c) && AttackStyles++ == Chosen) {
      Chosen = 1<<c;
      break;
    }
  }
  if (StateIsActivated(VAMPIRISM) && !RAND_2) Chosen = USE_HEAD;

  if (Chosen == USE_ARMS) {
    if (CanAttackWithAnArm() && (!(Flags & SADIST_HIT) || HasSadistWeapon())) {
      msgsystem::EnterBigMessageMode();
      Hostility(Enemy);
      sLong FirstAPCost = 0, SecondAPCost = 0;
      arm *FirstArm;
      arm *SecondArm;
      if (RAND_2) {
        FirstArm = GetRightArm();
        SecondArm = GetLeftArm();
      } else {
        FirstArm = GetLeftArm();
        SecondArm = GetRightArm();
      }

      // if we have some weapon that is "good with", use it first.
      // FIXME: this might not be the better choice if our second weapon is powerful.
      if (SecondArm && SecondArm->GetWielded()) {
        // maybe use second arm first?
        if (Enemy->IsPlant() && SecondArm->GetWielded()->IsGoodWithPlants()) {
          if (!FirstArm || !FirstArm->GetWielded() ||
              !FirstArm->GetWielded()->IsGoodWithPlants())
          {
            if (RAND_4) {
              Swap(FirstArm, SecondArm);
            }
          }
        } else if (Enemy->IsUndead() && SecondArm->GetWielded()->IsGoodWithUndead()) {
          if (!FirstArm || !FirstArm->GetWielded() ||
              !FirstArm->GetWielded()->IsGoodWithUndead())
          {
            if (RAND_4) {
              Swap(FirstArm, SecondArm);
            }
          }
        }
      }

      int Strength = Max(GetAttribute(ARM_STRENGTH), 1);
      if (FirstArm && FirstArm->GetDamage() &&
          (!(Flags&SADIST_HIT) || FirstArm->HasSadistWeapon()))
      {
        FirstAPCost = FirstArm->GetAPCost();
        FirstArm->Hit(Enemy, HitPos, Direction, Flags);
        if (StateIsActivated(LEPROSY) && !RAND_N(25*GetAttribute(ENDURANCE))) {
          DropBodyPart(FirstArm->GetBodyPartIndex());
        }
      }

      if (!GetAction() && IsEnabled() && Enemy->IsEnabled() &&
          SecondArm && SecondArm->GetDamage() &&
          (!(Flags & SADIST_HIT) || SecondArm->HasSadistWeapon()))
      {
        SecondAPCost = SecondArm->GetAPCost();
        SecondArm->Hit(Enemy, HitPos, Direction, Flags);
        if (StateIsActivated(LEPROSY) && !RAND_N(25*GetAttribute(ENDURANCE))) {
          DropBodyPart(SecondArm->GetBodyPartIndex());
        }
      }

      EditNP(-50);
      EditAP(-Max(FirstAPCost, SecondAPCost));
      EditStamina(-10000/Strength, false);
      msgsystem::LeaveBigMessageMode();
      return true;
    }
    Chosen = USE_LEGS; // try next
  }

  if (Chosen == USE_LEGS) {
    if (HasTwoUsableLegs()) {
      msgsystem::EnterBigMessageMode();
      Hostility(Enemy);
      Kick(GetNearLSquare(HitPos), Direction, Flags&SADIST_HIT);
      if (StateIsActivated(LEPROSY) && !RAND_N(25*GetAttribute(ENDURANCE))) {
        DropBodyPart(RAND_2 ? RIGHT_LEG_INDEX : LEFT_LEG_INDEX);
      }
      msgsystem::LeaveBigMessageMode();
      return true;
    }
    Chosen = USE_HEAD; // try next
  }

  if (Chosen == USE_HEAD) {
    if (GetHead()) {
      msgsystem::EnterBigMessageMode();
      Hostility(Enemy);
      Bite(Enemy, HitPos, Direction, Flags&SADIST_HIT);
      msgsystem::LeaveBigMessageMode();
      return true;
    }
  }

  if (IsPlayer()) {
    ADD_MESSAGE("You are currently quite unable to damage anything.");
  }
  return false;
}


//==========================================================================
//
//  humanoid::AddSpecialSkillInfo
//
//==========================================================================
truth humanoid::AddSpecialSkillInfo (felist &List, int maxNameWdt, int maxLevelWdt,
                                     int maxPointsWdt, int maxNeededWdt) const
{
  truth Something = false;
  if (CurrentRightSWeaponSkill && CurrentRightSWeaponSkill->GetHits()/100) {
    List.AddEntry(CONST_S(""), LIGHT_GRAY);
    festring Buffer = CONST_S("right accustomization");
    //Buffer.Resize(30);
    FONT->RPadToPixWidth(Buffer, maxNameWdt);
    Buffer << CurrentRightSWeaponSkill->GetLevel();
    //Buffer.Resize(40);
    FONT->RPadToPixWidth(Buffer, maxLevelWdt);
    Buffer << CurrentRightSWeaponSkill->GetHits()/100;
    //Buffer.Resize(50);
    FONT->RPadToPixWidth(Buffer, maxPointsWdt);
    if (CurrentRightSWeaponSkill->GetLevel() != 20) {
      Buffer << (CurrentRightSWeaponSkill->GetLevelMap(CurrentRightSWeaponSkill->GetLevel()+1)-CurrentRightSWeaponSkill->GetHits())/100;
    } else {
      Buffer << '-';
    }
    //Buffer.Resize(60);
    FONT->RPadToPixWidth(Buffer, maxNeededWdt);
    int Bonus = CurrentRightSWeaponSkill->GetBonus();
    Buffer << '+' << (Bonus-1000)/10;
    if (Bonus %= 10) Buffer << '.' << Bonus;
    Buffer << '%';
    List.AddEntry(Buffer, WHITE);
    Something = true;
  }

  if (CurrentLeftSWeaponSkill && CurrentLeftSWeaponSkill->GetHits()/100) {
    if (!Something) List.AddEntry(CONST_S(""), LIGHT_GRAY);
    festring Buffer = CONST_S("left accustomization");
    //Buffer.Resize(30);
    FONT->RPadToPixWidth(Buffer, maxNameWdt);
    Buffer << CurrentLeftSWeaponSkill->GetLevel();
    //Buffer.Resize(40);
    FONT->RPadToPixWidth(Buffer, maxLevelWdt);
    Buffer << CurrentLeftSWeaponSkill->GetHits()/100;
    //Buffer.Resize(50);
    FONT->RPadToPixWidth(Buffer, maxPointsWdt);
    if (CurrentLeftSWeaponSkill->GetLevel() != 20) {
      Buffer << (CurrentLeftSWeaponSkill->GetLevelMap(CurrentLeftSWeaponSkill->GetLevel()+1)-CurrentLeftSWeaponSkill->GetHits())/100;
    } else {
      Buffer << '-';
    }
    //Buffer.Resize(60);
    FONT->RPadToPixWidth(Buffer, maxNeededWdt);
    int Bonus = CurrentLeftSWeaponSkill->GetBonus();
    Buffer << '+' << (Bonus-1000)/10;
    if (Bonus %= 10) Buffer << '.' << Bonus;
    Buffer << '%';
    List.AddEntry(Buffer, WHITE);
    Something = true;
  }
  return Something;
}


//==========================================================================
//
//  humanoid::CheckThrowItemOpportunity
//
//==========================================================================
truth humanoid::CheckThrowItemOpportunity () {
  if (!HasAUsableArm()) return false;
  return character::CheckThrowItemOpportunity();
}


//==========================================================================
//
//  humanoid::CheckAIZapOpportunity
//
//==========================================================================
truth humanoid::CheckAIZapOpportunity () {
  if (!HasAUsableArm() || !CanZap()) return false;
  return character::CheckAIZapOpportunity();
}


//==========================================================================
//
//  humanoid::GetSize
//
//==========================================================================
int humanoid::GetSize () const {
  int Size = 0;
  if (GetHead()) Size += GetHead()->GetSize();
  if (GetTorso()) Size += GetTorso()->GetSize();
  leg *RightLeg = GetRightLeg();
  leg *LeftLeg = GetLeftLeg();
  int lls = (LeftLeg ? LeftLeg->GetSize() : 0);
  int rls = (RightLeg ? RightLeg->GetSize() : 0);
  Size += (lls > rls ? lls : rls);
  return Max(1, Size); // fix for assassin and such
}


//==========================================================================
//
//  humanoid::GetBodyPartSize
//
//==========================================================================
sLong humanoid::GetBodyPartSize (int I, int TotalSize) const {
  switch (I) {
    case HEAD_INDEX: return 20;
    case TORSO_INDEX: return ((TotalSize-20)<<1)/5;
    case RIGHT_ARM_INDEX:
    case LEFT_ARM_INDEX: return (TotalSize-20)*3/5;
    case GROIN_INDEX: return (TotalSize-20)/3;
    case RIGHT_LEG_INDEX:
    case LEFT_LEG_INDEX: return (TotalSize-20)*3/5;
  }
  ABORT("Illegal humanoid bodypart size request!");
  return 0;
}


//==========================================================================
//
//  humanoid::GetBodyPartVolume
//
//==========================================================================
sLong humanoid::GetBodyPartVolume (int I) const {
  switch (I) {
    case HEAD_INDEX: return 4000;
    case TORSO_INDEX: return (GetTotalVolume()-4000)*13/30;
    case RIGHT_ARM_INDEX:
    case LEFT_ARM_INDEX: return (GetTotalVolume()-4000)/10;
    case GROIN_INDEX: return (GetTotalVolume()-4000)/10;
    case RIGHT_LEG_INDEX:
    case LEFT_LEG_INDEX: return ((GetTotalVolume()-4000)<<1)/15;
  }
  ABORT("Illegal humanoid bodypart volume request!");
  return 0;
}


//==========================================================================
//
//  humanoid::MakeBodyPart
//
//==========================================================================
bodypart *humanoid::MakeBodyPart (int I) const {
  switch (I) {
    case TORSO_INDEX: return humanoidtorso::Spawn(0, NO_MATERIALS);
    case HEAD_INDEX: return head::Spawn(0, NO_MATERIALS);
    case RIGHT_ARM_INDEX: return rightarm::Spawn(0, NO_MATERIALS);
    case LEFT_ARM_INDEX: return leftarm::Spawn(0, NO_MATERIALS);
    case GROIN_INDEX: return groin::Spawn(0, NO_MATERIALS);
    case RIGHT_LEG_INDEX: return rightleg::Spawn(0, NO_MATERIALS);
    case LEFT_LEG_INDEX: return leftleg::Spawn(0, NO_MATERIALS);
  }
  ABORT("Weird bodypart to make for a humanoid. It must be your fault!");
  return 0;
}


//==========================================================================
//
//  humanoid::ReceiveDamage
//
//  note that `Damager` can be 0 for parazite/god damage, for example.
//
//==========================================================================
truth humanoid::ReceiveDamage (character *Damager, int Damage, int Type,
                               int TargetFlags, int Direction,
                               truth Divide, truth PenetrateArmor, truth Critical,
                               truth ShowMsg)
{
  int ChooseFrom[MAX_BODYPARTS], BodyParts = 0;
  if ((TargetFlags&RIGHT_ARM) && GetRightArm()) ChooseFrom[BodyParts++] = 2;
  if ((TargetFlags&LEFT_ARM) && GetLeftArm()) ChooseFrom[BodyParts++] = 3;
  if ((TargetFlags&RIGHT_LEG) && GetRightLeg()) ChooseFrom[BodyParts++] = 5;
  if ((TargetFlags&LEFT_LEG) && GetLeftLeg()) ChooseFrom[BodyParts++] = 6;
  if ((TargetFlags&HEAD) && GetHead()) ChooseFrom[BodyParts++] = 1;
  if ((TargetFlags&TORSO) && GetTorso()) ChooseFrom[BodyParts++] = 0;
  if ((TargetFlags&GROIN) && GetGroin()) ChooseFrom[BodyParts++] = 4;
  if (!BodyParts) return false;
  truth Affected = false;

  if (Divide) {
    int c;
    sLong TotalVolume = 0;
    for (c = 0; c < BodyParts; ++c) TotalVolume += GetBodyPart(ChooseFrom[c])->GetBodyPartVolume();
    for (c = 0; c < BodyParts; ++c) {
      if (ReceiveBodyPartDamage(Damager, sLong(Damage)*GetBodyPart(ChooseFrom[c])->GetBodyPartVolume()/TotalVolume, Type, ChooseFrom[c], Direction, PenetrateArmor, Critical, false)) {
        Affected = true;
      }
    }
  } else {
    sLong Possibility[MAX_BODYPARTS], PossibilitySum = 0;
    int Index = 0;
    for (int c = 0; c < BodyParts; ++c) PossibilitySum += Possibility[Index++] = GetBodyPart(ChooseFrom[c])->GetBodyPartVolume();
    Index = femath::WeightedRand(Possibility, PossibilitySum);
    Affected = ReceiveBodyPartDamage(Damager, Damage, Type, ChooseFrom[Index], Direction, PenetrateArmor, Critical, false);
  }

  if (!Affected && ShowMsg) {
    if (IsPlayer()) ADD_MESSAGE("You are not hurt.");
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s is not hurt.", GetPersonalPronoun().CStr());
  }

  if (DamageTypeAffectsInventory(Type)) {
    for (int c = 0; c < GetEquipments(); ++c) {
      item *Equipment = GetEquipment(c);
      if (Equipment) Equipment->ReceiveDamage(Damager, Damage, Type);
    }
    GetStack()->ReceiveDamage(Damager, Damage, Type);
  }

  return Affected;
}


//==========================================================================
//
//  humanoid::GetMainArm
//
//==========================================================================
arm *humanoid::GetMainArm () const {
  return (GetRightArm() ? GetRightArm() : GetLeftArm());
}


//==========================================================================
//
//  humanoid::GetSecondaryArm
//
//==========================================================================
arm *humanoid::GetSecondaryArm () const {
  return (GetRightArm() ? GetLeftArm() : 0);
}


//==========================================================================
//
//  humanoid::GetEquipmentName
//
//  convert to array
//
//==========================================================================
cchar *humanoid::GetEquipmentName (int I) const  {
  switch (I) {
    case HELMET_INDEX: return "helmet";
    case AMULET_INDEX: return "amulet";
    case CLOAK_INDEX: return "cloak";
    case BODY_ARMOR_INDEX: return "body armor";
    case BELT_INDEX: return "belt";
    case RIGHT_WIELDED_INDEX: return "right hand"; //return "right hand wielded";
    case LEFT_WIELDED_INDEX: return "left hand"; //return "left hand wielded";
    case RIGHT_RING_INDEX: return "right ring";
    case LEFT_RING_INDEX: return "left ring";
    case RIGHT_GAUNTLET_INDEX: return "right glove"; //return "right gauntlet";
    case LEFT_GAUNTLET_INDEX: return "left glove"; //return "left gauntlet";
    case RIGHT_BOOT_INDEX: return "right boot";
    case LEFT_BOOT_INDEX: return "left boot";
  }
  return "forbidden piece of cloth";
}


//==========================================================================
//
//  humanoid::EquipmentSorter
//
//==========================================================================
sorter humanoid::EquipmentSorter (int I) const {
  switch (I) {
    case HELMET_INDEX: return &item::IsHelmet;
    case AMULET_INDEX: return &item::IsAmulet;
    case CLOAK_INDEX: return &item::IsCloak;
    case BODY_ARMOR_INDEX: return &item::IsBodyArmor;
    case BELT_INDEX: return &item::IsBelt;
    case RIGHT_WIELDED_INDEX: case LEFT_WIELDED_INDEX: return 0;
    case RIGHT_RING_INDEX: case LEFT_RING_INDEX: return &item::IsRing;
    case RIGHT_GAUNTLET_INDEX: case LEFT_GAUNTLET_INDEX: return &item::IsGauntlet;
    case RIGHT_BOOT_INDEX: case LEFT_BOOT_INDEX: return &item::IsBoot;
  }
  return 0;
}


//==========================================================================
//
//  humanoid::GetBodyPartOfEquipment
//
//==========================================================================
bodypart *humanoid::GetBodyPartOfEquipment (int I) const {
  switch (I) {
    case HELMET_INDEX:
    case AMULET_INDEX:
      return GetHead();
    case CLOAK_INDEX:
    case BODY_ARMOR_INDEX:
    case BELT_INDEX:
      return GetTorso();
    case RIGHT_WIELDED_INDEX:
    case RIGHT_RING_INDEX:
    case RIGHT_GAUNTLET_INDEX:
      return GetRightArm();
    case LEFT_WIELDED_INDEX:
    case LEFT_RING_INDEX:
    case LEFT_GAUNTLET_INDEX:
      return GetLeftArm();
    case RIGHT_BOOT_INDEX:
      return GetRightLeg();
    case LEFT_BOOT_INDEX:
      return GetLeftLeg();
  }
  return 0;
}


//==========================================================================
//
//  humanoid::GetEquipment
//
//==========================================================================
item *humanoid::GetEquipment (int I) const {
  switch (I) {
    case HELMET_INDEX: return GetHelmet();
    case AMULET_INDEX: return GetAmulet();
    case CLOAK_INDEX: return GetCloak();
    case BODY_ARMOR_INDEX: return GetBodyArmor();
    case BELT_INDEX: return GetBelt();
    case RIGHT_WIELDED_INDEX: return GetRightWielded();
    case LEFT_WIELDED_INDEX: return GetLeftWielded();
    case RIGHT_RING_INDEX: return GetRightRing();
    case LEFT_RING_INDEX: return GetLeftRing();
    case RIGHT_GAUNTLET_INDEX: return GetRightGauntlet();
    case LEFT_GAUNTLET_INDEX: return GetLeftGauntlet();
    case RIGHT_BOOT_INDEX: return GetRightBoot();
    case LEFT_BOOT_INDEX: return GetLeftBoot();
  }
  return 0;
}


//==========================================================================
//
//  humanoid::SetEquipment
//
//==========================================================================
void humanoid::SetEquipment (int I, item *What) {
  switch (I) {
    case HELMET_INDEX: SetHelmet(What); break;
    case AMULET_INDEX: SetAmulet(What); break;
    case CLOAK_INDEX: SetCloak(What); break;
    case BODY_ARMOR_INDEX: SetBodyArmor(What); break;
    case BELT_INDEX: SetBelt(What); break;
    case RIGHT_WIELDED_INDEX: SetRightWielded(What); break;
    case LEFT_WIELDED_INDEX: SetLeftWielded(What); break;
    case RIGHT_RING_INDEX: SetRightRing(What); break;
    case LEFT_RING_INDEX: SetLeftRing(What); break;
    case RIGHT_GAUNTLET_INDEX: SetRightGauntlet(What); break;
    case LEFT_GAUNTLET_INDEX: SetLeftGauntlet(What); break;
    case RIGHT_BOOT_INDEX: SetRightBoot(What); break;
    case LEFT_BOOT_INDEX: SetLeftBoot(What); break;
  }
}


//==========================================================================
//
//  humanoid::SwitchToDig
//
//==========================================================================
void humanoid::SwitchToDig (item *DigItem, v2 Square) {
  dig *Dig = dig::Spawn(this);
  if (GetRightArm()) {
    item *Item = GetRightArm()->GetWielded();
    if (Item && Item != DigItem) {
      Dig->SetRightBackupID(GetRightArm()->GetWielded()->GetID());
      GetRightArm()->GetWielded()->MoveTo(GetStack());
    }
  }
  if (GetLeftArm()) {
    item *Item = GetLeftArm()->GetWielded();
    if (Item && Item != DigItem) {
      Dig->SetLeftBackupID(GetLeftArm()->GetWielded()->GetID());
      GetLeftArm()->GetWielded()->MoveTo(GetStack());
    }
  }
  if (GetMainWielded() != DigItem) {
    Dig->SetMoveDigger(true);
    DigItem->RemoveFromSlot();
    if (GetMainArm() && GetMainArm()->IsUsable()) {
      GetMainArm()->SetWielded(DigItem);
    } else {
      GetSecondaryArm()->SetWielded(DigItem);
    }
  } else {
    Dig->SetMoveDigger(false);
  }
  Dig->SetSquareDug(Square);
  SetAction(Dig);
}


void humanoid::SwitchToButchery (item *DigItem, item *Corpse) {
  IvanAssert(DigItem);
  IvanAssert(Corpse && Corpse->IsCorpse());

  butcher *Butch = butcher::Spawn(this);

  if (GetRightArm()) {
    item *Item = GetRightArm()->GetWielded();
    if (Item && Item != DigItem) {
      Butch->SetRightBackupID(GetRightArm()->GetWielded()->GetID());
      GetRightArm()->GetWielded()->MoveTo(GetStack());
    }
  }

  if (GetLeftArm()) {
    item *Item = GetLeftArm()->GetWielded();
    if (Item && Item != DigItem) {
      Butch->SetLeftBackupID(GetLeftArm()->GetWielded()->GetID());
      GetLeftArm()->GetWielded()->MoveTo(GetStack());
    }
  }

  if (GetMainWielded() != DigItem) {
    Butch->SetMoveTool(true);
    DigItem->RemoveFromSlot();
    if (GetMainArm() && GetMainArm()->IsUsable()) {
      GetMainArm()->SetWielded(DigItem);
    } else {
      GetSecondaryArm()->SetWielded(DigItem);
    }
  } else {
    Butch->SetMoveTool(false);
  }
  Butch->SetCorpseID(Corpse->GetID());
  SetAction(Butch);
}


//==========================================================================
//
//  humanoid::CheckKick
//
//==========================================================================
truth humanoid::CheckKick () const {
  if (!CanKick()) {
    if (IsPlayer()) ADD_MESSAGE("This race can't kick.");
    return false;
  }
  if (GetUsableLegs() < 2) {
    if (IsPlayer()) ADD_MESSAGE("How are you going to do that with %s?", GetUsableLegs() ? "only one usable leg" : "no usable legs");
    return false;
  }
  return true;
}


//==========================================================================
//
//  humanoid::GetUsableLegs
//
//==========================================================================
int humanoid::GetUsableLegs () const {
  int Legs = 0;
  if (RightLegIsUsable()) ++Legs;
  if (LeftLegIsUsable()) ++Legs;
  return Legs;
}


//==========================================================================
//
//  humanoid::GetUsableArms
//
//==========================================================================
int humanoid::GetUsableArms () const {
  int Arms = 0;
  if (RightArmIsUsable()) ++Arms;
  if (LeftArmIsUsable()) ++Arms;
  return Arms;
}


//==========================================================================
//
//  humanoid::CheckThrow
//
//==========================================================================
truth humanoid::CheckThrow () const {
  if (!character::CheckThrow()) return false;
  if (HasAUsableArm()) return true;
  ADD_MESSAGE("You don't have a usable arm to do that!");
  return false;
}


//==========================================================================
//
//  humanoid::CheckOffer
//
//==========================================================================
truth humanoid::CheckOffer () const {
  if (HasAUsableArm()) return true;
  ADD_MESSAGE("You need a usable arm to offer.");
  return false;
}


//==========================================================================
//
//  humanoid::GetEquipmentPanelPos
//
//  convert to array
//
//==========================================================================
v2 humanoid::GetEquipmentPanelPos (int I) const {
  switch (I) {
    case HELMET_INDEX: return v2(34, -22);
    case AMULET_INDEX: return v2(14, -22);
    case CLOAK_INDEX: return v2(-6, -22);
    case BODY_ARMOR_INDEX: return v2(54, -22);
    case BELT_INDEX: return v2(24, 70);
    case RIGHT_WIELDED_INDEX: return v2(-14, 4);
    case LEFT_WIELDED_INDEX: return v2(62, 4);
    case RIGHT_RING_INDEX: return v2(-14, 44);
    case LEFT_RING_INDEX: return v2(62, 44);
    case RIGHT_GAUNTLET_INDEX: return v2(-14, 24);
    case LEFT_GAUNTLET_INDEX: return v2(62, 24);
    case RIGHT_BOOT_INDEX: return v2(4, 70);
    case LEFT_BOOT_INDEX: return v2(44, 70);
  }
  return v2(24, 12);
}


//==========================================================================
//
//  humanoid::GetGlobalResistance
//
//==========================================================================
int humanoid::GetGlobalResistance (int Type) const {
  int Resistance = GetResistance(Type);
  if (GetCloak()) Resistance += GetCloak()->GetResistance(Type);

  // shields grant some overall protection (taken from comm. fork).
  // i nerfed the resistance (divided by 2), because otherwise shields
  // are way too strong for my taste. like, Jenny is mostly helpless
  // when you have oak shield.
  if (GetRightWielded() && GetRightWielded()->IsShield(this)) {
    if (Type&PHYSICAL_DAMAGE) {
      #if 0
      ConLogf("SHIELD: res=%d; shield=%d (%d); new=%d",
              Resistance, GetRightWielded()->GetResistance(Type),
              Div2Ass(GetRightWielded()->GetResistance(Type)),
              Resistance + Div2Ass(GetRightWielded()->GetResistance(Type)));
      #endif
      Resistance += Div2Ass(GetRightWielded()->GetResistance(Type));
    } else {
      Resistance += GetRightWielded()->GetResistance(Type);
    }
  }
  if (GetLeftWielded() && GetLeftWielded()->IsShield(this)) {
    if (Type&PHYSICAL_DAMAGE) {
      Resistance += Div2Ass(GetLeftWielded()->GetResistance(Type));
    } else {
      Resistance += GetLeftWielded()->GetResistance(Type);
    }
  }
  // end of shields code

  if (!(Type&PHYSICAL_DAMAGE)) {
    if (GetAmulet()) Resistance += GetAmulet()->GetResistance(Type);
    if (GetRightRing()) Resistance += GetRightRing()->GetResistance(Type);
    if (GetLeftRing()) Resistance += GetLeftRing()->GetResistance(Type);
  }

  return Resistance;
}


//==========================================================================
//
//  humanoid::TryToRiseFromTheDead
//
//==========================================================================
truth humanoid::TryToRiseFromTheDead () {
  int c;
  for (c = 0; c < BodyParts; ++c) {
    if (!GetBodyPart(c)) {
      bodypart *BodyPart = SearchForOriginalBodyPart(c);
      if (BodyPart) {
        BodyPart->RemoveFromSlot();
        AttachBodyPart(BodyPart);
        BodyPart->SetHP(1);
      }
    }
  }
  for (c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (BodyPartIsVital(c) && !BodyPart) {
      if (!HandleNoBodyPart(c)) return false;
    }
    if (BodyPart) {
      BodyPart->ResetSpoiling();
      if (BodyPart->CanRegenerate() || BodyPart->GetHP() < 1) BodyPart->SetHP(1);
    }
  }
  ResetStates();
  return true;
}


//==========================================================================
//
//  humanoid::HandleNoBodyPart
//
//==========================================================================
truth humanoid::HandleNoBodyPart (int I) {
  switch (I) {
    case HEAD_INDEX:
      if (CanBeSeenByPlayer()) ADD_MESSAGE("The headless body of %s vibrates violently.", CHAR_NAME(DEFINITE));
      return false;
    case GROIN_INDEX:
      if (CanBeSeenByPlayer()) ADD_MESSAGE("The groinless body of %s vibrates violently.", CHAR_NAME(DEFINITE));
      return false;
    case TORSO_INDEX:
      ABORT("The corpse does not have a torso.");
    default:
      return true;
  }
}


//==========================================================================
//
//  humanoid::GetBodyPartBitmapPos
//
//==========================================================================
v2 humanoid::GetBodyPartBitmapPos (int I, truth) const {
  switch (I) {
    case TORSO_INDEX: return GetTorsoBitmapPos();
    case HEAD_INDEX: return GetHeadBitmapPos();
    case RIGHT_ARM_INDEX: return GetRightArmBitmapPos();
    case LEFT_ARM_INDEX: return GetLeftArmBitmapPos();
    case GROIN_INDEX: return GetGroinBitmapPos();
    case RIGHT_LEG_INDEX: return GetRightLegBitmapPos();
    case LEFT_LEG_INDEX: return GetLeftLegBitmapPos();
  }
  ABORT("Weird bodypart BitmapPos request for a humanoid!");
  return v2();
}


//==========================================================================
//
//  humanoid::GetBodyPartColorB
//
//==========================================================================
col16 humanoid::GetBodyPartColorB (int I, truth) const {
  switch (I) {
    case TORSO_INDEX: return GetTorsoMainColor();
    case HEAD_INDEX: return GetCapColor();
    case RIGHT_ARM_INDEX:
    case LEFT_ARM_INDEX: return GetArmMainColor();
    case GROIN_INDEX:
    case RIGHT_LEG_INDEX:
    case LEFT_LEG_INDEX: return GetLegMainColor();
  }
  ABORT("Weird bodypart col B request for a humanoid!");
  return 0;
}


//==========================================================================
//
//  humanoid::GetBodyPartColorC
//
//==========================================================================
col16 humanoid::GetBodyPartColorC (int I, truth) const {
  switch (I) {
    case TORSO_INDEX: return GetBeltColor();
    case HEAD_INDEX: return GetHairColor();
    case RIGHT_ARM_INDEX:
    case LEFT_ARM_INDEX: return GetGauntletColor();
    case GROIN_INDEX:
    case RIGHT_LEG_INDEX:
    case LEFT_LEG_INDEX: return GetBootColor();
  }
  ABORT("Weird bodypart col C request for a humanoid!");
  return 0;
}


//==========================================================================
//
//  humanoid::GetBodyPartColorD
//
//==========================================================================
col16 humanoid::GetBodyPartColorD (int I, truth) const {
  switch (I) {
    case TORSO_INDEX: return GetTorsoSpecialColor();
    case HEAD_INDEX: return GetEyeColor();
    case RIGHT_ARM_INDEX:
    case LEFT_ARM_INDEX: return GetArmSpecialColor();
    case GROIN_INDEX:
    case RIGHT_LEG_INDEX:
    case LEFT_LEG_INDEX: return GetLegSpecialColor();
  }
  ABORT("Weird bodypart col D request for a humanoid!");
  return 0;
}


//==========================================================================
//
//  humanoid::GetBodyPartSparkleFlags
//
//==========================================================================
int humanoid::GetBodyPartSparkleFlags (int I) const {
  truth Sparkling = false;
  int SparkleFlags = (GetNaturalSparkleFlags()&SKIN_COLOR ? SPARKLING_A : 0);
  switch (I) {
    case TORSO_INDEX: Sparkling = GetNaturalSparkleFlags()&TORSO_MAIN_COLOR; break;
    case HEAD_INDEX: Sparkling = GetNaturalSparkleFlags()&CAP_COLOR; break;
    case RIGHT_ARM_INDEX: case LEFT_ARM_INDEX: Sparkling = GetNaturalSparkleFlags()&ARM_MAIN_COLOR; break;
    case GROIN_INDEX: case RIGHT_LEG_INDEX: case LEFT_LEG_INDEX: Sparkling = GetNaturalSparkleFlags()&LEG_MAIN_COLOR; break;
  }
  SparkleFlags |= (Sparkling ? SPARKLING_B : 0);
  Sparkling = false;
  switch (I) {
    case TORSO_INDEX: Sparkling = GetNaturalSparkleFlags()&BELT_COLOR; break;
    case HEAD_INDEX: Sparkling = GetNaturalSparkleFlags()&HAIR_COLOR; break;
    case RIGHT_ARM_INDEX: case LEFT_ARM_INDEX: Sparkling = GetNaturalSparkleFlags()&GAUNTLET_COLOR; break;
    case GROIN_INDEX: case RIGHT_LEG_INDEX: case LEFT_LEG_INDEX: Sparkling = GetNaturalSparkleFlags()&BOOT_COLOR; break;
  }
  SparkleFlags |= (Sparkling ? SPARKLING_C : 0);
  Sparkling = false;
  switch (I) {
    case TORSO_INDEX: Sparkling = GetNaturalSparkleFlags()&TORSO_SPECIAL_COLOR; break;
    case HEAD_INDEX: Sparkling = GetNaturalSparkleFlags()&EYE_COLOR; break;
    case RIGHT_ARM_INDEX: case LEFT_ARM_INDEX: Sparkling = GetNaturalSparkleFlags()&ARM_SPECIAL_COLOR; break;
    case GROIN_INDEX: case RIGHT_LEG_INDEX: case LEFT_LEG_INDEX: Sparkling = GetNaturalSparkleFlags()&LEG_SPECIAL_COLOR; break;
  }
  SparkleFlags |= (Sparkling ? SPARKLING_D : 0);
  return SparkleFlags;
}


//==========================================================================
//
//  humanoid::Bite
//
//==========================================================================
void humanoid::Bite (character *Enemy, v2 HitPos, int Direction, truth ForceHit) {
  if (!GetHead()) ABORT("Love can make you beheaded!");
  // this function ought not to be called without a head
  EditNP(-50);
  EditAP(-GetHead()->GetBiteAPCost());
  EditExperience(AGILITY, 150, 1<<9);
  EditStamina(-1000, false);
  Enemy->TakeHit(this, 0, GetHead(), HitPos, GetHead()->GetBiteDamage(),
                 GetHead()->GetBiteToHitValue(), RAND_N(26)-RAND_N(26), BITE_ATTACK,
                 Direction, !RAND_N(GetCriticalModifier()), ForceHit);
}


//==========================================================================
//
//  humanoid::Kick
//
//==========================================================================
void humanoid::Kick (lsquare *Square, int Direction, truth ForceHit) {
  leg *KickLeg = (RAND_2 ? GetRightLeg() : GetLeftLeg());
  if (!KickLeg) {
    if (GetRightLeg()) KickLeg = GetRightLeg();
    else if (GetLeftLeg()) KickLeg = GetLeftLeg();
    else return;
  }
  EditNP(-50);
  EditAP(-KickLeg->GetKickAPCost());
  EditStamina(-10000/GetAttribute(LEG_STRENGTH), false);
  if (Square->BeKicked(this, 0, KickLeg, KickLeg->GetKickDamage(), KickLeg->GetKickToHitValue(),
                       RAND_N(26)-RAND_N(26), Direction, !RAND_N(GetCriticalModifier()), ForceHit))
  {
    KickLeg->EditExperience(LEG_STRENGTH, 75, 1<<9);
    KickLeg->EditExperience(AGILITY, 75, 1<<9);
  }
}


//==========================================================================
//
//  humanoid::GetTimeToKill
//
//==========================================================================
double humanoid::GetTimeToKill (ccharacter *Enemy, truth UseMaxHP) const {
  IvanAssert(Enemy);
  double Effectivity = 0.0;
  int AttackStyles = 0;

  if (IsUsingArms()) {
    arm *RightArm = GetRightArm();
    arm *LeftArm = GetLeftArm();
    if (RightArm || LeftArm) {
      if (RightArm) {
        double Damage = RightArm->GetDamage();
        if (Damage > 0) {
          Effectivity +=
            1.0 / (Enemy->GetTimeToDie(this, int(Damage) + 1, RightArm->GetToHitValue(),
                      AttackIsBlockable(GetRightWielded() ? WEAPON_ATTACK : UNARMED_ATTACK),
                      UseMaxHP) * RightArm->GetAPCost());
        }
      }
      if (LeftArm) {
        double Damage = LeftArm->GetDamage();
        if (Damage > 0) {
          Effectivity +=
            1.0 / (Enemy->GetTimeToDie(this, int(Damage) + 1, LeftArm->GetToHitValue(),
                      AttackIsBlockable(GetLeftWielded() ? WEAPON_ATTACK : UNARMED_ATTACK),
                      UseMaxHP) * LeftArm->GetAPCost());
        }
      }
      AttackStyles += 1;
    }
  }

  if (IsUsingLegs()) {
    leg *RightLeg = GetRightLeg();
    leg *LeftLeg = GetLeftLeg();
    if (RightLeg || LeftLeg) {
      double TimeToDie = 0.0;
      if (RightLeg) {
        TimeToDie += Enemy->GetTimeToDie(this, int(RightLeg->GetKickDamage()) + 1,
                        RightLeg->GetKickToHitValue(),
                        AttackIsBlockable(KICK_ATTACK), UseMaxHP) * RightLeg->GetKickAPCost();
      }
      if (LeftLeg) {
        TimeToDie += Enemy->GetTimeToDie(this, int(LeftLeg->GetKickDamage()) + 1,
                        LeftLeg->GetKickToHitValue(),
                        AttackIsBlockable(KICK_ATTACK), UseMaxHP) * LeftLeg->GetKickAPCost();
      }
      Effectivity += 2.0 / TimeToDie;
      AttackStyles += 1;
    }
  }

  if (IsUsingHead()) {
    head *Head = GetHead();
    if (Head) {
      Effectivity +=
        1.0 / (Enemy->GetTimeToDie(this, int(Head->GetBiteDamage()) + 1,
                  Head->GetBiteToHitValue(),
                  AttackIsBlockable(BITE_ATTACK), UseMaxHP) * Head->GetBiteAPCost());
      AttackStyles += 1;
    }
  }

  if (StateIsActivated(HASTE)) Effectivity *= 2.0;
  if (StateIsActivated(SLOW)) Effectivity /= 2.0;

  return (AttackStyles ? (double)AttackStyles / Effectivity : 10000000);
}


//==========================================================================
//
//  humanoid::GetAttribute
//
//==========================================================================
int humanoid::GetAttribute (int Identifier, truth AllowBonus) const {
  if (Identifier < BASE_ATTRIBUTES) {
    return character::GetAttribute(Identifier, AllowBonus);
  }
  int Attrib = 0;
  if (Identifier == ARM_STRENGTH || Identifier == DEXTERITY) {
    arm *RightArm = GetRightArm();
    if (RightArm) Attrib += RightArm->GetAttribute(Identifier, AllowBonus);
    arm *LeftArm = GetLeftArm();
    if (LeftArm) Attrib += LeftArm->GetAttribute(Identifier, AllowBonus);
  } else if (Identifier == LEG_STRENGTH || Identifier == AGILITY) {
    leg *RightLeg = GetRightLeg();
    if (RightLeg) Attrib += RightLeg->GetAttribute(Identifier, AllowBonus);
    leg *LeftLeg = GetLeftLeg();
    if (LeftLeg) Attrib += LeftLeg->GetAttribute(Identifier, AllowBonus);
  } else {
    ABORT("Illegal humanoid attribute %d request!", Identifier);
    return 0xEBBA;
  }
  //k8: it is used for divisions, so avoid returning 0 here.
  //    this should not happen, ever, but...
  //return Attrib>>1;
  Attrib >>= 1;
  if (Attrib == 0) Attrib = 1;
  return Attrib;
}


//==========================================================================
//
//  humanoid::EditAttribute
//
//==========================================================================
truth humanoid::EditAttribute (int Identifier, int Value) {
  if (Identifier < BASE_ATTRIBUTES) return character::EditAttribute(Identifier, Value);
  if (Identifier == ARM_STRENGTH || Identifier == DEXTERITY) {
    truth Success = false;
    if (GetRightArm() && GetRightArm()->EditAttribute(Identifier, Value)) Success = true;
    if (GetLeftArm() && GetLeftArm()->EditAttribute(Identifier, Value)) Success = true;
    return Success;
  }
  if (Identifier == LEG_STRENGTH || Identifier == AGILITY) {
    truth Success = false;
    if (GetRightLeg() && GetRightLeg()->EditAttribute(Identifier, Value)) Success = true;
    if (GetLeftLeg() && GetLeftLeg()->EditAttribute(Identifier, Value)) Success = true;
    return Success;
  }
  ABORT("Illegal humanoid attribute %d edit request!", Identifier);
  return false;
}


//==========================================================================
//
//  humanoid::EditExperience
//
//==========================================================================
void humanoid::EditExperience (int Identifier, double Value, double Speed) {
  if (!AllowExperience()) return;
  if (Identifier < BASE_ATTRIBUTES) {
    character::EditExperience(Identifier, Value, Speed);
  } else if (Identifier == ARM_STRENGTH || Identifier == DEXTERITY) {
    if (GetRightArm()) GetRightArm()->EditExperience(Identifier, Value, Speed);
    if (GetLeftArm()) GetLeftArm()->EditExperience(Identifier, Value, Speed);
  } else if (Identifier == LEG_STRENGTH || Identifier == AGILITY) {
    if (GetRightLeg()) GetRightLeg()->EditExperience(Identifier, Value, Speed);
    if (GetLeftLeg()) GetLeftLeg()->EditExperience(Identifier, Value, Speed);
  } else {
    ABORT("Illegal humanoid attribute %d experience edit request!", Identifier);
  }
}


//==========================================================================
//
//  humanoid::GetRandomStepperBodyPart
//
//==========================================================================
int humanoid::GetRandomStepperBodyPart () const {
  int Possible = 0, PossibleArray[3];
  if (GetRightLeg()) PossibleArray[Possible++] = RIGHT_LEG_INDEX;
  if (GetLeftLeg()) PossibleArray[Possible++] = LEFT_LEG_INDEX;
  if (Possible) return PossibleArray[RAND_N(Possible)];
  if (GetRightArm()) PossibleArray[Possible++] = RIGHT_ARM_INDEX;
  if (GetLeftArm()) PossibleArray[Possible++] = LEFT_ARM_INDEX;
  if (Possible) return PossibleArray[RAND_N(Possible)];
  if (GetHead()) PossibleArray[Possible++] = HEAD_INDEX;
  if (GetGroin()) PossibleArray[Possible++] = GROIN_INDEX;
  PossibleArray[Possible++] = TORSO_INDEX;
  return PossibleArray[RAND_N(Possible)];
}


//==========================================================================
//
//  humanoid::CheckForBlock
//
//==========================================================================
int humanoid::CheckForBlock (character *Enemy, item *Weapon, double ToHitValue,
                             int Damage, int Success, int Type)
{
  if (GetAction()) return Damage;

  if (Damage) {
    // prefer shield arm first
    item *RBlocker = GetRightWielded();
    item *LBlocker = GetLeftWielded();

    if (RBlocker && RBlocker->IsShield(this)) {
      Damage = CheckForBlockWithArm(Enemy, Weapon, GetRightArm(), ToHitValue, Damage, Success, Type);
      if (Damage && LBlocker && (!Weapon || Weapon->Exists())) {
        Damage = CheckForBlockWithArm(Enemy, Weapon, GetLeftArm(), ToHitValue, Damage, Success, Type);
      }
    } else if (LBlocker && LBlocker->IsShield(this)) {
      Damage = CheckForBlockWithArm(Enemy, Weapon, GetLeftArm(), ToHitValue, Damage, Success, Type);
      if (Damage && RBlocker && (!Weapon || Weapon->Exists())) {
        Damage = CheckForBlockWithArm(Enemy, Weapon, GetRightArm(), ToHitValue, Damage, Success, Type);
      }
    } else {
      // original code
      if (GetRightWielded()) {
        Damage = CheckForBlockWithArm(Enemy, Weapon, GetRightArm(), ToHitValue, Damage, Success, Type);
      }
      if (Damage && GetLeftWielded() && (!Weapon || Weapon->Exists())) {
        Damage = CheckForBlockWithArm(Enemy, Weapon, GetLeftArm(), ToHitValue, Damage, Success, Type);
      }
    }
  }

  return Damage;
}


//==========================================================================
//
//  humanoid::CanWield
//
//==========================================================================
truth humanoid::CanWield () const {
  return CanUseEquipment(RIGHT_WIELDED_INDEX) || CanUseEquipment(LEFT_WIELDED_INDEX);
}


//==========================================================================
//
//  humanoid::CheckBalance
//
//==========================================================================
truth humanoid::CheckBalance (double KickDamage) {
  return
    !CanMove() ||
    IsStuck() ||
    !KickDamage ||
    (GetUsableLegs() != 1 && !IsFlying() && KickDamage*5 < RAND_N(GetSize()));
}


//==========================================================================
//
//  humanoid::GetMoveAPRequirement
//
//==========================================================================
sLong humanoid::GetMoveAPRequirement (int Difficulty) const {
  if (IsFlying()) return (!StateIsActivated(PANIC) ? 10000000 : 8000000)*Difficulty/(APBonus(GetAttribute(AGILITY))*GetMoveEase());
  switch (GetUsableLegs()) {
    case 0: return (!StateIsActivated(PANIC) ? 20000000 : 16000000)*Difficulty/(APBonus(GetAttribute(AGILITY))*GetMoveEase());
    case 1: return (!StateIsActivated(PANIC) ? 13333333 : 10666667)*Difficulty/(APBonus(GetAttribute(AGILITY))*GetMoveEase());
    case 2: return (!StateIsActivated(PANIC) ? 10000000 : 8000000)*Difficulty/(APBonus(GetAttribute(AGILITY))*GetMoveEase());
  }
  ABORT("A %d legged humanoid invaded the dungeon!", GetUsableLegs());
  return 0;
}


//==========================================================================
//
//  humanoid::EquipmentEasilyRecognized
//
//==========================================================================
truth humanoid::EquipmentEasilyRecognized (int I) const {
  if (GetRelation(PLAYER) != HOSTILE) return true;
  switch (I) {
    case AMULET_INDEX:
    case RIGHT_RING_INDEX:
    case LEFT_RING_INDEX:
    case BELT_INDEX:
      return false;
  }
  return true;
}


//==========================================================================
//
//  humanoid::SurgicallyDetachBodyPart
//
//==========================================================================
void humanoid::SurgicallyDetachBodyPart () {
  int ToBeDetached;
  switch (game::KeyQuestion(CONST_S("What limb? \1Gl\2eft arm, \1Gr\2ight arm, "
                                    "\1GL\2eft leg, \1GR\2ight leg, \1Hh\2ead?"),
                                    /*KEY_ESC*/REQUIRES_ANSWER,
                                    'L', 'L' + KEY_MOD_SHIFT,
                                    'r', 'R' + KEY_MOD_SHIFT,
                                    'h', 'H' + KEY_MOD_SHIFT,
                                    KEY_ESC, 0))
  {
    case 'L': ToBeDetached = LEFT_ARM_INDEX; break;
    case 'R': ToBeDetached = RIGHT_ARM_INDEX; break;
    case 'L' + KEY_MOD_SHIFT: ToBeDetached = LEFT_LEG_INDEX; break;
    case 'R' + KEY_MOD_SHIFT: ToBeDetached = RIGHT_LEG_INDEX; break;
    case 'H': case 'H' + KEY_MOD_SHIFT: ToBeDetached = HEAD_INDEX; break; // maybe no head, should be up to the player to decide.
    default: return;
  }
  if (GetBodyPart(ToBeDetached)) {
    item *ToDrop = SevereBodyPart(ToBeDetached);
    SendNewDrawRequest();
    if (ToDrop) {
      GetStack()->AddItem(ToDrop);
      ToDrop->DropEquipment();
    }
    ADD_MESSAGE("Bodypart detached!");
  } else {
    ADD_MESSAGE("That bodypart has already been detached.");
  }
  CheckDeath(CONST_S("had one of his vital bodyparts surgically removed"), 0);
}


//==========================================================================
//
//  humanoid::SignalEquipmentAdd
//
//==========================================================================
void humanoid::SignalEquipmentAdd (int EquipmentIndex) {
  character::SignalEquipmentAdd(EquipmentIndex);
  if (EquipmentIndex == RIGHT_WIELDED_INDEX) EnsureCurrentSWeaponSkillIsCorrect(CurrentRightSWeaponSkill, GetRightWielded());
  else if (EquipmentIndex == LEFT_WIELDED_INDEX) EnsureCurrentSWeaponSkillIsCorrect(CurrentLeftSWeaponSkill, GetLeftWielded());
  if (!IsInitializing()) CalculateBattleInfo();
}


//==========================================================================
//
//  humanoid::SignalEquipmentRemoval
//
//==========================================================================
void humanoid::SignalEquipmentRemoval (int EquipmentIndex, citem *Item) {
  character::SignalEquipmentRemoval(EquipmentIndex, Item);
  if (EquipmentIndex == RIGHT_WIELDED_INDEX) EnsureCurrentSWeaponSkillIsCorrect(CurrentRightSWeaponSkill, 0);
  else if (EquipmentIndex == LEFT_WIELDED_INDEX) EnsureCurrentSWeaponSkillIsCorrect(CurrentLeftSWeaponSkill, 0);
  if (!IsInitializing()) CalculateBattleInfo();
}


//==========================================================================
//
//  humanoid::SWeaponSkillTick
//
//==========================================================================
void humanoid::SWeaponSkillTick () {
  for (std::list<sweaponskill*>::iterator i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ) {
    if ((*i)->Tick() && IsPlayer()) {
      item *Item = SearchForItem(*i);
      if (Item) {
        auto ffs = Item->TempDisableFluids();
        (*i)->AddLevelDownMessage(Item->CHAR_NAME(UNARTICLED));
      }
    }
    if (!(*i)->GetHits() && *i != GetCurrentRightSWeaponSkill() && *i != GetCurrentLeftSWeaponSkill()) {
      std::list<sweaponskill*>::iterator Dirt = i++;
      SWeaponSkill.erase(Dirt);
    } else {
      ++i;
    }
  }
}


//==========================================================================
//
//  humanoid::DrawBodyParts
//
//==========================================================================
void humanoid::DrawBodyParts (blitdata &BlitData, col16 MonoColor) const {
  bitmap *TileBuffer = igraph::GetTileBuffer();
  bitmap *RealBitmap = BlitData.Bitmap;
  blitdata B = {
    TileBuffer,
    { BlitData.Dest.X, BlitData.Dest.Y },
    { 0, 0 },
    { TILE_SIZE, TILE_SIZE },
    { 0 },
    TRANSPARENT_COLOR,
    BlitData.CustomData
  };

  if (MonoColor == TRANSPARENT_COLOR) {
    RealBitmap->NormalBlit(B);
  } else {
    //TileBuffer->Fill(BlitData.Dest, B.Border, TRANSPARENT_COLOR);
    TileBuffer->Fill(v2(0, 0), B.Border, TRANSPARENT_COLOR);
  }
  TileBuffer->FillPriority(0);
  B.Src.X = B.Src.Y = 0;
  B.Luminance = BlitData.Luminance;
  for (int c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(DrawOrder[c]);
    if (BodyPart) {
      B.Dest = GetDrawDisplacement(c);
      BodyPart->Draw(B);
    }
  }
  B.Dest.X = B.Dest.Y = 0;
  arm *LeftArm = GetLeftArm();
  if (LeftArm) LeftArm->DrawWielded(B);
  arm *RightArm = GetRightArm();
  if (RightArm) RightArm->DrawWielded(B);

  if (MonoColor == TRANSPARENT_COLOR) {
    TileBuffer->FastBlit(RealBitmap, BlitData.Dest);
  } else {
    B.Src = v2(0, 0);
    B.Dest = BlitData.Dest;
    B.Bitmap = BlitData.Bitmap;
    TileBuffer->BlitMono(B, MonoColor);
  }
}


//==========================================================================
//
//  humanoid::CanUseStethoscope
//
//==========================================================================
truth humanoid::CanUseStethoscope (truth PrintReason) const {
  if (!GetUsableArms()) {
    if (PrintReason) ADD_MESSAGE("You need a usable arm to use a stethoscope.");
    return false;
  }
  if (!GetHead()) {
    if (PrintReason) ADD_MESSAGE("You need a head to use stethoscope.");
    return false;
  }
  return true;
}


//==========================================================================
//
//  humanoid::IsUsingArms
//
//==========================================================================
truth humanoid::IsUsingArms () const {
  return (GetAttackStyle()&USE_ARMS) && CanAttackWithAnArm();
}


//==========================================================================
//
//  humanoid::IsUsingLegs
//
//==========================================================================
truth humanoid::IsUsingLegs () const {
  return ((GetAttackStyle()&USE_LEGS) || ((GetAttackStyle()&USE_ARMS) && !CanAttackWithAnArm())) && HasTwoUsableLegs();
}


//==========================================================================
//
//  humanoid::IsUsingHead
//
//==========================================================================
truth humanoid::IsUsingHead () const {
  return ((GetAttackStyle()&USE_HEAD) || (((GetAttackStyle()&USE_LEGS) || ((GetAttackStyle()&USE_ARMS) && !CanAttackWithAnArm())) && !HasTwoUsableLegs())) && GetHead();
}


//==========================================================================
//
//  humanoid::CalculateBattleInfo
//
//==========================================================================
void humanoid::CalculateBattleInfo () {
  CalculateDodgeValue();
  doforbodyparts()(this, &bodypart::CalculateAttackInfo);
}


//==========================================================================
//
//  humanoid::AddSpecialEquipmentInfo
//
//==========================================================================
void humanoid::AddSpecialEquipmentInfo (festring &String, int I) const {
  if ((I == RIGHT_WIELDED_INDEX && GetRightArm()->TwoHandWieldIsActive()) ||
      (I == LEFT_WIELDED_INDEX && GetLeftArm()->TwoHandWieldIsActive()))
  {
    String << " (in both hands)";
  }
}


/* Yes, this is evil. */
#define INSTANTIATE(name)  do { \
  if (DataBase->name.IsValid() && (Item = DataBase->name.Instantiate(SpecialFlags))) { \
    Set##name(Item); \
  } \
} while (0)


//==========================================================================
//
//  humanoid::CreateInitialEquipment
//
//==========================================================================
void humanoid::CreateInitialEquipment (int SpecialFlags) {
  character::CreateInitialEquipment(SpecialFlags);
  item *Item;

  INSTANTIATE(Helmet);
  INSTANTIATE(Amulet);
  INSTANTIATE(Cloak);
  INSTANTIATE(BodyArmor);
  INSTANTIATE(Belt);
  INSTANTIATE(RightWielded);
  INSTANTIATE(LeftWielded);
  INSTANTIATE(RightRing);
  INSTANTIATE(LeftRing);
  INSTANTIATE(RightGauntlet);
  INSTANTIATE(LeftGauntlet);
  INSTANTIATE(RightBoot);
  INSTANTIATE(LeftBoot);

  if (CurrentRightSWeaponSkill) CurrentRightSWeaponSkill->AddHit(GetRightSWeaponSkillHits()*100);
  if (CurrentLeftSWeaponSkill) CurrentLeftSWeaponSkill->AddHit(GetLeftSWeaponSkillHits()*100);
}


//==========================================================================
//
//  humanoid::GetBodyPartName
//
//==========================================================================
festring humanoid::GetBodyPartName (int I, truth Articled) const {
  festring Article;
  if (Articled) Article << 'a';
  switch (I) {
    case HEAD_INDEX: return Article + "head";
    case TORSO_INDEX: return Article + "torso";
    case RIGHT_ARM_INDEX: return Article + "right arm";
    case LEFT_ARM_INDEX: return Article + "left arm";
    case GROIN_INDEX: return Article + "groin";
    case RIGHT_LEG_INDEX: return Article + "right leg";
    case LEFT_LEG_INDEX: return Article + "left leg";
  }
  ABORT("Illegal humanoid bodypart name request!");
  return festring();
}


//==========================================================================
//
//  humanoid::CreateBlockPossibilityVector
//
//==========================================================================
void humanoid::CreateBlockPossibilityVector (blockvector &Vector, double ToHitValue) const {
  double RightBlockChance = 0;
  int RightBlockCapability = 0;
  double LeftBlockChance = 0;
  int LeftBlockCapability = 0;
  arm *RightArm = GetRightArm();
  arm *LeftArm = GetLeftArm();
  if (RightArm) {
    RightBlockChance = RightArm->GetBlockChance(ToHitValue);
    RightBlockCapability = RightArm->GetBlockCapability();
  }
  if (LeftArm) {
    LeftBlockChance = LeftArm->GetBlockChance(ToHitValue);
    LeftBlockCapability = LeftArm->GetBlockCapability();
  }
  /* Double block */
  if (RightBlockCapability+LeftBlockCapability) {
    Vector.push_back(std::make_pair(RightBlockChance*LeftBlockChance, RightBlockCapability+LeftBlockCapability));
  }
  /* Right block */
  if (RightBlockCapability) {
    Vector.push_back(std::make_pair(RightBlockChance*(1-LeftBlockChance), RightBlockCapability));
  }
  /* Left block */
  if (LeftBlockCapability) {
    Vector.push_back(std::make_pair(LeftBlockChance*(1-RightBlockChance), LeftBlockCapability));
  }
}


item *humanoid::SevereBodyPart (int BodyPartIndex, truth ForceDisappearance, stack *EquipmentDropStack) {
  if (BodyPartIndex == RIGHT_ARM_INDEX) EnsureCurrentSWeaponSkillIsCorrect(CurrentRightSWeaponSkill, 0);
  else if (BodyPartIndex == LEFT_ARM_INDEX) EnsureCurrentSWeaponSkillIsCorrect(CurrentLeftSWeaponSkill, 0);
  return character::SevereBodyPart(BodyPartIndex, ForceDisappearance, EquipmentDropStack);
}


//==========================================================================
//
//  humanoid::GetDeathMessage
//
//==========================================================================
cfestring &humanoid::GetDeathMessage () const {
  static festring HeadlessDeathMsg = CONST_S("@Dd dies without a sound.");
  return (GetHead() || character::GetDeathMessage() != "@Dd dies screaming." ? character::GetDeathMessage() : HeadlessDeathMsg);
}


//==========================================================================
//
//  humanoid::GetSWeaponSkillLevel
//
//==========================================================================
int humanoid::GetSWeaponSkillLevel (citem *Item) const {
  std::list<sweaponskill*>::const_iterator i;
  for (i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) if ((*i)->IsSkillOf(Item)) return (*i)->GetLevel();
  for (idholder *I = Item->GetCloneMotherID(); I; I = I->Next) {
    for(i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) {
      if ((*i)->IsSkillOfCloneMother(Item, I->ID)) return (*i)->GetLevel();
    }
  }
  return 0;
}


//==========================================================================
//
//  humanoid::UseMaterialAttributes
//
//==========================================================================
truth humanoid::UseMaterialAttributes () const {
  return combinebodypartpredicates()(this, &bodypart::UseMaterialAttributes, 0);
}


//==========================================================================
//
//  humanoid::CalculateDodgeValue
//
//==========================================================================
void humanoid::CalculateDodgeValue () {
  DodgeValue = 0.05*GetMoveEase()*GetAttribute(AGILITY)/sqrt(GetSize());
  if (IsFlying()) {
    DodgeValue *= 2;
  } else {
    if (!HasAUsableLeg()) DodgeValue *= 0.50;
    if (!HasTwoUsableLegs()) DodgeValue *= 0.75;
  }
  if (DodgeValue < 1) DodgeValue = 1;
}


//==========================================================================
//
//  humanoid::CheckZap
//
//==========================================================================
truth humanoid::CheckZap () {
  if (!GetUsableArms()) {
    ADD_MESSAGE("You need at least one usable arm to zap.");
    return false;
  }
  return character::CheckZap();
}


//==========================================================================
//
//  humanoid::CheckTalk
//
//==========================================================================
truth humanoid::CheckTalk () {
  if (!character::CheckTalk()) return false;
  if (!GetHead()) {
    ADD_MESSAGE("You need a head to talk.");
    return false;
  }
  return true;
}


//==========================================================================
//
//  humanoid::GetWeaponTooHeavyRate
//
//==========================================================================
int humanoid::GetWeaponTooHeavyRate (int I) const {
  switch (I) {
    case RIGHT_WIELDED_INDEX:
      return (GetRightArm() ? GetRightArm()->GetWeaponTooHeavyRate() : WEAPON_UNUSABLE);
    case LEFT_WIELDED_INDEX:
      return (GetLeftArm() ? GetLeftArm()->GetWeaponTooHeavyRate() : WEAPON_UNUSABLE);
    default:
      break;
  }
  return WEAPON_UNUSABLE;
}


//==========================================================================
//
//  humanoid::CheckIfEquipmentIsNotUsable
//
//==========================================================================
truth humanoid::CheckIfEquipmentIsNotUsable (int I) const {
  return
    (I == RIGHT_WIELDED_INDEX && GetRightArm()->CheckIfWeaponTooHeavy("this item")) ||
    (I == LEFT_WIELDED_INDEX && GetLeftArm()->CheckIfWeaponTooHeavy("this item")) ||
    (I == RIGHT_WIELDED_INDEX && GetLeftWielded() && GetLeftWielded()->IsTwoHanded() && GetLeftArm()->CheckIfWeaponTooHeavy(festring(GetPossessivePronoun()+" other wielded item").CStr())) ||
    (I == LEFT_WIELDED_INDEX && GetRightWielded() && GetRightWielded()->IsTwoHanded() && GetRightArm()->CheckIfWeaponTooHeavy(festring(GetPossessivePronoun()+" other wielded item").CStr()));
}


//==========================================================================
//
//  humanoid::AddSpecialStethoscopeInfo
//
//==========================================================================
void humanoid::AddSpecialStethoscopeInfo (felist &Info) const {
  Info.AddEntry(CONST_S("Arm strength: \1W")+GetAttribute(ARM_STRENGTH), LIGHT_GRAY);
  Info.AddEntry(CONST_S("Leg strength: \1W")+GetAttribute(LEG_STRENGTH), LIGHT_GRAY);
  Info.AddEntry(CONST_S("Dexterity: \1W")+GetAttribute(DEXTERITY), LIGHT_GRAY);
  Info.AddEntry(CONST_S("Agility: \1W")+GetAttribute(AGILITY), LIGHT_GRAY);
}


//==========================================================================
//
//  humanoid::GetPairEquipment
//
//==========================================================================
item *humanoid::GetPairEquipment (int I) const {
  switch (I) {
    case RIGHT_WIELDED_INDEX: return GetLeftWielded();
    case LEFT_WIELDED_INDEX: return GetRightWielded();
    case RIGHT_GAUNTLET_INDEX: return GetLeftGauntlet();
    case LEFT_GAUNTLET_INDEX: return GetRightGauntlet();
    case RIGHT_BOOT_INDEX: return GetLeftBoot();
    case LEFT_BOOT_INDEX: return GetRightBoot();
  }
  return 0;
}


//==========================================================================
//
//  humanoid::GetStandVerb
//
//==========================================================================
cfestring& humanoid::GetStandVerb () const {
  if (ForceCustomStandVerb()) return DataBase->StandVerb;
  static festring HasntFeet = CONST_S("crawling");
  static festring Hovering = CONST_S("hovering");
  static festring Swimming = CONST_S("swimming");
  if (StateIsActivated(LEVITATION)) return Hovering;
  if (IsSwimming()) return Swimming;
  return (HasAUsableLeg() ? DataBase->StandVerb : HasntFeet);
}


//==========================================================================
//
//  humanoid::Behead
//
//==========================================================================
head *humanoid::Behead () {
  head *Head = GetHead();
  if (Head) SevereBodyPart(HEAD_INDEX);
  return Head;
}


//==========================================================================
//
//  humanoid::GetRandomApplyBodyPart
//
//==========================================================================
int humanoid::GetRandomApplyBodyPart () const {
  if (RightArmIsUsable()) {
    if (LeftArmIsUsable()) return (RAND_2 ? RIGHT_ARM_INDEX : LEFT_ARM_INDEX);
    return RIGHT_ARM_INDEX;
  }
  if (LeftArmIsUsable()) return LEFT_ARM_INDEX;
  if (GetHead()) return HEAD_INDEX;
  return TORSO_INDEX;
}


//==========================================================================
//
//  humanoid::AddAttributeInfo
//
//==========================================================================
void humanoid::AddAttributeInfo (festring &Entry) const {
  Entry.Resize(45);
  Entry << GetAttribute(ARM_STRENGTH);
  Entry.Resize(48);
  Entry << GetAttribute(LEG_STRENGTH);
  Entry.Resize(51);
  Entry << GetAttribute(DEXTERITY);
  Entry.Resize(54);
  Entry << GetAttribute(AGILITY);
  character::AddAttributeInfo(Entry);
}


//==========================================================================
//
//  humanoid::AddAttackInfo
//
//==========================================================================
void humanoid::AddAttackInfo (felist &List) const {
  if (GetAttackStyle()&USE_ARMS) {
    if (GetRightArm()) GetRightArm()->AddAttackInfo(List);
    if (GetLeftArm()) GetLeftArm()->AddAttackInfo(List);
  }
  festring Entry;
  if (IsUsingLegs()) {
    GetRightLeg()->AddAttackInfo(List);
    GetLeftLeg()->AddAttackInfo(List);
  }
  if (IsUsingHead()) {
    Entry = CONST_S("   bite attack");
    Entry.Resize(50);
    Entry << GetHead()->GetBiteMinDamage() << '-' << GetHead()->GetBiteMaxDamage();
    Entry.Resize(60);
    Entry << int(GetHead()->GetBiteToHitValue());
    Entry.Resize(70);
    Entry << GetHead()->GetBiteAPCost();
    List.AddEntry(Entry, LIGHT_GRAY);
  }
}


//==========================================================================
//
//  humanoid::AddDefenceInfo
//
//==========================================================================
void humanoid::AddDefenceInfo (felist &List) const {
  character::AddDefenceInfo(List);
  if (GetRightArm()) GetRightArm()->AddDefenceInfo(List);
  if (GetLeftArm()) GetLeftArm()->AddDefenceInfo(List);
}


//==========================================================================
//
//  humanoid::DetachBodyPart
//
//==========================================================================
void humanoid::DetachBodyPart () {
  int ToBeDetached;
  switch (game::KeyQuestion(CONST_S("What limb? \1Gl\2eft arm, \1Gr\2ight arm, "
                                    "\1GL\2eft leg, \1GR\2ight leg, \1Hh\2ead?"),
                                    /*KEY_ESC*/REQUIRES_ANSWER,
                                    'L', 'L' + KEY_MOD_SHIFT,
                                    'R', 'R' + KEY_MOD_SHIFT,
                                    'H', 'H' + KEY_MOD_SHIFT,
                                    KEY_ESC, 0))
  {
    case 'L': ToBeDetached = LEFT_ARM_INDEX; break;
    case 'R': ToBeDetached = RIGHT_ARM_INDEX; break;
    case 'L' + KEY_MOD_SHIFT: ToBeDetached = LEFT_LEG_INDEX; break;
    case 'R' + KEY_MOD_SHIFT: ToBeDetached = RIGHT_LEG_INDEX; break;
    case 'H': case 'H' + KEY_MOD_SHIFT: ToBeDetached = HEAD_INDEX; break;
    default: return;
  }
  if (GetBodyPart(ToBeDetached)) {
    item *ToDrop = SevereBodyPart(ToBeDetached);
    SendNewDrawRequest();
    if (ToDrop) {
      GetStack()->AddItem(ToDrop);
      ToDrop->DropEquipment();
    }
    ADD_MESSAGE("Bodypart detached!");
  } else {
    ADD_MESSAGE("That bodypart has already been detached.");
  }
  CheckDeath(CONST_S("removed one of his vital bodyparts"), 0);
}


//==========================================================================
//
//  humanoid::PreProcessForBone
//
//==========================================================================
truth humanoid::PreProcessForBone () {
  for (std::list<sweaponskill*>::iterator i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) (*i)->PreProcessForBone();
  return character::PreProcessForBone();
}


//==========================================================================
//
//  humanoid::FinalProcessForBone
//
//==========================================================================
void humanoid::FinalProcessForBone () {
  character::FinalProcessForBone();
  for (std::list<sweaponskill*>::iterator i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ) {
    boneidmap::iterator BI = game::GetBoneItemIDMap().find(-(*i)->GetID());
    if (BI == game::GetBoneItemIDMap().end()) {
      std::list<sweaponskill*>::iterator Dirt = i++;
      SWeaponSkill.erase(Dirt);
    } else {
      (*i)->SetID(BI->second);
      ++i;
    }
  }
}


//==========================================================================
//
//  humanoid::EnsureCurrentSWeaponSkillIsCorrect
//
//==========================================================================
void humanoid::EnsureCurrentSWeaponSkillIsCorrect (sweaponskill *&Skill, citem *Wielded) {
  if (Wielded) {
    if (!Skill || !Skill->IsSkillOf(Wielded)) {
      if (Skill) EnsureCurrentSWeaponSkillIsCorrect(Skill, 0);
      std::list<sweaponskill*>::iterator i;
      for (i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) {
        if((*i)->IsSkillOf(Wielded)) {
          Skill = *i;
          return;
        }
      }
      for (idholder *I = Wielded->GetCloneMotherID(); I; I = I->Next) {
        for (i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) {
          if ((*i)->IsSkillOfCloneMother(Wielded, I->ID)) {
            Skill = new sweaponskill(**i);
            Skill->SetID(Wielded->GetID());
            SWeaponSkill.push_back(Skill);
            return;
          }
        }
      }
      Skill = new sweaponskill(Wielded);
      SWeaponSkill.push_back(Skill);
    }
  } else if (Skill) {
    if (!Skill->GetHits() && (CurrentRightSWeaponSkill != Skill || CurrentLeftSWeaponSkill != Skill)) {
      for (std::list<sweaponskill*>::iterator i = SWeaponSkill.begin(); i != SWeaponSkill.end(); ++i) {
        if (*i == Skill) {
          delete *i;
          SWeaponSkill.erase(i);
          break;
        }
      }
    }
    Skill = 0;
  }
}


//==========================================================================
//
//  humanoid::GetSumOfAttributes
//
//==========================================================================
int humanoid::GetSumOfAttributes () const {
  return character::GetSumOfAttributes()+GetAttribute(LEG_STRENGTH)+GetAttribute(DEXTERITY);
}


//==========================================================================
//
//  humanoid::CheckConsume
//
//==========================================================================
truth humanoid::CheckConsume (cfestring &Verb) const {
  if (!HasHead()) {
    if (IsPlayer()) ADD_MESSAGE("You need a head to %s.", Verb.CStr());
    return false;
  }
  return character::CheckConsume(Verb);
}


//==========================================================================
//
//  humanoid::CanConsume
//
//==========================================================================
truth humanoid::CanConsume (material *Material) const {
  return character::CanConsume(Material) && HasHead();
}


//==========================================================================
//
//  humanoid::StayOn
//
//==========================================================================
void humanoid::StayOn (liquid *Liquid) {
  if (IsFlying()) return;
  truth Standing = false;
  if (GetRightLeg()) {
    GetRightLeg()->StayOn(Liquid);
    Standing = true;
  }
  if (IsEnabled() && GetLeftLeg()) {
    GetLeftLeg()->StayOn(Liquid);
    Standing = true;
  }
  if (!Standing) {
    bodypart *BodyPart[MAX_BODYPARTS];
    int Index = 0;
    for (int c = 0; c < BodyParts; ++c) if (GetBodyPart(c)) BodyPart[Index++] = GetBodyPart(c);
    BodyPart[RAND_N(Index)]->StayOn(Liquid);
  }
}


//==========================================================================
//
//  humanoid::CreateZombie
//
//==========================================================================
character *humanoid::CreateZombie () const {
  if (!TorsoIsAlive()) return 0;
  humanoid *Zombie = zombie::Spawn();
  int c;
  for (c = 0; c < BodyParts; ++c) {
    bodypart *BodyPart = GetBodyPart(c);
    if (!BodyPart) {
      BodyPart = SearchForOriginalBodyPart(c);
      if (BodyPart) {
        BodyPart->RemoveFromSlot();
        BodyPart->SendToHell();
      }
    }
    if (BodyPart) {
      bodypart *ZombieBodyPart = Zombie->GetBodyPart(c);
      if (!ZombieBodyPart) ZombieBodyPart = Zombie->CreateBodyPart(c);
      material *M = BodyPart->GetMainMaterial()->Duplicate();
      M->SetSpoilCounter(2000+RAND_N(1000));
      M->SetSkinColor(Zombie->GetSkinColor());
      ZombieBodyPart->ChangeMainMaterial(M);
      ZombieBodyPart->CopyAttributes(BodyPart);
    } else if (!Zombie->BodyPartIsVital(c)) {
      bodypart *ZombieBodyPart = Zombie->GetBodyPart(c);
      if (ZombieBodyPart) {
        ZombieBodyPart->RemoveFromSlot();
        ZombieBodyPart->SendToHell();
      }
    }
  }
  for (c = 0; c < Zombie->AllowedWeaponSkillCategories; ++c) {
    Zombie->CWeaponSkill[c] = CWeaponSkill[c];
  }
  Zombie->SWeaponSkill.resize(SWeaponSkill.size());
  std::list<sweaponskill*>::iterator i1 = Zombie->SWeaponSkill.begin();
  std::list<sweaponskill*>::const_iterator i2 = SWeaponSkill.begin();
  for (; i2 != SWeaponSkill.end(); ++i1, ++i2) *i1 = new sweaponskill(**i2);
  memmove(Zombie->BaseExperience, BaseExperience, BASE_ATTRIBUTES*sizeof(*BaseExperience));
  Zombie->CalculateAll();
  Zombie->RestoreHP();
  Zombie->RestoreStamina();
  static_cast<zombie *>(Zombie)->SetDescription(GetZombieDescription());
  Zombie->GenerationDanger = GenerationDanger;
  return Zombie;
}


//==========================================================================
//
//  humanoid::LeprosyHandler
//
//==========================================================================
void humanoid::LeprosyHandler () {
  if (IsImmuneToLeprosy()) return;
  if (!RAND_N(1000*GetAttribute(ENDURANCE))) {
    DropRandomNonVitalBodypart();
  }
  if (!game::IsInWilderness()) {
    for (int d = 0; d < GetNeighbourSquares(); ++d) {
      lsquare *Square = GetNeighbourLSquare(d);
      if (Square && Square->GetCharacter()) {
        Square->GetCharacter()->TryToInfectWithLeprosy(this);
      }
    }
  }
  character::LeprosyHandler();
}


//==========================================================================
//
//  humanoid::DropRandomNonVitalBodypart
//
//==========================================================================
void humanoid::DropRandomNonVitalBodypart () {
  int BodyPartIndexToDrop = GetRandomNonVitalBodyPart();
  if (BodyPartIndexToDrop != NONE_INDEX) DropBodyPart(BodyPartIndexToDrop);
}


//==========================================================================
//
//  humanoid::DropBodyPart
//
//==========================================================================
void humanoid::DropBodyPart (int Index) {
  if (!GetBodyPart(Index)->IsAlive()) return;
  festring NameOfDropped = GetBodyPart(Index)->GetBodyPartName();
  item *Dropped = SevereBodyPart(Index);
  if (Dropped) {
    GetStack()->AddItem(Dropped);
    Dropped->DropEquipment();
    if (IsPlayer()) {
      ADD_MESSAGE("You feel very ill. Your %s snaps off.", NameOfDropped.CStr());
      game::AskForEscPress(CONST_S("Bodypart severed!"));
      DeActivateVoluntaryAction();
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("Suddenly %s's %s snaps off.", CHAR_NAME(DEFINITE), NameOfDropped.CStr());
    }
  } else {
    if (IsPlayer()) {
      ADD_MESSAGE("You feel very ill. Your %s disappears.", NameOfDropped.CStr());
      game::AskForEscPress(CONST_S("Bodypart destroyed!"));
      DeActivateVoluntaryAction();
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("Suddenly %s's %s disappears.", CHAR_NAME(DEFINITE), NameOfDropped.CStr());
    }
  }
}


//==========================================================================
//
//  humanoid::DuplicateEquipment
//
//==========================================================================
void humanoid::DuplicateEquipment (character *Receiver, feuLong Flags) {
  character::DuplicateEquipment(Receiver, Flags);
  EnsureCurrentSWeaponSkillIsCorrect(CurrentRightSWeaponSkill, GetRightWielded());
  EnsureCurrentSWeaponSkillIsCorrect(CurrentLeftSWeaponSkill, GetLeftWielded());
}


//==========================================================================
//
//  humanoid::GetAttributeAverage
//
//==========================================================================
int humanoid::GetAttributeAverage () const {
  return GetSumOfAttributes()/9;
}


//==========================================================================
//
//  humanoid::CanVomit
//
//==========================================================================
truth humanoid::CanVomit () const {
  return HasHead() && character::CanVomit();
}


//==========================================================================
//
//  humanoid::CheckApply
//
//==========================================================================
truth humanoid::CheckApply () const {
  if (!character::CheckApply()) return false;
  if (!HasAUsableArm()) {
    ADD_MESSAGE("You need a usable arm to apply.");
    return false;
  }
  return true;
}


//==========================================================================
//
//  humanoid::IsTransparent
//
//==========================================================================
truth humanoid::IsTransparent () const {
  return character::IsTransparent() || !(GetRightLeg() || GetLeftLeg());
}


//==========================================================================
//
//  humanoid::ModifySituationDanger
//
//==========================================================================
void humanoid::ModifySituationDanger (double &Danger) const {
  character::ModifySituationDanger(Danger);
  switch (GetUsableArms()) {
    case 0: Danger *= 10;
    case 1: Danger *= 2;
  }
  switch (GetUsableLegs()) {
    case 0: Danger *= 10;
    case 1: Danger *= 2;
  }
}


//==========================================================================
//
//  humanoid::RandomizeTryToUnStickBodyPart
//
//==========================================================================
int humanoid::RandomizeTryToUnStickBodyPart (feuLong PossibleBodyParts) const {
  int Possible = 0, PossibleArray[3];
  if (RightArmIsUsable() && 1<<RIGHT_ARM_INDEX&PossibleBodyParts) PossibleArray[Possible++] = RIGHT_ARM_INDEX;
  if (LeftArmIsUsable() && 1<<LEFT_ARM_INDEX&PossibleBodyParts) PossibleArray[Possible++] = LEFT_ARM_INDEX;
  if (Possible) return PossibleArray[RAND_N(Possible)];
  if (RightLegIsUsable() && 1<<RIGHT_LEG_INDEX&PossibleBodyParts) PossibleArray[Possible++] = RIGHT_LEG_INDEX;
  if (LeftLegIsUsable() && 1<<LEFT_LEG_INDEX&PossibleBodyParts) PossibleArray[Possible++] = LEFT_LEG_INDEX;
  if (Possible) return PossibleArray[RAND_N(Possible)];
  if (GetHead() && 1<<HEAD_INDEX&PossibleBodyParts) return HEAD_INDEX;
  if (GetGroin() && 1<<GROIN_INDEX&PossibleBodyParts) PossibleArray[Possible++] = GROIN_INDEX;
  if (1<<TORSO_INDEX&PossibleBodyParts) PossibleArray[Possible++] = TORSO_INDEX;
  return (Possible ? PossibleArray[RAND_N(Possible)] : NONE_INDEX);
}


//==========================================================================
//
//  humanoid::HasAUsableArm
//
//==========================================================================
truth humanoid::HasAUsableArm () const {
  arm *R = GetRightArm(), *L = GetLeftArm();
  return (R && R->IsUsable()) || (L && L->IsUsable());
}


//==========================================================================
//
//  humanoid::HasAUsableLeg
//
//==========================================================================
truth humanoid::HasAUsableLeg () const {
  leg *R = GetRightLeg(), *L = GetLeftLeg();
  return (R && R->IsUsable()) || (L && L->IsUsable());
}


//==========================================================================
//
//  humanoid::HasTwoUsableLegs
//
//==========================================================================
truth humanoid::HasTwoUsableLegs () const {
  leg *R = GetRightLeg(), *L = GetLeftLeg();
  return R && R->IsUsable() && L && L->IsUsable();
}


//==========================================================================
//
//  humanoid::CanAttackWithAnArm
//
//==========================================================================
truth humanoid::CanAttackWithAnArm () const {
  arm *R = GetRightArm();
  if (R && R->GetDamage()) return true;
  arm *L = GetLeftArm();
  return L && L->GetDamage();
}


//==========================================================================
//
//  humanoid::RightArmIsUsable
//
//==========================================================================
truth humanoid::RightArmIsUsable () const {
  arm *A = GetRightArm();
  return A && A->IsUsable();
}


//==========================================================================
//
//  humanoid::LeftArmIsUsable
//
//==========================================================================
truth humanoid::LeftArmIsUsable () const {
  arm *A = GetLeftArm();
  return A && A->IsUsable();
}


//==========================================================================
//
//  humanoid::RightLegIsUsable
//
//==========================================================================
truth humanoid::RightLegIsUsable () const {
  leg *L = GetRightLeg();
  return L && L->IsUsable();
}


//==========================================================================
//
//  humanoid::LeftLegIsUsable
//
//==========================================================================
truth humanoid::LeftLegIsUsable () const {
  leg *L = GetLeftLeg();
  return L && L->IsUsable();
}


//==========================================================================
//
//  humanoid::AllowUnconsciousness
//
//==========================================================================
truth humanoid::AllowUnconsciousness () const {
  return (DataBase->AllowUnconsciousness && TorsoIsAlive() && BodyPartIsVital(HEAD_INDEX));
}


//==========================================================================
//
//  humanoid::CanChokeOnWeb
//
//==========================================================================
truth humanoid::CanChokeOnWeb (web *Web) const {
  return CanChoke() && Web->IsStuckToBodyPart(HEAD_INDEX);
}


//==========================================================================
//
//  humanoid::BrainsHurt
//
//==========================================================================
truth humanoid::BrainsHurt () const {
  head *Head = GetHead();
  return !Head || Head->IsBadlyHurt();
}


//==========================================================================
//
//  humanoid::GetRunDescriptionLine
//
//==========================================================================
cchar *humanoid::GetRunDescriptionLine (int I) const {
  if (!GetRunDescriptionLineOne().IsEmpty()) {
    return !I ? GetRunDescriptionLineOne().CStr() : GetRunDescriptionLineTwo().CStr();
  }

  if (IsFlying()) {
    return !I ? "Flying" : "very fast";
  }

  if (IsSwimming()) {
    if (IsPlayer() && game::IsInWilderness() && game::PlayerHasBoat()) {
      return !I ? "Sailing" : " very fast";
    }
    if (!GetRightArm() && !GetLeftArm() && !GetRightLeg() && !GetLeftLeg()) {
      return !I ? "Floating" : "ahead fast";
    }
    return !I ? "Swimming" : "very fast";
  }

  if (!GetRightLeg() && !GetLeftLeg()) return !I ? "Rolling" : "very fast";
  if (!GetRightLeg() || !GetLeftLeg()) return !I ? "Hopping" : "very fast";
  return !I ? "Running" : "";
}


//==========================================================================
//
//  humanoid::GetNormalDeathMessage
//
//==========================================================================
cchar *humanoid::GetNormalDeathMessage () const {
  if (BodyPartIsVital(HEAD_INDEX) && (!GetHead() || GetHead()->GetHP() <= 0)) return "beheaded @k";
  if (BodyPartIsVital(GROIN_INDEX) && (!GetGroin() || GetGroin()->GetHP() <= 0)) return "killed @bkp dirty attack below the belt";
  return "killed @k";
}


//==========================================================================
//
//  humanoid::ApplySpecialAttributeBonuses
//
//==========================================================================
void humanoid::ApplySpecialAttributeBonuses () {
  if (GetHead()) {
    AttributeBonus[CHARISMA] -= GetHead()->CalculateScarAttributePenalty(GetAttribute(CHARISMA, false));
  } else {
    AttributeBonus[CHARISMA] -= GetAttribute(CHARISMA, false)-1;
  }
}


//==========================================================================
//
//  humanoid::MindWormCanPenetrateSkull
//
//==========================================================================
truth humanoid::MindWormCanPenetrateSkull (mindworm *) const {
  /*old code
  if (GetHelmet()) {
    if (RAND_N(102) > GetHelmet()->GetCoverPercentile()) return RAND_2;
  }
  return RAND_2;
  */
  // comm. fork
  if (GetHelmet()) {
    if (RAND_N(100) < GetHelmet()->GetCoverPercentile()) {
      return false;
    }
  }
  return true;
}


//==========================================================================
//
//  humanoid::HasSadistWeapon
//
//==========================================================================
truth humanoid::HasSadistWeapon () const {
  arm *Right = GetRightArm(), *Left = GetLeftArm();
  return (Right && Right->HasSadistWeapon()) || (Left && Left->HasSadistWeapon());
}


//==========================================================================
//
//  humanoid::HasSadistAttackMode
//
//==========================================================================
truth humanoid::HasSadistAttackMode () const {
  return HasSadistWeapon() || IsUsingLegs();
}


//==========================================================================
//
//  humanoid::SpecialBiteEffect
//
//==========================================================================
truth humanoid::SpecialBiteEffect (character *Victim, v2 HitPos, int BodyPartIndex, int Direction, truth BlockedByArmour, truth Critical, int DoneDamage) {
  if (StateIsActivated(VAMPIRISM)) {
    if (!BlockedByArmour && Victim->IsWarmBlooded() && !(RAND_2 || Critical) && !Victim->AllowSpoil()) {
      /*
      if (Victim->IsHumanoid()) Victim->BeginTemporaryState(VAMPIRISM, 1000+RAND_N(500)); // Randomly instigate vampirism
      if (Victim->IsPlayer() || IsPlayer() || Victim->CanBeSeenByPlayer() || CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s drains some precious lifeblood from %s!", CHAR_DESCRIPTION(DEFINITE), Victim->CHAR_DESCRIPTION(DEFINITE));
      }
      return Victim->ReceiveBodyPartDamage(this, 8+(RAND_N(9)), DRAIN, BodyPartIndex, Direction);
      */
      if (IsPlayer()) {
        ADD_MESSAGE("You drain some precious lifeblood from %s!", Victim->CHAR_DESCRIPTION(DEFINITE));
      } else if (Victim->IsPlayer() || Victim->CanBeSeenByPlayer() || CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s drains some precious lifeblood from %s!", CHAR_DESCRIPTION(DEFINITE), Victim->CHAR_DESCRIPTION(DEFINITE));
      }

      if (Victim->IsHumanoid() && !Victim->StateIsActivated(VAMPIRISM) &&
          !Victim->StateIsActivated(LYCANTHROPY) && !Victim->StateIsActivated(DISEASE_IMMUNITY))
      {
        Victim->BeginTemporaryState(VAMPIRISM, 2000 + RAND_N(500));
      }

      // HP recieved is about half the damage done; against werewolves this is full
      int DrainDamage = (DoneDamage>>1)+1;
      if (Victim->StateIsActivated(LYCANTHROPY)) DrainDamage = DoneDamage+1;

      // To perpetuate vampirism, simply keep doing drain attacks
      BeginTemporaryState(VAMPIRISM, 50*DrainDamage);
      if (IsPlayer()) game::DoEvilDeed(10);

      return Victim->ReceiveBodyPartDamage(this, DrainDamage, DRAIN, BodyPartIndex, Direction);
    }
  }
  return false;
}


//==========================================================================
//
//  humanoid::TryToHardenItem
//
//  used by smith and tailor.
//  all necessary material checks should be done by the caller.
//
//==========================================================================
truth humanoid::TryToHardenItem (itemvector &Item) {
  if (Item.size() == 0) return false;

  if (!Item[0]->IsMaterialChangeable()) {
    ADD_MESSAGE("I cannot harden your %s!",
                Item[0]->CHAR_NAME(UNARTICLED|DEFINITE|(Item.size() == 1 ? 0 : PLURAL)));
    return false;
  }

  if (Item[0]->HandleInPairs() && Item.size() == 1) {
    festring msg;
    msg << "Only one " << Item[0]->CHAR_NAME(UNARTICLED) << " will be altered.";
    ADD_MESSAGE("%s", msg.CStr());
    if (!game::TruthQuestion(msg + "\nStill continue?")) return false;
  }

  /*
  if (Item.size() == 1) {
    ADD_MESSAGE("Suddenly your %s is consumed in roaring magical flames.", Item[0]->CHAR_NAME(UNARTICLED));
  } else {
    ADD_MESSAGE("Suddenly your %s are consumed in roaring magical flames.", Item[0]->CHAR_NAME(PLURAL));
  }
  */

  int Config = Item[0]->GetMainMaterial()->GetHardenedMaterial(Item[0]);

  if (!Config) {
    /* Should not be possible */
    if (Item.size() == 1) {
      ADD_MESSAGE("But it is already as hard as it can get.");
    } else {
      ADD_MESSAGE("But they are already as hard as they can get.");
    }
    return false;
  }

  material *TempMaterial = MAKE_MATERIAL(Config);
  int Intelligence = GetAttribute(INTELLIGENCE);

  if (TempMaterial->GetIntelligenceRequirement() > Intelligence) {
    delete TempMaterial;
    ADD_MESSAGE("I am not skilled enough to harden %s.", Item.size() == 1 ? "it" : "them");
    return false;
  }

  for (int NewConfig = TempMaterial->GetHardenedMaterial(Item[0]), c = 1; NewConfig;
       NewConfig = TempMaterial->GetHardenedMaterial(Item[0]), ++c)
  {
    material *NewMaterial = MAKE_MATERIAL(NewConfig);
    if (NewMaterial->GetIntelligenceRequirement() <= Intelligence - c * 5) {
      delete TempMaterial;
      TempMaterial = NewMaterial;
    } else {
      break;
    }
  }

  material *MainMaterial = Item[0]->GetMainMaterial();
  material *SecondaryMaterial = Item[0]->GetSecondaryMaterial();

  // let the caller tell about success
  if (Item.size() == 1) {
    //ADD_MESSAGE("As the fire dies out it looks much harder.");
    if (SecondaryMaterial && SecondaryMaterial->IsSameAs(MainMaterial)) {
      Item[0]->ChangeSecondaryMaterial(TempMaterial->SpawnMore());
    }
    Item[0]->ChangeMainMaterial(TempMaterial);
  } else {
    //ADD_MESSAGE("As the fire dies out they look much harder.");
    if (SecondaryMaterial && SecondaryMaterial->IsSameAs(MainMaterial)) {
      for (uInt c = 0; c < Item.size(); ++c) {
        Item[c]->ChangeSecondaryMaterial(TempMaterial->SpawnMore());
      }
    }
    Item[0]->ChangeMainMaterial(TempMaterial);
    for (size_t c = 1; c < Item.size(); c += 1) {
      Item[c]->ChangeMainMaterial(TempMaterial->SpawnMore());
    }
  }

  // no, smith and tailor will not gain expirience by doing this!
  //EditExperience(INTELLIGENCE, 300, 1 << 12);
  return true;
}


#endif
