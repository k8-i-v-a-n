#ifdef HEADER_PHASE
CHARACTER(child, humanoid)
{
public:
  virtual truth MoveRandomly (truth allowInterestingItems=true) override;
  virtual truth IsKing () const override;
  virtual truth TryQuestTalks () override;
  virtual truth MustBeRemovedFromBone() const override;
};


#else

truth child::IsKing () const { return GetConfig() == KING; }


truth child::MustBeRemovedFromBone () const {
  return (!IsEnabled() || GetConfig() == KING);
}


truth child::MoveRandomly (truth allowInterestingItems) {
  return (GetConfig() == KING ? MoveRandomlyInRoom(allowInterestingItems)
                              : humanoid::MoveRandomly(allowInterestingItems));
}


truth child::TryQuestTalks () {
  if (GetConfig() != KING || GetRelation(PLAYER) == HOSTILE || GetTeam() == PLAYER->GetTeam()) {
    return false;
  }

  owterrain *terra = game::goblinfortPOI();
  if (GetDungeon()->GetIndex() != game::GetPOIConfigIndex(terra) ||
        GetLevel()->GetIndex() != KING_LEVEL)
  {
    return false;
  }

  // Prince Artorius will follow you back to Aslona.
  ADD_MESSAGE("%s looks at you with hope. \"I want to go home. Will you take me home, %s?\"",
              CHAR_DESCRIPTION(DEFINITE), PLAYER->GetAssignedName().CStr());
  ChangeTeam(PLAYER->GetTeam());

  return true;
}


/*
void child::BeTalkedTo () {
  owterrain *terra = game::goblinfortPOI();
  if (terra) {
    if (GetConfig() == KING && GetRelation(PLAYER) != HOSTILE &&
        GetTeam() != PLAYER->GetTeam() &&
        GetDungeon()->GetIndex() == game::GetPOIConfigIndex(terra) &&
        GetLevel()->GetIndex() == KING_LEVEL)
    {
      // Prince Artorius will follow you back to Aslona.
      ADD_MESSAGE("%s looks at you with hope. \"I want to go home. Will you take me home, %s?\"",
                  CHAR_DESCRIPTION(DEFINITE), PLAYER->GetAssignedName().CStr());
      ChangeTeam(PLAYER->GetTeam());
      return;
    }
  }

  character::BeTalkedTo();
}
*/


#endif
