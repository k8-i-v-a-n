#ifdef HEADER_PHASE
CHARACTER(communist, humanoid)
{
public:
  virtual truth MoveRandomly (truth allowInterestingItems=true) override;
  virtual truth TryQuestTalks () override;
  virtual truth BoundToUse (citem *, int) const override;

protected:
  virtual truth ShowClassDescription () const override;
};


#else


truth communist::ShowClassDescription () const { return (GetAssignedName() != "Ivan"); }


truth communist::TryQuestTalks () {
  if (GetRelation(PLAYER) != HOSTILE && GetTeam() != PLAYER->GetTeam() &&
      PLAYER->GetRelativeDanger(this, true) > 0.1)
  {
    ADD_MESSAGE("%s seems to be very friendly. \"%s make good communist. %s go with %s!\"",
                CHAR_DESCRIPTION(DEFINITE), PLAYER->GetAssignedName().CStr(),
                CHAR_NAME(UNARTICLED), PLAYER->GetAssignedName().CStr());
    team *oldteam = GetTeam();
    while (oldteam->GetMembers() > 0) {
      for (auto &it : oldteam->GetMember()) {
        it->ChangeTeam(PLAYER->GetTeam());
        //if (GetTeam()->GetMembers() == 1) break; // only Ivan left in Party
        break;
      }
    }
    //ChangeTeam(PLAYER->GetTeam());
    return true;
  } else if (GetTeam() != PLAYER->GetTeam() && !RAND_N(5)) {
    ADD_MESSAGE("\"You weak. Learn killing and come back.\"");
    return true;
  }
  return false;
}


truth communist::MoveRandomly (truth allowInterestingItems) {
  switch (RAND_N(1000)) {
    case 0:
      if (CanBeSeenByPlayer()) ADD_MESSAGE("%s engraves something to the ground.", CHAR_NAME(UNARTICLED));
      Engrave(CONST_S("The bourgeois is a bourgeois -- for the benefit of the working class."));
      return true;
    case 1:
      if (CanBeSeenByPlayer()) ADD_MESSAGE("%s engraves something to the ground.", CHAR_NAME(UNARTICLED));
      Engrave(CONST_S("Proletarians of all countries, unite!"));
      return true;
    case 2:
      if (CanBeSeenByPlayer()) ADD_MESSAGE("%s engraves something to the ground.", CHAR_NAME(UNARTICLED));
      Engrave(CONST_S("Capital is therefore not only personal; it is a social power."));
      return true;
    default:
      return character::MoveRandomly(allowInterestingItems);
  }
}


truth communist::BoundToUse (citem* Item, int I) const {
  return (Item && Item->IsGorovitsFamilyRelic() && Item->IsInCorrectSlot(I));
}


#endif
