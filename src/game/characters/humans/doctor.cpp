#ifdef HEADER_PHASE
CHARACTER(doctor, humanoid)
{
public:
  virtual truth TryQuestTalks () override;
};


#else


truth doctor::TryQuestTalks () {
  truth res = false;

  if (GetRelation(PLAYER) == HOSTILE) {
    ADD_MESSAGE("\"Bacillus! I surgically detach your every limb!\"");
    return true;
  }

  for (int c = 0; c < PLAYER->GetBodyParts(); ++c) {
    if (!PLAYER->GetBodyPart(c) && PLAYER->CanCreateBodyPart(c)) {
      truth HasOld = false;
      for (auto &it : PLAYER->GetOriginalBodyPartID(c)) {
        bodypart *OldBodyPart = static_cast<bodypart *>(PLAYER->SearchForItem(it));
        if (OldBodyPart) {
          HasOld = true;
          sLong Price = (2*GetAttribute(CHARISMA)) < PLAYER->GetAttribute(CHARISMA) ? 5 : (2*GetAttribute(CHARISMA)-PLAYER->GetAttribute(CHARISMA));
          if (PLAYER->GetMoney() >= Price) {
            if (GetAttribute(INTELLIGENCE) < OldBodyPart->GetMainMaterial()->GetIntelligenceRequirement() && !OldBodyPart->CanRegenerate()) {
              ADD_MESSAGE("\"I no smart enough to put back bodyparts made of %s, especially not your severed %s.\"", OldBodyPart->GetMainMaterial()->GetName(false, false).CStr(), PLAYER->GetBodyPartName(c).CStr());
            } else {
              festring msg;
              msg << "\"I could put your old \1Y" << PLAYER->GetBodyPartName(c);
              msg << "\2 back in exchange for \1Y" << Price << "\2 gold pieces.\"";
              ADD_MESSAGE("%s, yes, yes.", msg.CStr());
              if (game::TruthQuestion(msg + "\nDo you agree?")) {
                OldBodyPart->SetHP(1);
                PLAYER->SetMoney(PLAYER->GetMoney()-Price);
                SetMoney(GetMoney()+Price);
                OldBodyPart->RemoveFromSlot();
                PLAYER->AttachBodyPart(OldBodyPart);
                return true;
              }
            }
          } else {
            res = true;
            ADD_MESSAGE("\"Oh my! Your %s severed! Help yourself and get \1Y%dgp\2 and Party help you too.\"",
                        PLAYER->GetBodyPartName(c).CStr(), Price);
          }
        }
      }
      // okay, doctors cannot summon limbs
      if (!HasOld) {
        res = true;
        ADD_MESSAGE("\"You don't have your original %s with you. A priest or priestess "
                    "might summon you a new one with the right rituals.\"", PLAYER->GetBodyPartName(c).CStr());
      }
    }
  }

  // remove limb looks like this:
  // first, select limb to detach (ask player this)
  // then detach limb
  // then make player scream, according to endurance level. above 25 endurance, no scream, nor panic
  // if scream, then panic for a duration according to (25 - Endurance)*4 or zero
  // if state confused is activated, then no scream, no panic (makes vodka handy)
  // hand over 5 gold
  if (PLAYER->GetMoney() >= 5) {
    res = true;
    festring msg = CONST_S("\"I can surgically remove one of your limbs in exchange for \1Y5\2 gold.");
    ADD_MESSAGE("%s Flat rate, genuine bargain!\"", msg.CStr());
    if (game::TruthQuestion(msg + "\"\nDo you agree?")) {
      PLAYER->SetMoney(PLAYER->GetMoney()-5);
      SetMoney(GetMoney()+5);
      PLAYER->SurgicallyDetachBodyPart();
      if ((PLAYER->GetAttribute(ENDURANCE) <= 24) && !PLAYER->StateIsActivated(CONFUSED)) {
        PLAYER->BeginTemporaryState(PANIC, 500+RAND_N(4)*(25-PLAYER->GetAttribute(ENDURANCE)));
        ADD_MESSAGE("You let out a gut-wrenching scream of agony!");
      }
      return true;
    }
  }

  // cure poison
  if (PLAYER->TemporaryStateIsActivated(POISONED)) {
    res = true;
    sLong Price = GetAttribute(CHARISMA) < PLAYER->GetAttribute(CHARISMA) ? 5 : (GetAttribute(CHARISMA)-PLAYER->GetAttribute(CHARISMA));
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"You seem to be rather ill, yes yes. I give you small dose "
                  "of antidote for \1Y%d\2 gold pieces.\"", Price);
      festring msg;
      msg << "\"I give you small dose of \1Gantidote\2 for \1Y" << Price << "\2 gold pieces.\"";
      if (game::TruthQuestion(msg + "\nDo you agree?")) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(POISONED);
        PLAYER->SetMoney(PLAYER->GetMoney()-Price);
        SetMoney(GetMoney()+Price);
        return true;
      }
    } else {
      ADD_MESSAGE("\"You seem to be rather ill. Get \1Y%d\2 gold pieces and I fix that.\"", Price);
    }
  }

  // cure leprosy
  if (PLAYER->TemporaryStateIsActivated(LEPROSY)) {
    res = true;
    sLong Price = GetAttribute(CHARISMA) < PLAYER->GetAttribute(CHARISMA) ? 5 : (GetAttribute(CHARISMA)-PLAYER->GetAttribute(CHARISMA));
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"You seem to have contracted vile disease of leprosy, yes yes. "
                  "I can give you small dose of medicince for \1Y%d\2 gold pieces.\"",
                  Price);
      festring msg;
      msg << "I can cure your \1Glycantropy\2 for \1Y" << Price << "\2 gold pieces.\"";
      if (game::TruthQuestion(msg + "\nDo you agree?")) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(LEPROSY);
        PLAYER->SetMoney(PLAYER->GetMoney()-Price);
        SetMoney(GetMoney()+Price);
        return true;
      }
    } else {
      ADD_MESSAGE("\"You seem to be falling apart. Get \1Y%d\2 gold pieces and I fix that.\"",
                  Price);
    }
  }

  // cure lycanthropy
  if (PLAYER->TemporaryStateIsActivated(LYCANTHROPY)) {
    res = true;
    sLong Price = GetAttribute(CHARISMA) < PLAYER->GetAttribute(CHARISMA) ? 5 : (GetAttribute(CHARISMA)-PLAYER->GetAttribute(CHARISMA));
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"You seem to be turning into a werewolf quite frequently. Party must crush epidemic, special priority! If you wish to pay \1Y%dgp\2, I can remove the canine blood from your veins with acupucnture, yes yes.\"", Price);
      festring msg;
      msg << "\"I can cure you from \1Gwerewolfitis\2 for \1Y" << Price << "\2 gold pieces.";
      if (game::TruthQuestion(msg + "\nDo you agree?")) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(LYCANTHROPY);
        PLAYER->SetMoney(PLAYER->GetMoney()-Price);
        SetMoney(GetMoney()+Price);
        return true;
      }
    } else {
      ADD_MESSAGE("\"You seem to be lycanthropic. I might be able to do something for that but I need \1Y%dgp\2 for the Party first.\"", Price);
    }
  }

  // cure vampirism
  if (PLAYER->TemporaryStateIsActivated(VAMPIRISM)) {
    res = true;
    sLong Price = GetAttribute(CHARISMA) < PLAYER->GetAttribute(CHARISMA) ? 5 : (GetAttribute(CHARISMA)-PLAYER->GetAttribute(CHARISMA));
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"You seem to have addiction to drinking blood. Party must crush epidemic, special priority! If you wish to pay \1Y%dgp\2, I can remove vampiric urges with acupucnture, yes yes.\"", Price);
      festring msg;
      msg << "\"I can cure you from \1Gvampirism\2 for \1Y" << Price << "\2 gold pieces.";
      if (game::TruthQuestion(msg + "\nDo you agree?")) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(VAMPIRISM);
        PLAYER->SetMoney(PLAYER->GetMoney()-Price);
        SetMoney(GetMoney()+Price);
        return true;
      }
    } else {
      ADD_MESSAGE("\"You seem to be vampiric. I might be able to do something for that but I need \1Y%dgp\2 for the Party first.\"", Price);
    }
  }

  return res;
}


#endif
