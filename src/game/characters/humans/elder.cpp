#ifdef HEADER_PHASE
CHARACTER(elder, humanoid)
{
public:
  elder ();
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void GetAICommand () override;
  virtual void CreateBodyParts (int) override;
  virtual truth TryQuestTalks () override;

protected:
  truth HasBeenSpokenTo;
};


#else


elder::elder () : HasBeenSpokenTo(false) {}


void elder::Save (outputfile &SaveFile) const {
  humanoid::Save(SaveFile);
  SaveFile << HasBeenSpokenTo;
}


void elder::Load (inputfile &SaveFile) {
  humanoid::Load(SaveFile);
  SaveFile >> HasBeenSpokenTo;
}


void elder::CreateBodyParts (int SpecialFlags) {
  for (int c = 0; c < BodyParts; ++c) {
    if (c != LEFT_LEG_INDEX) CreateBodyPart(c, SpecialFlags); else SetBodyPart(LEFT_LEG_INDEX, 0);
  }
}


void elder::GetAICommand () {
  // select a place to guide the tourists to
  if (!RAND_N(10)) {
    v2 Where = GetLevel()->GetRandomSquare();
    if (Where != ERROR_V2) SetGoingTo(Where);
  }
  humanoid::GetAICommand();
}


truth elder::TryQuestTalks () {
  #if 0
  ConLogf("SPOKEN=%d; TweraifIsFree=%d; FFST=%d; liberator=%d",
          (int)HasBeenSpokenTo, (int)game::TweraifIsFree(),
          game::GetFreedomStoryState(), (int)game::GetLiberator());
  #endif
  if (!HasBeenSpokenTo && game::TweraifIsFree() &&
      (game::GetFreedomStoryState() == 0 || game::GetFreedomStoryState() == 2) &&
      GetRelation(PLAYER) != HOSTILE)
  {
    game::TextScreen(
      CONST_S("\"My boy, my wonderful boy! From the very day I found you,\n"
              "I knew there was something special in you, something even\n"
              "the accursed hippos couldn't spoil. And now you have saved us\n"
              "from valpurian clutches and given us a chance at freedom!\n"
              "Thank you so very, very much.\"\n\n"
              "\"Alas, I'm afraid Tweraif is not yet out of the proverbial woods.\n"
              "We are few and the Attnamese army is massive. Their battleships\n"
              "will be ready once the winter ends and the ice thaws, and they will\n"
              "not hesitate to bring their tyranny back. I still don't get why they\n"
              "love those bananas so much.\"\n\n"
              "\"We have no hope to defeat them in a fight, so fight them we shan't.\""));
    game::TextScreen(
      CONST_S("\"Let me tell you a story, or a myth if you will.\"\n\n"
              "\"Once upon a time, there was a town. No one could find the town\n"
              "unless they already knew where it was, and on one could enter\n"
              "uninvited. The town was called Mondedr and it was concealed\n"
              "from the world by the power of Cleptia. It was never conquered.\"\n\n"
              "\"The thing is, I know for a fact that Mondedr exists, and that\n"
              "their cloaking spell can be replicated. Attnam tried to take our\n"
              "goddess away, but she is still strong in our hearts. I have faith\n"
              "she will protect this island from valpurians, just as Cleptia did\n"
              "for Mondedr.\""));
    if (game::GetFreedomStoryState() == 2) {
      // already has a seedling
      game::TextScreen(
        CONST_S("\"The prayers are simple, but no god can affect the world uninvited,\n"
                "and a miracle of such strength requires more power than any priest\n"
                "could channel. We need a conduit, something close to Silva herself.\"\n\n"
                "\"We need a scion of the Holy Mango World-tree.\"\n\n"
                "\"Please, plant the seedling of this tree. Once we plant it here,\n"
                "in the village, I can cast the spell and no army will find us.\n"
                "The first valpurian attack surprised us, caught us unaware, unprepared\n"
                "and unable to defend our land. So let's not repeat history and\n"
                "get ready for them this time.\""));
    } else {
      IvanAssert(game::GetFreedomStoryState() == 0);
      game::TextScreen(
        CONST_S("\"The prayers are simple, but no god can affect the world uninvited,\n"
                "and a miracle of such strength requires more power than any priest\n"
                "could channel. We need a conduit, something close to Silva herself.\"\n\n"
                "\"We need a scion of the Holy Mango World-tree.\"\n\n"
                "\"You have done so much for your village, yet I must ask for another\n"
                "favour. You know that the late viceroy destroyed the altar of Silva\n"
                "in our shrine, but you might not know that there is another shrine of Silva\n"
                "on this island, or rather below it. I would implore you to go down into\n"
                "the underwater tunnel and find a strange formation of rock where our people\n"
                "buried the stairs to the crystal cave of Silva under a cave-in,\n"
                "once it was obvious that we will be conquered. We couldn't let the Attnamese\n"
                "desecrate that most holy place. There, in an ancient temple of Silva,\n"
                "grows a tree of wondrous power, a tiny sapling of the World-tree.\""));
      game::TextScreen(
        CONST_S("\"Please, bring back a seedling of this tree. Once we plant it here,\n"
                "in the village, I can cast the spell and no army will find us.\n"
                "The first valpurian attack surprised us, caught us unaware, unprepared\n"
                "and unable to defend our land. So let's not repeat history and\n"
                "get ready for them this time.\""));
      game::SetFreedomStoryState(1);
    }

    GetArea()->SendNewDrawRequest();
    ADD_MESSAGE("\"Oh, and give my regards to Terra, if she's still alive.\"");

    HasBeenSpokenTo = true;
    return true;
  } else if (game::GetFreedomStoryState() == 2 && GetRelation(PLAYER) != HOSTILE) {
    ADD_MESSAGE("\"You have the seedling! Wonderful. Please, plant it by the "
                "banana delivery spot, and we shan't fear the imperialists anymore.\"");
    return true;
  }

  return false;
}


#endif
