#ifdef HEADER_PHASE
CHARACTER(encourager, humanoid)
{
protected:
  virtual void GetAICommand () override;
};


#else


void encourager::GetAICommand () {
  StandIdleAI();
}


#endif
