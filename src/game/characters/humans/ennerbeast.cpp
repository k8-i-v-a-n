#ifdef HEADER_PHASE
CHARACTER(ennerbeast, humanoid)
{
public:
  virtual truth Hit (character*, v2, int, int = 0) override;
  virtual truth ReceiveDamage(character*, int, int, int = ALL, int = 8, truth = false, truth = false, truth = false, truth = true) override;
  virtual truth NeedCheckNearbyItems () const override;

protected:
  virtual bodypart *MakeBodyPart (int) const override;
  virtual void GetAICommand () override;
  virtual truth AttackIsBlockable (int) const override;

  virtual int GetBaseScreamStrength () const;
  virtual truth IsEnnerChild () const;
};


#else


truth ennerbeast::AttackIsBlockable (int) const { return false; }
int ennerbeast::GetBaseScreamStrength () const { return 70; }
truth ennerbeast::IsEnnerChild () const { return false; }


truth ennerbeast::Hit (character *Enemy, v2, int, int) {
  if (CheckIfTooScaredToHit(Enemy)) return false;

  if (RAND_2) {
    ADD_MESSAGE("%s yells: UGH UGHAaaa!", CHAR_DESCRIPTION(DEFINITE));
  } else {
    ADD_MESSAGE("%s yells: Uga Ugar Ugade Ugat!", CHAR_DESCRIPTION(DEFINITE));
  }

  rect Rect;
  femath::CalculateEnvironmentRectangle(Rect, GetLevel()->GetBorder(), GetPos(), 30);

  int BaseScreamStrength = GetBaseScreamStrength();
  for (int x = Rect.X1; x <= Rect.X2; ++x) {
    for (int y = Rect.Y1; y <= Rect.Y2; ++y) {
      int ScreamStrength = (int)(BaseScreamStrength/(hypot(GetPos().X-x, GetPos().Y-y)+1));
      if (ScreamStrength) {
        character *Char = GetNearSquare(x, y)->GetCharacter();
        if (Char && Char != this) {
          msgsystem::EnterBigMessageMode();
               if (Char->IsPlayer()) ADD_MESSAGE("You are hit by the horrible waves of high sound.");
          else if (Char->CanBeSeenByPlayer()) ADD_MESSAGE("%s is hit by the horrible waves of high sound.", Char->CHAR_NAME(DEFINITE));
          Char->ReceiveDamage(this, ScreamStrength, SOUND, ALL, YOURSELF, true);
          Char->CheckDeath(CONST_S("killed @bkp scream"), this);
          msgsystem::LeaveBigMessageMode();
          if (/*!IsEnnerChild() &&*/ Char->IsPlayer() && Char->HasOmmelBlood() && CanBeSeenByPlayer()) {
            if (Char->CurdleOmmelBlood()) ADD_MESSAGE("Your vial of Ommel Blood vibrates and it contents curdles with the scream of the Enner Beast.");
          }
        }
        GetNearLSquare(x, y)->GetStack()->ReceiveDamage(this, ScreamStrength, SOUND);
      }
    }
  }

  EditNP(-100);
  EditAP(-1000000 / GetCWeaponSkill(BITE)->GetBonus());
  EditStamina(-1000, false);
  return true;
}


truth ennerbeast::ReceiveDamage (character *Damager, int Damage, int Type, int TargetFlags, int Direction,
                                 truth Divide, truth PenetrateArmor, truth Critical, truth ShowMsg)
{
  truth Success = false;
  if (Type != SOUND) {
    Success = humanoid::ReceiveDamage(Damager, Damage, Type, TargetFlags, Direction, Divide, PenetrateArmor, Critical, ShowMsg);
  }
  return Success;
}


truth ennerbeast::NeedCheckNearbyItems () const {
  return false;
}


void ennerbeast::GetAICommand () {
  SeekLeader(GetLeader());
  if (StateIsActivated(PANIC) || !RAND_N(3)) {
    Hit(0, ZERO_V2, YOURSELF);
  }
  if (CheckForEnemies(false, false, true)) return;
  if (FollowLeader(GetLeader())) return;
  if (MoveRandomly()) return;
  EditAP(-1000);
}


bodypart *ennerbeast::MakeBodyPart (int I) const {
  if (I == HEAD_INDEX) return ennerhead::Spawn(0, NO_MATERIALS);
  return humanoid::MakeBodyPart(I);
}


#endif
