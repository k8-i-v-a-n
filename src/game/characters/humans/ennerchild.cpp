#ifdef HEADER_PHASE
CHARACTER(ennerchild, ennerbeast)
{
protected:
  virtual int GetBaseScreamStrength () const override;
  virtual truth IsEnnerChild () const override;
};


#else


// Enner girl is older
int ennerchild::GetBaseScreamStrength () const { return (GetConfig() == BOY ? 40 : 50); }
truth ennerchild::IsEnnerChild () const { return true; }


#endif
