#ifdef HEADER_PHASE
CHARACTER(farmer, humanoid)
{
public:
  virtual v2 GetHeadBitmapPos () const override;
  virtual v2 GetRightArmBitmapPos () const override;
  virtual v2 GetLeftArmBitmapPos () const override;
};


#else


v2 farmer::GetHeadBitmapPos () const { return v2(96, (4 + RAND_2) << 4); }
v2 farmer::GetRightArmBitmapPos () const { return v2(64, RAND_2 << 4); }
v2 farmer::GetLeftArmBitmapPos () const { return GetRightArmBitmapPos(); }


#endif
