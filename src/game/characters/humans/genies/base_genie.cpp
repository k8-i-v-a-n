#ifdef HEADER_PHASE
CHARACTER(genie, humanoid)
{
public:
  virtual truth BodyPartIsVital (int) const override;
  virtual int GetAttribute (int, truth = true) const override;
  virtual truth CanCreateBodyPart (int) const override;
  virtual cfestring& GetStandVerb () const override;
};


#else


cfestring& genie::GetStandVerb() const { return character::GetStandVerb(); }
truth genie::BodyPartIsVital(int I) const { return I == TORSO_INDEX || I == HEAD_INDEX; }
truth genie::CanCreateBodyPart (int I) const { return I == TORSO_INDEX || I == HEAD_INDEX || I == RIGHT_ARM_INDEX || I == LEFT_ARM_INDEX; }


// temporary until someone invents a better way of doing this
int genie::GetAttribute (int Identifier, truth AllowBonus) const {
  if (Identifier == LEG_STRENGTH) return GetDefaultLegStrength();
  if (Identifier == AGILITY) return GetDefaultAgility();
  return humanoid::GetAttribute(Identifier, AllowBonus);
}


#endif
