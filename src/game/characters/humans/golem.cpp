#ifdef HEADER_PHASE
CHARACTER(golem, humanoid)
{
public:
  golem ();

  virtual truth MoveRandomly (truth allowInterestingItems=true) override;
  virtual truth CheckForUsefulItemsOnGround (truth = true) override;
  virtual truth NeedCheckNearbyItems () const override;
  virtual void BeTalkedTo () override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  void SetItemVolume (sLong What);

protected:
  virtual truth AddAdjective (festring &, truth) const override;
  virtual material *CreateBodyPartMaterial (int, sLong) const override;
  virtual void CreateCorpse (lsquare *) override;

protected:
  sLong ItemVolume;
};


#else


//==========================================================================
//
//  golem::golem
//
//==========================================================================
golem::golem () {
  if (!game::IsLoading()) {
    ItemVolume = 50+RAND_N(100);
  }
}


//==========================================================================
//
//  golem::NeedCheckNearbyItems
//
//==========================================================================
truth golem::NeedCheckNearbyItems () const {
  return false;
}


//==========================================================================
//
//  golem::CreateBodyPartMaterial
//
//==========================================================================
material *golem::CreateBodyPartMaterial (int, sLong Volume) const {
  return MAKE_MATERIAL(GetConfig(), Volume);
}


//==========================================================================
//
//  golem::CheckForUsefulItemsOnGround
//
//==========================================================================
truth golem::CheckForUsefulItemsOnGround (truth) {
  return false;
}


//==========================================================================
//
//  golem::SetItemVolume
//
//==========================================================================
void golem::SetItemVolume (sLong What) {
  ItemVolume = What;
}


//==========================================================================
//
//  golem::MoveRandomly
//
//==========================================================================
truth golem::MoveRandomly (truth allowInterestingItems) {
  if (!RAND_N(500)) {
    Engrave(CONST_S("Golem Needs Master"));
    EditAP(-1000);
    return true;
  }
  return humanoid::MoveRandomly(allowInterestingItems);
}


//==========================================================================
//
//  golem::BeTalkedTo
//
//==========================================================================
void golem::BeTalkedTo () {
  static sLong Said;

  if (GetRelation(PLAYER) == HOSTILE) {
    Engrave(GetHostileReplies()[RandomizeReply(Said, GetHostileReplies().Size)]);
  } else {
    Engrave(GetFriendlyReplies()[RandomizeReply(Said, GetFriendlyReplies().Size)]);
  }

  if (CanBeSeenByPlayer()) ADD_MESSAGE("%s engraves something.", CHAR_NAME(DEFINITE));
}


//==========================================================================
//
//  golem::AddAdjective
//
//==========================================================================
truth golem::AddAdjective (festring &String, truth Articled) const {
  int TotalRustLevel = sumbodypartproperties()(this, &bodypart::GetMainMaterialRustLevel);

  if (!TotalRustLevel) return humanoid::AddAdjective(String, Articled);

  if (Articled) String << "a ";

       if (TotalRustLevel <= GetBodyParts()) String << "slightly rusted ";
  else if (TotalRustLevel <= GetBodyParts() << 1) String << "rusted ";
  else String << "very rusted ";

  String << GetAdjective() << ' ';
  return true;
}


//==========================================================================
//
//  golem::CreateCorpse
//
//==========================================================================
void golem::CreateCorpse (lsquare *Square) {
  material *Material = GetTorso()->GetMainMaterial();
  //if (Material->IsSolid()) Square->AddItem(Material->CreateNaturalForm(ItemVolume));

  if (Material->IsLiquid()) {
    for (int d = 0; d < GetExtendedNeighbourSquares(); d += 1) {
      lsquare *NeighbourSquare = Square->GetNeighbourLSquare(d);
      if (NeighbourSquare) {
        NeighbourSquare->SpillFluid(0,
          static_cast<liquid*>(GetTorso()->GetMainMaterial()->SpawnMore(250 + RAND_N(250))));
      }
    }
  } else if (Material->IsGaseous()) {
    game::GetCurrentLevel()->GasExplosion(static_cast<gas*>(Material), Square, this);
  } else if (Material->IsSolid()) {
    Square->AddItem(Material->CreateNaturalForm(ItemVolume));
  }

  SendToHell();
}


//==========================================================================
//
//  golem::Save
//
//==========================================================================
void golem::Save (outputfile &SaveFile) const {
  humanoid::Save(SaveFile);
  SaveFile << ItemVolume;
}


//==========================================================================
//
//  golem::Load
//
//==========================================================================
void golem::Load (inputfile &SaveFile) {
  humanoid::Load(SaveFile);
  SaveFile >> ItemVolume;
}


#endif
