#ifdef HEADER_PHASE
CHARACTER(guard, humanoid)
{
public:
  guard () : NextWayPoint(0) {}

  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void GetAICommand () override;
  virtual void SetWayPoints (const fearray<packv2> &) override;
  virtual truth MoveTowardsHomePos () override;
  virtual truth TryQuestTalks () override;

protected:
  std::vector<v2> WayPoints;
  uInt NextWayPoint;
};


#else


void guard::SetWayPoints (const fearray<packv2> &What) { ArrayToVector(What, WayPoints); }


void guard::Save (outputfile &SaveFile) const {
  humanoid::Save(SaveFile);
  SaveFile << WayPoints << NextWayPoint;
}


void guard::Load (inputfile &SaveFile) {
  humanoid::Load(SaveFile);
  SaveFile >> WayPoints >> NextWayPoint;
}


void guard::GetAICommand () {
  //FIXME!!!
  //k8: (30, 17)? looks like some hard-coded Attnam position
  if (GetConfig() == MASTER && (HP<<1) < MaxHP && (GetPos()-v2(30, 17)).GetLengthSquare() > 9) {
    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s disappears.", CHAR_NAME(DEFINITE));
    GetLevel()->GetLSquare(30, 16)->KickAnyoneStandingHereAway();
    Move(v2(30, 16), true);
    EditAP(-1000);
    return;
  }

  if (WayPoints.size() && !IsGoingSomeWhere()) {
    if (GetPos() == WayPoints[NextWayPoint]) {
      if (NextWayPoint < WayPoints.size()-1) ++NextWayPoint; else NextWayPoint = 0;
    }
    GoingTo = WayPoints[NextWayPoint];
  }

  SeekLeader(GetLeader());
  if (CheckForEnemies(true, true, true)) return;
  if (CheckForUsefulItemsOnGround()) return;
  if (FollowLeader(GetLeader())) return;
  if (CheckForDoors()) return;
  if (MoveTowardsHomePos()) return;
  if (CheckSadism()) return;
  if (CheckForBeverage()) return;

  EditAP(-1000);
}


truth guard::MoveTowardsHomePos () {
  //k8: (30, 16)? looks like some hard-coded Attnam position
  if (GetConfig() == MASTER && GetPos() != v2(30, 16)) {
    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s disappears.", CHAR_NAME(DEFINITE));
    GetLevel()->GetLSquare(30, 16)->KickAnyoneStandingHereAway();
    Move(v2(30, 16), true);
    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s appears.", CHAR_NAME(DEFINITE));
    EditAP(-1000);
    return true;
  }
  return humanoid::MoveTowardsHomePos();
}


truth guard::TryQuestTalks () {
  if (GetRelation(PLAYER) != HOSTILE) {
    if (GetConfig() == EMISSARY) {
      /* We're talking to the emissary of Aslona from now on. */
      if (game::GetStoryState() <= 1 &&
          // necromancer quest
          (/*game::GetStoryState() <= 1 &&*/ game::GetXinrochTombStoryState() == 0))
      {
        ADD_MESSAGE("%s eyes you, calculating: \"I might have work for you and I can make it worth your while.\"",
                    CHAR_NAME(DEFINITE));

        if (game::TruthQuestion(CONST_S("Do you accept the quest?"), REQUIRES_ANSWER)) {
          game::TextScreen(CONST_S(
            "\"I shouldn't be saying this so openly, but my kingdom is in dire straits and needs any\n"
            "help it can get. High priest Petrus will not hear my pleas and I don't believe that\n"
            "my colleagues in other lands will be more successful. Lord Regent is doing his best,\n"
            "but his army just barely holds the rebels back.\"\n\n"
            "\"I know you are just one man, but maybe you could help where an army couldn't. Please,\n"
            "go to the Castle of Aslona and seek out Lord Efra Peredivall. He will know what must\n"
            "be done to mercilessly crush the rebel scum!\""));
          if (PLAYER->HasEncryptedScroll()) {
            game::TextScreen(CONST_S(
              "You nods in agreement, but suddenly the bright thought blazed throught your brain.\n"
              "\"By the way, could you please tell me what is written in this scroll?\", you asked,\n"
              "passing the encrypted scroll to the emissary. He looked at the scroll for some time,\n"
              "and replied: \"This might be something interesting. I cannot read it, but I will send\n"
              "it to our wizard to decipher.\" He puts the scroll in his pocket."));
            PLAYER->RemoveEncryptedScroll();
          }

          game::GivePlayerBoat();
          game::RevealPOI(game::aslonaPOI());
          //game::RevealPOI(game::rebelCampPOI());
          GetArea()->SendNewDrawRequest();

          ADD_MESSAGE("\"If you need to cross the sea, you can use my ship. It should be waiting at the shore.\"");
          game::SetAslonaStoryState(1);
          game::SetStoryState(2);
          return true;
        } else {
          ADD_MESSAGE("%s narrows his eyes: \"I would think twice about brushing me aside, "
                      "if I were you. Think about it.\"", CHAR_NAME(DEFINITE));
          return true;
        }
      }

      if (game::GetAslonaStoryState() && !RAND_N(4)) {
        // Isn't he charming?
        ADD_MESSAGE("\"You should know that I'm counting on you. My whole country is "
                    "counting on you. Don't screw it up!\"");
        return true;
      }
    } else {
      // ordinary guard
      itemvector Item;

      if (PLAYER->SelectFromPossessions(Item, CONST_S("Do you have something to give me?"),
                                        NO_MULTI_SELECT | SS_FIRST_ITEM | STACK_ALLOW_NAMING,
                                        &item::IsBeverage) &&
          !Item.empty())
      {
        for (size_t c = 0; c < Item.size(); ++c) {
          Item[c]->RemoveFromSlot();
          GetStack()->AddItem(Item[c]);
        }
        return true;
      }
    }
  }

  return false;
}


#endif
