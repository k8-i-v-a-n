#ifdef HEADER_PHASE
CHARACTER(imperialist, humanoid)
{
public:
  virtual void BeTalkedTo () override;
  virtual truth TryQuestTalks () override;
  virtual void DisplayStethoscopeInfo (character *) const override;

protected:
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;
};


#else


void imperialist::GetAICommand () {
  StandIdleAI();
}


truth imperialist::TryQuestTalks () {
  decosadshirt *Shirt = static_cast<decosadshirt *>(PLAYER->SearchForItem(this, &item::IsDecosAdShirt));
  if (Shirt) {
    feuLong Reward = Shirt->GetEquippedTicks() / 500;
    if (Reward) {
      ADD_MESSAGE("%s smiles. \"I see you have advertised our company diligently. "
                  "Here's \1Y%dgp\2 as a token of my gratitude.\"", CHAR_NAME(DEFINITE), Reward);
      PLAYER->EditMoney(Reward);
      Shirt->SetEquippedTicks(0);
    } else if (!RAND_N(5)) {
      ADD_MESSAGE("\"Come back when you've worn the shirt for some time and I'll reward you generously!\"");
    }
    return true;
  }
  return false;
}


void imperialist::BeTalkedTo () {
  static sLong Said;
       if (GetRelation(PLAYER) == HOSTILE) ProcessAndAddMessage(GetHostileReplies()[RandomizeReply(Said, GetHostileReplies().Size)]);
  else if (!game::PlayerIsSumoChampion()) ProcessAndAddMessage(GetFriendlyReplies()[RandomizeReply(Said, GetFriendlyReplies().Size)]);
  else ProcessAndAddMessage(GetFriendlyReplies()[RandomizeReply(Said, GetFriendlyReplies().Size-1)]);
}


void imperialist::DisplayStethoscopeInfo (character *) const {
  ADD_MESSAGE("You hear coins clinking inside.");
}


void imperialist::CreateCorpse (lsquare *Square) {
  if (!game::GetLiberator()) {
    game::SetLiberator(1);
    ADD_MESSAGE("You liberate citizens of New Attnam!");
  }
  imperialistsysbase::CreateCorpse(Square);
}


#endif
