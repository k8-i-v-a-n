#ifdef HEADER_PHASE
CHARACTER(crimsonimp, imp)
{
protected:
  virtual truth SpecialBiteEffect (character *, v2, int, int, truth, truth, int) override;
  virtual void CreateCorpse (lsquare *) override;
};

#else


truth crimsonimp::SpecialBiteEffect (character *Victim, v2 HitPos, int BodyPartIndex,
                                     int Direction, truth BlockedByArmour,
                                     truth Critical, int DoneDamage)
{
  /*k8: as we have no fire system, this is nothing, sadly...
  bodypart *BodyPart = Victim->GetBodyPart(BodyPartIndex);

  if (BodyPart && BodyPart->IsDestroyable(Victim) &&
      BodyPart->GetMainMaterial() && BodyPart->CanBeBurned() &&
      (BodyPart->GetMainMaterial()->GetInteractionFlags() & CAN_BURN) && !BodyPart->IsBurning())
  {
    if (BodyPart->TestActivationEnergy(150)) return true;
  }
  */
  return false;
}


void crimsonimp::CreateCorpse (lsquare *Square) {
  game::GetCurrentLevel()->Explosion(this, CONST_S("consumed by the hellfire of ") + GetName(INDEFINITE),
                                     Square->GetPos(), RAND_BETWEEN(20 - 4, 20 + 4)/*20 + RAND_N(5) - RAND_N(5)*/);
  SendToHell();
}


#endif
