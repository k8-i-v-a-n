#ifdef HEADER_PHASE
CHARACTER(mirrorimp, imp)
{
public:
  virtual truth IsMagicDrinker () const override;
  virtual truth DrinkMagic (const beamdata &) override;
protected:
  virtual void CreateCorpse (lsquare *) override;
};

#else


truth mirrorimp::IsMagicDrinker () const { return true; }


truth mirrorimp::DrinkMagic (const beamdata &Beam) {
  if (!Beam.Wand) return false;
  if (!Beam.Wand->IsExplosive()) return false;

  // comm. fork: WILL_POWER
  // k8: we have no use to willpower. have to find another stat.
  if (Beam.Owner && RAND_N(GetMana()) <= RAND_N(Beam.Owner->GetAttribute(WILL_POWER))) {
    Beam.Owner->EditExperience(WILL_POWER, 100, 1 << 12);
    return false;
  }

  festring DeathMsg = CONST_S("killed by an explosion of ");
  Beam.Wand->AddName(DeathMsg, INDEFINITE);
  DeathMsg << " caused @bk";

  if (IsPlayer()) {
    ADD_MESSAGE("You grin as %s %s.", Beam.Wand->GetExtendedDescription().CStr(),
                Beam.Wand->GetBreakMsg().CStr());
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s cackles with glee as %s %s.", CHAR_NAME(DEFINITE),
                Beam.Wand->GetExtendedDescription().CStr(), Beam.Wand->GetBreakMsg().CStr());
  }

  Beam.Wand->BreakEffect(this, DeathMsg);
  return true;
}


void mirrorimp::CreateCorpse (lsquare *Square) {
  // do not replace any interesting terrain
  // nope, allow to replace everything except stairs -- it is funnier this way
  //if (!Square->GetOLTerrain())
  {
    decoration *Shard = decoration::Spawn(SHARD);
    Shard->InitMaterials(MAKE_MATERIAL(GLASS));
    if (!Square->ChangeOLTerrainAndUpdateLights(Shard)) {
      // cannot replace stairs
      delete Shard;
    }
  }
  SendToHell();
}


#endif
