#ifdef HEADER_PHASE
CHARACTER(kamikazedwarf, humanoid)
{
public:
  virtual truth Hit (character *, v2, int, int = 0) override;
  virtual truth CheckForUsefulItemsOnGround (truth = true) override;
  virtual void GetAICommand () override;
  virtual truth NeedCheckNearbyItems () const override;
  virtual void CreateInitialEquipment (int) override;
  virtual truth IsKamikazeDwarf () const override;
  virtual void SingRandomSong ();

protected:
  virtual col16 GetTorsoMainColor () const override;
  virtual col16 GetGauntletColor () const override;
  virtual col16 GetLegMainColor () const override;
  virtual v2 GetDrawDisplacement (int) const override;
  virtual int GetWSkillHits () const;
  virtual truth IsElite () const;
  virtual truth IsRookie () const;
  virtual truth IsGrenadier () const;
};


#else


truth kamikazedwarf::IsKamikazeDwarf () const { return true; }
truth kamikazedwarf::IsElite () const { return false; }
truth kamikazedwarf::IsRookie () const { return false; }
truth kamikazedwarf::IsGrenadier () const { return false; }
// they will not be distracted by items, it's not their life goal
truth kamikazedwarf::NeedCheckNearbyItems () const { return false; }
truth kamikazedwarf::CheckForUsefulItemsOnGround (truth) { return false; }
int kamikazedwarf::GetWSkillHits () const { return 10000; }

col16 kamikazedwarf::GetTorsoMainColor () const { return GetMasterGod()->GetColor(); }
col16 kamikazedwarf::GetGauntletColor () const { return GetMasterGod()->GetColor(); }
col16 kamikazedwarf::GetLegMainColor () const { return GetMasterGod()->GetColor(); }


void kamikazedwarf::CreateInitialEquipment (int SpecialFlags) {
  if (IsRookie()) {
    humanoid::CreateInitialEquipment(SpecialFlags);
    SetRightWielded(holybook::Spawn(GetConfig(), SpecialFlags));
    SetLeftWielded(meleeweapon::Spawn(AXE));
    for (int k = 0; k < 6; ++k) GetStack()->AddItem(meleeweapon::Spawn(AXE));
    GetStack()->AddItem(lantern::Spawn());
    GetCWeaponSkill(AXES)->AddHit(GetWSkillHits());
    GetCurrentRightSWeaponSkill()->AddHit(GetWSkillHits());
    return;
  }

  if (IsGrenadier()) {
    humanoid::CreateInitialEquipment(SpecialFlags);
    SetLeftWielded(holybook::Spawn(GetConfig(), SpecialFlags));
    SetRightWielded(meleeweapon::Spawn(GREAT_AXE, SpecialFlags));
    for (int k = 0; k < 3; ++k) GetStack()->AddItem(gasgrenade::Spawn());
    GetStack()->AddItem(lantern::Spawn());
    GetCWeaponSkill(AXES)->AddHit(GetWSkillHits());
    GetCurrentRightSWeaponSkill()->AddHit(GetWSkillHits());
    return;
  }

  humanoid::CreateInitialEquipment(SpecialFlags);
  SetRightWielded(holybook::Spawn(GetConfig(), SpecialFlags));
  GetCWeaponSkill(UNCATEGORIZED)->AddHit(GetWSkillHits());
  GetCurrentRightSWeaponSkill()->AddHit(GetWSkillHits());
}


truth kamikazedwarf::Hit (character *Enemy, v2 HitPos, int Direction, int Flags) {
  if (!IsPlayer()) {
    itemvector KamikazeWeapon;
    sortdata SortData(KamikazeWeapon, this, false, &item::IsKamikazeWeapon);
    SortAllItems(SortData);

    if (IsRookie() && RAND_4) {
      return humanoid::Hit(Enemy, HitPos, Direction, Flags);
    }

    if (!KamikazeWeapon.empty()) {
      if (IsElite() && RAND_2) {
        ADD_MESSAGE("%s shouts: \"This time I won't fail, O Great %s!\"", CHAR_DESCRIPTION(DEFINITE), GetMasterGod()->GetName());
      } else if (IsGrenadier() && RAND_2) {
        ADD_MESSAGE("%s shouts: \"What the hell, it'll be quick. Here goes nothing!\"", CHAR_DESCRIPTION(DEFINITE));
      } else if (IsRookie() && RAND_2) {
        ADD_MESSAGE("%s sobs: \"War is hell!\"", CHAR_DESCRIPTION(DEFINITE));
      } else if (RAND_2) {
        ADD_MESSAGE("%s shouts: \"For %s!\"", CHAR_DESCRIPTION(DEFINITE), GetMasterGod()->GetName());
      } else {
        ADD_MESSAGE("%s screams: \"%s, here I come!\"", CHAR_DESCRIPTION(DEFINITE), GetMasterGod()->GetName());
      }
      if (KamikazeWeapon[RAND_N(KamikazeWeapon.size())]->Apply(this)) return true;
    }
  }

  return humanoid::Hit(Enemy, HitPos, Direction, Flags);
}


void kamikazedwarf::GetAICommand () {
  if (GetHomeRoom()) {
    StandIdleAI();
  } else {
    if (!RAND_N(50)) {
      SingRandomSong();
      return;
    }
    character::GetAICommand();
  }
}


// the argument is body part index
v2 kamikazedwarf::GetDrawDisplacement (int I) const {
  const v2 DrawDisplacement[] = {
    v2(0, 0),
    v2(0, 1),
    v2(0, -1),
    v2(0, -1),
    v2(0, -1),
    v2(0, 0),
    v2(0, 0),
  };
  return DrawDisplacement[I];
}


void kamikazedwarf::SingRandomSong () {
  festring Song;
  festring God = CONST_S(GetMasterGod()->GetName());
  festring Bodypart;

  switch (RAND_N(9)) {
    case 0:
      switch (RAND_N(3)) {
        case 0: Bodypart = "palm"; break;
        case 1: Bodypart = "forehead"; break;
        default: Bodypart = "tongue"; break;
      }
      Song = festring("On the ")+Bodypart+festring(" of ")+God+" everybody fears everything";
      break;
    case 1: Song = festring("Joy to the world, ")+God+" is come! Let all above Valpurus receive her "+(GetMasterGod()->GetSex() == MALE ? "King" : "Queen"); break;
    case 2: Song = festring("Hark the herald angels sing. Glory to ")+God+"!"; break;
    case 3: Song = festring("O ")+God+", You are so big, So absolutely huge, Gosh, we're all really impressed down here, I can tell You."; break;
    case 4: Song = festring("Forgive us, O ")+God+" for this, our dreadful toadying and barefaced flattery"; break;
    case 5: Song = festring("But you, ")+God+", are so strong and, well, just so super fantastic. Amen."; break;
    case 6: Song = festring("O ")+God+", please don't burn us"; break;
    case 7: Song = festring("O ")+God+", please don't grill or toast your flock"; break;
    case 8: Song = festring("O ")+God+", please don't simmer us in stock"; break;
  }
  EditAP(-1000);
  if (CanBeSeenByPlayer()) ADD_MESSAGE("%s sings: \"%s\"", CHAR_DESCRIPTION(DEFINITE), Song.CStr());
  else ADD_MESSAGE("You hear someone sing: \"%s\"", Song.CStr());
}


#endif
