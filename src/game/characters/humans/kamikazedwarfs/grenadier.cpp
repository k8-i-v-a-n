#ifdef HEADER_PHASE
CHARACTER(grenadierdwarf, kamikazedwarf)
{
public:
  virtual void GetAICommand () override;
  //virtual void CreateInitialEquipment (int) override;

protected:
  virtual void PostConstruct () override;
  virtual col16 GetTorsoMainColor () const override;
  virtual col16 GetGauntletColor () const override;
  virtual col16 GetLegMainColor() const override;
  virtual int GetWSkillHits () const override;
  virtual truth IsGrenadier () const override;
};


#else


truth grenadierdwarf::IsGrenadier () const { return true; }

int grenadierdwarf::GetWSkillHits () const { return 50000; }
col16 grenadierdwarf::GetTorsoMainColor () const { return GetMasterGod()->GetEliteColor(); }
col16 grenadierdwarf::GetGauntletColor () const { return GetMasterGod()->GetEliteColor(); }
col16 grenadierdwarf::GetLegMainColor () const { return GetMasterGod()->GetEliteColor(); }


void grenadierdwarf::GetAICommand () {
  if (CheckThrowItemOpportunity()) return;
  if (!RAND_N(50)) {
    SingRandomSong();
    return;
  }
  kamikazedwarf::GetAICommand();
}


/*
void grenadierdwarf::CreateInitialEquipment (int SpecialFlags) {
  SetLeftWielded(meleeweapon::Spawn(GREAT_AXE, SpecialFlags));
  for (int k = 0; k < 3; ++k) GetStack()->AddItem(gasgrenade::Spawn());
  GetStack()->AddItem(lantern::Spawn());
  GetCWeaponSkill(AXES)->AddHit(GetWSkillHits());
  GetCurrentRightSWeaponSkill()->AddHit(GetWSkillHits());
  humanoid::CreateInitialEquipment(SpecialFlags);
  SetRightWielded(holybook::Spawn(GetConfig(), SpecialFlags));
}
*/


void grenadierdwarf::PostConstruct () {
  kamikazedwarf::PostConstruct();
  ivantime Time;
  game::GetTime(Time);
  int Modifier = Time.Day-KAMIKAZE_INVISIBILITY_DAY_MIN;
  if (Time.Day >= KAMIKAZE_INVISIBILITY_DAY_MAX || (Modifier > 0 && RAND_N(KAMIKAZE_INVISIBILITY_DAY_MAX-KAMIKAZE_INVISIBILITY_DAY_MIN) < Modifier)) {
    GainIntrinsic(INVISIBLE);
  }
}


#endif
