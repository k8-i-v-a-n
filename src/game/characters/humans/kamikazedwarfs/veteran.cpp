#ifdef HEADER_PHASE
CHARACTER(veterankamikazedwarf, kamikazedwarf)
{
protected:
  virtual void PostConstruct () override;
  virtual col16 GetTorsoMainColor () const override;
  virtual col16 GetGauntletColor () const override;
  virtual col16 GetLegMainColor () const override;
  virtual int GetWSkillHits () const override;
  virtual truth IsElite () const override;
};


#else


truth veterankamikazedwarf::IsElite () const { return true; }

int veterankamikazedwarf::GetWSkillHits () const { return 50000; }
col16 veterankamikazedwarf::GetTorsoMainColor () const { return GetMasterGod()->GetEliteColor(); }
col16 veterankamikazedwarf::GetGauntletColor () const { return GetMasterGod()->GetEliteColor(); }
col16 veterankamikazedwarf::GetLegMainColor () const { return GetMasterGod()->GetEliteColor(); }


void veterankamikazedwarf::PostConstruct () {
  kamikazedwarf::PostConstruct();
  ivantime Time;
  game::GetTime(Time);
  int Modifier = Time.Day - KAMIKAZE_INVISIBILITY_DAY_MIN;
  if (Time.Day >= KAMIKAZE_INVISIBILITY_DAY_MAX ||
      (Modifier > 0 && RAND_N(KAMIKAZE_INVISIBILITY_DAY_MAX - KAMIKAZE_INVISIBILITY_DAY_MIN) < Modifier))
  {
    GainIntrinsic(INVISIBLE);
  }
}


#endif
