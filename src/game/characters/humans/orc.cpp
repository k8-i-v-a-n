#ifdef HEADER_PHASE
CHARACTER(orc, humanoid)
{
  virtual truth MoveRandomly (truth allowInterestingItems=true) override;
protected:
  virtual void PostConstruct () override;
};


#else


truth orc::MoveRandomly (truth allowInterestingItems) {
  return (GetConfig() == REPRESENTATIVE ? MoveRandomlyInRoom(allowInterestingItems)
                                        : humanoid::MoveRandomly(allowInterestingItems));
}


void orc::PostConstruct () {
  if (!RAND_N(25)) GainIntrinsic(LEPROSY);
}


#endif
