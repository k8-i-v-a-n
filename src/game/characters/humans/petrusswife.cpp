#ifdef HEADER_PHASE
CHARACTER(petrusswife, humanoid)
{
public:
  petrusswife () : GiftTotal(0) {}

  virtual truth MoveRandomly (truth allowInterestingItems=true) override;
  virtual truth SpecialEnemySightedReaction (character *) override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth TryQuestTalks () override;
  virtual truth IsPetrussWife () const override;

protected:
  int GiftTotal;
};


#else


truth petrusswife::IsPetrussWife () const { return true; }


truth petrusswife::MoveRandomly (truth allowInterestingItems) {
  if (PLAYER && PLAYER->IsEnabled() && GetRelation(PLAYER) == FRIEND) {
    return humanoid::MoveRandomly(allowInterestingItems);
  }
  return MoveRandomlyInRoom(allowInterestingItems);
}


truth petrusswife::SpecialEnemySightedReaction (character *Char) {
  item *Weapon = Char->GetMainWielded();
  if (Weapon && Weapon->IsWeapon(Char) && !RAND_N(20)) {
    ADD_MESSAGE("%s screams: \"Oh my Frog, %s's got %s %s!\"",
                CHAR_DESCRIPTION(DEFINITE), Char->CHAR_PERSONAL_PRONOUN_THIRD_PERSON_VIEW,
                Weapon->GetArticle(), Weapon->GetNameSingular().CStr());
  }
  return false;
}


void petrusswife::Save (outputfile &SaveFile) const {
  humanoid::Save(SaveFile);
  SaveFile << GiftTotal;
}


void petrusswife::Load (inputfile &SaveFile) {
  humanoid::Load(SaveFile);
  SaveFile >> GiftTotal;
}


truth petrusswife::TryQuestTalks () {
  itemvector Item;

  if (GetConfig() == 3 || GetConfig() == 7) {
    if (!PLAYER->SelectFromPossessions(Item, CONST_S("Do you have something to give me?"),
                                       NO_MULTI_SELECT | SS_FIRST_ITEM | STACK_ALLOW_NAMING,
                                       &item::IsGoodWarriorPrincessItem))
    {
      return false;
    }
  } else {
    if (!PLAYER->SelectFromPossessions(Item, CONST_S("Do you have something to give me?"),
                                       NO_MULTI_SELECT | SS_FIRST_ITEM | STACK_ALLOW_NAMING,
                                       &item::IsLuxuryItem))
    {
      return false;
    }
  }

  if (Item.empty()) return false;

  int Accepted = 0;
  truth RefusedSomething = false;
  int wasWeapon = 0;
  bool wasNonWeapon = false;

  for (size_t c = 0; c < Item.size(); ++c) {
    item *It = Item[c];
    if (!MakesBurdened(GetCarriedWeight() + It->GetWeight())) {
      if (/*It->IsHelmet(this) ||
          It->IsBodyArmor(this) ||
          It->IsShield(this) ||
          It->IsWeapon(this)*/
          It->IsGoodWarriorPrincessItemNoLux(this))
      {
        wasWeapon += 1;
      } else {
        wasNonWeapon = true;
      }
      Accepted += 1;
      GiftTotal += It->GetTruePrice();
      It->RemoveFromSlot();
      GetStack()->AddItem(Item[c]);
    } else {
      RefusedSomething = true;
      break;
    }
  }

  if (Accepted) {
    if (wasNonWeapon || !wasWeapon) {
      ADD_MESSAGE("\"I thank you for your little gift%s.\"", Accepted == 1 ? "" : "s");
    }
    if (wasWeapon) {
      if (wasNonWeapon) {
        ADD_MESSAGE("\"And thank you for your \1Yspecial\2 gift%s too.\" "
                    "She added in a lower voice.",
                    (wasWeapon == 1 ? "" : "s"));
      } else {
        ADD_MESSAGE("\"Thank you for your \1Yspecial\2 gift%s.\" "
                    "She said in a lower voice.",
                    (wasWeapon == 1 ? "" : "s"));
      }
    }
    return true;
  }

  if (RefusedSomething) {
    ADD_MESSAGE("\"Unfortunately I cannot carry any more of your gifts. "
                "I'm a delicate woman, you see.\"");
    return true;
  }

  return false;
}


#endif
