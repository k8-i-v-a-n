#ifdef HEADER_PHASE
CHARACTER(slave, playerkind)
{
public:
  virtual void BeTalkedTo () override;
  virtual truth TryQuestTalks () override;
  virtual void GetAICommand () override;
  virtual col16 GetHairColor () const override;
  virtual col16 GetEyeColor () const override;

protected:
  virtual void PostConstruct () override;
};


#else


col16 slave::GetHairColor () const { return humanoid::GetHairColor(); }
col16 slave::GetEyeColor () const { return humanoid::GetEyeColor(); }


truth slave::TryQuestTalks () {
  if (GetRelation(PLAYER) == HOSTILE) {
    ADD_MESSAGE("\"Yikes!\"");
    return true;
  }

  room *Room = GetHomeRoom();
  if (Room && Room->MasterIsActive()) {
    character *Master = Room->GetMaster();
    if (PLAYER->GetMoney() >= 50) {
      festring msg;
      msg << "\"Do you want to buy me? \1Y50\2 gold pieces. I work very hard.\"";
      ADD_MESSAGE("%s talks: %s", CHAR_DESCRIPTION(DEFINITE), msg.CStr());
      if (game::TruthQuestion(msg + "\nDo you want to buy him?")) {
        PLAYER->SetMoney(PLAYER->GetMoney() - 50);
        Master->SetMoney(Master->GetMoney() + 50);
        ChangeTeam(PLAYER->GetTeam());
        RemoveHomeData();
      }
    } else {
      ADD_MESSAGE("\"Don't touch me! Master doesn't want people to touch sale items. "
                  "I'm worth \1Y50\2 gold pieces, you know!\"");
    }
    return true;
  }

  if (GetTeam() == PLAYER->GetTeam()) {
    if ((PLAYER->GetMainWielded() && PLAYER->GetMainWielded()->IsWhip()) ||
        (PLAYER->GetSecondaryWielded() && PLAYER->GetSecondaryWielded()->IsWhip()))
    {
      ADD_MESSAGE("\"Don't hit me! I work! I obey! I don't think!\"");
      // remove panic mode, why not
      if (TemporaryStateIsActivated(PANIC)) {
        DeActivateTemporaryState(PANIC);
      }
      return true;
    }
  } else if (!RAND_2) {
    ADD_MESSAGE("\"I'm free! Rejoice!\"");
    return true;
  }

  return false;
}


void slave::BeTalkedTo () {
  character::BeTalkedTo();
}


void slave::GetAICommand () {
  SeekLeader(GetLeader());
  if (CheckForEnemies(true, true, true)) return;
  if (CheckForUsefulItemsOnGround()) return;
  if (FollowLeader(GetLeader())) return;
  if (CheckForDoors()) return;

  if (!GetHomeRoom() || !GetHomeRoom()->MasterIsActive()) {
    RemoveHomeData();
    if (MoveRandomly()) return;
  } else if (MoveTowardsHomePos()) {
    return;
  }

  EditAP(-1000);
}


void slave::PostConstruct () {
  Talent = TALENT_STRONG;
  Weakness = TALENT_CLEVER;
}


#endif
