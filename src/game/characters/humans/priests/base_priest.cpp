#ifdef HEADER_PHASE
CHARACTER(priest, humanoid)
{
public:
  virtual truth TryQuestTalks () override;

protected:
  virtual truth PriestHealingService ();
  virtual void GetAICommand () override;
};


#else


//==========================================================================
//
//  priest::GetAICommand
//
//==========================================================================
void priest::GetAICommand () {
  StandIdleAI();
}


//==========================================================================
//
//  priest::PriestHealingService
//
//==========================================================================
truth priest::PriestHealingService () {
  if (GetRelation(PLAYER) == HOSTILE) {
    //ADD_MESSAGE("\"Sinner! My hands shall pour Divine Wrath upon thee!\"");
    return false;
  }

  truth res = false;

  for (int c = 0; c < PLAYER->GetBodyParts(); ++c) {
    if (!PLAYER->GetBodyPart(c) && PLAYER->CanCreateBodyPart(c)) {
      truth HasOld = false;
      bool healed = false;
      res = true;

      for (std::list<feuLong>::const_iterator i = PLAYER->GetOriginalBodyPartID(c).begin();
           i != PLAYER->GetOriginalBodyPartID(c).end(); ++i)
      {
        bodypart *OldBodyPart = static_cast<bodypart *>(PLAYER->SearchForItem(*i));
        if (OldBodyPart) {
          HasOld = true;
          sLong Price = (GetConfig() == VALPURUS ? 50 : 10);
          if (PLAYER->GetMoney() >= Price) {
            if (!OldBodyPart->CanRegenerate()) {
              ADD_MESSAGE("\"Sorry, I cannot put back bodyparts made of %s, not even your severed %s.\"",
                          OldBodyPart->GetMainMaterial()->GetName(false, false).CStr(),
                          PLAYER->GetBodyPartName(c).CStr());
            } else {
              festring ss;
              ss << "\"I could put your old " << PLAYER->GetBodyPartName(c).CStr();
              ss << " back for \1Y" << Price << "\2 gold pieces.\"";
              ADD_MESSAGE("%s", ss.CStr());
              if (game::TruthQuestion(ss + "\nDo you agree?")) {
                OldBodyPart->SetHP(1);
                PLAYER->SetMoney(PLAYER->GetMoney()-Price);
                SetMoney(GetMoney()+Price);
                OldBodyPart->RemoveFromSlot();
                PLAYER->AttachBodyPart(OldBodyPart);
                healed = true;
                break; // sorry
              }
            }
          } else {
            ADD_MESSAGE("\"Your %s is severed. Help yourself and get \1Y%dgp\2 and I'll help you too.\"",
                        PLAYER->GetBodyPartName(c).CStr(), Price);
          }
        }
      }

      if (!healed) {
        sLong Price = (GetConfig() == VALPURUS ? 100 : 20);
        if (PLAYER->GetMoney() >= Price) {
          festring ss;
          if (HasOld) {
            ADD_MESSAGE("\"I could still summon up a new one for \1Y%d\2 gold pieces.\"", Price);
          } else {
            ADD_MESSAGE("\"Since you don't seem to have your original %s with you, "
                        "I could summon up a new one for \1Y%d\2 gold pieces.\"",
                        PLAYER->GetBodyPartName(c).CStr(), Price);
          }
          ss.Empty();
          ss << "\"I can summon a new " << PLAYER->GetBodyPartName(c).CStr();
          ss << " for \1Y" << Price << "\2 gold pieces.\"";
          if (game::TruthQuestion(ss + "\nAgreed?")) {
            PLAYER->SetMoney(PLAYER->GetMoney()-Price);
            SetMoney(GetMoney()+Price);
            PLAYER->CreateBodyPart(c);
            PLAYER->GetBodyPart(c)->SetHP(1);
          }
        } else if (!HasOld) {
          ADD_MESSAGE("\"You don't have your original %s with you. "
                      "I could create you a new one, but my Divine Employer "
                      "is not a communist and you need \1Y%d\2 gp first.\"",
                      PLAYER->GetBodyPartName(c).CStr(), Price);
        }
      }
    }
  }

  // cure poison
  if (PLAYER->TemporaryStateIsActivated(POISONED)) {
    res = true;
    sLong Price = (GetConfig() == VALPURUS ? 25 : 5);
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"You seem to be rather ill. I could give you a small dose of "
                  "antidote for \1Y%d\2 gold pieces.\"", Price);
      festring ss = CONST_S("One dose of antidote costs ");
      ss << "\1Y" << Price << "\2 gold pieces.\nDo you agree?";
      if (game::TruthQuestion(ss)) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(POISONED);
        PLAYER->SetMoney(PLAYER->GetMoney()-Price);
        SetMoney(GetMoney()+Price);
        //return;
      }
    } else {
      ADD_MESSAGE("\"You seem to be rather ill. Get \1Y%d\2 "
                  "gold pieces and I'll fix that.\"",
                  Price);
    }
  }

  // cure leprosy
  if (PLAYER->TemporaryStateIsActivated(LEPROSY)) {
    res = true;
    sLong Price = (GetConfig() == VALPURUS ? 100 : 20);
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"You seem to have contracted the vile disease of leprosy. "
                  "I could give you a small dose of medicince for \1Y%d\2 gold pieces.\"",
                  Price);
      festring ss = CONST_S("Getting rid of leprosy costs ");
      ss << "\1Y" << Price << "\2 gold pieces.\nDo you agree?";
      if (game::TruthQuestion(ss)) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(LEPROSY);
        PLAYER->SetMoney(PLAYER->GetMoney()-Price);
        SetMoney(GetMoney()+Price);
        //return;
      }
    } else {
      ADD_MESSAGE("\"You seem to be falling apart. Get \1Y%d\2 "
                  "gold pieces and I'll fix that.\"",
                  Price);
    }
  }

  // cure lycanthropy
  if (PLAYER->TemporaryStateIsActivated(LYCANTHROPY)) {
    res = true;
    sLong Price = GetConfig() == VALPURUS ? 100 : 20;
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"You seem to be turning into a werewolf quite frequently. "
                  "Well, everyone has right to little secret habits, but if you "
                  "wish to donate \1Y%dgp\2 to %s, maybe I could pray %s to remove "
                  "the canine blood from your veins, just so you don't scare our "
                  "blessed youth.\"", Price, GetMasterGod()->GetName(),
                  GetMasterGod()->GetObjectPronoun());
      festring ss = CONST_S("Getting rid of werewolfitis costs ");
      ss << "\1Y" << Price << "\2 gold pieces.\nDo you agree?";
      if (game::TruthQuestion(ss)) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(LYCANTHROPY);
        PLAYER->SetMoney(PLAYER->GetMoney()-Price);
        SetMoney(GetMoney()+Price);
        //return;
      }
    } else {
      ADD_MESSAGE("\"You seem to be lycanthropic. I might be able to do something "
                  "for that but I need \1Y%dgp\2 for the ritual materials first.\"",
                  Price);
    }
  }

  // cure vampirism
  if (PLAYER->TemporaryStateIsActivated(VAMPIRISM)) {
    res = true;
    sLong Price = GetConfig() == VALPURUS ? 100 : 20;
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"You seem to have an addiction to drinking blood. Well, everyone "
                  "has right to little secret habits, but if you wish to donate \1Y%dgp\2 "
                  "to %s, maybe I could pray %s to remove your vampiric urges, just so you "
                  "don't victimize our besotted youth.\"", Price, GetMasterGod()->GetName(),
                  GetMasterGod()->GetObjectPronoun());
      festring ss = CONST_S("Getting rid of vampirism costs ");
      ss << "\1Y" << Price << "\2 gold pieces.\nDo you agree?";
      if (game::TruthQuestion(ss)) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(VAMPIRISM);
        PLAYER->SetMoney(PLAYER->GetMoney()-Price);
        SetMoney(GetMoney()+Price);
        //return;
      }
    } else {
      ADD_MESSAGE("\"You seem to be vampiric. I might be able to do something for that "
                  "but I need \1Y%dgp\2 for the ritual materials first.\"", Price);
    }
  }

  // remove tape worms
  if (PLAYER->TemporaryStateIsActivated(PARASITE_TAPE_WORM)) {
    res = true;
    sLong Price = (GetConfig() == VALPURUS ? 100 : 20);
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"Ugh, there seems to be some other creature living in your body. I could try "
                  "to purge the parasite, that is if you wish to donate \1Y%dgp\2 to %s, of course.\"",
                  Price, GetMasterGod()->GetName()/*, GetMasterGod()->GetObjectPronoun()*/);
      festring ss = CONST_S("Getting rid of tape worm ");
      ss << "\1Y" << Price << "\2 gold pieces.\nDo you agree?";
      if (game::TruthQuestion(ss)) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(PARASITE_TAPE_WORM);
        PLAYER->SetMoney(PLAYER->GetMoney() - Price);
        SetMoney(GetMoney() + Price);
        //return;
      }
    } else {
      ADD_MESSAGE("\"You seem to have an unwanted guest in your guts. I can help but "
                  "I need \1Y%dgp\2 for a ritual of cleansing.\"", Price);
    }
  }

  // remove mind worms
  if (PLAYER->TemporaryStateIsActivated(PARASITE_MIND_WORM)) {
    res = true;
    sLong Price = (GetConfig() == VALPURUS ? 100 : 20);
    if (PLAYER->GetMoney() >= Price) {
      ADD_MESSAGE("\"Ugh, there seems to be some other creature living in your body. I could try "
                  "to purge the parasite, that is if you wish to donate \1Y%dgp\2 to %s, of course.\"",
                  Price, GetMasterGod()->GetName()/*, GetMasterGod()->GetObjectPronoun()*/);
      festring ss = CONST_S("Getting rid of mind worm ");
      ss << "\1Y" << Price << "\2 gold pieces.\nDo you agree?";
      if (game::TruthQuestion(CONST_S("Do you agree?"))) {
        ADD_MESSAGE("You feel better.");
        PLAYER->DeActivateTemporaryState(PARASITE_MIND_WORM);
        PLAYER->SetMoney(PLAYER->GetMoney() - Price);
        SetMoney(GetMoney() + Price);
        //return;
      }
    } else {
      ADD_MESSAGE("\"You seem to have an unwanted guest in your head. I can help but "
                  "I need \1Y%dgp\2 for a ritual of cleansing.\"", Price);
    }
  }

  return res;
}


truth priest::TryQuestTalks () {
  return PriestHealingService();
}


#endif
