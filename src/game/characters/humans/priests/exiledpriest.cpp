#ifdef HEADER_PHASE
CHARACTER(exiledpriest, priest)
{
public:
  exiledpriest ();
  virtual ~exiledpriest ();

  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth TryQuestTalks () override;
  virtual truth MoveTowardsHomePos () override;

protected:
  virtual void GetAICommand () override;
  truth HealBodyParts ();
  truth HealDeseases ();
  truth HealAll ();
  void FriendlyTalk ();

protected:
  truth mAtHome;
};


#else


//==========================================================================
//
//  exiledpriest::exiledpriest
//
//==========================================================================
exiledpriest::exiledpriest ()
  : exiledpriestsysbase()
  , mAtHome(false)
{
}


//==========================================================================
//
//  exiledpriest::~exiledpriest
//
//==========================================================================
exiledpriest::~exiledpriest () {
}


//==========================================================================
//
//  exiledpriest::Save
//
//==========================================================================
void exiledpriest::Save (outputfile &saveFile) const {
  priest::Save(saveFile);
  saveFile << mAtHome;
}


//==========================================================================
//
//  exiledpriest::Load
//
//==========================================================================
void exiledpriest::Load (inputfile &saveFile) {
  priest::Load(saveFile);
  saveFile >> mAtHome;
}


//==========================================================================
//
//  exiledpriest::HealBodyParts
//
//==========================================================================
truth exiledpriest::HealBodyParts () {
  truth res = false;
  for (int c = 0; c < PLAYER->GetBodyParts(); ++c) {
    if (!PLAYER->GetBodyPart(c) && PLAYER->CanCreateBodyPart(c)) {
      res = true;
      bool healed = false;
      truth HasOld = false;
      for (std::list<feuLong>::const_iterator i = PLAYER->GetOriginalBodyPartID(c).begin();
            i != PLAYER->GetOriginalBodyPartID(c).end(); ++i)
      {
        bodypart *OldBodyPart = static_cast<bodypart *>(PLAYER->SearchForItem(*i));
        if (OldBodyPart) {
          HasOld = true;
          if (!OldBodyPart->CanRegenerate()) {
            ADD_MESSAGE("\"Sorry, I cannot put back bodyparts made of %s, not even your severed %s.\"",
                        OldBodyPart->GetMainMaterial()->GetName(false, false).CStr(),
                        PLAYER->GetBodyPartName(c).CStr());
          } else {
            festring ss;
            ss << "\"I will put your old " << PLAYER->GetBodyPartName(c).CStr() << " back.\"";
            ADD_MESSAGE("%s", ss.CStr());
            if (game::TruthQuestion(ss + "\nDo you agree?")) {
              OldBodyPart->SetHP(1);
              OldBodyPart->RemoveFromSlot();
              PLAYER->AttachBodyPart(OldBodyPart);
              healed = true;
              break; //sorry!
            }
          }
        }
      }
      if (!healed) {
        res = true;
        festring ss;
        if (HasOld) {
          ss << "\"I could summon up a new " << PLAYER->GetBodyPartName(c).CStr() << ".\"";
        } else {
          ss << "\"Since you don't seem to have your original ";
          ss << PLAYER->GetBodyPartName(c).CStr();
          ss << " with you, I could summon up a new one.\"";
        }
        ADD_MESSAGE("%s", ss.CStr());
        ss.Empty();
        ss << "\"I can summon a new " << PLAYER->GetBodyPartName(c).CStr() << ".\"";
        if (game::TruthQuestion(ss + "\nAgreed?")) {
          PLAYER->CreateBodyPart(c);
          PLAYER->GetBodyPart(c)->SetHP(1);
        }
      }
    }
  }

  return res;
}


//==========================================================================
//
//  exiledpriest::HealDeseases
//
//==========================================================================
truth exiledpriest::HealDeseases () {
  truth res = false;

  if (PLAYER->TemporaryStateIsActivated(POISONED)) {
    res = true;
    ADD_MESSAGE("\"You seem to be rather ill. I could give you a small dose of antidote.\"");
    if (game::TruthQuestion(CONST_S("\"I can cure your poisoning.\"\nDo you agree?"))) {
      ADD_MESSAGE("You feel better.");
      PLAYER->DeActivateTemporaryState(POISONED);
    }
  }

  if (PLAYER->TemporaryStateIsActivated(PARASITE_TAPE_WORM)) {
    res = true;
    ADD_MESSAGE("\"You seem to have something inside you. "
                "I could give you a small dose of antidote.\"");
    if (game::TruthQuestion(CONST_S("\"I can remove toy tape worm.\"\nDo you agree?"))) {
      ADD_MESSAGE("You feel better.");
      PLAYER->DeActivateTemporaryState(PARASITE_TAPE_WORM);
    }
  }

  if (PLAYER->TemporaryStateIsActivated(PARASITE_MIND_WORM)) {
    res = true;
    ADD_MESSAGE("\"You seem to have something inside your brains. "
                "I could give you a small dose of antidote.\"");
    if (game::TruthQuestion(CONST_S("\"I can remove your mind worm.\"\nDo you agree?"))) {
      ADD_MESSAGE("You feel better.");
      PLAYER->DeActivateTemporaryState(PARASITE_MIND_WORM);
    }
  }

  if (PLAYER->TemporaryStateIsActivated(LEPROSY)) {
    res = true;
    ADD_MESSAGE("\"You seem to have contracted the vile disease of leprosy. "
                "I could give you a small dose of medicince.\"");
    if (game::TruthQuestion(CONST_S("\"I can cure your leprosy.\"\nDo you agree?"))) {
      ADD_MESSAGE("You feel better.");
      PLAYER->DeActivateTemporaryState(LEPROSY);
    }
  }

  if (PLAYER->TemporaryStateIsActivated(LYCANTHROPY)) {
    res = true;
    ADD_MESSAGE("\"You seem to be turning into a werewolf quite frequently. "
                "I could give you a small dose of antidote.\"");
    if (game::TruthQuestion(CONST_S("\"I can cure your werewolfitis.\"\nDo you agree?"))) {
      ADD_MESSAGE("You feel better.");
      PLAYER->DeActivateTemporaryState(LYCANTHROPY);
    }
  }

  if (PLAYER->TemporaryStateIsActivated(VAMPIRISM)) {
    res = true;
    ADD_MESSAGE("\"You seem to have an addiction to drinking blood. "
                "I could give you a small dose of antidote.\"");
    if (game::TruthQuestion(CONST_S("\"I can cure your vampirism.\"\nDo you agree?"))) {
      ADD_MESSAGE("You feel better.");
      PLAYER->DeActivateTemporaryState(VAMPIRISM);
    }
  }

  return res;
}


//==========================================================================
//
//  exiledpriest::HealAll
//
//==========================================================================
truth exiledpriest::HealAll () {
  const truth r0 = HealBodyParts();
  const truth r1 = HealDeseases();
  return (r0 || r1);
}


//==========================================================================
//
//  exiledpriest::FriendlyTalk
//
//==========================================================================
void exiledpriest::FriendlyTalk () {
  static sLong Said = 0;
  // just in case
  if (GetRelation(PLAYER) == HOSTILE) {
    priest::BeTalkedTo();
  } else {
    bool ok;
    festring msg;
    do {
      msg = GetFriendlyReplies()[RandomizeReply(Said, GetFriendlyReplies().Size)];
      if (msg.IsEmpty()) {
        ok = false;
      } else {
        char ch = msg[0];
        if (ch == '^') {
          ok = !game::GetLiberator();
          msg.Erase(0, 1);
        } else if (ch == '+') {
          ok = game::GetLiberator();
          msg.Erase(0, 1);
        } else {
          ok = true;
        }
        if (msg.IsEmpty()) ok = false;
      }
    } while (!ok);
    ProcessAndAddMessage(msg);
  }
}


//==========================================================================
//
//  exiledpriest::TryQuestTalks
//
//==========================================================================
truth exiledpriest::TryQuestTalks () {
  if (GetRelation(PLAYER) != HOSTILE && game::GetLiberator()) {
    if (!mAtHome) {
      ADD_MESSAGE(
        "Priestess says: "
        "\"Let %s bless you! I can return to my temple now! "
        "Get this things and remember how kind %s is!\"",
        GetMasterGod()->GetName(), GetMasterGod()->GetName());
      //GetMasterGod()->GetObjectPronoun());
      mAtHome = true;
      //GetGiftStack()
      {
        potion *bottle = potion::Spawn(0, NO_MATERIALS);
        bottle->InitMaterials(MAKE_MATERIAL(GLASS), MAKE_MATERIAL(HEALING_LIQUID));
        PLAYER->GetStack()->AddItem(bottle);
        ADD_MESSAGE("Priestess gives %s to you.", bottle->CHAR_DESCRIPTION(DEFINITE));
      }
      {
        potion *bottle = potion::Spawn(0, NO_MATERIALS);
        bottle->InitMaterials(MAKE_MATERIAL(GLASS), MAKE_MATERIAL(ANTIDOTE_LIQUID));
        PLAYER->GetStack()->AddItem(bottle);
        ADD_MESSAGE("Priestess gives %s to you.", bottle->CHAR_DESCRIPTION(DEFINITE));
      }
      if (RAND_N(100) >= 90) {
        potion *bottle = potion::Spawn(0, NO_MATERIALS);
        bottle->InitMaterials(MAKE_MATERIAL(GLASS), MAKE_MATERIAL(VODKA));
        PLAYER->GetStack()->AddItem(bottle);
        ADD_MESSAGE("Priestess gives %s to you.", bottle->CHAR_DESCRIPTION(DEFINITE));
      }
      ADD_MESSAGE("Also, I will heal you for free.");
    }
    if (HealAll()) return true;
    FriendlyTalk();
    return true;
  } else if (GetRelation(PLAYER) != HOSTILE) {
    if (PriestHealingService()) return true;
    FriendlyTalk();
    return true;
  }

  return false;
}


//==========================================================================
//
//  exiledpriest::MoveTowardsHomePos
//
//==========================================================================
truth exiledpriest::MoveTowardsHomePos () {
  v2 homePos = (mAtHome ? v2(12, 10) : v2(27, 21));
  if (GetPos() != homePos) {
    SetGoingTo(homePos);
    if (MoveTowardsTarget(false)) return true;
    if (GetPos().IsAdjacent(homePos)) return true;
    return MoveRandomly();
  }
  return false;
}


//==========================================================================
//
//  exiledpriest::GetAICommand
//
//==========================================================================
void exiledpriest::GetAICommand () {
  priest::GetAICommand();
}


#endif
