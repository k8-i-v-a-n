#ifdef HEADER_PHASE
CHARACTER(terra, priest)
{
public:
  terra ();
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth TryQuestTalks () override;

protected:
  truth HasBeenSpokenTo;
};


#else

terra::terra () : HasBeenSpokenTo(false) {}


void terra::Save (outputfile &SaveFile) const {
  humanoid::Save(SaveFile);
  SaveFile << HasBeenSpokenTo;
}


void terra::Load (inputfile &SaveFile) {
  humanoid::Load(SaveFile);
  SaveFile >> HasBeenSpokenTo;
}


truth terra::TryQuestTalks () {
  if (!HasBeenSpokenTo &&
      (game::GetFreedomStoryState() == 1 || game::GetLiberator()) &&
      GetRelation(PLAYER) != HOSTILE)
  {
    if (game::GetFreedomStoryState() != 2) {
      // no mango seedling yet
      game::TextScreen(
        CONST_S("\"Tweraif has been freed?! What wonderful news you bring me!\"\n\n"
                "\"I have volunteered all those years ago to be buried here in this cave\n"
                "along with the shrine, to tend it and to protect the rites and traditions\n"
                "that the Attnamese would rather see burnt and forgotten. Yet I have hoped\n"
                "every day that a word would come about an end to the tyranny, that\n"
                "I would be free to return home. I guess my hope dwindled over the years,\n"
                "but you are here now and my wishes came true. Thank you.\"\n\n"
                "\"Nevertheless, I know what you came for. A seedling of this holy tree,\n"
                "to channel the power of Silva and shroud Tweraif against further attacks.\n"
                "I wish it was that simple, but I have no seeds to give you.\""));
        game::TextScreen(
          CONST_S("\"You see, this shrine is built in a remote, lost cave for a reason.\n"
                  "It is a guarding post, a bulwark, and a seal on a prison.\"\n\n"
                  "\"One thousand years ago, Nefas, the goddess of forbidden pleasures,\n"
                  "came to Scabies, the goddess of diseases, mutations and deformity,\n"
                  "in the form of a handsome hero, and seduced her. Whether it was\n"
                  "for Nefas' own amusement, or the humiliation Scabies suffered\n"
                  "when she discovered who she laid with, no one knows, but Scabies got\n"
                  "pregnant and eventually delivered a divine baby - a monstrous spider\n"
                  "the likes of which this world had never seen before.\"\n\n"
                  "\"The spider was a behemoth of her kind, massive and terrifying\n"
                  "and truly detestable. Spurned and abandoned by both her mothers,\n"
                  "the spider rampaged through the world until she was defeated and\n"
                  "bound by a circle of druids and priests. Her name is Lobh-se and\n"
                  "she is imprisoned below this cave, trapped by the power of Silva\n"
                  "channeled through the Holy Mango Tree.\""));
        game::TextScreen(
          CONST_S("\"Lobh-se is a terrible creature, an avatar of famine and consumption.\n"
                  "She breeds thousands of lesser spiders and immediately devours them\n"
                  "in her endless hunger. She strains against her bonds and even comes here,\n"
                  "feasting on the animals attracted to the magicks of Silva, and on the few\n"
                  "plants that scrape a living this deep underground. I can somewhat keep her\n"
                  "at bay, protecting myself and the tree, but the magic of the holy seedlings\n"
                  "is sweet to Lobh-se, and not strong enough to ward her off. She devoured\n"
                  "the last seedling just a few days ago.\"\n\n"
                  "\"You are a hero already for liberating our village,\n"
                  "but if you really wish to ensure the safety of Tweraif, you have to venture\n"
                  "deeper, to the very lair of Lobh-se. She may be a godling, but her body\n"
                  "is still mortal. Cut the seedling from her gullet, and I will keep her spirit\n"
                  "bound so that it cannot create a new body to harass this world.\""));
      game::TextScreen(CONST_S("\"May Silva bless you in your doings.\""));
    } else {
      game::TextScreen(
        CONST_S("\"Tweraif has been freed?! What wonderful news you bring me!\"\n\n"
                "\"I have volunteered all those years ago to be buried here in this cave\n"
                "along with the shrine, to tend it and to protect the rites and traditions\n"
                "that the Attnamese would rather see burnt and forgotten. Yet I have hoped\n"
                "every day that a word would come about an end to the tyranny, that\n"
                "I would be free to return home. I guess my hope dwindled over the years,\n"
                "but you are here now and my wishes came true. Thank you.\""));
    }

    GetArea()->SendNewDrawRequest();
    ADD_MESSAGE("\"Oh, and give my love to Kaethos, if he's still alive.\"");

    HasBeenSpokenTo = true;
    return true;
  } else if (game::GetFreedomStoryState() == 2 && GetRelation(PLAYER) != HOSTILE) {
    //priest::BeTalkedTo(); // in case player also needs a cure, before the tip (below) to grant it wont be ignored
    PriestHealingService(); // in case player also needs a cure, before the tip (below) to grant it wont be ignored
    ADD_MESSAGE("\"You bested her, I see! Now hurry back to the village, and Attnam shall threaten us no more.\"");
    return true;
  }

  // she can heal you as all other priests
  return priest::TryQuestTalks();
}


#endif
