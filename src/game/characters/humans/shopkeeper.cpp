#ifdef HEADER_PHASE
CHARACTER(shopkeeper, humanoid)
{
public:
  shopkeeper ();

protected:
  virtual void GetAICommand () override;
};


#else


//==========================================================================
//
//  shopkeeper::shopkeeper
//
//==========================================================================
shopkeeper::shopkeeper () {
  if (!game::IsLoading()) {
    Money = GetMoney() + RAND_N(2001);
  }
}


//==========================================================================
//
//  shopkeeper::GetAICommand
//
//==========================================================================
void shopkeeper::GetAICommand () {
  StandIdleAI();
}


#endif
