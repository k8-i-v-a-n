#ifdef HEADER_PHASE
CHARACTER(siren, humanoid)
{
public:
  virtual void GetAICommand () override;
  virtual truth MoveRandomly (truth allowInterestingItems=true) override;

protected:
  virtual truth TryToSing ();

};


#else


void siren::GetAICommand () {
  if (!IsEnabled()) return;
  if (TryToSing()) return;
  humanoid::GetAICommand();
}


truth siren::TryToSing () {
  truth Success = false;
  for (int d = 0; d < GetNeighbourSquares(); ++d) {
    lsquare *Square = GetNeighbourLSquare(d);
    if (Square && Square->GetCharacter()) {
      Success = Square->GetCharacter()->ReceiveSirenSong(this);
      if (Success) break;
    }
  }
  if (Success) EditAP(-2000);
  return Success;
}


// siren sometimes will prefer to stay near non-hostile male humanoids
truth siren::MoveRandomly (truth allowInterestingItems) {
  if (!IsEnabled()) return false;

  if (GetConfig() == AMBASSADOR_SIREN) {
    return MoveRandomlyInRoom(allowInterestingItems);
  }

  if (allowInterestingItems && MoveToInterestingItem(false)) return true;

  if (RAND_N(10) < 5) {
    // if we have non-hostile humanoid male nearby, don't move
    truth found = false;
    for (int d = 0; !found && d < GetNeighbourSquares(); d += 1) {
      lsquare *Square = GetNeighbourLSquare(d);
      if (!Square) continue;
      character *Char = Square->GetCharacter();
      if (Char && GetTeam() != Char->GetTeam() &&
          Char->IsHumanoid() && Char->GetSex() == MALE)
      {
        found = IsPlayer() || (IsCharmable() || RAND_N(6) < 2);
      }
    }
    if (found) return true;
  }

  // we already tried an interesting item
  return humanoid::MoveRandomly(false);
}


#endif
