#ifdef HEADER_PHASE
CHARACTER(skeleton, humanoid)
{
public:
  virtual truth TryQuestTalks () override;
  virtual void BeTalkedTo () override;
  virtual item *SevereBodyPart (int, truth = false, stack* = 0) override;
  virtual truth BodyPartIsVital (int) const override;
  virtual sLong GetBodyPartVolume (int) const override;
  virtual truth CannotDrinkPotions () const override;
  virtual truth AllowEquipment (citem *Item, int EquipmentIndex) const override;

protected:
  virtual void CreateCorpse (lsquare *) override;
};


#else


//==========================================================================
//
//  skeleton::CannotDrinkPotions
//
//==========================================================================
truth skeleton::CannotDrinkPotions () const {
  return true;
}


//==========================================================================
//
//  skeleton::BodyPartIsVital
//
//==========================================================================
truth skeleton::BodyPartIsVital (int I) const {
  return (I == GROIN_INDEX || I == TORSO_INDEX);
}


//==========================================================================
//
//  skeleton::AllowEquipment
//
//==========================================================================
truth skeleton::AllowEquipment (citem *Item, int EquipmentIndex) const {
  if (EquipmentIndex == HELMET_INDEX && Item->IsASkull()) return false;
  return humanoid::AllowEquipment(Item, EquipmentIndex);
}


//==========================================================================
//
//  skeleton::CreateCorpse
//
//==========================================================================
void skeleton::CreateCorpse (lsquare *Square) {
  if (GetHead()) {
    item *Skull = SevereBodyPart(HEAD_INDEX, false, Square->GetStack());
    Square->AddItem(Skull);
  }

  int Amount = 2 + RAND_4;
  for (int c = 0; c < Amount; ++c) {
    Square->AddItem(bone::Spawn());
  }

  SendToHell();
}


//==========================================================================
//
//  skeleton::TryQuestTalks
//
//  to prevent sci-talk checks ;-)
//
//==========================================================================
truth skeleton::TryQuestTalks () {
  if (!GetHead()) {
    ADD_MESSAGE("The headless %s remains silent.", PLAYER->CHAR_DESCRIPTION(UNARTICLED));
    return true;
  }
  return false;
}


//==========================================================================
//
//  skeleton::BeTalkedTo
//
//==========================================================================
void skeleton::BeTalkedTo () {
  if (GetHead()) {
    humanoid::BeTalkedTo();
  } else {
    ADD_MESSAGE("The headless %s remains silent.", PLAYER->CHAR_DESCRIPTION(UNARTICLED));
  }
}


//==========================================================================
//
//  skeleton::SevereBodyPart
//
//==========================================================================
item *skeleton::SevereBodyPart (int BodyPartIndex, truth ForceDisappearance,
                                stack *EquipmentDropStack)
{
  if (BodyPartIndex == RIGHT_ARM_INDEX) {
    EnsureCurrentSWeaponSkillIsCorrect(CurrentRightSWeaponSkill, 0);
  } else if (BodyPartIndex == LEFT_ARM_INDEX) {
    EnsureCurrentSWeaponSkillIsCorrect(CurrentLeftSWeaponSkill, 0);
  }

  item *BodyPart = GetBodyPart(BodyPartIndex);
  item *Bone = 0;

  if (!ForceDisappearance) {
    bool mkMaterial = true;
    if (BodyPartIndex == HEAD_INDEX) {
      if (GetConfig() == WAR_LORD) {
        Bone = skullofxinroch::Spawn(0, NO_MATERIALS);
      } else {
        // very rarely spawn crystall skull
        Bone = skull::Spawn(0, NO_MATERIALS);
        if (!RAND_N(80)) {
          //BLUE_CRYSTAL
          //GREEN_CRYSTAL
          //PURPLE_CRYSTAL
          //MAGIC_CRYSTAL
          switch (RAND_N(5)) {
            case 0: Bone->InitMaterials(MAKE_MATERIAL(SUN_CRYSTAL)); mkMaterial = false; break;
            case 1: Bone->InitMaterials(MAKE_MATERIAL(BLUE_CRYSTAL)); mkMaterial = false; break;
            case 2: Bone->InitMaterials(MAKE_MATERIAL(GREEN_CRYSTAL)); mkMaterial = false; break;
            case 3: Bone->InitMaterials(MAKE_MATERIAL(PURPLE_CRYSTAL)); mkMaterial = false; break;
            case 4: Bone->InitMaterials(MAKE_MATERIAL(MAGIC_CRYSTAL)); mkMaterial = false; break;
          }
        }
      }
    } else {
      Bone = bone::Spawn(0, NO_MATERIALS);
    }

    if (mkMaterial) {
      Bone->InitMaterials(BodyPart->GetMainMaterial());
    }
    BodyPart->DropEquipment(EquipmentDropStack);
    BodyPart->RemoveFromSlot();
    BodyPart->SetMainMaterial(0, NO_PIC_UPDATE|NO_SIGNALS);
  } else {
    BodyPart->DropEquipment(EquipmentDropStack);
    BodyPart->RemoveFromSlot();
  }

  BodyPart->SendToHell();
  CalculateAttributeBonuses();
  CalculateBattleInfo();
  SignalPossibleTransparencyChange();
  RemoveTraps(BodyPartIndex);
  return Bone;
}


//==========================================================================
//
//  skeleton::GetBodyPartVolume
//
//==========================================================================
sLong skeleton::GetBodyPartVolume (int I) const {
  switch (I) {
    case HEAD_INDEX: return 600;
    case TORSO_INDEX: return (GetTotalVolume() - 600) * 13 / 30;
    case RIGHT_ARM_INDEX:
    case LEFT_ARM_INDEX: return (GetTotalVolume() - 600) / 10;
    case GROIN_INDEX: return (GetTotalVolume() - 600) / 10;
    case RIGHT_LEG_INDEX:
    case LEFT_LEG_INDEX: return ((GetTotalVolume() - 600) << 1) / 15;
  }
  ABORT("Illegal humanoid bodypart volume request!");
  return 0;
}


#endif
