#ifdef HEADER_PHASE
CHARACTER(smith, humanoid)
{
public:
  virtual truth TryQuestTalks () override;
  virtual void GetAICommand () override;
};


#else


void smith::GetAICommand () {
  StandIdleAI();
}


truth smith::TryQuestTalks () {
  if (GetRelation(PLAYER) == HOSTILE) {
    if (!RAND_N(50)) {
      ADD_MESSAGE("\"You motherfucker, come on, you little ass... fuck with me, eh?! "
                  "You fucking little asshole, dickhead, cocksucker... You fuckin'... "
                  "come on, come fuck with me! I'll get your ass, you jerk! Oh, you "
                  "fuckhead, motherfucker! Fuck all you and your family! Come on, you "
                  "cocksucker, slime bucket, shitface, turdball! Come on, you scum "
                  "sucker, you fucking with me?! Come on, you asshole!\"");
    } else {
      ADD_MESSAGE("\"You talkin' to me? You talkin' to me? You talkin' to me? "
                  "Then who the hell else are you talkin' to? You talkin' to me? "
                  "Well I'm the only one here. Who do you think you're talking to? "
                  "Oh yeah? Huh? Ok.\"");
    }
    return true;
  }

  if (!GetMainWielded() || !GetMainWielded()->CanBeUsedBySmith()) {
    ADD_MESSAGE("\"Sorry, I need an intact hammer to practise the art of smithing.\"");
    return true;
  }

  felist ActionList(CONST_S("What do you want from me?"));
  ActionList.RemoveFlags(FADE);
  ActionList.AddEntry(CONST_S("Fix something"), LIGHT_GRAY);
  ActionList.AddEntry(CONST_S("Harden something"), LIGHT_GRAY);
  ActionList.AddEntry(CONST_S("Buy a strong-box"), LIGHT_GRAY);
  game::SetStandardListAttributes(ActionList);
  ActionList.AddFlags(SELECTABLE | FELIST_NO_BADKEY_EXIT);
  int Selected = ActionList.Draw();
  if (Selected == ESCAPED) {
    return true;
  }

  if (Selected == 0) {
    if (PLAYER->PossessesItem(&item::IsFixableBySmith)) {
      item *Item = PLAYER->SelectFromPossessions(CONST_S("\"What do you want me to fix?\""),
                                                 &item::IsFixableBySmith);
      if (Item) {
        // the smith can break the lock for you.
        // as it is done with a hammer, it's not particually gentle job.
        if (Item->IsLockableContainer()) {
          sLong OpenPrice = Max(10, Item->GetFixPrice() / 2);
          if (PLAYER->GetMoney() < OpenPrice) {
            ADD_MESSAGE("\"Getting the lock open cost you \1Y%d\2 gold pieces. "
                        "Get the money and we'll talk.\"", OpenPrice);
          } else {
            festring msg;
            msg << "\"I can open your " << Item->CHAR_NAME(UNARTICLED);
            msg << ", but it'll cost you \1Y" << OpenPrice << "\2 gold pieces.\"";
            ADD_MESSAGE("%s", msg.CStr());
            msg.Empty();
            msg << "\"I can open your \1Y" << Item->CHAR_NAME(UNARTICLED);
            msg << "\2 for \1Y" << OpenPrice << "\2 gold pieces.\"";
            if (game::TruthQuestion(msg + "\nDo you accept this deal?")) {
              PLAYER->EditMoney(-OpenPrice);
              ADD_MESSAGE("%s strikes %s with the hammer.",
                          CHAR_NAME(DEFINITE), Item->CHAR_NAME(DEFINITE));
              Item->ReceiveDamage(this, 10 + Item->GetStrengthValue() * 4, PHYSICAL_DAMAGE);
            }
          }
          return true;
        }

        if (!(Item->GetMainMaterial()->GetCategoryFlags() & IS_METAL)) {
          ADD_MESSAGE("\"I only fix items made of metal.\"");
          return true;
        }

        /** update messages */
        sLong FixPrice = Item->GetFixPrice();
        if (PLAYER->GetMoney() < FixPrice) {
          ADD_MESSAGE("\"Getting that fixed costs you \1Y%d\2 gold pieces. "
                      "Get the money and we'll talk.\"", FixPrice);
          return true;
        }

        ADD_MESSAGE("\"I can fix your %s, but it'll cost you \1Y%d\2 gold pieces.\"",
                    Item->CHAR_NAME(UNARTICLED), FixPrice);
        festring msg;
        msg << "\"I can fix your \1Y" << Item->CHAR_NAME(UNARTICLED);
        msg << "\2 for \1Y" << FixPrice << "\2 gold pieces.\"";
        if (game::TruthQuestion(msg + "\nDo you accept this deal?")) {
          Item->RemoveRust();
          Item->Fix();
          PLAYER->EditMoney(-FixPrice);
          ADD_MESSAGE("%s fixes %s in no time.", CHAR_NAME(DEFINITE), Item->CHAR_NAME(DEFINITE));
        }

        return true;
      }
    } else {
      ADD_MESSAGE("\"Come back when you have some weapons or armor I can fix.\"");
    }
    return true;
  }

  if (Selected == 1) {
    if (PLAYER->PossessesItem(&item::IsUpgradeableBySmith)) {
      item *Item = PLAYER->SelectFromPossessions(CONST_S("\"What do you want me to harden?\""),
                                                 &item::IsUpgradeableBySmith);
      if (Item) {
        if (!(Item->GetMainMaterial()->GetCategoryFlags() & IS_METAL)) {
          ADD_MESSAGE("\"I only harden items made of metal.\"");
          return true;
        }

        sLong HardenPrice = Item->GetPrice();
        //HardenPrice = HardenPrice + (HardenPrice / 2) + Item->GetFixPrice() * 4;
        HardenPrice = HardenPrice * 2 + Item->GetFixPrice() * 4;

        if (PLAYER->GetMoney() < HardenPrice) {
          ADD_MESSAGE("\"Getting that hardened costs you \1Y%d\2 gold pieces. "
                      "Get the money and we'll talk.\"", HardenPrice);
          return true;
        }

        ADD_MESSAGE("\"I can harden your %s, but it'll cost you \1Y%d\2 gold pieces.\"",
                    Item->CHAR_NAME(UNARTICLED), HardenPrice);
        festring msg;
        msg << "\"I can harden your \1Y" << Item->CHAR_NAME(UNARTICLED);
        msg << " for \1Y" << HardenPrice << "\2 gold pieces.\"";
        if (game::TruthQuestion(msg + "\nDo you accept this deal?")) {
          Item->RemoveRust();
          itemvector itemList;
          itemList.push_back(Item);
          if (TryToHardenItem(itemList)) {
            ADD_MESSAGE("As the fire dies out, %s looks much harder.",
                        Item->CHAR_NAME(DEFINITE));
            PLAYER->EditMoney(-HardenPrice);
          }
        }
      }
      return true;
    } else {
      ADD_MESSAGE("\"Come back when you have some weapons or armor I can harden.\"");
    }
    return true;
  }

  if (Selected == 2) {
    itemcontainer *SBox = itemcontainer::Spawn(EMPTY_STRONG_BOX);
    SBox->SetIsLocked(0);
    SBox->SetIsLocked(false);
    sLong price = SBox->GetTruePrice();
    if (PLAYER->GetMoney() < price) {
      ADD_MESSAGE("\"The strong-box costs \1Y%d\2 gold pieces. "
                  "Get the money and we'll talk.\"", price);
      delete SBox;
    } else {
      festring msg = CONST_S("The strong-box costs \1Y") + price + "\2 gold pieces.";
      ADD_MESSAGE("\"%s\"", msg.CStr());
      if (game::TruthQuestion(msg + "\nDo you accept this deal?")) {
        PLAYER->GetStack()->AddItem(SBox);
        PLAYER->EditMoney(-price);
      } else {
        delete SBox;
      }
    }
    return true;
  }

  return true;
}


#endif
