#ifdef HEADER_PHASE
CHARACTER(tailor, humanoid)
{
public:
  virtual truth TryQuestTalks () override;
  virtual void GetAICommand () override;
};


#else


void tailor::GetAICommand () {
  StandIdleAI();
}


truth tailor::TryQuestTalks () {
  if (GetRelation(PLAYER) == HOSTILE) {
    if (!RAND_N(50)) {
      ADD_MESSAGE("\"You motherfucker, come on, you little ass... fuck with me, eh?! "
                  "You fucking little asshole, dickhead, cocksucker... You fuckin'... "
                  "come on, come fuck with me! I'll get your ass, you jerk! Oh, you "
                  "fuckhead, motherfucker! Fuck all you and your family! Come on, you "
                  "cocksucker, slime bucket, shitface, turdball! Come on, you scum "
                  "sucker, you fucking with me?! Come on, you asshole!\"");
    } else {
      ADD_MESSAGE("\"You talkin' to me? You talkin' to me? You talkin' to me? "
                  "Then who the hell else are you talkin' to? You talkin' to me? "
                  "Well I'm the only one here. Who do you think you're talking to? "
                  "Oh yeah? Huh? Ok.\"");
    }
    return true;
  }

  felist ActionList(CONST_S("What do you want from me?"));
  ActionList.RemoveFlags(FADE);
  ActionList.AddEntry(CONST_S("Fix something"), LIGHT_GRAY);
  ActionList.AddEntry(CONST_S("Harden something"), LIGHT_GRAY);
  game::SetStandardListAttributes(ActionList);
  ActionList.AddFlags(SELECTABLE | FELIST_NO_BADKEY_EXIT);
  int Selected = ActionList.Draw();
  if (Selected == ESCAPED) {
    return true;
  }

  if (Selected == 0) {
    if (PLAYER->PossessesItem(&item::IsFixableByTailor)) {
      item *Item = PLAYER->SelectFromPossessions(CONST_S("\"What do you want me to fix?\""),
                                                 &item::IsFixableByTailor);
      if (Item) {
        if (!(Item->GetMainMaterial()->GetCategoryFlags() & CAN_BE_TAILORED)) {
          ADD_MESSAGE("\"I can't work on %s.\"", Item->GetMainMaterial()->GetName(false, false).CStr());
          return true;
        }

        /** update messages */
        sLong FixPrice = Item->GetFixPrice();
        if (PLAYER->GetMoney() < FixPrice) {
          ADD_MESSAGE("\"Getting that fixed costs you \1Y%d\2 gold pieces. "
                      "Get the money and we'll talk.\"", FixPrice);
          return true;
        }

        ADD_MESSAGE("\"I can fix your %s, but it'll cost you \1Y%d\2 gold pieces.\"",
                    Item->CHAR_NAME(UNARTICLED), FixPrice);
        festring msg;
        msg << "\"I can fix your \1Y" << Item->CHAR_NAME(UNARTICLED);
        msg << "\2 for \1Y" << FixPrice << "\2 gold pieces.\"";
        if (game::TruthQuestion(msg + "\nDo you accept this deal?")) {
          Item->Fix();
          PLAYER->EditMoney(-FixPrice);
          ADD_MESSAGE("%s fixes %s in no time.", CHAR_NAME(DEFINITE), Item->CHAR_NAME(DEFINITE));
        }

        return true;
      }
    } else {
      ADD_MESSAGE("\"Come back when you have some weapons or armor I can fix.\"");
    }
    return true;
  }

  if (PLAYER->PossessesItem(&item::IsUpgradeableByTailor)) {
    item *Item = PLAYER->SelectFromPossessions(CONST_S("\"What do you want me to harden?\""),
                                               &item::IsUpgradeableByTailor);
    if (Item) {
      if (!(Item->GetMainMaterial()->GetCategoryFlags() & CAN_BE_TAILORED)) {
        ADD_MESSAGE("\"I can't work on %s.\"", Item->GetMainMaterial()->GetName(false, false).CStr());
        return true;
      }

      sLong HardenPrice = Item->GetPrice();
      //HardenPrice = HardenPrice + (HardenPrice / 2) + Item->GetFixPrice() * 4;
      HardenPrice = HardenPrice * 2 + Item->GetFixPrice() * 4;

      if (PLAYER->GetMoney() < HardenPrice) {
        ADD_MESSAGE("\"Getting that hardened costs you \1Y%d\2 gold pieces. "
                    "Get the money and we'll talk.\"", HardenPrice);
        return true;
      }

      ADD_MESSAGE("\"I can harden your %s, but it'll cost you \1Y%d\2 gold pieces.\"",
                  Item->CHAR_NAME(UNARTICLED), HardenPrice);
      festring msg;
      msg << "\"I can harden your \1Y" << Item->CHAR_NAME(UNARTICLED);
      msg << "\2 for \1Y" << HardenPrice << "\2 gold pieces.\"";
      if (game::TruthQuestion(msg + "\nDo you accept this deal?")) {
        Item->RemoveRust();
        itemvector itemList;
        itemList.push_back(Item);
        if (TryToHardenItem(itemList)) {
          ADD_MESSAGE("Oh, %s looks much harder now.", Item->CHAR_NAME(DEFINITE));
          PLAYER->EditMoney(-HardenPrice);
        }
      }
    } else {
      ADD_MESSAGE("\"Come back when you have some weapons or armor I can harden.\"");
    }
    return true;
  }

  return true;
}


#endif
