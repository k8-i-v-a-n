#ifdef HEADER_PHASE
CHARACTER(fruitbat, bat)
{
public:
  virtual void GetAICommand () override;
  virtual truth IsRetreating () const override;
};


#else


truth fruitbat::IsRetreating () const {
  if (nonhumanoid::IsRetreating()) return true;
  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
    if ((*i)/*->GetMainMaterial()->IsFruit()*/->IsFood()) return true;
  }
  return false;
}


void fruitbat::GetAICommand () {
  if (!IsRetreating()) {
    character *Char = GetRandomNeighbour();
    if (Char) {
      itemvector Fruits;
      for (stackiterator i = Char->GetStack()->GetBottom(); i.HasItem(); ++i) {
        if (((*i)->IsFood()/*->GetMainMaterial()->IsFruit())*/) && !MakesBurdened((*i)->GetWeight())) {
          Fruits.push_back(*i);
        }
      }
      if (!Fruits.empty()) {
        item *ToSteal = Fruits[RAND_N(Fruits.size())];
        StealItemFrom(Char, ToSteal);
        EditAP(-500);
        return;
      }
    }
  }
  nonhumanoid::GetAICommand();
}


#endif
