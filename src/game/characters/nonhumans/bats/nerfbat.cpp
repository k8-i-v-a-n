#ifdef HEADER_PHASE
CHARACTER(nerfbat, bat)
{
protected:
  virtual int TakeHit (character*, item*, bodypart*, v2, double, double, int, int, int, truth, truth) override;
};


#else


int nerfbat::TakeHit (character *Enemy, item *Weapon, bodypart *EnemyBodyPart,
                      v2 HitPos, double Damage, double ToHitValue, int Success,
                      int Type, int Direction, truth Critical, truth ForceHit)
{
  int Return = nonhumanoid::TakeHit(Enemy, Weapon, EnemyBodyPart, HitPos, Damage, ToHitValue,
                                    Success, Type, Direction, Critical, ForceHit);

  if (Return != HAS_DIED) {
    // Compare Mana against enemy Willpower to see if they resist polymorph.
    // comm. fork: WILL_POWER
    // k8: we have no use of willpower. have to find another stat.
    // k8: FIXME! this is shit.
    if (RAND_N(GetMana()) > RAND_N(Enemy->GetMana() * 2)) {
      if (IsPlayer()) {
        ADD_MESSAGE("You are engulfed in a malignant aura!");
      } else if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s is engulfed in a malignant aura!", CHAR_DESCRIPTION(DEFINITE));
      }

      if (Weapon) {
        Weapon->Polymorph(this, Enemy);
      } else if (EnemyBodyPart) {
        Enemy->PolymorphRandomly(1, 999999, (int)(Damage * 300 + RAND_N(500)));
      }
    } else {
      if (IsPlayer()) {
        ADD_MESSAGE("You are engulfed in a malignant aura, but nothing seems to happen.");
      } else if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s is engulfed in a malignant aura, but nothing seems to happen.", CHAR_DESCRIPTION(DEFINITE));
      }

      Enemy->EditExperience(WILL_POWER, 100, 1 << 12);
    }
  }

  return Return;
}


#endif
