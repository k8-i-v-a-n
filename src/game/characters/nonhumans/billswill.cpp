#ifdef HEADER_PHASE
CHARACTER(billswill, nonhumanoid)
{
protected:
  virtual int GetBodyPartWobbleData (int) const override;
  virtual cchar *FirstPersonBiteVerb () const override;
  virtual cchar *FirstPersonCriticalBiteVerb () const override;
  virtual cchar *ThirdPersonBiteVerb () const override;
  virtual cchar *ThirdPersonCriticalBiteVerb () const override;
  virtual truth AttackIsBlockable (int) const override;
  virtual truth AttackMayDamageArmor () const override;
};


#else


cchar *billswill::FirstPersonBiteVerb() const { return "emit psi waves at"; }
cchar *billswill::FirstPersonCriticalBiteVerb() const { return "emit powerful psi waves at"; }
cchar *billswill::ThirdPersonBiteVerb() const { return "emits psi waves at"; }
cchar *billswill::ThirdPersonCriticalBiteVerb() const { return "emits powerful psi waves at"; }
int billswill::GetBodyPartWobbleData(int) const { return WOBBLE_HORIZONTALLY|(2 << WOBBLE_FREQ_SHIFT); }
truth billswill::AttackIsBlockable (int) const { return false; }
truth billswill::AttackMayDamageArmor () const { return false; }


#endif
