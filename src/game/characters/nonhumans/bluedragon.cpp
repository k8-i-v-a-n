#ifdef HEADER_PHASE
CHARACTER(bluedragon, nonhumanoid)
{
public:
  virtual truth CannotDrinkPotions () const override;
};


#else


// this allows potion drinking
truth bluedragon::CannotDrinkPotions () const { return false; }


#endif
