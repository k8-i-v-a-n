#ifdef HEADER_PHASE
CHARACTER(canine, nonhumanoid)
{
public:
  virtual truth NeedCheckNearbyItems () const override;
};


#else

//==========================================================================
//
//  canine::NeedCheckNearbyItems
//
//  canines look for food
//
//==========================================================================
truth canine::NeedCheckNearbyItems () const {
  return true;
}

#endif
