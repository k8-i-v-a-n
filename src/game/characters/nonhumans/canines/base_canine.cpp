#ifdef HEADER_PHASE
CHARACTER(canine, nonhumanoid)
{
public:
  virtual truth Catches (item *) override;
  virtual truth NeedCheckNearbyItems () const override;

protected:
  virtual truth WantCatchIt (citem *Item) const; // default is `true`
};


#else

//==========================================================================
//
//  canine::NeedCheckNearbyItems
//
//  canines look for food
//
//==========================================================================
truth canine::NeedCheckNearbyItems () const {
  return true;
}


//==========================================================================
//
//  canine::WantCatchIt
//
//==========================================================================
truth canine::WantCatchIt (citem *Item) const {
  return true;
}


//==========================================================================
//
//  canine::Catches
//
//==========================================================================
truth canine::Catches (item *Thingy) {
  if (WantCatchIt(Thingy) && Thingy->DogWillCatchAndConsume(this)) {
    if (ConsumeItem(Thingy, CONST_S("eating"))) {
      if (IsPlayer()) {
        ADD_MESSAGE("You catch %s in mid-air and consume it.", Thingy->CHAR_NAME(DEFINITE));
      } else {
        if (CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s catches %s and eats it.",
                      CHAR_NAME(DEFINITE), Thingy->CHAR_NAME(DEFINITE));
        }
        ChangeTeam(PLAYER->GetTeam());
      }
    } else if (IsPlayer()) {
      ADD_MESSAGE("You catch %s in mid-air.", Thingy->CHAR_NAME(DEFINITE));
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s catches %s.", CHAR_NAME(DEFINITE), Thingy->CHAR_NAME(DEFINITE));
    }
    return true;
  }
  return false;
}


#endif
