#ifdef HEADER_PHASE
CHARACTER(blinkdog, dog)
{
public:
  blinkdog ();

  virtual void Save (outputfile&) const override;
  virtual void Load (inputfile&) override;
  virtual int TakeHit (character*, item*, bodypart*, v2, double, double, int, int, int, truth, truth) override;
  virtual truth SpecialEnemySightedReaction (character*) override;

protected:
  virtual bodypart *MakeBodyPart (int) const override;
  void MonsterTeleport (cchar *);
  truth SummonFriend ();

protected:
  int SummonModifier;
};


#else


//==========================================================================
//
//  blinkdog::blinkdog
//
//==========================================================================
blinkdog::blinkdog () {
  if (!game::IsLoading()) {
    SummonModifier = RAND_2+RAND_2+RAND_2+RAND_2+RAND_2+RAND_2+RAND_2;
  }
}


//==========================================================================
//
//  blinkdog::MakeBodyPart
//
//==========================================================================
bodypart *blinkdog::MakeBodyPart (int) const {
  return blinkdogtorso::Spawn(0, NO_MATERIALS);
}


//==========================================================================
//
//  blinkdog::SpecialEnemySightedReaction
//
//==========================================================================
truth blinkdog::SpecialEnemySightedReaction (character *) {
  if (!RAND_16 && SummonFriend()) {
    return true;
  }
  if (!RAND_32) {
    MonsterTeleport("a playful bark");
    return true;
  }
  if ((!RAND_4 && StateIsActivated(PANIC)) || (!RAND_8 && IsInBadCondition())) {
    MonsterTeleport("a frightened howl");
    return true;
  }
  return false;
}


//==========================================================================
//
//  blinkdog::MonsterTeleport
//
//==========================================================================
void blinkdog::MonsterTeleport (cchar *BarkMsg) {
  if (IsPlayer()) {
    ADD_MESSAGE("You vanish.");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("You hear %s inside your head as %s vanishes!", BarkMsg, CHAR_NAME(DEFINITE));
  } else {
    ADD_MESSAGE("You hear %s inside your head.", BarkMsg);
  }

  v2 Pos = GetPos();
  rect Border(Pos+v2(-5, -5), Pos+v2(5, 5));

  Pos = GetLevel()->GetRandomSquare(this, 0, &Border);
  if (Pos == ERROR_V2) {
    Pos = GetLevel()->GetRandomSquare(this);
    if (Pos == ERROR_V2) ABORT("No country for the old blinking dog...");
  }

  Move(Pos, true);

  if (IsPlayer()) {
    ADD_MESSAGE("You materialize.");
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s materializes from nowhere!", CHAR_NAME(INDEFINITE));
  }

  EditAP(-1000);
}


//==========================================================================
//
//  blinkdog::TakeHit
//
//==========================================================================
int blinkdog::TakeHit (character *Enemy, item *Weapon, bodypart *EnemyBodyPart, v2 HitPos,
                       double Damage, double ToHitValue, int Success, int Type, int Direction,
                       truth Critical, truth ForceHit)
{
  int Return = nonhumanoid::TakeHit(Enemy, Weapon, EnemyBodyPart, HitPos, Damage, ToHitValue, Success, Type, Direction, Critical, ForceHit);
  if (Return != HAS_DIED) {
    if (!RAND_16 && SummonFriend()) return Return;
    if ((RAND_2 && StateIsActivated(PANIC)) ||
        (!RAND_4 && IsInBadCondition()) || !RAND_16)
    {
      MonsterTeleport("a terrified yelp");
    }
  }
  return Return;
}


//==========================================================================
//
//  blinkdog::SummonFriend
//
//==========================================================================
truth blinkdog::SummonFriend () {
  if (!SummonModifier) return false;

  SummonModifier -= 1;
  blinkdog *Buddy = blinkdog::Spawn();
  Buddy->SummonModifier = SummonModifier;
  Buddy->SetTeam(GetTeam());
  Buddy->SetGenerationDanger(GetGenerationDanger());

  if (!Buddy->PutNear(GetPos(), true)) {
    //Buddy->Disable();
    //Buddy->SendToHell();
    delete Buddy;
    return false;
  }

  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s wags its tail in a mysterious pattern.", CHAR_NAME(DEFINITE));
    if (Buddy->CanBeSeenByPlayer()) ADD_MESSAGE("Another of its kin appears!");
  } else if (Buddy->CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s appears!", Buddy->CHAR_NAME(INDEFINITE));
  }

  EditAP(-1000);
  return true;
}


//==========================================================================
//
//  blinkdog::Save
//
//==========================================================================
void blinkdog::Save (outputfile& SaveFile) const {
  dog::Save(SaveFile);
  SaveFile << SummonModifier;
}


//==========================================================================
//
//  blinkdog::Load
//
//==========================================================================
void blinkdog::Load (inputfile& SaveFile) {
  dog::Load(SaveFile);
  SaveFile >> SummonModifier;
}


#endif
