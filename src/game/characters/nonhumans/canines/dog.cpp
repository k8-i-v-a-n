#ifdef HEADER_PHASE
CHARACTER(dog, canine)
{
public:
  virtual void BeTalkedTo () override;
  virtual void CreateCorpse (lsquare *Square) override;
  virtual truth EatToBloatedState () const override;

protected:
  virtual bodypart *MakeBodyPart (int) const override;
  virtual void GetAICommand ();
};


#else


//==========================================================================
//
//  dog::EatToBloatedState
//
//  dogs are stupid
//
//==========================================================================
truth dog::EatToBloatedState () const {
  return true;
}


//==========================================================================
//
//  dog::CannotDrinkPotions
//
//==========================================================================
/*
no dogs can
truth dog::CannotDrinkPotions () const {
  return (GetConfig() == SKELETON_DOG);
}
*/


//==========================================================================
//
//  dog::MakeBodyPart
//
//==========================================================================
bodypart *dog::MakeBodyPart (int) const {
  return dogtorso::Spawn(0, NO_MATERIALS);
}


//==========================================================================
//
//  dog::BeTalkedTo
//
//==========================================================================
void dog::BeTalkedTo () {
  if (StateIsActivated(CONFUSED)) {
    ADD_MESSAGE("%s looks a bit confused: \"Meow.\"", CHAR_NAME(DEFINITE));
    return;
  }

  if (RAND_N(5)) {
    if (GetRelation(PLAYER) != HOSTILE) {
      static truth Last;
      cchar *Reply;
      if (GetHP() * 2 > GetMaxHP()) {
        Reply = (Last ? "barks happily" : "wags its tail happily");
      } else {
        Reply = (Last ? "yelps" : "howls");
      }
      ADD_MESSAGE("%s %s.", CHAR_NAME(DEFINITE), Reply);
      Last = !Last;
    } else {
      character::BeTalkedTo();
    }
  } else if (!RAND_N(5)) {
    ADD_MESSAGE("\"Can't you understand I can't speak?\"");
  } else if (!RAND_N(5)) {
    ADD_MESSAGE("\"Meow.\"");
  } else {
    character::BeTalkedTo();
  }
}


//==========================================================================
//
//  dog::GetAICommand
//
//==========================================================================
void dog::GetAICommand () {
  if (!game::IsInWilderness()) {
    int droolType = (GetConfig() == SKELETON_DOG ? ECTOPLASM_LIQUID : DOG_DROOL);
    if (!RAND_8) {
      GetLSquareUnder()->SpillFluid(this, liquid::Spawn(droolType, 15+RAND_N(40)), false, false);
    }
  }
  character::GetAICommand();
}


//==========================================================================
//
//  dog::CreateCorpse
//
//==========================================================================
void dog::CreateCorpse (lsquare *Square) {
  if (GetConfig() == SKELETON_DOG) {
    Square->AddItem(skull::Spawn(PUPPY_SKULL));
    int Amount = 2 + RAND_2;
    for (int c = 0; c < Amount; ++c) {
      Square->AddItem(bone::Spawn());
    }
    SendToHell();
  } else {
    canine::CreateCorpse(Square);
  }
}


#endif
