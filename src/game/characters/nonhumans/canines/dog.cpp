#ifdef HEADER_PHASE
CHARACTER(dog, canine)
{
public:
  virtual truth Catches (item *) override;
  virtual void BeTalkedTo () override;
  virtual void CreateCorpse (lsquare *Square) override;
  virtual truth EatToBloatedState () const override;

protected:
  virtual bodypart *MakeBodyPart (int) const override;
  virtual void GetAICommand ();
};


#else


//==========================================================================
//
//  dog::EatToBloatedState
//
//  dogs are stupid
//
//==========================================================================
truth dog::EatToBloatedState () const {
  return true;
}


//==========================================================================
//
//  dog::CannotDrinkPotions
//
//==========================================================================
/*
no dogs can
truth dog::CannotDrinkPotions () const {
  return (GetConfig() == SKELETON_DOG);
}
*/


//==========================================================================
//
//  dog::MakeBodyPart
//
//==========================================================================
bodypart *dog::MakeBodyPart (int) const {
  return dogtorso::Spawn(0, NO_MATERIALS);
}


//==========================================================================
//
//  dog::Catches
//
//==========================================================================
truth dog::Catches (item *Thingy) {
  if (Thingy->DogWillCatchAndConsume(this)) {
    if (ConsumeItem(Thingy, CONST_S("eating"))) {
      if (IsPlayer()) {
        ADD_MESSAGE("You catch %s in mid-air and consume it.", Thingy->CHAR_NAME(DEFINITE));
      } else {
        if (CanBeSeenByPlayer()) ADD_MESSAGE("%s catches %s and eats it.", CHAR_NAME(DEFINITE), Thingy->CHAR_NAME(DEFINITE));
        ChangeTeam(PLAYER->GetTeam());
      }
    } else if (IsPlayer()) {
      ADD_MESSAGE("You catch %s in mid-air.", Thingy->CHAR_NAME(DEFINITE));
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s catches %s.", CHAR_NAME(DEFINITE), Thingy->CHAR_NAME(DEFINITE));
    }
    return true;
  }
  return false;
}



//==========================================================================
//
//  dog::BeTalkedTo
//
//==========================================================================
void dog::BeTalkedTo () {
  if (StateIsActivated(CONFUSED)) {
    ADD_MESSAGE("%s looks a bit confused: \"Meow.\"", CHAR_NAME(DEFINITE));
    return;
  }

  if (RAND_N(5)) {
    if (GetRelation(PLAYER) != HOSTILE) {
      static truth Last;
      cchar *Reply;
      if (GetHP() * 2 > GetMaxHP()) {
        Reply = (Last ? "barks happily" : "wags its tail happily");
      } else {
        Reply = (Last ? "yelps" : "howls");
      }
      ADD_MESSAGE("%s %s.", CHAR_NAME(DEFINITE), Reply);
      Last = !Last;
    } else {
      character::BeTalkedTo();
    }
  } else if (!RAND_N(5)) {
    ADD_MESSAGE("\"Can't you understand I can't speak?\"");
  } else if (!RAND_N(5)) {
    ADD_MESSAGE("\"Meow.\"");
  } else {
    character::BeTalkedTo();
  }
}


//==========================================================================
//
//  dog::GetAICommand
//
//==========================================================================
void dog::GetAICommand () {
  if (!game::IsInWilderness()) {
    if (GetConfig() == SKELETON_DOG) {
      if (!RAND_8) {
        GetLSquareUnder()->SpillFluid(this, liquid::Spawn(ECTOPLASM, 15+RAND_N(40)), false, false);
      }
    } else {
      if (!RAND_8) {
        GetLSquareUnder()->SpillFluid(this, liquid::Spawn(DOG_DROOL, 25+RAND_N(50)), false, false);
      }
    }
  }
  character::GetAICommand();
}


//==========================================================================
//
//  dog::CreateCorpse
//
//==========================================================================
void dog::CreateCorpse (lsquare *Square) {
  if (GetConfig() == SKELETON_DOG) {
    Square->AddItem(skull::Spawn(PUPPY_SKULL));
    int Amount = 2 + RAND_2;
    for (int c = 0; c < Amount; ++c) {
      Square->AddItem(bone::Spawn());
    }
    SendToHell();
  } else {
    canine::CreateCorpse(Square);
  }
}


#endif
