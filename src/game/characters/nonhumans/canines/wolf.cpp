#ifdef HEADER_PHASE
CHARACTER(wolf, canine)
{
protected:
  virtual truth WantCatchIt (citem *Item) const override;
  virtual col16 GetSkinColor () const override;
};


#else


truth wolf::WantCatchIt (citem *Item) const { return false; }


col16 wolf::GetSkinColor () const {
  int Element = 40 + RAND_N(50);
  return MakeRGB16(Element, Element, Element);
}


#endif
