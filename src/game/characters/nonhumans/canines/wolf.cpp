#ifdef HEADER_PHASE
CHARACTER(wolf, canine)
{
protected:
  virtual col16 GetSkinColor () const override;
};


#else


col16 wolf::GetSkinColor () const {
  int Element = 40 + RAND_N(50);
  return MakeRGB16(Element, Element, Element);
}


#endif
