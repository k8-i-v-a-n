#ifdef HEADER_PHASE
CHARACTER(carnivorousplant, nonhumanoid)
{
protected:
  virtual col16 GetTorsoSpecialColor () const override;
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;
};


#else


// the flower
col16 carnivorousplant::GetTorsoSpecialColor () const {
  if (!GetConfig()) return MakeRGB16(RAND_N(100), 125 + RAND_N(125), RAND_N(100));
  if (GetConfig() == GREATER) return MakeRGB16(RAND_N(100), RAND_N(100), 125 + RAND_N(125));
  return MakeRGB16(125 + RAND_N(125), 125 + RAND_N(125), RAND_N(100));
}


void carnivorousplant::CreateCorpse (lsquare *Square) {
  int Amount =   !GetConfig() ? (RAND_N(7) ? 0 : 1)
               : GetConfig() == GREATER ? (RAND_2 ? 0 : (RAND_N(5) ? 1 : (RAND_N(5) ? 2 : 3)))
               : (!RAND_N(3) ? 0 : (RAND_N(3) ? 1 : (RAND_N(3) ? 2 : 3)));
  for (int c = 0; c < Amount; ++c) {
    Square->AddItem(kiwi::Spawn());
  }
  nonhumanoid::CreateCorpse(Square);
}


void carnivorousplant::GetAICommand () {
  SeekLeader (GetLeader());
  if (FollowLeader(GetLeader())) return;
  if (AttackAdjacentEnemyAI()) return;
  if (CheckForUsefulItemsOnGround()) return;
  if (MoveRandomly()) return;
  EditAP(-1000);
}


#endif
