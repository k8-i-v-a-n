#ifdef HEADER_PHASE
CHARACTER(chameleon, nonhumanoid)
{
public:
  virtual int TakeHit (character *, item *, bodypart *, v2, double, double, int, int, int, truth, truth) override;
  virtual truth SpecialEnemySightedReaction (character *) override;

protected:
  virtual col16 GetSkinColor () const override;
  virtual void SpecialTurnHandler () override;
};


#else



col16 chameleon::GetSkinColor() const { return MakeRGB16(60 + RAND_N(190), 60 + RAND_N(190), 60 + RAND_N(190)); }
void chameleon::SpecialTurnHandler() { UpdatePictures(); }


truth chameleon::SpecialEnemySightedReaction (character *) {
  if (HP != MaxHP || !RAND_N(3)) {
    character *NewForm = PolymorphRandomly(100, 1000, 500 + RAND_N(500));
    NewForm->GainIntrinsic(POLYMORPH);
    return true;
  }
  return false;
}


int chameleon::TakeHit (character *Enemy, item *Weapon, bodypart *EnemyBodyPart, v2 HitPos, double Damage, double ToHitValue, int Success, int Type, int Direction, truth Critical, truth ForceHit) {
  int Return = nonhumanoid::TakeHit(Enemy, Weapon, EnemyBodyPart, HitPos, Damage, ToHitValue, Success, Type, Direction, Critical, ForceHit);
  if (Return != HAS_DIED) {
    character *NewForm = PolymorphRandomly(100, 1000, 500 + RAND_N(500));
    NewForm->GainIntrinsic(POLYMORPH);
  }
  return Return;
}


#endif
