#ifdef HEADER_PHASE
CHARACTER(dolphin, nonhumanoid)
{
protected:
  virtual int GetSpecialBodyPartFlags (int) const override;
  virtual void SpecialTurnHandler () override;
};


#else


int dolphin::GetSpecialBodyPartFlags (int) const { return /*RAND()*/RAND_256 & (MIRROR|ROTATE); }
void dolphin::SpecialTurnHandler () { UpdatePictures(); }


#endif
