#ifdef HEADER_PHASE
CHARACTER(eddy, nonhumanoid)
{
public:
  virtual truth Hit (character *, v2, int, int = 0) override;
  virtual truth NeedCheckNearbyItems () const override;

protected:
  virtual int GetBodyPartWobbleData (int) const override;
  virtual bodypart *MakeBodyPart (int) const override;
  virtual void GetAICommand () override;
};


#else


bodypart *eddy::MakeBodyPart (int) const { return eddytorso::Spawn(0, NO_MATERIALS); }
int eddy::GetBodyPartWobbleData (int) const { return WOBBLE_VERTICALLY|(2 << WOBBLE_FREQ_SHIFT); }


//==========================================================================
//
//  eddy::NeedCheckNearbyItems
//
//  it is totally indifferent to items
//
//==========================================================================
truth eddy::NeedCheckNearbyItems () const {
  return false;
}


truth eddy::Hit (character *Enemy, v2, int, int) {
  if (IsPlayer()) {
    if (!(Enemy->IsMasochist() && GetRelation(Enemy) == FRIEND) &&
        GetRelation(Enemy) != HOSTILE &&
        !game::TruthQuestion(CONST_S("This might cause a hostile reaction. Are you sure?")))
    {
      return false;
    }
  }

  Hostility(Enemy);

  if (RAND_2) {
         if (IsPlayer()) ADD_MESSAGE("You engulf %s.", Enemy->CHAR_DESCRIPTION(DEFINITE));
    else if (Enemy->IsPlayer() || CanBeSeenByPlayer() || Enemy->CanBeSeenByPlayer()) ADD_MESSAGE("%s engulfs %s.", CHAR_DESCRIPTION(DEFINITE), Enemy->CHAR_DESCRIPTION(DEFINITE));
    Enemy->TeleportRandomly();
  } else if (IsPlayer()) {
    ADD_MESSAGE("You miss %s.", Enemy->CHAR_DESCRIPTION(DEFINITE));
  }

  EditAP(-500);
  return true;
}


void eddy::GetAICommand () {
  if (!GetLSquareUnder()->GetOLTerrain() && !RAND_N(500)) {
    decoration *Couch = decoration::Spawn(RAND_N(5) ? COUCH : DOUBLE_BED);
    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s spits out %s.", CHAR_NAME(DEFINITE), Couch->CHAR_NAME(INDEFINITE));
    GetLSquareUnder()->ChangeOLTerrainAndUpdateLights(Couch);
    EditAP(-1000);
    return;
  }

  if (GetStackUnder()->GetItems() && !RAND_N(10)) {
    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s engulfs something under it.", CHAR_NAME(DEFINITE));
    GetStackUnder()->TeleportRandomly(3);
    EditAP(-1000);
    return;
  }

  if (!RAND_N(100)) {
    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s engulfs itself.", CHAR_NAME(DEFINITE));
    TeleportRandomly(true);
    EditAP(-1000);
    return;
  }

  nonhumanoid::GetAICommand();
}


#endif
