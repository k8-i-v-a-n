#ifdef HEADER_PHASE
CHARACTER(feline, nonhumanoid)
{
public:
  virtual truth Catches (item *) override;
  virtual truth NeedCheckNearbyItems () const override;
  virtual truth CannotDrinkPotions () const override;

protected:
  virtual truth WantCatchIt (citem *Item) const; // default is `true`
};


#else


//==========================================================================
//
//  largecat::CannotDrinkPotions
//
//  this allows potion drinking (cats are smart!)
//
//==========================================================================
truth feline::CannotDrinkPotions () const {
  return false;
}


//==========================================================================
//
//  feline::NeedCheckNearbyItems
//
//  felines look for food
//
//==========================================================================
truth feline::NeedCheckNearbyItems () const {
  return true;
}


//==========================================================================
//
//  feline::WantCatchIt
//
//==========================================================================
truth feline::WantCatchIt (citem *Item) const {
  return true;
}


//==========================================================================
//
//  feline::Catches
//
//==========================================================================
truth feline::Catches (item *Thingy) {
  if (WantCatchIt(Thingy) && Thingy->CatWillCatchAndConsume(this)) {
    if (ConsumeItem(Thingy, CONST_S("eating"))) {
      if (IsPlayer()) {
        ADD_MESSAGE("You catch %s in mid-air and consume it.", Thingy->CHAR_NAME(DEFINITE));
      } else {
        if (CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s catches %s and eats it.",
                      CHAR_NAME(DEFINITE), Thingy->CHAR_NAME(DEFINITE));
        }
        if (PLAYER->GetRelativeDanger(this, true) > 0.1) {
          ChangeTeam(PLAYER->GetTeam());
          ADD_MESSAGE("%s seems to be much more friendly towards you.", CHAR_NAME(DEFINITE));
        }
      }
    } else if (IsPlayer()) {
      ADD_MESSAGE("You catch %s in mid-air.", Thingy->CHAR_NAME(DEFINITE));
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s catches %s.", CHAR_NAME(DEFINITE), Thingy->CHAR_NAME(DEFINITE));
    }
    return true;
  } else {
    return false;
  }
}


#endif
