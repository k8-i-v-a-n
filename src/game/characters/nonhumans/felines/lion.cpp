#ifdef HEADER_PHASE
CHARACTER(lion, feline)
{
protected:
  virtual truth WantCatchIt (citem *Item) const override;
};


#else


truth lion::WantCatchIt (citem *Item) const {
  //return false;
  return true; // why, let it catch the fish! ;-)
}


#endif
