#ifdef HEADER_PHASE
CHARACTER(floatingeye, nonhumanoid)
{
public:
  floatingeye () : NextWayPoint(0) {}

  virtual truth Hit (character *, v2, int, int = 0) override;
  virtual int TakeHit (character *, item *, bodypart *, v2, double, double, int, int, int, truth, truth) override;
  virtual void SetWayPoints (const fearray<packv2> &) override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth IsSpy () const override;
  virtual truth CanAttack () const override;
  virtual truth NeedCheckNearbyItems () const override;

protected:
  virtual void GetAICommand () override;

protected:
  std::vector<v2> WayPoints;
  uInt NextWayPoint;
};


#else


truth floatingeye::IsSpy () const { return true; }
truth floatingeye::CanAttack () const { return false; }
void floatingeye::SetWayPoints (const fearray<packv2> &What) { ArrayToVector(What, WayPoints); }


void floatingeye::Save (outputfile &SaveFile) const {
  nonhumanoid::Save(SaveFile);
  SaveFile << WayPoints << NextWayPoint;
}


void floatingeye::Load (inputfile &SaveFile) {
  nonhumanoid::Load(SaveFile);
  SaveFile >> WayPoints >> NextWayPoint;
}


//==========================================================================
//
//  floatingeye::NeedCheckNearbyItems
//
//  it cannot wield anything anyway
//
//==========================================================================
truth floatingeye::NeedCheckNearbyItems () const {
  return false;
}


void floatingeye::GetAICommand () {
  if (WayPoints.size() && !IsGoingSomeWhere()) {
    if (GetPos() == WayPoints[NextWayPoint]) {
      if (NextWayPoint < WayPoints.size() - 1) ++NextWayPoint; else NextWayPoint = 0;
    }
    GoingTo = WayPoints[NextWayPoint];
  }

  SeekLeader(GetLeader());

  if (CheckForEnemies(false, false, true)) return;
  if (FollowLeader(GetLeader())) return;
  if (MoveRandomly()) return;

  EditAP(-1000);
}


truth floatingeye::Hit (character *Enemy, v2, int, int) {
  if (IsPlayer()) {
    ADD_MESSAGE("You stare at %s.", Enemy->CHAR_DESCRIPTION(DEFINITE));
  } else if (Enemy->IsPlayer() && CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s stares at you.", CHAR_NAME(DEFINITE));
  }

  EditAP(-1000);
  return true;
}


int floatingeye::TakeHit (character *Enemy, item *Weapon, bodypart *EnemyBodyPart, v2 HitPos, double Damage, double ToHitValue, int Success, int Type, int Direction, truth Critical, truth ForceHit) {
  // changes for fainting 2 out of 3
  if (CanBeSeenBy(Enemy) && Enemy->HasEyes() && RAND_N(3) && Enemy->LoseConsciousness(150 + RAND_N(150))) {
    if (!Enemy->IsPlayer()) Enemy->EditExperience(WISDOM, 75, 1 << 13);
    return HAS_FAILED;
  }
  return nonhumanoid::TakeHit(Enemy, Weapon, EnemyBodyPart, HitPos, Damage, ToHitValue, Success, Type, Direction, Critical, ForceHit);
}


#endif
