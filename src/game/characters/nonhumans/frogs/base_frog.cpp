#ifdef HEADER_PHASE
CHARACTER(frog, nonhumanoid)
{
public:
  virtual truth MoveRandomly (truth allowInterestingItems) override;
};


#else


truth frog::MoveRandomly (truth allowInterestingItems) {
  return MoveRandomlyInRoom(allowInterestingItems);
}


#endif
