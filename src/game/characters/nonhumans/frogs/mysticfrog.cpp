#ifdef HEADER_PHASE
CHARACTER(mysticfrog, frog)
{
public:
  virtual void GetAICommand () override;

protected:
  virtual int GetBodyPartWobbleData (int) const override;
  virtual bodypart* MakeBodyPart (int) const override;
  int GetSpellAPCost() const;
};


#else


int mysticfrog::GetSpellAPCost () const { return 1500; }
int mysticfrog::GetBodyPartWobbleData(int) const { return WOBBLE_HORIZONTALLY|(1 << WOBBLE_SPEED_SHIFT)|(3 << WOBBLE_FREQ_SHIFT); }
bodypart *mysticfrog::MakeBodyPart (int) const { return mysticfrogtorso::Spawn(0, NO_MATERIALS); }


void mysticfrog::GetAICommand () {
  SeekLeader(GetLeader());

  if (FollowLeader(GetLeader())) return;

  character *NearestEnemy = 0;
  sLong NearestEnemyDistance = 0x7FFFFFFF;
  character *RandomFriend = 0;
  charactervector Friend;
  v2 Pos = GetPos();
  truth Enemies = false;

  for (int c = 0; c < game::GetTeams(); ++c) {
    if (GetTeam()->GetRelation(game::GetTeam(c)) == HOSTILE) {
      for (auto &it : game::GetTeam(c)->GetMember()) {
        if (it->IsEnabled()) {
          Enemies = true;
          sLong ThisDistance = Max<sLong>(abs(it->GetPos().X-Pos.X), abs(it->GetPos().Y-Pos.Y));
          if ((ThisDistance < NearestEnemyDistance ||
               (ThisDistance == NearestEnemyDistance && !RAND_N(3))) &&
              it->CanBeSeenBy(this))
          {
            NearestEnemy = it;
            NearestEnemyDistance = ThisDistance;
          }
        }
      }
    } else if (GetTeam()->GetRelation(game::GetTeam(c)) == FRIEND) {
      for (auto &it : game::GetTeam(c)->GetMember()) {
        if (it->IsEnabled() && it->CanBeSeenBy(this)) Friend.push_back(it);
      }
    }
  }

  if (NearestEnemy && NearestEnemy->GetPos().IsAdjacent(Pos)) {
    if (NearestEnemy->IsSmall() &&
        GetAttribute(WISDOM) < NearestEnemy->GetAttackWisdomLimit() &&
        !RAND_N(5) &&
        Hit(NearestEnemy, NearestEnemy->GetPos(),
            game::GetDirectionForVector(NearestEnemy->GetPos()-GetPos())))
    {
      return;
    }
    if (!RAND_4) {
      if (CanBeSeenByPlayer()) ADD_MESSAGE("%s invokes a spell and disappears.", CHAR_NAME(DEFINITE));
      TeleportRandomly(true);
      EditAP(-GetSpellAPCost());
      return;
    }
  }

  if (NearestEnemy && (NearestEnemyDistance < 10 || StateIsActivated(PANIC)) && RAND_4) {
    SetGoingTo((Pos << 1) - NearestEnemy->GetPos());
    if (MoveTowardsTarget(true)) return;
  }

  if (Friend.size() && !RAND_4) {
    RandomFriend = Friend[RAND_N(Friend.size())];
    NearestEnemy = 0;
  }

  if (GetRelation(PLAYER) == HOSTILE && PLAYER->CanBeSeenBy(this) && !RAND_4) NearestEnemy = PLAYER;

  beamdata Beam(
    this,
    CONST_S("killed by the spells of ") + GetName(INDEFINITE),
    YOURSELF,
    0
  );

  if (NearestEnemy) {
    lsquare *Square = NearestEnemy->GetLSquareUnder();
    EditAP(-GetSpellAPCost());

    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s invokes a spell!", CHAR_NAME(DEFINITE));

    switch (RAND_N(20)) {
      case 0: case 1: case 2: case 3: case 4: case 5:
        Square->DrawParticles(RED);
        if (NearestEnemy->TeleportRandomItem(GetConfig() == DARK)) break;
        /* fallthrough */
     case 6: case 7: case 8: case 9: case 10: Square->DrawParticles(RED); Square->Teleport(Beam); break;
     case 11: case 12: case 13: case 14: Square->DrawParticles(RED); Square->Slow(Beam); break;
     case 15: Square->DrawParticles(RED); Square->LowerEnchantment(Beam); break;
     default: Square->DrawLightning(v2(8, 8), WHITE, YOURSELF); Square->Lightning(Beam); break;
    }

    if (CanBeSeenByPlayer()) {
      NearestEnemy->DeActivateVoluntaryAction(CONST_S("The spell of ") + GetName(DEFINITE) + CONST_S(" interrupts you."));
    } else {
      NearestEnemy->DeActivateVoluntaryAction(CONST_S("The spell interrupts you."));
    }

    return;
  }

  if (RandomFriend && Enemies) {
    lsquare *Square = RandomFriend->GetLSquareUnder();
    EditAP(-GetSpellAPCost());
    Square->DrawParticles(RED);
    if (RAND_2) Square->Invisibility(Beam); else Square->Haste(Beam);
    return;
  }

  StandIdleAI();
}


#endif
