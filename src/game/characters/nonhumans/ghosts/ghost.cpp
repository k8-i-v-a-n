#ifdef HEADER_PHASE
CHARACTER(ghost, nonhumanoid)
{
public:
  ghost () : Active(true) {}

  virtual void AddName (festring &, int) const override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth IsNameable () const override;
  virtual truth RaiseTheDead (character *) override;
  virtual int ReceiveBodyPartDamage (character *, int, int, int, int = 8, truth = false, truth = false, truth = true, truth = false) override;
  virtual truth SpecialEnemySightedReaction (character *) override;
  virtual truth IsPolymorphable () const override;

  void SetOwnerSoul (cfestring &What);
  void SetIsActive (truth What);

protected:
  virtual int GetBodyPartWobbleData (int) const override;
  virtual cchar *FirstPersonBiteVerb () const override;
  virtual cchar *FirstPersonCriticalBiteVerb () const override;
  virtual cchar *ThirdPersonBiteVerb () const override;
  virtual cchar *ThirdPersonCriticalBiteVerb () const override;
  virtual truth AttackIsBlockable (int) const override;
  virtual truth AttackMayDamageArmor () const override;
  virtual void GetAICommand () override;

protected:
  festring OwnerSoul;
  truth Active;
};


#else


void ghost::SetOwnerSoul (cfestring &What) { OwnerSoul = What; }
void ghost::SetIsActive (truth What) { Active = What; }

truth ghost::IsNameable () const { return OwnerSoul.IsEmpty(); }
truth ghost::IsPolymorphable () const { return MaxHP < 100; }

cchar *ghost::FirstPersonBiteVerb () const { return "touch"; }
cchar *ghost::FirstPersonCriticalBiteVerb () const { return "awfully touch"; }
cchar *ghost::ThirdPersonBiteVerb () const { return "touches"; }
cchar *ghost::ThirdPersonCriticalBiteVerb () const { return "awfully touches"; }

truth ghost::SpecialEnemySightedReaction (character *) { return !(Active = true); }

int ghost::GetBodyPartWobbleData (int) const { return WOBBLE_HORIZONTALLY|(2 << WOBBLE_FREQ_SHIFT); }

truth ghost::AttackIsBlockable (int) const { return false; }
truth ghost::AttackMayDamageArmor () const { return false; }


void ghost::AddName (festring &String, int Case) const {
  if (OwnerSoul.IsEmpty() || (Case&PLURAL) != 0) {
    character::AddName(String, Case);
  } else {
    character::AddName(String, (Case|ARTICLE_BIT)&~INDEFINE_BIT);
    String << " of " << OwnerSoul;
  }
}


void ghost::Save (outputfile &SaveFile) const {
  nonhumanoid::Save(SaveFile);
  SaveFile << OwnerSoul << Active;
}


void ghost::Load (inputfile &SaveFile) {
  nonhumanoid::Load(SaveFile);
  SaveFile >> OwnerSoul >> Active;
}


truth ghost::RaiseTheDead (character *Summoner) {
  itemvector ItemVector;
  GetStackUnder()->FillItemVector(ItemVector);
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->SuckSoul(this, Summoner)) return true;
  }

       if (IsPlayer()) ADD_MESSAGE("You shudder.");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s shudders.", CHAR_NAME(DEFINITE));

  return false;
}


int ghost::ReceiveBodyPartDamage (character *Damager, int Damage, int Type, int BodyPartIndex, int Direction, truth PenetrateResistance, truth Critical, truth ShowNoDamageMsg, truth CaptureBodyPart) {
  if (Type != SOUND) {
    Active = true;
    return character::ReceiveBodyPartDamage(Damager, Damage, Type, BodyPartIndex, Direction, PenetrateResistance, Critical, ShowNoDamageMsg, CaptureBodyPart);
  }
  return 0;
}



void ghost::GetAICommand () {
  if (Active) {
    character::GetAICommand();
  } else {
    if (CheckForEnemies(false, false, false)) return;
    EditAP(-1000);
  }
}


#endif
