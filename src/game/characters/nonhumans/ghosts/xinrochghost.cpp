#ifdef HEADER_PHASE
CHARACTER(xinrochghost, ghost)
{
public:
  virtual truth IsNameable () const override;
  virtual truth IsPolymorphable () const override;
  virtual truth CheckForUsefulItemsOnGround (truth = true) override;
  virtual truth NeedCheckNearbyItems () const override;

protected:
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;
};


#else


truth xinrochghost::IsNameable () const { return false; }
truth xinrochghost::IsPolymorphable () const { return false; }

truth xinrochghost::NeedCheckNearbyItems () const { return false; }
truth xinrochghost::CheckForUsefulItemsOnGround (truth) { return false; }


void xinrochghost::GetAICommand () {
  if (GetHomeRoom()) {
    StandIdleAI();
  } else {
    nonhumanoid::GetAICommand(); // not a `humanoid`, 'cause `humanoid` has no overriden `GetAICommand()`
  }
}


void xinrochghost::CreateCorpse (lsquare* Square) {
  SendToHell();
  if (!game::GetCurrentLevel()->IsOnGround()) {
    ADD_MESSAGE("Suddenly a horrible earthquake shakes the level.");
    ADD_MESSAGE("You hear an eerie scream: \"Ahh! Free at last! FREE TO BE REBORN!\"");
    game::GetCurrentLevel()->PerformEarthquake();
  }
}


#endif
