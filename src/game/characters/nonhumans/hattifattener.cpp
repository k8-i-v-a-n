#ifdef HEADER_PHASE
CHARACTER(hattifattener, nonhumanoid)
{
public:
  virtual truth Hit (character*, v2, int, int = 0) override;
  virtual truth NeedCheckNearbyItems () const override;

protected:
  virtual int GetBodyPartWobbleData (int) const override;
  virtual int GetSpecialBodyPartFlags (int) const override;
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;
};


#else


int hattifattener::GetSpecialBodyPartFlags (int) const { return ST_LIGHTNING; }
int hattifattener::GetBodyPartWobbleData (int) const { return WOBBLE_HORIZONTALLY|(1 << WOBBLE_SPEED_SHIFT)|(1 << WOBBLE_FREQ_SHIFT); }
truth hattifattener::Hit (character*, v2, int, int) { return false; }


//==========================================================================
//
//  hattifattener::NeedCheckNearbyItems
//
//  they love lightnings, and lightnings are not lying on the ground
//
//==========================================================================
truth hattifattener::NeedCheckNearbyItems () const {
  return false;
}


void hattifattener::GetAICommand () {
  if (!RAND_N(7)) {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s emits a lightning bolt!", CHAR_DESCRIPTION(DEFINITE));
    }
    beamdata Beam(
      this,
      CONST_S("killed by a hattifattener's lightning"),
      GetPos(),
      WHITE,
      BEAM_LIGHTNING,
      RAND_8,
      1 + RAND_8,
      0,
      NULL
    );
    GetLevel()->LightningBeam(Beam);
    EditAP(-1000);
    return;
  }

  SeekLeader(GetLeader());

  if (FollowLeader(GetLeader())) return;
  if (MoveRandomly()) return;

  EditAP(-1000);
}


void hattifattener::CreateCorpse (lsquare *Square) {
  level *Level = Square->GetLevel();
  feuLong StackSize = Level->AddRadiusToSquareStack(Square->GetPos(), 9);
  lsquare **SquareStack = Level->GetSquareStack();

  for (feuLong c = 0; c < StackSize; ++c) SquareStack[c]->RemoveFlags(IN_SQUARE_STACK);

  fearray<lsquare*> Stack(SquareStack, StackSize);
  Level->LightningVisualizer(Stack, WHITE);

  for (feuLong c = 0; c < Stack.Size; ++c) {
    beamdata Beam(
      this,
      CONST_S("killed by electricity released by a dying hattifattener"),
      YOURSELF,
      0
    );
    Stack[c]->Lightning(Beam);
  }

  SendToHell();
}


#endif
