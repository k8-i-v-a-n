#ifdef HEADER_PHASE
CHARACTER(invisiblestalker, nonhumanoid)
{
public:
  virtual truth CannotDrinkPotions () const override;
};

#else


// this allows potion drinking
truth invisiblestalker::CannotDrinkPotions () const { return false; }


#endif
