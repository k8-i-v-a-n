#ifdef HEADER_PHASE
CHARACTER(largecat, nonhumanoid)
{
public:
  largecat () : Lives(7) {}

  virtual void Save (outputfile&) const override;
  virtual void Load (inputfile&) override;
  virtual truth SpecialSaveLife () override;
  virtual truth Catches (item *) override;
  virtual truth CannotDrinkPotions () const override;

protected:
  int Lives;
};


#else


void largecat::Save (outputfile& SaveFile) const {
  nonhumanoid::Save(SaveFile);
  SaveFile << Lives;
}


void largecat::Load (inputfile& SaveFile) {
  nonhumanoid::Load(SaveFile);
  SaveFile >> Lives;
}


// this allows potion drinking (cats are smart!)
truth largecat::CannotDrinkPotions () const { return false; }


truth largecat::SpecialSaveLife () {
  if (--Lives <= 0 || game::IsInWilderness()) return false;

  v2 Pos = GetPos();
  rect Border(Pos + v2(-20, -20), Pos + v2(20, 20));

  Pos = GetLevel()->GetRandomSquare(this, 0, &Border);
  if (Pos == ERROR_V2) {
    Pos = GetLevel()->GetRandomSquare(this);
    if (Pos == ERROR_V2) return false;
  }

       if (IsPlayer()) ADD_MESSAGE("But wait! You seem to have miraculously avoided certain death!");
  else if (CanBeSeenByPlayer()) ADD_MESSAGE("But wait, %s seems to have miraculously avoided certain death!", GetPersonalPronoun().CStr());

  Move(Pos, true);

  if (!IsPlayer() && CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s appears!", CHAR_NAME(INDEFINITE));
  }

  if (IsPlayer()) {
    game::AskForEscPress(CONST_S("Life saved!"));
  }

  RestoreBodyParts();
  ResetSpoiling();
  RestoreHP();
  RestoreStamina();
  ResetStates();

  if (GetNP() < SATIATED_LEVEL) SetNP(SATIATED_LEVEL);

  SendNewDrawRequest();

  if (GetAction()) GetAction()->Terminate(false);

  return true;
}


truth largecat::Catches (item* Thingy) {
  if (Thingy->CatWillCatchAndConsume(this)) {
    if (ConsumeItem(Thingy, CONST_S("eating"))) {
      if (IsPlayer()) {
        ADD_MESSAGE("You catch %s in mid-air and consume it.", Thingy->CHAR_NAME(DEFINITE));
      } else {
        if (CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s catches %s and eats it.", CHAR_NAME(DEFINITE), Thingy->CHAR_NAME(DEFINITE));
        }

        if (PLAYER->GetRelativeDanger(this, true) > 0.1) {
          ChangeTeam(PLAYER->GetTeam());
          ADD_MESSAGE("%s seems to be much more friendly towards you.", CHAR_NAME(DEFINITE));
        }
      }
    } else if (IsPlayer()) {
      ADD_MESSAGE("You catch %s in mid-air.", Thingy->CHAR_NAME(DEFINITE));
    } else if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s catches %s.", CHAR_NAME(DEFINITE), Thingy->CHAR_NAME(DEFINITE));
    }
    return true;
  } else {
    return false;
  }
}

#endif
