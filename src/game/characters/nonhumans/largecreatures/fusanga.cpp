#ifdef HEADER_PHASE
CHARACTER(fusanga, largecreature)
{
public:
  virtual col16 GetSkinColor () const override;
  virtual bodypart *MakeBodyPart (int) const override;
  virtual void SpecialTurnHandler () override;
  virtual truth IsMushroom () const override;
 protected:
  virtual truth AllowExperience () const override;
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;
  virtual truth MustBeRemovedFromBone () const override;
};


#else

bodypart *fusanga::MakeBodyPart (int) const { return fusangatorso::Spawn(0, NO_MATERIALS); }
col16 fusanga::GetSkinColor () const { return MakeRGB16(60 + RAND_N(190), 60 + RAND_N(190), 60 + RAND_N(190)); }
void fusanga::SpecialTurnHandler () { UpdatePictures(); }
truth fusanga::IsMushroom () const { return true; }
truth fusanga::AllowExperience () const { return false; }


void fusanga::GetAICommand () {
  if (AttackAdjacentEnemyAI()) return;

  /* Chaos magic */
  lsquare *Square = GetLevel()->GetLSquare(GetLevel()->GetRandomSquare(0, HAS_NO_OTERRAIN));

  if (Square && !RAND_N(20)) {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s radiates pure magic.", CHAR_NAME(DEFINITE));
    }

    switch (RAND_4) {
      case 0: // Random spell
        {
          int BeamEffect = RAND_N(17); // Change if more beams are added.
          beamdata Beam (
            this,
            CONST_S("killed by the sorcery of ") + GetName(DEFINITE),
            GetPos(),
            RANDOM_COLOR,
            BeamEffect,
            YOURSELF,
            1,
            0,
            NULL
          );
          (Square->*lsquare::GetBeamEffect(BeamEffect))(Beam);
          break;
        }
      case 1: // Create gas
        {
          // Change if more gases are added.
          int gas;
          do {
            gas = FIRST_GAS + RAND_N(LAST_GAS - FIRST_GAS + 1);
            IvanAssert(gas >= FIRST_GAS && gas <= LAST_GAS);
            // fusanga cannot spawn air or vacuum
          } while (gas != AIR && gas != VACUUM);
          if (!RAND_2) {
            GetLevel()->GasExplosion(gas::Spawn(gas, 100), Square, this);
          } else {
            Square->AddSmoke(gas::Spawn(gas, 100));
          }

          ADD_MESSAGE("You hear the hiss of gas.");
          break;
        }
      default: // Create rain
        {
          // Change if more liquids are added.
          const int liquid = FIRST_LIQUID + RAND_N(LAST_LIQUID - FIRST_LIQUID + 1);
          IvanAssert(liquid >= FIRST_LIQUID && liquid <= LAST_LIQUID);

          beamdata Beam(
            this,
            CONST_S("killed by the showers of ") + GetName(DEFINITE),
            YOURSELF,
            0
          );
          Square->LiquidRain(Beam, liquid);

          if (Square->CanBeSeenByPlayer()) {
            ADD_MESSAGE("A drizzle comes down.");
          } else {
            ADD_MESSAGE("You hear the sounds of rainfall.");
          }

          break;
        }
    }

    EditAP(-4000);
    return;
  }

  /* Spawn mushrooms */
  if (!RAND_N(40)) {
    int NumberOfMushrooms = RAND_N(3) + RAND_N(3) + RAND_N(3) + RAND_N(3);

    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s radiates strange magic.", CHAR_NAME(DEFINITE));
    }

    for (int i = 0; i < NumberOfMushrooms; i += 1) {
      character *NewShroom;
      switch (RAND_4) {
        case 2: NewShroom = magicmushroom::Spawn(); break;
        //case 3: NewShroom = weepmushroom::Spawn(); break;
        default: NewShroom = mushroom::Spawn(); break;
      }

      NewShroom->SetGenerationDanger(GetGenerationDanger());
      NewShroom->SetTeam(GetTeam());
      NewShroom->PutTo(GetLevel()->GetRandomSquare(NewShroom));

      if (NewShroom->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s sprouts from the ground.", NewShroom->CHAR_NAME(INDEFINITE));
      }
    }

    EditAP(-2000);
    return;
  }

  /* Just chill there. */
  EditAP(-1000);
}


void fusanga::CreateCorpse (lsquare *Square) {
  largecreature::CreateCorpse(Square);
}


truth fusanga::MustBeRemovedFromBone () const {
  /*
  return !IsEnabled() || GetTeam()->GetID() != MONSTER_TEAM
                      || GetDungeon()->GetIndex() != FUNGAL_CAVE
                      || GetLevel()->GetIndex() != FUSANGA_LEVEL;
  */
  return true;
}


#endif
