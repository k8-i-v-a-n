#ifdef HEADER_PHASE
CHARACTER(genefourxvesana, largecreature)
{
public:
  genefourxvesana() : TurnsExisted(0), Species(0) {}

  //void SetSpecies (int);
  int GetSpecies () const;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void FinalProcessForBone () override;

protected:
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;

protected:
  sLong TurnsExisted;
  int Species;
};


#else


int genefourxvesana::GetSpecies () const { return Species; }


void genefourxvesana::Save (outputfile &SaveFile) const {
  nonhumanoid::Save(SaveFile);
  SaveFile << TurnsExisted;
  SaveFile << Species;
}


void genefourxvesana::Load (inputfile &SaveFile) {
  nonhumanoid::Load(SaveFile);
  SaveFile >> TurnsExisted;
  SaveFile >> Species;
}


void genefourxvesana::FinalProcessForBone () {
  largecreature::FinalProcessForBone();
  TurnsExisted = 0;
}


void genefourxvesana::CreateCorpse (lsquare* Square) {
  for (int c = 0; c < 1; ++c) Square->AddItem(solstone::Spawn());
  largecreature::CreateCorpse(Square);
}


void genefourxvesana::GetAICommand () {
  StandIdleAI();
  ++TurnsExisted;
  SeekLeader(GetLeader());
  if (FollowLeader(GetLeader())) return;

  if (!RAND_N(10)) {
    int NumberOfPlants = RAND_N(3)+RAND_N(3)+RAND_N(3)+RAND_N(3);
    for (int c1 = 0; c1 < 50 && NumberOfPlants; ++c1) {
      for (int c2 = 0; c2 < game::GetTeams() && NumberOfPlants; ++c2) {
        if (GetTeam()->GetRelation(game::GetTeam(c2)) == HOSTILE) {
          for (auto &it : game::GetTeam(c2)->GetMember()) {
            if (it->IsEnabled()) {
              lsquare *LSquare = it->GetNeighbourLSquare(RAND_N(GetNeighbourSquares()));
              if (LSquare && (LSquare->GetWalkability() & WALK) && !LSquare->GetCharacter()) {
                character *NewPlant;
                sLong RandomValue = RAND_N(TurnsExisted);

                     if (RandomValue < 250) NewPlant = carnivorousplant::Spawn(GIANT);
                else if (RandomValue < 1500) NewPlant = carnivorousplant::Spawn(LILY);
                else NewPlant = carnivorousplant::Spawn(SHAMBLING);

                for (int c = 3; c < TurnsExisted / 500; ++c) NewPlant->EditAllAttributes(1);

                NewPlant->SetGenerationDanger(GetGenerationDanger());
                NewPlant->SetTeam(GetTeam());
                NewPlant->PutTo(LSquare->GetPos());
                --NumberOfPlants;

                if (NewPlant->CanBeSeenByPlayer()) {
                       if (it->IsPlayer()) ADD_MESSAGE("%s sprouts from the ground near you.", NewPlant->CHAR_NAME(INDEFINITE));
                  else if (it->CanBeSeenByPlayer()) ADD_MESSAGE("%s sprouts from the ground near %s.", NewPlant->CHAR_NAME(INDEFINITE), it->CHAR_NAME(DEFINITE));
                  else ADD_MESSAGE("%s sprouts from the ground.", NewPlant->CHAR_NAME(INDEFINITE));
                }
              }
            }
          }
        }
      }

      SeekLeader(GetLeader());
      if (FollowLeader(GetLeader())) return;

      lsquare *CradleSquare = GetNeighbourLSquare(RAND_N(8));
      if (CradleSquare && !CradleSquare->GetCharacter() && (CradleSquare->GetWalkability() & WALK)) {
        int SpoiledItems = 0;
        int MushroomsNear = 0;
        for (int d = 0; d < 8; ++d) {
          lsquare *Square = CradleSquare->GetNeighbourLSquare(d);
          if (Square) {
            character *Char = Square->GetCharacter();
            if (Char && Char->IsMushroom()) ++MushroomsNear;
            SpoiledItems += Square->GetSpoiledItems();
          }
        }
        if ((SpoiledItems && MushroomsNear < 1 && !RAND_N(2)) || (MushroomsNear < 3 && !RAND_N((1+MushroomsNear)*2))) {
          carnivorousplant *Child = carnivorousplant::Spawn(SHAMBLING);
          Child->SetTeam(GetTeam());
          Child->SetGenerationDanger(GetGenerationDanger());
          Child->PutTo(CradleSquare->GetPos());
          if (Child->CanBeSeenByPlayer()) ADD_MESSAGE("%s sprouts from the ground.", Child->CHAR_NAME(INDEFINITE));
        }
      }
      if (AttackAdjacentEnemyAI()) return;
      if (MoveRandomly()) return;
      EditAP(-1000);
    }
    EditAP(-2000);
    return;
  }
  if (AttackAdjacentEnemyAI()) return;
  if (MoveRandomly()) return;
  EditAP(-1000);
}


#endif
