#ifdef HEADER_PHASE
CHARACTER(genetrixvesana, largecreature)
{
public:
  genetrixvesana () : TurnsExisted(0) {}
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void FinalProcessForBone () override;

protected:
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;

protected:
  sLong TurnsExisted;
};


#else


void genetrixvesana::GetAICommand () {
  ++TurnsExisted;

  SeekLeader(GetLeader());
  if (FollowLeader(GetLeader())) return;

  if (!RAND_N(60)) {
    int NumberOfPlants = RAND_N(3) + RAND_N(3) + RAND_N(3) + RAND_N(3);
    for (int c1 = 0; c1 < 50 && NumberOfPlants; ++c1) {
      for (int c2 = 0; c2 < game::GetTeams() && NumberOfPlants; ++c2) {
        if (GetTeam()->GetRelation(game::GetTeam(c2)) == HOSTILE) {
          for (auto &it : game::GetTeam(c2)->GetMember()) {
            if (it->IsEnabled()) {
              lsquare *LSquare = it->GetNeighbourLSquare(RAND_N(GetNeighbourSquares()));
              if (LSquare && (LSquare->GetWalkability() & WALK) && !LSquare->GetCharacter()) {
                character *NewPlant;
                sLong RandomValue = RAND_N(TurnsExisted);

                     if (RandomValue < 250) NewPlant = carnivorousplant::Spawn();
                else if (RandomValue < 1500) NewPlant = carnivorousplant::Spawn(GREATER);
                else NewPlant = carnivorousplant::Spawn(GIANTIC);

                for (int c = 3; c < TurnsExisted / 500; ++c) NewPlant->EditAllAttributes(1);

                NewPlant->SetGenerationDanger(GetGenerationDanger());
                NewPlant->SetTeam(GetTeam());
                NewPlant->PutTo(LSquare->GetPos());
                --NumberOfPlants;

                if (NewPlant->CanBeSeenByPlayer()) {
                  if (it->IsPlayer()) {
                    ADD_MESSAGE("%s sprouts from the ground near you.",
                                NewPlant->CHAR_NAME(INDEFINITE));
                  } else if (it->CanBeSeenByPlayer()) {
                    ADD_MESSAGE("%s sprouts from the ground near %s.",
                                NewPlant->CHAR_NAME(INDEFINITE), it->CHAR_NAME(DEFINITE));
                  } else {
                    ADD_MESSAGE("%s sprouts from the ground.",
                                NewPlant->CHAR_NAME(INDEFINITE));
                  }
                }
              }
            }
          }
        }
      }
    }
    EditAP(-2000);
    return;
  }

  if (AttackAdjacentEnemyAI()) return;
  if (MoveRandomly()) return;

  EditAP(-1000);
}


void genetrixvesana::CreateCorpse (lsquare *Square) {
  for (int c = 0; c < 3; ++c) Square->AddItem(pineapple::Spawn());
  largecreature::CreateCorpse(Square);
}


void genetrixvesana::Save (outputfile &SaveFile) const {
  nonhumanoid::Save(SaveFile);
  SaveFile << TurnsExisted;
}


void genetrixvesana::Load (inputfile &SaveFile) {
  nonhumanoid::Load(SaveFile);
  SaveFile >> TurnsExisted;
}


void genetrixvesana::FinalProcessForBone () {
  largecreature::FinalProcessForBone();
  TurnsExisted = 0;
}


#endif
