#ifdef HEADER_PHASE
CHARACTER(haastseagle, largecreature)
{
protected:
  virtual cchar *FirstPersonBiteVerb () const override;
  virtual cchar *FirstPersonCriticalBiteVerb () const override;
  virtual cchar *ThirdPersonBiteVerb () const override;
  virtual cchar *ThirdPersonCriticalBiteVerb () const override;
};


#else


cchar *haastseagle::FirstPersonBiteVerb() const { return "peck"; }
cchar *haastseagle::FirstPersonCriticalBiteVerb() const { return "critically peck"; }
cchar *haastseagle::ThirdPersonBiteVerb() const { return "pecks"; }
cchar *haastseagle::ThirdPersonCriticalBiteVerb() const { return "critically pecks"; }


#endif
