#ifdef HEADER_PHASE
CHARACTER(lobhse, largecreature)
{
public:
  virtual truth IsSpider () const override;

protected:
  virtual truth SpecialBiteEffect (character *, v2, int, int, truth, truth Critical, int DoneDamage) override;
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;
  virtual bodypart *MakeBodyPart (int) const override;
};


#else


truth lobhse::IsSpider () const { return true; }
bodypart *lobhse::MakeBodyPart (int) const { return lobhsetorso::Spawn(0, NO_MATERIALS); }


truth lobhse::SpecialBiteEffect (character *Char, v2, int, int, truth BlockedByArmour, truth Critical, int DoneDamage) {
  if (!BlockedByArmour || Critical) {
    //Char->BeginTemporaryState(POISONED, 80 + RAND_N(40));
    int Effect = (Char->StateIsActivated(DISEASE_IMMUNITY) ? 6 : RAND_N(/*10*/18)); //k8:10 in comm. fork
    switch (Effect) {
      case 0: Char->BeginTemporaryState(LYCANTHROPY, 6000 + RAND_N(2000)); break;
      case 1: Char->BeginTemporaryState(VAMPIRISM, 5000 + RAND_N(2500)); break;
      case 2: Char->BeginTemporaryState(PARASITE_TAPE_WORM, 6000 + RAND_N(3000)); break;
      case 3: Char->BeginTemporaryState(PARASITE_MIND_WORM, 400 + RAND_N(200)); break;
      case 4: Char->GainIntrinsic(LEPROSY); break;
      default: Char->BeginTemporaryState(POISONED, 80 + RAND_N(40)); break;
    }
    return true;
  }
  return false;
}


void lobhse::GetAICommand () {
  SeekLeader(GetLeader()); // will follow if tamed
  if (FollowLeader(GetLeader())) return;
  //StandIdleAI(); //k8:??? removed in comm. fork
  if (MoveRandomly()) {
    web *Web = web::Spawn();
    Web->SetStrength(GetConfig() == LARGE ? 50 : 100);
    if (GetLSquareUnder()->AddTrap(Web)) {
      if (CanBeSeenByPlayer()) ADD_MESSAGE("%s spins a web.", CHAR_NAME(DEFINITE));
      EditAP(-1000);
      return;
    }
  }
}


void lobhse::CreateCorpse (lsquare *Square) {
  largecreature::CreateCorpse(Square);
  Square->AddItem(mangoseedling::Spawn());
  game::SetFreedomStoryState(2);
}


#endif
