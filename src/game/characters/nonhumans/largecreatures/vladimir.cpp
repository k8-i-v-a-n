#ifdef HEADER_PHASE
CHARACTER(vladimir, largecreature)
{
public:
  virtual col16 GetSkinColor () const override;
  virtual void SpecialTurnHandler () override;
};


#else


col16 vladimir::GetSkinColor() const { return MakeRGB16(60 + RAND_N(190), 60 + RAND_N(190), 60 + RAND_N(190)); }
void vladimir::SpecialTurnHandler () { UpdatePictures(); }


#endif
