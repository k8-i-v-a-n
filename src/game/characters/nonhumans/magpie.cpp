#ifdef HEADER_PHASE
CHARACTER(magpie, nonhumanoid)
{
public:
  virtual void GetAICommand () override;
  virtual truth NeedCheckNearbyItems () const override;
  virtual truth IsRetreating () const override;

protected:
  virtual bodypart *MakeBodyPart (int) const override;
  virtual cchar *FirstPersonBiteVerb () const override;
  virtual cchar *FirstPersonCriticalBiteVerb () const override;
  virtual cchar *ThirdPersonBiteVerb () const override;
  virtual cchar *ThirdPersonCriticalBiteVerb () const override;
};


#else


bodypart *magpie::MakeBodyPart (int) const { return magpietorso::Spawn(0, NO_MATERIALS); }

cchar *magpie::FirstPersonBiteVerb () const { return "peck"; }
cchar *magpie::FirstPersonCriticalBiteVerb () const { return "critically peck"; }
cchar *magpie::ThirdPersonBiteVerb () const { return "pecks"; }
cchar *magpie::ThirdPersonCriticalBiteVerb () const { return "critically pecks"; }


//==========================================================================
//
//  magpie::NeedCheckNearbyItems
//
//  stupid thing cannot wield items.
//  FIXME: should it look for any valuables on the ground?
//
//==========================================================================
truth magpie::NeedCheckNearbyItems () const {
  return false;
}


truth magpie::IsRetreating () const {
  if (nonhumanoid::IsRetreating()) return true;

  for (stackiterator i = GetStack()->GetBottom(); i.HasItem(); ++i) {
    if ((*i)->GetSparkleFlags()) return true;
  }

  return false;
}


void magpie::GetAICommand () {
  if (!IsRetreating()) {
    character *Char = GetRandomNeighbour();
    if (Char) {
      itemvector Sparkling;
      for (stackiterator i = Char->GetStack()->GetBottom(); i.HasItem(); ++i) {
        if ((*i)->GetSparkleFlags() && !MakesBurdened((*i)->GetWeight())) {
          Sparkling.push_back(*i);
        }
      }
      if (!Sparkling.empty()) {
        item *ToSteal = Sparkling[RAND_N(Sparkling.size())];
        StealItemFrom(Char, ToSteal);
        EditAP(-500);
        return;
      }
    }
  }

  nonhumanoid::GetAICommand();
}


#endif
