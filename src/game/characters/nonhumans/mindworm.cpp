#ifdef HEADER_PHASE
CHARACTER(mindworm, nonhumanoid)
{
protected:
  virtual void GetAICommand () override;

protected:
  truth TryToImplantLarvae (character *);
  void PsiAttack (character *);
};


#else


void mindworm::GetAICommand () {
  character *NeighbourEnemy = GetRandomNeighbour(HOSTILE, /*anybody*/true);

  if (GetConfig() == BOIL && NeighbourEnemy &&
      NeighbourEnemy->IsHumanoid() && /*not in comm. fork*/
      NeighbourEnemy->HasHead() &&
      !NeighbourEnemy->StateIsActivated(PARASITE_MIND_WORM)
      /*&& !NeighbourEnemy->IsInfectedByMindWorm()*/)
  {
    if (TryToImplantLarvae(NeighbourEnemy)) {
      return;
    }
  }

  character *NearestEnemy = GetNearestEnemy();
  if (NearestEnemy && !NearestEnemy->IsESPBlockedByEquipment() &&
      !StateIsActivated(CONFUSED) &&
      !RAND_2/*(RAND() & 2)*/)
  {
    PsiAttack(NearestEnemy);
    return;
  }

  //if (MoveRandomly()) return;
  //EditAP(-1000);
  nonhumanoid::GetAICommand();
}


truth mindworm::TryToImplantLarvae (character *Victim) {
  if (Victim->MindWormCanPenetrateSkull(this) && Victim->CanBeParasitized()) {
    Victim->BeginTemporaryState(PARASITE_MIND_WORM, 400 + RAND_N(200));
    //Victim->SetCounterToMindWormHatch(100);
    if (Victim->IsPlayer()) {
      ADD_MESSAGE("%s digs through your skull, lays %s eggs and jumps out.",
      CHAR_NAME(DEFINITE), CHAR_POSSESSIVE_PRONOUN);
    } else if (Victim->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s digs through %s's skull, lays %s eggs and jumps out.",
      CHAR_NAME(DEFINITE), Victim->CHAR_NAME(DEFINITE), CHAR_POSSESSIVE_PRONOUN);
    }
    MoveRandomly();
    EditAP(-1000);
    return true;
  }
  return false;
}


void mindworm::PsiAttack (character *Victim) {
  if (Victim->IsPlayer()) {
    ADD_MESSAGE("Your brain is on fire.");
  } else if (Victim->CanBeSeenByPlayer() && PLAYER->GetAttribute(PERCEPTION) > RAND_N(20)) {
    ADD_MESSAGE("%s looks scared.", Victim->CHAR_NAME(DEFINITE));
  }

  Victim->ReceiveDamage(this, 1 + RAND_N(5), PSI, ALL, YOURSELF, false, false, false, false); //HEAD?
  Victim->CheckDeath(CONST_S("killed by ") + GetName(INDEFINITE) + "'s psi attack", this);
  EditAP(-2000);
  //EditStamina(GetAdjustedStaminaCost(-1000, GetAttribute(INTELLIGENCE)), false);
  EditStamina(-10000 / GetAttribute(INTELLIGENCE), false);
}


#endif
