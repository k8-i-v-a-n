#ifdef HEADER_PHASE
CHARACTER(mushroom, nonhumanoid)
{
public:
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth IsMushroom () const override;
  virtual truth NeedCheckNearbyItems () const override;

  void SetSpecies (int);
  int GetSpecies () const;

protected:
  virtual void PostConstruct () override;
  virtual void GetAICommand () override;
  virtual col16 GetTorsoMainColor () const override;

protected:
  int Species /*= MakeRGB16(100, 125, 100)*/;
};


#else


truth mushroom::IsMushroom () const { return true; }
int mushroom::GetSpecies () const { return Species; }
col16 mushroom::GetTorsoMainColor () const { return Species; }
truth mushroom::NeedCheckNearbyItems () const { return false; }


void mushroom::Save (outputfile &SaveFile) const {
  nonhumanoid::Save(SaveFile);
  SaveFile << Species;
}


void mushroom::Load (inputfile &SaveFile) {
  nonhumanoid::Load(SaveFile);
  SaveFile >> Species;
}


void mushroom::GetAICommand () {
  SeekLeader(GetLeader());
  if (FollowLeader(GetLeader())) return;

  lsquare *CradleSquare = GetNeighbourLSquare(RAND_N(8));

  if (CradleSquare && !CradleSquare->GetCharacter() && (CradleSquare->GetWalkability() & WALK)) {
    int SpoiledItems = 0;
    int MushroomsNear = 0;

    for (int d = 0; d < 8; ++d) {
      lsquare *Square = CradleSquare->GetNeighbourLSquare(d);
      if (Square) {
        character *Char = Square->GetCharacter();
        if (Char && Char->IsMushroom()) ++MushroomsNear;
        SpoiledItems += Square->GetSpoiledItems();
      }
    }

    if ((SpoiledItems && MushroomsNear < 5 && !RAND_N(50)) ||
        (MushroomsNear < 3 && !RAND_N((1 + MushroomsNear) * 100)))
    {
      mushroom *Child = static_cast<mushroom *>(GetProtoType()->Spawn(GetConfig()));
      Child->SetSpecies(Species);
      Child->SetTeam(GetTeam());
      Child->SetGenerationDanger(GetGenerationDanger());
      Child->PutTo(CradleSquare->GetPos());
      for (int c = 0; c < BASE_ATTRIBUTES; ++c) {
        Child->BaseExperience[c] = RandomizeBabyExperience(BaseExperience[c] * 4);
      }
      if (Child->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s pops out from the ground.", Child->CHAR_NAME(INDEFINITE));
      }
    }
  }

  if (AttackAdjacentEnemyAI()) return;

  if (MoveRandomly()) return;

  EditAP(-1000);
}


void mushroom::PostConstruct () {
  switch (RAND_N(3)) {
    case 0: SetSpecies(MakeRGB16(125 + RAND_N(125), RAND_N(100), RAND_N(100))); break;
    case 1: SetSpecies(MakeRGB16(RAND_N(100), 125 + RAND_N(125), RAND_N(100))); break;
    case 2: SetSpecies(MakeRGB16(RAND_N(100), RAND_N(100), 125 + RAND_N(125))); break;
  }
}


void mushroom::SetSpecies (int What) {
  Species = What;
  UpdatePictures();
}


#endif
