#ifdef HEADER_PHASE
CHARACTER(magicmushroom, mushroom)
{
protected:
  virtual bodypart *MakeBodyPart (int) const override;
  virtual void GetAICommand () override;
};


#else


bodypart *magicmushroom::MakeBodyPart (int) const {
  return magicmushroomtorso::Spawn(0, NO_MATERIALS);
}


void magicmushroom::GetAICommand () {
  if (!RAND_N(750)) {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s disappears.", CHAR_NAME(DEFINITE));
    }
    TeleportRandomly(true);
    EditAP(-1000);
  } else if (!RAND_N(50)) {
    lsquare *Square = GetNeighbourLSquare(RAND_N(8));
    if (Square && Square->IsFlyable()) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s releases odd-looking gas.", CHAR_NAME(DEFINITE));
      }
      Square->AddSmoke(gas::Spawn(MAGIC_VAPOUR, 1000));
      EditAP(-1000);
    }
  } else {
    mushroom::GetAICommand();
  }
}


#endif
