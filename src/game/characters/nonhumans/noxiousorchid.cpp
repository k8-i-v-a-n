#ifdef HEADER_PHASE
CHARACTER(noxiousorchid, nonhumanoid)
{
public:
  virtual truth Hit (character *, v2, int, int = 0) override;

protected:
  virtual col16 GetTorsoSpecialColor () const override;
  virtual void GetAICommand () override;
  virtual void CreateCorpse (lsquare *) override;
  virtual void PostConstruct () override;
};


#else


col16 noxiousorchid::GetTorsoSpecialColor () const {
  if (!GetConfig()) return MakeRGB16(125+RAND_N(100), RAND_N(125), RAND_N(100));
  if (GetConfig() == GREATER) return MakeRGB16(100+RAND_N(100), RAND_N(100), 155+RAND_N(100));
  // giant
  return MakeRGB16(200+RAND_N(55), RAND_N(60), 150);
}


void noxiousorchid::PostConstruct () {
  //GetTorso()->GetMainMaterial()->SetSpoilCounter(200+RAND_N(100));
}


void noxiousorchid::CreateCorpse (lsquare *Square) {
  nonhumanoid::CreateCorpse(Square);
}


truth noxiousorchid::Hit (character *Enemy, v2 HitPos, int Direction, int Flags) {
  // this was `RAND()&2`. it is basically the same as `RAND()&1`, and hence, `RAND_2`.
  if (!RAND_2) {
    liquid *Fluid = 0; // = liquid::Spawn(WATER, 25+RAND_N(25));

    if (IsPlayer()) {
      ADD_MESSAGE("You hit %s.", Enemy->CHAR_DESCRIPTION(DEFINITE));
    } else if (Enemy->IsPlayer() || CanBeSeenByPlayer() || Enemy->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s hits %s.", CHAR_DESCRIPTION(DEFINITE), Enemy->CHAR_DESCRIPTION(DEFINITE));
    }

    int lqConfig = -1;
    auto cfg = GetConfig();
    if (cfg == 0) {
      switch (RAND_N(48)) {
        case 0: lqConfig = ANTIDOTE_LIQUID; break;
        case 1: case 2: case 3: case 4: lqConfig = POISON_LIQUID; break;
        case 5: lqConfig = LIQUID_HORROR; break;
        case 6: case 7: case 8: lqConfig = SULPHURIC_ACID; break;
        default: break;
      }
    } else if (cfg == GREATER) {
      switch (RAND_N(24)) {
        case 0: lqConfig = ANTIDOTE_LIQUID; break;
        case 1: case 2: case 3: case 4: lqConfig = POISON_LIQUID; break;
        case 5: lqConfig = LIQUID_HORROR; break;
        case 6: case 7: case 8: lqConfig = SULPHURIC_ACID; break;
        default: break;
      }
    } else if (cfg == GIANTIC) {
      switch (RAND_N(24)) {
        case 0: lqConfig = ANTIDOTE_LIQUID; break;
        case 1: lqConfig = YELLOW_SLIME; break;
        case 2: case 3: case 4: lqConfig = POISON_LIQUID; break;
        case 5: lqConfig = LIQUID_HORROR; break;
        case 6: case 7: case 8: lqConfig = SULPHURIC_ACID; break;
        case 9: lqConfig = MUSTARD_GAS_LIQUID; break;
        default: break;
      }
    } else {
      //Fluid = liquid::Spawn(WATER, 25+RAND_N(25)); break;
    }

    if (lqConfig > 0) {
      int lqVolume = 0;
           if (cfg == 0) lqVolume = 15+RAND_N(25);
      else if (cfg == GREATER) lqVolume = 25+RAND_N(25);
      else if (cfg == GIANTIC) lqVolume = 50+RAND_N(50);
      if (lqVolume > 0) {
        //ConLogf("SPILL: cfg=%d; volume=%d", lqConfig, lqVolume);
        Fluid = liquid::Spawn(lqConfig, lqVolume);
        //ConLogf("SPILL: <%s:%d>", Fluid->GetTypeID(), Fluid->GetConfig());
        //ConLogf("  0: GetName:<%s>", Fluid->GetName(false, false).CStr());
      }
    }

    if (Fluid) {
      Enemy->SpillFluid(Enemy, Fluid);
      //ConLogf("  1: GetName:<%s>", Fluid->GetName(false, false).CStr());
      if (IsPlayer()) {
        ADD_MESSAGE("You spill %s on %s.", Fluid->GetName(false, false).CStr(), Enemy->CHAR_DESCRIPTION(DEFINITE));
      } else if (Enemy->IsPlayer() || CanBeSeenByPlayer() || Enemy->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s spills %s on %s.", CHAR_DESCRIPTION(DEFINITE), Fluid->GetName(false, false).CStr(), Enemy->CHAR_DESCRIPTION(DEFINITE));
      }
    }
  } else if (nonhumanoid::Hit(Enemy, HitPos, Direction, Flags)) {
    return true;
  } else if (IsPlayer()) {
    ADD_MESSAGE("You miss %s.", Enemy->CHAR_DESCRIPTION(DEFINITE));
  }

  EditAP(-1000);

  return true;
}


void noxiousorchid::GetAICommand () {
  SeekLeader(GetLeader());
  if (FollowLeader(GetLeader())) return;
  if (AttackAdjacentEnemyAI()) return;
  if (CheckForUsefulItemsOnGround()) return;
  if (MoveRandomly()) return;
  EditAP(-1000);
}


#endif
