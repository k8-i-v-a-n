#ifdef HEADER_PHASE
CHARACTER(pig, nonhumanoid)
{
public:
  virtual truth CannotDrinkPotions () const override;
};


#else

// this allows potion drinking (pigs are smart!)
truth pig::CannotDrinkPotions () const { return false; }


#endif
