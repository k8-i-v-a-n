#ifdef HEADER_PHASE
CHARACTER(reddragon, nonhumanoid)
{
public:
  virtual truth CannotDrinkPotions () const override;
};


#else

// this allows potion drinking
truth reddragon::CannotDrinkPotions () const { return false; }

#endif
