#ifdef HEADER_PHASE
CHARACTER(snake, nonhumanoid)
{
protected:
  virtual bodypart *MakeBodyPart (int) const override;
  virtual truth SpecialBiteEffect (character *, v2, int, int, truth, truth Critical, int DoneDamage) override;
};


#else


//==========================================================================
//
//  snake::MakeBodyPart
//
//==========================================================================
bodypart *snake::MakeBodyPart (int) const {
  return snaketorso::Spawn(0, NO_MATERIALS);
}


//==========================================================================
//
//  snake::SpecialBiteEffect
//
//==========================================================================
truth snake::SpecialBiteEffect (character *Char, v2, int, int, truth BlockedByArmour,
                                truth Critical, int DoneDamage)
{
  if (!BlockedByArmour || Critical) {
    //Char->BeginTemporaryState(POISONED, 400 + RAND_N(200));
    const int cfg = GetConfig();
         if (cfg == RED_SNAKE) Char->BeginTemporaryState(/*PANIC*/CONFUSED, 400 + RAND_N(200));
    else if (cfg == GREEN_SNAKE) Char->BeginTemporaryState(POISONED, 400 + RAND_N(200));
    else if (cfg == BLUE_SNAKE) Char->BeginTemporaryState(SLOW, 400 + RAND_N(200));
    else Char->BeginTemporaryState(POISONED, 400 + RAND_N(200)); /* just in case */
    return true;
  }
  return false;
}


#endif
