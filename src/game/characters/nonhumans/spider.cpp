#ifdef HEADER_PHASE
CHARACTER(spider, nonhumanoid)
{
public:
  virtual truth IsSpider () const override;

protected:
  virtual truth SpecialBiteEffect (character *, v2, int, int, truth, truth Critical, int DoneDamage) override;
  virtual void GetAICommand () override;
  virtual bodypart *MakeBodyPart (int) const override;
};


#else


truth spider::IsSpider () const { return true; }
bodypart* spider::MakeBodyPart (int) const { return spidertorso::Spawn(0, NO_MATERIALS); }


truth spider::SpecialBiteEffect (character *Victim, v2 HitPos, int BodyPartIndex,
                                 int Direction, truth BlockedByArmour, truth Critical,
                                 int DoneDamage)
{
  if (!BlockedByArmour || Critical) {
    if (GetConfig() == GIANT_GOLD) {
      bodypart *BodyPart = Victim->GetBodyPart(BodyPartIndex);
      if (BodyPart && BodyPart->IsMaterialChangeable()) {
        festring Desc;
        int CurrentHP = BodyPart->GetHP();
        BodyPart->AddName(Desc, UNARTICLED);

        // Instead of a cockatrice turning you to stone, gold spider will turn you to gold!
        BodyPart->SetMainMaterial(MAKE_MATERIAL(GOLD));

        // Here changing material would revert all damage done, but we don't want that.
        CurrentHP = Min(CurrentHP, BodyPart->GetHP());
        BodyPart->SetHP(CurrentHP);

        if (Victim->IsPlayer()) {
          Desc << " tingles painfully";
          ADD_MESSAGE("Your %s.", Desc.CStr());
        } else if (Victim->CanBeSeenByPlayer()) {
          Desc << " vibrates and changes into gold";
          ADD_MESSAGE("%s's %s.", Victim->CHAR_DESCRIPTION(DEFINITE), Desc.CStr());
        }
        return true;
      }
    } else {
      Victim->BeginTemporaryState(POISONED, GetConfig() == LARGE ? 80 + RAND_N(40) : 400 + RAND_N(200));
    }
    return true;
  }
  return false;
}


void spider::GetAICommand () {
  SeekLeader(GetLeader());
  if (FollowLeader(GetLeader())) return;

  character *NearestChar = 0;
  sLong NearestDistance = 0x7FFFFFFF;
  v2 Pos = GetPos();
  int Hostiles = 0;

  for (int c = 0; c < game::GetTeams(); ++c) {
    if (GetTeam()->GetRelation(game::GetTeam(c)) == HOSTILE) {
      for (auto &it : game::GetTeam(c)->GetMember()) {
        if (it->IsEnabled() && GetAttribute(WISDOM) < it->GetAttackWisdomLimit()) {
          sLong ThisDistance = Max<sLong>(abs(it->GetPos().X-Pos.X), abs(it->GetPos().Y-Pos.Y));
          ++Hostiles;
          if ((ThisDistance < NearestDistance || (ThisDistance == NearestDistance && !RAND_N(3))) &&
              it->CanBeSeenBy(this, false, IsGoingSomeWhere()) &&
              (!IsGoingSomeWhere() || HasClearRouteTo(it->GetPos())))
          {
            NearestChar = it;
            NearestDistance = ThisDistance;
          }
        }
      }
    }
  }

  if (Hostiles && !RAND_N(Max(80 / Hostiles, 8))) {
    web *Web = web::Spawn();
    Web->SetStrength(GetConfig() == LARGE ? 10 : 25);
    if (GetLSquareUnder()->AddTrap(Web)) {
      if (CanBeSeenByPlayer()) ADD_MESSAGE("%s spins a web.", CHAR_NAME(DEFINITE));
      EditAP(-1000);
      return;
    }
  }

  if (NearestChar) {
    if (NearestChar->IsStuck()) SetGoingTo(NearestChar->GetPos()); else SetGoingTo((Pos<<1)-NearestChar->GetPos());
    if (MoveTowardsTarget(true)) return;
  }

  if (MoveRandomly()) return;

  EditAP(-1000);
}


#endif
