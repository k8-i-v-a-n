#ifdef HEADER_PHASE
CHARACTER(unicorn, nonhumanoid)
{
public:
  virtual int TakeHit (character *, item *, bodypart *, v2, double, double, int, int, int, truth, truth) override;
  virtual truth SpecialEnemySightedReaction (character *) override;
  virtual truth CannotDrinkPotions () const override;

protected:
  void MonsterTeleport (cchar *);
};


#else


// this allows potion drinking
truth unicorn::CannotDrinkPotions () const { return false; }


truth unicorn::SpecialEnemySightedReaction (character *) {
  if (!RAND_16) {
    MonsterTeleport("neighs happily");
    return true;
  }

  if (StateIsActivated(PANIC) || (RAND_2 && IsInBadCondition())) {
    MonsterTeleport("neighs");
    return true;
  }

  if (!RAND_N(3) && MoveRandomly()) return true;

  return false;
}


int unicorn::TakeHit (character *Enemy, item *Weapon, bodypart *EnemyBodyPart, v2 HitPos, double Damage, double ToHitValue, int Success, int Type, int Direction, truth Critical, truth ForceHit) {
  int Return = nonhumanoid::TakeHit(Enemy, Weapon, EnemyBodyPart, HitPos, Damage, ToHitValue, Success, Type, Direction, Critical, ForceHit);
  if (Return != HAS_DIED && (StateIsActivated(PANIC) || (RAND_2 && IsInBadCondition()) || !RAND_8)) {
    MonsterTeleport(" in terror");
  }
  return Return;
}


void unicorn::MonsterTeleport (cchar *NeighMsg) {
  v2 np = GetLevel()->GetRandomSquare(this);
  if (np != ERROR_V2) {
         if (IsPlayer()) ADD_MESSAGE("You neigh%s and disappear.", NeighMsg);
    else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s neighs%s and disappears!", CHAR_NAME(DEFINITE), NeighMsg);

    Move(np, true);

         if (IsPlayer()) ADD_MESSAGE("You reappear.");
    else if(CanBeSeenByPlayer()) ADD_MESSAGE("Suddenly %s appears from nothing!", CHAR_NAME(INDEFINITE));

    EditAP(-1000);
  } else {
    SendToHell();
  }
}


#endif
