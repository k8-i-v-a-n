/* *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include <string.h>
#include <stdio.h>

#include "whandler.h"
#include "fesave.h"
#include "feparse.h"
#include "graphics.h"
#include "rawbit.h"
#include "command.h"
#include "char.h"
#include "message.h"
#include "game.h"
#include "stack.h"
#include "room.h"
#include "god.h"
#include "felist.h"
#include "iconf.h"
#include "bitmap.h"
#include "actions.h"
#include "itemset.h"
#include "worldmap.h"
#include "wsquare.h"
#include "materia.h"
#include "database.h"
#include "team.h"
#include "wterra.h"
#include "proto.h"

#define KEYBIND_FILE_VERSION  "version_1"


#define COMMAND(name)  static truth name (character *Char)


//==========================================================================
//
//  GetDirCommandName
//
//==========================================================================
static cchar *GetDirCommandName (int idx, int *Dir) {
  const char *res = 0;
  int dirRes = -1;
  switch (idx) {
    case 0: res = "MoveLeftUp"; dirRes = 0; break;
    case 1: res = "MoveUp"; dirRes = 1; break;
    case 2: res = "MoveRightUp"; dirRes = 2; break;
    case 3: res = "MoveLeft"; dirRes = 3; break;
    case 4: res = "MoveRight"; dirRes = 4; break;
    case 5: res = "MoveLeftDown"; dirRes = 5; break;
    case 6: res = "MoveDown"; dirRes = 6; break;
    case 7: res = "MoveRightDown"; dirRes = 7; break;
    case 8: res = "NoMovement"; dirRes = 8; break;
    case 9: res = "GoLeftUp"; dirRes = 100; break;
    case 10: res = "GoUp"; dirRes = 101; break;
    case 11: res = "GoRightUp"; dirRes = 102; break;
    case 12: res = "GoLeft"; dirRes = 103; break;
    case 13: res = "GoRight"; dirRes = 104; break;
    case 14: res = "GoLeftDown"; dirRes = 105; break;
    case 15: res = "GoDown"; dirRes = 106; break;
    case 16: res = "GoRightDown"; dirRes = 107; break;
    default: break;
  }
  if (Dir) *Dir = dirRes;
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
// command
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  command::command
//
//==========================================================================
command::command (cchar *name, truth (*LinkedFunction)(character *), cchar *Description,
                  cchar *KeyName, truth UsableInWilderness, truth WizardModeFunction)
  : mName(name)
  , LinkedFunction(LinkedFunction)
  , Description(Description)
  , KeyList()
  , UsableInWilderness(UsableInWilderness)
  , WizardModeFunction(WizardModeFunction)
{
  AddKey(KeyName);
}


//==========================================================================
//
//  command::ClearKeys
//
//==========================================================================
void command::ClearKeys () {
  KeyList.clear();
}


//==========================================================================
//
//  command::AddKey
//
//==========================================================================
void command::AddKey (cchar *KeyName) {
  if (!WizardModeFunction && KeyName && KeyName[0]) {
    int Key = globalwindowhandler::NameToKey(CONST_S(KeyName));
    if (Key > 0) {
      auto it = KeyList.find(Key);
      if (it == KeyList.end()) {
        KeyList.insert(Key);
        #if 0
        ConLogf("DEBUG: bound key '%s' (%s) to command '%s'.",
                KeyName, globalwindowhandler::GetKeyName(Key).CStr(),
                mName.CStr());
        #endif
      }
    } else {
      ConLogf("ERROR: invalid key '%s' for command '%s'!", KeyName, mName.CStr());
    }
  }
}


//==========================================================================
//
//  command::IsKey
//
//==========================================================================
bool command::IsKey (int Key) {
  if (Key > 0) {
    auto it = KeyList.find(Key);
    return (it != KeyList.end());
  } else {
    return false;
  }
}


//==========================================================================
//
//  command::GetKeyName
//
//==========================================================================
festring command::GetKeyName (int idx) const {
  if (idx >= 0 && idx < (int)KeyList.size()) {
    for (auto &&it : KeyList) {
      if (idx == 0) return globalwindowhandler::GetKeyName(it);
      idx -= 1;
    }
  }
  return festring();
}


// ////////////////////////////////////////////////////////////////////////// //
// commandsystem
// ////////////////////////////////////////////////////////////////////////// //

std::vector<command *> commandsystem::mCommands;
std::vector<commandsystem::MoveKey> commandsystem::mMoveKeys;


//==========================================================================
//
//  commandsystem::GetMoveCommandKey
//
//  return 0 on invalid dir; [0..9]
//
//==========================================================================
int commandsystem::GetMoveCommandKey (int Dir) {
  if (Dir >= 0 && Dir <= 9) {
    for (auto &&it : mMoveKeys) {
      if (it.Dir == Dir) return it.Key;
    }
  }
  return 0;
}


//==========================================================================
//
//  commandsystem::MoveKeyToDir
//
//  return -1, or dir; if ctrl is allowed and pressed, return 1000+dir
//
//==========================================================================
int commandsystem::MoveKeyToDir (int Key, bool allowFastExt) {
  if (Key > 0) {
    for (auto &&it : mMoveKeys) {
      if (it.Key == Key) {
        if (allowFastExt || it.Dir < 100) return it.Dir;
      }
    }
  }
  return -1;
}


//==========================================================================
//
//  commandsystem::UnbindDirKeys
//
//==========================================================================
void commandsystem::UnbindDirKeys () {
  mMoveKeys.clear();
}


//==========================================================================
//
//  commandsystem::BindDirKey
//
//==========================================================================
void commandsystem::BindDirKey (int Dir, cchar *KeyName, bool fastExt) {
  if (Dir >= 0 && Dir <= 8) {
    if (KeyName && KeyName[0]) {
      int Key = globalwindowhandler::NameToKey(CONST_S(KeyName));
      if (Key == 0) {
        ConLogf("ERROR: invalid key '%s' for %sdirection #%d!", KeyName,
                (fastExt ? "fast " : ""), Dir);
      } else {
        if (fastExt) Dir += 100;
        bool again;
        do {
          again = false;
          for (size_t f = 0; !again && f != mMoveKeys.size(); f += 1) {
            if (mMoveKeys[f].Key == Key) {
              mMoveKeys.erase(mMoveKeys.begin() + f);
              again = true;
            }
          }
        } while (again);
        mMoveKeys.push_back(MoveKey(Dir, Key));
      }
    }
  } else {
    ConLogf("ERROR: invalid direction #%d", Dir);
  }
}


//==========================================================================
//
//  commandsystem::GetSelfDirKey
//
//==========================================================================
festring commandsystem::GetSelfDirKey () {
  festring res;
  for (size_t f = 0; f != mMoveKeys.size(); f += 1) {
    if (mMoveKeys[f].Dir == 8) {
      festring kn = globalwindowhandler::GetKeyName(mMoveKeys[f].Key);
      if (!kn.IsEmpty() && (res.IsEmpty() || res.GetSize() > kn.GetSize())) {
        res = kn;
      }
    }
  }
  return res;
}


//==========================================================================
//
//  commandsystem::GetFullConfigFileName
//
//==========================================================================
festring commandsystem::GetFullConfigFileName () {
  festring fname;
  fname << game::GetConfigPath();
  fname << "k8ivan_keys.rc";
  return fname;
}


//==========================================================================
//
//  commandsystem::ConfigureKeys
//
//==========================================================================
void commandsystem::ConfigureKeys () {
  command *cmd;
  static char buf[512];
  festring fname = GetFullConfigFileName();
  FILE *fl = fopen(fname.CStr(), "r");
  if (!fl) return;
  bool versionOk = false;
  while (!versionOk && fgets(buf, 510, fl)) {
    festring cmdstr = CONST_S(buf);
    cmdstr.TrimAll();
    versionOk = cmdstr.EquCI(KEYBIND_FILE_VERSION);
  }
  if (versionOk) {
    while (fgets(buf, 510, fl)) {
      festring cmdstr = CONST_S(buf);
      cmdstr.TrimAll();
      if (cmdstr.IsEmpty() || cmdstr[0] == '#') continue;
      std::vector<festring> args;
      while (!cmdstr.IsEmpty()) {
        festring::sizetype pos = 0;
        while (pos != cmdstr.GetSize() && (unsigned char)(cmdstr[pos]) > 32) {
          pos += 1;
        }
        if (pos > 0) {
          args.push_back(cmdstr.LeftCopy((int)pos));
          cmdstr.Erase(0, pos);
        } else {
          cmdstr.TrimLeft();
        }
      }
      if (args.empty()) continue;
      if (args[0].StartsWithCI("version")) {
        // do nothing
      } else if (args[0].EquCI("unbind_all")) {
        for (int f = 0; (cmd = commandsystem::GetCommand(f)) != 0; f += 1) {
          cmd->ClearKeys();
        }
        UnbindDirKeys();
      } else if (args[0].EquCI("unbind_all_commands")) {
        for (int f = 0; (cmd = commandsystem::GetCommand(f)) != 0; f += 1) {
          cmd->ClearKeys();
        }
      } else if (args[0].EquCI("unbind_all_dirs")) {
        UnbindDirKeys();
      } else if (args[0].EquCI("bind") && args.size() == 3) {
        int key = globalwindowhandler::NameToKey(args[1]);
        if (key == 0) {
          ConLogf("ERROR: unknown key name: '%s'!", args[1].CStr());
        } else {
          bool found = false;
          for (int f = 0; !found && (cmd = commandsystem::GetCommand(f)) != 0; f += 1) {
            if (cmd->GetName().EquCI(args[2])) {
              //ConLogf("DEBUG: bound key '%s' to command '%s'.", args[1].CStr(), args[2].CStr());
              cmd->AddKey(args[1].CStr());
              found = true;
            }
          }
          if (!found) {
            // try movement
            int didx = 0;
            while (!found) {
              int dir;
              cchar *dcmd = GetDirCommandName(didx, &dir);
              if (!dcmd) break;
              found = args[2].EquCI(dcmd);
              if (!found) didx += 1;
            }
            if (found) {
              int dir;
              cchar *dcmd = GetDirCommandName(didx, &dir);
              IvanAssert(dcmd);
              bool fastExt = false;
              if (dir >= 100) { dir -= 100; fastExt = true; }
              BindDirKey(dir, args[1].CStr(), fastExt);
            } else {
              ConLogf("ERROR: unknown command '%s'!", args[2].CStr());
            }
          }
        }
      } else {
        ConLogf("ERROR: unknown or invalid keybind command: '%s'!", args[0].CStr());
      }
    }
  } else {
    ConLogf("WARNING: ignored keybinding file due to invalid version.");
  }
  fclose(fl);
}


//==========================================================================
//
//  CheckKeyFileVersion
//
//==========================================================================
static bool CheckKeyFileVersion (cfestring &fname) {
  static char buf[512];
  FILE *fl = fopen(fname.CStr(), "r");
  if (!fl) return false;
  while (fgets(buf, 510, fl)) {
    festring cmdstr = CONST_S(buf);
    cmdstr.TrimAll();
    if (cmdstr.EquCI(KEYBIND_FILE_VERSION)) {
      fclose(fl);
      return true;
    }
  }
  fclose(fl);
  return false;
}


//==========================================================================
//
//  commandsystem::SaveKeys
//
//==========================================================================
void commandsystem::SaveKeys (truth forced) {
  command *cmd;
  //int isWizard = 0;
  festring fname = GetFullConfigFileName();
  if (!forced) {
    //if (inputfile::fileExists(fname)) return;
    if (CheckKeyFileVersion(fname)) return;
  }
  FILE *fl = fopen(fname.CStr(), "w");
  if (!fl) return;
  fprintf(fl, "%s\n", KEYBIND_FILE_VERSION);
  fputs("# keyboard bindings\n", fl);
  fputs("unbind_all\n", fl);
  fputs("# there are also `unbind_all_commands` and `unbind_all_dirs`\n", fl);
  fputs("\n# commands\n", fl);
  //for (; isWizard < 2; isWizard++)
  {
    //fputs(isWizard ? "\n# wizard actions\n" : "# actions\n", fl);
    //fputs("# keyboard bindings\n", fl);
    for (int f = 0; (cmd = commandsystem::GetCommand(f)) != 0; f += 1) {
      /*
      if (cmd->IsWizardModeFunction()) {
        if (!isWizard) continue;
      } else {
        if (isWizard) continue;
      }
      */
      if (!cmd->IsWizardModeFunction()) {
        cchar *dsc = cmd->GetName().CStr();
        if (!dsc || !dsc[0]) continue;
        for (int idx = 0;; idx += 1) {
          festring kname = cmd->GetKeyName(idx);
          if (kname.IsEmpty()) break;
          fprintf(fl, "bind %s %s\n", kname.CStr(), dsc);
        }
      }
    }
  }

  fputs("\n# movement\n", fl);
  int didx = 0;
  for (;;) {
    int dir;
    cchar *dcmd = GetDirCommandName(didx, &dir);
    if (!dcmd) break;
    didx += 1;
    for (auto &&it : mMoveKeys) {
      if (it.Dir == dir) {
        festring kname = globalwindowhandler::GetKeyName(it.Key);
        if (!kname.IsEmpty()) {
          fprintf(fl, "bind %s %s\n", kname.CStr(), dcmd);
        } else {
          ConLogf("ERROR: command '%s' has invalid key (0x%08x). WTF?!", dcmd,
                  (unsigned)it.Key);
        }
      }
    }
  }

  fclose(fl);
}


//==========================================================================
//
//  commandsystem::GetMoveKeyDescr
//
//==========================================================================
festring commandsystem::GetMoveKeyDescr (int aDir, bool fast) {
  if (aDir >= 0 && aDir <= 8) {
    const int ndir = aDir + (fast ? 100 : 0);
    switch (ndir) {
      case 0: return CONST_S("move left and up");
      case 1: return CONST_S("move up");
      case 2: return CONST_S("move right and up");
      case 3: return CONST_S("move left");
      case 4: return CONST_S("move right");
      case 5: return CONST_S("move left and down");
      case 6: return CONST_S("move down");
      case 7: return CONST_S("move right and down");
      case 8: return CONST_S("no movement (used in lome location queries)");
      case 100: return CONST_S("go left and up");
      case 101: return CONST_S("go up");
      case 102: return CONST_S("go right and up");
      case 103: return CONST_S("go left");
      case 104: return CONST_S("go right");
      case 105: return CONST_S("go left and down");
      case 106: return CONST_S("go down");
      case 107: return CONST_S("go right and down");
      default: break;
    }
  }
  return festring();
}


//==========================================================================
//
//  commandsystem::GetMoveKeyName
//
//==========================================================================
festring commandsystem::GetMoveKeyName (int aDir, int idx, bool fast) {
  if (aDir >= 0 && aDir <= 8 && idx >= 0) {
    const int ndir = aDir + (fast ? 100 : 0);
    for (size_t f = 0; f != mMoveKeys.size(); f += 1) {
      if (mMoveKeys[f].Dir == ndir) {
        if (idx == 0) return globalwindowhandler::GetKeyName(mMoveKeys[f].Key);
        idx -= 1;
      }
    }
  }
  return festring();
}


//==========================================================================
//
//  commandsystem::SortCommands
//
//==========================================================================
void commandsystem::SortCommands () {
  std::sort(mCommands.begin(), mCommands.end(),
    [&] (const command *a, const command *b) {
      //return (a->GetName().CompareCI(b->GetName()) < 0);
      festring s = CONST_S(a->GetDescription());
      return (s.CompareCI(b->GetDescription()) < 0);
    });
}


//==========================================================================
//
//  commandsystem::RegisterCommand
//
//==========================================================================
void commandsystem::RegisterCommand (command *cmd) {
  if (cmd->IsWizardModeFunction()) {
    cmd->ClearKeys();
  }
  for (size_t f = 0; f < mCommands.size(); ++f) {
    if (mCommands[f]->GetName().EquCI(cmd->GetName())) {
      // replace command
      delete mCommands[f];
      mCommands[f] = cmd;
      SortCommands();
      return;
    }
  }
  mCommands.push_back(cmd);
  SortCommands();
}


//==========================================================================
//
//  commandsystem::FindCommand
//
//==========================================================================
command *commandsystem::FindCommand (cchar *name) {
  if (name && name[0]) {
    for (size_t f = 0; f < mCommands.size(); ++f) {
      if (mCommands[f]->GetName() == name) {
        return mCommands[f];
      }
    }
  }
  return 0;
}


//==========================================================================
//
//  commandsystem::FindCommandByKey
//
//==========================================================================
command *commandsystem::FindCommandByKey (int Key) {
  command *cmd = nullptr;
  if (Key > 0) {
    for (size_t f = 0; !cmd && f != mCommands.size(); f += 1) {
      if (mCommands[f]->IsKey(Key)) {
        cmd = mCommands[f];
      }
    }
  }
  return cmd;
}


//==========================================================================
//
//  commandsystem::AddCommandKey
//
//==========================================================================
void commandsystem::AddCommandKey (cchar *name, cchar *KeyName) {
  if (KeyName && KeyName[0]) {
    command *cmd = FindCommand(name);
    if (cmd) {
      cmd->AddKey(KeyName);
    } else {
      ConLogf("ERROR: invalid key '%s' for unknown command '%s'!", KeyName, name);
    }
  }
}


//==========================================================================
//
//  commandsystem::GetHelpKeyNameFor
//
//==========================================================================
festring commandsystem::GetHelpKeyNameFor (cchar *cmdname) {
  festring kname;
  command *cmd = commandsystem::FindCommand(cmdname);
  if (cmd) {
    for (int idx = 0;; idx += 1) {
      festring kn = cmd->GetKeyName(idx);
      if (kn.IsEmpty()) break;
      if (kname.IsEmpty() || kname.Length() < kn.Length()) {
        kname = kn;
      }
    }
  }
  return kname;
}


// ////////////////////////////////////////////////////////////////////////// //
// common code
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  Consume
//
//==========================================================================
static truth Consume (character *Char, cchar *ConsumeVerb, sorter Sorter, truth nibbling=false) {
  lsquare *Square = Char->GetLSquareUnder();
  stack *Inventory = Char->GetStack();
  stack *StackUnder = Square->GetStack();
  if ((game::IsInWilderness() || !StackUnder->SortedItems(Char, Sorter)) &&
      !Inventory->SortedItems(Char, Sorter))
  {
    ADD_MESSAGE("You have nothing to %s!", ConsumeVerb);
    return false;
  }
  itemvector Item;
  festring Question = CONST_S("What do you wish to ")+ConsumeVerb+'?';
  if (!game::IsInWilderness() && StackUnder->SortedItems(Char, Sorter)) {
    Inventory->DrawContents(Item, StackUnder, Char, Question,
                            CONST_S("Items in your inventory"),
                            CONST_S("Items on the ground"),
                            CONST_S(""), 0, NO_MULTI_SELECT|SELECT_ZEROPICK_FIRST, Sorter);
  } else {
    Inventory->DrawContents(Item, Char, Question, NO_MULTI_SELECT, Sorter);
  }
  if (strcmp(ConsumeVerb, "taste") == 0) ConsumeVerb = "tast";
  return (!Item.empty() ? Char->ConsumeItem(Item[0], CONST_S(ConsumeVerb) + "ing", nibbling) : false);
}


////////////////////////////////////////////////////////////////////////////////
#include "commands/0list.cpp"

#define CCL(name_,cmd_,dsc_,key_)  RegisterCommand(new command(name_, cmd_, dsc_, key_))
#define CCW(name_,cmd_,dsc_,key_)  RegisterCommand(new command(name_, cmd_, dsc_, key_, true))
#define CCC(name_,cmd_,dsc_,key_,ww_)  RegisterCommand(new command(name_, cmd_, dsc_, key_, ww_, true))

//==========================================================================
//
//  commandsystem::Init
//
//  first bool: usable in wilderness (def: false)
//  second bool: WM command (def: false)
//
//==========================================================================
void commandsystem::Init () {
  if (!mCommands.empty()) return;
  CCL("Apply",              &Apply,               "apply",                              "a");
  CCL("Talk",               &Talk,                "chat",                               "S-C");
  CCL("CanMeat",            &CanMeat,             "can lump of flesh",                  "S-N");
  CCL("Close",              &Close,               "close",                              "c");
  CCW("Dip",                &Dip,                 "dip",                                "S-1");
  CCL("Dump",               &Dump,                "dump potion/can contents",           "S-U");
  CCW("Drink",              &Drink,               "drink",                              "S-D");
  CCW("Drop",               &Drop,                "drop",                               "d");
  CCW("Eat",                &Eat,                 "eat",                                "e");
  CCL("WhatToEngrave",      &WhatToEngrave,       "engrave",                            "S-G");
  CCW("EquipmentScreen",    &EquipmentScreen,     "equipment menu",                     "S-E");
  CCW("Go",                 &Go,                  "go",                                 "g");
  CCW("GoStairsDown",       &GoDown,              "go down/enter area",                 "S-.");
  CCW("GoStairsUp",         &GoUp,                "go up",                              "S-,");
  CCL("IssueCommand",       &IssueCommand,        "issue command(s) to team member(s)", "S-I");
  CCL("Kick",               &Kick,                "kick",                               "k");
  CCL("Disarm",             &Untrap,              "disarm trap or mine",                "S-K");
  CCW("Look",               &Look,                "look",                               "C-L");
  CCW("LookSquare",         &LookSquare,          "redescribe map position",            "S-L");
  CCW("MiniMapNormal",      &MapNormal,           "show minimap /2",                    "M-M");
  CCW("MiniMapSmall",       &MapSmall,            "show minimap /4",                    "M-S-M");
  CCL("AssignName",         &AssignName,          "name",                               "n");
  CCL("Offer",              &Offer,               "offer",                              "S-O");
  CCW("Open",               &Open,                "open",                               "o");
  CCL("PickUp",             &PickUp,              "pick up",                            ",");
  CCL("Pray",               &Pray,                "pray",                               "S-P"); //FIXME: should be usable in wilderness?
  CCW("Quit",               &Quit,                "quit",                               "S-Q");
  CCL("Read",               &Read,                "read",                               "r");   //FIXME: should be usable in wilderness?
  CCW("Rest",               &Rest,                "rest/heal",                          "h");
  CCW("Save",               &Save,                "save game",                          "S-S");
  CCW("Taste",              &Taste,               "taste liquid",                       "S-T");
  CCW("ScrollMessagesDown", &ScrollMessagesDown,  "scroll messages down",               "M-Down");
  CCW("ScrollMessagesUp",   &ScrollMessagesUp,    "scroll messages up",                 "M-Up");
  // saving/loading is quite fast. there is no reason to have in-game options.
  // not that i don't want to have them, but some options might not even work
  // without restarting. so why bother?
  //CCW("ShowConfigScreen", &ShowConfigScreen, "show config screen", "M-C");
  CCW("ShowInventory",      &ShowInventory,       "show inventory",                     "i");
  CCW("ShowKeyLayout",      &ShowKeyLayout,       "show key layout",                    "F1");
  CCW("DrawMessageHistory", &DrawMessageHistory,  "show message history",               "M-H");
  CCW("ShowWeaponSkills",   &ShowWeaponSkills,    "show weapon skills",                 "S-2");
  CCL("Search",             &Search,              "search",                             "s");
  CCL("Sit",                &Sit,                 "sit",                                "_");
  CCL("Throw",              &Throw,               "throw",                              "t");
  CCW("ToggleRunning",      &ToggleRunning,       "toggle running",                     "u");
  CCL("ForceVomit",         &ForceVomit,          "vomit",                              "S-V");
  CCW("NOP",                &NOP,                 "wait",                               ".");
  CCW("WieldInRightArm",    &WieldInRightArm,     "wield in right arm",                 "w");
  CCW("WieldInLeftArm",     &WieldInLeftArm,      "wield in left arm",                  "S-W");
  CCL("Burn",               &Burn,                "burn",                               "S-B");
  CCL("Zap",                &Zap,                 "zap",                                "z");

  CCW("SaveScreenshot",     &Screenshot,          "save screenshot",                    "PrScr");

  // wizard commands need to be registered, but accessible only from the console
  CCW("WizardMode",           &WizardMode,          "wizard mode activation", 0/*"X"*/);
  /* Sort according to key */
  CCC("RaiseStats",           &RaiseStats,          "raise stats", "1", true);
  CCC("LowerStats",           &LowerStats,          "lower stats", "2", true);
  CCC("SeeWholeMap",          &SeeWholeMap,         "see whole map", "3", true);
  CCC("WalkThroughWalls",     &WalkThroughWalls,    "toggle walk through walls mode", "4", true);
  CCC("RaiseGodRelations",    &RaiseGodRelations,   "raise your relations to the gods", "5", true);
  CCC("LowerGodRelations",    &LowerGodRelations,   "lower your relations to the gods", "6", true);
  CCC("WizardWish",           &WizardWish,          "wish something", "~", true);
  CCC("GainDivineKnowledge",  &GainDivineKnowledge, "gain knowledge of all gods", "\"", true);
  CCC("GainAllItems",         &GainAllItems,        "gain all items", "$", true);
  CCC("SecretKnowledge",      &SecretKnowledge,     "reveal secret knowledge", "*", true);
  CCC("DetachBodyPart",       &DetachBodyPart,      "detach a limb", "0", true);
  CCC("SummonMonster",        &SummonMonster,       "summon monster", "&", false);
  CCC("LevelTeleport",        &LevelTeleport,       "level teleport", "|", false);
  CCC("Possess",              &Possess,             "possess creature", "{", false);
  CCC("Polymorph",            &Polymorph,           "polymorph", "[", true);
  CCC("GetScroll",            &GetScroll,           "get scroll", "R", true);
  CCC("ShowCoords",           &ShowCoords,          "show current coordinates", "(", true);
  CCC("WizardHeal",           &WizardHeal,          "wizard healing", "H", true);
  CCC("WizardBlow",           &WizardBlow,          "wizard blowing", "%", false);
  CCC("WizardTeam",           &WizardTeam,          "wizard teaming", "/", false);
  CCC("WizardPOI",            &WizardPOI,           "teleport to POI", ")", true);

  /* aliases for most some commands */
  //AddCommandKey("ShowKeyLayout", KEY_F1);
  AddCommandKey("Eat", "Delete");
  AddCommandKey("PickUp", "Insert");
  AddCommandKey("EquipmentScreen", "Numpad+");
  AddCommandKey("MiniMapNormal", "Tab");
  AddCommandKey("MiniMapSmall", "S-Tab");
  AddCommandKey("NOP", "Num5");

  AddCommandKey("GoStairsDown", "S-Down");
  AddCommandKey("GoStairsUp", "S-Up");

  UnbindDirKeys();
  BindDirKey(0, "Home", false);
  BindDirKey(1, "Up", false);
  BindDirKey(2, "PageUp", false);
  BindDirKey(3, "Left", false);
  BindDirKey(4, "Right", false);
  BindDirKey(5, "End", false);
  BindDirKey(6, "Down", false);
  BindDirKey(7, "PageDown", false);
  BindDirKey(8, "Num5", false);
  BindDirKey(8, ".", false);

  BindDirKey(0, "C-Home", true);
  BindDirKey(1, "C-Up", true);
  BindDirKey(2, "C-PageUp", true);
  BindDirKey(3, "C-Left", true);
  BindDirKey(4, "C-Right", true);
  BindDirKey(5, "C-End", true);
  BindDirKey(6, "C-Down", true);
  BindDirKey(7, "C-PageDown", true);
}

#undef CCL
#undef CCW
#undef CCC
