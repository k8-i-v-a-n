/* *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include <string.h>
#include <stdio.h>

#include "whandler.h"
#include "fesave.h"
#include "feparse.h"
#include "graphics.h"
#include "rawbit.h"
#include "command.h"
#include "char.h"
#include "message.h"
#include "game.h"
#include "stack.h"
#include "room.h"
#include "god.h"
#include "felist.h"
#include "iconf.h"
#include "bitmap.h"
#include "actions.h"
#include "itemset.h"
#include "worldmap.h"
#include "wsquare.h"
#include "materia.h"
#include "database.h"
#include "team.h"
#include "wterra.h"
#include "proto.h"

#define KEYBIND_FILE_VERSION  "version_1"


#define COMMAND(name)  static truth name (character *Char)


//==========================================================================
//
//  GetDirCommandName
//
//==========================================================================
static cchar *GetDirCommandName (int idx, int *Dir) {
  const char *res = 0;
  int dirRes = -1;
  switch (idx) {
    case 0: res = "MoveLeftUp"; dirRes = 0; break;
    case 1: res = "MoveUp"; dirRes = 1; break;
    case 2: res = "MoveRightUp"; dirRes = 2; break;
    case 3: res = "MoveLeft"; dirRes = 3; break;
    case 4: res = "MoveRight"; dirRes = 4; break;
    case 5: res = "MoveLeftDown"; dirRes = 5; break;
    case 6: res = "MoveDown"; dirRes = 6; break;
    case 7: res = "MoveRightDown"; dirRes = 7; break;
    case 8: res = "NoMovement"; dirRes = 8; break;
    case 9: res = "GoLeftUp"; dirRes = 100; break;
    case 10: res = "GoUp"; dirRes = 101; break;
    case 11: res = "GoRightUp"; dirRes = 102; break;
    case 12: res = "GoLeft"; dirRes = 103; break;
    case 13: res = "GoRight"; dirRes = 104; break;
    case 14: res = "GoLeftDown"; dirRes = 105; break;
    case 15: res = "GoDown"; dirRes = 106; break;
    case 16: res = "GoRightDown"; dirRes = 107; break;
    default: break;
  }
  if (Dir) *Dir = dirRes;
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
// command
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  command::command
//
//==========================================================================
command::command (cchar *name, truth (*LinkedFunction)(character *), cchar *Description,
                  cchar *KeyName, truth UsableInWilderness, truth WizardModeFunction)
  : mName(name)
  , LinkedFunction(LinkedFunction)
  , Description(Description)
  , KeyList()
  , UsableInWilderness(UsableInWilderness)
  , WizardModeFunction(WizardModeFunction)
{
  AddKey(KeyName);
}


//==========================================================================
//
//  command::ClearKeys
//
//==========================================================================
void command::ClearKeys () {
  KeyList.clear();
}


//==========================================================================
//
//  command::AddKey
//
//==========================================================================
void command::AddKey (cchar *KeyName) {
  if (!WizardModeFunction && KeyName && KeyName[0]) {
    int Key = globalwindowhandler::NameToKey(CONST_S(KeyName));
    if (Key > 0) {
      auto it = KeyList.find(Key);
      if (it == KeyList.end()) {
        KeyList.insert(Key);
        #if 0
        ConLogf("DEBUG: bound key '%s' (%s) to command '%s'.",
                KeyName, globalwindowhandler::GetKeyName(Key).CStr(),
                mName.CStr());
        #endif
      }
    } else {
      ConLogf("ERROR: invalid key '%s' for command '%s'!", KeyName, mName.CStr());
    }
  }
}


//==========================================================================
//
//  command::IsKey
//
//==========================================================================
bool command::IsKey (int Key) {
  if (Key > 0) {
    auto it = KeyList.find(Key);
    return (it != KeyList.end());
  } else {
    return false;
  }
}


//==========================================================================
//
//  command::GetKeyName
//
//==========================================================================
festring command::GetKeyName (int idx) const {
  if (idx >= 0 && idx < (int)KeyList.size()) {
    for (auto &&it : KeyList) {
      if (idx == 0) return globalwindowhandler::GetKeyName(it);
      idx -= 1;
    }
  }
  return festring();
}


// ////////////////////////////////////////////////////////////////////////// //
// commandsystem
// ////////////////////////////////////////////////////////////////////////// //

std::vector<command *> commandsystem::mCommands;
std::vector<commandsystem::MoveKey> commandsystem::mMoveKeys;


//==========================================================================
//
//  commandsystem::GetMoveCommandKey
//
//  return 0 on invalid dir; [0..9]
//
//==========================================================================
int commandsystem::GetMoveCommandKey (int Dir) {
  if (Dir >= 0 && Dir <= 9) {
    for (auto &&it : mMoveKeys) {
      if (it.Dir == Dir) return it.Key;
    }
  }
  return 0;
}


//==========================================================================
//
//  commandsystem::MoveKeyToDir
//
//  return -1, or dir; if ctrl is allowed and pressed, return 1000+dir
//
//==========================================================================
int commandsystem::MoveKeyToDir (int Key, bool allowFastExt) {
  if (Key > 0) {
    for (auto &&it : mMoveKeys) {
      if (it.Key == Key) {
        if (allowFastExt || it.Dir < 100) return it.Dir;
      }
    }
  }
  return -1;
}


//==========================================================================
//
//  commandsystem::UnbindDirKeys
//
//==========================================================================
void commandsystem::UnbindDirKeys () {
  mMoveKeys.clear();
}


//==========================================================================
//
//  commandsystem::BindDirKey
//
//==========================================================================
void commandsystem::BindDirKey (int Dir, cchar *KeyName, bool fastExt) {
  if (Dir >= 0 && Dir <= 8) {
    if (KeyName && KeyName[0]) {
      int Key = globalwindowhandler::NameToKey(CONST_S(KeyName));
      if (Key == 0) {
        ConLogf("ERROR: invalid key '%s' for %sdirection #%d!", KeyName,
                (fastExt ? "fast " : ""), Dir);
      } else {
        if (fastExt) Dir += 100;
        bool again;
        do {
          again = false;
          for (size_t f = 0; !again && f != mMoveKeys.size(); f += 1) {
            if (mMoveKeys[f].Key == Key) {
              mMoveKeys.erase(mMoveKeys.begin() + f);
              again = true;
            }
          }
        } while (again);
        mMoveKeys.push_back(MoveKey(Dir, Key));
      }
    }
  } else {
    ConLogf("ERROR: invalid direction #%d", Dir);
  }
}


//==========================================================================
//
//  commandsystem::GetFullConfigFileName
//
//==========================================================================
festring commandsystem::GetFullConfigFileName () {
  festring fname;
  fname << game::GetConfigPath();
  fname << "k8ivan_keys.rc";
  return fname;
}


//==========================================================================
//
//  commandsystem::ConfigureKeys
//
//==========================================================================
void commandsystem::ConfigureKeys () {
  command *cmd;
  static char buf[512];
  festring fname = GetFullConfigFileName();
  FILE *fl = fopen(fname.CStr(), "r");
  if (!fl) return;
  bool versionOk = false;
  while (!versionOk && fgets(buf, 510, fl)) {
    festring cmdstr = CONST_S(buf);
    cmdstr.TrimAll();
    versionOk = cmdstr.EquCI(KEYBIND_FILE_VERSION);
  }
  if (versionOk) {
    while (fgets(buf, 510, fl)) {
      festring cmdstr = CONST_S(buf);
      cmdstr.TrimAll();
      if (cmdstr.IsEmpty() || cmdstr[0] == '#') continue;
      std::vector<festring> args;
      while (!cmdstr.IsEmpty()) {
        festring::sizetype pos = 0;
        while (pos != cmdstr.GetSize() && (unsigned char)(cmdstr[pos]) > 32) {
          pos += 1;
        }
        if (pos > 0) {
          args.push_back(cmdstr.LeftCopy((int)pos));
          cmdstr.Erase(0, pos);
        } else {
          cmdstr.TrimLeft();
        }
      }
      if (args.empty()) continue;
      if (args[0].StartsWithCI("version")) {
        // do nothing
      } else if (args[0].EquCI("unbind_all")) {
        for (int f = 0; (cmd = commandsystem::GetCommand(f)) != 0; f += 1) {
          cmd->ClearKeys();
        }
        UnbindDirKeys();
      } else if (args[0].EquCI("unbind_all_commands")) {
        for (int f = 0; (cmd = commandsystem::GetCommand(f)) != 0; f += 1) {
          cmd->ClearKeys();
        }
      } else if (args[0].EquCI("unbind_all_dirs")) {
        UnbindDirKeys();
      } else if (args[0].EquCI("bind") && args.size() == 3) {
        int key = globalwindowhandler::NameToKey(args[1]);
        if (key == 0) {
          ConLogf("ERROR: unknown key name: '%s'!", args[1].CStr());
        } else {
          bool found = false;
          for (int f = 0; !found && (cmd = commandsystem::GetCommand(f)) != 0; f += 1) {
            if (cmd->GetName().EquCI(args[2])) {
              //ConLogf("DEBUG: bound key '%s' to command '%s'.", args[1].CStr(), args[2].CStr());
              cmd->AddKey(args[1].CStr());
              found = true;
            }
          }
          if (!found) {
            // try movement
            int didx = 0;
            while (!found) {
              int dir;
              cchar *dcmd = GetDirCommandName(didx, &dir);
              if (!dcmd) break;
              found = args[2].EquCI(dcmd);
              if (!found) didx += 1;
            }
            if (found) {
              int dir;
              cchar *dcmd = GetDirCommandName(didx, &dir);
              IvanAssert(dcmd);
              bool fastExt = false;
              if (dir >= 100) { dir -= 100; fastExt = true; }
              BindDirKey(dir, args[1].CStr(), fastExt);
            } else {
              ConLogf("ERROR: unknown command '%s'!", args[2].CStr());
            }
          }
        }
      } else {
        ConLogf("ERROR: unknown or invalid keybind command: '%s'!", args[0].CStr());
      }
    }
  } else {
    ConLogf("WARNING: ignored keybinding file due to invalid version.");
  }
  fclose(fl);
}


//==========================================================================
//
//  CheckKeyFileVersion
//
//==========================================================================
static bool CheckKeyFileVersion (cfestring &fname) {
  static char buf[512];
  FILE *fl = fopen(fname.CStr(), "r");
  if (!fl) return false;
  while (fgets(buf, 510, fl)) {
    festring cmdstr = CONST_S(buf);
    cmdstr.TrimAll();
    if (cmdstr.EquCI(KEYBIND_FILE_VERSION)) {
      fclose(fl);
      return true;
    }
  }
  fclose(fl);
  return false;
}


//==========================================================================
//
//  commandsystem::SaveKeys
//
//==========================================================================
void commandsystem::SaveKeys (truth forced) {
  command *cmd;
  //int isWizard = 0;
  festring fname = GetFullConfigFileName();
  if (!forced) {
    //if (inputfile::fileExists(fname)) return;
    if (CheckKeyFileVersion(fname)) return;
  }
  FILE *fl = fopen(fname.CStr(), "w");
  if (!fl) return;
  fprintf(fl, "%s\n", KEYBIND_FILE_VERSION);
  fputs("# keyboard bindings\n", fl);
  fputs("unbind_all\n", fl);
  fputs("# there are also `unbind_all_commands` and `unbind_all_dirs`\n", fl);
  fputs("\n# commands\n", fl);
  //for (; isWizard < 2; isWizard++)
  {
    //fputs(isWizard ? "\n# wizard actions\n" : "# actions\n", fl);
    //fputs("# keyboard bindings\n", fl);
    for (int f = 0; (cmd = commandsystem::GetCommand(f)) != 0; f += 1) {
      /*
      if (cmd->IsWizardModeFunction()) {
        if (!isWizard) continue;
      } else {
        if (isWizard) continue;
      }
      */
      if (!cmd->IsWizardModeFunction()) {
        cchar *dsc = cmd->GetName().CStr();
        if (!dsc || !dsc[0]) continue;
        for (int idx = 0;; idx += 1) {
          festring kname = cmd->GetKeyName(idx);
          if (kname.IsEmpty()) break;
          fprintf(fl, "bind %s %s\n", kname.CStr(), dsc);
        }
      }
    }
  }

  fputs("\n# movement\n", fl);
  int didx = 0;
  for (;;) {
    int dir;
    cchar *dcmd = GetDirCommandName(didx, &dir);
    if (!dcmd) break;
    didx += 1;
    for (auto &&it : mMoveKeys) {
      if (it.Dir == dir) {
        festring kname = globalwindowhandler::GetKeyName(it.Key);
        if (!kname.IsEmpty()) {
          fprintf(fl, "bind %s %s\n", kname.CStr(), dcmd);
        } else {
          ConLogf("ERROR: command '%s' has invalid key (0x%08x). WTF?!", dcmd,
                  (unsigned)it.Key);
        }
      }
    }
  }

  fclose(fl);
}


//==========================================================================
//
//  commandsystem::GetMoveKeyDescr
//
//==========================================================================
festring commandsystem::GetMoveKeyDescr (int aDir, bool fast) {
  if (aDir >= 0 && aDir <= 8) {
    const int ndir = aDir + (fast ? 100 : 0);
    switch (ndir) {
      case 0: return CONST_S("move left and up");
      case 1: return CONST_S("move up");
      case 2: return CONST_S("move right and up");
      case 3: return CONST_S("move left");
      case 4: return CONST_S("move right");
      case 5: return CONST_S("move left and down");
      case 6: return CONST_S("move down");
      case 7: return CONST_S("move right and down");
      case 8: return CONST_S("no movement (used in lome location queries)");
      case 100: return CONST_S("go left and up");
      case 101: return CONST_S("go up");
      case 102: return CONST_S("go right and up");
      case 103: return CONST_S("go left");
      case 104: return CONST_S("go right");
      case 105: return CONST_S("go left and down");
      case 106: return CONST_S("go down");
      case 107: return CONST_S("go right and down");
      default: break;
    }
  }
  return festring();
}


//==========================================================================
//
//  commandsystem::GetMoveKeyName
//
//==========================================================================
festring commandsystem::GetMoveKeyName (int aDir, int idx, bool fast) {
  if (aDir >= 0 && aDir <= 8 && idx >= 0) {
    const int ndir = aDir + (fast ? 100 : 0);
    for (size_t f = 0; f != mMoveKeys.size(); f += 1) {
      if (mMoveKeys[f].Dir == ndir) {
        if (idx == 0) return globalwindowhandler::GetKeyName(mMoveKeys[f].Key);
        idx -= 1;
      }
    }
  }
  return festring();
}


//==========================================================================
//
//  commandsystem::SortCommands
//
//==========================================================================
void commandsystem::SortCommands () {
  std::sort(mCommands.begin(), mCommands.end(),
    [&] (const command *a, const command *b) {
      //return (a->GetName().CompareCI(b->GetName()) < 0);
      festring s = CONST_S(a->GetDescription());
      return (s.CompareCI(b->GetDescription()) < 0);
    });
}


//==========================================================================
//
//  commandsystem::RegisterCommand
//
//==========================================================================
void commandsystem::RegisterCommand (command *cmd) {
  if (cmd->IsWizardModeFunction()) {
    cmd->ClearKeys();
  }
  for (size_t f = 0; f < mCommands.size(); ++f) {
    if (mCommands[f]->GetName().EquCI(cmd->GetName())) {
      // replace command
      delete mCommands[f];
      mCommands[f] = cmd;
      SortCommands();
      return;
    }
  }
  mCommands.push_back(cmd);
  SortCommands();
}


//==========================================================================
//
//  commandsystem::FindCommand
//
//==========================================================================
command *commandsystem::FindCommand (cchar *name) {
  if (name && name[0]) {
    for (size_t f = 0; f < mCommands.size(); ++f) {
      if (mCommands[f]->GetName() == name) {
        return mCommands[f];
      }
    }
  }
  return 0;
}


//==========================================================================
//
//  commandsystem::FindCommandByKey
//
//==========================================================================
command *commandsystem::FindCommandByKey (int Key) {
  command *cmd = nullptr;
  if (Key > 0) {
    for (size_t f = 0; !cmd && f != mCommands.size(); f += 1) {
      if (mCommands[f]->IsKey(Key)) {
        cmd = mCommands[f];
      }
    }
  }
  return cmd;
}


//==========================================================================
//
//  commandsystem::AddCommandKey
//
//==========================================================================
void commandsystem::AddCommandKey (cchar *name, cchar *KeyName) {
  if (KeyName && KeyName[0]) {
    command *cmd = FindCommand(name);
    if (cmd) {
      cmd->AddKey(KeyName);
    } else {
      ConLogf("ERROR: invalid key '%s' for unknown command '%s'!", KeyName, name);
    }
  }
}


//==========================================================================
//
//  commandsystem::GetHelpKeyNameFor
//
//==========================================================================
festring commandsystem::GetHelpKeyNameFor (cchar *cmdname) {
  festring kname;
  command *cmd = commandsystem::FindCommand(cmdname);
  if (cmd) {
    for (int idx = 0;; idx += 1) {
      festring kn = cmd->GetKeyName(idx);
      if (kn.IsEmpty()) break;
      if (kname.IsEmpty() || kname.Length() < kn.Length()) {
        kname = kn;
      }
    }
  }
  return kname;
}


// ////////////////////////////////////////////////////////////////////////// //
// common code
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  Consume
//
//==========================================================================
static truth Consume (character *Char, cchar *ConsumeVerb, sorter Sorter, truth nibbling=false) {
  lsquare *Square = Char->GetLSquareUnder();
  stack *Inventory = Char->GetStack();
  stack *StackUnder = Square->GetStack();
  if ((game::IsInWilderness() || !StackUnder->SortedItems(Char, Sorter)) &&
      !Inventory->SortedItems(Char, Sorter))
  {
    ADD_MESSAGE("You have nothing to %s!", ConsumeVerb);
    return false;
  }
  itemvector Item;
  festring Question = CONST_S("What do you wish to ")+ConsumeVerb+'?';
  if (!game::IsInWilderness() && StackUnder->SortedItems(Char, Sorter)) {
    Inventory->DrawContents(Item, StackUnder, Char, Question,
                            CONST_S("Items in your inventory"),
                            CONST_S("Items on the ground"),
                            CONST_S(""), 0, NO_MULTI_SELECT|SELECT_ZEROPICK_FIRST, Sorter);
  } else {
    Inventory->DrawContents(Item, Char, Question, NO_MULTI_SELECT, Sorter);
  }
  if (strcmp(ConsumeVerb, "taste") == 0) ConsumeVerb = "tast";
  return (!Item.empty() ? Char->ConsumeItem(Item[0], CONST_S(ConsumeVerb) + "ing", nibbling) : false);
}


////////////////////////////////////////////////////////////////////////////////
#include "commands/0list.cpp"


//==========================================================================
//
//  commandsystem::Init
//
//  first bool: usable in wilderness (def: false)
//  second bool: WM command (def: false)
//
//==========================================================================
void commandsystem::Init () {
  if (mCommands.empty()) {
    RegisterCommand(new command("Apply", &Apply, "apply", "a"));
    RegisterCommand(new command("Talk", &Talk, "chat", "S-C"));
    RegisterCommand(new command("CanMeat", &CanMeat, "can lump of flesh", "S-N"));
    RegisterCommand(new command("Close", &Close, "close", "c"));
    RegisterCommand(new command("Dip", &Dip, "dip", "S-1")); //FIXME: should be usable in wilderness
    RegisterCommand(new command("Dump", &Dump, "dump potion/can contents", "S-U"));
    RegisterCommand(new command("Drink", &Drink, "drink", "S-D", true));
    RegisterCommand(new command("Drop", &Drop, "drop", "d", true));
    RegisterCommand(new command("Eat", &Eat, "eat", "e", true));
    RegisterCommand(new command("WhatToEngrave", &WhatToEngrave, "engrave", "S-G"));
    RegisterCommand(new command("EquipmentScreen", &EquipmentScreen, "equipment menu", "S-E", true));
    RegisterCommand(new command("Go", &Go, "go", "g", true));
    RegisterCommand(new command("GoStairsDown", &GoDown, "go down/enter area", "S-.", true));
    RegisterCommand(new command("GoStairsUp", &GoUp, "go up", "S-,", true));
    RegisterCommand(new command("IssueCommand", &IssueCommand, "issue command(s) to team member(s)", "S-I"));
    RegisterCommand(new command("Kick", &Kick, "kick", "k"));
    RegisterCommand(new command("Disarm", &Untrap, "disarm trap or mine", "S-K"));
    RegisterCommand(new command("Look", &Look, "look", "l", true));
    RegisterCommand(new command("LookSquare", &LookSquare, "redescribe map position", "S-L", true));
    RegisterCommand(new command("MiniMapNormal", &MapNormal, "show minimap /2", "m", true));
    RegisterCommand(new command("MiniMapSmall", &MapSmall, "show minimap /4", "M-M", true));
    RegisterCommand(new command("AssignName", &AssignName, "name", "n"));
    RegisterCommand(new command("Offer", &Offer, "offer", "S-O"));
    RegisterCommand(new command("Open", &Open, "open", "o", true));
    RegisterCommand(new command("PickUp", &PickUp, "pick up", ","));
    RegisterCommand(new command("Pray", &Pray, "pray", "S-P")); //FIXME: should be usable in wilderness?
    RegisterCommand(new command("Quit", &Quit, "quit", "S-Q", true));
    RegisterCommand(new command("Read", &Read, "read", "r")); //FIXME: should be usable in wilderness?
    RegisterCommand(new command("Rest", &Rest, "rest/heal", "h", true));
    RegisterCommand(new command("Save", &Save, "save game", "S-S", true));
    RegisterCommand(new command("Taste", &Taste, "taste", "S-T", true));
    RegisterCommand(new command("ScrollMessagesDown", &ScrollMessagesDown, "scroll messages down", "M-Down", true));
    RegisterCommand(new command("ScrollMessagesUp", &ScrollMessagesUp, "scroll messages up", "M-Up", true));
    // saving/loading is quite fast. there is no reason to have in-game options.
    // not that i don't want to have them, but some options might not even work
    // without restarting. so why bother?
    //RegisterCommand(new command("ShowConfigScreen", &ShowConfigScreen, "show config screen", "M-C", true));
    RegisterCommand(new command("ShowInventory", &ShowInventory, "show inventory", "i", true));
    RegisterCommand(new command("ShowKeyLayout", &ShowKeyLayout, "show key layout", "F1", true));
    RegisterCommand(new command("DrawMessageHistory", &DrawMessageHistory, "show message history", "M-H", true));
    RegisterCommand(new command("ShowWeaponSkills", &ShowWeaponSkills, "show weapon skills", "S-2", true));
    RegisterCommand(new command("Search", &Search, "search", "s"));
    RegisterCommand(new command("Sit", &Sit, "sit", "_"));
    RegisterCommand(new command("Throw", &Throw, "throw", "t"));
    RegisterCommand(new command("ToggleRunning", &ToggleRunning, "toggle running", "u", true));
    RegisterCommand(new command("ForceVomit", &ForceVomit, "vomit", "S-V"));
    RegisterCommand(new command("NOP", &NOP, "wait", ".", true));
    RegisterCommand(new command("WieldInRightArm", &WieldInRightArm, "wield in right arm", "w", true));
    RegisterCommand(new command("WieldInLeftArm", &WieldInLeftArm, "wield in left arm", "S-W", true));
    RegisterCommand(new command("Burn", &Burn, "burn", "B"));
    RegisterCommand(new command("Zap", &Zap, "zap", "z"));

    RegisterCommand(new command("SaveScreenshot", &Screenshot, "save screenshot", "PrtScr", true));

    // wizard commands need to be registered, but accessible only from the console
    RegisterCommand(new command("WizardMode", &WizardMode, "wizard mode activation", 0/*"X"*/, true));
    /* Sort according to key */
    RegisterCommand(new command("RaiseStats", &RaiseStats, "raise stats", "1", true, true));
    RegisterCommand(new command("LowerStats", &LowerStats, "lower stats", "2", true, true));
    RegisterCommand(new command("SeeWholeMap", &SeeWholeMap, "see whole map", "3", true, true));
    RegisterCommand(new command("WalkThroughWalls", &WalkThroughWalls, "toggle walk through walls mode", "4", true, true));
    RegisterCommand(new command("RaiseGodRelations", &RaiseGodRelations, "raise your relations to the gods", "5", true, true));
    RegisterCommand(new command("LowerGodRelations", &LowerGodRelations, "lower your relations to the gods", "6", true, true));
    RegisterCommand(new command("WizardWish", &WizardWish, "wish something", "~", true, true));
    RegisterCommand(new command("GainDivineKnowledge", &GainDivineKnowledge, "gain knowledge of all gods", "\"", true, true));
    RegisterCommand(new command("GainAllItems", &GainAllItems, "gain all items", "$", true, true));
    RegisterCommand(new command("SecretKnowledge", &SecretKnowledge, "reveal secret knowledge", "*", true, true));
    RegisterCommand(new command("DetachBodyPart", &DetachBodyPart, "detach a limb", "0", true, true));
    RegisterCommand(new command("SummonMonster", &SummonMonster, "summon monster", "&", false, true));
    RegisterCommand(new command("LevelTeleport", &LevelTeleport, "level teleport", "|", false, true));
    RegisterCommand(new command("Possess", &Possess, "possess creature", "{", false, true));
    RegisterCommand(new command("Polymorph", &Polymorph, "polymorph", "[", true, true));
    RegisterCommand(new command("GetScroll", &GetScroll, "get scroll", "R", true, true));
    RegisterCommand(new command("ShowCoords", &ShowCoords, "show current coordinates", "(", true, true));
    RegisterCommand(new command("WizardHeal", &WizardHeal, "wizard healing", "H", true, true));
    RegisterCommand(new command("WizardBlow", &WizardBlow, "wizard blowing", "%", false, true));
    RegisterCommand(new command("WizardTeam", &WizardTeam, "wizard teaming", "/", false, true));
    RegisterCommand(new command("WizardPOI", &WizardPOI, "teleport to POI", ")", true, true));
    /* Numpad aliases for most commonly used commands */
    //AddCommandKey("ShowKeyLayout", KEY_F1);
    AddCommandKey("Eat", "Delete");
    AddCommandKey("PickUp", "Insert");
    AddCommandKey("EquipmentScreen", "Numpad+");
    AddCommandKey("MiniMapNormal", "Tab");
    AddCommandKey("MiniMapSmall", "S-Tab");
    AddCommandKey("NOP", "Num5");

    AddCommandKey("GoStairsDown", "S-Down");
    AddCommandKey("GoStairsUp", "S-Up");

    UnbindDirKeys();
    BindDirKey(0, "Home", false);
    BindDirKey(1, "Up", false);
    BindDirKey(2, "PageUp", false);
    BindDirKey(3, "Left", false);
    BindDirKey(4, "Right", false);
    BindDirKey(5, "End", false);
    BindDirKey(6, "Down", false);
    BindDirKey(7, "PageDown", false);
    BindDirKey(8, "Num5", false);
    BindDirKey(8, ".", false);

    BindDirKey(0, "C-Home", true);
    BindDirKey(1, "C-Up", true);
    BindDirKey(2, "C-PageUp", true);
    BindDirKey(3, "C-Left", true);
    BindDirKey(4, "C-Right", true);
    BindDirKey(5, "C-End", true);
    BindDirKey(6, "C-Down", true);
    BindDirKey(7, "C-PageDown", true);
  }
}
