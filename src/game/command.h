/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __COMMAND_H__
#define __COMMAND_H__

#include <vector>

#include "ivandef.h"


typedef truth (item::*sorter) (ccharacter *) const;

class command {
public:
  command (cchar *name, truth (*LinkedFunction)(character *), cchar *Description,
    cchar *KeyName, truth UsableInWilderness=false, truth WizardModeFunction=false);

  inline truth (*GetLinkedFunction() const) (character *) { return LinkedFunction; }
  inline cchar *GetDescription () const { return Description; }
  inline const festring &GetName () const { return mName; }

  inline truth IsUsableInWilderness () const { return UsableInWilderness; }
  inline truth IsWizardModeFunction () const { return WizardModeFunction; }

  void ClearKeys ();
  void AddKey (cchar *KeyName);
  bool IsKey (int Key);
  festring GetKeyName (int idx) const;

private:
  festring mName;
  truth (*LinkedFunction) (character *);
  cchar *Description;
  std::unordered_set<int> KeyList;
  truth UsableInWilderness;
  truth WizardModeFunction;
};


class commandsystem {
public:
  commandsystem ();

  static void Init ();

  static void ConfigureKeys ();
  static void SaveKeys (truth forced=false);

  static command *GetCommand (int idx) { return (idx >= 0 && idx < (int)mCommands.size()) ? mCommands[idx] : 0; }

  static truth Consume (character*, cchar*, sorter);

  static void RegisterCommand (command *);
  static command *FindCommand (cchar *name);

  static void AddCommandKey (cchar *name, cchar *KeyName);

  // return 0 on invalid dir; [0..9]
  static int GetMoveCommandKey (int Dir);
  // return -1, or dir; if ctrl is allowed and pressed, return 1000+dir
  static int MoveKeyToDir (int Key, bool allowFastExt=false);

  static void UnbindDirKeys ();
  static void BindDirKey (int Dir, cchar *KeyName, bool fastExt);
  static festring GetSelfDirKey ();

  static command *FindCommandByKey (int Key);

  static festring GetFullConfigFileName ();

  static festring GetMoveKeyName (int aDir, int idx, bool fast);
  static festring GetMoveKeyDescr (int aDir, bool fast);

  static festring GetHelpKeyNameFor (cchar *cmdname);

protected:
  static void SortCommands ();

protected:
  struct MoveKey {
    int Dir;
    int Key;

    inline MoveKey () : Dir(-1), Key(0) {}
    inline MoveKey (int aDir, int aKey) : Dir(aDir), Key(aKey) {}
  };

protected:
  static std::vector<command *> mCommands;
  static std::vector<MoveKey> mMoveKeys;
};


#endif
