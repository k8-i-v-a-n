COMMAND(Burn) {
  item *Item = 0;
  if (!Char->PossessesItem(&item::IsFlaming)) {
    ADD_MESSAGE("You have no flaming items, %s.", game::Insult());
    return false;
  }

  if (Char->MoreThanOnePossessesItem(&item::IsFlaming)) {
    Item = Char->SelectFromPossessions(CONST_S("What do you want to use?"), &item::IsFlaming);
  } else {
    Item = Char->FirstPossessesItem(&item::IsFlaming);
  }

  if (Item) {
    int Answer = game::DirectionQuestion(CONST_S("In what direction do you wish to burn?"),
                                         false/*RequireAnswer*/, false/*AcceptYourself*/);
    if (Answer == DIR_ERROR) return false;
    if (Item->Burn(Char, Char->GetPos(), Answer)) {
      festring itname;
      if (Char->IsPlayer()) {
        Item->AddName(itname, UNARTICLED);
      } else {
        Item->AddName(itname, INDEFINITE);
      }
      //FIXME: AP, and other actions... balance them!
      Char->EditExperience(DEXTERITY, 75, 1 << 8);
      Char->EditNP(-10);
      Char->DexterityAction(5);
      Char->EditAP(-100000 / APBonus(Char->GetAttribute(DEXTERITY)));
      ADD_MESSAGE("You managed to burn the web with your %s.", itname.CStr());
      return true;
    }
    return false;
  }

  return false;
};
