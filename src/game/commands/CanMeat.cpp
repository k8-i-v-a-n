COMMAND(CanMeat) {
  if (!Char->GetStack()->SortedItems(Char, &item::IsMeatCannable) &&
      !Char->EquipsSomething(&item::IsMeatCannable))
  {
    ADD_MESSAGE("You have nothing to can the food into!");
    return false;
  }

  itemvector Cannable;
  itemvector InventoryCannable;

  // check inventory
  {
    sortdata SortData2(InventoryCannable, Char, false, &item::IsCanBeCanned);
    Char->GetStack()->SortAllItems(SortData2);
  }

  stack *Stack = 0;
  {
    lsquare *Square = Char->GetLSquareUnder();
    Stack = (Square ? Square->GetStack() : nullptr);
    if (Stack) {
      sortdata SortData(Cannable, Char, false, &item::IsCanBeCanned);
      Stack->SortAllItems(SortData);
    }
  }

  item *Flesh = 0;

  if (InventoryCannable.empty() && Cannable.empty()) {
    ADD_MESSAGE("There is nothing you can pack into can.");
    return false;
  }

  item *Item;
  if (Char->MoreThanOnePossessesItem(&item::IsFlaming)) {
    Item = Char->SelectFromPossessions(CONST_S("What do you want to use as a can?"),
                                       &item::IsMeatCannable);
  } else {
    Item = Char->FirstPossessesItem(&item::IsMeatCannable);
  }

  if (!Item || !Item->IsMeatCannable(Char)) {
    return false;
  }

  if (!InventoryCannable.empty() && !Cannable.empty()) {
    if (game::TruthQuestion(CONST_S("Do you want to can something from your inventory?"))) {
      InventoryCannable.clear();
      if (!Char->SelectFromPossessions(InventoryCannable, CONST_S("What do you want to can?"),
                                       NO_MULTI_SELECT | STACK_ALLOW_NAMING /*| NONE_AS_CHOICE*/,
                                       &item::IsCanBeCanned))
      {
        return false;
      }
      if (InventoryCannable.empty()) return false;
      Flesh = InventoryCannable[0];
    } else {
      IvanAssert(Stack);
      Flesh = Stack->DrawContents(Char, CONST_S("What do you want to can?"),
                                  NO_MULTI_SELECT /*| NONE_AS_CHOICE*/, &item::IsCanBeCanned);
    }
  } else if (!Cannable.empty()) {
    IvanAssert(Stack);
    Flesh = Stack->DrawContents(Char, CONST_S("What do you want to can?"),
                                NO_MULTI_SELECT /*| NONE_AS_CHOICE*/, &item::IsCanBeCanned);
  } else {
    IvanAssert(!InventoryCannable.empty());
    Flesh = Char->SelectFromPossessions(CONST_S("What do you want to can?"), &item::IsCanBeCanned);
  }
  if (!Flesh) return false;

  Item->CanItem(Char, Flesh);

  return false;
};
