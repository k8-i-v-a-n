COMMAND(Dip) {
  if (!Char->GetStack()->SortedItems(Char, &item::IsDippable) &&
      !Char->EquipsSomething(&item::IsDippable))
  {
    ADD_MESSAGE("You have nothing to dip!");
    return false;
  }

  truth HasDipDestination = Char->PossessesItem(&item::IsDipDestination);
  int DipDestinationNear = 0;
  lsquare *DipSquare = 0;
  if (!game::IsInWilderness()) {
    for (int d = 0; d < 9; ++d) {
      lsquare *Square = Char->GetNaturalNeighbourLSquare(d);
      if (Square && Square->IsDipDestination()) {
        DipDestinationNear += 1;
        if (!DipSquare) DipSquare = Square;
      }
    }
  }

  if (!HasDipDestination && !DipDestinationNear) {
    ADD_MESSAGE("There is nothing to dip anything into.");
    return false;
  }

  item *Item = Char->SelectFromPossessions(CONST_S("What do you want to dip?"), &item::IsDippable);
  if (!Item) return false;

  if (!HasDipDestination && game::TruthQuestion(CONST_S("Do you wish to dip in a nearby square?"))) {
    IvanAssert(DipDestinationNear);
    if (DipDestinationNear != 1) {
      int Dir = game::DirectionQuestion(CONST_S("Where do you want to dip ") + Item->GetName(DEFINITE) + "?",
                                        false/*RequireAnswer*/, true/*AcceptYourself*/);
      v2 Pos = Char->GetPos() + game::GetMoveVector(Dir);
      if (Dir == DIR_ERROR || !Char->GetArea()->IsValidPos(Pos) ||
          !Char->GetNearLSquare(Pos)->IsDipDestination())
      {
        return false;
      }
      DipSquare = Char->GetNearLSquare(Pos);
    }
    IvanAssert(DipSquare);
    return DipSquare->DipInto(Item, Char);
  }

  item *DipTo = Char->SelectFromPossessions(CONST_S("Into what do you wish to dip it?"),
                                            &item::IsDipDestination);
  if (!DipTo) return false;

  if (Item == DipTo) {
    ADD_MESSAGE("Very funny...");
    return false;
  }
  //ConLogf("BSM: %p", DipTo->GetSecondaryMaterial());
  Item->DipInto(DipTo->CreateDipLiquid(), Char);
  //ConLogf("ASM: %p", DipTo->GetSecondaryMaterial());
  if (!game::IsInWilderness() && DipTo->Exists() && game::CheckDropLeftover(DipTo)) {
    // drop this item
    if (!Char->GetRoom() || Char->GetRoom()->DropItem(Char, DipTo, 1)) {
      ADD_MESSAGE("%s dropped.", DipTo->GetName(INDEFINITE, 1).CStr());
      DipTo->MoveTo(Char->GetStackUnder());
      Char->DexterityAction(2);
    }
  }

  return true;
};
