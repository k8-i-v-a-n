COMMAND(Dump) {
  if (!Char->GetStack()->SortedItems(Char, &item::IsDumpable) &&
      !Char->EquipsSomething(&item::IsDumpable))
  {
    ADD_MESSAGE("You have nothing to dump!");
    return false;
  }

  item *Item = Char->SelectFromPossessions(CONST_S("What do you want to dump?"), &item::IsDumpable);
  if (Item) {
    int Dir = game::DirectionQuestion(CONST_S("In what direction do you wish to dump it?"),
                                      false/*RequireAnswer*/, true/*AcceptYourself*/);
    if (Dir == DIR_ERROR) return false;
    v2 Pos = Char->GetPos() + game::GetMoveVector(Dir);
    int Volume = -1;
    if (Item->GetSecondaryMaterial()->IsLiquid()) {
      int ivol = Item->CurrSecondaryVolume();
      if (ivol > 250) {
        felist List(CONST_S("How much liquid do you want do dump?"));
        List.RemoveFlags(FADE);
        int num = ivol / 250;
        for (int f = 0; f != num; f += 1) {
          festring ee;
          ee << "\1W" << (f + 1) * 250 << "\2\x12ml";
          List.AddEntry(ee, LIGHT_GRAY);
        }
        List.AddEntry(CONST_S("whole"), LIGHT_GRAY);
        game::SetStandardListAttributes(List);
        List.AddFlags(SELECTABLE | FELIST_NO_BADKEY_EXIT);
        int Selected = List.Draw();
        if (Selected == ESCAPED) return false;
        Char->GetArea()->SendNewDrawRequest();
        if (Selected == num) {
          Volume = -1;
        } else {
          Volume = (Selected + 1) * 250;
          const int dex = Char->GetAttribute(DEXTERITY);
          if (dex < 20) {
            const int chg = RAND_N((21 - dex) * 2);
            if (!RAND_2) Volume -= chg; else Volume += chg;
          }
        }
      }
    }
    return Item->DumpTo(Char, Pos, Volume);
  }

  return false;
};
