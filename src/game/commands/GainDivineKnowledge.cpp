COMMAND(GainDivineKnowledge) {
  for (int c = 1; c <= GODS; ++c) {
    game::LearnAbout(game::GetGod(c));
  }
  ConLogf("gained knowledge of all gods.");
  return false;
};
