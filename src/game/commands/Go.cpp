COMMAND(Go) {
  if (Char->IsPlayer() && game::IsInWilderness()) {
    std::vector<owterrain *> list;
    worldmap *wmap = game::GetWorldMap();
    if (!wmap) return false; // just in case
    wmap->GetListOfKnownPOIs(list);
    if (list.empty()) {
      ADD_MESSAGE("You have nowhere to go.");
      return false;
    } else {
      felist List(CONST_S("Where do you want to go?"));
      List.RemoveFlags(FADE);
      for (size_t f = 0; f != list.size(); f += 1) {
        List.AddEntry(list[f]->GetNameSingular(), LIGHT_GRAY);
      }
      game::SetStandardListAttributes(List);
      List.AddFlags(SELECTABLE | FELIST_NO_BADKEY_EXIT);
      int Selected = List.Draw();
      if (Selected == ESCAPED) return false;
      Char->GetArea()->SendNewDrawRequest();
      autotravel *Go = autotravel::Spawn(Char);
      Go->SetDestination(list[Selected]);
      Char->SetAction(Go);
      Char->EditAP(Char->GetStateAPGain(100)); // gum solution
      Char->AutoTravel(Go, true);
      return !!Char->GetAction();
    }
  } else {
    int Dir = game::DirectionQuestion(CONST_S("In what direction do you want to go?"),
                                      false/*RequireAnswer*/, false/*AcceptYourself*/);
    if (Dir == DIR_ERROR) return false;
    go *Go = go::Spawn(Char);
    Go->SetDirection(Dir);
    Go->SetPrevWasTurn(false);
    /*
    int OKDirectionsCounter = 0;
    for (int d = 0; d < Char->GetNeighbourSquares(); ++d) {
      lsquare *Square = Char->GetNeighbourLSquare(d);
      if (Square && Char->CanMoveOn(Square)) ++OKDirectionsCounter;
    }
    Go->SetIsWalkingInOpen(OKDirectionsCounter > 2);
    */
    Char->SetAction(Go);
    Char->EditAP(Char->GetStateAPGain(100)); // gum solution
    Char->GoOn(Go, true);
    return !!Char->GetAction();
  }
};
