COMMAND(Look) {
  festring Msg;
  if (!game::IsInWilderness()) {
    Char->GetLevel()->AddSpecialCursors();
  }
  if (!game::IsInWilderness()) {
    Msg = CONST_S("Direction keys move cursor, \1RESC\2 exits, '\1Gi\2' examines items, '\1Gc\2' examines a character.");
  } else {
    Msg = CONST_S("Direction keys move cursor, \1RESC\2 exits, '\1Gc\2' examines a character.");
  }
  game::PositionQuestion(Msg, Char->GetPos(), &game::LookHandler, &game::LookKeyHandler, ivanconfig::GetLookZoom());
  game::RemoveSpecialCursors();
  return false;
};
