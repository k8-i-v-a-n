COMMAND(MapNormal) {
  game::MiniMapProcessor(false);
  return false;
};


COMMAND(MapSmall) {
  game::MiniMapProcessor(true);
  return false;
};
