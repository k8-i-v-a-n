COMMAND(Polymorph) {
  character *NewForm;
  if (!Char->GetNewFormForPolymorphWithControl(NewForm)) return true;
  truth escaped;
  sLong time = game::NumberQuestion(CONST_S("For how long?"), WHITE, false, &escaped);
  if (!escaped) {
    if (time < 1) {
      ADD_MESSAGE("really?");
    } else {
      Char->Polymorph(NewForm, time);
    }
  }
  return true;
};
