COMMAND(Quit) {
  if (game::TruthQuestion(CONST_S("Your quest is not yet compeleted! Really quit?"))) {
    festring Msg = CONST_S("cowardly quit the game");
    if (!game::IsCheating()) {
      Char->AddQuitedScoreEntry(Msg, 0.75);
    }
    Char->ShowAdventureInfo(Msg);
    if (game::WizardModeIsActive() && !game::TruthQuestion(CONST_S("Remove saves?"))) {
      game::Save(true/*vacuum*/);
      game::End(Msg, false, true);
    } else {
      game::End(Msg);
    }
    return true;
  }
  return false;
};
