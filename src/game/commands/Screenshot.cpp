COMMAND(Screenshot) {
  if (!game::inScreenShoter) {
    game::inScreenShoter = true;
    bool doIt = false;
    try {
      doIt = !!game::TruthQuestion(CONST_S("Do you want to save screenshot?"), REQUIRES_ANSWER);
      game::inScreenShoter = false;
    } catch (...) {
      game::inScreenShoter = false;
      throw;
    }
    if (doIt) {
      game::SaveScreenshot();
    }
  }
  return false;
};
