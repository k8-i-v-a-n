COMMAND(ShowInventory) {
  festring Title("Your inventory (total weight: ");
  //Title << Char->GetStack()->GetWeight();
  //Title << "g)";
  Title.PutWeight(Char->GetStack()->GetWeight(), "\1C", "\2");
  Title << ")";
  Char->GetStack()->DrawContents(Char, Title, /*NO_SELECT |*/ STACK_ALLOW_NAMING);
  return false;
};
