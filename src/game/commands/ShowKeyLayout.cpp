COMMAND(ShowKeyLayout) {
  festring Buffer;
  command *cmd;

  felist List(CONST_S("Keyboard Layout"));
  List.AddDescription(CONST_S(""));

  // calculate maximum length of key name
  int xww = 0;
  for (int c = 0; (cmd = commandsystem::GetCommand(c)); ++c) {
    if (!cmd->IsWizardModeFunction() || game::WizardModeIsActive()) {
      for (int idx = 0;; idx += 1) {
        festring kname = cmd->GetKeyName(idx);
        if (kname.IsEmpty()) break;
        xww = Max(xww, FONT->TextWidth(kname));
      }
    }
  }

  // movement keys
  for (int mode = 0; mode < 2; mode += 1) {
    for (int dir = 0; !commandsystem::GetMoveKeyDescr(dir, mode != 0).IsEmpty(); dir += 1) {
      for (int idx = 0;; idx += 1) {
        festring kname = commandsystem::GetMoveKeyName(dir, idx, mode != 0);
        if (kname.IsEmpty()) break;
        xww = Max(xww, FONT->TextWidth(kname));
      }
    }
  }

  // create list
  festring hdr = CONST_S("Key");
  int kkwdt = Max(FONT->TextWidth(hdr) + 6, xww + 6);
  FONT->PadCenter(hdr, kkwdt);
  hdr << "Description";
  List.AddDescription(hdr);

  const char *c1[2] = { "\1#aa0|", "\1#ff0|" };
  const col16 c2[2] = { LIGHT_GRAY, MakeRGB16(192 + 16, 192 + 16, 192 + 16) };
  int cidx = 0;

  for (int c = 0; (cmd = commandsystem::GetCommand(c)); ++c) {
    if (!cmd->IsWizardModeFunction()) {
      if (game::WizardModeIsActive() && cmd->GetName() == "WizardMode") continue;
      for (int idx = 0;; idx += 1) {
        festring kname = cmd->GetKeyName(idx);
        if (kname.IsEmpty()) break;
        Buffer.Empty();
        Buffer << c1[cidx];
        Buffer << kname;
        Buffer << "\2";
        //FONT->PadCenter(Buffer, xww);
        FONT->LPadToPixWidth(Buffer, xww);
        FONT->RPadToPixWidth(Buffer, kkwdt);
        Buffer << cmd->GetDescription();
        List.AddEntry(Buffer, c2[cidx]); cidx ^= 1;
      }
    }
  }

  /*
  if (game::WizardModeIsActive()) {
    List.AddEntry(CONST_S(""), WHITE);
    List.AddEntry(CONST_S("Wizard mode functions:"), WHITE);
    List.AddEntry(CONST_S(""), WHITE);
    for (int c = 0; (cmd = commandsystem::GetCommand(c)); ++c) {
      if (cmd->IsWizardModeFunction()) {
        Buffer.Empty();
        Buffer << cmd->GetKey();
        FONT->PadCenter(Buffer, xww);
        FONT->RPadToPixWidth(Buffer, kkwdt);
        Buffer << cmd->GetDescription();
        List.AddEntry(Buffer, c2[cidx]); cidx ^= 1;
      }
    }
  }
  */

  // movement keys
  List.AddEntry(CONST_S(""), WHITE);
  List.AddEntry(CONST_S("Movement keys:"), WHITE);
  cidx = 0;
  for (int mode = 0; mode < 2; mode += 1) {
    for (int dir = 0; !commandsystem::GetMoveKeyDescr(dir, mode != 0).IsEmpty(); dir += 1) {
      for (int idx = 0;; idx += 1) {
        festring kname = commandsystem::GetMoveKeyName(dir, idx, mode != 0);
        if (kname.IsEmpty()) break;
        Buffer.Empty();
        Buffer << c1[cidx];
        Buffer << kname;
        Buffer << "\2";
        //FONT->PadCenter(Buffer, xww);
        FONT->LPadToPixWidth(Buffer, xww);
        FONT->RPadToPixWidth(Buffer, kkwdt);
        Buffer << commandsystem::GetMoveKeyDescr(dir, mode != 0);
        List.AddEntry(Buffer, c2[cidx]); cidx ^= 1;
      }
    }
  }

  // rc file
  List.AddEntry(CONST_S(""), WHITE);
  List.AddEntry(CONST_S("You can edit the following file to change the layout:"), WHITE);
  List.AddEntry(CONST_S("  ") + commandsystem::GetFullConfigFileName(), YELLOW);

  // additional info
  /*
  List.AddEntry(CONST_S(""), WHITE);
  List.AddEntry(CONST_S("Additional info:"), WHITE);
  List.AddEntry(CONST_S("  Do not forget that you can use diagonal movement!"), YELLOW);
  */

  game::SetStandardListAttributes(List);
  List.Draw();

  return false;
};
