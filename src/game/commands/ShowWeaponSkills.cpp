COMMAND(ShowWeaponSkills) {
  felist List(CONST_S("Your experience in weapon categories"));
  List.AddDescription(CONST_S(""));

  int maxNameWdt = FONT->TextWidth("right accustomization");
  int maxLevelWdt = FONT->CharWidth('0') * 10;
  int maxPointsWdt = FONT->CharWidth('0') * 10;
  int maxNeededWdt = FONT->CharWidth('0') * 10;
  for (int c = 0; c < Char->GetAllowedWeaponSkillCategories(); ++c) {
    cweaponskill *Skill = Char->GetCWeaponSkill(c);
    festring name = CONST_S(Skill->GetName(c));
    //ConLogf("<%s>", name.CStr());
    int wdt = FONT->TextWidth(name);
    if (maxNameWdt < wdt) maxNameWdt = wdt;
  }
  maxNameWdt += 32;

  festring shdr("Category name");
  FONT->RPadToPixWidth(shdr, maxNameWdt);
  festring pad;
  pad.Empty(); pad << "Level"; FONT->RPadToPixWidth(pad, maxLevelWdt); shdr << pad;
  maxLevelWdt = FONT->TextWidth(shdr);
  pad.Empty(); pad << "Points"; FONT->RPadToPixWidth(pad, maxPointsWdt); shdr << pad;
  maxPointsWdt = FONT->TextWidth(shdr);
  pad.Empty(); pad << "Needed"; FONT->RPadToPixWidth(pad, maxNeededWdt); shdr << pad;
  maxNeededWdt = FONT->TextWidth(shdr);
  shdr << "Battle bonus";
  //List.AddDescription(CONST_S("Category name                 Level     Points    Needed    Battle bonus"));
  List.AddDescription(shdr);

  truth Something = false;
  festring Buffer;
  for (int c = 0; c < Char->GetAllowedWeaponSkillCategories(); ++c) {
    cweaponskill *Skill = Char->GetCWeaponSkill(c);
    if (Skill->GetHits()/100 || (Char->IsUsingWeaponOfCategory(c))) {
      Buffer = Skill->GetName(c);
      //Buffer.Resize(30);
      FONT->RPadToPixWidth(Buffer, maxNameWdt);
      Buffer << Skill->GetLevel();
      //Buffer.Resize(40);
      FONT->RPadToPixWidth(Buffer, maxLevelWdt);
      Buffer << Skill->GetHits()/100;
      //Buffer.Resize(50);
      FONT->RPadToPixWidth(Buffer, maxPointsWdt);
      if (Skill->GetLevel() != 20) {
        Buffer << (Skill->GetLevelMap(Skill->GetLevel()+1)-Skill->GetHits())/100;
      } else {
        Buffer << '-';
      }
      //Buffer.Resize(60);
      FONT->RPadToPixWidth(Buffer, maxNeededWdt);
      Buffer << '+' << (Skill->GetBonus()-1000)/10;
      if (Skill->GetBonus() % 10) Buffer << '.' << Skill->GetBonus()%10;
      Buffer << '%';
      //ConLogf("<%s>", Buffer.CStr());
      List.AddEntry(Buffer, Char->IsUsingWeaponOfCategory(c) ? WHITE : LIGHT_GRAY);
      Something = true;
    }
  }

  if (Char->AddSpecialSkillInfo(List, maxNameWdt, maxLevelWdt, maxPointsWdt, maxNeededWdt)) {
    Something = true;
  }

  if (Something) {
    game::SetStandardListAttributes(List);
    List.Draw();
  } else {
    ADD_MESSAGE("You are not experienced in any weapon skill yet!");
  }

  return false;
};
