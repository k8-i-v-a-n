COMMAND(SummonMonster) {
  character *Summoned = 0;
  while (!Summoned) {
    festring Temp = game::DefaultQuestion(CONST_S("Summon which monster?"), game::GetDefaultSummonMonster());
    if (Temp == "none") return false;
    Summoned = protosystem::CreateMonster(Temp);
  }

  bool friendly = false;
  if (game::TruthQuestion(CONST_S("Do you want it to be friendly?"))) {
    friendly = true;
  }

  if (friendly) {
    Summoned->SetTeam(game::GetTeam(PLAYER_TEAM));
  } else {
    Summoned->SetTeam(game::GetTeam(MONSTER_TEAM));
  }

  if (!Summoned->PutNear(Char->GetPos(), true)) {
    //Summoned->Disable();
    //Summoned->SendToHell();
    delete Summoned;
    ADD_MESSAGE("This place is too crowded.");
  }

  return false;
};
