COMMAND(Untrap) {
  if (!Char->CanApply()) {
    ADD_MESSAGE("This monster type cannot apply.");
    return false;
  }

  if (!Char->CheckApply()) return false;

  // accept "." too
  int Dir = game::DirectionQuestion(CONST_S("In what direction the trap is?"),
                                    false/*RequireAnswer*/, true/*AcceptYourself*/);
  if (Dir == DIR_ERROR) return false;
  if (Dir != YOURSELF && !Char->GetArea()->IsValidPos(Char->GetPos()+game::GetMoveVector(Dir))) {
    return false;
  }

  lsquare *LSquare = (Dir == YOURSELF ? Char->GetLSquareUnder() : Char->GetNeighbourLSquare(Dir));
  if (!LSquare) {
    ADD_MESSAGE("There is noting to untrap there.");
    return false;
  }

  //TODO: select the trap if we have several of them?
  item *trap = 0;
  stack *stk = LSquare->GetStack();
  for (stackiterator i = stk->GetBottom(); i.HasItem(); ++i) {
    if (i->IsTrap() && i->NeedDangerSymbol() && i->CanBeSeenBy(Char)) {
      trap = *i;
      break;
    }
  }

  if (!trap) {
    ADD_MESSAGE("There is noting to untrap there.");
    return false;
  }

  return trap->TryToDeactivateTrap(Char);
};
