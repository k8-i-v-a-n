COMMAND(WizardBlow) {
  v2 Input = game::PositionQuestion(CONST_S("What do you with to exploda? [direction keys move cursor, space accepts]"),
                                    PLAYER->GetPos(), &game::TeleportHandler, 0, ivanconfig::GetLookZoom());
  if (Input == ERROR_V2) { return false; } // esc pressed

  if (!PLAYER->GetLevel()->IsValidPos(Input)) return false;

  lsquare *Square = PLAYER->GetNearLSquare(Input);
  //lsquare* Square = GetLSquareUnder();
  if (Square) {
    Square->DrawParticles(RED);
    Square->GetLevel()->Explosion(PLAYER, CONST_S("Killed by WizardBlow"), Square->GetPos(), 800);

    for (int d = 2; d <= 2; d += 1) {
      for (int f = 0; f < 8; ++f) {
        v2 dest = Square->GetPos();
        for (int c = 0; c != d; c += 1) dest = dest + game::GetMoveVector(f);
        if (PLAYER->GetLevel()->IsValidPos(dest)) {
          lsquare *Square2 = PLAYER->GetNearLSquare(dest);
          if (Square2) {
            Square2->DrawParticles(RED);
            Square2->GetLevel()->Explosion(PLAYER, CONST_S("Killed by WizardBlow"), dest, 666);
          }
        }
      }
    }
  }

  return false;
};
