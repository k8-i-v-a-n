COMMAND(WizardMode) {
  if (!game::WizardModeIsActive()) {
    if (game::TruthQuestion(CONST_S("Do you want to cheat, cheater? This action cannot be undone."))) {
      game::ActivateWizardMode();
      ADD_MESSAGE("Wizard mode activated.");
      #if 0
      game::RevealPOI(game::elpuricavePOI());
      //game::RevealPOI(game::underwatertunnelexitPOI());
      #else
      // reveal all POIs
      /*
      for (int f = 0; f < game::poiCount(); ++f) {
        owterrain *terra = game::poiByIndex(f);
        if (!terra->IsGenerated()) continue;
        if (terra->MustBeSkipped) continue;
        game::RevealPOI(terra);
      }
      Char->GetArea()->SendNewDrawRequest();
      */
      #endif
      game::RunGlobalEvent(CONST_S("wizard_mode_activated"));
      //game::ScheduleImmediateSave();
    }
    return false;
  }

  // wizard mode
  if (game::HasGlobalEvent(CONST_S("wizard_mode_deactivate_check"))) {
    if (game::RunGlobalEvent(CONST_S("wizard_mode_deactivate_check"), false)) {
      game::DeactivateWizardMode();
      ADD_MESSAGE("You are the ordinary mortal again.");
      return false;
    }
  }

  if (PLAYER->GetVirtualHead()) {
    ADD_MESSAGE("spillfluid.");
    //PLAYER->GetVirtualHead()->SpillFluid(0, liquid::Spawn(GetBloodMaterial(), 1000));
    PLAYER->GetVirtualHead()->SpillBlood(1000);
  }

  return false;
};
