COMMAND(WizardPOI) {
  if (!Char->IsPlayer() || !game::IsInWilderness()) {
    ADD_MESSAGE("This command is usable only in wilderness.");
    return false;
  }

  felist List(CONST_S("Where do you want to be teleported?"));
  List.RemoveFlags(FADE);

  std::vector<owterrain *> poiList;

  for (int f = 0; f < game::poiCount(); f += 1) {
    owterrain *terra = game::poiByIndex(f);
    if (terra && terra->IsGenerated()) {
      if (terra->IsPlaced()) {
        List.AddEntry(terra->GetNameSingular(), LIGHT_GRAY);
      } else {
        festring nn;
        nn << terra->GetNameSingular() << " (hidden yet)";
        List.AddEntry(terra->GetNameSingular(), ORANGE);
      }
      poiList.push_back(terra);
    }
  }

  game::SetStandardListAttributes(List);
  List.AddFlags(SELECTABLE | FELIST_NO_BADKEY_EXIT);

  int Selected = List.Draw();
  if (Selected == ESCAPED) return false;

  Char->GetArea()->SendNewDrawRequest();

  if (Selected >= 0 && Selected < (int)poiList.size()) {
    owterrain *terra = poiList[Selected];
    IvanAssert(terra);
    if (!terra->IsPlaced()) {
      game::RevealPOI(terra);
      Char->GetArea()->SendNewDrawRequest();
    }
    if (Char->WildernessTeleport(terra->GetPosition())) {
      ADD_MESSAGE("Your wish is my command!");
      return false;
    }
  }

  ADD_MESSAGE("This is the place where no men has been before. Neither will you.");

  return false;
};
