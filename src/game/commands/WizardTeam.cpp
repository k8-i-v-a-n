COMMAND(WizardTeam) {
  int Dir = game::DirectionQuestion(CONST_S("Choose creature to teaming."),
                                    false/*RequireAnswer*/, false/*AcceptYourself*/);
  if (Dir == DIR_ERROR || !Char->GetArea()->IsValidPos(Char->GetPos()+game::GetMoveVector(Dir))) {
    return false;
  }

  character *ToTeam = Char->GetNearLSquare(Char->GetPos()+game::GetMoveVector(Dir))->GetCharacter();
  if (ToTeam) {
    felist List(CONST_S("Where do you want to be teleported?"));
    game::SetStandardListAttributes(List);
    List.RemoveFlags(FADE);
    List.AddFlags(SELECTABLE | FELIST_NO_BADKEY_EXIT);

    const int tcount = game::GetTeams();
    for (int f = 0; f != tcount; f += 1) {
      team *t = game::GetTeam(f);
      if (t) {
        List.AddEntry(t->GetName(), LIGHT_GRAY);
      }
    }

    int sel = List.Draw();
    if (sel != ESCAPED) {
      for (int f = 0; f != tcount; f += 1) {
        team *t = game::GetTeam(f);
        if (t) {
          if (sel == 0) {
            ToTeam->ChangeTeam(t);
            ADD_MESSAGE("Team changed.");
            return false;
          }
          sel -= 1;
        }
      }
    }

    /*
    team *t = 0;
    while (!t) {
      festring Temp = game::DefaultQuestion(CONST_S("Assign to which team?"), game::GetDefaultTeam());
      if (Temp == "none") return false;
      t = game::FindTeam(Temp);
    }
    //ToTeam->ChangeTeam(game::GetTeam(MONSTER_TEAM));
    ToTeam->ChangeTeam(t);
    //ToTeam->SetRelation(ToTeam->GetRelation(PLAYER->GetTeam()))
    ADD_MESSAGE("Team changed.");
    */
  } else {
    ADD_MESSAGE("There's no one to team, %s!", game::Insult());
  }

  return false;
};
