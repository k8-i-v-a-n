/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

/* Compiled through wmapset.cpp */

uChar **continent::TypeBuffer;
short **continent::AltitudeBuffer;
uChar **continent::ContinentBuffer;


//==========================================================================
//
//  continent::continent
//
//==========================================================================
continent::continent () : Index(0), Name("") {
  memset(ttypeCount, 0, sizeof(ttypeCount));
}


//==========================================================================
//
//  continent::continent
//
//==========================================================================
continent::continent (int Index) : Index(Index), Name("") {
  memset(ttypeCount, 0, sizeof(ttypeCount));
}


//==========================================================================
//
//  continent::~continent
//
//==========================================================================
continent::~continent () {}


//==========================================================================
//
//  continent::GetSize
//
//==========================================================================
sLong continent::GetSize () const {
  return Member.size();
}


//==========================================================================
//
//  continent::GetMember
//
//==========================================================================
v2 continent::GetMember (int I) const {
  if (I < 0 || (unsigned)I >= Member.size()) {
    ABORT("continent::GetMember: invalid member requiested (%d)!", I);
  }
  return Member[I];
}


//==========================================================================
//
//  continent::GetGTerrainAmount
//
//==========================================================================
int continent::GetGTerrainAmount (int Type) const {
  if (Type < 0 || Type >= TerrainTypeLimit) {
    ABORT("continent::GetGTerrainAmount: invalid terrain type requiested (%d)!", Type);
  }
  return ttypeCount[Type];
}


//==========================================================================
//
//  continent::Save
//
//==========================================================================
void continent::Save (outputfile &SaveFile) const {
  SaveFile << Name << Member << Index;
}


//==========================================================================
//
//  continent::Load
//
//==========================================================================
void continent::Load (inputfile &SaveFile) {
  SaveFile >> Name >> Member >> Index;
}


//==========================================================================
//
//  continent::AttachTo
//
//==========================================================================
void continent::AttachTo (continent *Continent) {
  for (feuLong c = 0; c < Member.size(); ++c) {
    ContinentBuffer[Member[c].X][Member[c].Y] = Continent->Index;
  }
  if (!Continent->Member.size()) {
    Continent->Member.swap(Member);
  } else {
    Continent->Member.insert(Continent->Member.end(), Member.begin(), Member.end());
    Member.clear();
  }
}


//==========================================================================
//
//  continent::GenerateInfo
//
//==========================================================================
void continent::GenerateInfo () {
  if (TerrainTypeLimit < protocontainer<gwterrain>::GetSize()+1) ABORT("Too many world terrain types!");
  memset(ttypeCount, 0, sizeof(ttypeCount));

  for (feuLong c = 0; c < Member.size(); ++c) {
    auto tt = TypeBuffer[Member[c].X][Member[c].Y];
    if (tt < 0 || tt >= TerrainTypeLimit) ABORT("continent::GenerateInfo: invalid terrain type requiested (%d)!", tt);
    ++ttypeCount[tt];
  }

  Name = CONST_S("number ");
  Name << Index;
}


//==========================================================================
//
//  continent::Add
//
//==========================================================================
void continent::Add (v2 Pos) {
  Member.push_back(Pos);
  ContinentBuffer[Pos.X][Pos.Y] = Index;
}


//==========================================================================
//
//  InArray
//
//==========================================================================
static bool InArray (int value, const fearray<sLong> allowedTypes) {
  bool found = false;
  for (int f = 0; !found && f != allowedTypes.GetSize(); f += 1) {
    if (value == allowedTypes.GetItemAt(f)) {
      found = true;
    }
  }
  return found;
}


//==========================================================================
//
//  IsEmptyArray
//
//==========================================================================
static inline bool IsEmptyArray (const fearray<sLong> allowedTypes) {
  bool found = false;
  for (int f = 0; !found && f != allowedTypes.GetSize(); f += 1) {
    if (allowedTypes.GetItemAt(f) != 0) {
      found = true;
    }
  }
  return !found;
}


//==========================================================================
//
//  continent::GetShuffledMembers
//
//==========================================================================
void continent::GetShuffledMembers (const fearray<sLong> allowedTypes, std::vector<v2> &res,
                                    CheckerFn checkCB)
{
  res.clear();

  if (IsEmptyArray(allowedTypes)) {
    for (auto &pos : Member) {
      if (!checkCB || checkCB(pos)) {
        res.push_back(pos);
      }
    }
  } else {
    int totalCount = 0;
    for (int f = 0; f != allowedTypes.GetSize(); f += 1) {
      const int tt = allowedTypes.GetItemAt(f);
      if (tt > 0 && tt < TerrainTypeLimit) {
        totalCount += ttypeCount[tt];
      } else {
        ABORT("invalid allowed terrain type %d\n", tt);
      }
    }

    if (totalCount == 0) {
      return;
    }

    for (feuLong c = 0; c < Member.size(); ++c) {
      v2 pos = Member[c];
      if (InArray(TypeBuffer[pos.X][pos.Y], allowedTypes)) {
        if (!checkCB || checkCB(pos)) {
          res.push_back(pos);
        }
      }
    }
  }

  // shuffle: https://en.wikipedia.org/wiki/Fisher-Yates_shuffle
  if (res.size() > 1) {
    for (size_t f = (size_t)res.size() - 1; f != 0; f -= 1) {
      const size_t swp = (size_t)RAND_GOOD((int)f + 1);
      if (swp != f) {
        const v2 tmp = res[f];
        res[f] = res[swp];
        res[swp] = tmp;
      }
    }
  }
}


//==========================================================================
//
//  continent::GetRandomMember
//
//==========================================================================
v2 continent::GetRandomMember (const fearray<sLong> allowedTypes, truth *success) {
  if (success) *success = false;

  if (IsEmptyArray(allowedTypes)) {
    if (Member.size() == 0) return v2(0, 0);
    if (success) *success = true;
    return Member[RAND_N((int)Member.size())];
  }

  int totalCount = 0;
  for (int f = 0; f != allowedTypes.GetSize(); f += 1) {
    const int tt = allowedTypes.GetItemAt(f);
    if (tt > 0 && tt < TerrainTypeLimit) {
      totalCount += ttypeCount[tt];
    } else {
      ABORT("invalid allowed terrain type %d\n", tt);
    }
  }

  if (totalCount == 0) return v2(0, 0);

  sLong cnt = RAND_N(totalCount);
  for (feuLong c = 0; c < Member.size(); ++c) {
    v2 pos = Member[c];
    if (InArray(TypeBuffer[pos.X][pos.Y], allowedTypes)) {
      cnt -= 1;
      if (cnt == 0) {
        if (success) *success = true;
        return pos;
      }
    }
  }
  ABORT("Something is very wrong with our planet!");

  if (success) *success = true;
  return v2(0, 0);
}


/* old code
//==========================================================================
//
//  continent::GetRandomMember
//
//==========================================================================
v2 continent::GetRandomMember (int Type, truth *success) {
  if (success) *success = false;

  if (Type == -1) {
    if (Member.size() == 0) return v2(0, 0);
    if (success) *success = true;
    return Member[RAND_N((int)Member.size())];
  }

  if (Type < 0 || Type >= TerrainTypeLimit) ABORT("continent::GetRandomMember: invalid terrain type requiested (%d)!", Type);

  if (!ttypeCount[Type]) return v2(0, 0);

  sLong cnt = RAND_N(ttypeCount[Type]);
  for (feuLong c = 0; c < Member.size(); ++c) {
    v2 pos = Member[c];
    if (TypeBuffer[pos.X][pos.Y] == Type) {
      if (cnt-- == 0) {
        if (success) *success = true;
        return pos;
      }
    }
  }
  ABORT("Something is very wrong with our planet!");

  if (success) *success = true;
  return v2(0, 0);
}
*/


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const continent *Continent) {
  if (Continent) {
    SaveFile.Put(1);
    Continent->Save(SaveFile);
  } else {
    SaveFile.Put(0);
  }
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, continent *&Continent) {
  if (SaveFile.Get()) {
    Continent = new continent;
    Continent->Load(SaveFile);
  }
  return SaveFile;
}
