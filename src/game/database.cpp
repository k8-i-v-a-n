/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through dataset.cpp */
#include <stack>
#include "feparse.h"
#include "wterra.h"


//==========================================================================
//
//  CreateConfigTable
//
//==========================================================================
int CreateConfigTable (databasebase ***ConfigTable, databasebase ***TempTable,
                       databasebase **ConfigArray, sLong *TempTableInfo,
                       int Type, int Configs, int TempTables)
{
  memset(ConfigTable, 0, CONFIG_TABLE_SIZE * sizeof(databasebase **));
  IvanAssert(Configs >= 0);
  for (int c = 0; c != Configs; c += 1) {
    int Config = ConfigArray[c]->Config;
    int Hash = (Config >> 8) ^ (Config & 0xFF);
    if ((TempTableInfo[Hash] & 0xFFFF) != Type) {
      TempTable[0][Hash] = ConfigArray[c];
      TempTableInfo[Hash] = Type | 0x10000;
    } else {
      int Conflicts = (TempTableInfo[Hash] & 0xFFFF0000) >> 16;
      if (Conflicts == TempTables) {
        TempTable[TempTables++] = new databasebase *[CONFIG_TABLE_SIZE];
      }
      TempTable[Conflicts][Hash] = ConfigArray[c];
      TempTableInfo[Hash] += 0x10000;
    }
  }
  for (int c1 = 0; c1 != CONFIG_TABLE_SIZE; c1 += 1) {
    if ((TempTableInfo[c1] & 0xFFFF) == Type) {
      int Entries = (TempTableInfo[c1] & 0xFFFF0000) >> 16;
      ConfigTable[c1] = new databasebase*[Entries + 1];
      for (int c2 = 0; c2 < Entries; ++c2) {
        ConfigTable[c1][c2] = TempTable[c2][c1];
      }
      ConfigTable[c1][Entries] = 0;
    }
  }
  return TempTables;
}


//==========================================================================
//
//  databasecreator<type>::ReadFrom
//
//==========================================================================
template <class type> void databasecreator<type>::ReadFrom (const festring &baseFileName) {
  std::stack<TextInput *> infStack;

  // patch file
  festring pfname = game::GetGamePath()+"script/_patch/" + baseFileName + ".def";
  if (inputfile::DataFileExists(pfname)) {
    //ConLogf("::: %s", pfname.CStr());
    TextInput *ifl = new TextInputFile(pfname, &game::GetGlobalValueMap());
    ifl->SetGetVarCB(game::ldrGetVar);
    infStack.push(ifl);
  }

  // add module files
  auto modlist = game::GetModuleList();
  for (unsigned idx = modlist.size(); idx != 0; --idx) {
    festring infname = game::GetGamePath()+"script/"+modlist[idx-1]+"/"+baseFileName+".def";
    if (inputfile::DataFileExists(infname)) {
      TextInput *ifl = new TextInputFile(infname, &game::GetGlobalValueMap());
      ifl->SetGetVarCB(game::ldrGetVar);
      infStack.push(ifl);
    }
  }

  // add main file
  {
    festring infname = game::GetGamePath()+"script/"+baseFileName+".def";
    TextInput *ifl = new TextInputFile(infname, &game::GetGlobalValueMap());
    ifl->SetGetVarCB(game::ldrGetVar);
    infStack.push(ifl);
  }

  if (infStack.size() == 0) ABORT("Cannot find '%s' scripts!", baseFileName.CStr());

  // load files
  typedef typename type::prototype prototype;
  festring Word, defName;
  database *TempConfig[1024];
  databasebase **TempTable[1024];
  sLong TempTableInfo[CONFIG_TABLE_SIZE];
  int TempTables = 1;
  databasebase *FirstTempTable[CONFIG_TABLE_SIZE];
  TempTable[0] = FirstTempTable;
  memset(TempTableInfo, 0, sizeof(TempTableInfo));
  CreateDataBaseMemberMap();
  while (!infStack.empty()) {
    TextInput *inFile = infStack.top();
    infStack.pop();
    inFile->SetGetVarCB(game::ldrGetVar);
    //ConLogf("MAIN: %s", inFile->GetFileName().CStr());
    for (inFile->ReadWord(Word, false); !inFile->Eof(); inFile->ReadWord(Word, false)) {
      if (Word == "Include") {
        Word = inFile->ReadWord();
        if (inFile->ReadWord() != ";") {
          inFile->Error("Invalid terminator");
        }
        //ConLogf("loading: %s", Word.CStr());
        //ConLogf("include: <%s> <%s>", Word.CStr(), inFile->GetFileName().CStr());
        TextInput *incf = new TextInputFile(inputfile::buildIncludeName(inFile->GetFileName(), Word), &game::GetGlobalValueMap());
        infStack.push(inFile);
        inFile = incf;
        inFile->SetGetVarCB(game::ldrGetVar);
        continue;
      }
      if (Word == "Message") {
        Word = inFile->ReadWord();
        if (inFile->ReadWord() != ";") {
          inFile->Error("Invalid terminator");
        }
        ConLogf("MESSAGE: %s", Word.CStr());
        continue;
      }
      ////////
      truth doExtend = false, doOverride = false;
      if (Word == "Extend") {
        Word = inFile->ReadWord();
        doExtend = true;
      } else if (Word == "Override" || Word == "Redefine") {
        Word = inFile->ReadWord();
        doOverride = true;
      }
      ////////
      defName = Word;
      int Type = protocontainer<type>::SearchCodeName(Word);
      if (!Type) {
        inFile->Error("Odd term <%s>", Word.CStr());
      }
      //ConLogf("class <%s>", Word.CStr());
      prototype *Proto = protocontainer<type>::GetProtoData()[Type];
      if (!Proto) {
        inFile->Error("Something weird with <%s>", defName.CStr());
      }
      database *DataBase;
      int Configs;
      if (doOverride) {
        if (!Proto->ConfigData) {
          inFile->Error("Can't override undefined <%s>", defName.CStr());
        }
        delete [] Proto->ConfigData;
        Proto->ConfigData = NULL;
        Proto->ConfigSize = 0;
      }
      if (doExtend) {
        if (!Proto->ConfigData) {
          inFile->Error("Can't extend undefined <%s>", defName.CStr());
        }
        DataBase = Proto->ConfigData[0];
        Configs = Proto->ConfigSize;
        for (int f = 0; f < Configs; f++) TempConfig[f] = Proto->ConfigData[f];
        delete [] Proto->ConfigData;
        Proto->ConfigData = NULL;
        Proto->ConfigSize = 0;
      } else {
        if (Proto->ConfigData) {
          inFile->Error("Can't redefine <%s>", defName.CStr());
        }
        if (Proto->Base && !Proto->Base->ConfigData) {
          inFile->Error("Database has no description of base prototype <%s>",
                        Proto->Base->GetClassID());
        }
        const database *XBaseDB = (Proto->Base ? *Proto->Base->ConfigData : nullptr);
        DataBase = (XBaseDB ? new database(*XBaseDB) : new database);
        DataBase->InitDefaults(Proto, 0, CONST_S("default"), XBaseDB);
        TempConfig[0] = DataBase;
        Configs = 1;
      }
      if (inFile->ReadWord() != "{") {
        inFile->Error("'{' missing in '%s'", protocontainer<type>::GetMainClassID());
      }
      //for (inFile->ReadWord(Word); Word != "}"; inFile->ReadWord(Word))
      for (;;) {
        inFile->ReadWord(Word, false);
        if (Word.IsEmpty() && inFile->Eof()) {
          if (infStack.empty()) {
            inFile->Error("'}' missing in '%s'", protocontainer<type>::GetMainClassID());
          }
          delete inFile;
          inFile = infStack.top();
          infStack.pop();
          continue;
        }
        //ConLogf("D: %d; <%s>", (int)infStack.size(), Word.CStr());
        if (Word == "}") break;

        if (Word == "Include") {
          Word = inFile->ReadWord();
          if (inFile->ReadWord() != ";") {
            inFile->Error("Invalid terminator");
          }
          //ConLogf("loading: %s", Word.CStr());
          TextInput *incf = new TextInputFile(inputfile::buildIncludeName(inFile->GetFileName(), Word),
                                              &game::GetGlobalValueMap());
          infStack.push(inFile);
          inFile = incf;
          inFile->SetGetVarCB(game::ldrGetVar);
          continue;
        }
        if (Word == "Message") {
          Word = inFile->ReadWord();
          if (inFile->ReadWord() != ";") {
            inFile->Error("Invalid terminator");
          }
          ConLogf("MESSAGE: %s", Word.CStr());
          continue;
        }
        if (Word == "Config") {
          sLong ConfigNumber = -1;
          truth isString = false;
          festring fname;
          fname = inFile->ReadStringOrNumberKeepStr(&ConfigNumber, &isString);
          if (isString) {
            // include file
            //ConLogf("loading: %s", fname.CStr());
            /*
            TextInput *incf = new TextInputFile(game::GetGamePath()+"script/"+fname, &game::GetGlobalValueMap());
            infStack.push(inFile);
            inFile = incf;
            */
            ABORT("Config includes aren't supported yet!");
          } else {
            //ConLogf("new config %d (%s) for %s", ConfigNumber, inFile->GetNumStr().CStr(), defName.CStr());
            const database *CBaseDB = Proto->ChooseBaseForConfig(TempConfig, Configs, ConfigNumber);
            IvanAssert(CBaseDB);
            database *ConfigDataBase = new database(*CBaseDB);
            festring cfgname = inFile->GetNumStr();
            festring infname = inFile->GetFileName();
            ConfigDataBase->InitDefaults(Proto, ConfigNumber, cfgname, CBaseDB);
            TempConfig[Configs++] = ConfigDataBase;
            const TextFileLocation loc(*inFile);
            if (inFile->ReadWord() != "{") {
              inFile->Error("'{' missing in '%s'", protocontainer<type>::GetMainClassID());
            }
            for (inFile->ReadWord(Word); Word != "}"; inFile->ReadWord(Word)) {
              if (Word == "on") {
                Proto->mOnEvents.CollectSource(infStack, &inFile, ConfigNumber);
                continue;
              }
              if (Word == "MainMaterialConfigExt") {
                if (!AnalyzeDataMC(*inFile, *ConfigDataBase, true)) {
                  inFile->Error("Illegal datavalue '%s' found while building up '%s' config #%d (%s)",
                                Word.CStr(), Proto->GetClassID(), ConfigNumber, cfgname.CStr());
                }
              } else if (Word == "SecondaryMaterialConfigExt") {
                if (!AnalyzeDataMC(*inFile, *ConfigDataBase, false)) {
                  inFile->Error("Illegal datavalue '%s' found while building up '%s' config #%d (%s)",
                                Word.CStr(), Proto->GetClassID(), ConfigNumber, cfgname.CStr());
                }
              } else {
                if (!AnalyzeData(*inFile, Word, *ConfigDataBase)) {
                  inFile->Error("Illegal datavalue '%s' found while building up '%s' config #%d (%s)",
                                Word.CStr(), Proto->GetClassID(), ConfigNumber, cfgname.CStr());
                }
              }
            }
            ConfigDataBase->PostProcess(loc);
          }
          continue;
        }
        if (Word == "on") {
          Proto->mOnEvents.CollectSource(infStack, &inFile, -1);
          continue;
        }
        if (Word == "MainMaterialConfigExt") {
          if (!AnalyzeDataMC(*inFile, *DataBase, true)) {
            inFile->Error("Illegal datavalue '%s' found while building up '%s'",
                          Word.CStr(), Proto->GetClassID());
          }
        } else if (Word == "SecondaryMaterialConfigExt") {
          if (!AnalyzeDataMC(*inFile, *DataBase, false)) {
            inFile->Error("Illegal datavalue '%s' found while building up '%s'",
                          Word.CStr(), Proto->GetClassID());
          }
        } else {
          if (!AnalyzeData(*inFile, Word, *DataBase)) {
            inFile->Error("Illegal datavalue '%s' found while building up '%s'",
                          Word.CStr(), Proto->GetClassID());
          }
        }
      }
      DataBase->PostProcess(TextFileLocation());
      //Configs = Proto->CreateSpecialConfigurations(TempConfig, Configs);
      Proto->ConfigData = new database *[Configs];
      Proto->ConfigSize = Configs;
      memmove(Proto->ConfigData, TempConfig, Configs*sizeof(database *));
    }
    delete inFile;
  }

  // remove undefined items
  protocontainer<type>::Cleanup();

  for (int c1 = 0; c1 < SPECIAL_CONFIGURATION_GENERATION_LEVELS; ++c1) {
    for (int c2 = 1; c2 < protocontainer<type>::GetSize(); ++c2) {
      prototype *Proto = protocontainer<type>::GetProtoData()[c2];
      if (!Proto) continue; // missing something
      int Configs = Proto->ConfigSize;
      memmove(TempConfig, Proto->ConfigData, Configs*sizeof(database *));
      Configs = Proto->CreateSpecialConfigurations(TempConfig, Configs, c1);
      if (Proto->ConfigSize != Configs) {
        delete [] Proto->ConfigData;
        Proto->ConfigData = new database *[Configs];
        Proto->ConfigSize = Configs;
        memmove(Proto->ConfigData, TempConfig, Configs*sizeof(database *));
      }
    }
  }

  for (int c1 = 1; c1 < protocontainer<type>::GetSize(); c1 += 1) {
    prototype *Proto = protocontainer<type>::GetProtoData()[c1];
    if (!Proto) continue; // missing something
    TempTables =
      CreateConfigTable(reinterpret_cast<databasebase ***>(Proto->ConfigTable),
      TempTable,
      reinterpret_cast<databasebase **>(Proto->ConfigData),
      TempTableInfo, c1, Proto->ConfigSize, TempTables);
  }

  for (int c1 = 1; c1 < TempTables; ++c1) delete [] TempTable[c1];

  GetDataBaseMemberMap().clear();

  protocontainer<type>::CheckDuplicateConfigs();
}


//==========================================================================
//
//  databasecreator<type>::CreateDivineConfigurations
//
//==========================================================================
template <class type>
int databasecreator<type>::CreateDivineConfigurations (const prototype *Proto,
                                                       database **TempConfig, int Configs)
{
  int OldConfigs = Configs;
  for (int c1 = 1; c1 < protocontainer<god>::GetSize(); c1 += 1) {
    database *ConfigDataBase = 0;
    int c2;
    for (c2 = 1; c2 < OldConfigs; c2 += 1) {
      ConfigDataBase = TempConfig[c2];
      if (ConfigDataBase->Config == c1) break;
    }
    truth Created = false;
    if (c2 == OldConfigs) {
      festring dcfgname = CONST_S("divine(");
      dcfgname << protocontainer<god>::GetProto(c1)->GetClassID();
      dcfgname << ") ";
      dcfgname << (*TempConfig)->CfgStrName;
      //ConLogf("DIVINE [%s] <%s>", dcfgname.CStr(), Proto->GetClassID());
      ConfigDataBase = new database(**TempConfig);
      ConfigDataBase->InitDefaults(Proto, c1, dcfgname, *TempConfig);
      Created = true;
    }
    ConfigDataBase->AttachedGod = c1;
    ConfigDataBase->PostFix << "of " << festring(protocontainer<god>::GetProto(c1)->GetClassID()).CapitalizeCopy();
    if (Created) TempConfig[Configs++] = ConfigDataBase;
  }
  return Configs;
}


template int databasecreator<character>::CreateDivineConfigurations (const prototype *, database **, int);
template int databasecreator<item>::CreateDivineConfigurations (const prototype *, database **, int);
template int databasecreator<olterrain>::CreateDivineConfigurations (const prototype *, database **, int);


// ////////////////////////////////////////////////////////////////////////// //
// databasemember
// ////////////////////////////////////////////////////////////////////////// //
template<class database, class member> struct databasemember : public databasememberbase<database> {
  databasemember (member aMember, cchar *aMName)
    : Member(aMember)
    , MName(aMName)
  {}

  virtual void ReadData (database &DataBase, TextInput &SaveFile) {
    /*
    MClassID = DataBase.ProtoType->GetClassID();
    if (wasRead) {
      SaveFile.Warning("duplicate definition of <%s>:%s", MClassID, MName);
    }
    wasRead = true;
    */
    DataBase.FieldsSet.insert(MName);
    ::ReadData(DataBase.*Member, SaveFile);
    /*
    ConLogf("DB<%s>: member '%s', cfg #%d (%s)", DataBase.ProtoType->GetClassID(),
            MName, DataBase.Config, DataBase.CfgStrName.CStr());
    */
  }

  member Member;
  cchar *MName;
};


//==========================================================================
//
//  AddMember
//
//==========================================================================
template<class database, class member>
void AddMember (std::unordered_map<festring, databasememberbase<database>*> &Map, cchar *Str, member Member) {
  Map.insert(std::pair<festring, databasememberbase<database>*>(Str, new databasemember<database, member>(Member, Str)));
}


/* Explicit instantiations seem to increase compile speed greatly here... */

#define INST_ADD_MEMBER(type, member) \
template void AddMember<type##database, member type##database::*> \
  (std::unordered_map<festring, databasememberbase<type##database>*>&, cchar*, member type##database::*)

INST_ADD_MEMBER(character, int);
//INST_ADD_MEMBER(character, sLong); //k8:64
INST_ADD_MEMBER(character, packcol16);
INST_ADD_MEMBER(character, col24);
INST_ADD_MEMBER(character, packv2);
INST_ADD_MEMBER(character, festring);
INST_ADD_MEMBER(character, fearray<sLong>);
INST_ADD_MEMBER(character, fearray<festring>);
INST_ADD_MEMBER(character, itemcontentscript);

INST_ADD_MEMBER(item, int);
//INST_ADD_MEMBER(item, sLong); //k8:64
INST_ADD_MEMBER(item, col24);
INST_ADD_MEMBER(item, v2);
INST_ADD_MEMBER(item, festring);
INST_ADD_MEMBER(item, fearray<sLong>);
INST_ADD_MEMBER(item, fearray<festring>);
INST_ADD_MEMBER(item, RandomChance);

INST_ADD_MEMBER(glterrain, int);
//INST_ADD_MEMBER(glterrain, sLong); //k8:64
INST_ADD_MEMBER(glterrain, v2);
INST_ADD_MEMBER(glterrain, festring);
INST_ADD_MEMBER(glterrain, fearray<sLong>);

INST_ADD_MEMBER(olterrain, int);
//INST_ADD_MEMBER(olterrain, sLong); //k8:64
INST_ADD_MEMBER(olterrain, v2);
INST_ADD_MEMBER(olterrain, festring);
INST_ADD_MEMBER(olterrain, fearray<sLong>);
INST_ADD_MEMBER(olterrain, fearray<itemcontentscript>);

INST_ADD_MEMBER(owterrain, int);
//INST_ADD_MEMBER(owterrain, sLong); //k8:64
INST_ADD_MEMBER(owterrain, v2);
INST_ADD_MEMBER(owterrain, festring);

INST_ADD_MEMBER(gwterrain, int);
//INST_ADD_MEMBER(gwterrain, sLong); //k8:64
INST_ADD_MEMBER(gwterrain, v2);
INST_ADD_MEMBER(gwterrain, festring);
INST_ADD_MEMBER(gwterrain, float);

INST_ADD_MEMBER(material, int);
//INST_ADD_MEMBER(material, sLong); //k8:64
INST_ADD_MEMBER(material, col24);
INST_ADD_MEMBER(material, festring);
INST_ADD_MEMBER(material, itemcontentscript);

#define ADD_MEMBER(data)  AddMember(Map, #data, &database::data);


//==========================================================================
//
//  databasecreator<character>::CreateDataBaseMemberMap
//
//==========================================================================
template<> void databasecreator<character>::CreateDataBaseMemberMap () {
  databasemembermap &Map = GetDataBaseMemberMap();
  ADD_MEMBER(DefaultArmStrength);
  ADD_MEMBER(DefaultLegStrength);
  ADD_MEMBER(DefaultDexterity);
  ADD_MEMBER(DefaultAgility);
  ADD_MEMBER(DefaultEndurance);
  ADD_MEMBER(DefaultPerception);
  ADD_MEMBER(DefaultIntelligence);
  ADD_MEMBER(DefaultWisdom);
  ADD_MEMBER(DefaultWillPower);
  ADD_MEMBER(DefaultCharisma);
  ADD_MEMBER(DefaultMana);
  ADD_MEMBER(DefaultMoney);
  ADD_MEMBER(TotalSize);
  ADD_MEMBER(CanRead);
  ADD_MEMBER(Sex);
  ADD_MEMBER(CanBeGenerated);
  ADD_MEMBER(CriticalModifier);
  ADD_MEMBER(StandVerb);
  ADD_MEMBER(CanOpen);
  ADD_MEMBER(Frequency);
  ADD_MEMBER(EnergyResistance);
  ADD_MEMBER(FireResistance);
  ADD_MEMBER(PoisonResistance);
  ADD_MEMBER(ElectricityResistance);
  ADD_MEMBER(AcidResistance);
  ADD_MEMBER(SoundResistance);
  ADD_MEMBER(IsUnique);
  ADD_MEMBER(ConsumeFlags);
  ADD_MEMBER(TotalVolume);
  ADD_MEMBER(HeadBitmapPos);
  ADD_MEMBER(TorsoBitmapPos);
  ADD_MEMBER(ArmBitmapPos);
  ADD_MEMBER(LegBitmapPos);
  ADD_MEMBER(RightArmBitmapPos);
  ADD_MEMBER(LeftArmBitmapPos);
  ADD_MEMBER(RightLegBitmapPos);
  ADD_MEMBER(LeftLegBitmapPos);
  ADD_MEMBER(GroinBitmapPos);
  ADD_MEMBER(ClothColor);
  ADD_MEMBER(SkinColor);
  ADD_MEMBER(CapColor);
  ADD_MEMBER(HairColor);
  ADD_MEMBER(EyeColor);
  ADD_MEMBER(TorsoMainColor);
  ADD_MEMBER(BeltColor);
  ADD_MEMBER(BootColor);
  ADD_MEMBER(TorsoSpecialColor);
  ADD_MEMBER(ArmMainColor);
  ADD_MEMBER(GauntletColor);
  ADD_MEMBER(ArmSpecialColor);
  ADD_MEMBER(LegMainColor);
  ADD_MEMBER(LegSpecialColor);
  ADD_MEMBER(IsNameable);
  ADD_MEMBER(BaseEmitation);
  ADD_MEMBER(UsesLongArticle);
  ADD_MEMBER(Adjective);
  ADD_MEMBER(UsesLongAdjectiveArticle);
  ADD_MEMBER(NameSingular);
  ADD_MEMBER(NamePlural);
  ADD_MEMBER(PostFix);
  ADD_MEMBER(ArticleMode);
  ADD_MEMBER(IsAbstract);
  ADD_MEMBER(IsPolymorphable);
  ADD_MEMBER(BaseUnarmedStrength);
  ADD_MEMBER(BaseBiteStrength);
  ADD_MEMBER(BaseKickStrength);
  ADD_MEMBER(AttackStyle);
  ADD_MEMBER(CanUseEquipment);
  ADD_MEMBER(CanKick);
  ADD_MEMBER(CanTalk);
  ADD_MEMBER(ClassStates);
  ADD_MEMBER(CanBeWished);
  ADD_MEMBER(Alias);
  ADD_MEMBER(CreateDivineConfigurations);
  ADD_MEMBER(CreateGolemMaterialConfigurations);
  ADD_MEMBER(Helmet);
  ADD_MEMBER(Amulet);
  ADD_MEMBER(Cloak);
  ADD_MEMBER(BodyArmor);
  ADD_MEMBER(Belt);
  ADD_MEMBER(RightWielded);
  ADD_MEMBER(LeftWielded);
  ADD_MEMBER(RightRing);
  ADD_MEMBER(LeftRing);
  ADD_MEMBER(RightGauntlet);
  ADD_MEMBER(LeftGauntlet);
  ADD_MEMBER(RightBoot);
  ADD_MEMBER(LeftBoot);
  ADD_MEMBER(AttributeBonus);
  ADD_MEMBER(KnownCWeaponSkills);
  ADD_MEMBER(CWeaponSkillHits);
  ADD_MEMBER(RightSWeaponSkillHits);
  ADD_MEMBER(LeftSWeaponSkillHits);
  ADD_MEMBER(PanicLevel);
  ADD_MEMBER(CanBeCloned);
  ADD_MEMBER(Inventory);
  ADD_MEMBER(DangerModifier);
  ADD_MEMBER(DefaultName);
  ADD_MEMBER(FriendlyReplies);
  ADD_MEMBER(HostileReplies);
  ADD_MEMBER(CanZap);
  ADD_MEMBER(FleshMaterial);
  ADD_MEMBER(HasALeg);
  ADD_MEMBER(DeathMessage);
  ADD_MEMBER(IgnoreDanger);
  ADD_MEMBER(HPRequirementForGeneration);
  ADD_MEMBER(DayRequirementForGeneration);
  ADD_MEMBER(IsExtraCoward);
  ADD_MEMBER(SpillsBlood);
  ADD_MEMBER(HasEyes);
  ADD_MEMBER(HasHead);
  ADD_MEMBER(CanThrow);
  ADD_MEMBER(UsesNutrition);
  ADD_MEMBER(AttackWisdomLimit);
  ADD_MEMBER(AttachedGod);
  ADD_MEMBER(BodyPartsDisappearWhenSevered);
  ADD_MEMBER(CanBeConfused);
  ADD_MEMBER(CanApply);
  ADD_MEMBER(WieldedPosition);
  ADD_MEMBER(NaturalSparkleFlags);
  ADD_MEMBER(BiteCapturesBodyPart);
  ADD_MEMBER(IsPlant);
  ADD_MEMBER(MoveType);
  ADD_MEMBER(DestroysWalls);
  ADD_MEMBER(IsRooted);
  ADD_MEMBER(BloodMaterial);
  ADD_MEMBER(VomitMaterial);
  ADD_MEMBER(HasSecondaryMaterial);
  ADD_MEMBER(IsImmuneToLeprosy);
  ADD_MEMBER(PolymorphIntelligenceRequirement);
  ADD_MEMBER(AutomaticallySeen);
  ADD_MEMBER(CanHear);
  ADD_MEMBER(DefaultCommandFlags);
  ADD_MEMBER(ConstantCommandFlags);
  ADD_MEMBER(WillCarryItems);
  ADD_MEMBER(ForceVomitMessage);
  ADD_MEMBER(SweatMaterial);
  ADD_MEMBER(Sweats);
  ADD_MEMBER(IsImmuneToItemTeleport);
  ADD_MEMBER(AlwaysUseMaterialAttributes);
  ADD_MEMBER(IsEnormous);
  ADD_MEMBER(ScienceTalkAdjectiveAttribute);
  ADD_MEMBER(ScienceTalkSubstantiveAttribute);
  ADD_MEMBER(ScienceTalkPrefix);
  ADD_MEMBER(ScienceTalkName);
  ADD_MEMBER(ScienceTalkPossibility);
  ADD_MEMBER(ScienceTalkIntelligenceModifier);
  ADD_MEMBER(ScienceTalkWisdomModifier);
  ADD_MEMBER(ScienceTalkCharismaModifier);
  ADD_MEMBER(ScienceTalkIntelligenceRequirement);
  ADD_MEMBER(ScienceTalkWisdomRequirement);
  ADD_MEMBER(ScienceTalkCharismaRequirement);
  ADD_MEMBER(IsExtraFragile);
  ADD_MEMBER(AllowUnconsciousness);
  ADD_MEMBER(CanChoke);
  ADD_MEMBER(IsImmuneToStickiness);
  ADD_MEMBER(DisplacePriority);
  ADD_MEMBER(RunDescriptionLineOne);
  ADD_MEMBER(RunDescriptionLineTwo);
  ADD_MEMBER(ForceCustomStandVerb);
  ADD_MEMBER(VomittingIsUnhealthy);
  ADD_MEMBER(AllowPlayerToChangeEquipment);
  ADD_MEMBER(TamingDifficulty);
  ADD_MEMBER(IsMasochist);
  ADD_MEMBER(IsSadist);
  ADD_MEMBER(IsCatacombCreature);
  ADD_MEMBER(CreateUndeadConfigurations);
  ADD_MEMBER(UndeadVersions);
  ADD_MEMBER(UndeadAttributeModifier);
  ADD_MEMBER(UndeadVolumeModifier);
  ADD_MEMBER(UndeadCopyMaterials);
  ADD_MEMBER(CanBeGeneratedOnlyInTheCatacombs);
  ADD_MEMBER(IsAlcoholic);
  ADD_MEMBER(IsUndead);
  ADD_MEMBER(IsImmuneToWhipOfThievery);
  ADD_MEMBER(IsRangedAttacker);
  ADD_MEMBER(WhatCategoryToThrow);
  ADD_MEMBER(WhatWeaponConfigToThrow);
  ADD_MEMBER(WhatThrowItemTypesToThrow);
  ADD_MEMBER(AllowedDungeons);
  ADD_MEMBER(LevelTags);
  ADD_MEMBER(HomeLevel);
  ADD_MEMBER(NaturalTeam);
}


//==========================================================================
//
//  databasecreator<item>::CreateDataBaseMemberMap
//
//==========================================================================
template<> void databasecreator<item>::CreateDataBaseMemberMap () {
  databasemembermap &Map = GetDataBaseMemberMap();
  ADD_MEMBER(Possibility);
  ADD_MEMBER(IsDestroyable);
  ADD_MEMBER(CanBeWished);
  ADD_MEMBER(IsMaterialChangeable);
  ADD_MEMBER(WeaponCategory);
  ADD_MEMBER(IsPolymorphSpawnable);
  ADD_MEMBER(IsAutoInitializable);
  ADD_MEMBER(Category);
  ADD_MEMBER(EnergyResistance);
  ADD_MEMBER(FireResistance);
  ADD_MEMBER(PoisonResistance);
  ADD_MEMBER(ElectricityResistance);
  ADD_MEMBER(AcidResistance);
  ADD_MEMBER(SoundResistance);
  ADD_MEMBER(StrengthModifier);
  ADD_MEMBER(FormModifier);
  ADD_MEMBER(DefaultSize);
  ADD_MEMBER(DefaultMainVolume);
  ADD_MEMBER(DefaultSecondaryVolume);
  ADD_MEMBER(BitmapPos);
  ADD_MEMBER(Price);
  ADD_MEMBER(BaseEmitation);
  ADD_MEMBER(UsesLongArticle);
  ADD_MEMBER(Adjective);
  ADD_MEMBER(UsesLongAdjectiveArticle);
  ADD_MEMBER(NameSingular);
  ADD_MEMBER(NamePlural);
  ADD_MEMBER(PostFix);
  ADD_MEMBER(ArticleMode);
  ADD_MEMBER(MainMaterialConfig);
  ADD_MEMBER(SecondaryMaterialConfig);
  ADD_MEMBER(MaterialConfigChances);
  ADD_MEMBER(IsAbstract);
  ADD_MEMBER(IsPolymorphable);
  ADD_MEMBER(Alias);
  ADD_MEMBER(OKVisualEffects);
  ADD_MEMBER(CanBeGeneratedInContainer);
  ADD_MEMBER(ForcedVisualEffects);
  ADD_MEMBER(Roundness);
  ADD_MEMBER(GearStates);
  ADD_MEMBER(IsTwoHanded);
  ADD_MEMBER(CreateDivineConfigurations);
  ADD_MEMBER(CanBeCloned);
  ADD_MEMBER(CanBeMirrored);
  ADD_MEMBER(BeamRange);
  ADD_MEMBER(CanBeBroken);
  ADD_MEMBER(CanBeBurned);
  ADD_MEMBER(WallBitmapPos);
  ADD_MEMBER(FlexibleNameSingular);
  ADD_MEMBER(MaxCharges);
  ADD_MEMBER(MinCharges);
  ADD_MEMBER(CanBePiled);
  ADD_MEMBER(StorageVolume);
  ADD_MEMBER(MaxGeneratedContainedItems);
  ADD_MEMBER(AffectsArmStrengthNoDiv2);
  ADD_MEMBER(AffectsArmStrength);
  ADD_MEMBER(AffectsLegStrength);
  ADD_MEMBER(AffectsDexterityNoDiv2);
  ADD_MEMBER(AffectsDexterity);
  ADD_MEMBER(AffectsAgility);
  ADD_MEMBER(AffectsEndurance);
  ADD_MEMBER(AffectsPerception);
  ADD_MEMBER(AffectsIntelligence);
  ADD_MEMBER(AffectsWisdom);
  ADD_MEMBER(AffectsWillPower);
  ADD_MEMBER(AffectsCharisma);
  ADD_MEMBER(AffectsMana);
  ADD_MEMBER(BaseEnchantment);
  ADD_MEMBER(PriceIsProportionalToEnchantment);
  ADD_MEMBER(InElasticityPenaltyModifier);
  ADD_MEMBER(CanBeUsedBySmith);
  ADD_MEMBER(AffectsCarryingCapacity);
  ADD_MEMBER(DamageDivider);
  ADD_MEMBER(HandleInPairs);
  ADD_MEMBER(CanBeEnchanted);
  ADD_MEMBER(BeamColor);
  ADD_MEMBER(BeamEffect);
  ADD_MEMBER(BeamStyle);
  ADD_MEMBER(WearWisdomLimit);
  ADD_MEMBER(AttachedGod);
  ADD_MEMBER(BreakEffectRangeSquare);
  ADD_MEMBER(WieldedBitmapPos);
  ADD_MEMBER(IsQuestItem);
  ADD_MEMBER(IsGoodWithPlants);
  ADD_MEMBER(IsGoodWithUndead);
  ADD_MEMBER(CreateLockConfigurations);
  ADD_MEMBER(CanBePickedUp);
  ADD_MEMBER(CoverPercentile);
  ADD_MEMBER(TorsoArmorBitmapPos);
  ADD_MEMBER(ArmArmorBitmapPos);
  ADD_MEMBER(AthleteArmArmorBitmapPos);
  ADD_MEMBER(LegArmorBitmapPos);
  ADD_MEMBER(HelmetBitmapPos);
  ADD_MEMBER(CloakBitmapPos);
  ADD_MEMBER(BeltBitmapPos);
  ADD_MEMBER(GauntletBitmapPos);
  ADD_MEMBER(BootBitmapPos);
  ADD_MEMBER(HasSecondaryMaterial);
  ADD_MEMBER(AllowEquip);
  ADD_MEMBER(ReadDifficulty);
  ADD_MEMBER(IsValuable);
  ADD_MEMBER(EnchantmentMinusChance);
  ADD_MEMBER(EnchantmentPlusChance);
  ADD_MEMBER(TeleportPriority);
  ADD_MEMBER(HasNormalPictureDirection);
  ADD_MEMBER(DamageFlags);
  ADD_MEMBER(IsKamikazeWeapon);
  ADD_MEMBER(FlexibilityIsEssential);
  ADD_MEMBER(BreakMsg);
  ADD_MEMBER(IsSadistWeapon);
  ADD_MEMBER(IsThrowingWeapon);
  ADD_MEMBER(ThrowItemTypes);
  ADD_MEMBER(CanFlame);
  ADD_MEMBER(MagicEffect);
  ADD_MEMBER(MagicEffectDuration);
  ADD_MEMBER(MagicEffectChance);
  ADD_MEMBER(MagicMessageCanSee);
  ADD_MEMBER(MagicMessageCannotSee);
  ADD_MEMBER(AllowedDungeons);
  ADD_MEMBER(LevelTags);
  ADD_MEMBER(DescriptiveInfo);
  ADD_MEMBER(CanBeCanned);
  ADD_MEMBER(ForceAllowRegularColors);
  ADD_MEMBER(DrawOutlined);
  ADD_MEMBER(SirenSongResistance);
}


//==========================================================================
//
//  databasecreator<type>::CreateLTerrainDataBaseMemberMap
//
//==========================================================================
template <class type> void databasecreator<type>::CreateLTerrainDataBaseMemberMap () {
  databasemembermap &Map = GetDataBaseMemberMap();
  ADD_MEMBER(BitmapPos);
  ADD_MEMBER(UsesLongArticle);
  ADD_MEMBER(Adjective);
  ADD_MEMBER(UsesLongAdjectiveArticle);
  ADD_MEMBER(NameSingular);
  ADD_MEMBER(NamePlural);
  ADD_MEMBER(PostFix);
  ADD_MEMBER(ArticleMode);
  ADD_MEMBER(MainMaterialConfig);
  ADD_MEMBER(SecondaryMaterialConfig);
  ADD_MEMBER(MaterialConfigChances);
  ADD_MEMBER(IsAbstract);
  ADD_MEMBER(OKVisualEffects);
  ADD_MEMBER(MaterialColorB);
  ADD_MEMBER(MaterialColorC);
  ADD_MEMBER(MaterialColorD);
  ADD_MEMBER(SitMessage);
  ADD_MEMBER(ShowMaterial);
  ADD_MEMBER(AttachedGod);
  ADD_MEMBER(Walkability);
  ADD_MEMBER(HasSecondaryMaterial);
  ADD_MEMBER(UseBorderTiles);
  ADD_MEMBER(BorderTilePriority);
}


//==========================================================================
//
//  databasecreator<glterrain>::CreateDataBaseMemberMap
//
//==========================================================================
template<> void databasecreator<glterrain>::CreateDataBaseMemberMap () {
  CreateLTerrainDataBaseMemberMap();
}


//==========================================================================
//
//  databasecreator<olterrain>::CreateDataBaseMemberMap
//
//==========================================================================
template<> void databasecreator<olterrain>::CreateDataBaseMemberMap () {
  CreateLTerrainDataBaseMemberMap();
  databasemembermap &Map = GetDataBaseMemberMap();
  ADD_MEMBER(CreateDivineConfigurations);
  ADD_MEMBER(DigMessage);
  ADD_MEMBER(CanBeDestroyed);
  ADD_MEMBER(RestModifier);
  ADD_MEMBER(RestMessage);
  ADD_MEMBER(IsUpLink);
  ADD_MEMBER(StorageVolume);
  ADD_MEMBER(HPModifier);
  ADD_MEMBER(IsSafeToCreateDoor);
  ADD_MEMBER(OpenBitmapPos);
  ADD_MEMBER(CreateLockConfigurations);
  ADD_MEMBER(IsAlwaysTransparent);
  ADD_MEMBER(CreateWindowConfigurations);
  ADD_MEMBER(WindowBitmapPos);
  ADD_MEMBER(ShowThingsUnder);
  ADD_MEMBER(LeftOverItems);
  ADD_MEMBER(IsWall);
}


//==========================================================================
//
//  databasecreator<gwterrain>::CreateDataBaseMemberMap
//
//==========================================================================
template<> void databasecreator<gwterrain>::CreateDataBaseMemberMap () {
  databasemembermap &Map = GetDataBaseMemberMap();
  ADD_MEMBER(BitmapPos);
  ADD_MEMBER(IsAbstract);
  ADD_MEMBER(NameStem);
  ADD_MEMBER(UsesLongArticle);
  ADD_MEMBER(Priority);
  ADD_MEMBER(AnimationFrames);
  ADD_MEMBER(IsFatalToStay);
  ADD_MEMBER(SurviveMessage);
  ADD_MEMBER(MonsterSurviveMessage);
  ADD_MEMBER(DeathMessage);
  ADD_MEMBER(MonsterDeathVerb);
  ADD_MEMBER(ScoreEntry);
  ADD_MEMBER(Walkability);
  //HACK: worldmap generation constants
  ADD_MEMBER(MaxTemperature);
  ADD_MEMBER(LatitudeEffect);
  ADD_MEMBER(AltitudeEffect);
  ADD_MEMBER(TemperatureCold);
  ADD_MEMBER(TemperatureMedium);
  ADD_MEMBER(TemperatureWarm);
  ADD_MEMBER(TemperatureHot);
}


//==========================================================================
//
//  databasecreator<owterrain>::CreateDataBaseMemberMap
//
//==========================================================================
template<> void databasecreator<owterrain>::CreateDataBaseMemberMap () {
  databasemembermap &Map = GetDataBaseMemberMap();
  ADD_MEMBER(BitmapPos);
  ADD_MEMBER(IsAbstract);
  ADD_MEMBER(NameSingular);
  ADD_MEMBER(NameStem);
  ADD_MEMBER(UsesLongArticle);
  ADD_MEMBER(AttachedDungeon);
  ADD_MEMBER(AttachedArea);
  ADD_MEMBER(CanBeGenerated);
  ADD_MEMBER(NativeGTerrainType);
  ADD_MEMBER(RevealEnvironmentInitially);
  ADD_MEMBER(WantContinentWith);
  ADD_MEMBER(DistancePOI);
  ADD_MEMBER(MinimumDistanceTo);
  ADD_MEMBER(MaximumDistanceTo);
  ADD_MEMBER(MinimumContinentSize);
  ADD_MEMBER(MaximumContinentSize);
  ADD_MEMBER(CloseTo);
  ADD_MEMBER(Group);
  ADD_MEMBER(SeparateContinent);
  ADD_MEMBER(Probability);
  ADD_MEMBER(CanBeSkipped);
  ADD_MEMBER(PlaceInitially);
  ADD_MEMBER(GenerationMessage);
}


//==========================================================================
//
//  databasecreator<material>::CreateDataBaseMemberMap
//
//==========================================================================
template<> void databasecreator<material>::CreateDataBaseMemberMap () {
  databasemembermap &Map = GetDataBaseMemberMap();
  ADD_MEMBER(CommonFlags);
  ADD_MEMBER(NameFlags);
  ADD_MEMBER(CategoryFlags);
  ADD_MEMBER(BodyFlags);
  ADD_MEMBER(InteractionFlags);
  ADD_MEMBER(StrengthValue);
  ADD_MEMBER(ConsumeType);
  ADD_MEMBER(Density);
  ADD_MEMBER(Color);
  ADD_MEMBER(RainColor);
  ADD_MEMBER(PriceModifier);
  ADD_MEMBER(Emitation);
  ADD_MEMBER(NutritionValue);
  ADD_MEMBER(NameStem);
  ADD_MEMBER(AdjectiveStem);
  ADD_MEMBER(Effect);
  ADD_MEMBER(ConsumeEndMessage);
  ADD_MEMBER(HitMessage);
  ADD_MEMBER(ExplosivePower);
  ADD_MEMBER(Alpha);
  ADD_MEMBER(Flexibility);
  ADD_MEMBER(SpoilModifier);
  ADD_MEMBER(EffectStrength);
  ADD_MEMBER(DigProductMaterial);
  ADD_MEMBER(ConsumeWisdomLimit);
  ADD_MEMBER(AttachedGod);
  ADD_MEMBER(BreatheMessage);
  ADD_MEMBER(StepInWisdomLimit);
  ADD_MEMBER(RustModifier);
  ADD_MEMBER(Acidicity);
  ADD_MEMBER(NaturalForm);
  ADD_MEMBER(HardenedMaterial);
  ADD_MEMBER(SoftenedMaterial);
  ADD_MEMBER(IntelligenceRequirement);
  ADD_MEMBER(Stickiness);
  ADD_MEMBER(FireResistance);
  ADD_MEMBER(DisablesPanicWhenConsumed);
  ADD_MEMBER(BlockESP);
  ADD_MEMBER(CanBeCanned);
}


#define ADD_BASE_VALUE(name)  do { \
  if (Word == #name) { \
    game::GetGlobalValueMap()[CONST_S("Base")] = \
      DataBase.*static_cast<databasemember<database, feuLong database::*>*>(Data)->Member; \
  } \
} while (0)


//==========================================================================
//
//  databasecreator<type>::SetBaseValue
//
//==========================================================================
template <class type> void databasecreator<type>::SetBaseValue (cfestring &,
                                                                databasememberbase<database> *,
                                                                database &)
{}


//==========================================================================
//
//  databasecreator<material>::SetBaseValue
//
//==========================================================================
template<> void databasecreator<material>::SetBaseValue (cfestring &Word,
                                                         databasememberbase<materialdatabase> *Data,
                                                         materialdatabase &DataBase)
{
  ADD_BASE_VALUE(CommonFlags);
  ADD_BASE_VALUE(NameFlags);
  ADD_BASE_VALUE(CategoryFlags);
  ADD_BASE_VALUE(BodyFlags);
  ADD_BASE_VALUE(InteractionFlags);
}


//==========================================================================
//
//  databasecreator<type>::AnalyzeData
//
//==========================================================================
template <class type>
truth databasecreator<type>::AnalyzeData (TextInput &SaveFile, cfestring &Word, database &DataBase) {
  typename databasemembermap::iterator i = GetDataBaseMemberMap().find(Word);
  if (i != GetDataBaseMemberMap().end()) {
    SetBaseValue(Word, i->second, DataBase);
    i->second->ReadData(DataBase, SaveFile);
    CheckDefaults(Word, DataBase);
    return true;
  }
  return false;
}


//==========================================================================
//
//  databasecreator<type>::AnalyzeDataMC
//
//==========================================================================
template <class type>
truth databasecreator<type>::AnalyzeDataMC (TextInput &SaveFile, database &DataBase, bool isPrimary) {
  typename databasemembermap::iterator i = GetDataBaseMemberMap().find(
    CONST_S(isPrimary ? "MainMaterialConfig" : "SecondaryMaterialConfig"));
  if (i == GetDataBaseMemberMap().end()) return false;
  typename databasemembermap::iterator i2 = GetDataBaseMemberMap().find(CONST_S("MaterialConfigChances"));
  if (i2 == GetDataBaseMemberMap().end()) return false;
  TextInput *t1 = 0;
  TextInput *t2 = 0;
  // split to two arrays
  ReadMCData(SaveFile, t1, t2);
  if (!AnalyzeData(*t1, CONST_S(isPrimary ? "MainMaterialConfig" : "SecondaryMaterialConfig"), DataBase)) {
    delete t1;
    delete t2;
    return false;
  }
  if (!AnalyzeData(*t2, CONST_S("MaterialConfigChances"), DataBase)) {
    delete t1;
    delete t2;
    return false;
  }
  delete t1;
  delete t2;
  return true;
}


#define DB_SET_DEFAULT_FLD_X(srcfld_,fldname_,expr_)  do { \
  if (!DataBase.IsFieldSet("" # fldname_)) { \
    DataBase.fldname_ = (expr_); \
  } else { \
    ConLogf("proto '%s:%s': field '%s' cannot override already set field '%s'!", \
            DataBase.ProtoType->GetClassID(), DataBase.CfgStrName.CStr(), \
            "" # srcfld_, "" # fldname_); \
  } \
} while (0)


#define DB_SET_DEFAULT_FLD(srcfld_,fldname_)  do { \
  if (!DataBase.IsFieldSet("" # fldname_)) { \
    DataBase.fldname_ = DataBase.srcfld_; \
  } else { \
    ConLogf("proto '%s:%s': field '%s' cannot override already set field '%s'!", \
            DataBase.ProtoType->GetClassID(), DataBase.CfgStrName.CStr(), \
            "" # srcfld_, "" # fldname_); \
  } \
} while (0)


//==========================================================================
//
//  databasecreator<character>::CheckDefaults
//
//==========================================================================
template <> void databasecreator<character>::CheckDefaults (cfestring &Word, character::database &DataBase) {
  if (Word == "ArmBitmapPos") {
    DB_SET_DEFAULT_FLD(ArmBitmapPos, RightArmBitmapPos);
    DB_SET_DEFAULT_FLD(ArmBitmapPos, LeftArmBitmapPos);
    //DataBase.RightArmBitmapPos = DataBase.LeftArmBitmapPos = DataBase.ArmBitmapPos;
    return;
  }
  if (Word == "LegBitmapPos") {
    DB_SET_DEFAULT_FLD(LegBitmapPos, GroinBitmapPos);
    DB_SET_DEFAULT_FLD(LegBitmapPos, RightLegBitmapPos);
    DB_SET_DEFAULT_FLD(LegBitmapPos, LeftLegBitmapPos);
    //DataBase.GroinBitmapPos = DataBase.RightLegBitmapPos = DataBase.LeftLegBitmapPos = DataBase.LegBitmapPos;
    return;
  }
  if (Word == "ClothColor") {
    DB_SET_DEFAULT_FLD(ClothColor, CapColor);
    DB_SET_DEFAULT_FLD(ClothColor, TorsoMainColor);
    DB_SET_DEFAULT_FLD(ClothColor, ArmMainColor);
    DB_SET_DEFAULT_FLD(ClothColor, GauntletColor);
    DB_SET_DEFAULT_FLD(ClothColor, LegMainColor);
    //DataBase.CapColor = DataBase.TorsoMainColor =
    //    DataBase.ArmMainColor = DataBase.GauntletColor = DataBase.LegMainColor = DataBase.ClothColor;
    return;
  }
  if (Word == "NameSingular") {
    DB_SET_DEFAULT_FLD_X(NameSingular, NamePlural, DataBase.NameSingular + 's');
    //DataBase.NamePlural = DataBase.NameSingular+'s';
    return;
  }
  if (Word == "BaseUnarmedStrength") {
    DB_SET_DEFAULT_FLD_X(BaseUnarmedStrength, BaseBiteStrength, DataBase.BaseUnarmedStrength>>1);
    DB_SET_DEFAULT_FLD_X(BaseUnarmedStrength, BaseKickStrength, DataBase.BaseUnarmedStrength<<1);
    //DataBase.BaseBiteStrength = DataBase.BaseUnarmedStrength>>1;
    //DataBase.BaseKickStrength = DataBase.BaseUnarmedStrength<<1;
    return;
  }
  if (Word == "RightGauntlet") {
    DB_SET_DEFAULT_FLD(RightGauntlet, LeftGauntlet);
    //DataBase.LeftGauntlet = DataBase.RightGauntlet;
    return;
  }
  if (Word == "RightBoot") {
    DB_SET_DEFAULT_FLD(RightBoot, LeftBoot);
    //DataBase.LeftBoot = DataBase.RightBoot;
    return;
  }
  if (Word == "IsUnique") {
    DB_SET_DEFAULT_FLD_X(IsUnique, CanBeWished, !DataBase.IsUnique);
    //DataBase.CanBeWished = !DataBase.IsUnique;
    return;
  }
  if (Word == "DefaultName") {
    // k8: don't add duplicate aliases
    bool found = false;
    for (uInt c = 0; !found && c != DataBase.Alias.Size; c += 1) {
      found = (DataBase.Alias[c] == DataBase.DefaultName);
    }
    if (!found) DataBase.Alias.Add(DataBase.DefaultName);
    return;
  }
}


//==========================================================================
//
//  databasecreator<item>::CheckDefaults
//
//==========================================================================
template <> void databasecreator<item>::CheckDefaults (cfestring &Word, item::database &DataBase) {
  if (Word == "NameSingular") {
    DB_SET_DEFAULT_FLD_X(NameSingular, NamePlural, DataBase.NameSingular + 's');
    DB_SET_DEFAULT_FLD(NameSingular, FlexibleNameSingular);
    //DataBase.NamePlural = DataBase.NameSingular+'s';
    //DataBase.FlexibleNameSingular = DataBase.NameSingular;
    return;
  }
  if (Word == "BitmapPos") {
    DB_SET_DEFAULT_FLD(BitmapPos, WallBitmapPos);
    //DataBase.WallBitmapPos = DataBase.BitmapPos;
    return;
  }
  if (Word == "CanBeCloned") {
    DB_SET_DEFAULT_FLD(CanBeCloned, CanBeMirrored);
    //DataBase.CanBeMirrored = DataBase.CanBeCloned;
    return;
  }
  if (Word == "MaterialConfigChances") {
    DataBase.MaterialConfigChanceSum = femath::SumArray(DataBase.MaterialConfigChances);
    return;
  }
}


//==========================================================================
//
//  databasecreator<glterrain>::CheckDefaults
//
//==========================================================================
template <> void databasecreator<glterrain>::CheckDefaults (cfestring &Word, glterrain::database &DataBase) {
  if (Word == "NameSingular") {
    DB_SET_DEFAULT_FLD_X(NameSingular, NamePlural, DataBase.NameSingular + 's');
    //DataBase.NamePlural = DataBase.NameSingular+'s';
    return;
  }
  if (Word == "MaterialConfigChances") {
    DataBase.MaterialConfigChanceSum = femath::SumArray(DataBase.MaterialConfigChances);
    return;
  }
}


//==========================================================================
//
//  databasecreator<olterrain>::CheckDefaults
//
//==========================================================================
template <> void databasecreator<olterrain>::CheckDefaults (cfestring &Word, olterrain::database &DataBase) {
  if (Word == "NameSingular") {
    DB_SET_DEFAULT_FLD_X(NameSingular, NamePlural, DataBase.NameSingular + 's');
    //DataBase.NamePlural = DataBase.NameSingular+'s';
    return;
  }
  if (Word == "MaterialConfigChances") {
    DataBase.MaterialConfigChanceSum = femath::SumArray(DataBase.MaterialConfigChances);
    return;
  }
}


//==========================================================================
//
//  databasecreator<material>::CheckDefaults
//
//==========================================================================
template <> void databasecreator<material>::CheckDefaults (cfestring &Word, material::database &DataBase) {
  if (Word == "NameStem") {
    DB_SET_DEFAULT_FLD(NameStem, AdjectiveStem);
    //DataBase.AdjectiveStem = DataBase.NameStem;
    return;
  }
  if (Word == "Color") {
    DB_SET_DEFAULT_FLD(Color, RainColor);
    //DataBase.RainColor = DataBase.Color;
    return;
  }
}


//==========================================================================
//
//  databasecreator<owterrain>::CheckDefaults
//
//==========================================================================
template<> void databasecreator<owterrain>::CheckDefaults (cfestring& Word, owterrain::database& DataBase) {
}


//==========================================================================
//
//  databasecreator<gwterrain>::CheckDefaults
//
//==========================================================================
template<> void databasecreator<gwterrain>::CheckDefaults (cfestring& Word, gwterrain::database& DataBase) {
}



//==========================================================================
//
//  databasesystem::Initialize
//
//==========================================================================
void databasesystem::Initialize () {
  /* Must be before character */
  databasecreator<material>::ReadFrom(CONST_S("material"));
  databasecreator<character>::ReadFrom(CONST_S("char"));
  /* Must be before olterrain */
  databasecreator<item>::ReadFrom(CONST_S("item"));
  databasecreator<glterrain>::ReadFrom(CONST_S("glterra"));
  databasecreator<olterrain>::ReadFrom(CONST_S("olterra"));
  databasecreator<gwterrain>::ReadFrom(CONST_S("gwterra"));
  /* Must be last */
  databasecreator<owterrain>::ReadFrom(CONST_S("owterra"));
}


//==========================================================================
//
//  databasecreator<type>::FindDataBase
//
//==========================================================================
template <class type>
inline void databasecreator<type>::FindDataBase (const database *&DataBase,
                                                 const prototype *Proto, int Config)
{
  database **Table = Proto->ConfigTable[(Config >> 8) ^ (Config & 0xFF)];
  if (Table) {
    if ((*Table)->Config == Config) {
      DataBase = *Table;
      return;
    }
    for (++Table; *Table; ++Table) {
      if ((*Table)->Config == Config) {
        DataBase = *Table;
        return;
      }
    }
  }
  DataBase = 0;
}


//==========================================================================
//
//  databasecreator<type>::FindConfigByName
//
//==========================================================================
template <class type>
int databasecreator<type>::FindConfigByName (const prototype *proto, cfestring &acfgname) {
  for (int f = 0; f < CONFIG_TABLE_SIZE; ++f) {
    database **tbl = proto->ConfigTable[f];
    if (!tbl) continue;
    for (; *tbl; ++tbl) {
      if ((*tbl)->CfgStrName == acfgname) {
        return (*tbl)->Config;
      }
    }
  }
  return -1;
}


template void databasecreator<character>::FindDataBase (const database *&, const prototype *, int);
template void databasecreator<material>::FindDataBase (const database *&, const prototype *, int);


//==========================================================================
//
//  databasecreator<type>::InstallDataBase
//
//==========================================================================
template <class type> void databasecreator<type>::InstallDataBase (type *Instance, int Config) {
  const prototype *Proto = Instance->FindProtoType();
  if (!Proto) {
    ABORT("Cannot find prototype for type <%s>", getCPPTypeNameCStr<type>());
  }
  FindDataBase(Instance->DataBase, Proto, Config);
  if (!Instance->DataBase) {
    ABORT("Undefined %s configuration #%08x sought!", Proto->GetClassID(), (unsigned int)Config);
  }
  /*
  if (Config >= 1024) {
    ConLogf("FUCK! Config=%d", Config);
  }
  */
}

#define INST_INSTALL_DATABASE(type) \
  template void databasecreator<type>::InstallDataBase (type *, int); \
  template int databasecreator<type>::FindConfigByName (const prototype *, cfestring &);

INST_INSTALL_DATABASE(material);
INST_INSTALL_DATABASE(character);
INST_INSTALL_DATABASE(item);
INST_INSTALL_DATABASE(glterrain);
INST_INSTALL_DATABASE(olterrain);
INST_INSTALL_DATABASE(gwterrain);
INST_INSTALL_DATABASE(owterrain);
