/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include <stdio.h>
#include "game.h" /* for debug flags */
/* Compiled through coreset.cpp */


//#define xlogf(...)  do { ConPrintf(__VA_ARGS__); } while (0)
#define xlogf(...)


//==========================================================================
//
//  entity::DumpDeadSet
//
//==========================================================================
void entity::DumpDeadSet () {
/*
  ConLogf("=== dead set ===");
  for (VoidSet::const_iterator i = mDeadSet.begin(); i != mDeadSet.end(); ++i) ConLogf("%p", *i);
*/
}


//==========================================================================
//
//  entity::entity
//
//==========================================================================
entity::entity (const entity &Entity) : Emitation(Entity.Emitation), Flags(Entity.Flags) {
  xlogf("entity::entity0: %p\n", this);
  pool::RemoveFromHell(this);
  if (Flags & HAS_BE) {
    //if (DebugGetName()[0] == '[') __builtin_trap();
    pool::Add(this);
  } else {
    // just in case
    pool::Remove(this);
  }
  mOnEvents = Entity.mOnEvents;
}


//==========================================================================
//
//  entity::entity
//
//==========================================================================
entity::entity (int Flags) : Emitation(0), Flags(Flags|EXISTS) {
  xlogf("entity::entity1: %p\n", this);
  pool::RemoveFromHell(this);
  if (Flags & HAS_BE) {
    //if (DebugGetName()[0] == '[') __builtin_trap();
    pool::Add(this);
  } else {
    // just in case
    pool::Remove(this);
  }
}


//==========================================================================
//
//  entity::~entity
//
//==========================================================================
entity::~entity () {
  xlogf("entity::~entity: %p\n", this);
  pool::Remove(this);
  pool::RemoveFromHell(this);
}


//==========================================================================
//
//  entity::SendToHellHelper
//
//==========================================================================
void entity::SendToHellHelper () {
}


//==========================================================================
//
//  entity::DebugGetName
//
//==========================================================================
festring entity::DebugGetName () {
  char buf[128];
  snprintf(buf, sizeof(buf), "[%p]", (void *)this);
  festring res;
  res << buf;
  return res;
}


//==========================================================================
//
//  entity::SendToHell
//
//  Calling SendToHell() marks the entity dead,
//  so that it will be removed by hell::Burn() at the end of the tick.
//  In general, you should never delete an entity instead of calling this,
//  because pool::Be() will crash if the entity currently Be()ing is deleted.
//
//==========================================================================
void entity::SendToHell () {
  if (Flags & EXISTS) {
    if (Flags & HAS_BE) {
      pool::Remove(this);
      Flags ^= HAS_BE;
    }
    pool::AddToHell(this);
    if (DEBUG_HELL_VERBOSE) {
      festring nn = DebugGetName();
      // there are too many fluids leaking ;-)
      if (!nn.StartsWith("fluid:")) {
        ConLogf("DEBUG: entity '%s' sent to hell.", nn.CStr());
      }
    }
    Flags ^= EXISTS;
    SendToHellHelper();
  }
}


//==========================================================================
//
//  entity::Enable
//
//  This function tell the poolsystem whether the `Be()` function of the
//  entity ought to be called during each tick, thus allowing it to alter
//  itself independently. If the entity is stabile, use `Disable()`, since
//  it speeds up the game.
//
//==========================================================================
void entity::Enable () {
  if (!(Flags & HAS_BE)) {
    //if (DebugGetName()[0] == '[') __builtin_trap();
    pool::Add(this);
    Flags |= HAS_BE;
  }
}


//==========================================================================
//
//  entity::Disable
//
//  This function tell the poolsystem whether the `Be()` function of the
//  entity ought to be called during each tick, thus allowing it to alter
//  itself independently. If the entity is stabile, use `Disable()`, since
//  it speeds up the game.
//
//==========================================================================
void entity::Disable () {
  if (Flags & HAS_BE) {
    pool::Remove(this);
    Flags ^= HAS_BE;
  }
}


//==========================================================================
//
//  entity::GetUniqueMemoryMark
//
//==========================================================================
uint32_t entity::GetUniqueMemoryMark (entity *e) {
  if (e) {
    return ((uint32_t *)e)[-1];
  } else {
    return 0;
  }
}


//==========================================================================
//
//  operator new
//
//==========================================================================
void *entity::operator new (size_t size) {
  void *p;
  static int mark = 0;
  if (size == 0) {
    ABORT("FATAL: ALLOCATING ENTITY OF ZERO SIZE!");
  }
  p = ::malloc(size + sizeof(void *) + 16);
  if (!p) {
    ABORT("FATAL: out of memory!");
  }
  ::memset(p, 0, size + sizeof(void *) + 16);
  {
    mark += 1; if (mark == 0) mark = 1;
    *(uint32_t *)p = mark;
    p = (void **)p + 1;
  }
  return p;
}


//==========================================================================
//
//  operator delete
//
//==========================================================================
void entity::operator delete (void *p) {
  if (p) {
    xlogf("delete(%u): %p\n", ((uint32_t *)p)[-1], p);
    if (((uint32_t *)p)[-1] == 0) {
      ABORT("FATAL: FREEING INVALID/FREE ENTITY!");
    }
    ((uint32_t *)p)[-1] = 0;
    p = (void **)p - 1;
    ::free(p);
  }
}


#undef xlogf
