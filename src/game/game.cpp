/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

#include <algorithm>
#include <cstdarg>

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#include "whandler.h"
#include "hscore.h"
#include "rawbit.h"
#include "message.h"
#include "feio.h"
#include "team.h"
#include "iconf.h"
#include "allocate.h"
#include "pool.h"
#include "god.h"
#include "proto.h"
#include "stack.h"
#include "felist.h"
#include "charset.h"
#include "wsquare.h"
#include "game.h"
#include "graphics.h"
#include "bitmap.h"
#include "fesave.h"
#include "feparse.h"
#include "itemset.h"
#include "room.h"
#include "materias.h"
#include "rain.h"
#include "fetime.h"
#include "balance.h"
#include "confdef.h"
#include "wmapset.h"
#include "wterra.h"

#include "fenamegen.h"

#define SAVE_FILE_VERSION   143  /* Increment this if changes make savefiles incompatible */
#define BONE_FILE_VERSION   128  /* Increment this if changes make bonefiles incompatible */

#define LOADED    0
#define NEW_GAME  1
#define BACK      2


std::stack<TextInput *> game::mFEStack;
EventContext game::curevctx;


int game::CurrentLevelIndex;
truth game::InWilderness = false;
worldmap* game::WorldMap;
area* game::AreaInLoad;
square* game::SquareInLoad;
dungeon** game::Dungeon;
int game::CurrentDungeonIndex;
feuLong game::NextCharacterID = 1;
feuLong game::NextItemID = 1;
feuLong game::NextTrapID = 1;
team** game::Team;
feuLong game::LOSTick;
v2 game::CursorPos(-1, -1);
truth game::Zoom = false;
truth game::Generating = false;
double game::AveragePlayerArmStrengthExperience;
double game::AveragePlayerLegStrengthExperience;
double game::AveragePlayerDexterityExperience;
double game::AveragePlayerAgilityExperience;
int game::Teams = 0;
int game::Dungeons = 0;
int game::StoryState = 0;
/* */
int game::XinrochTombStoryState;
int game::MondedrPass;
int game::RingOfThieves;
int game::Masamune;
int game::Muramasa;
int game::LoricatusHammer;
int game::Liberator;
int game::OmmelBloodMission;
int game::RegiiTalkState;
int game::GloomyCaveStoryState;
int game::FreedomStoryState;
int game::AslonaStoryState;
int game::RebelStoryState;

/* */
massacremap game::PlayerMassacreMap;
massacremap game::PetMassacreMap;
massacremap game::MiscMassacreMap;
sLong game::PlayerMassacreAmount = 0;
sLong game::PetMassacreAmount = 0;
sLong game::MiscMassacreAmount = 0;
boneidmap game::BoneItemIDMap;
boneidmap game::BoneCharacterIDMap;
truth game::TooGreatDangerFoundTruth;
itemvectorvector game::ItemDrawVector;
charactervector game::CharacterDrawVector;
truth game::SumoWrestling;
liquid *game::GlobalRainLiquid;
v2 game::GlobalRainSpeed;
sLong game::GlobalRainTimeModifier;
truth game::PlayerSumoChampion;
truth game::PlayerSolicitusChampion;
truth game::PlayerIsChampion;
truth game::HasBoat;
truth game::OnBoat;
truth game::TouristHasSpider;
feuLong game::SquarePartEmitationTick = 0;
sLong game::Turn;
truth game::PlayerRunning;
character* game::LastPetUnderCursor;
charactervector game::PetVector;
double game::DangerFound;
int game::OldAttribute[ATTRIBUTES];
int game::NewAttribute[ATTRIBUTES];
int game::LastAttributeChangeTick[ATTRIBUTES];
int game::NecroCounter;
int game::CursorData;
truth game::CausePanicFlag;

truth game::Loading = false;
truth game::JumpToPlayerBe = false;
//truth game::InGetCommand = false;
character *game::Petrus = 0;
time_t game::TimePlayedBeforeLastLoad;
time_t game::LastLoad;
time_t game::GameBegan;
truth game::PlayerHasReceivedAllGodsKnownBonus;

//festring game::AutoSaveFileName = game::GetSavePath()+"AutoSave";
cchar *const game::Alignment[] = { "L++", "L+", "L", "L-", "N+", "N=", "N-", "C+", "C", "C-", "C--", NULL };
god **game::God;

//cint game::MoveNormalCommandKey[] = { KEY_HOME, KEY_UP, KEY_PAGE_UP, KEY_LEFT, KEY_RIGHT, KEY_END, KEY_DOWN, KEY_PAGE_DOWN, '.' };

cv2 game::MoveVector[] = { v2(-1, -1), v2(0, -1), v2(1, -1), v2(-1, 0), v2(1, 0), v2(-1, 1), v2(0, 1), v2(1, 1), v2(0, 0) };
cv2 game::RelativeMoveVector[] = { v2(-1, -1), v2(1, 0), v2(1, 0), v2(-2, 1), v2(2, 0), v2(-2, 1), v2(1, 0), v2(1, 0), v2(-1, -1) };
cv2 game::BasicMoveVector[] = { v2(-1, 0), v2(1, 0), v2(0, -1), v2(0, 1) };
cv2 game::LargeMoveVector[] = { v2(-1, -1), v2(0, -1), v2(1, -1), v2(2, -1), v2(-1, 0), v2(2, 0), v2(-1, 1), v2(2, 1), v2(-1, 2), v2(0, 2), v2(1, 2), v2(2, 2), v2(0, 0), v2(1, 0), v2(0, 1), v2(1, 1) };
cint game::LargeMoveDirection[] = { 0, 1, 1, 2, 3, 4, 3, 4, 5, 6, 6, 7, 8, 8, 8, 8 };

truth game::LOSUpdateRequested = false;
uChar ***game::LuxTable = 0;
truth game::Running;
character *game::Player;
v2 game::Camera(0, 0);
feuLong game::Tick;
gamescript *game::GameScript = 0;
valuemap game::GlobalValueMap;
mutablemap game::GlobalMutableMap;
dangermap game::DangerMap;
int game::NextDangerIDType;
int game::NextDangerIDConfigIndex;
//int game::FirstDangerIDType;
//int game::FirstDangerIDConfigIndex;
int game::DangerMapItemCount;
characteridmap game::CharacterIDMap;
itemidmap game::ItemIDMap;
trapidmap game::TrapIDMap;
truth game::PlayerHurtByExplosion;
area *game::CurrentArea;
level *game::CurrentLevel;
wsquare ***game::CurrentWSquareMap;
lsquare ***game::CurrentLSquareMap;
festring game::DefaultPolymorphTo;
festring game::DefaultSummonMonster;
festring game::DefaultWish;
festring game::DefaultChangeMaterial;
festring game::DefaultDetectMaterial;
festring game::DefaultTeam;
truth game::WizardMode;
truth game::SkipHiScore;
int game::SeeWholeMapCheatMode;
truth game::GoThroughWallsCheat;
int game::QuestMonstersFound;
bitmap *game::BusyAnimationCache[32];
festring game::PlayerName;
feuLong game::EquipmentMemory[MAX_EQUIPMENT_SLOTS];
olterrain *game::MonsterPortal;
std::vector<v2> game::SpecialCursorPos;
std::vector<int> game::SpecialCursorData;
bool game::SpecialCursorsEnabled = true;
cbitmap *game::EnterImage;
v2 game::EnterTextDisplacement;

std::vector<festring> game::mModuleList;
std::unordered_map<feuLong, festring> game::mItemNotes;

int game::MonsterGenDelay = 0;
bool game::InGetPlayerCommand = false;

// fractional rate part
int game::mDMapAccum;
int game::mDMLastRate;
feuLong game::mDMLastWrapAroundTick;
feuLong game::mDMLastWrapAroundTurn;

highscore::Hash game::GameHash;

v2 game::HiSquare; // highlighted square position


//==========================================================================
//
//  game::GetMaxAlignmentWidth
//
//==========================================================================
int game::GetMaxAlignmentWidth () {
  int wdt = 0;
  for (int f = 0; Alignment[f]; f += 1) {
    wdt = Max(wdt, FONT->TextWidth(Alignment[f]));
  }
  return wdt;
}


//==========================================================================
//
//  game::GetMaxGodNameWidth
//
//==========================================================================
int game::GetMaxGodNameWidth () {
  int wdt = 0;
  for (int f = 1; f <= GODS; f += 1) {
    wdt = Max(wdt, FONT->TextWidth(GetGod(f)->GetName()));
  }
  return wdt;
}


//==========================================================================
//
//  game::CalcGameHash
//
//  should be called after player name is set
//
//==========================================================================
void game::CalcGameHash () {
  // it doesn't have to be crypto-secure. but it better to be one. later.
  do {
    RandomBuffer(GameHash, sizeof(GameHash));
    uint32_t nhash = PlayerName.Hash();
    uint32_t ctime = HashU32((uint32_t)time(NULL));
    uint32_t xhash = nhash - ctime;
    uint8_t xordata[3 * 4];
    memcpy(&xordata[0], &nhash, 4);
    memcpy(&xordata[4], &ctime, 4);
    memcpy(&xordata[8], &xhash, 4);
    size_t n = 0;
    while (n != sizeof(GameHash)) {
      const uint8_t *p = xordata;
      for (unsigned ofs = 0; ofs != (unsigned)sizeof(xordata) && n != sizeof(GameHash); ofs += 1) {
        GameHash[n] ^= p[ofs];
        n += 1;
      }
    }
    // just in case, check if we don't have such hash already.
    // this should never happen, but...
  } while (highscore::HasHash(GameHash));
}


//==========================================================================
//
//  game::DumpGameHash
//
//==========================================================================
void game::DumpGameHash () {
  char buf[512];
  size_t n = 0;
  size_t bpos = 0;
  while (n != sizeof(GameHash)) {
    snprintf(buf + bpos, 8, "%02x", GameHash[n]);
    bpos += 2; n += 1;
  }
  ConLogf("game id: %s", buf);
}


// ////////////////////////////////////////////////////////////////////////// //
owterrain **game::pois = nullptr;
int game::poisSize = 0;

festring game::mBusyMessage;
time_t game::mBusyMessageTime = 0;

int game::AutoSaveTimer = 0;
bool game::PerformImmSave = false;
bool game::JustLoaded = false;

bool game::mMiniMapActive = false;
bool game::mMiniMapSmall = false;
v2 game::mMiniMapXY0 = v2(0, 0);
v2 game::mMiniMapCurPos = v2(-1, -1);
v2 game::mMiniMapScrPos = v2(0, 0);
static bitmap *mmBitmap = 0;
static bitmap *mmBackBitmap = 0;

int game::CurrentSaveOpenCount = 0;


#include "game_concmd.cpp"


//==========================================================================
//
//  game::RegisterGlobalVars
//
//==========================================================================
void game::RegisterGlobalVars () {
  for (auto &&it : GlobalMutableMap) {
    if (HasGlobalValue(it)) {
      new ConGVar(it.CStr(), it.CStr());
    }
  }

  if (FindGlobalValue(CONST_S("CONSOLE_ENABLED"), 0)) {
    graphics::EnableConsole();
    ConEnableStdErr();
  }

  commandsystem::Init(); // in case we haven't yet

  new ConCmdExec();
  new ConCmdTicks();
  new ConCmdMyPos();
  new ConCmdSave();
  new ConCmdAbort();
  new ConCmdWizardMode();
  new ConCmdAddItem();
  new ConCmdSummon();
  new ConCmdEdit();
  new ConCmdState();
  new ConCmdMaterialInfo();
  new ConCmdMiniMap();
  new ConCmdRevealMap();
  new ConCmdSeeMap();
  new ConCmdNoClip();
  new ConCmdScreenshot();
  new ConCmdLevelTeleport();

  new ConCmdPlayerCommand("WM_Heal", "WizardHeal");
  new ConCmdPlayerCommand("WM_Blow", "WizardBlow");
  new ConCmdPlayerCommand("WM_POI", "WizardPOI");
  new ConCmdPlayerCommand("WM_Team", "WizardTeam");
  new ConCmdPlayerCommand("WM_Wish", "WizardWish");
  new ConCmdPlayerCommand("WM_Summon", "SummonMonster");
  //new ConCmdPlayerCommand("WM_Coords", "ShowCoords");
  new ConCmdPlayerCommand("WM_Knowledge", "SecretKnowledge");
  new ConCmdPlayerCommand("WM_Possess", "Possess");
  new ConCmdPlayerCommand("WM_GainDivineKnowledge", "GainDivineKnowledge");
  new ConCmdPlayerCommand("WM_Detach", "DetachBodyPart");
  new ConCmdPlayerCommand("WM_GetScroll", "GetScroll");
  //new ConCmdPlayerCommand("WM_LevelTeleport", "LevelTeleport");
  new ConCmdPlayerCommand("WM_RaiseStats", "RaiseStats");
  new ConCmdPlayerCommand("WM_LowerStats", "LowerStats");
  new ConCmdPlayerCommand("WM_SeeMap", "SeeWholeMap");
  new ConCmdPlayerCommand("WM_NoClip", "WalkThroughWalls");
  new ConCmdPlayerCommand("WM_RaiseGodRelations", "RaiseGodRelations");
  new ConCmdPlayerCommand("WM_LowerGodRelations", "LowerGodRelations");
  new ConCmdPlayerCommand("WM_GainAllItems", "GainAllItems");
  new ConCmdPlayerCommand("WM_Polymorph", "Polymorph");
}


//==========================================================================
//
//  game::ResetAutoSaveTimer
//
//==========================================================================
void game::ResetAutoSaveTimer () {
  AutoSaveTimer = 0;
}


//==========================================================================
//
//  game::CheckAutoSaveTimer
//
//  increments, resets
//
//==========================================================================
bool game::CheckAutoSaveTimer () {
  if (JustLoaded) {
    JustLoaded = false;
    AutoSaveTimer = 0;
    PerformImmSave = false;
    return false;
  }
  if (PerformImmSave) {
    AutoSaveTimer = 0;
    PerformImmSave = false;
    return true;
  }
  const int ival = ivanconfig::GetAutoSaveInterval();
  if (ival > 0) {
    AutoSaveTimer += 1;
    if (AutoSaveTimer >= ival) {
      AutoSaveTimer = 0;
      return true;
    }
  } else {
    AutoSaveTimer = 0;
  }
  return false;
}


//==========================================================================
//
//  game::poiCount
//
//==========================================================================
int game::poiCount () {
  return poisSize;
}


//==========================================================================
//
//  game::poiByIndex
//
//==========================================================================
owterrain *game::poiByIndex (int idx, truth abortOnNotFound) {
  if (idx >= 0 && idx < poisSize) {
    if (!abortOnNotFound || pois[idx]) return pois[idx];
  }
  if (abortOnNotFound) ABORT("POI with index %d not found!", idx);
  return nullptr;
}


//==========================================================================
//
//  game::FindPOIByConfig
//
//==========================================================================
owterrain *game::FindPOIByConfig (int configid) {
  for (int f = 0; f != poisSize; f += 1) {
    if (pois[f] && pois[f]->GetConfig() == configid) return pois[f];
  }
  return nullptr;
}


//==========================================================================
//
//  game::poi
//
//==========================================================================
owterrain *game::poi (cfestring &name, truth abortOnNotFound) {
  auto pcfg = FindGlobalValue(name, -1);
  //ConLogf("<%s>=%d", name.CStr(), pcfg);
  for (int f = 0; f != poisSize; f += 1) {
    if (pois[f] && pois[f]->GetConfig() == pcfg) return pois[f];
  }
  if (abortOnNotFound) {
    //*(int*)(0) = 42;
    ABORT("POI config '%s' not found!", name.CStr());
  }
  return nullptr;
}


//owterrain *game::alienvesselPOI () { return poi("ALIEN_VESSEL", true); }
owterrain *game::attnamPOI () { return poi(CONST_S("ATTNAM"), true); }
owterrain *game::darkforestPOI () { return poi(CONST_S("DARK_FOREST"), true); }
//owterrain *game::dragontowerPOI () { return poi(CONST_S("DRAGON_TOWER"), true); }
owterrain *game::elpuricavePOI () { return poi(CONST_S("ELPURI_CAVE"), true); }
owterrain *game::mondedrPOI () { return poi(CONST_S("MONDEDR"), true); }
owterrain *game::muntuoPOI () { return poi(CONST_S("MUNTUO"), true); }
owterrain *game::newattnamPOI () { return poi(CONST_S("NEW_ATTNAM"), true); }
owterrain *game::underwatertunnelPOI () { return poi(CONST_S("UNDER_WATER_TUNNEL"), true); }
owterrain *game::underwatertunnelexitPOI () { return poi(CONST_S("UNDER_WATER_TUNNEL_EXIT"), true); }
owterrain *game::xinrochtombPOI () { return poi(CONST_S("XINROCH_TOMB"), true); }
owterrain *game::goblinfortPOI () { return poi(CONST_S("GOBLIN_FORT"), true); }
owterrain *game::fungalcavePOI () { return poi(CONST_S("FUNGAL_CAVE"), true); }
owterrain *game::pyramidPOI () { return poi(CONST_S("PYRAMID"), true); }
owterrain *game::aslonaPOI () { return poi(CONST_S("ASLONA_CASTLE"), true); }
owterrain *game::rebelCampPOI () { return poi(CONST_S("REBEL_CAMP"), true); }
owterrain *game::blackmarketPOI () { return poi(CONST_S("BLACK_MARKET"), true); }


//==========================================================================
//
//  game::GetPOIConfigIndex
//
//==========================================================================
int game::GetPOIConfigIndex (owterrain *terra) {
  return (terra ? terra->GetConfig() : 0);
}


//==========================================================================
//
//  game::MoveVectorToDirection
//
//  -1: none
//
//==========================================================================
int game::MoveVectorToDirection (cv2 &mv) {
  for (int c = 0; c < 9; ++c) if (MoveVector[c] == mv) return c;
  return -1;
}


//==========================================================================
//
//  game::AddCharacterID
//
//==========================================================================
void game::AddCharacterID (character *Char, feuLong ID) {
  IvanAssert(Char);
  characteridmap::iterator it = CharacterIDMap.find(ID);
  if (it != CharacterIDMap.end()) {
    #if 1
    ConLogf("WARNING: trying to add existing char ID; id=%u; char='%s'",
            ID, Char->CHAR_DESCRIPTION(DEFINITE));
    #endif
    CharacterIDMap.erase(it);
  }
  CharacterIDMap.insert(std::make_pair(ID, Char));
}


//==========================================================================
//
//  game::RemoveCharacterID
//
//==========================================================================
void game::RemoveCharacterID (feuLong ID) {
  characteridmap::iterator it = CharacterIDMap.find(ID);
  if (it != CharacterIDMap.end()) {
    CharacterIDMap.erase(it);
  }
}


//==========================================================================
//
//  game::AddItemID
//
//==========================================================================
void game::AddItemID (item *Item, feuLong ID) {
  IvanAssert(Item);
  itemidmap::iterator it = ItemIDMap.find(ID);
  if (it != ItemIDMap.end()) {
    #if 1
    ConLogf("WARNING: trying to add existing item ID; id=%u; item='%s'",
            ID, Item->GetNameSingular().CStr());
    #endif
    ItemIDMap.erase(it);
  }
  ItemIDMap.insert(std::make_pair(ID, Item));
}


//==========================================================================
//
//  game::RemoveItemID
//
//==========================================================================
void game::RemoveItemID (feuLong ID) {
  if (ID) {
    itemidmap::iterator it = ItemIDMap.find(ID);
    if (it != ItemIDMap.end()) {
      ItemIDMap.erase(it);
    }
  }
}


//==========================================================================
//
//  game::UpdateItemID
//
//==========================================================================
void game::UpdateItemID (item *Item, feuLong ID) {
  itemidmap::iterator it = ItemIDMap.find(ID);
  if (it != ItemIDMap.end()) {
    it->second = Item;
  }
}


//==========================================================================
//
//  game::AddTrapID
//
//==========================================================================
void game::AddTrapID (entity *Trap, feuLong ID) {
  if (ID) {
    IvanAssert(Trap);
    trapidmap::iterator it = TrapIDMap.find(ID);
    if (it != TrapIDMap.end()) {
      #if 1
      ConLogf("WARNING: trying to add existing trap ID; id=%u", ID);
      #endif
      TrapIDMap.erase(it);
    }
    TrapIDMap.insert(std::make_pair(ID, Trap));
  }
}


//==========================================================================
//
//  game::RemoveTrapID
//
//==========================================================================
void game::RemoveTrapID (feuLong ID) {
  if (ID) {
    trapidmap::iterator it = TrapIDMap.find(ID);
    if (it != TrapIDMap.end()) {
      TrapIDMap.erase(it);
    }
  }
}


//==========================================================================
//
//  game::UpdateTrapID
//
//==========================================================================
void game::UpdateTrapID (entity *Trap, feuLong ID) {
  trapidmap::iterator it = TrapIDMap.find(ID);
  if (it != TrapIDMap.end()) {
    it->second = Trap;
  }
}


//==========================================================================
//
//  game::GetDangerMap
//
//==========================================================================
const dangermap &game::GetDangerMap () {
  return DangerMap;
}


//==========================================================================
//
//  game::ClearItemDrawVector
//
//==========================================================================
void game::ClearItemDrawVector () {
  ItemDrawVector.clear();
}


//==========================================================================
//
//  game::ClearCharacterDrawVector
//
//==========================================================================
void game::ClearCharacterDrawVector () {
  CharacterDrawVector.clear();
}


//==========================================================================
//
//  game::InitScript
//
//==========================================================================
void game::InitScript () {
  TextInputFile ScriptFile(GetGamePath()+"script/dungeon.def", &GlobalValueMap);
  GameScript = new gamescript;
  GameScript->ReadFrom(ScriptFile);

  festring pfname = game::GetGamePath()+"script/_patch/dungeon.def";
  if (inputfile::DataFileExists(pfname)) {
    TextInputFile ifl(pfname, &game::GetGlobalValueMap());
    GameScript->ReadFrom(ifl);
  }

  // load dungeons from modules
  for (auto &modname : mModuleList) {
    festring infname = game::GetGamePath()+"script/"+modname+"/dungeon.def";
    if (inputfile::DataFileExists(infname)) {
      TextInputFile ifl(infname, &game::GetGlobalValueMap());
      GameScript->ReadFrom(ifl);
    }
  }
  GameScript->RandomizeLevels();

  LoadGlobalEvents();
}


//==========================================================================
//
//  game::DeInitPlaces
//
//==========================================================================
void game::DeInitPlaces () {
  for (int f = 0; f < poisSize; ++f) delete pois[f];
  delete [] pois;
  poisSize = 0;
  pois = nullptr;
}


//==========================================================================
//
//  game::InitPlaces
//
//==========================================================================
void game::InitPlaces () {
  // spawn all POI configs (except 0, which is abstract base)
  //ConLogf("owterras: %u", protocontainer<owterrain>::GetSize());
  auto xtype = protocontainer<owterrain>::SearchCodeName(CONST_S("owterrain"));
  if (!xtype) ABORT("Your worldmap is dull and empty.");
  const owterrain::prototype* proto = protocontainer<owterrain>::GetProto(xtype);
  if (!proto) ABORT("wtf?!");
  const owterrain::database *const *configs = proto->GetConfigData();
  if (!configs) ABORT("wtf?!");
  int cfgcount = proto->GetConfigSize();
  //ConLogf("owterrain configs: %d", cfgcount);
  // count "good" pois
  int goodPOIs = 0;
  for (int f = 0; f < cfgcount; ++f) {
    auto cfg = configs[f];
    if (cfg->IsAbstract) continue;
    if (cfg->Config == 0) continue; // base config, skip it (just in case)
    if (!cfg->CanBeGenerated) continue;
    if (cfg->Probability < 1) continue;
    //ConLogf("***POI <%s>", cfg->CfgStrName.CStr());
    ++goodPOIs;
  }
  // at least 4: attnam, new attnam, and two entries to underwater tunner
  if (goodPOIs < 4) ABORT("The world is so dull and boring...");

  pois = new owterrain *[goodPOIs];
  poisSize = 0; // will be used as counter
  for (int f = 0; f < cfgcount; ++f) {
    auto cfg = configs[f];
    if (cfg->IsAbstract) continue;
    if (cfg->Config == 0) continue; // base config, skip it (just in case)
    if (!cfg->CanBeGenerated) continue;
    if (cfg->Probability < 1) continue;
    //if (!ConfigData[c2]->IsAbstract && ConfigData[c2]->IsAutoInitializable)
    //ConLogf("POI <%s>", cfg->CfgStrName.CStr());
    owterrain *poi = proto->Spawn(cfg->Config);
    poi->SetRevealed(false); // for now
    poi->SetPlaced(false); // for now
    if (poisSize >= goodPOIs) ABORT("Somehow i cannot count right!");
    pois[poisSize++] = poi;
  }

  // bubble sort, sorry
  /*
  if (poisSize > 1) {
    int last = poisSize;
    do {
      int newn = 0;
      for (int f = 1; f != last; f += 1) {
        if (pois[f - 1]->GetConfig() > pois[f]->GetConfig()) {
          owterrain *tmp = pois[f - 1];
          pois[f - 1] = pois[f];
          pois[f] = tmp;
          newn = f;
        }
      }
      last = newn;
    } while (last >= 1);
  }
  */
}


//==========================================================================
//
//  game::RevealPOI
//
//==========================================================================
void game::RevealPOI (owterrain *terra) {
  if (!terra) return;
  for (int f = 0; f != poisSize; f += 1) {
    if (pois[f] == terra) {
      const bool killWorldMap = !WorldMap;
      if (killWorldMap) LoadWorldMap();
      GetWorldMap()->PlacePOIAtMap(terra, true); // force revealing
      SaveWorldMap(killWorldMap);
      if (!killWorldMap) {
        GetWorldMap()->SendNewDrawRequest();
      }
    }
  }
}


//==========================================================================
//
//  game::ShowPrologueStory
//
//==========================================================================
void game::ShowPrologueStory () {
  iosystem::TextScreen(CONST_S(
    "You couldn't possibly have guessed this day would differ from any other.\n"
    "It began just as always. You woke up at dawn and drove off the giant spider\n"
    "resting on your face. On your way to work you had serious trouble avoiding\n"
    "the lions and pythons roaming wild around the village. After getting kicked\n"
    "by colony masters for being late you performed your twelve-hour routine of\n"
    "climbing trees, gathering bananas, climbing trees, gathering bananas, chasing\n"
    "monkeys that stole the first gathered bananas, carrying bananas to the village\n"
    "and trying to look happy when real food was distributed.\n\n"
    "Finally you were about to enjoy your free time by taking a quick dip in the\n"
    "nearby crocodile bay. However, at this point something unusual happened.\n"
    "You were summoned to the mansion of Richel Decos, the viceroy of the\n"
    "colony, and were led directly to him."));
  iosystem::TextScreen(CONST_S(
    "\"I have a task for you, citizen\", said the viceroy picking his golden\n"
    "teeth, \"The market price of bananas has taken a deep dive and yet the\n"
    "central government is about to raise taxes. I have sent appeals to high\n"
    "priest Petrus but received no response. I fear my enemies in Attnam are\n"
    "plotting against me and intercepting my messages before they reach him!\"\n\n"
    "\"That is why you must travel to Attnam with a letter I'll give you and\n"
    "deliver it to Petrus directly. Alas, you somehow have to cross the sea\n"
    "between. Because it's winter, all Attnamese ships are trapped by ice and\n"
    "I have none. Therefore you must venture through the small underwater tunnel\n"
    "connecting our islands. It is infested with monsters, but since you have\n"
    "stayed alive here so long, the trip will surely cause you no trouble.\"\n\n"
    "You have never been so happy! According to the mansion's traveling\n"
    "brochures, Attnam is a peaceful but bustling world city on a beautiful\n"
    "snowy fell surrounded by frozen lakes glittering in the arctic sun just\n"
    "like the diamonds of the imperial treasury. Not that you would believe a\n"
    "word. The point is that tomorrow you can finally forget your home and\n"
    "face the untold adventures ahead."));
}


//==========================================================================
//
//  game::ClearSomeListsOnNewGame
//
//==========================================================================
void game::ClearSomeListsOnNewGame () {
  Running = false;
  Player = 0;
  Tick = 0;
  NextDangerIDType = 0;
  NextDangerIDConfigIndex = 0;
  //FirstDangerIDType = 0;
  //FirstDangerIDConfigIndex = 0;
  DangerMapItemCount = 0;
  mDMLastWrapAroundTick = 0;
  mDMLastWrapAroundTurn = 0;
  CharacterIDMap.clear();
  ItemIDMap.clear();
  TrapIDMap.clear();
  PlayerHurtByExplosion = false;
  CurrentArea = 0;
  CurrentLevel = 0;
  //wsquare ***game::CurrentWSquareMap;
  //lsquare ***game::CurrentLSquareMap;
  WizardMode = false;
  SkipHiScore = false;
  SeeWholeMapCheatMode = 0;
  GoThroughWallsCheat = 0;
  QuestMonstersFound = 0;
  MonsterPortal = 0;
  SpecialCursorPos.clear();
  SpecialCursorData.clear();
  SpecialCursorsEnabled = true;
  HiSquare = ERROR_V2;
  ResetAutoSaveTimer();
  MonsterGenDelay = 0;
  InGetPlayerCommand = false;

  AutoSaveTimer = 0;
  PerformImmSave = false;
  JustLoaded = false;

  mMiniMapActive = false;
  mMiniMapSmall = false;

  CurrentSaveOpenCount = 0;
  if (IsSaveArchiveOpened()) CloseSaveArchive();

  WipeItemNotes();
}


//==========================================================================
//
//  game::InitializeGameState
//
//==========================================================================
void game::InitializeGameState () {
  //game::DeInit();
  //pool::BurnHell();
  DeInitPlaces();
  WorldMap = 0;
  Dungeon = 0;
  God = 0;
  Team = 0;
  GameScript = 0;
  msgsystem::Format();
  DangerMap.clear();
  //pool::BurnAllMaterials();
  pool::RemoveEverything(); // memory leak!

  femath::RandSeed(); // reseed PRNG, why not

  //k8: some more cleanups
  ClearSomeListsOnNewGame();

  InitPlaces(); // why not
  CurrentLevel = 0;
  MonsterGenDelay = 0;
  InGetPlayerCommand = false;
  globalwindowhandler::InstallControlLoop(AnimationController);
  LOSTick = 2;
  DangerFound = 0;
  CausePanicFlag = false;
  SetIsRunning(true);
  InWilderness = true;

  iosystem::TextScreen(CONST_S("Generating game...\n\nThis may take some time, please wait."),
                       ZERO_V2, WHITE, false, true, &BusyAnimation);
  igraph::CreateBackGround(GRAY_FRACTAL);

  NextCharacterID = 1;
  NextItemID = 1;
  NextTrapID = 1;

  InitScript();
  CreateTeams();
  CreateGods();

  // create player pawn
  SetPlayer(playerkind::Spawn());
  Player->SetAssignedName(PlayerName);
  Player->SetTeam(GetTeam(PLAYER_TEAM));
  Player->SetNP(SATIATED_LEVEL);
  for (int c = 0; c < ATTRIBUTES; ++c) {
    if (c != ENDURANCE) Player->EditAttribute(c, RAND_2 - RAND_2);
    Player->EditExperience(c, 500, 1<<11);
  }
  Player->SetMoney(Player->GetMoney()+RAND_N(11));
  GetTeam(0)->SetLeader(Player);

  InitDangerMap();

  pool::KillBees();
  if (Player->IsEnabled()) { Player->Disable(); Player->Enable(); }

  Petrus = 0;
  InitDungeons();
  SetCurrentArea(WorldMap = new worldmap(128, 128));
  CurrentWSquareMap = WorldMap->GetMap();
  WorldMap->Generate();
  UpdateCamera();
  SendLOSUpdateRequest();
  Tick = 0;
  Turn = 0;
  InitPlayerAttributeAverage();

  StoryState = 0;
  XinrochTombStoryState = 0;
  MondedrPass = 0;
  RingOfThieves = 0;
  Masamune = 0;
  Muramasa = 0;
  LoricatusHammer = 0;
  Liberator = 0;
  OmmelBloodMission = 0;
  RegiiTalkState = 0;

  GloomyCaveStoryState = 0;
  FreedomStoryState = 0;
  AslonaStoryState = 0;
  RebelStoryState = 0;
  PlayerIsChampion = false;
  HasBoat = false;
  OnBoat = false;

  PlayerMassacreMap.clear();
  PetMassacreMap.clear();
  MiscMassacreMap.clear();
  PlayerMassacreAmount = PetMassacreAmount = MiscMassacreAmount = 0;
  DefaultPolymorphTo.Empty();
  DefaultSummonMonster.Empty();
  DefaultWish.Empty();
  DefaultChangeMaterial.Empty();
  DefaultDetectMaterial.Empty();
  DefaultTeam.Empty();

  // add encrypted scroll
  Player->GetStack()->AddItem(encryptedscroll::Spawn());

  // create pet
  if (!ivanconfig::GetDefaultPetName().IsEmpty() && !ivanconfig::GetStartWithNoPet()) {
    character *Doggie;
    // spawn skeleton puppy sometimes
    if (!RAND_N(100)) {
      Doggie = dog::Spawn(SKELETON_DOG);
    } else {
      Doggie = dog::Spawn();
    }
    Doggie->SetTeam(GetTeam(PLAYER_TEAM));
    GetWorldMap()->GetPlayerGroup().push_back(Doggie);
    Doggie->SetAssignedName(ivanconfig::GetDefaultPetName());
  }

  WizardMode = false;
  SkipHiScore = false;
  SeeWholeMapCheatMode = MAP_HIDDEN;
  GoThroughWallsCheat = false;
  SumoWrestling = false;
  GlobalRainTimeModifier = 2048 - RAND_N(4096);
  PlayerSumoChampion = false;
  PlayerSolicitusChampion = false;
  TouristHasSpider = false;

  protosystem::InitCharacterDataBaseFlags();

  memset(EquipmentMemory, 0, sizeof(EquipmentMemory));
  PlayerRunning = false;
  InitAttributeMemory();
  NecroCounter = 0;
  GameBegan = time(0);
  LastLoad = time(0);
  TimePlayedBeforeLastLoad = time::GetZeroTime();
  PlayerHasReceivedAllGodsKnownBonus = false;

  /*
  ADD_MESSAGE("You commence your journey to Attnam. "
              "Use direction keys to move, "
              "'>' to enter an area and "
              "'?' to view other commands.");
  */

  festring helpmsg;
  bool wasHelp = false;
  festring kname = commandsystem::GetHelpKeyNameFor("ShowKeyLayout");
  if (!kname.IsEmpty()) {
    helpmsg << "Press \1Y" << kname << "\2 for a list of commands";
    wasHelp = true;
  }
  kname = commandsystem::GetHelpKeyNameFor("GoStairsDown");
  if (!kname.IsEmpty()) {
    if (wasHelp) {
      helpmsg << ", \1Y" << kname << "\2 to enter an area.";
    } else {
      helpmsg << "Press \1Y" << kname << "\2 to enter an area.";
    }
  } else {
    if (wasHelp) helpmsg << ".";
  }

  if (!helpmsg.IsEmpty()) {
    ADD_MESSAGE("%s", helpmsg.CStr());
  }

  festring hellomsg = CONST_S("You commence your journey to Attnam.");
  festring feelings = WorldMap->GetFeelings();
  if (!feelings.IsEmpty()) {
    hellomsg << " " << feelings;
  }

  ADD_MESSAGE("%s", hellomsg.CStr());

  //WorldMap->ShowFeelings();
}


//==========================================================================
//
//  game::XmasPresents
//
//==========================================================================
void game::XmasPresents () {
  if (IsXMas()) {
    item *Present = banana::Spawn();
    Player->GetStack()->AddItem(Present);
    ADD_MESSAGE("Atavus is happy today! He gives you %s.", Present->CHAR_NAME(INDEFINITE));
  }
}


//==========================================================================
//
//  game::RunNewGameEvents
//
//==========================================================================
truth game::RunNewGameEvents () {
  return RunGlobalEvent(CONST_S("game_start"));
}


//==========================================================================
//
//  GeneratePlayerName
//
//==========================================================================
static festring GeneratePlayerName () {
  festring PlayerName;
  const char *randNamePatterns[] = {
    MIDDLE_EARTH,
    JAPANESE_NAMES_CONSTRAINED,
    JAPANESE_NAMES_DIVERSE,
    CHINESE_NAMES,
    GREEK_NAMES,
    HAWAIIAN_NAMES_1,
    HAWAIIAN_NAMES_2,
    OLD_LATIN_PLACE_NAMES,
    DRAGONS_PERN,
    DRAGON_RIDERS,
    POKEMON,
    FANTASY_VOWELS_R,
    FANTASY_S_A,
    FANTASY_H_L,
    FANTASY_N_L,
    FANTASY_K_N,
    FANTASY_J_G_Z,
    FANTASY_K_J_Y,
    FANTASY_S_E,
    "!ss !sV", // comm. fork default
    NULL
  };
  do {
    int count = 0; while (randNamePatterns[count]) count += 1;
    count = NG_RAND_GOOD(count);
    PlayerName.Empty();
    if (GenerateRandomName(PlayerName, CONST_S(randNamePatterns[count])) != 0) {
      PlayerName.Empty();
    }
  } while (PlayerName.IsEmpty());
  PlayerName.Capitalize();
  return PlayerName;
}


//==========================================================================
//
//  game::Init
//
//==========================================================================
truth game::Init (cfestring &Name) {
  bool askPlayerName = false;
  if (Name.IsEmpty()) {
    if (ivanconfig::GetDefaultName().IsEmpty()) {
      PlayerName.Empty();
      // look for a single save
      festring savename = iosystem::FindSingleSave(game::GetSavePath());
      if (savename.IsEmpty()) {
        if (PlayerName.IsEmpty()) PlayerName = GeneratePlayerName();
        if (iosystem::StringQuestion(PlayerName, CONST_S("What is your name? (up to 20 letters, empty for random)"),
                                     v2(30, 46), WHITE, 0, 20, true, true) == ABORTED)
        {
          return false;
        }
        if (PlayerName.IsEmpty()) PlayerName = GeneratePlayerName();
      } else {
        PlayerName = savename;
        askPlayerName = true;
      }
    } else {
      PlayerName = ivanconfig::GetDefaultName();
    }
  } else {
    PlayerName = Name;
  }

  outputfile::MakeDir(GetSavePath());
  outputfile::MakeDir(GetBonePath());

  LOSTick = 2;
  DangerFound = 0;
  CausePanicFlag = false;

  ClearSomeListsOnNewGame(); // for loaded games too

  InitPlaces();
  msgsystem::EnableSounds();
  pool::KillBees();
  ResetAutoSaveTimer();
  PerformImmSave = true;

  switch (Load(SaveName(PlayerName))) {
    case LOADED:
      globalwindowhandler::InstallControlLoop(AnimationController);
      SetIsRunning(true);
      SetForceJumpToPlayerBe(true);
      GetCurrentArea()->SendNewDrawRequest();
      SendLOSUpdateRequest();
      ADD_MESSAGE("Game loaded successfully.");
      return true;
    case NEW_GAME:
      if (askPlayerName) {
        //PlayerName.Empty();
        PlayerName = GeneratePlayerName();
        if (iosystem::StringQuestion(PlayerName, CONST_S("What is your name? (up to 20 letters, empty for random)"),
                                     v2(30, 46), WHITE, 0, 20, true, true) == ABORTED)
        {
          return false;
        }
        if (PlayerName.IsEmpty()) PlayerName = GeneratePlayerName();
      }
      CalcGameHash();
      DumpGameHash();
      if (!ivanconfig::GetSkipIntro()) {
        ShowPrologueStory();
      }
      InitializeGameState();
      XmasPresents();
      if (!RunNewGameEvents()) {
        iosystem::TextScreen(CONST_S("Something went very wrong."));
        return false;
      }
      return true;
    default:
      return false;
  }
}


//==========================================================================
//
//  game::DeInit
//
//==========================================================================
void game::DeInit () {
  #if 0
  ConLogf("*** GAME DEINIT ***");
  #endif
  pool::BurnHell();
  DeInitPlaces();
  delete WorldMap; WorldMap = 0;
  if (Dungeon) {
    for (int c = 1; c < Dungeons; ++c) delete Dungeon[c];
    delete [] Dungeon; Dungeon = 0;
  }
  if (God) {
    for (int c = 1; c <= GODS; ++c) delete God[c]; // sorry, Valpuri!
    delete [] God; God = 0;
  }
  if (Team) {
    for (int c = 0; c < Teams; ++c) delete Team[c];
    delete [] Team; Team = 0;
  }
  delete GameScript; GameScript = 0;
  msgsystem::Format();
  DangerMap.clear();
  pool::BurnAllMaterials(); // it should be called here!
  igraph::WipeTiles();

  delete mmBitmap; mmBitmap = 0;
  delete mmBackBitmap; mmBackBitmap = 0;
}


//==========================================================================
//
//  game::Run
//
//  main game loop
//
//==========================================================================
void game::Run () {
  for (;;) {
    if (!InWilderness) {
      // populate levels with new monsters
      MonsterGenDelay += 1;
      if (MonsterGenDelay >= Clamp(MONSTER_GEN_DELAY, 1, 1024)) {
        CurrentLevel->GenerateMonsters();
        MonsterGenDelay = 0;
      }

      // generate necromancers on zombie level
      if (CurrentDungeonIndex == ELPURI_CAVE && CurrentLevelIndex == ZOMBIE_LEVEL && !RAND_N(1000+NecroCounter)) {
        character *Char = necromancer::Spawn(RAND_N(4) ? APPRENTICE_NECROMANCER : MASTER_NECROMANCER);
        v2 Pos;
        for (int c2 = 0; c2 < 30; ++c2) {
          Pos = GetCurrentLevel()->GetRandomSquare(Char);
          if (Pos == ERROR_V2) break;
          if (abs(int(Pos.X)-Player->GetPos().X) > 20 || abs(int(Pos.Y)-Player->GetPos().Y) > 20) break;
        }
        if (Pos != ERROR_V2) {
          Char->SetTeam(GetTeam(MONSTER_TEAM));
          Char->PutTo(Pos);
          Char->SetGenerationDanger(GetCurrentLevel()->GetDifficulty());
          Char->SignalGeneration();
          Char->SignalNaturalGeneration();
          ivantime Time;
          GetTime(Time);
          int Modifier = Time.Day - EDIT_ATTRIBUTE_DAY_MIN;
          if (Modifier > 0) Char->EditAllAttributes(Modifier >> EDIT_ATTRIBUTE_DAY_SHIFT);
          NecroCounter += 50;
        } else {
          delete Char; //Char->SendToHell(); // k8:equipment crash?
        }
      }

      if (GetTick() % 1000 == 0) {
        CurrentLevel->CheckSunLight();
      }

      // it may be rainy there
      //if ((CurrentDungeonIndex == NEW_ATTNAM || CurrentDungeonIndex == ATTNAM) && CurrentLevelIndex == 0) {
      if (!IsInWilderness() && CurrentLevel->IsGlobalRainRandom()) {
        if (GlobalRainLiquid) {
          sLong OldVolume = GlobalRainLiquid->GetVolume();
          sLong NewVolume = Max(sLong(sin((Tick+GlobalRainTimeModifier)*0.0003)*300-150), 0);
          #if 0
          ConLogf("GLOBAL RAIN: old=%d; new=%d", OldVolume, NewVolume);
          #endif
          if (NewVolume && !OldVolume) {
            CurrentLevel->EnableGlobalRain();
          } else if (!NewVolume && OldVolume) {
            CurrentLevel->DisableGlobalRain();
          }
          GlobalRainLiquid->SetVolumeNoSignals(NewVolume);
        }
      }
    }

    try {
      pool::Be();
      pool::BurnHell();
      IncrementTick();
      ApplyDivineTick();
    } catch (quitrequest) {
      if (IsSaveArchiveOpened()) CloseSaveArchive();
      break;
    } catch (areachangerequest) {
    } catch (...) {
      if (IsSaveArchiveOpened()) CloseSaveArchive();
      throw;
    }
  }
}


//==========================================================================
//
//  game::InitLuxTable
//
//==========================================================================
void game::InitLuxTable () {
  if (!LuxTable) {
    Alloc3D(LuxTable, 256, 33, 33);
    for (int c = 0; c < 0x100; ++c) {
      for (int x = 0; x < 33; ++x) {
        for (int y = 0; y < 33; ++y) {
          int X = x-16, Y = y-16;
          LuxTable[c][x][y] = int(c/(double(X*X+Y*Y)/128+1));
        }
      }
    }
    atexit(DeInitLuxTable);
  }
}


//==========================================================================
//
//  game::DeInitLuxTable
//
//==========================================================================
void game::DeInitLuxTable () {
  delete [] LuxTable; LuxTable = 0;
}


//==========================================================================
//
//  game::GetScreenXSize
//
//==========================================================================
int game::GetScreenXSize () {
  /*
       if (WIN_WIDTH > 800) return 42 + 8;
  else if (WIN_WIDTH > 600) return 42;
  else return 42;
  */
  return (ivanconfig::WinWidth()-96) / 16 - 2;
}


//==========================================================================
//
//  game::GetScreenYSize
//
//==========================================================================
int game::GetScreenYSize () {
  //return 26 + 8;
  //return (ivanconfig::WinHeight()-184) / 16;
  return (ivanconfig::WinHeight()-154) / 16; //164
}


//==========================================================================
//
//  game::GetLeftStatsPos
//
//==========================================================================
int game::GetLeftStatsPos () {
  return 4;
}


//==========================================================================
//
//  game::GetVertDollOfs
//
//==========================================================================
int game::GetVertDollOfs () {
  //return 160;
  if (ivanconfig::GetDoubleResModifier() > 5) {
    return ivanconfig::WinHeight() / 6 + 24;
  } else {
    return ivanconfig::WinHeight() / 6 + 6;
  }
}


//==========================================================================
//
//  game::GetVertStatOfs
//
//==========================================================================
int game::GetVertStatOfs () {
  return GetVertDollOfs() / 10;
}


//==========================================================================
//
//  game::CalculateScreenCoordinates
//
//==========================================================================
v2 game::CalculateScreenCoordinates (v2 Pos) {
  if (ivanconfig::GetStatusOnLeft()) {
    return v2(96 + ((Pos.X-Camera.X+1)<<4), (Pos.Y-Camera.Y+2)<<4);
  } else {
    return v2((Pos.X-Camera.X+1)<<4, (Pos.Y-Camera.Y+2)<<4);
  }
}


//==========================================================================
//
//  game::GetTopAreaPosSize
//
//==========================================================================
void game::GetTopAreaPosSize (v2 *scrpos, v2 *size) {
  const v2 sp = CalculateScreenCoordinates(GetCamera());
  const int wdt = GetScreenXSize() * TILE_SIZE;
  if (scrpos) *scrpos = v2(sp.X, 0);
  if (size) *size = v2(wdt, sp.Y - 3);
}


//==========================================================================
//
//  game::GetTopAreaTextPos
//
//==========================================================================
v2 game::GetTopAreaTextPos (int lineCount) {
  v2 scrpos, scrsize;
  GetTopAreaPosSize(&scrpos, &scrsize);
  if (lineCount > 1) {
    scrpos.Y += scrsize.Y - 20;
  } else {
    scrpos.Y += scrsize.Y - 10;
  }
  return scrpos;
}


//==========================================================================
//
//  game::EraseTopArea
//
//==========================================================================
void game::EraseTopArea () {
  v2 scrpos, scrsize;
  GetTopAreaPosSize(&scrpos, &scrsize);
  igraph::BlitBackGround(scrpos, scrsize);
}


//==========================================================================
//
//  game::UpdateCameraX
//
//==========================================================================
void game::UpdateCameraX () {
  UpdateCameraX(Player->GetPos().X);
}


//==========================================================================
//
//  game::UpdateCameraY
//
//==========================================================================
void game::UpdateCameraY () {
  UpdateCameraY(Player->GetPos().Y);
}


//==========================================================================
//
//  game::UpdateCameraX
//
//==========================================================================
void game::UpdateCameraX (int X) {
  UpdateCameraCoordinate(Camera.X, X, GetCurrentArea()->GetXSize(), GetScreenXSize());
}


//==========================================================================
//
//  game::UpdateCameraY
//
//==========================================================================
void game::UpdateCameraY (int Y) {
  UpdateCameraCoordinate(Camera.Y, Y, GetCurrentArea()->GetYSize(), GetScreenYSize());
}


//==========================================================================
//
//  game::UpdateCameraCoordinate
//
//==========================================================================
void game::UpdateCameraCoordinate (int &Coordinate, int Center, int Size, int ScreenSize) {
  const int OldCoordinate = Coordinate;
       if (Size < ScreenSize) Coordinate = (Size-ScreenSize)>>1;
  else if (Center < ScreenSize>>1) Coordinate = 0;
  else if (Center > Size-(ScreenSize>>1)) Coordinate = Size-ScreenSize;
  else Coordinate = Center-(ScreenSize>>1);

  Coordinate = Clamp(Coordinate, 0, Size - ScreenSize);

  if (Coordinate != OldCoordinate) {
    GetCurrentArea()->SendNewDrawRequest();
  }
}


//==========================================================================
//
//  game::Insult
//
//==========================================================================
cchar *game::Insult () {
  const char *insults[19] = {
    "moron",
    "silly",
    "idiot",
    "airhead",
    "jerk",
    "dork",
    "Mr. Mole",
    "navastater",
    "potatoes-for-eyes",
    "lamer",
    "mommo-for-brains",
    "pinhead",
    "stupid-headed person",
    "software abuser",
    "loser",
    "peaballs",
    "person-with-problems",
    "unimportant user",
    "hugger-mugger"
  };
  int n = RAND_N(18);
  if (n < 0 || n > 18) n = 18;
  return insults[n];
}


//==========================================================================
//
//  game::TruthQuestion
//
//  DefaultAnswer = REQUIRES_ANSWER the question requires an answer
//
//==========================================================================
truth game::TruthQuestion (cfestring &String, int DefaultAnswer, int OtherKeyForTrue) {
  festring xstr = String;
  if (DefaultAnswer >= 'a' && DefaultAnswer <= 'z') {
    DefaultAnswer -= 32;
  }
  if (DefaultAnswer == NO || DefaultAnswer == 'N') {
    DefaultAnswer = 'N';
    xstr << " [\1Cy\2/\1RN\2]";
  } else if (DefaultAnswer == YES || DefaultAnswer == 'Y') {
    DefaultAnswer = 'Y';
    xstr << " [\1OY\2/\1Cn\2]";
  } else if (DefaultAnswer == REQUIRES_ANSWER) {
    xstr << " [\1Cy\2/\1Cn\2]";
  }
  else ABORT("Illegal TruthQuestion DefaultAnswer send!");
  int FromKeyQuestion = KeyQuestion(xstr, DefaultAnswer, 'Y', 'N', 'T', 'O',
                                    OtherKeyForTrue, 0);
  return
    FromKeyQuestion == 'Y' ||
    FromKeyQuestion == 'T' ||
    FromKeyQuestion == OtherKeyForTrue;
}


//==========================================================================
//
//  game::DrawEverything
//
//==========================================================================
void game::DrawEverything () {
  DrawEverythingNoBlit();
  graphics::BlitDBToScreen();
}


//==========================================================================
//
//  game::OnScreen
//
//==========================================================================
truth game::OnScreen (v2 Pos) {
  return
    Pos.X >= 0 && Pos.Y >= 0 && Pos.X >= Camera.X && Pos.Y >= Camera.Y &&
    Pos.X < GetCamera().X + GetScreenXSize() && Pos.Y < GetCamera().Y + GetScreenYSize();
}


//==========================================================================
//
//  game::DrawUnderCursor
//
//==========================================================================
void game::DrawUnderCursor (truth AnimationDraw) {
  if (OnScreen(CursorPos)) {
    v2 ScreenCoord = CalculateScreenCoordinates(CursorPos);

    blitdata B = {
      DOUBLE_BUFFER,
      { 0, 0 },
      { ScreenCoord.X, ScreenCoord.Y },
      { TILE_SIZE, TILE_SIZE },
      { 0 },
      TRANSPARENT_COLOR,
      ALLOW_ANIMATE|ALLOW_ALPHA
    };

    if (!IsInWilderness() && !GetSeeWholeMapCheatMode()) {
      lsquare *Square = CurrentLSquareMap[CursorPos.X][CursorPos.Y];
      if (Square->GetLastSeen() != GetLOSTick()) {
        Square->DrawMemorized(B);
      }
    }

    if (DoZoom()) {
      B.Src = B.Dest;
      if (ivanconfig::GetStatusOnLeft()) {
        B.Dest.X = 16;
      } else {
        B.Dest.X = RES.X - 96;
      }
      B.Dest.Y = RES.Y - 96;
      B.Stretch = 5;
      DOUBLE_BUFFER->StretchBlit(B);
    }

    igraph::DrawCursor(ScreenCoord, CursorData|CURSOR_SHADE);
  }
}


//==========================================================================
//
//  game::GetMiniMapXSize
//
//==========================================================================
int game::GetMiniMapXSize () {
  return (mMiniMapSmall ? GetScreenXSize() * 4 - 2 : GetScreenXSize() * 2 - 2);
  //return GetScreenXSize();
}


//==========================================================================
//
//  game::GetMiniMapYSize
//
//==========================================================================
int game::GetMiniMapYSize () {
  return (mMiniMapSmall ? GetScreenYSize() * 4 - 2 : GetScreenYSize() * 2 - 2);
  //return GetScreenYSize();
}


//==========================================================================
//
//  game::CalcMiniMapXY0
//
//==========================================================================
v2 game::CalcMiniMapXY0 (v2 center) {
  const int lw = Max(0, GetCurrentArea()->GetXSize() - GetMiniMapXSize());
  const int lh = Max(0, GetCurrentArea()->GetYSize() - GetMiniMapYSize());
  v2 res = center - v2(GetMiniMapXSize() / 2, GetMiniMapYSize() / 2);
  res.X = Clamp(res.X, 0, lw);
  res.Y = Clamp(res.Y, 0, lh);
  return res;
}


//==========================================================================
//
//  game::ShiftMiniMap
//
//==========================================================================
void game::ShiftMiniMap (v2 delta) {
  const int lw = Max(0, GetCurrentArea()->GetXSize() - GetMiniMapXSize());
  const int lh = Max(0, GetCurrentArea()->GetYSize() - GetMiniMapYSize());
  mMiniMapXY0 += delta;
  mMiniMapXY0.X = Clamp(mMiniMapXY0.X, 0, lw);
  mMiniMapXY0.Y = Clamp(mMiniMapXY0.Y, 0, lh);
}


//==========================================================================
//
//  GenerateMiniMapBack
//
//==========================================================================
static void GenerateMiniMapBack (v2 Size) {
  IvanAssert(Size.X > 0 && Size.X <= 16384);
  IvanAssert(Size.Y > 0 && Size.Y <= 16384);

  if (!mmBackBitmap || mmBackBitmap->GetSize() != Size) {
    delete mmBackBitmap;
    mmBackBitmap = new bitmap(Size);
    int Side = 1025;
    int **Map;
    Alloc2D(Map, Side, Side);
    femath::GenerateFractalMap(Map, Side, Side - 1, 800);

    for (int x = 0; x < Size.X; x += 1) {
      for (int y = 0; y < Size.Y; y += 1) {
        int Element = Limit<int>(abs(Map[1024 - x][1024 - (Size.Y - y)]) / 30, 0, 100);
        Element = 3 + Element / 5;
        col16 cc = MakeRGB16(Element + (Element >> 1), Element + (Element >> 1), 0);
        mmBackBitmap->PutPixel(x, y, cc);
      }
    }

    delete [] Map;
  }
}


//==========================================================================
//
//  game::PrepareMiniMapBitmap
//
//==========================================================================
bitmap *game::PrepareMiniMapBitmap (v2 Size) {
  Size.X = Min(Size.X, GetCurrentArea()->GetXSize() * TILE_SIZE);
  Size.Y = Min(Size.Y, GetCurrentArea()->GetYSize() * TILE_SIZE);
  IvanAssert(Size.X > 0 && Size.X <= 16384);
  IvanAssert(Size.Y > 0 && Size.Y <= 16384);
  if (!mmBitmap || mmBitmap->GetSize().X != Size.X || mmBitmap->GetSize().Y != Size.Y) {
    delete mmBitmap;
    mmBitmap = new bitmap(Size);
    //GenerateMiniMapBack(v2(Size.X + 16, Size.Y + 16));
    GenerateMiniMapBack(v2(GetScreenXSize() * TILE_SIZE,
                           GetScreenYSize() * TILE_SIZE));
  }
  //mmBitmap->ClearToColor(MakeRGB16(100/2, 80/2, 20/2));
  mmBitmap->ClearToColor(TRANSPARENT_COLOR);
  return mmBitmap;
}


//==========================================================================
//
//  game::DrawMiniMapNoBlit
//
//==========================================================================
void game::DrawMiniMapNoBlit () {
  v2 Size = v2(GetMiniMapXSize(), GetMiniMapYSize());

  mMiniMapScrPos = CalculateScreenCoordinates(game::GetCamera());
  //mMiniMapScrPos += v2(4, 4);

  if (Size.X > GetCurrentArea()->GetXSize()) {
    mMiniMapXY0.X = 0;
    Size.X = GetCurrentArea()->GetXSize();
    int mwdt = Size.X * (game::IsMiniMapSmall() ? 4 : 8);
    int swdt = GetScreenXSize() * TILE_SIZE;
    //ConLogf("mwdt=%d; swdt=%d", mwdt, swdt);
    mMiniMapScrPos.X += (swdt - mwdt) / 2;
  } else {
    mMiniMapScrPos.X += 8;
  }

  if (Size.Y > GetCurrentArea()->GetYSize()) {
    mMiniMapXY0.Y = 0;
    Size.Y = GetCurrentArea()->GetYSize();
    int mhgt = Size.Y * (game::IsMiniMapSmall() ? 4 : 8);
    int shgt = GetScreenYSize() * TILE_SIZE;
    mMiniMapScrPos.Y += (shgt - mhgt) / 2;
  } else {
    mMiniMapScrPos.Y += 8;
  }

  GetCurrentArea()->DrawMiniMap(mMiniMapXY0, Size);

  blitdata BlitData = {
    Bitmap:DOUBLE_BUFFER,
    Src:{ 0, 0 }, /* src */
    //Dest:mMiniMapScrPos - v2(8, 8), /* dest */
    Dest:CalculateScreenCoordinates(game::GetCamera()),
    Border:mmBackBitmap->GetSize(), /* size */
    { Flags:0 }, /* lumi; flags/stretch */
    MaskColor:TRANSPARENT_COLOR, /* mask color */
    CustomData:0/*ALLOW_ANIMATE|ALLOW_ALPHA*/, /* custom data */
  };
  mmBackBitmap->NormalBlit(BlitData);

  blitdata SBlitData = {
    Bitmap:DOUBLE_BUFFER,
    Src:{ 0, 0 }, /* src */
    Dest:mMiniMapScrPos, /* dest */
    Border:mmBitmap->GetSize(), /* size */
    { Flags:0 }, /* lumi; flags/stretch */
    MaskColor:TRANSPARENT_COLOR, /* mask color */
    CustomData:ALLOW_ANIMATE|ALLOW_ALPHA, /* custom data */
  };
  SBlitData.Stretch = (game::IsMiniMapSmall() ? -4 : -2);
  mmBitmap->StretchBlit(SBlitData);

  DrawMiniMapCursors();
  DrawMiniMapMarks();
}


//==========================================================================
//
//  game::DrawComparisonInfo
//
//==========================================================================
void game::DrawComparisonInfo () {
  if (Player && Player->IsPlayer() && Player->IsEnabled()) {
    v2 Pos = Player->GetPos();
    if (OnScreen(Pos) && Player->HasComparisonInfo()) {
      const std::vector<festring> &mComparisonInfo = Player->GetGearComparisonInfo();
      const std::vector<festring> &mOtherItemInfo = Player->GetOtherComparisonInfo();

      int yofs = 0;
      if (!mComparisonInfo.empty()) {
        int lines = (int)mComparisonInfo.size();
        int hgt = 10; //lines * 10 + 10;
        for (int f = 0; f != lines; f += 1) {
          festring s = mComparisonInfo[f];
          if (!s.IsEmpty() && s[0] == '-') {
            hgt += 4;
          } else {
            hgt += 10;
          }
        }
        bool incyofs = false;

        v2 plrScrPos = game::CalculateScreenCoordinates(Pos);
        v2 winPos = game::CalculateScreenCoordinates(game::GetCamera() + v2(0, game::GetScreenYSize()));
        if (winPos.Y - hgt - 2 >= plrScrPos.Y) {
          // bottom
          winPos.Y -= hgt;
        } else {
          // top
          winPos = game::CalculateScreenCoordinates(v2(game::GetCamera()));
          winPos.Y += 4;
          incyofs = true;
        }
        winPos.X += 4;

        int wdt = 0;
        for (int f = 0; f != lines; f += 1) {
          festring s = mComparisonInfo[f];
          if (!s.IsEmpty() && s[0] != '-') {
            const int sw = FONT->TextWidth(s);
            if (wdt < sw) wdt = sw;
          }
        }
        wdt += 8;

        DOUBLE_BUFFER->DrawSimplePopup(winPos, v2(wdt, hgt - 4), LIGHT_GRAY, MakeRGB16(16, 16, 16));
        winPos.X += 4; winPos.Y += 4;
        for (int f = 0; f != lines; f += 1) {
          festring s = mComparisonInfo[f];
          int hgt = 10;
          if (!s.IsEmpty()) {
            if (s[0] == '-') {
              DOUBLE_BUFFER->DrawHorizontalLine(winPos.X, winPos.X + wdt - 10, winPos.Y,
                                                MakeRGB16(0x46, 0x46, 0x46), true);
              hgt = 4;
            } else {
              FONT->PrintStr(DOUBLE_BUFFER, winPos, ORANGE, s);
            }
          }
          winPos.Y += hgt;
        }
        if (incyofs) yofs = winPos.Y - 2;
      }

      if (!mOtherItemInfo.empty()) {
        int lines = (int)mOtherItemInfo.size();
        int hgt = lines * 10 + 10;

        // top
        v2 winPos = game::CalculateScreenCoordinates(v2(game::GetCamera()));
        winPos.Y += 4 + yofs;
        winPos.X += 4;

        int wdt = 0;
        for (int f = 0; f != lines; f += 1) {
          festring s = mOtherItemInfo[f];
          const int sw = FONT->TextWidth(s);
          if (wdt < sw) wdt = sw;
        }
        wdt += 8;

        DOUBLE_BUFFER->DrawSimplePopup(winPos, v2(wdt, hgt - 4), LIGHT_GRAY, MakeRGB16(16, 16, 16));
        winPos.X += 4; winPos.Y += 4;
        for (int f = 0; f != lines; f += 1) {
          festring s = mOtherItemInfo[f];
          FONT->PrintStr(DOUBLE_BUFFER, winPos, ORANGE, s);
          winPos.Y += 10;
        }
      }
    }
  }
}


//==========================================================================
//
//  game::DrawEverythingNoBlit
//
//==========================================================================
void game::DrawEverythingNoBlit (truth AnimationDraw) {
  if (LOSUpdateRequested && Player->IsEnabled()) {
    if (!IsInWilderness()) {
      GetCurrentLevel()->UpdateLOS();
    } else {
      GetWorldMap()->UpdateLOS();
    }
  }

  if (OnScreen(CursorPos)) {
    if (!IsInWilderness() || GetSeeWholeMapCheatMode() ||
        CurrentWSquareMap[CursorPos.X][CursorPos.Y]->GetLastSeen())
    {
      GetCurrentArea()->GetSquare(CursorPos)->SendStrongNewDrawRequest();
    } else {
      DOUBLE_BUFFER->Fill(CalculateScreenCoordinates(CursorPos), TILE_V2, 0);
    }
  }

  if (SpecialCursorsEnabled) {
    for (size_t c = 0; c < SpecialCursorPos.size(); ++c) {
      if (OnScreen(SpecialCursorPos[c])) {
        GetCurrentArea()->GetSquare(SpecialCursorPos[c])->SendStrongNewDrawRequest();
      }
    }
  }

  if (HiSquare != ERROR_V2 && GetCurrentArea()->IsValidPos(HiSquare) &&
      OnScreen(HiSquare))
  {
    GetCurrentArea()->GetSquare(HiSquare)->SendStrongNewDrawRequest();
  }


  globalwindowhandler::UpdateTick();
  GetCurrentArea()->Draw(AnimationDraw);

  if (HiSquare != ERROR_V2 && GetCurrentArea()->IsValidPos(HiSquare) &&
      OnScreen(HiSquare))
  {
    v2 ScreenCoord = CalculateScreenCoordinates(HiSquare);
    igraph::DrawCursor(ScreenCoord, GREEN_CURSOR | CURSOR_TARGET);
    GetCurrentArea()->GetSquare(HiSquare)->SendStrongNewDrawRequest();
  }

  // this draws player paperdoll and stats; also, draws player name
  Player->DrawPanel(AnimationDraw);

  if (!AnimationDraw) msgsystem::Draw();

  DrawUnderCursor(AnimationDraw);

  if (Player->IsEnabled()) {
    if (Player->IsSmall()) {
      v2 Pos = Player->GetPos();
      if (OnScreen(Pos)) {
        v2 ScreenCoord = CalculateScreenCoordinates(Pos);
        igraph::DrawCursor(ScreenCoord, Player->GetCursorData());
      }
    } else {
      for (int f = 0; f < Player->GetSquaresUnder(); ++f) {
        v2 Pos = Player->GetPos(f);
        if (OnScreen(Pos)) {
          v2 ScreenCoord = CalculateScreenCoordinates(Pos);
          igraph::DrawCursor(ScreenCoord, Player->GetCursorData()|CURSOR_BIG, f);
        }
      }
    }
  }

  if (SpecialCursorsEnabled) {
    for (size_t c = 0; c < SpecialCursorPos.size(); ++c) {
      if (OnScreen(SpecialCursorPos[c])) {
        v2 ScreenCoord = CalculateScreenCoordinates(SpecialCursorPos[c]);
        igraph::DrawCursor(ScreenCoord, SpecialCursorData[c]);
        GetCurrentArea()->GetSquare(SpecialCursorPos[c])->SendStrongNewDrawRequest();
      }
    }
  }

  DrawComparisonInfo();

  if (mMiniMapActive) {
    DrawMiniMapNoBlit();
  }
}


//==========================================================================
//
//  game::SetMiniMapActive
//
//==========================================================================
void game::SetMiniMapActive (bool active) {
  if (mMiniMapActive != active) {
    if (active) {
      //FIXME
      mMiniMapXY0 = v2(0, 0);
      if (PLAYER && PLAYER->IsEnabled()) {
        mMiniMapXY0 = CalcMiniMapXY0(PLAYER->GetPos());
      }
      mMiniMapActive = true;
    } else {
      mMiniMapActive = false;
    }
    GetCurrentArea()->SendNewDrawRequest();
    DrawEverythingNoBlit();
  }
}


//==========================================================================
//
//  game::AutoSave
//
//  called from `character::Be()`. other places will simply
//  signal "imm save".
//
//==========================================================================
truth game::AutoSave () {
  // avoid double saving
  ResetAutoSaveTimer();
  PerformImmSave = false;
  return game::Save(false/*vacuum*/);
}


//==========================================================================
//
//  game::Save
//
//==========================================================================
truth game::Save (bool needVacuum) {
  //ConLogf("plrpos=(%d,%d)", Player->GetPos().X, Player->GetPos().Y);
  if (!GetCurrentArea() || !GetCurrentArea()->GetSquare(Player->GetPos()) ||
      !GetCurrentArea()->GetSquare(Player->GetPos())->GetCharacter())
  {
    Menu(0, v2(RES.X >> 1, RES.Y >> 1), CONST_S("Sorry, can't save due to I.V.A.N. bug.\r"),
         CONST_S("Continue\r"), LIGHT_GRAY);
    return false;
  }

  festring skipfile;
  if (InWilderness) {
    skipfile = "worldmap/worldmap.map";
  } else {
    skipfile = GetCurrentDungeon()->GetSaveLevelFileName(CurrentLevelIndex);
  }

  const bool commtTrx = (CurrentSaveOpenCount != 0);
  if (CurrentSaveOpenCount == 0) {
    ConLogf("*** saving game (from scratch)...");
    OpenSaveFileForWrite(false);
  } else {
    ConLogf("*** saving game (to opened save)...");
  }
  IvanAssert(CurrentSaveOpenCount == 1);

  if (!RunSaveUpdater(&GameSaveFn, NULL, false)) {
    ConLogf("ERROR: saving failed!");
    if (commtTrx) SaveArchiveAbortAllTransactions();
    CloseSaveFileForWrite(false, false);
    Menu(0, v2(RES.X >> 1, RES.Y >> 1), CONST_S("Sorry, cannot write save file.\r"),
         CONST_S("Continue\r"), LIGHT_GRAY);
    return false;
  }

  IvanAssert(CurrentSaveOpenCount == 1);
  //if (commtTrx) SaveArchiveEndTransaction();
  if (!CloseSaveFileForWrite(needVacuum, false)) {
    ConLogf("ERROR: cannot close SQLite archive!");
    Menu(0, v2(RES.X >> 1, RES.Y >> 1), CONST_S("Sorry, cannot write save file.\r"),
         CONST_S("Continue\r"), LIGHT_GRAY);
    return false;
  }

  ConLogf("*** game saved, and save file closed.");
  IvanAssert(CurrentSaveOpenCount == 0);
  game::GetCurrentArea()->SendNewDrawRequest();

  return true;
}


//==========================================================================
//
//  game::GameSaveFn
//
//==========================================================================
truth game::GameSaveFn (void *udata) {
  // create screenshot
  DrawEverythingNoBlit();
  graphics::DrawTransientPopup(CONST_S("Saving..."));

  // save screenshot
  {
    RawBitmapScreenShot *rawss = DOUBLE_BUFFER->CreateRaw(0.6);
    SQArchive *arc = GetSaveArchiveObj();
    IvanAssert(arc);
    SQAFile xfd = arc->FileCreate(CONST_S("screenshot.raw"), 2); // normal compression
    if (!xfd) {
      delete rawss;
      return false;
    }
    rawss->Save(arc, xfd);
    arc->FileClose(xfd);
    delete rawss;
  }

  // save game info (not used by the game)
  #define iprintf(...)  arc->FilePrintf(wfd, __VA_ARGS__)
  {
    SQArchive *arc = GetSaveArchiveObj();
    IvanAssert(arc);
    SQAFile wfd = arc->FileCreate(CONST_S("info.txt"), 0); // do not compress
    if (!wfd) {
      return false;
    }
    iprintf("Player Name: %s\n", PlayerName.CStr());
    time_t tms = GetTimeSpent();
    int tsecs = (int)(tms % 60); tms /= 60;
    int tmins = (int)(tms % 60); tms /= 60;
    int thours = (int)(tms % 24); tms /= 24;
    int tdays = (int)tms;
    if (tdays) {
      iprintf("Real time played: %d days, %d hours, %d minutes, %d seconds\n",
               tdays, thours, tmins, tsecs);
    } else if (thours) {
      iprintf("Real time played: %d hours, %d minutes, %d seconds\n", thours, tmins, tsecs);
    } else if (tmins) {
      iprintf("Real time played: %d minutes, %d seconds\n", tmins, tsecs);
    } else {
      iprintf("Real time played: %d seconds\n", tsecs);
    }
    if (IsInWilderness()) {
      iprintf("Current location: wilderness\n");
    } else {
      /*
      iprintf("Current location: dungeon #%d, level #%d\n",
               CurrentDungeonIndex, CurrentLevelIndex);
      */
      iprintf("Current location: %s\n",
               game::GetCurrentDungeon()->GetShortLevelDescription(game::GetCurrentLevelIndex()).CStr());
    }
    if (WizardMode) {
      iprintf("Wizard mode active%s%s\n",
               (SeeWholeMapCheatMode ? " (with map cheat)" : ""),
               (GoThroughWallsCheat ? " (with wall cheat)" : ""));
    }
    if (SkipHiScore) {
      iprintf("Console used.\n");
    }
    iprintf("Game tick: %d\n", Tick);
    iprintf("Game turn: %d\n", game::GetTurn());
    ivantime Time;
    game::GetTime(Time);
    iprintf("Game time: day %d, time %d:%s%d\n", Time.Day,
             Time.Hour, (Time.Min < 10 ? "0" : ""), Time.Min);

    if (PlayerSumoChampion) iprintf("Won Sumo fight.\n");
    if (PlayerSolicitusChampion) iprintf("Champion of Solicitus.\n");
    if (TouristHasSpider) iprintf("Gave spider to the child tourist.\n");
    if (HasBoat) iprintf("Can travel over the sea on a ship.\n");

    if (WizardMode) {
      iprintf("Avg. arm strength exp: %f\n", AveragePlayerArmStrengthExperience);
      iprintf("Avg. leg strength exp: %f\n", AveragePlayerLegStrengthExperience);
      iprintf("Avg. dexterity exp: %f\n", AveragePlayerDexterityExperience);
      iprintf("Avg. agility exp: %f\n", AveragePlayerAgilityExperience);
    }

    //FIXME: add more data here!
    arc->FileClose(wfd);
  }
  #undef iprintf

  if (!SaveGameState()) {
    #if 1
    ConLogf("ERROR saving game state!");
    #endif
    return false;
  }

  if (InWilderness) {
    //SaveWorldMap(false);
    outputfile SaveFile(-1, CONST_S("worldmap/worldmap.map"), ivanconfig::GetCompressionLevel());
    SaveFile << WorldMap;
  } else {
    GetCurrentDungeon()->SaveLevel(CurrentLevelIndex, false);
  }

  game::ResetAutoSaveTimer();
  return true;
}


//==========================================================================
//
//  game::SaveGameState
//
//==========================================================================
truth game::SaveGameState () {
  {
    outputfile SaveFile(-1, CONST_S("game/info.dat"), 0);
    uint32_t tm = (uint32_t)time(NULL);
    // version, time, etc.
    SaveFile << int(SAVE_FILE_VERSION);
    SaveFile << (uint32_t)sizeof(highscore::Hash);
    SaveFile.WriteBytes(GameHash, (int)sizeof(GameHash));
    SaveFile
      << tm << (uint32_t)0
      << GetTimeSpent() /* or in more readable format: time() - LastLoad + TimeAtLastLoad */
      << PlayerName
      << Player->GetPos()
      << CurrentDungeonIndex
      << CurrentLevelIndex
      << Camera
    ;
  }

  {
    outputfile SaveFile(-1, CONST_S("game/prng.dat"), 0);
    femath::SavePRNG(SaveFile);
  }

  /* no need to save this, chardb will do the checks for us
  {
    outputfile SaveFile(-1, CONST_S("game/modules.dat"), 0);
    SaveModuleList(SaveFile);
  }
  */

  {
    outputfile SaveFile(-1, CONST_S("game/script.dat"), ivanconfig::GetCompressionLevel());
    SaveFile << GameScript;
  }

  {
    outputfile SaveFile(-1, CONST_S("game/state.dat"), ivanconfig::GetCompressionLevel());
    SaveFile
      << PlayerName
      << Player->GetPos()
      << CurrentDungeonIndex
      << CurrentLevelIndex
      << Camera
      << WizardMode
      << SkipHiScore
      << SeeWholeMapCheatMode
      << GoThroughWallsCheat
      << Tick
      << Turn
      << InWilderness
      << NextCharacterID
      << NextItemID
      << NextTrapID
      << NecroCounter
      << SumoWrestling
      << PlayerSumoChampion
      << GlobalRainTimeModifier
      << PlayerSolicitusChampion
      << TouristHasSpider
      << AveragePlayerArmStrengthExperience
      << AveragePlayerLegStrengthExperience
      << AveragePlayerDexterityExperience
      << AveragePlayerAgilityExperience
      << Teams
      << Dungeons
      << PlayerRunning
      << StoryState
      << MondedrPass
      << RingOfThieves
      << Masamune
      << Muramasa
      << LoricatusHammer
      << Liberator
      << OmmelBloodMission
      << RegiiTalkState
      << XinrochTombStoryState
      << GloomyCaveStoryState
      << FreedomStoryState
      << AslonaStoryState
      << RebelStoryState
      << PlayerIsChampion
      << HasBoat
      << OnBoat
      << PlayerHasReceivedAllGodsKnownBonus
      << PlayerMassacreMap
      << PetMassacreMap
      << MiscMassacreMap
      << PlayerMassacreAmount
      << PetMassacreAmount
      << MiscMassacreAmount
      << DangerMap
      << NextDangerIDType
      << NextDangerIDConfigIndex
      << DefaultPolymorphTo
      << DefaultSummonMonster
      << DefaultWish
      << DefaultChangeMaterial
      << DefaultDetectMaterial
      << DefaultTeam
    ;
  }

  {
    outputfile SaveFile(-1, CONST_S("game/pois.dat"), ivanconfig::GetCompressionLevel());
    SaveFile << (sLong)poisSize;
    for (int pc = 0; pc != poisSize; pc += 1) {
      SaveFile << pois[pc];
    }
  }

  {
    outputfile SaveFile(-1, CONST_S("game/equipment.dat"), ivanconfig::GetCompressionLevel());
    SaveFile << (int32_t)MAX_EQUIPMENT_SLOTS;
    SaveArray(SaveFile, EquipmentMemory, MAX_EQUIPMENT_SLOTS);
  }

  {
    outputfile SaveFile(-1, CONST_S("game/attrs.dat"), 0);
    SaveFile << (int32_t)ATTRIBUTES;
    for (int c = 0; c != ATTRIBUTES; c += 1) {
      SaveFile << OldAttribute[c] << NewAttribute[c] << LastAttributeChangeTick[c];
    }
  }

  {
    outputfile SaveFile(-1, CONST_S("game/dungeons.dat"), ivanconfig::GetCompressionLevel());
    SaveFile << (int32_t)Dungeons;
    for (int c = 1; c != Dungeons; c += 1) {
      SaveFile << Dungeon[c];
    }
  }

  {
    outputfile SaveFile(-1, CONST_S("game/gods.dat"), ivanconfig::GetCompressionLevel());
    SaveFile << (int32_t)GODS;
    for (int c = 1; c <= GODS; c += 1) {
      SaveFile << God[c];
    }
  }

  {
    outputfile SaveFile(-1, CONST_S("game/teams.dat"), ivanconfig::GetCompressionLevel());
    SaveFile << (int32_t)Teams;
    for (int c = 0; c != Teams; c += 1) {
      if (Team[c]) {
        SaveFile << (uint8_t)1;
        SaveFile << Team[c];
      } else {
        SaveFile << (uint8_t)0;
      }
    }
  }

  if (!mItemNotes.empty()) {
    outputfile SaveFile(-1, CONST_S("game/item-notes.dat"), ivanconfig::GetCompressionLevel());
    SaveFile << mItemNotes;
  } else {
    GetSaveArchiveObj()->FileDelete(CONST_S("game/item-notes.dat"));
  }

  {
    outputfile SaveFile(-1, CONST_S("game/messages.dat"), ivanconfig::GetCompressionLevel());
    msgsystem::Save(SaveFile);
  }

  {
    outputfile SaveFile(-1, CONST_S("game/chardb.dat"), ivanconfig::GetCompressionLevel());
    protosystem::SaveCharacterDataBaseFlags(SaveFile);
  }

  return true;
}


//==========================================================================
//
//  game::Load
//
//==========================================================================
int game::Load (cfestring &SaveNamePath) {
  JustLoaded = false;
  mMiniMapActive = false;
  festring wadname = SaveNamePath + ".save";
  if (!inputfile::fileExists(wadname)) {
    return NEW_GAME;
  }
  if (!OpenSaveArchive(wadname, false)) {
    return NEW_GAME;
  }

  graphics::DrawTransientPopup(CONST_S("Loading..."));

  int res = LoadGameState();
  CloseSaveArchive();
  if (res == LOADED) {
    ResetAutoSaveTimer();
    JustLoaded = true;
  }
  return res;
}


//==========================================================================
//
//  game::LoadGameState
//
//==========================================================================
truth game::LoadGameState () {
  try {
    v2 Pos;

    // "info.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/info.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      int Version;
      SaveFile >> Version;
      if (Version != SAVE_FILE_VERSION) {
        if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                            CONST_S("Sorry, this save is incompatible with the new version.\r"
                                    "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
        {
          return NEW_GAME;
        } else {
          return BACK;
        }
      }
      // unique game id
      uint32_t hsz = 0;
      SaveFile >> hsz;
      if (hsz != (uint32_t)sizeof(highscore::Hash)) {
        if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                            CONST_S("Sorry, this save is incompatible with the current game version.\r"
                                    "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
        {
          return NEW_GAME;
        } else {
          return BACK;
        }
      }
      SaveFile.ReadBytes(GameHash, (int)sizeof(GameHash));
      // skip save time
      uint32_t tm;
      SaveFile >> tm >> tm;
      SaveFile
        >> TimePlayedBeforeLastLoad
        >> PlayerName
        >> Pos
        >> CurrentDungeonIndex
        >> CurrentLevelIndex
        >> Camera
      ;
    }

    // "modules.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/modules.dat"), false);
      if (SaveFile.IsOpen()) {
        if (!LoadAndCheckModuleList(SaveFile)) {
          if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                              CONST_S("Sorry, this save is incompatible with the current module list.\r"
                                      "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
          {
            return NEW_GAME;
          } else {
            return BACK;
          }
        }
      }
    }

    // "prng.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/prng.dat"), false);
      if (SaveFile.IsOpen()) {
        femath::LoadPRNG(SaveFile);
      }
    }

    // "script.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/script.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      SaveFile >> GameScript;
    }

    // "state.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/state.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      SaveFile
        >> PlayerName
        >> Pos
        >> CurrentDungeonIndex
        >> CurrentLevelIndex
        >> Camera
        >> WizardMode
        >> SkipHiScore
        >> SeeWholeMapCheatMode
        >> GoThroughWallsCheat
        >> Tick
        >> Turn
        >> InWilderness
        >> NextCharacterID
        >> NextItemID
        >> NextTrapID
        >> NecroCounter
        >> SumoWrestling
        >> PlayerSumoChampion
        >> GlobalRainTimeModifier
        >> PlayerSolicitusChampion
        >> TouristHasSpider
        >> AveragePlayerArmStrengthExperience
        >> AveragePlayerLegStrengthExperience
        >> AveragePlayerDexterityExperience
        >> AveragePlayerAgilityExperience
        >> Teams
        >> Dungeons
        >> PlayerRunning
        >> StoryState
        >> MondedrPass
        >> RingOfThieves
        >> Masamune
        >> Muramasa
        >> LoricatusHammer
        >> Liberator
        >> OmmelBloodMission
        >> RegiiTalkState
        >> XinrochTombStoryState
        >> GloomyCaveStoryState
        >> FreedomStoryState
        >> AslonaStoryState
        >> RebelStoryState
        >> PlayerIsChampion
        >> HasBoat
        >> OnBoat
        >> PlayerHasReceivedAllGodsKnownBonus
        >> PlayerMassacreMap
        >> PetMassacreMap
        >> MiscMassacreMap
        >> PlayerMassacreAmount
        >> PetMassacreAmount
        >> MiscMassacreAmount
        >> DangerMap
        >> NextDangerIDType
        >> NextDangerIDConfigIndex
        >> DefaultPolymorphTo
        >> DefaultSummonMonster
        >> DefaultWish
        >> DefaultChangeMaterial
        >> DefaultDetectMaterial
        >> DefaultTeam
      ;
    }

    // will be recalculated
    DangerMapItemCount = 0;
    mDMLastWrapAroundTick = 0;
    mDMLastWrapAroundTurn = 0;

    // "pois.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/pois.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      sLong pcnt;
      SaveFile >> pcnt;
      if (pcnt != poisSize) {
        if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                            CONST_S("Sorry, this save is incompatible with the new version (POIs).\r"
                                    "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
        {
          return NEW_GAME;
        } else {
          return BACK;
        }
      }

      for (int pc = 0; pc != pcnt; pc += 1) {
        delete pois[pc];
        SaveFile >> pois[pc];
        if (!pois[pc]) {
          if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                              CONST_S("Sorry, this save is broken.\r"
                                      "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
          {
            return NEW_GAME;
          } else {
            return BACK;
          }
        }
      }
    }

    // "equipment.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/equipment.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      int32_t cnt;
      SaveFile >> cnt;
      if (cnt != MAX_EQUIPMENT_SLOTS) {
        if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                            CONST_S("Sorry, this save is incompatible with the new version.\r"
                                    "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
        {
          return NEW_GAME;
        } else {
          return BACK;
        }
      }
      LoadArray(SaveFile, EquipmentMemory, MAX_EQUIPMENT_SLOTS);
    }

    // "attrs.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/attrs.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      int32_t cnt;
      SaveFile >> cnt;
      if (cnt != ATTRIBUTES) {
        if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                            CONST_S("Sorry, this save is incompatible with the new version.\r"
                                    "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
        {
          return NEW_GAME;
        } else {
          return BACK;
        }
      }
      for (int c = 0; c != ATTRIBUTES; c += 1) {
        SaveFile >> OldAttribute[c] >> NewAttribute[c] >> LastAttributeChangeTick[c];
      }
    }

    // "dungeons.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/dungeons.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      int32_t cnt;
      SaveFile >> cnt;
      if (cnt != Dungeons) {
        if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                            CONST_S("Sorry, this save is incompatible with the new version.\r"
                                    "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
        {
          return NEW_GAME;
        } else {
          return BACK;
        }
      }
      //FIXME: memory leak!
      Dungeon = new dungeon*[Dungeons];
      Dungeon[0] = 0;
      for (int c = 1; c != Dungeons; c += 1) {
        SaveFile >> Dungeon[c];
      }
    }

    // "gods.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/gods.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      int32_t cnt;
      SaveFile >> cnt;
      if (cnt != GODS) {
        if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                            CONST_S("Sorry, this save is incompatible with the new version.\r"
                                    "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
        {
          return NEW_GAME;
        } else {
          return BACK;
        }
      }
      //FIXME: memory leak!
      God = new god*[GODS+1];
      God[0] = 0;
      for (int c = 1; c <= GODS; c += 1) {
        SaveFile >> God[c];
      }
    }

    // "teams.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/teams.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      int32_t cnt;
      SaveFile >> cnt;
      if (cnt != Teams) {
        if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                            CONST_S("Sorry, this save is incompatible with the new version.\r"
                                    "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
        {
          return NEW_GAME;
        } else {
          return BACK;
        }
      }
      //FIXME: memory leak!
      Team = new team*[Teams];
      for (int c = 0; c != Teams; c += 1) {
        uint8_t present = 255;
        SaveFile >> present;
        if (present == 0) {
          Team[c] = 0;
        } else if (present == 1) {
          SaveFile >> Team[c];
        } else {
          if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                              CONST_S("Sorry, this save is incompatible with the new version.\r"
                                      "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
          {
            return NEW_GAME;
          } else {
            return BACK;
          }
        }
      }
    }

    if (InWilderness) {
      {
        inputfile SaveFile(-1, CONST_S("worldmap/worldmap.map"));
        SaveFile >> WorldMap;
      }
      SetCurrentArea(WorldMap);
      CurrentWSquareMap = WorldMap->GetMap();
      igraph::CreateBackGround(GRAY_FRACTAL);
    } else {
      CurrentLevel = GetCurrentDungeon()->LoadLevel(CurrentLevelIndex);
      SetCurrentArea(CurrentLevel);
      CurrentLSquareMap = CurrentLevel->GetMap();
      igraph::CreateBackGround(*CurrentLevel->GetLevelScript()->GetBackGroundType());
    }

    SetPlayer(GetCurrentArea()->GetSquare(Pos)->GetCharacter());
    if (!PLAYER) {
      DeInit();
      if (!iosystem::Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                          CONST_S("Sorry, this save is broken due to bug in I.V.A.N.\r"
                                  "Start new game?\r"), CONST_S("Yes\rNo\r"), LIGHT_GRAY))
      {
        return NEW_GAME;
      } else {
        return BACK;
      }
    }

    // "chardb.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/chardb.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      protosystem::LoadCharacterDataBaseFlags(SaveFile);
    }

    // "item-notes.dat"
    {
      WipeItemNotes();
      inputfile SaveFile(-1, CONST_S("game/item-notes.dat"), false);
      if (SaveFile.IsOpen()) {
        SaveFile >> mItemNotes;
      }
    }

    // "messages.dat"
    {
      inputfile SaveFile(-1, CONST_S("game/messages.dat"), false);
      if (!SaveFile.IsOpen()) return NEW_GAME;
      msgsystem::Load(SaveFile);
    }

    LastLoad = time(NULL);
  } catch (FileError *e) {
    festring msg = e->msg;
    delete e;
    if (IsSaveArchiveOpened()) CloseSaveArchive();
    if (!iosystem::Menu(0, v2(RES.X/2, RES.Y/2),
                        CONST_S("Sorry, got reading error\r<") + msg + ">\rStart new game?\r",
                        CONST_S("Yes\rNo\r"), LIGHT_GRAY))
    {
      return NEW_GAME;
    } else {
      return BACK;
    }
  }

  DumpGameHash();

  return LOADED;
}


//==========================================================================
//
//  game::OpenCurrentSave
//
//==========================================================================
void game::OpenCurrentSave () {
  if (CurrentSaveOpenCount == 0) {
    festring wadname = SaveName() + ".save";
    if (!OpenSaveArchive(wadname, true)) {
      ABORT("cannot open saved game");
    }
  }
  CurrentSaveOpenCount += 1;
}


//==========================================================================
//
//  game::CloseCurrentSave
//
//==========================================================================
void game::CloseCurrentSave () {
  IvanAssert(CurrentSaveOpenCount > 0);
  CurrentSaveOpenCount -= 1;
  if (CurrentSaveOpenCount == 0) {
    CloseSaveArchive();
  }
}


//==========================================================================
//
//  game::OpenSaveFileForWrite
//
//==========================================================================
truth game::OpenSaveFileForWrite (bool abortOnError) {
  festring savefn = SaveName() + ".save";

  IvanAssert(CurrentSaveOpenCount == 0);
  if (!OpenSaveArchive(savefn, true)) {
    if (abortOnError) ABORT("cannot save game");
    return false;
  }

  IvanAssert(CurrentSaveOpenCount == 0);
  CurrentSaveOpenCount += 1;
  SaveArchiveBeginTransaction();
  return true;
}


//==========================================================================
//
//  game::CloseSaveFileForWrite
//
//==========================================================================
truth game::CloseSaveFileForWrite (bool doVacuum, bool abortOnError) {
  #if 1
  if (!IsSaveArchiveOpened()) {
    ConLogf("ERROR: cannot close unopened save archive!");
    //__builtin_trap();
  } else if (IsSaveArchiveErrored()) {
    ConLogf("ERROR: error writing to save archive!");
  }
  #endif

  IvanAssert(CurrentSaveOpenCount == 1);
  CurrentSaveOpenCount -= 1;

  if (!IsSaveArchiveOpened() || IsSaveArchiveErrored()) {
    SaveArchiveAbortAllTransactions();
    CloseSaveArchive();
    if (abortOnError) ABORT("cannot save game");
    return false;
  }

  SaveArchiveEndTransaction();
  if (doVacuum) {
    ConLogf("vacuuming savegame archive...");
    SaveArchiveVacuum();
  }
  if (!CloseSaveArchive()) {
    if (abortOnError) ABORT("cannot save game");
    return false;
  }

  return true;
}


//==========================================================================
//
//  game::RunSaveUpdater
//
//==========================================================================
truth game::RunSaveUpdater (truth (*updaterFn) (void *udata), void *udata, bool abortOnError) {
  if (!IsSaveArchiveOpened() || IsSaveArchiveErrored()) {
    if (abortOnError) ABORT("cannot save game");
    return false;
  }

  return !!updaterFn(udata);
}


struct DungeonSaverInfo {
  int levelIndex;
};


//==========================================================================
//
//  game::DungeonSaveFn
//
//==========================================================================
truth game::DungeonSaveFn (void *udata) {
  DungeonSaverInfo *nfo = (DungeonSaverInfo *)udata;
  GetCurrentDungeon()->SaveLevel(nfo->levelIndex, true);
  return true;
}


//==========================================================================
//
//  game::SaveCurrentDungeonLevel
//
//==========================================================================
void game::SaveCurrentDungeonLevel (int levelIndex) {
  IvanAssert(!IsInWilderness());
  DungeonSaverInfo nfo;
  nfo.levelIndex = levelIndex;
  festring skipfile = GetCurrentDungeon()->GetSaveLevelFileName(CurrentLevelIndex);
  const bool needClose = (CurrentSaveOpenCount == 0);
  if (needClose) {
    ConLogf("*** saving dungeon (from scratch)...");
    OpenSaveFileForWrite(true);
  } else {
    ConLogf("*** saving dungeon (to opened save)...");
  }
  RunSaveUpdater(&DungeonSaveFn, &nfo, true);
  if (needClose) {
    CloseSaveFileForWrite(false, true);
  }
}


//==========================================================================
//
//  game::WorldMapSaveFn
//
//==========================================================================
truth game::WorldMapSaveFn (void *udata) {
  outputfile SaveFile(-1, CONST_S("worldmap/worldmap.map"), ivanconfig::GetCompressionLevel());
  SaveFile << WorldMap;
  return true;
}


//==========================================================================
//
//  game::SaveWorldMap
//
//  save archive should be opened
//
//==========================================================================
void game::SaveWorldMap (truth DeleteAfterwards) {
  const bool needClose = (CurrentSaveOpenCount == 0);
  if (needClose) {
    ConLogf("*** saving worldmap (from scratch)...");
    OpenSaveFileForWrite(true);
  } else {
    ConLogf("*** saving worldmap (to opened save)...");
  }
  RunSaveUpdater(&WorldMapSaveFn, NULL, true);
  if (needClose) {
    CloseSaveFileForWrite(false, true);
  }
}


//==========================================================================
//
//  game::LoadWorldMap
//
//==========================================================================
worldmap *game::LoadWorldMap () {
  if (!WorldMap) {
    OpenCurrentSave();
    {
      inputfile SaveFile(-1, CONST_S("worldmap/worldmap.map"));
      SaveFile >> WorldMap;
    }
    CloseCurrentSave();
  }
  return WorldMap;
}


//==========================================================================
//
//  xappend
//
//==========================================================================
static void xappend (festring &dest, cfestring &s) {
  for (festring::sizetype c = 0; c < s.GetSize(); ++c) {
    char ch = s[c];
    if (ch <= ' ' || !isalnum(ch)) ch = '_';
    dest << ch;
  }
}


//==========================================================================
//
//  game::SaveName
//
//==========================================================================
festring game::SaveName (cfestring &Base) {
  festring SaveName = GetSavePath();
  if (Base.IsEmpty()) xappend(SaveName, PlayerName); else xappend(SaveName, Base);
  return SaveName;
}


//==========================================================================
//
//  game::GetMoveCommandKeyBetweenPoints
//
//==========================================================================
int game::GetMoveCommandKeyBetweenPoints (v2 A, v2 B) {
  for (int c = 0; c < EXTENDED_DIRECTION_COMMAND_KEYS; c += 1) {
    if ((A + GetMoveVector(c)) == B) {
      //return GetMoveCommandKey(c);
      const int res = commandsystem::GetMoveCommandKey(c);
      if (res != 0) return res;
      return DIR_ERROR;
    }
  }
  return DIR_ERROR;
}


//==========================================================================
//
//  game::ApplyDivineTick
//
//==========================================================================
void game::ApplyDivineTick () {
  for (int c = 1; c <= GODS; ++c) {
    GetGod(c)->ApplyDivineTick();
  }
}


//==========================================================================
//
//  game::ApplyDivineAlignmentBonuses
//
//==========================================================================
void game::ApplyDivineAlignmentBonuses (god *CompareTarget, int Multiplier, truth Good) {
  for (int c = 1; c <= GODS; ++c) {
    if (GetGod(c) != CompareTarget) {
      GetGod(c)->AdjustRelation(CompareTarget, Multiplier, Good);
    }
  }
}


//==========================================================================
//
//  game::GetDirectionVectorForKey
//
//==========================================================================
v2 game::GetDirectionVectorForKey (int Key) {
  //if (MoveKeyToDir(Key) == 8) return ZERO_V2;
  //if (Key == KEY_NUMPAD_5 || Key == '.') return ZERO_V2; /* k8: '.' */
  /*
  for (int c = 0; c < EXTENDED_DIRECTION_COMMAND_KEYS; c += 1) {
    if (Key == GetMoveCommandKey(c)) {
      return GetMoveVector(c);
    }
  }
  */
  const int Dir = MoveKeyToDir(Key);
  if (Dir >= 0 && Dir < EXTENDED_DIRECTION_COMMAND_KEYS) {
    return GetMoveVector(Dir);
  }
  return ERROR_V2;
}


//==========================================================================
//
//  game::GetMinDifficulty
//
//==========================================================================
double game::GetMinDifficulty () {
  double Base = CurrentLevel->GetDifficulty() * 0.2;
  sLong MultiplierExponent = 0;
  ivantime Time;
  GetTime(Time);
  int Modifier = Time.Day - DANGER_PLUS_DAY_MIN;
  if (Modifier > 0) Base += DANGER_PLUS_MULTIPLIER * Modifier;
  for (;;) {
    int Dice = RAND_N(25);
    if (Dice < 5 && MultiplierExponent > -3) {
      Base /= 3;
      --MultiplierExponent;
      continue;
    }
    if (Dice >= 20 && MultiplierExponent < 3) {
      Base *= 3;
      ++MultiplierExponent;
      continue;
    }
    return Base;
  }
}


//==========================================================================
//
//  game::ShowLevelMessage
//
//==========================================================================
void game::ShowLevelMessage () {
  if (CurrentLevel->GetLevelMessage().GetSize()) {
    ADD_MESSAGE("%s", CurrentLevel->GetLevelMessage().CStr());
  }
  CurrentLevel->SetLevelMessage(festring::EmptyStr());
}


//==========================================================================
//
//  game::DirectionQuestion
//
//==========================================================================
int game::DirectionQuestion (cfestring &RawTopic, truth RequireAnswer, truth AcceptYourself) {
  festring Topic = RawTopic;
  Topic << "\n\1#cc0|[press a direction key";
  if (AcceptYourself) {
    festring kn = commandsystem::GetSelfDirKey();
    if (!kn.IsEmpty()) {
      Topic << " or \1G" << kn << "\1#cc0|";
    }
  }
  Topic << "]\2";
  for (;;) {
    int Key = AskForKeyPress(Topic);
    /*
    if (AcceptYourself) {
      //if (Key == '.' || Key == KEY_NUMPAD_5) return YOURSELF; //k8
      if (MoveKeyToDir(Key) == 8) return YOURSELF;
    }
    */
    /*
    for (int c = 0; c < DIRECTION_COMMAND_KEYS; c += 1) {
      if (Key == GetMoveCommandKey(c)) {
        return c;
      }
    }
    */
    const int Dir = MoveKeyToDir(Key);
    if (Dir >= 0 && Dir < DIRECTION_COMMAND_KEYS) return Dir;
    if (AcceptYourself && Dir == 8) return YOURSELF;
    if (!RequireAnswer) return DIR_ERROR;
  }
}


//==========================================================================
//
//  game::RemoveSaves
//
//==========================================================================
void game::RemoveSaves () {
  festring svname;
  svname << SaveName() << ".save";
  remove(svname.CStr());
}


//==========================================================================
//
//  game::SetPlayer
//
//==========================================================================
void game::SetPlayer (character *NP) {
  Player = NP;
  if (Player) Player->AddFlags(C_PLAYER);
}


//==========================================================================
//
//  game::InitDungeons
//
//==========================================================================
void game::InitDungeons () {
  Dungeons = GetGameScript()->GetDungeonCount();
  IvanAssert(Dungeons > 0);
  //ConLogf("dungeon count: %d", Dungeons);
  Dungeon = new dungeon *[Dungeons];
  Dungeon[0] = 0;
  for (int c = 1; c < Dungeons; ++c) {
    Dungeon[c] = new dungeon(c);
    Dungeon[c]->SetIndex(c);
  }
}


//==========================================================================
//
//  game::DoEvilDeed
//
//==========================================================================
void game::DoEvilDeed (int Amount) {
  if (!Amount) return;
  for (int c = 1; c <= GODS; ++c) {
    int Change = Amount-Amount*GetGod(c)->GetAlignment()/5;
    if (!IsInWilderness() && Player->GetLSquareUnder()->GetDivineMaster() == c) {
      if (GetGod(c)->GetRelation()-(Change << 1) < -750) {
        if (GetGod(c)->GetRelation() > -750) GetGod(c)->SetRelation(-750);
      } else if (GetGod(c)->GetRelation()-(Change << 1) > 750) {
        if (GetGod(c)->GetRelation() < 750) GetGod(c)->SetRelation(750);
      } else GetGod(c)->SetRelation(GetGod(c)->GetRelation()-(Change << 1));
    } else {
      if(GetGod(c)->GetRelation()-Change < -500) {
        if (GetGod(c)->GetRelation() > -500) GetGod(c)->SetRelation(-500);
      } else if (GetGod(c)->GetRelation()-Change > 500) {
        if (GetGod(c)->GetRelation() < 500) GetGod(c)->SetRelation(500);
      } else GetGod(c)->SetRelation(GetGod(c)->GetRelation() - Change);
    }
  }
}


//==========================================================================
//
//  game::Hostility
//
//==========================================================================
void game::Hostility (team *Attacker, team *Defender) {
  /* old code
  for (int c = 0; c < Teams; ++c) {
    if (GetTeam(c) != Attacker && GetTeam(c) != Defender &&
        GetTeam(c)->GetRelation(Defender) == FRIEND &&
        c != NEW_ATTNAM_TEAM && c != TOURIST_GUIDE_TEAM) // gum solution
    {
      GetTeam(c)->SetRelation(Attacker, HOSTILE);
    }
  }
  */
  for (int c = 0; c < Teams; ++c) {
    if (GetTeam(c) != Attacker && GetTeam(c) != Defender &&
        GetTeam(c)->IsProtectiveOf(Defender))
    {
      GetTeam(c)->SetRelation(Attacker, HOSTILE);
    }
  }
}


//==========================================================================
//
//  game::CreateTeams
//
//==========================================================================
void game::CreateTeams () {
  Teams = GetGameScript()->GetTeamCount();
  IvanAssert(Teams > 0);
  //ConLogf("team count: %d", Teams);
  Team = new team*[Teams];

  for (int c = 0; c != Teams; c += 1) {
    Team[c] = new team(c);
  }
  for (int c = 0; c != Teams; c += 1) {
    for (int i = 0; i != Teams; i += 1) {
      if (i != c) {
        Team[i]->SetRelation(Team[c], UNCARING);
      } else {
        Team[i]->SetRelation(Team[c], FRIEND);
      }
    }
  }

  const int monsterTeamIdx = MONSTER_TEAM;
  for (int c = 0; c != Teams; c += 1) {
    if (c != monsterTeamIdx) {
      Team[monsterTeamIdx]->SetRelation(Team[c], HOSTILE);
    }
  }

  //const gamescript::teamlist &TeamScript = GetGameScript()->GetTeamList();
  //for (gamescript::teamlist::const_iterator i = TeamScript.begin(); i != TeamScript.end(); ++i) {
  //  team *tm1 = GetTeam(i->first);
  for (int f = 0; f != Teams; f += 1) {
    teamscript *tscr = GetGameScript()->GetTeamScript(f);
    if (tscr) {
      team *tm1 = GetTeam(f);
      if (tm1) {
        for (uInt c = 0; c < tscr->GetRelation().size(); ++c) {
          GetTeam(tscr->GetRelation()[c].first)->SetRelation(tm1, tscr->GetRelation()[c].second);
        }
        for (uInt c = 0; c < tscr->GetProtective().size(); ++c) {
          tm1->SetProtectiveOf(GetTeam(tscr->GetProtective()[c]));
        }
        cint *KillEvilness = tscr->GetKillEvilness();
        if (KillEvilness) tm1->SetKillEvilness(*KillEvilness);
        if (tscr->GetName()) tm1->SetName(*tscr->GetName());
      }
    }
  }
}


//==========================================================================
//
//  game::FindTeam
//
//==========================================================================
team *game::FindTeam (cfestring &name) {
  for (int c = 0; c < Teams; ++c) {
    if (Team[c]->GetName().EquCI(name)) return Team[c];
  }
  return 0;
}


//==========================================================================
//
//  game::StringQuestionEx
//
//==========================================================================
festring game::StringQuestionEx (cfestring &Topic, cfestring &InitStr, col16 Color,
                                 festring::sizetype MinLetters, festring::sizetype MaxLetters,
                                 truth AllowExit, stringkeyhandler KeyHandler, truth *aborted)
{
  v2 scrpos = GetTopAreaTextPos(2);
  DrawEverythingNoBlit();
  EraseTopArea();
  festring Return = InitStr;
  int res = iosystem::StringQuestion(Return, Topic, scrpos, Color,
                                     MinLetters, MaxLetters, false, AllowExit, KeyHandler);
  EraseTopArea();
  if (aborted) *aborted = (res == ABORTED);
  return Return;
}


//==========================================================================
//
//  game::StringQuestion
//
//==========================================================================
festring game::StringQuestion (cfestring &Topic, col16 Color,
                               festring::sizetype MinLetters, festring::sizetype MaxLetters,
                               truth AllowExit, stringkeyhandler KeyHandler, truth *aborted)
{
  return StringQuestionEx(Topic, festring::EmptyStr(), Color, MinLetters, MaxLetters,
                          AllowExit, KeyHandler, aborted);
}


//==========================================================================
//
//  game::NumberQuestion
//
//==========================================================================
sLong game::NumberQuestion (cfestring &Topic, col16 Color, truth ReturnZeroOnEsc,
                            truth *escaped)
{
  v2 scrpos = GetTopAreaTextPos(2);
  DrawEverythingNoBlit();
  EraseTopArea();
  sLong Return = iosystem::NumberQuestion(Topic, scrpos, Color,
                    (ReturnZeroOnEsc ? iosystem::ZERO_ON_ESC : iosystem::DEFAULT), escaped);
  EraseTopArea();
  return Return;
}


//==========================================================================
//
//  game::ScrollBarQuestion
//
//==========================================================================
sLong game::ScrollBarQuestion (cfestring &Topic, sLong BeginValue, sLong Step,
                               sLong Min, sLong Max, sLong AbortValue,
                               col16 TopicColor, col16 Color1, col16 Color2,
                               void (*Handler) (sLong))
{
  v2 scrpos;
  GetTopAreaPosSize(&scrpos, nullptr);
  scrpos.Y = scrpos.Y + 6;
  DrawEverythingNoBlit();
  //igraph::BlitBackGround(v2(16, 6), v2(GetScreenXSize() << 4, 23));
  EraseTopArea();
  sLong Return = iosystem::ScrollBarQuestion(Topic, scrpos/*v2(16, 6)*/, BeginValue, Step, Min, Max,
                                             AbortValue, TopicColor, Color1, Color2,
                                             KEY_LEFT, //GetMoveCommandKey(KEY_LEFT_INDEX),
                                             KEY_RIGHT, //GetMoveCommandKey(KEY_RIGHT_INDEX),
                                             false,
                                             Handler);
  EraseTopArea();
  //igraph::BlitBackGround(v2(16, 6), v2(GetScreenXSize() << 4, 23));
  return Return;
}


//==========================================================================
//
//  game::IncreaseLOSTick
//
//==========================================================================
feuLong game::IncreaseLOSTick () {
  if (LOSTick != 0xFE) return LOSTick += 2;
  CurrentLevel->InitLastSeen();
  return LOSTick = 4;
}


//==========================================================================
//
//  game::UpdateCamera
//
//==========================================================================
void game::UpdateCamera () {
  UpdateCameraX();
  UpdateCameraY();
}


//==========================================================================
//
//  game::HandleQuitMessage
//
//==========================================================================
truth game::HandleQuitMessage () {
  if (IsRunning()) {
    if (InGetPlayerCommand) {
      switch (Menu(0, v2(RES.X >> 1, RES.Y >> 1),
              CONST_S("Do you want to save your game before quitting?\r"),
              CONST_S("Yes\rNo\rCancel\r"), LIGHT_GRAY))
      {
        case 0:
          game::Save(true/*vacuum*/);
          //RemoveSaves(false);
          break;
        case 2:
          GetCurrentArea()->SendNewDrawRequest();
          DrawEverything();
          return false;
        default:
          {
            festring Msg = CONST_S("cowardly quit the game");
            Player->AddQuitedScoreEntry(Msg, 0.75);
            End(Msg, true, false);
            break;
          }
      }
    } else if (!Menu(0, v2(RES.X >> 1, RES.Y >> 1),
                     CONST_S("You can't save at this point. Are you sure you still want to do this?\r"),
                     CONST_S("Yes\rNo\r"), LIGHT_GRAY))
    {
      RemoveSaves();
    } else {
      GetCurrentArea()->SendNewDrawRequest();
      DrawEverything();
      return false;
    }
  }
  return true;
}


//==========================================================================
//
//  game::GetDirectionForVector
//
//==========================================================================
int game::GetDirectionForVector (v2 Vector) {
  for (int c = 0; c < DIRECTION_COMMAND_KEYS; ++c) {
    if (Vector == GetMoveVector(c)) return c;
  }
  return DIR_ERROR;
}


//==========================================================================
//
//  GetPlayerAlignmentSum
//
//==========================================================================
int game::GetPlayerAlignmentSum () {
  sLong Sum = 0;
  for (int c = 1; c <= GODS; ++c) {
    if (GetGod(c)->GetRelation() > 0) {
      Sum += GetGod(c)->GetRelation() * (5 - GetGod(c)->GetAlignment());
    }
  }
  return Sum;
}


//==========================================================================
//
//  game::GetDirectionForVector
//
//==========================================================================
int game::GetPlayerAlignment () {
  const sLong Sum = GetPlayerAlignmentSum();
  if (Sum >  15000) return  4;
  if (Sum >  10000) return  3;
  if (Sum >   5000) return  2;
  if (Sum >   1000) return  1;
  if (Sum >  -1000) return  0;
  if (Sum >  -5000) return -1;
  if (Sum > -10000) return -2;
  if (Sum > -15000) return -3;
  return -4;
}


//==========================================================================
//
//  game::GetVerbalPlayerAlignment
//
//==========================================================================
cchar *game::GetVerbalPlayerAlignment () {
  switch (GetPlayerAlignment()) {
    case  4: return "extremely lawful";
    case  3: return "very lawful";
    case  2: return "lawful";
    case  1: return "mildly lawful";
    case  0: return "neutral";
    case -1: return "mildly chaotic";
    case -2: return "chaotic";
    case -3: return "very chaotic";
    case -4: return "extremely chaotic";
  }
  ABORT("wut alignment?!");
  return "WUT?!";
}


//==========================================================================
//
//  game::CreateGods
//
//==========================================================================
void game::CreateGods () {
  const char * godconsts[16] = {
    "", // unused
    "VALPURUS",
    "LEGIFER",
    "ATAVUS",
    "DULCIS",
    "SEGES",
    "SOPHOS",
    "SILVA",
    "LORICATUS",
    "MELLIS",
    "CLEPTIA",
    "NEFAS",
    "SCABIES",
    "INFUSCOR",
    "CRUENTUS",
    "MORTIFER",
  };
  God = new god*[GODS+1];
  God[0] = 0;
  for (int c = 1; c < protocontainer<god>::GetSize(); ++c) {
    auto proto = protocontainer<god>::GetProto(c);
    if (!proto) ABORT("God #%d is not defined!", c);
    if (c > GODS) ABORT("Too many gods!");
    God[c] = proto->Spawn();
    if (game::GetGlobalConst(CONST_S(godconsts[c])) != God[c]->GetType()) {
      ABORT("God number %d must be '%s', but it is '%s'!", c, godconsts[c], God[c]->GetTypeID());
    }
  }
}


//==========================================================================
//
//  game::BusyAnimation
//
//==========================================================================
void game::BusyAnimation () {
  BusyAnimation(DOUBLE_BUFFER, false);
}


//==========================================================================
//
//  game::BusyAnimation
//
//==========================================================================
void game::BusyAnimation (bitmap *Buffer, truth ForceDraw) {
  static clock_t LastTime = 0;
  static int Frame = 0;

  static blitdata B1 = {
    Bitmap:0,
    Src:v2(0, 0),
    Dest:v2(0, 0),
    Border:v2(RES.X, RES.Y),
    {Flags:0},
    MaskColor:0,
    CustomData:0
  };

  static blitdata B2 = {
    Bitmap:0,
    Src:v2(0, 0),
    Dest:v2((RES.X >> 1) - 100, (RES.Y << 1) / 3 - 100),
    Border:v2(200, 200),
    {Flags:0},
    MaskColor:0,
    CustomData:0
  };

  if (ForceDraw || clock()-LastTime > CLOCKS_PER_SEC/25) {
    B1.Border = v2(RES.X, RES.Y);

    B2.Bitmap = Buffer;
    B2.Dest.X = (RES.X>>1)-100+EnterTextDisplacement.X;
    B2.Dest.Y = (RES.Y<<1)/3-100+EnterTextDisplacement.Y;
    if (EnterImage) {
      B1.Bitmap = Buffer;
      EnterImage->NormalMaskedBlit(B1);
    }
    BusyAnimationCache[Frame]->NormalBlit(B2);

    const int msgY = Buffer->GetSize().Y - FONT->FontHeight() - 2;
    if (!mBusyMessage.IsEmpty()) {
      if (mBusyMessageTime == 0) mBusyMessageTime = time(NULL);
      if (time(NULL) - mBusyMessageTime > 10) {
        mBusyMessage.Empty();
      } else {
        Buffer->Fill(0, msgY - 4, Buffer->GetSize().X, FONT->FontHeight() + 4, 0);
        FONT->PrintStr(Buffer, v2(2, msgY - 2), MakeRGB16(192, 192, 0), mBusyMessage);
      }
    } else if (mBusyMessageTime != 0) {
      mBusyMessageTime = 0;
      Buffer->Fill(0, msgY - 4, Buffer->GetSize().X, FONT->FontHeight() + 4, 0);
    }

    if (Buffer == DOUBLE_BUFFER) {
      graphics::BlitDBToScreen();
    }

    if (++Frame == 32) Frame = 0;
    LastTime = clock();
  }
}


//==========================================================================
//
//  game::CreateBusyAnimationCache
//
//==========================================================================
void game::CreateBusyAnimationCache () {
  bitmap Elpuri(TILE_V2, TRANSPARENT_COLOR);
  Elpuri.ActivateFastFlag();
  packcol16 Color = MakeRGB16(60, 60, 60);
  igraph::GetCharacterRawGraphic()->MaskedBlit(&Elpuri, v2(64, 0), ZERO_V2, TILE_V2, &Color);
  bitmap Circle(v2(200, 200), TRANSPARENT_COLOR);
  Circle.ActivateFastFlag();
  for (int x = 0; x < 4; ++x) Circle.DrawPolygon(100, 100, 95+x, 50, MakeRGB16(255-12*x, 0, 0));
  blitdata B1 = {
    0,
    { 0, 0 },
    { 92, 92 },
    { TILE_SIZE, TILE_SIZE },
    { 0 },
    TRANSPARENT_COLOR,
    0
  };
  blitdata B2 = {
    0,
    { 0, 0 },
    { 0, 0 },
    { 200, 200 },
    { 0 },
    TRANSPARENT_COLOR,
    0
  };
  for (int c = 0; c < 32; ++c) {
    B1.Bitmap = B2.Bitmap = BusyAnimationCache[c] = new bitmap(v2(200, 200), 0);
    B1.Bitmap->ActivateFastFlag();
    Elpuri.NormalMaskedBlit(B1);
    double Rotation = 0.3+c*FPI/80;
    for (int x = 0; x < 10; ++x) B1.Bitmap->DrawPolygon(100, 100, 95, 5, MakeRGB16(5+25*x, 0, 0), false, true, Rotation+double(x)/50);
    Circle.NormalMaskedBlit(B2);
  }
}


//==========================================================================
//
//  game::AskForKeyPress
//
//==========================================================================
int game::AskForKeyPress (cfestring &Topic) {
  v2 scrsize;
  GetTopAreaPosSize(nullptr, &scrsize);
  festring ss(Topic);
  ss.Capitalize();
  std::vector<festring> text;
  FONT->WordWrap(ss, text, scrsize.X - 8);
  v2 scrpos = GetTopAreaTextPos((int)text.size());
  DrawEverythingNoBlit();
  EraseTopArea();
  //FONT->PrintStr(DOUBLE_BUFFER, scrpos, WHITE, ss);
  v2 curpos;
  for (size_t f = 0; f != text.size(); f += 1) {
    FONT->PrintStr(DOUBLE_BUFFER, scrpos, WHITE, text[f]);
    curpos = scrpos + v2(FONT->TextWidth(text[f]) + 3, 0);
    scrpos.Y += 10;
  }
  //const int twdt = FONT->TextWidth(ss);
  auto cursave = graphics::GetCursorState();
  graphics::GotoXY(curpos);
  graphics::RectCursor();
  graphics::ShowCursor();
  graphics::BlitDBToScreen();
  int Key = GET_KEY();
  EraseTopArea();
  return Key;
}


//==========================================================================
//
//  game::AskForSpacePress
//
//==========================================================================
int game::AskForSpacePress (cfestring &Topic) {
  v2 scrpos = GetTopAreaTextPos(1);
  DrawEverythingNoBlit();
  EraseTopArea();
  festring ss(Topic);
  ss.Capitalize(); ss << " [press \1YSpace\2]";
  FONT->PrintStr(DOUBLE_BUFFER, scrpos, WHITE, ss);
  const int twdt = FONT->TextWidth(ss);
  auto cursave = graphics::GetCursorState();
  graphics::GotoXY(scrpos.X + twdt + 3, scrpos.Y);
  graphics::RectCursor();
  graphics::ShowCursor();
  graphics::BlitDBToScreen();
  //int Key = GET_KEY();
  int Key;
  do { Key = GET_KEY(); } while (Key != KEY_SPACE);
  EraseTopArea();
  return Key;
}


//==========================================================================
//
//  game::AskForEscPress
//
//==========================================================================
void game::AskForEscPress (cfestring &Topic) {
  DrawEverythingNoBlit();
  EraseTopArea();
  festring ss(Topic);
  ss.Capitalize(); ss << " [press" << NBSP << "\1RESC\2]";
  v2 scrsize;
  GetTopAreaPosSize(nullptr, &scrsize);
  std::vector<festring> text;
  FONT->WordWrap(ss, text, scrsize.X);
  v2 scrpos = GetTopAreaTextPos((int)text.size());
  v2 curpos;
  for (size_t f = 0; f != text.size(); f += 1) {
    FONT->PrintStr(DOUBLE_BUFFER, scrpos, MakeRGB16(255, 50, 50), text[f]);
    curpos = scrpos + v2(FONT->TextWidth(text[f]) + 3, 0);
    scrpos.Y += 10;
  }
  auto cursave = graphics::GetCursorState();
  graphics::GotoXY(curpos.X, curpos.Y);
  graphics::RectCursor();
  graphics::ShowCursor();
  graphics::BlitDBToScreen();
  int Key;
  do { Key = GET_KEY(); } while (Key != KEY_ESC);
  EraseTopArea();
}


//==========================================================================
//
//  game::PositionQuestion
//
//  Handler is called when the key has been identified as a movement key
//  KeyHandler is called when the key has NOT been identified as a movement key
//  Both can be deactivated by passing 0 as parameter
//
//==========================================================================
v2 game::PositionQuestion (cfestring &Topic, v2 CursorPos, void (*Handler)(v2),
                           positionkeyhandler KeyHandler, truth Zoom)
{
  int Key = 0;
  SetDoZoom(Zoom);
  v2 Return;
  CursorData = RED_CURSOR;
  auto stpos = CursorPos;
  if (Handler) Handler(CursorPos);
  for (;;) {
    square *Square = GetCurrentArea()->GetSquare(CursorPos);
    if (!Square->HasBeenSeen() &&
        (!Square->GetCharacter() || !Square->GetCharacter()->CanBeSeenByPlayer()) &&
        !GetSeeWholeMapCheatMode())
    {
      DOUBLE_BUFFER->Fill(CalculateScreenCoordinates(CursorPos), TILE_V2, BLACK);
    } else {
      GetCurrentArea()->GetSquare(CursorPos)->SendStrongNewDrawRequest();
    }

    //if (Key == ' ' || Key == '.' || Key == KEY_NUMPAD_5) { Return = CursorPos; break; }
    if (Key == KEY_SPACE) { Return = CursorPos; break; }
    if (Key == KEY_ESC) { Return = ERROR_V2; break; }

    /*
    v2 DirectionVector = GetDirectionVectorForKey(Key);
    if (DirectionVector != ERROR_V2) {
      if (globalwindowhandler::lastCtrl &&
          !globalwindowhandler::lastAlt &&
          !globalwindowhandler::lastShift)
      {
        CursorPos += DirectionVector * 6;
      } else {
        CursorPos += DirectionVector;
      }
      */
    int Dir = MoveKeyToDir(Key, /*allowFastExt*/true);
    if (Dir >= 0 && (Dir % 100) < DIRECTION_COMMAND_KEYS) {
      v2 dv = GetMoveVector(Dir % 100);
      if (Dir >= 100) dv *= 6;
      CursorPos += dv;
      CursorPos.X = (CursorPos.X + GetCurrentArea()->GetXSize() * 6) % GetCurrentArea()->GetXSize();
      CursorPos.Y = (CursorPos.Y + GetCurrentArea()->GetYSize() * 6) % GetCurrentArea()->GetYSize();
      if (Handler) Handler(CursorPos);
    } if (Dir >= 0 && (Dir % 100) == 8) {
      Return = CursorPos;
      break;
    } else if (KeyHandler) {
      CursorPos = KeyHandler(CursorPos, Key);
      if (CursorPos == ERROR_V2 || CursorPos == ABORT_V2) {
        Return = CursorPos;
        break;
      }
      // return back to start
      if (KEY_EQU(Key, "S-2")) {
        if (CursorPos != stpos) {
          CursorPos = stpos;
          if (Handler) Handler(CursorPos);
        }
      }
    }

    if (ivanconfig::GetAutoCenterMapOnLook()) {
      UpdateCameraX(CursorPos.X);
      UpdateCameraY(CursorPos.Y);
    } else {
      if (CursorPos.X < GetCamera().X+3 || CursorPos.X >= GetCamera().X+GetScreenXSize()-3) UpdateCameraX(CursorPos.X);
      if (CursorPos.Y < GetCamera().Y+3 || CursorPos.Y >= GetCamera().Y+GetScreenYSize()-3) UpdateCameraY(CursorPos.Y);
    }

    FONT->PrintStr(DOUBLE_BUFFER, v2(16, 8), WHITE, Topic);
    SetCursorPos(CursorPos);
    DrawEverything();
    Key = GET_KEY();
  }

  igraph::BlitBackGround(v2(16, 6), v2(GetScreenXSize()<<4, 23));
  igraph::BlitBackGround(v2(RES.X-96, RES.Y-96), v2(80, 80));
  SetDoZoom(false);
  SetCursorPos(v2(-1, -1));
  return Return;
}


//==========================================================================
//
//  game::LookHandler
//
//==========================================================================
void game::LookHandler (v2 CursorPos) {
  square *Square = GetCurrentArea()->GetSquare(CursorPos);
  festring OldMemory;

  msgsystem::DisableSounds();

  if (GetSeeWholeMapCheatMode()) {
    OldMemory = Square->GetMemorizedDescription();
    if (IsInWilderness()) {
      GetWorldMap()->GetWSquare(CursorPos)->UpdateMemorizedDescription(true);
    } else {
      GetCurrentLevel()->GetLSquare(CursorPos)->UpdateMemorizedDescription(true);
    }
  }

  festring Msg;
  if (Square->HasBeenSeen() || GetSeeWholeMapCheatMode()) {
    if (!IsInWilderness() && !Square->CanBeSeenByPlayer() &&
        GetCurrentLevel()->GetLSquare(CursorPos)->CanBeFeltByPlayer())
    {
      Msg = CONST_S("You feel here ");
    } else if (Square->CanBeSeenByPlayer(true) || GetSeeWholeMapCheatMode()) {
      Msg = CONST_S("You see here ");
    } else {
      Msg = CONST_S("You remember here ");
    }
    Msg << Square->GetMemorizedDescription() << '.';
    if (!IsInWilderness() && (Square->CanBeSeenByPlayer() || GetSeeWholeMapCheatMode())) {
      lsquare *LSquare = GetCurrentLevel()->GetLSquare(CursorPos);
      LSquare->DisplaySmokeInfo(Msg);
      if (LSquare->HasEngravings() && LSquare->IsTransparent()) {
        if (LSquare->EngravingsCanBeReadByPlayer() || GetSeeWholeMapCheatMode()) {
          LSquare->DisplayEngravedInfo(Msg);
        } else {
          Msg << " Something has been engraved here.";
        }
      }
    }
  } else {
    Msg = CONST_S("You have never been here.");
  }

  character *Character = Square->GetCharacter();
  if (Character && (Character->CanBeSeenByPlayer() || GetSeeWholeMapCheatMode())) {
    Character->DisplayInfo(Msg);
  }

  if (!RAND_N(10000) && (Square->CanBeSeenByPlayer() || GetSeeWholeMapCheatMode())) {
    Msg << " You see here a frog eating a magnolia.";
  }
  ADD_MESSAGE("%s", Msg.CStr());

  if (GetSeeWholeMapCheatMode()) {
    Square->SetMemorizedDescription(OldMemory);
  }

  msgsystem::EnableSounds();
}


//==========================================================================
//
//  game::AnimationController
//
//==========================================================================
truth game::AnimationController () {
  DrawEverythingNoBlit(true);
  return true;
}


//==========================================================================
//
//  game::LoadGlobalValueMap
//
//==========================================================================
void game::LoadGlobalValueMap (TextInput &fl, bool allowRedefine) {
  festring word;
  fl.SetGetVarCB(&game::ldrGetVar);
  while (fl.ReadWord(word, false)) {
    if (word == "Include") {
      word = fl.ReadWord();
      if (fl.ReadWord() != ";") fl.Error("Invalid terminator");
      //ConLogf("loading: %s", word.CStr());
      TextInputFile incf(inputfile::buildIncludeName(fl.GetFileName(), word), &game::GetGlobalValueMap());
      LoadGlobalValueMap(incf, allowRedefine);
      continue;
    }

    if (word == "Message") {
      word = fl.ReadWord();
      if (fl.ReadWord() != ";") fl.Error("Invalid terminator");
      ConLogf("MESSAGE: %s", word.CStr());
      continue;
    }

    #if 0
    ConLogf("*** <%s> (%s : %d)", word.CStr(), fl.GetFileName().CStr(), fl.TokenLine());
    #endif
    // the same as "#define"
    if (word == "CONST" || word == "VAR") {
      const bool isvar = (word == "VAR");
      word = fl.ReadWord();
      if (word.GetSize() == 0) fl.Error("The thing that should not be");
      if (!allowRedefine && HasGlobalValue(word) && !IsMutableGlobal(word)) {
        fl.Error("duplicate constant '%s'", word.CStr());
      }
      if (fl.ReadWord() != "=") fl.Error("`=` expected");
      sLong v = fl.ReadNumberPreserveTerm();
      GlobalValueMap[word] = v;
      if (isvar) MakeGlobalMutable(word);
      if (fl.ReadWord() != ";") fl.Error("`;` expected");
      continue;
    }

    if (word != "#") fl.Error("Illegal datafile define");
    fl.ReadWord(word, true);

    if (word == "enum" || word == "bitenum") {
      const bool isBit = (word == "bitenum");
      sLong idx = 0;
      fl.ReadWord(word, true);
      // check for named enum
      festring enumName;
      //if (enumName.GetSize()) fl.Error("The thing that should not be");
      if (word == "#") {
        // get enum name
        fl.ReadWord(word, true);
        enumName = (isBit ? "# bitenum # " : "# enum # ");
        enumName << word;
        // get starting index
        idx = game::FindGlobalValue(enumName, 0);
        // get bracket
        fl.ReadWord(word, true);
        #if 0
        ConLogf("named enum '%s'; idx=%d", enumName.CStr(), idx);
        #endif
      }
      if (word != "{") fl.Error("'{' expected");
      festring idName;
      truth done = false;
      while (!done) {
        truth forcedIndex = false;
        sLong idxnew = 0;
        truth doAdvance = true;
        // read name
        fl.ReadWord(word, true);
        if (word == "}") break;
        if (word == "=" || word == ":=") {
          // nameless
          idName.Empty();
        } else {
          idName = word;
          fl.ReadWord(word, true);
        }
        if (word == "=") {
          // set current index
          idxnew = fl.ReadNumber();
          forcedIndex = true;
          doAdvance = true; // just in case
          //ConLogf("force index for `%s`(%s): %d (idx=%d)", idName.CStr(), (idName.GetSize() == 0 ? "empty" : "non-empty"), idxnew, idx);
        } else if (word == ":=") {
          idxnew = fl.ReadNumber();
          forcedIndex = true;
          doAdvance = false;
          //ConLogf("enum; %s := %d!", idName.CStr(), idxnew);
        } else {
          if (word != "," && word != ";" && word != "}") fl.Error("',' expected");
          if (word == "}") done = true;
          forcedIndex = false;
          doAdvance = true; // just in case
        }
        if (!allowRedefine && idName.GetSize() && HasGlobalValue(idName)) {
          fl.Error("duplicate global '%s'", idName.CStr());
        }
        if (forcedIndex) {
          if (isBit) {
            // for bitsets, nameless "=" forces new bit index, otherwise index isn't changed, and taken as is (without shifts)
            if (idName.GetSize()) {
              //GlobalValueMap.insert(std::make_pair(idName, idxnew));
              GlobalValueMap[idName] = idxnew;
              doAdvance = false;
            }
          } else {
            // for enums, index is always forced, but not increased for nameless "="
            if (idName.GetSize()) {
              //GlobalValueMap.insert(std::make_pair(idName, idxnew));
              GlobalValueMap[idName] = idxnew;
              if (doAdvance) ++idxnew;
            }
          }
          if (doAdvance) idx = idxnew;
        } else {
          // no forced index
          if (idName.GetSize() == 0) fl.Error("The thing that should not be");
          if (!doAdvance) fl.Error("The thing that should not be");
          sLong i = idx;
          if (isBit) {
            if (i < 0 || i > 31) fl.Error("bitenum overflow");
            i = 1<<i;
          }
          //GlobalValueMap.insert(std::make_pair(idName, i));
          GlobalValueMap[idName] = i;
          if (doAdvance) ++idx; // advance index
        }
      }
      fl.SkipBlanks();
      int ch = fl.GetChar();
      if (ch != EOF && ch != ';') fl.UngetChar(ch);
      //if (fl.ReadWord() != ";") ABORT("';' expected in file %s at line %d!", fl.GetFileName().CStr(), fl.TokenLine());
      // save current enum index, so we can continue later
      if (enumName.GetSize()) {
        #if 0
        ConLogf("named enum '%s' ends with idx=%d", enumName.CStr(), idx);
        #endif
        //GlobalValueMap.insert(std::make_pair(enumName, idx));
        GlobalValueMap[enumName] = idx; // 'cause `insert()` will not replace the element
      }
      continue;
    }

    if (word == "define") {
      fl.ReadWord(word);
      if (word.GetSize() == 0) fl.Error("The thing that should not be");
      if (!allowRedefine && HasGlobalValue(word)) fl.Error("duplicate constant '%s'", word.CStr());
      sLong v = fl.ReadNumber();
      //GlobalValueMap.insert(std::make_pair(word, v));
      GlobalValueMap[word] = v;
      continue;
    }

    fl.Error("Illegal datafile define");
  }
}


//==========================================================================
//
//  game::HasGlobalValue
//
//==========================================================================
truth game::HasGlobalValue (cfestring &name) {
  auto it = GlobalValueMap.find(name);
  return (it != GlobalValueMap.end());
}


//==========================================================================
//
//  game::FindGlobalValue
//
//==========================================================================
sLong game::FindGlobalValue (cfestring &name, sLong defval, truth* found) {
  auto it = GlobalValueMap.find(name);
  if (it != GlobalValueMap.end()) {
    if (found) *found = true;
    return it->second;
  } else {
    if (found) *found = false;
    return defval;
  }
}


//==========================================================================
//
//  game::FindGlobalValue
//
//==========================================================================
sLong game::FindGlobalValue (cfestring &name, truth* found) {
  return FindGlobalValue(name, -1, found);
}


//==========================================================================
//
//  game::IsMutableGlobal
//
//==========================================================================
truth game::IsMutableGlobal (cfestring &name) {
  auto it = GlobalMutableMap.find(name);
  return (it != GlobalMutableMap.end());
}


//==========================================================================
//
//  game::MakeGlobalMutable
//
//==========================================================================
void game::MakeGlobalMutable (cfestring &name) {
  if (!name.IsEmpty()) {
    GlobalMutableMap.insert(name);
  }
}


//==========================================================================
//
//  game::SetGlobalValue
//
//==========================================================================
truth game::SetGlobalValue (cfestring &name, sLong value) {
  if (IsMutableGlobal(name)) {
    GlobalValueMap[name] = value;
    return true;
  } else {
    return false;
  }
}


//==========================================================================
//
//  game::GetGlobalConst
//
//  this will fail if there is no such constant
//  TODO: cache values
//
//==========================================================================
sLong game::GetGlobalConst (cfestring &name) {
  auto it = GlobalValueMap.find(name);
  if (it == GlobalValueMap.end()) {
    ABORT("Global constant '%s' not found!", name.CStr());
  }
  return it->second;
}


//==========================================================================
//
//  game::GetModuleList
//
//==========================================================================
const std::vector<festring> &game::GetModuleList () {
  return mModuleList;
}


//==========================================================================
//
//  game::SaveModuleList
//
//==========================================================================
void game::SaveModuleList (outputfile &ofile) {
  ofile << (sLong)mModuleList.size();
  for (auto &modname : mModuleList) ofile << modname;
}


//==========================================================================
//
//  game::LoadAndCheckModuleList
//
//  false: incomaptible
//
//==========================================================================
truth game::LoadAndCheckModuleList (inputfile &ifile) {
  sLong modcount;
  ifile >> modcount;
  if (modcount != (sLong)mModuleList.size()) return false;
  for (auto &modname : mModuleList) {
    festring svname;
    ifile >> svname;
    if (svname != modname) return false;
  }
  return true;
}


//==========================================================================
//
//  game::LoadModuleList
//
//==========================================================================
void game::LoadModuleList () {
  mModuleList.push_back(CONST_S("maingame")); // always loaded
  TextInputFile ifl(GetGamePath() + "script/module.def", nullptr, false); // don't fail if no file
  if (ifl.IsOpen()) LoadModuleListFile(ifl);
}


//==========================================================================
//
//  game::LoadModuleListFile
//
//==========================================================================
void game::LoadModuleListFile (TextInput &fl) {
  festring word;
  fl.SetGetVarCB(&game::ldrGetVar);
  for (;;) {
    fl.ReadWord(word, false);
    if (fl.Eof()) break; // no more modules
    // messages
    if (word == "Message") {
      word = fl.ReadWord();
      if (fl.ReadWord() != ";") ABORT("Invalid terminator in file %s at line %d!", fl.GetFileName().CStr(), fl.TokenLine());
      ConLogf("MESSAGE: %s", word.CStr());
      continue;
    }
    // includes
    if (word == "Include") {
      word = fl.ReadWord();
      if (fl.ReadWord() != ";") ABORT("Invalid terminator in file %s at line %d!", fl.GetFileName().CStr(), fl.TokenLine());
      //ConLogf("loading: %s", word.CStr());
      TextInputFile ifl(inputfile::buildIncludeName(fl.GetFileName(), word));
      LoadModuleListFile(ifl);
      continue;
    }
    // module
    if (word != "Module") ABORT("`Module` expected in file %s at line %d!", fl.GetFileName().CStr(), fl.TokenLine());
    word = fl.ReadWord();
    if (word.GetSize() == 0) ABORT("Cannot register empty module (file %s at line %d)!", fl.GetFileName().CStr(), fl.TokenLine());
    if (fl.ReadWord() != ";") ABORT("Invalid terminator in file %s at line %d!", fl.GetFileName().CStr(), fl.TokenLine());
    // check for duplicates
    for (auto &modname : mModuleList) {
      if (modname == word) ABORT("Duplicate module '%s' in file %s at line %d!", word.CStr(), fl.GetFileName().CStr(), fl.TokenLine());
    }
    // looks like valid moudle, remember it
    mModuleList.push_back(word);
  }
}


//==========================================================================
//
//  game::InitGlobalValueMap
//
//==========================================================================
void game::InitGlobalValueMap () {
  TextInputFile SaveFile(GetGamePath()+"script/define.def", &GlobalValueMap);
  LoadGlobalValueMap(SaveFile);
  // load defines from modules
  for (auto &modname : mModuleList) {
    festring infname = game::GetGamePath()+"script/"+modname+"/define.def";
    if (inputfile::DataFileExists(infname)) {
      TextInputFile ifl(infname, &game::GetGlobalValueMap());
      LoadGlobalValueMap(ifl);
    }
  }
  // for setting debug things
  {
    festring dfn = ivanconfig::GetMyDir() + "/debug_defines.def";
    if (inputfile::DataFileExists(dfn)) {
      TextInputFile SaveFile2(dfn, &GlobalValueMap);
      LoadGlobalValueMap(SaveFile2, true);
    }
  }
}


//==========================================================================
//
//  game::TextScreen
//
//==========================================================================
void game::TextScreen (cfestring &Text, v2 Displacement, col16 Color, truth GKey,
                       truth Fade, bitmapeditor BitmapEditor)
{
  globalwindowhandler::DisableControlLoops();
  iosystem::TextScreen(Text, Displacement, Color, GKey, Fade, BitmapEditor);
  globalwindowhandler::EnableControlLoops();
}


//==========================================================================
//
//  game::KeyQuestion
//
//  ... all the keys that are acceptable. finish the list with `0`.
//  DefaultAnswer = REQUIRES_ANSWER if this question requires an answer
//
//==========================================================================
int game::KeyQuestion (cfestring &Message, int DefaultAnswer, ...) {
  if (DefaultAnswer >= 'a' && DefaultAnswer <= 'z') {
    DefaultAnswer -= 32;
  }

  DrawEverythingNoBlit();
  EraseTopArea();

  auto cursave = graphics::GetCursorState();
  // two lines?
  festring::sizetype pos = 0;
  while (pos != Message.GetSize() && Message[pos] != '\n') pos += 1;

  if (pos != Message.GetSize()) {
    v2 scrpos = GetTopAreaTextPos(2);
    FONT->PrintStr(DOUBLE_BUFFER, scrpos, WHITE, Message.LeftCopy((int)pos));
    festring pp = Message;
    pp.Erase(0, pos + 1);
    FONT->PrintStr(DOUBLE_BUFFER, scrpos + v2(0, 10), WHITE, pp);
    const int twdt = FONT->TextWidth(pp);
    graphics::GotoXY(scrpos.X + twdt + 3, scrpos.Y + 10);
  } else {
    v2 scrpos = GetTopAreaTextPos(1);
    FONT->PrintStr(DOUBLE_BUFFER, scrpos, WHITE, Message);
    const int twdt = FONT->TextWidth(Message);
    graphics::GotoXY(scrpos.X + twdt + 3, scrpos.Y);
  }

  graphics::RectCursor();
  graphics::ShowCursor();

  int Return = 0;
  while (!Return) {
    const int k = GET_KEY();
    va_list Arguments;
    va_start(Arguments, DefaultAnswer);
    int v = -666;
    do {
      v = va_arg(Arguments, int);
      if (v > 0) {
        if (v >= 'a' && v <= 'z') v -= 32;
        if (v == k) Return = k;
      }
    } while (!Return && v > 0);
    va_end(Arguments);

    if (!Return && DefaultAnswer != REQUIRES_ANSWER) {
      Return = DefaultAnswer;
    }
  }

  //igraph::BlitBackGround(v2(16, 6), v2(GetScreenXSize()<<4, 23));
  EraseTopArea();
  return Return;
}


//==========================================================================
//
//  game::LookKeyHandler
//
//==========================================================================
v2 game::LookKeyHandler (v2 CursorPos, int Key) {
  square *Square = GetCurrentArea()->GetSquare(CursorPos);
  switch (Key) {
    case 'I':
      if (!IsInWilderness()) {
        if (Square->CanBeSeenByPlayer() || CursorPos == Player->GetPos() || GetSeeWholeMapCheatMode()) {
          lsquare *LSquare = GetCurrentLevel()->GetLSquare(CursorPos);
          stack *Stack = LSquare->GetStack();
          if (LSquare->IsTransparent() && Stack->GetVisibleItems(Player)) {
            Stack->DrawContents(Player, CONST_S("Items here"),
                                NO_SELECT|(GetSeeWholeMapCheatMode() ? 0 : NO_SPECIAL_INFO));
          } else {
            ADD_MESSAGE("You see no items here.");
          }
        } else {
          ADD_MESSAGE("You should perhaps move a bit closer.");
        }
      }
      break;
    case 'C':
      if (Square->CanBeSeenByPlayer() || CursorPos == Player->GetPos() ||
           GetSeeWholeMapCheatMode())
      {
        character *Char = Square->GetCharacter();
        if (Char && (Char->CanBeSeenByPlayer() || Char->IsPlayer() || GetSeeWholeMapCheatMode())) {
          Char->PrintInfo();
        } else {
          ADD_MESSAGE("You see no one here.");
        }
      } else {
        ADD_MESSAGE("You should perhaps move a bit closer.");
      }
      break;
  }
  return CursorPos;
}


//==========================================================================
//
//  game::NameKeyHandler
//
//==========================================================================
v2 game::NameKeyHandler (v2 CursorPos, int Key) {
  if (SelectPet(Key)) return LastPetUnderCursor->GetPos();
  if (KEY_EQU(Key, "N")) {
    character *Char = GetCurrentArea()->GetSquare(CursorPos)->GetCharacter();
    if (Char && Char->CanBeSeenByPlayer()) Char->TryToName();
    else ADD_MESSAGE("You don't see anyone here to name.");
  }
  return CursorPos;
}


//==========================================================================
//
//  game::AbortGame
//
//==========================================================================
void game::AbortGame () {
  ConLogf("abortint the game...");
  pool::AbortBe();
  globalwindowhandler::DeInstallControlLoop(AnimationController);
  SetIsRunning(false);
  /* This prevents monster movement etc. after death. */
  throw quitrequest();
}


//==========================================================================
//
//  game::End
//
//==========================================================================
void game::End (festring DeathMessage, truth Permanently, truth AndGoToMenu) {
  #if 0
  // the caller is responsible for saving
  if (!Permanently /*&& WizardModeIsReallyActive()*/) {
    // jumping out of the world causes segfault in save
    if (!wizardquit) {
      game::Save(true/*vacuum*/);
    }
  }
  #endif
  pool::AbortBe();
  globalwindowhandler::DeInstallControlLoop(AnimationController);
  SetIsRunning(false);
  if (IsSaveArchiveOpened()) CloseSaveArchive();
  if (Permanently) {
    RemoveSaves();
  }
  if (Permanently && !WizardModeIsReallyActive() && !IsCheating()) {
    if (highscore::LastAddFailed()) {
      iosystem::TextScreen(CONST_S("You didn't manage to get onto the high score list.\n\n\n\n")+
                           GetPlayerName()+", "+DeathMessage+"\nRIP");
    } else {
      highscore::Draw(ivanconfig::GetSaveQuitScore());
    }
  }
  if (AndGoToMenu) {
    /* This prevents monster movement etc. after death. */
    throw quitrequest();
  }
}


//==========================================================================
//
//  game::CalculateRoughDirection
//
//==========================================================================
int game::CalculateRoughDirection (v2 Vector) {
  if (!Vector.X && !Vector.Y) return YOURSELF;
  double Angle = femath::CalculateAngle(Vector);
  if (Angle < FPI / 8) return 4;
  else if (Angle < 3*FPI/8) return 7;
  else if (Angle < 5*FPI/8) return 6;
  else if (Angle < 7*FPI/8) return 5;
  else if (Angle < 9*FPI/8) return 3;
  else if (Angle < 11*FPI/8) return 0;
  else if (Angle < 13*FPI/8) return 1;
  else if (Angle < 15*FPI/8) return 2;
  else return 4;
}


//==========================================================================
//
//  game::Menu
//
//==========================================================================
int game::Menu (bitmap *BackGround, v2 Pos, cfestring &Topic, cfestring &sMS,
                col16 Color, cfestring &SmallText1, cfestring &SmallText2)
{
  globalwindowhandler::DisableControlLoops();
  int Return = iosystem::Menu(BackGround, Pos, Topic, sMS, Color, SmallText1, SmallText2);
  globalwindowhandler::EnableControlLoops();
  return Return;
}


//==========================================================================
//
//  game::InitDangerMap
//
//==========================================================================
void game::InitDangerMap () {
  truth First = true;
  pool::RegisterState(false);
  //ConLogf("game::InitDangerMap(): START");
  for (int protoIdx = 1; protoIdx < protocontainer<character>::GetSize(); ++protoIdx) {
    BusyAnimation();
    const character::prototype *Proto = protocontainer<character>::GetProto(protoIdx);
    if (!Proto) continue; // missing character
    const character::database *const *ConfigData = Proto->GetConfigData();
    const int ConfigSize = Proto->GetConfigSize();
    for (int configIdx = 0; configIdx < ConfigSize; ++configIdx) {
      if (!ConfigData[configIdx]->IsAbstract) {
        const int Config = ConfigData[configIdx]->Config;
        if (First) {
          NextDangerIDType = protoIdx;
          NextDangerIDConfigIndex = configIdx;
          First = false;
        }
        character *Char = Proto->Spawn(Config, NO_EQUIPMENT|NO_PIC_UPDATE|
                                               NO_EQUIPMENT_PIC_UPDATE|
                                               /*from comm. fork*/
                                               NO_SEVERED_LIMBS);
        double NakedDanger = Char->GetRelativeDanger(Player, true);
        //ConLogf(" game::InitDangerMap(): 00");
        delete Char;
        //Char->SendToHell(); // k8: equipment
        Char = Proto->Spawn(Config, NO_PIC_UPDATE|NO_EQUIPMENT_PIC_UPDATE|
                                    /*from comm. fork*/
                                    NO_SEVERED_LIMBS);
        double EquippedDanger = Char->GetRelativeDanger(Player, true);
        //ConLogf(" game::InitDangerMap(): 01");
        delete Char;
        //Char->SendToHell(); // k8: equipment
        DangerMap[configid(protoIdx, Config)] = dangerid(NakedDanger, EquippedDanger);
      }
    }
  }
  pool::RegisterState(true);
  //ConLogf("game::InitDangerMap(): DONE");
}


//==========================================================================
//
//  game::DebugPrintCurrDangerID
//
//==========================================================================
void game::DebugPrintCurrDangerID (const char *pfx) {
  IvanAssert(NextDangerIDType >= 0 && NextDangerIDType < protocontainer<character>::GetSize());
  const character::prototype *Proto = protocontainer<character>::GetProto(NextDangerIDType);
  IvanAssert(Proto);
  IvanAssert(NextDangerIDConfigIndex >= 0 && NextDangerIDConfigIndex < Proto->GetConfigSize());
  const character::database *const *ConfigData = Proto->GetConfigData();
  IvanAssert(ConfigData);
  const character::database *DataBase = ConfigData[NextDangerIDConfigIndex];
  IvanAssert(DataBase);
  ConLogf("DID: %s: NextDangerIDType=%d; NextDangerIDConfigIndex=%d; '%s:%s' "
          "(maxcfg=%d)",
          pfx, NextDangerIDType, NextDangerIDConfigIndex,
          Proto->GetClassID(), DataBase->CfgStrName.CStr(),
          Proto->GetConfigSize());
}


//==========================================================================
//
//  game::AdvanceDangerIDType
//
//==========================================================================
void game::AdvanceDangerIDType () {
  for (int loopIter = 0; loopIter < 2; loopIter += 1) {
    while (NextDangerIDType + 1 < protocontainer<character>::GetSize()) {
      NextDangerIDType += 1;
      const character::prototype *Proto = protocontainer<character>::GetProto(NextDangerIDType);
      if (!Proto) continue;
      const character::database *const *ConfigData = Proto->GetConfigData();
      const int ConfigSize = Proto->GetConfigSize();
      NextDangerIDConfigIndex = 0;
      while (NextDangerIDConfigIndex < ConfigSize &&
             ConfigData[NextDangerIDConfigIndex]->IsAbstract)
      {
        NextDangerIDConfigIndex += 1;
      }
      if (NextDangerIDConfigIndex < ConfigSize) {
        #if 0
        if (!mDMLastWrapAroundTick) {
          mDMLastWrapAroundTurn = (feuLong)GetTurn();
          mDMLastWrapAroundTick = (feuLong)GetTick();
        }
        ConLogf("danger map: next id (%d)! turn=%u; tick=%u",
                NextDangerIDType,
                (feuLong)GetTurn() - mDMLastWrapAroundTurn,
                (feuLong)GetTick() - mDMLastWrapAroundTick);
        #endif
        return;
      }
    }
    NextDangerIDType = 0;
    #if 1
    if (mDMLastWrapAroundTick) {
      ConLogf("*** danger map: wraparound! turns=%u; ticks=%u",
              (feuLong)GetTurn() - mDMLastWrapAroundTurn,
              (feuLong)GetTick() - mDMLastWrapAroundTick);
    }
    #endif
    mDMLastWrapAroundTurn = (feuLong)GetTurn();
    mDMLastWrapAroundTick = (feuLong)GetTick();
  }
  ABORT("It is dangerous to go ice fishing in the summer.");
}


//==========================================================================
//
//  game::AdvanceDangerIDConfig
//
//  calls `AdvanceDangerIDType()` if necessary
//
//==========================================================================
void game::AdvanceDangerIDConfig () {
  IvanAssert(NextDangerIDType < protocontainer<character>::GetSize());
  const character::prototype *Proto = protocontainer<character>::GetProto(NextDangerIDType);
  IvanAssert(Proto);
  NextDangerIDConfigIndex += 1;
  if (NextDangerIDConfigIndex >= Proto->GetConfigSize()) {
    AdvanceDangerIDType();
  }
}


//==========================================================================
//
//  game::CalculateDangerMapUpdateRate
//
//  always > 0; but `-1` means "one config at a time"
//
//==========================================================================
int game::CalculateDangerMapUpdateRate () {
  bool showDebug = false;

  IvanAssert(DangerMapItemCount >= 0);
  if (DangerMapItemCount == 0) {
    DangerMapItemCount = (int)DangerMap.size();
    IvanAssert(DangerMapItemCount > 0);
    /*
    NextDangerIDType = 0;
    AdvanceDangerIDType();
    FirstDangerIDType = NextDangerIDType;
    FirstDangerIDConfigIndex = NextDangerIDConfigIndex;
    */
    showDebug = true;
    mDMapAccum = 0;
    mDMLastRate = 0;
  }

  int rate = Min(2000, DANGERMAP_REFRESH_RATE);

  // one monster at a time?
  if (rate < 0) {
    return 1;
  }

  // one config at a time?
  if (rate == 0) {
    return -1; // special value
  }

  if (rate >= DangerMapItemCount) {
    if (showDebug) {
      ConLogf("danger map update count is slowest (%d items)", DangerMapItemCount);
    }
    return 1;
  }

  int res = Max(1, DangerMapItemCount / rate);
  //if (DangerMapItemCount % rate >= rate / 2) res += 1;

  if (rate != mDMLastRate) {
    mDMapAccum = 0;
    mDMLastRate = rate;
    showDebug = true;
  }

  const int err = DangerMapItemCount % rate;
  mDMapAccum += err;
  if (mDMapAccum >= rate) {
    res += mDMapAccum / rate;
    mDMapAccum %= rate;
  }

  if (showDebug) {
    ConLogf("danger map update count is %d (err=%d) (for ~%d turns) (%d items)",
            res, err, DangerMapItemCount / res, DangerMapItemCount);
  }
  return res;
}


//==========================================================================
//
//  game::CalculateNextDanger
//
//  this slowly updates danger map
//
//==========================================================================
void game::CalculateNextDanger () {
  if (IsInWilderness()) {
    if (DANGERMAP_UPDATE_IN_WORLDMAP == 0) return;
  } else {
    // k8: do we need this check at all?
    // i belive we don't. level scripts are not used by the updater.
    // this was prolly done to speed up some special maps.
    #if 0
    if (!CurrentLevel->GetLevelScript()->GenerateMonsters()) return;
    if (!*CurrentLevel->GetLevelScript()->GenerateMonsters()) return;
    #endif
  }

  int left = CalculateDangerMapUpdateRate();
  const bool doDebug = (DEBUG_DANGER_MAP_UPDATES != 0);
  if (doDebug) {
    ConLogf("=== game::CalculateNextDanger: count=%d ===", left);
  }

  while (left != 0) {
    const character::prototype *Proto = protocontainer<character>::GetProto(NextDangerIDType);
    if (Proto) {
      const character::database *const *ConfigData = (Proto ? Proto->GetConfigData() : 0);
      const character::database *DataBase = (Proto && ConfigData ? ConfigData[NextDangerIDConfigIndex] : 0);
      if (DataBase) {
        if (doDebug) {
          DebugPrintCurrDangerID("  CalculateNextDanger(000)");
        }
        dangermap::iterator DangerIterator = DangerMap.find(configid(NextDangerIDType, DataBase->Config));
        if (DangerIterator != DangerMap.end()) {
          team *Team = GetTeam(PLAYER_TEAM);
          //ConLogf("game::CalculateNextDanger(): START");
          character *Char;
          std::list<character *>::const_iterator i;

          pool::RegisterState(false);

          Char = Proto->Spawn(DataBase->Config, NO_EQUIPMENT|NO_PIC_UPDATE|NO_EQUIPMENT_PIC_UPDATE|
                                                /*k8:dunno*/NO_SEVERED_LIMBS);
          double DangerSum = Player->GetRelativeDanger(Char, true);
          for (i = Team->GetMember().begin(); i != Team->GetMember().end(); ++i) {
            if ((*i)->IsEnabled() && !(*i)->IsTemporary() && !RAND_N(10)) {
              DangerSum += (*i)->GetRelativeDanger(Char, true) / 4.0;
            }
          }
          if (fabs(DangerSum) < 0.0000000001) DangerSum = 1;
          double CurrentDanger = 1 / DangerSum;
          double NakedDanger = DangerIterator->second.NakedDanger;
          //ConLogf(" game::CalculateNextDanger(): 00");
          delete Char;

          //Char->SendToHell(); // k8: equipment
          if (NakedDanger > CurrentDanger) {
            DangerIterator->second.NakedDanger = (NakedDanger * 9 + CurrentDanger) / 10;
          }

          Char = Proto->Spawn(DataBase->Config, NO_PIC_UPDATE|NO_EQUIPMENT_PIC_UPDATE|
                                                /*k8:dunno*/NO_SEVERED_LIMBS);
          DangerSum = Player->GetRelativeDanger(Char, true);
          for (i = Team->GetMember().begin(); i != Team->GetMember().end(); ++i) {
            if ((*i)->IsEnabled() && !(*i)->IsTemporary() && !RAND_N(10)) {
              DangerSum += (*i)->GetRelativeDanger(Char, true) / 4.0;
            }
          }
          if (fabs(DangerSum) < 0.0000000001) DangerSum = 1;
          CurrentDanger = 1 / DangerSum;
          double EquippedDanger = DangerIterator->second.EquippedDanger;
          //ConLogf(" game::CalculateNextDanger(): 01");
          delete Char;

          //Char->SendToHell(); // k8: equipment
          pool::RegisterState(true);

          if (EquippedDanger > CurrentDanger) {
            DangerIterator->second.EquippedDanger = (EquippedDanger*9+CurrentDanger)/10;
          }

          if (left < 0) {
            // one config at a time
            const int oldIDType = NextDangerIDType;
            AdvanceDangerIDConfig();
            //k8: calculate for the whole class, why not.
            // i believe that doing this one-monster-at-a-time was
            // done to speed up the things. of course, this also allows
            // the game to change the danger level more smoothly, but
            // we have way more monsters now (zombies, golems, etc.),
            // so i think that doing it this way is better.
            // the game adapts in ~100 or so turns this way.
            //TODO: maybe we should process a fixed number of monsters instead,
            //      and have the fixed update rate in turns.
            if (oldIDType != NextDangerIDType) return;
          } else {
            AdvanceDangerIDConfig();
            left -= 1;
          }
        } else {
          AdvanceDangerIDType();
        }
      } else {
        AdvanceDangerIDType();
      }
    } else {
      AdvanceDangerIDType();
    }
  }
}


//==========================================================================
//
//  game::TryTravel
//
//==========================================================================
truth game::TryTravel (int Dungeon, int Area, int EntryIndex,
                       truth AllowHostiles, truth AlliesFollow)
{
  //ConLogf("game::TryTravel: Dungeon=%d; Area=%d; EntryIndex=%d", Dungeon, Area, EntryIndex);
  charactervector Group;
  // for SQLite archive mode, open save, so leaving and entering will be in the same transaction
  if (CurrentSaveOpenCount != 0) {
    IvanAssert(CurrentSaveOpenCount == 1);
    ConLogf("*** TryTravel: there is still opened save (it should be harmless)...");
    IvanAssert(GetSaveArchiveTransactionCount() > 0);
  } else {
    OpenCurrentSave();
    SaveArchiveBeginTransaction();
  }
  if (LeaveArea(Group, AllowHostiles, AlliesFollow)) {
    CurrentDungeonIndex = Dungeon;
    EnterArea(Group, Area, EntryIndex);
    IvanAssert(CurrentSaveOpenCount == 1);
    return true;
  }
  IvanAssert(CurrentSaveOpenCount == 1);
  return false;
}


//==========================================================================
//
//  game::LeaveArea
//
//==========================================================================
truth game::LeaveArea (charactervector &Group, truth AllowHostiles, truth AlliesFollow) {
  if (!IsInWilderness()) {
    //ConLogf("leaving; AlliesFollow=%d", (int)AlliesFollow);
    if (AlliesFollow && !GetCurrentLevel()->CollectCreatures(Group, Player, AllowHostiles)) {
      return false;
    }
    Player->Remove();
    //outputfile::MakeDir(SaveName());
    //GetCurrentDungeon()->SaveLevel(SaveName(), CurrentLevelIndex);
    SaveCurrentDungeonLevel(CurrentLevelIndex);
  } else {
    Player->Remove();
    GetWorldMap()->GetPlayerGroup().swap(Group);
    SaveWorldMap(true);
  }
  return true;
}


//==========================================================================
//
//  game::EnterArea
//
//  Used always when the player enters an area.
//
//==========================================================================
void game::EnterArea (charactervector &Group, int Area, int EntryIndex) {
  SpecialCursorsEnabled = false;
  HiSquare = ERROR_V2;
  if (Area != WORLD_MAP) {
    SetIsGenerating(true);
    SetIsInWilderness(false);
    CurrentLevelIndex = Area;
    truth New = !PrepareRandomBone(Area) && !GetCurrentDungeon()->PrepareLevel(Area);
    igraph::CreateBackGround(*CurrentLevel->GetLevelScript()->GetBackGroundType());
    GetCurrentArea()->SendNewDrawRequest();
    v2 Pos = GetCurrentLevel()->GetEntryPos(Player, EntryIndex);
    if (Player) {
      GetCurrentLevel()->GetLSquare(Pos)->KickAnyoneStandingHereAway();
      Player->PutToOrNear(Pos);
    } else {
      SetPlayer(GetCurrentLevel()->GetLSquare(Pos)->GetCharacter());
    }
    uInt c;
    for (c = 0; c < Group.size(); ++c) {
      v2 NPCPos = GetCurrentLevel()->GetNearestFreeSquare(Group[c], Pos);
      if (NPCPos == ERROR_V2) {
        NPCPos = GetCurrentLevel()->GetRandomSquare(Group[c]);
        if (NPCPos == ERROR_V2) ABORT("No country for the old NPC!");
      }
      Group[c]->PutTo(NPCPos);
    }
    GetCurrentLevel()->FiatLux();
    ctruth *AutoReveal = GetCurrentLevel()->GetLevelScript()->AutoReveal();
    if (New && AutoReveal && *AutoReveal) {
      ctruth *AutoRevealIgnoreDarkness = GetCurrentLevel()->GetLevelScript()->AutoRevealIgnoreDarkness();
      GetCurrentLevel()->Reveal(AutoRevealIgnoreDarkness ? *AutoRevealIgnoreDarkness : false);
    }
    ShowLevelMessage();
    SendLOSUpdateRequest();
    UpdateCamera();

    if (New) {
      GlobalRainLiquid = 0;
      GetCurrentArea()->SetGlobalRainRandom(false);

      int gRainLiquid = 0;
      if (GetCurrentLevel()->GetLevelScript()->GetGlobalRainLiquid()) {
        gRainLiquid = *(GetCurrentLevel()->GetLevelScript()->GetGlobalRainLiquid());
        if (!GetCurrentLevel()->GetLevelScript()->GetGlobalRainSpeed()) {
          gRainLiquid = 0;
        }
        if (!GetCurrentLevel()->GetLevelScript()->GetGlobalRainVolume()) {
          gRainLiquid = 0;
        }
      }

      if (gRainLiquid) {
        //FIXME: proper liquid!
        if (gRainLiquid == SNOW) {
          GlobalRainLiquid = powder::Spawn(SNOW);
        } else if (gRainLiquid == WATER) {
          GlobalRainLiquid = liquid::Spawn(WATER);
        } else if (gRainLiquid == BLOOD) {
          GlobalRainLiquid = liquid::Spawn(BLOOD);
        } else {
          ABORT("invalid global rain liquid!");
        }
        GlobalRainSpeed = *GetCurrentLevel()->GetLevelScript()->GetGlobalRainSpeed();
        CurrentLevel->CreateGlobalRain(GlobalRainLiquid, GlobalRainSpeed);
        int vol = *GetCurrentLevel()->GetLevelScript()->GetGlobalRainVolume();
        if (vol > 0) {
          GlobalRainLiquid->SetVolumeNoSignals(200);
          CurrentLevel->EnableGlobalRain();
        } else {
          GetCurrentArea()->SetGlobalRainRandom(true);
        }
      }
    }

    /* Gum solution! */
    /*
    if (New && CurrentDungeonIndex == ATTNAM && Area == 0) {
      GlobalRainLiquid = powder::Spawn(SNOW);
      GlobalRainSpeed = v2(-64, 128);
      CurrentLevel->CreateGlobalRain(GlobalRainLiquid, GlobalRainSpeed);
    }

    if (New && CurrentDungeonIndex == NEW_ATTNAM && Area == 0) {
      GlobalRainLiquid = liquid::Spawn(WATER);
      GlobalRainSpeed = v2(256, 512);
      CurrentLevel->CreateGlobalRain(GlobalRainLiquid, GlobalRainSpeed);
    }

    if (New && CurrentDungeonIndex == ELPURI_CAVE && Area == OREE_LAIR) {
      GlobalRainLiquid = liquid::Spawn(BLOOD);
      GlobalRainSpeed = v2(256, 512);
      CurrentLevel->CreateGlobalRain(GlobalRainLiquid, GlobalRainSpeed);
      GlobalRainLiquid->SetVolumeNoSignals(200);
      CurrentLevel->EnableGlobalRain();
    }

    if (New && CurrentDungeonIndex == MUNTUO && Area == 0) {
      GlobalRainLiquid = liquid::Spawn(WATER);
      GlobalRainSpeed = v2(-64, 1024);
      CurrentLevel->CreateGlobalRain(GlobalRainLiquid, GlobalRainSpeed);
    }
    */

    SetIsGenerating(false);
    GetCurrentLevel()->UpdateLOS();
    Player->SignalStepFrom(0);

    for (c = 0; c < Group.size(); ++c) {
      Group[c]->SignalStepFrom(0);
    }
  } else {
    igraph::CreateBackGround(GRAY_FRACTAL);
    SetIsInWilderness(true);
    LoadWorldMap();
    SetCurrentArea(WorldMap);
    CurrentWSquareMap = WorldMap->GetMap();
    GetWorldMap()->GetPlayerGroup().swap(Group);
    //ConLogf("EINDEX=%d; pos=(%d,%d)", EntryIndex, GetWorldMap()->GetEntryPos(Player, EntryIndex).X, GetWorldMap()->GetEntryPos(Player, EntryIndex).Y);
    Player->PutTo(GetWorldMap()->GetEntryPos(Player, EntryIndex));
    SendLOSUpdateRequest();
    UpdateCamera();
    GetWorldMap()->UpdateLOS();
  }

  /*
  if (ivanconfig::GetAutoSaveInterval()) {
    //Save(GetAutoSaveFileName().CStr());
    AutoSave();
  }
  */
  ScheduleImmediateSave();
}


//==========================================================================
//
//  game::SetStandardListAttributes
//
//==========================================================================
void game::SetStandardListAttributes (felist &List) {
  if (ivanconfig::GetStatusOnLeft()) {
    if (ivanconfig::GetShorterListsX()) {
      //k8: i have some part of the screen obscured. oops.
      List.SetPos(v2(26 - 6 + 96, 42 - 6));
    } else {
      List.SetPos(v2(26 + 96, 42));
    }
  } else {
    List.SetPos(v2(26, 42));
  }
  //List.SetPos(CalculateScreenCoordinates(v2(0, 0)) + v2(10, 10));
  //List.SetWidth(652);
  if (ivanconfig::GetShorterListsX()) {
    //k8: i have some part of the screen obscured. oops.
    List.SetWidth((GetScreenXSize() - 3) * 16 - 20);
  } else {
    List.SetWidth(GetScreenXSize() * 16 - 20);
  }
  List.SetFlags(DRAW_BACKGROUND_AFTERWARDS);
  //List.SetUpKey(GetMoveCommandKey(KEY_UP_INDEX));
  //List.SetDownKey(GetMoveCommandKey(KEY_DOWN_INDEX));
  List.SetUpKey(KEY_UP);
  List.SetDownKey(KEY_DOWN);
}


//==========================================================================
//
//  game::InitPlayerAttributeAverage
//
//==========================================================================
void game::InitPlayerAttributeAverage () {
  AveragePlayerArmStrengthExperience
    = AveragePlayerLegStrengthExperience
    = AveragePlayerDexterityExperience
    = AveragePlayerAgilityExperience
    = 0;

  if (!Player->IsHumanoid()) return;

  humanoid *Player = static_cast<humanoid*>(GetPlayer());
  int Arms = 0;
  int Legs = 0;
  arm *RightArm = Player->GetRightArm();

  if (RightArm && !RightArm->UseMaterialAttributes()) {
    AveragePlayerArmStrengthExperience += RightArm->GetStrengthExperience();
    AveragePlayerDexterityExperience += RightArm->GetDexterityExperience();
    ++Arms;
  }

  arm *LeftArm = Player->GetLeftArm();

  if (LeftArm && !LeftArm->UseMaterialAttributes()) {
    AveragePlayerArmStrengthExperience += LeftArm->GetStrengthExperience();
    AveragePlayerDexterityExperience += LeftArm->GetDexterityExperience();
    ++Arms;
  }

  leg *RightLeg = Player->GetRightLeg();

  if (RightLeg && !RightLeg->UseMaterialAttributes()) {
    AveragePlayerLegStrengthExperience += RightLeg->GetStrengthExperience();
    AveragePlayerAgilityExperience += RightLeg->GetAgilityExperience();
    ++Legs;
  }

  leg *LeftLeg = Player->GetLeftLeg();

  if (LeftLeg && !LeftLeg->UseMaterialAttributes()) {
    AveragePlayerLegStrengthExperience += LeftLeg->GetStrengthExperience();
    AveragePlayerAgilityExperience += LeftLeg->GetAgilityExperience();
    ++Legs;
  }

  if (Arms) {
    AveragePlayerArmStrengthExperience /= Arms;
    AveragePlayerDexterityExperience /= Arms;
  }

  if (Legs) {
    AveragePlayerLegStrengthExperience /= Legs;
    AveragePlayerAgilityExperience /= Legs;
  }
}


//==========================================================================
//
//  game::UpdatePlayerAttributeAverage
//
//==========================================================================
void game::UpdatePlayerAttributeAverage () {
  if (!Player->IsHumanoid()) return;

  humanoid *Player = static_cast<humanoid*>(GetPlayer());
  double PlayerArmStrengthExperience = 0;
  double PlayerLegStrengthExperience = 0;
  double PlayerDexterityExperience = 0;
  double PlayerAgilityExperience = 0;
  int Arms = 0;
  int Legs = 0;
  arm *RightArm = Player->GetRightArm();

  if (RightArm && !RightArm->UseMaterialAttributes()) {
    PlayerArmStrengthExperience += RightArm->GetStrengthExperience();
    PlayerDexterityExperience += RightArm->GetDexterityExperience();
    ++Arms;
  }

  arm *LeftArm = Player->GetLeftArm();

  if (LeftArm && !LeftArm->UseMaterialAttributes()) {
    PlayerArmStrengthExperience += LeftArm->GetStrengthExperience();
    PlayerDexterityExperience += LeftArm->GetDexterityExperience();
    ++Arms;
  }

  leg *RightLeg = Player->GetRightLeg();

  if (RightLeg && !RightLeg->UseMaterialAttributes()) {
    PlayerLegStrengthExperience += RightLeg->GetStrengthExperience();
    PlayerAgilityExperience += RightLeg->GetAgilityExperience();
    ++Legs;
  }

  leg *LeftLeg = Player->GetLeftLeg();

  if (LeftLeg && !LeftLeg->UseMaterialAttributes()) {
    PlayerLegStrengthExperience += LeftLeg->GetStrengthExperience();
    PlayerAgilityExperience += LeftLeg->GetAgilityExperience();
    ++Legs;
  }

  if (Arms) {
    AveragePlayerArmStrengthExperience = (49 * AveragePlayerArmStrengthExperience + PlayerArmStrengthExperience / Arms) / 50;
    AveragePlayerDexterityExperience = (49 * AveragePlayerDexterityExperience + PlayerDexterityExperience / Arms) / 50;
  }

  if (Legs) {
    AveragePlayerLegStrengthExperience = (49 * AveragePlayerLegStrengthExperience + PlayerLegStrengthExperience / Legs) / 50;
    AveragePlayerAgilityExperience = (49 * AveragePlayerAgilityExperience + PlayerAgilityExperience / Legs) / 50;
  }
}


//==========================================================================
//
//  game::CallForAttention
//
//==========================================================================
void game::CallForAttention (v2 Pos, int RangeSquare) {
  for (int c = 0; c != GetTeams(); c += 1) {
    if (GetTeam(c)->HasEnemy()) {
      for (std::list<character *>::const_iterator i = GetTeam(c)->GetMember().begin();
           i != GetTeam(c)->GetMember().end(); ++i)
      {
        character *Char = *i;
        if (Char->IsEnabled() && Char->GetSquaresUnder()) {
          sLong ThisDistance = HypotSquare(sLong(Char->GetPos().X) - Pos.X,
                                           sLong(Char->GetPos().Y) - Pos.Y);
          if (ThisDistance <= RangeSquare && !Char->IsGoingSomeWhere()) {
            Char->SetGoingTo(Pos);
          }
        } else if (Char->IsEnabled() && !Char->GetSquaresUnder()) {
          ConLogf("ERROR: char '%s(%d)' has no squares under it!",
                  Char->GetClassID(), Char->GetConfig());
        }
      }
    }
  }
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const homedata *HomeData) {
  if (HomeData) {
    SaveFile.Put(1);
    SaveFile << HomeData->Pos << HomeData->Dungeon << HomeData->Level << HomeData->Room;
  } else SaveFile.Put(0);
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, homedata *&HomeData) {
  if (SaveFile.Get()) {
    HomeData = new homedata;
    SaveFile >> HomeData->Pos >> HomeData->Dungeon >> HomeData->Level >> HomeData->Room;
  }
  return SaveFile;
}


//==========================================================================
//
//  game::CreateNewCharacterID
//
//==========================================================================
feuLong game::CreateNewCharacterID (character *NewChar) {
  feuLong ID = NextCharacterID++;
  if (CharacterIDMap.find(ID) != CharacterIDMap.end()) {
    ConLogf("ERROR: duplicate characted id!");
  }
  CharacterIDMap.insert(std::make_pair(ID, NewChar));
  return ID;
}


//==========================================================================
//
//  game::CreateNewItemID
//
//==========================================================================
feuLong game::CreateNewItemID (item *NewItem) {
  feuLong ID = NextItemID++;
  if (ItemIDMap.find(ID) != ItemIDMap.end()) {
    ConLogf("ERROR: duplicate item id!");
  }
  if (NewItem) ItemIDMap.insert(std::make_pair(ID, NewItem));
  return ID;
}


//==========================================================================
//
//  game::CreateNewTrapID
//
//==========================================================================
feuLong game::CreateNewTrapID (entity *NewTrap) {
  feuLong ID = NextTrapID++;
  if (TrapIDMap.find(ID) != TrapIDMap.end()) {
    ConLogf("ERROR: duplicate trap id!");
  }
  if (NewTrap) TrapIDMap.insert(std::make_pair(ID, NewTrap));
  return ID;
}


//==========================================================================
//
//  game::SearchCharacter
//
//==========================================================================
character *game::SearchCharacter (feuLong ID) {
  characteridmap::iterator Iterator = CharacterIDMap.find(ID);
  return (Iterator != CharacterIDMap.end() ? Iterator->second : 0);
}


//==========================================================================
//
//  game::SearchItem
//
//==========================================================================
item *game::SearchItem (feuLong ID) {
  itemidmap::iterator Iterator = ItemIDMap.find(ID);
  return (Iterator != ItemIDMap.end() ? Iterator->second : 0);
}


//==========================================================================
//
//  game::SearchTrap
//
//==========================================================================
entity *game::SearchTrap (feuLong ID) {
  trapidmap::iterator Iterator = TrapIDMap.find(ID);
  return (Iterator != TrapIDMap.end() ? Iterator->second : 0);
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const configid &Value) {
  //SaveFile.Write(reinterpret_cast<cchar*>(&Value), sizeof(Value));
  SaveFile << Value.Type << Value.Config;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, configid &Value) {
  //SaveFile.Read(reinterpret_cast<char*>(&Value), sizeof(Value));
  SaveFile >> Value.Type >> Value.Config;
  return SaveFile;
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const dangerid &Value) {
  SaveFile << Value.NakedDanger << Value.EquippedDanger;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, dangerid &Value) {
  SaveFile >> Value.NakedDanger >> Value.EquippedDanger;
  return SaveFile;
}


//==========================================================================
//
//  game::GetHomeDir
//
//==========================================================================
festring game::GetHomeDir () {
  /*
#ifndef SHITDOZE
  festring Dir;
  Dir << getenv("HOME") << '/';
  return Dir;
#else
  return festring("./");
#endif
  */
  return festring("./");
}


//==========================================================================
//
//  game::GetGamePath
//
//==========================================================================
festring game::GetGamePath () {
  /*k8! return DATADIR "/ivan/"; */
  /*k8! return DATADIR "/"; */
  festring Dir;
  #if 0
  Dir << ivanconfig::GetMyDir() << "/";
  #endif
  return Dir;
}


//==========================================================================
//
//  game::GetConfigPath
//
//==========================================================================
festring game::GetConfigPath () {
  festring path;
#ifdef LOCAL_SAVES
  path << ivanconfig::GetMyDir();
#else
  path << getenv("HOME");
  if (path.IsEmpty()) {
    path << ivanconfig::GetMyDir();
  } else {
    path << "/.k8ivan";
    outputfile::MakeDir(path);
  }
#endif
  path << "/";
  return path;
}


//==========================================================================
//
//  game::GetSavePath
//
//==========================================================================
festring game::GetSavePath () {
  festring Dir;
  Dir << GetConfigPath() << "saves/";
  outputfile::MakeDir(Dir);
  return Dir;
}


//==========================================================================
//
//  game::GetBonePath
//
//==========================================================================
festring game::GetBonePath () {
  /*k8! return LOCAL_STATE_DIR "/Bones/";*/
  festring Dir;
  Dir << GetConfigPath() << "bones/";
  outputfile::MakeDir(Dir);
  return Dir;
}


//==========================================================================
//
//  game::GetDeathshotPath
//
//==========================================================================
festring game::GetDeathshotPath () {
  festring Dir;
  Dir << GetConfigPath() << "deathshots/";
  outputfile::MakeDir(Dir);
  return Dir;
}


//==========================================================================
//
//  game::GetScreenshotPath
//
//==========================================================================
festring game::GetScreenshotPath () {
  festring Dir;
  Dir << GetConfigPath() << "screenshots/";
  outputfile::MakeDir(Dir);
  return Dir;
}


//==========================================================================
//
//  game::GetLevel
//
//==========================================================================
level *game::GetLevel (int I) {
  return GetCurrentDungeon()->GetLevel(I);
}


//==========================================================================
//
//  game::GetLevels
//
//==========================================================================
int game::GetLevels () {
  return GetCurrentDungeon()->GetLevels();
}


//==========================================================================
//
//  game::SignalDeath
//
//==========================================================================
void game::SignalDeath (ccharacter *Ghost, ccharacter *Murderer, festring DeathMsg) {
  if (InWilderness) DeathMsg << " in the world map";
  else DeathMsg << " in " << GetCurrentDungeon()->GetLevelDescription(CurrentLevelIndex);
  massacremap *MassacreMap;
  if (!Murderer) {
    ++MiscMassacreAmount;
    MassacreMap = &MiscMassacreMap;
  } else if(Murderer->IsPlayer()) {
    ++PlayerMassacreAmount;
    MassacreMap = &PlayerMassacreMap;
  } else if(Murderer->IsPet()) {
    ++PetMassacreAmount;
    MassacreMap = &PetMassacreMap;
  } else {
    ++MiscMassacreAmount;
    MassacreMap = &MiscMassacreMap;
  }

  massacreid MI(Ghost->GetType(), Ghost->GetConfig(), Ghost->GetAssignedName());
  massacremap::iterator i = MassacreMap->find(MI);

  if (i == MassacreMap->end()) {
    i = MassacreMap->insert(std::make_pair(MI, killdata(1, Ghost->GetGenerationDanger()))).first;
    i->second.Reason.push_back(killreason(DeathMsg, 1));
  } else {
    ++i->second.Amount;
    i->second.DangerSum += Ghost->GetGenerationDanger();
    std::vector<killreason>& Reason = i->second.Reason;
    uInt c;
    for (c = 0; c < Reason.size(); ++c) {
      if (Reason[c].String == DeathMsg) {
        ++Reason[c].Amount;
        break;
      }
    }
    if (c == Reason.size()) Reason.push_back(killreason(DeathMsg, 1));
  }
}


//==========================================================================
//
//  game::DisplayMassacreLists
//
//==========================================================================
void game::DisplayMassacreLists () {
  DisplayMassacreList(PlayerMassacreMap, "directly by you.", PlayerMassacreAmount);
  DisplayMassacreList(PetMassacreMap, "by your allies.", PetMassacreAmount);
  DisplayMassacreList(MiscMassacreMap, "by some other reason.", MiscMassacreAmount);
}


//==========================================================================
//
//  game::SaveMassacreLists
//
//==========================================================================
void game::SaveMassacreLists () {
  SaveMassacreList(0, PlayerMassacreMap, "directly by you", PlayerMassacreAmount);
  SaveMassacreList(1, PetMassacreMap, "by your allies", PetMassacreAmount);
  SaveMassacreList(2, MiscMassacreMap, "by some other reason", MiscMassacreAmount);
}


//==========================================================================
//
//  game::BuildMassacreList
//
//==========================================================================
void game::BuildMassacreList (std::unordered_set<massacresetentry> &MassacreSet,
                              charactervector &GraveYard, festring &FirstPronoun,
                              const massacremap &MassacreMap, cchar *Reason, sLong Amount)
{
  FirstPronoun.Empty();
  truth First = true;
  for (massacremap::const_iterator i1 = MassacreMap.begin(); i1 != MassacreMap.end(); ++i1) {
    auto monsProto = protocontainer<character>::GetProto(i1->first.Type);
    if (!monsProto) continue; // missing character
    character *Victim = monsProto->Spawn(i1->first.Config);
    Victim->SetAssignedName(i1->first.Name);
    massacresetentry Entry;
    GraveYard.push_back(Victim);
    Entry.ImageKey = AddToCharacterDrawVector(Victim);
    if (i1->second.Amount == 1) {
      Victim->AddName(Entry.Key, UNARTICLED);
      Victim->AddName(Entry.String, INDEFINITE);
    } else {
      Victim->AddName(Entry.Key, PLURAL);
      Entry.String << i1->second.Amount << ' ' << Entry.Key;
    }
    if (First) {
      FirstPronoun = (Victim->GetSex() == UNDEFINED ? "it" : Victim->GetSex() == MALE ? "he" : "she");
      First = false;
    }
    const std::vector<killreason>& Reason = i1->second.Reason;
    std::vector<festring> &Details = Entry.Details;
    if (Reason.size() == 1) {
      festring Begin;
      if (Reason[0].Amount == 1) Begin = "";
      else if(Reason[0].Amount == 2) Begin = "both ";
      else Begin = "all ";
      Details.push_back(Begin + Reason[0].String);
    } else {
      for (uInt c = 0; c < Reason.size(); ++c) Details.push_back(CONST_S("")+Reason[c].Amount+' '+Reason[c].String);
      std::sort(Details.begin(), Details.end(), ignorecaseorderer());
    }
    MassacreSet.insert(Entry);
  }
}


//==========================================================================
//
//  game::DisplayMassacreList
//
//==========================================================================
void game::DisplayMassacreList (const massacremap &MassacreMap, cchar *Reason, sLong Amount) {
  std::unordered_set<massacresetentry> MassacreSet;
  charactervector GraveYard;
  festring FirstPronoun;
  BuildMassacreList(MassacreSet, GraveYard, FirstPronoun, MassacreMap, Reason, Amount);

  sLong Total = PlayerMassacreAmount+PetMassacreAmount+MiscMassacreAmount;
  festring MainTopic;
  if (Total == 1) {
    MainTopic << "One creature perished during your adventure.";
  } else {
    MainTopic << Total << " creatures perished during your adventure.";
  }

  felist List(MainTopic);
  SetStandardListAttributes(List);
  List.SetPageLength(15);
  List.AddFlags(SELECTABLE);
  List.SetEntryDrawer(CharacterEntryDrawer);
  List.AddDescription(CONST_S(""));
  festring SideTopic;
  if (Amount != Total) {
    SideTopic = CONST_S("The following ");
    if (Amount == 1) SideTopic << "one was killed " << Reason;
    else SideTopic << Amount << " were killed " << Reason;
  } else {
    if (Amount == 1) {
      FirstPronoun.Capitalize();
      SideTopic << FirstPronoun << " was killed " << Reason;
    } else SideTopic << "They were all killed " << Reason;
  }
  List.AddDescription(SideTopic);
  List.AddDescription(CONST_S(""));
  List.AddDescription(CONST_S("Choose a type of creatures to browse death details."));
  std::unordered_set<massacresetentry>::const_iterator i2;
  for (i2 = MassacreSet.begin(); i2 != MassacreSet.end(); ++i2) List.AddEntry(i2->String, LIGHT_GRAY, 0, i2->ImageKey);
  for (;;) {
    int Chosen = List.Draw();
    if (Chosen & FELIST_ERROR_BIT) break;
    felist SubList(CONST_S("Massacre details"));
    SetStandardListAttributes(SubList);
    SubList.SetPageLength(20);
    int Counter = 0;
    for (i2 = MassacreSet.begin(); i2 != MassacreSet.end(); ++i2, ++Counter) {
      if (Counter == Chosen) {
        for (uInt c = 0; c < i2->Details.size(); ++c) SubList.AddEntry(i2->Details[c], LIGHT_GRAY);
        break;
      }
    }
    SubList.Draw();
  }
  ClearCharacterDrawVector();
  for (uInt c = 0; c < GraveYard.size(); ++c) delete GraveYard[c];
}


//==========================================================================
//
//  game::SaveMassacreList
//
//==========================================================================
void game::SaveMassacreList (int type, const massacremap &MassacreMap, cchar *Reason, sLong Amount) {
  std::unordered_set<massacresetentry> MassacreSet;
  charactervector GraveYard;
  festring FirstPronoun;
  BuildMassacreList(MassacreSet, GraveYard, FirstPronoun, MassacreMap, Reason, Amount);

  std::unordered_set<massacresetentry>::const_iterator i2;
  for (i2 = MassacreSet.begin(); i2 != MassacreSet.end(); ++i2) {
    highscore::AddLastMassacre(type, /*i2->Key,*/ i2->String, i2->Details);
  }

  for (uInt c = 0; c < GraveYard.size(); ++c) delete GraveYard[c];
}


//==========================================================================
//
//  game::MassacreListsEmpty
//
//==========================================================================
truth game::MassacreListsEmpty () {
  return PlayerMassacreMap.empty() && PetMassacreMap.empty() && MiscMassacreMap.empty();
}


//==========================================================================
//
//  game::SeeWholeMap
//
//==========================================================================
void game::SeeWholeMap () {
  if (SeeWholeMapCheatMode < 2) ++SeeWholeMapCheatMode; else SeeWholeMapCheatMode = 0;
  GetCurrentArea()->SendNewDrawRequest();
}


//==========================================================================
//
//  game::SetSkipHiScrore
//
//==========================================================================
void game::SetSkipHiScrore () {
  if (!SkipHiScore) {
    SkipHiScore = true;
    ScheduleImmediateSave(); // cheaters should be punished!
  }
}


//==========================================================================
//
//  game::ResetSkipHiScrore
//
//==========================================================================
void game::ResetSkipHiScrore () {
  if (SkipHiScore) {
    SkipHiScore = false;
    ScheduleImmediateSave(); // cheaters should be punished!
  }
}


//==========================================================================
//
//  game::CalculateAverageDanger
//
//==========================================================================
double game::CalculateAverageDanger (const charactervector &EnemyVector, character *Char) {
  double DangerSum = 0;
  int Enemies = 0;
  for (uInt c = 0; c < EnemyVector.size(); ++c) {
    DangerSum += EnemyVector[c]->GetRelativeDanger(Char, true);
    ++Enemies;
  }
  return DangerSum/Enemies;
}


//==========================================================================
//
//  game::CalculateAverageDangerOfAllNormalEnemies
//
//  used in player ghost creation
//
//==========================================================================
double game::CalculateAverageDangerOfAllNormalEnemies () {
  int rate = Min(2000, DANGERMAP_REFRESH_RATE);
  if (rate < 0) {
    // update danger level for all monsters, why not?
    // the game adapts to the danger level in ~100 turns,
    // so we may have a partially correct danger level here.
    // let's not assume anything, and stabilize the danger level.
    NextDangerIDType = 0;
    AdvanceDangerIDType(); // move to the first valid config
    const int oldIDType = NextDangerIDType;
    do {
      CalculateNextDanger();
    } while (NextDangerIDType != oldIDType);
  }
  // and use the data
  double DangerSum = 0;
  int Enemies = 0;
  for (int protoIdx = 1; protoIdx < protocontainer<character>::GetSize(); ++protoIdx) {
    const character::prototype *Proto = protocontainer<character>::GetProto(protoIdx);
    if (!Proto) continue; // missing character
    const character::database*const *ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    for (int configIdx = 0; configIdx < ConfigSize; ++configIdx) {
      if (!ConfigData[configIdx]->IsAbstract && !ConfigData[configIdx]->IsUnique &&
          ConfigData[configIdx]->CanBeGenerated)
      {
        auto it = DangerMap.find(configid(protoIdx, ConfigData[configIdx]->Config));
        if (it != DangerMap.end()) {
          DangerSum += it->second.EquippedDanger;
          Enemies += 1;
        } else {
          ConLogf("OOPS! no avg danger for '%s:%s'", Proto->GetClassID(),
                  ConfigData[configIdx]->CfgStrName.CStr());
        }
        //DangerSum += DangerMap.find(configid(protoIdx, ConfigData[configIdx]->Config))->second.EquippedDanger;
        //++Enemies;
      }
    }
  }
  return (Enemies ? DangerSum / Enemies : 1.0);
}


//==========================================================================
//
//  DumpGhostInfo
//
//==========================================================================
static void DumpGhostInfo (ghost *Ghost) {
  ConLogf("Information about %s:", Ghost->GetDescription(DEFINITE).CStr());
  ConLogf("  Strength: %d", Ghost->GetAttribute(ARM_STRENGTH));
  ConLogf("  Agility: %d", Ghost->GetAttribute(AGILITY));
  ConLogf("  Endurance: %d", Ghost->GetAttribute(ENDURANCE));
  ConLogf("  Perception: %d", Ghost->GetAttribute(PERCEPTION));
  ConLogf("  Intelligence: %d", Ghost->GetAttribute(INTELLIGENCE));
  ConLogf("  Wisdom: %d", Ghost->GetAttribute(WISDOM));
  ConLogf("  Willpower: %d", Ghost->GetAttribute(WILL_POWER));
  ConLogf("  Charisma: %d", Ghost->GetAttribute(CHARISMA));
  ConLogf("  HP: %d/%d", Ghost->GetHP(), Ghost->GetMaxHP());
}


//==========================================================================
//
//  game::CreateGhost
//
//  called on death to create player ghost for bones
//
//==========================================================================
character *game::CreateGhost () {
  double AverageDanger = CalculateAverageDangerOfAllNormalEnemies();
  charactervector EnemyVector;
  protosystem::CreateEveryNormalEnemy(EnemyVector);
  ghost *Ghost = ghost::Spawn();
  Ghost->SetTeam(GetTeam(MONSTER_TEAM));
  Ghost->SetGenerationDanger(CurrentLevel->GetDifficulty());
  Ghost->SetOwnerSoul(PlayerName);
  Ghost->SetIsActive(false);
  Ghost->EditAllAttributes(-4);
  Player->SetSoulID(Ghost->GetID());
  DumpGhostInfo(Ghost);
  #if 0
  while (CalculateAverageDanger(EnemyVector, Ghost) > AverageDanger &&
         Ghost->EditAllAttributes(1))
  {
    // loop
  }
  #else
  ConLogf("creating player ghost; enemies=%u; AverageDanger=%g",
          (unsigned)EnemyVector.size(), AverageDanger);
  #if 0
  for (size_t f = 0; f != EnemyVector.size(); f += 1) {
    character *cc = EnemyVector[f];
    ConLogf("  #%u: %s:%s (%s)", (unsigned)f,
            cc->GetClassID(), cc->GetConfigName().CStr(),
            cc->CHAR_NAME(INDEFINITE));
  }
  #endif
  unsigned maxTries = (unsigned)Clamp(MAX_GHOST_CREATION_BUFFS, 4, 256);
  unsigned tryCount = 0;
  for (;;) {
    double cdd = CalculateAverageDanger(EnemyVector, Ghost);
    if (cdd <= AverageDanger) {
      ConLogf("ghost: created a proper ghost after %u tries: avg=%g; cdd=%g",
              tryCount, AverageDanger, cdd);
      break;
    }
    tryCount += 1;
    // check for too powerful ghost, because 'cmon!
    // very powerful player could create a very powerful ghost, prevent this.
    if (!Ghost->EditAllAttributes(1) || tryCount > maxTries) {
      cdd = CalculateAverageDanger(EnemyVector, Ghost);
      if (cdd > AverageDanger) {
        //Player->SetSoulID(0);
        //delete Ghost;
        //Ghost = 0;
        ConLogf("ghost: failed to create a proper ghost after %u tries; avg=%g; cdd=%g",
                tryCount, AverageDanger, cdd);
      }
      break;
    }
  }
  #endif
  for (uInt c = 0; c < EnemyVector.size(); ++c) {
    delete EnemyVector[c];
  }
  if (Ghost) {
    DumpGhostInfo(Ghost);
  }
  return Ghost;
}


//==========================================================================
//
//  game::MoveKeyToDir
//
//  return -1, or dir; if ctrl is allowed and pressed, return 1000+dir
//
//==========================================================================
int game::MoveKeyToDir (int Key, bool allowFastExt) {
  return commandsystem::MoveKeyToDir(Key, allowFastExt);
}


//==========================================================================
//
//  game::GetScore
//
//==========================================================================
sLong game::GetScore () {
  double Counter = 0;
  massacremap::const_iterator i;
  massacremap SumMap = PlayerMassacreMap;
  for (i = PetMassacreMap.begin(); i != PetMassacreMap.end(); ++i) {
    killdata &KillData = SumMap[i->first];
    KillData.Amount += i->second.Amount;
    KillData.DangerSum += i->second.DangerSum;
  }
  for (i = SumMap.begin(); i != SumMap.end(); ++i) {
    auto monsProto = protocontainer<character>::GetProto(i->first.Type);
    if (!monsProto) continue; // missing character
    character *Char = monsProto->Spawn(i->first.Config);
    int SumOfAttributes = Char->GetSumOfAttributes();
    Counter += sqrt(i->second.DangerSum / DEFAULT_GENERATION_DANGER) * SumOfAttributes * SumOfAttributes;
    delete Char;
  }
  return sLong(0.01*Counter);
}


//==========================================================================
//
//  game::TweraifIsFree
//
//  Only works if New Attnam is loaded
//
//==========================================================================
truth game::TweraifIsFree () {
  for (std::list<character *>::const_iterator i = GetTeam(COLONIST_TEAM)->GetMember().begin();
       i != GetTeam(COLONIST_TEAM)->GetMember().end(); ++i)
  {
    if ((*i)->IsEnabled()) return false;
  }
  return true;
}


//==========================================================================
//
//  game::IsXMas
//
//  returns true if date is christmaseve or day
//
//==========================================================================
truth game::IsXMas () {
  time_t Time = time(0);
  struct tm *TM = localtime(&Time);
  return (TM->tm_mon == 11 && (TM->tm_mday == 24 || TM->tm_mday == 25));
}


//==========================================================================
//
//  game::AddToItemDrawVector
//
//==========================================================================
int game::AddToItemDrawVector (const itemvector &What) {
  ItemDrawVector.push_back(What);
  return ItemDrawVector.size()-1;
}


v2 ItemDisplacement[3][3] = {
  { v2(0, 0), ERROR_V2, ERROR_V2 },
  { v2(-2, -2), v2(2, 2), ERROR_V2 },
  { v2(-4, -4), v2(0, 0), v2(4, 4) }
};


//==========================================================================
//
//  game::ItemEntryDrawer
//
//==========================================================================
void game::ItemEntryDrawer (bitmap *Bitmap, v2 Pos, uInt I) {
  blitdata B = {
    Bitmap,
    { 0, 0 },
    { 0, 0 },
    { TILE_SIZE, TILE_SIZE },
    { NORMAL_LUMINANCE },
    TRANSPARENT_COLOR,
    ALLOW_ANIMATE
  };
  itemvector ItemVector = ItemDrawVector[I];
  int Amount = Min<int>(ItemVector.size(), 3);
  for (int c = 0; c < Amount; ++c) {
    v2 Displacement = ItemDisplacement[Amount-1][c];
    if (!ItemVector[0]->HasNormalPictureDirection()) Displacement.X = -Displacement.X;
    B.Dest = Pos+Displacement;
    if (ItemVector[c]->AllowAlphaEverywhere()) B.CustomData |= ALLOW_ALPHA;
    ItemVector[c]->Draw(B);
    B.CustomData &= ~ALLOW_ALPHA;
  }
  if (ItemVector.size() > 3) {
    B.Src.X = 0;
    B.Src.Y = 16;
    B.Dest = ItemVector[0]->HasNormalPictureDirection() ? Pos+v2(11, -2) : Pos+v2(-2, -2);
    B.Flags = 0;
    igraph::GetSymbolGraphic()->NormalMaskedBlit(B);
  }
}


//==========================================================================
//
//  game::AddToCharacterDrawVector
//
//==========================================================================
int game::AddToCharacterDrawVector (character *What) {
  CharacterDrawVector.push_back(What);
  return CharacterDrawVector.size()-1;
}


//==========================================================================
//
//  game::CharacterEntryDrawer
//
//==========================================================================
void game::CharacterEntryDrawer (bitmap *Bitmap, v2 Pos, uInt I) {
  if (CharacterDrawVector[I]) {
    blitdata B = {
      Bitmap,
      { 0, 0 },
      { Pos.X, Pos.Y },
      { TILE_SIZE, TILE_SIZE },
      { NORMAL_LUMINANCE },
      TRANSPARENT_COLOR,
      ALLOW_ANIMATE|ALLOW_ALPHA
    };
    CharacterDrawVector[I]->DrawBodyParts(B);
  }
}


//==========================================================================
//
//  game::GodEntryDrawer
//
//==========================================================================
void game::GodEntryDrawer (bitmap *Bitmap, v2 Pos, uInt I) {
  blitdata B = {
    Bitmap,
    { (int)I << 4, 0 },
    { Pos.X, Pos.Y },
    { TILE_SIZE, TILE_SIZE },
    { 0 },
    TRANSPARENT_COLOR,
    0
  };
  igraph::GetSymbolGraphic()->NormalMaskedBlit(B);
}


//==========================================================================
//
//  game::GetSumo
//
//==========================================================================
character *game::GetSumo () {
  return GetCurrentLevel()->GetLSquare(SUMO_ROOM_POS)->GetRoom()->GetMaster();
}


//==========================================================================
//
//  game::TryToEnterSumoArena
//
//==========================================================================
truth game::TryToEnterSumoArena () {
  #if 0
  // moved to script event
  character *Sumo = GetSumo();
  if (!Sumo || !Sumo->IsEnabled() || Sumo->GetRelation(Player) == HOSTILE || !Player->CanBeSeenBy(Sumo)) return true;
  if (TweraifIsFree()) {
    ADD_MESSAGE("\"You started this stupid revolution, after which I've been constantly hungry. Get lost!\"");
    return false;
  }
  if (PlayerIsSumoChampion()) {
    ADD_MESSAGE("\"I don't really enjoy losing, especially many times to the same guy. Go away.\"");
    return false;
  }
  if (Player->IsPolymorphed()) {
    ADD_MESSAGE("\"Don't try to cheat. Come back when you're normal again.\"");
    return false;
  }
  if (Player->GetHungerState() < SATIATED) {
    ADD_MESSAGE("\"Your figure is too slender for this sport. Eat a lot more and come back.\"");
    return false;
  }
  if (Player->GetHungerState() < BLOATED) {
    ADD_MESSAGE("\"You're still somewhat too thin. Eat some more and we'll compete.\"");
    return false;
  }
  ADD_MESSAGE("\"So you want to compete? Okay, I'll explain the rules. First, I'll make a mirror image out of us both. We'll enter the arena and fight till one is knocked out. Use of any equipment is not allowed. Note that we will not gain experience from fighting as a mirror image, but won't get really hurt, either. However, controlling the image is exhausting and you can get hungry very quickly.\"");
  if (!TruthQuestion("Do you want to challenge him?")) return false;
  #else
  character *Sumo = GetSumo();
  if (!Sumo) return false;
  #endif

  pool::AbortBe();
  SumoWrestling = true;
  character *MirrorPlayer = Player->Duplicate(IGNORE_PROHIBITIONS);
  character *MirrorSumo = Sumo->Duplicate(IGNORE_PROHIBITIONS);
  SetPlayer(MirrorPlayer);
  charactervector Spectators;
  if (Player->GetTeam()->GetRelation(GetTeam(TOURIST_GUIDE_TEAM)) != HOSTILE &&
      Player->GetTeam()->GetRelation(GetTeam(TOURIST_TEAM)) != HOSTILE) {
    GetTeam(TOURIST_GUIDE_TEAM)->MoveMembersTo(Spectators);
    GetTeam(TOURIST_TEAM)->MoveMembersTo(Spectators);
  }
  //GetCurrentDungeon()->SaveLevel(SaveName(), 0);
  SaveCurrentDungeonLevel(0);
  charactervector test;
  EnterArea(test, 1, STAIRS_UP);
  MirrorSumo->PutTo(SUMO_ARENA_POS+v2(6, 5));
  MirrorSumo->ChangeTeam(GetTeam(SUMO_TEAM));
  GetCurrentLevel()->GetLSquare(SUMO_ARENA_POS)->GetRoom()->SetMasterID(MirrorSumo->GetID());
  for (uInt c = 0; c < Spectators.size(); ++c) Spectators[c]->PutToOrNear(SUMO_ARENA_POS + v2(6, 10));
  throw areachangerequest();
  return true;
}


//==========================================================================
//
//  game::TryToExitSumoArena
//
//==========================================================================
truth game::TryToExitSumoArena () {
  if (GetTeam(PLAYER_TEAM)->GetRelation(GetTeam(NEW_ATTNAM_TEAM)) == HOSTILE) return true;
  itemvector IVector;
  charactervector CVector;
  if (IsSumoWrestling()) {
    #if 0
    // done in script
    if (TruthQuestion("Do you really wish to give up?")) return EndSumoWrestling(LOST);
    return false;
    #else
    return EndSumoWrestling(LOST);
    #endif
  } else {
    pool::AbortBe();
    Player->Remove();
    GetCurrentLevel()->CollectEverything(IVector, CVector);
    //GetCurrentDungeon()->SaveLevel(SaveName(), 1);
    SaveCurrentDungeonLevel(1);
    std::vector<character*> test;
    EnterArea(test, 0, STAIRS_DOWN);
    Player->GetStackUnder()->AddItems(IVector);
    if (!IVector.empty()) {
      character *Sumo = GetSumo();
      if (Sumo && Sumo->GetRelation(Player) != HOSTILE && Player->CanBeSeenBy(Sumo)) ADD_MESSAGE("\"Don't leave anything there, please.\"");
    }
    v2 PlayerPos = Player->GetPos();
    for (uInt c = 0; c < CVector.size(); ++c) CVector[c]->PutNear(PlayerPos);
    throw areachangerequest();
    return true;
  }
}


//==========================================================================
//
//  game::EndSumoWrestling
//
//==========================================================================
truth game::EndSumoWrestling (int Result) {
  pool::AbortBe();
  msgsystem::LeaveBigMessageMode();

  if (Result == LOST) {
    AskForSpacePress(CONST_S("You lose. [press any key to continue]"));
  } else if (Result == WON) {
    AskForSpacePress(CONST_S("You win! [press any key to continue]"));
  } else if (Result == DISQUALIFIED) {
    AskForSpacePress(CONST_S("You are disqualified! [press any key to continue]"));
  }
  character *Sumo = GetCurrentLevel()->GetLSquare(SUMO_ARENA_POS)->GetRoom()->GetMaster();
  /* We'll make a throw soon so deletes are allowed */

  if (Sumo) {
    Sumo->Remove();
    delete Sumo;
    //Sumo->SendToHell();
  }
  Player->Remove();
  delete Player; //k8: wut?!
  SetPlayer(0);

  itemvector IVector;
  charactervector CVector;

  GetCurrentLevel()->CollectEverything(IVector, CVector);
  //GetCurrentDungeon()->SaveLevel(SaveName(), 1);
  SaveCurrentDungeonLevel(0);

  charactervector test;
  EnterArea(test, 0, STAIRS_DOWN);
  SumoWrestling = false;
  Player->GetStackUnder()->AddItems(IVector);
  v2 PlayerPos = Player->GetPos();
  for (uInt c = 0; c < CVector.size(); ++c) CVector[c]->PutNear(PlayerPos);
  if (Result == LOST) {
    ADD_MESSAGE("\"I hope you've learned your lesson now!\"");
  } else if (Result == DISQUALIFIED) {
    ADD_MESSAGE("\"Don't do that again or I'll be really angry!\"");
  } else {
    PlayerSumoChampion = true;
    character *Sumo = GetSumo();
    festring Msg = Sumo->GetName(DEFINITE)+" seems humbler than before. \"Darn. You bested me.\n";
    Msg << "Here's a little something as a reward\", " << Sumo->GetPersonalPronoun() << " says and hands you a belt of levitation.\n\"";
    (belt::Spawn(BELT_OF_LEVITATION))->MoveTo(Player->GetStack());
    Msg << "Allow me to also teach you a few nasty martial art tricks the years have taught me.\"";
    Player->GetCWeaponSkill(UNARMED)->AddHit(100000);
    Player->GetCWeaponSkill(KICK)->AddHit(100000);
    character *Imperialist = GetCurrentLevel()->GetLSquare(5, 5)->GetRoom()->GetMaster();
    if (Imperialist && Imperialist->GetRelation(Player) != HOSTILE) {
      v2 Pos = Player->GetPos()+v2(0, 1);
      GetCurrentLevel()->GetLSquare(Pos)->KickAnyoneStandingHereAway();
      Imperialist->Remove();
      Imperialist->PutTo(Pos);
      Msg << "\n\nSuddenly you notice " << Imperialist->GetName(DEFINITE) << " has also entered.\n"
        "\"I see we have a promising fighter among us. I had already heard of your\n"
        "adventures outside the village, but hardly could I believe that one day you\n"
        "would defeat even the mighty Huang Ming Pong! A hero such as you is bound\n"
        "to become world famous, and can earn a fortune if wealthy sponsors are behind\n"
        "him. May I therefore propose a mutually profitable contract: I'll give you this\n"
        "nice shirt with my company's ad, and you'll wear it as you journey bravely to\n"
        "the unknown and fight epic battles against the limitless minions of evil. I'll\n"
        "reward you well when you return, depending on how much you have used it.\"";
      Player->GetStack()->AddItem(decosadshirt::Spawn());
    }
    TextScreen(Msg);
    GetCurrentArea()->SendNewDrawRequest();
    DrawEverything();
  }
  Player->EditNP(-25000);
  Player->CheckStarvationDeath(CONST_S("exhausted after controlling a mirror image for too long"));

  throw areachangerequest();
  return true;
}


//==========================================================================
//
//  game::ConstructGlobalRain
//
//==========================================================================
rain *game::ConstructGlobalRain () {
  return new rain(GlobalRainLiquid, static_cast<lsquare *>(GetSquareInLoad()),
                  GlobalRainSpeed, MONSTER_TEAM, false);
}


//==========================================================================
//
//  game::GetSunLightDirectionVector
//
//==========================================================================
v2 game::GetSunLightDirectionVector () {
  int Index = Tick % 48000 / 1000;
  /* Should have the same sign as sin(PI * Index / 24) and XTable[Index] /
     YTable[Index] should equal roughly -tan(PI * Index / 24). Also, vector
     (XTable[Index], YTable[Index]) + P should not be a valid position of
     any possible level L for any P belonging to L. */
  static int XTable[48] = {
           0,  1000,  1000,  1000,  1000,  1000,
        1000,  1303,  1732,  2414,  3732,  7596,
        1000,  7596,  3732,  2414,  1732,  1303,
        1000,  1000,  1000,  1000,  1000,  1000,
            0, -1000, -1000, -1000, -1000, -1000,
        -1000, -1303, -1732, -2414, -3732, -7596,
        -1000, -7596, -3732, -2414, -1732, -1303,
        -1000, -1000, -1000, -1000, -1000, -1000 };
  /* Should have the same sign as -cos(PI * Index / 24) */
  static int YTable[48] = { -1000, -7596, -3732, -2414, -1732, -1303,
          -1000, -1000, -1000, -1000, -1000, -1000,
          0,  1000,  1000,  1000,  1000,  1000,
          1000,  1303,  1732,  2414,  3732,  7596,
          1000,  7596,  3732,  2414,  1732,  1303,
          1000,  1000,  1000,  1000,  1000,  1000,
          0, -1000, -1000, -1000, -1000, -1000,
          -1000, -1303, -1732, -2414, -3732, -7596 };
  return v2(XTable[Index], YTable[Index]);
}


//==========================================================================
//
//  game::CalculateMinimumEmitationRadius
//
//==========================================================================
int game::CalculateMinimumEmitationRadius (col24 E) {
  int MaxElement = Max(GetRed24(E), GetGreen24(E), GetBlue24(E));
  return int(sqrt(double(MaxElement << 7) / LIGHT_BORDER - 120.));
}


//==========================================================================
//
//  game::IncreaseSquarePartEmitationTicks
//
//==========================================================================
feuLong game::IncreaseSquarePartEmitationTicks () {
  if ((SquarePartEmitationTick += 2) == 0x100) {
    CurrentLevel->InitSquarePartEmitationTicks();
    SquarePartEmitationTick = 2;
  }
  return SquarePartEmitationTick;
}


//==========================================================================
//
//  game::Wish
//
//==========================================================================
bool game::Wish (character *Wisher, cchar *MsgSingle, cchar *MsgPair, bool canAbort) {
  for (;;) {
    festring oldDef = DefaultWish;
    festring Temp = DefaultQuestion(CONST_S("What do you want to wish for?"), DefaultWish);
    if (DefaultWish == "nothing" && canAbort) {
      DefaultWish = oldDef;
      return false;
    }
         if (Temp == "socm") Temp = "scroll of change material";
    else if (Temp == "soc") Temp = "scroll of charging";
    else if (Temp == "sodm") Temp = "scroll of detect material";
    else if (Temp == "soea") Temp = "scroll of enchant armor";
    else if (Temp == "soew") Temp = "scroll of enchant weapon";
    else if (Temp == "sof") Temp = "scroll of fireballs";
    else if (Temp == "sogc") Temp = "scroll of golem creation";
    else if (Temp == "sohm") Temp = "scroll of harden material";
    else if (Temp == "sor") Temp = "scroll of repair";
    else if (Temp == "sot") Temp = "scroll of taming";
    else if (Temp == "sotp") Temp = "scroll of teleportation";
    else if (Temp == "sow") Temp = "scroll of wishing";
    else if (Temp == "vodka") Temp = "bottle full of vodka";
    else if (Temp == "troll blood") Temp = "bottle full of troll blood";
    //else if (game::WizardModeIsActive() && Temp == "kawaii") Temp = "Kawaii";
    //else if (game::WizardModeIsActive() && Temp == "superkawaii") Temp = "SuperKawai";

    item *TempItem = protosystem::CreateItem(Temp, Wisher->IsPlayer());
    if (TempItem) {
      Wisher->GetStack()->AddItem(TempItem);
      TempItem->SpecialGenerationHandler();
      if (TempItem->HandleInPairs()) ADD_MESSAGE(MsgPair, TempItem->CHAR_NAME(PLURAL));
      else ADD_MESSAGE(MsgSingle, TempItem->CHAR_NAME(INDEFINITE));
      return true;
    }
  }
}


//==========================================================================
//
//  game::DefaultQuestion
//
//==========================================================================
festring game::DefaultQuestion (festring Topic, festring &Default, stringkeyhandler KeyHandler) {
  festring ShortDefault = Default;
  if (Default.GetSize() > 29) {
    ShortDefault.Resize(27);
    ShortDefault = ShortDefault << CONST_S("...");
  }
  if (!Default.IsEmpty()) Topic << " [" << ShortDefault << ']';
  festring Answer = StringQuestion(Topic, WHITE, 0, 80, false, KeyHandler);
  if (Answer.IsEmpty()) Answer = Default;
  return Default = Answer;
}


//==========================================================================
//
//  game::GetTime
//
//==========================================================================
void game::GetTime (ivantime &Time) {
  Time.Hour = 12 + Tick / 2000;
  Time.Day = Time.Hour / 24 + 1;
  Time.Hour %= 24;
  Time.Min = Tick % 2000 * 60 / 2000;
}


//==========================================================================
//
//  NameOrderer
//
//==========================================================================
truth NameOrderer (character *C1, character *C2) {
  return festring::CompareCILess(C1->GetName(UNARTICLED), C2->GetName(UNARTICLED));
}


//==========================================================================
//
//  game::PolymorphControlKeyHandler
//
//==========================================================================
truth game::PolymorphControlKeyHandler (int Key, festring &String) {
  if (Key == KEY_F1) {
    felist List(CONST_S("List of known creatures and their intelligence requirements"));
    SetStandardListAttributes(List);
    List.SetPageLength(15);
    List.AddFlags(SELECTABLE);
    protosystem::CreateEverySeenCharacter(CharacterDrawVector);
    std::sort(CharacterDrawVector.begin(), CharacterDrawVector.end(), NameOrderer);
    List.SetEntryDrawer(CharacterEntryDrawer);
    std::vector<festring> StringVector;
    uInt c;
    for (c = 0; c < CharacterDrawVector.size(); ++c) {
      character *Char = CharacterDrawVector[c];
      if (Char->CanBeWished()) {
        festring Entry;
        Char->AddName(Entry, UNARTICLED);
        StringVector.push_back(Entry);
        int Req = Char->GetPolymorphIntelligenceRequirement();
        if (Char->IsSameAs(Player) || (Player->GetPolymorphBackup() && Player->GetPolymorphBackup()->IsSameAs(Char))) Req = 0;
        Entry << " (" << Req << ')';
        int Int = Player->GetAttribute(INTELLIGENCE);
        List.AddEntry(Entry, Req > Int ? RED : LIGHT_GRAY, 0, c);
      }
    }
    int Chosen = List.Draw();
    for (c = 0; c < CharacterDrawVector.size(); ++c) delete CharacterDrawVector[c];
    if (!(Chosen & FELIST_ERROR_BIT)) String = StringVector[Chosen];
    CharacterDrawVector.clear();
    return true;
  }
  return false;
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const killdata &Value) {
  SaveFile << Value.Amount << Value.DangerSum << Value.Reason;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, killdata &Value) {
  SaveFile >> Value.Amount >> Value.DangerSum >> Value.Reason;
  return SaveFile;
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const killreason &Value) {
  SaveFile << Value.Amount << Value.String;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, killreason &Value) {
  SaveFile >> Value.Amount >> Value.String;
  return SaveFile;
}


//==========================================================================
//
//  DistanceOrderer
//
//==========================================================================
truth DistanceOrderer (character *C1, character *C2) {
  v2 PlayerPos = PLAYER->GetPos();
  v2 Pos1 = C1->GetPos();
  v2 Pos2 = C2->GetPos();
  int D1 = Max(abs(Pos1.X - PlayerPos.X), abs(Pos1.Y - PlayerPos.Y));
  int D2 = Max(abs(Pos2.X - PlayerPos.X), abs(Pos2.Y - PlayerPos.Y));
  if (D1 != D2) return D1 < D2;
  if (Pos1.Y != Pos2.Y) return Pos1.Y < Pos2.Y;
  return Pos1.X < Pos2.X;
}


//==========================================================================
//
//  game::FillPetVector
//
//==========================================================================
truth game::FillPetVector (cchar *Verb) {
  PetVector.clear();
  team *Team = GetTeam(PLAYER_TEAM);
  for (std::list<character*>::const_iterator i = Team->GetMember().begin();
        i != Team->GetMember().end(); ++i)
  {
    if ((*i)->IsEnabled() && !(*i)->IsPlayer() && (*i)->CanBeSeenByPlayer()) PetVector.push_back(*i);
  }
  if (PetVector.empty()) {
    ADD_MESSAGE("You don't detect any friends to %s.", Verb);
    return false;
  }
  std::sort(PetVector.begin(), PetVector.end(), DistanceOrderer);
  LastPetUnderCursor = PetVector[0];
  return true;
}


//==========================================================================
//
//  game::CommandQuestion
//
//==========================================================================
truth game::CommandQuestion () {
  if (!FillPetVector("command")) return false;
  character *Char;
  if (PetVector.size() == 1) {
    Char = PetVector[0];
  } else {
    v2 Pos = PetVector[0]->GetPos();
    Pos = PositionQuestion(CONST_S("Whom do you wish to command? "
                                   "[direction keys/\1G+\2/\1G-\2/\1Ga\2ll/\1Gspace\2/esc]"),
                           Pos, &PetHandler, &CommandKeyHandler);
    if (Pos == ERROR_V2) return false;
    if (Pos == ABORT_V2) return true;
    Char = GetCurrentArea()->GetSquare(Pos)->GetCharacter();
    if (!Char || !Char->CanBeSeenByPlayer()) {
      ADD_MESSAGE("You don't see anyone here to command.");
      return false;
    }
    if (Char->IsPlayer()) {
      ADD_MESSAGE("You do that all the time.");
      return false;
    }
    if (!Char->IsPet()) {
      ADD_MESSAGE("%s refuses to be commanded by you.", Char->CHAR_NAME(DEFINITE));
      return false;
    }
  }
  return Char->IssuePetCommands();
}


//==========================================================================
//
//  game::NameQuestion
//
//==========================================================================
void game::NameQuestion () {
  if (!FillPetVector("name")) return;
  if (PetVector.size() == 1) {
    PetVector[0]->TryToName();
  } else {
    PositionQuestion(CONST_S("Who do you want to name? "
                             "[direction keys/\1G+\2/\1G-\2/\1Gn\2ame/esc]"),
                     PetVector[0]->GetPos(), &PetHandler, &NameKeyHandler);
  }
}


//==========================================================================
//
//  game::PetHandler
//
//==========================================================================
void game::PetHandler (v2 CursorPos) {
  character *Char = GetCurrentArea()->GetSquare(CursorPos)->GetCharacter();
  if (Char && Char->CanBeSeenByPlayer() && Char->IsPet() && !Char->IsPlayer()) {
    CursorData = RED_CURSOR|CURSOR_TARGET|CURSOR_SHADE;
  } else {
    CursorData = RED_CURSOR;
  }
  if (Char && !Char->IsPlayer() && Char->IsPet()) LastPetUnderCursor = Char;
}


//==========================================================================
//
//  game::CommandKeyHandler
//
//==========================================================================
v2 game::CommandKeyHandler (v2 CursorPos, int Key) {
  if (SelectPet(Key)) {
    return LastPetUnderCursor->GetPos();
  }
  if (KEY_EQU(Key, "A")) {
    return (CommandAll() ? ABORT_V2 : ERROR_V2);
  }
  return CursorPos;
}


//==========================================================================
//
//  game::SelectPet
//
//==========================================================================
truth game::SelectPet (int Key) {
  if (Key == KEY_TAB) {
    for (uInt c = 0; c < PetVector.size(); ++c) {
      if (PetVector[c] == LastPetUnderCursor) {
        if (++c == PetVector.size()) c = 0;
        LastPetUnderCursor = PetVector[c];
        return true;
      }
    }
  } else if (KEY_EQU(Key, "S-Tab")) {
    for (uInt c = 0; c < PetVector.size(); ++c) {
      if (PetVector[c] == LastPetUnderCursor) {
        if (!c) c = PetVector.size();
        LastPetUnderCursor = PetVector[--c];
        return true;
      }
    }
  }
  return false;
}


//==========================================================================
//
//  game::CommandScreen
//
//==========================================================================
void game::CommandScreen (cfestring &Topic, feuLong PossibleFlags, feuLong ConstantFlags,
                          feuLong &VaryFlags, feuLong &Flags)
{
  const cchar *CommandDescription[COMMAND_FLAGS] = {
    "Follow me",
    "Flee from enemies",
    "Don't change your equipment",
    "Don't consume anything valuable"
  };
  festring cc; cc << "Command                                                        ";
  int wdt = FONT->TextWidth(cc);
  for (int f = 0; f != COMMAND_FLAGS; f += 1) {
    wdt = Max(wdt, FONT->TextWidth(CommandDescription[f]));
  }
  FONT->RPadToPixWidth(cc, wdt); cc << "Active?";
  felist List(Topic);
  SetStandardListAttributes(List);
  List.AddFlags(SELECTABLE);
  List.AddDescription(CONST_S(""));
  List.AddDescription(cc);
  for (;;) {
    int c, i;
    for (c = 0; c < COMMAND_FLAGS; ++c) {
      if (1 << c & PossibleFlags) {
        truth Changeable = !(1 << c & ConstantFlags);
        festring Entry;
        if (Changeable) {
          Entry = CommandDescription[c];
          //Entry.Resize(60);
        } else {
          Entry << "   " << CommandDescription[c];
          //Entry.Resize(63);
        }
        FONT->RPadToPixWidth(Entry, wdt);
        if (1 << c & VaryFlags) Entry << "varies"; else Entry << (1 << c & Flags ? "yes" : "no");
        List.AddEntry(Entry, Changeable ? LIGHT_GRAY : DARK_GRAY, 0, NO_IMAGE, Changeable);
      }
    }
    int Chosen = List.Draw();
    if (Chosen & FELIST_ERROR_BIT) return;
    for (c = 0, i = 0; c < COMMAND_FLAGS; ++c) {
      if (1 << c & PossibleFlags && !(1 << c & ConstantFlags) && i++ == Chosen) {
        if (1 << c & VaryFlags) {
          VaryFlags &= ~(1 << c);
          Flags |= 1 << c;
        } else Flags ^= 1 << c;
        break;
      }
    }
    List.Empty();
    DrawEverythingNoBlit();
  }
}


//==========================================================================
//
//  game::CommandAll
//
//==========================================================================
truth game::CommandAll () {
  feuLong PossibleFlags = 0, ConstantFlags = ALL_COMMAND_FLAGS, VaryFlags = 0, OldFlags = 0;
  uInt c1, c2;
  for (c1 = 0; c1 < PetVector.size(); ++c1) {
    ConstantFlags &= PetVector[c1]->GetConstantCommandFlags();
    feuLong C = PetVector[c1]->GetCommandFlags();
    feuLong ThisPossible = PetVector[c1]->GetPossibleCommandFlags();
    for (c2 = 0; c2 < COMMAND_FLAGS; ++c2)
      if (1 << c2 & PossibleFlags & ThisPossible && (1 << c2 & C) != (1 << c2 & OldFlags)) VaryFlags |= 1 << c2;
    PossibleFlags |= ThisPossible;
    OldFlags |= C & ThisPossible;
  }
  if (!PossibleFlags) {
    ADD_MESSAGE("Not a single creature in your visible team can be commanded.");
    return false;
  }
  feuLong NewFlags = OldFlags;
  CommandScreen(CONST_S("Issue commands to whole visible team"), PossibleFlags, ConstantFlags, VaryFlags, NewFlags);
  truth Change = false;
  for (c1 = 0; c1 < PetVector.size(); ++c1) {
    character *Char = PetVector[c1];
    if (!Char->IsConscious()) continue;
    feuLong OldC = Char->GetCommandFlags();
    feuLong ConstC = Char->GetConstantCommandFlags();
    feuLong ThisC = (NewFlags & Char->GetPossibleCommandFlags() & ~(ConstC|VaryFlags)) | (OldC & (ConstC|VaryFlags));
    if (ThisC != OldC) Change = true;
    Char->SetCommandFlags(ThisC);
  }
  if (!Change) return false;
  Player->EditAP(-500);
  Player->EditExperience(CHARISMA, 50, 1 << 7);
  return true;
}


//==========================================================================
//
//  game::GetAttributeColor
//
//==========================================================================
col16 game::GetAttributeColor (int I) {
  int Delta = GetTick()-LastAttributeChangeTick[I];
  if (OldAttribute[I] == NewAttribute[I] || Delta >= 510) return WHITE;
  if (OldAttribute[I] < NewAttribute[I]) return MakeRGB16(255, 255, Delta >> 1);
  return MakeRGB16(255, Delta >> 1, Delta >> 1);
}


//==========================================================================
//
//  game::UpdateAttributeMemory
//
//==========================================================================
void game::UpdateAttributeMemory () {
  for (int c = 0; c < ATTRIBUTES; ++c) {
    int A = Player->GetAttribute(c);
    if (A != NewAttribute[c]) {
      OldAttribute[c] = NewAttribute[c];
      NewAttribute[c] = A;
      LastAttributeChangeTick[c] = GetTick();
    }
  }
}


//==========================================================================
//
//  game::InitAttributeMemory
//
//==========================================================================
void game::InitAttributeMemory () {
  for (int c = 0; c < ATTRIBUTES; ++c) {
    OldAttribute[c] = NewAttribute[c] = Player->GetAttribute(c);
  }
}


//==========================================================================
//
//  game::TeleportHandler
//
//==========================================================================
void game::TeleportHandler (v2 CursorPos) {
  if ((CursorPos-Player->GetPos()).GetLengthSquare() > Player->GetTeleportRangeSquare()) {
    CursorData = BLUE_CURSOR|CURSOR_TARGET|CURSOR_SHADE;
  } else {
    CursorData = RED_CURSOR|CURSOR_TARGET|CURSOR_SHADE;
  }
}


//==========================================================================
//
//  game::GetGameSituationDanger
//
//==========================================================================
double game::GetGameSituationDanger () {
  double SituationDanger = 0;
  character *Player = GetPlayer();
  truth PlayerStuck = Player->IsStuck();
  v2 PlayerPos = Player->GetPos();
  character *TruePlayer = Player;
  if (PlayerStuck) (Player = Player->Duplicate(IGNORE_PROHIBITIONS))->ChangeTeam(0);
  for (int c1 = 0; c1 < GetTeams(); ++c1) {
    if (GetTeam(c1)->GetRelation(GetTeam(PLAYER_TEAM)) == HOSTILE) {
      for (std::list<character*>::const_iterator i1 = GetTeam(c1)->GetMember().begin();
           i1 != GetTeam(c1)->GetMember().end(); ++i1)
      {
        character *Enemy = *i1;
        if (Enemy->IsEnabled() && Enemy->CanAttack() && (Enemy->CanMove() || Enemy->GetPos().IsAdjacent(PlayerPos))) {
          truth EnemyStuck = Enemy->IsStuck();
          v2 EnemyPos = Enemy->GetPos();
          truth Sees = TruePlayer->CanBeSeenBy(Enemy);
          character *TrueEnemy = Enemy;
          if (EnemyStuck) Enemy = Enemy->Duplicate(IGNORE_PROHIBITIONS);
          double PlayerTeamDanger = 1/Enemy->GetSituationDanger(Player, EnemyPos, PlayerPos, Sees);
          for (int c2 = 0; c2 < GetTeams(); ++c2)
            if (GetTeam(c2)->GetRelation(GetTeam(c1)) == HOSTILE)
              for (std::list<character*>::const_iterator i2 = GetTeam(c2)->GetMember().begin(); i2 != GetTeam(c2)->GetMember().end(); ++i2) {
                character *Friend = *i2;
                if (Friend->IsEnabled() && !Friend->IsPlayer() && Friend->CanAttack() && (Friend->CanMove() || Friend->GetPos().IsAdjacent(EnemyPos))) {
                  v2 FriendPos = Friend->GetPos();
                  truth Sees = TrueEnemy->CanBeSeenBy(Friend);
                  if (Friend->IsStuck()) {
                    Friend = Friend->Duplicate(IGNORE_PROHIBITIONS);
                    PlayerTeamDanger += Friend->GetSituationDanger(Enemy, FriendPos, EnemyPos, Sees) * .2;
                    delete Friend;
                  } else PlayerTeamDanger += Friend->GetSituationDanger(Enemy, FriendPos, EnemyPos, Sees);
                }
              }
          if (EnemyStuck) {
            PlayerTeamDanger *= 5;
            delete Enemy;
          }
          SituationDanger += 1 / PlayerTeamDanger;
        }
      }
    }
  }
  Player->ModifySituationDanger(SituationDanger);
  if (PlayerStuck) {
    SituationDanger *= 2;
    delete Player;
  }
  return SituationDanger;
}


//==========================================================================
//
//  game::GetTimeSpent
//
//==========================================================================
time_t game::GetTimeSpent () {
  return time::TimeAdd(time::TimeDifference(time(0), LastLoad), TimePlayedBeforeLastLoad);
}


//==========================================================================
//
//  GenBoneFileNameBase
//
//==========================================================================
static inline festring GenBoneFileName (int dungeonIdx, int levelIdx) {
  festring fname;
  fname << "bones/dungeon_" << dungeonIdx << "_" << levelIdx << ".lvl";
  return fname;
}


//==========================================================================
//
//  game::CreateBone
//
//==========================================================================
void game::CreateBone () {
  if (IsCheating() || IsInWilderness()) return;

  if (!BONE_DEBUG || !DEBUG_ALWAYS_CREATE_BONES) {
    if (NG_RAND_4) {
      ConLogf("rejected creation bone for 'dungeon_%d_%d' (%s) (prng)...",
                CurrentDungeonIndex, CurrentLevelIndex,
                game::GetCurrentDungeon()->GetShortLevelDescription(game::GetCurrentLevelIndex()).CStr());
      return;
    }
  }

  if (!GetCurrentLevel()->PreProcessForBone()) {
    ConLogf("rejected bone creation for 'dungeon_%d_%d' (%s) (forbidden)...",
              CurrentDungeonIndex, CurrentLevelIndex,
              game::GetCurrentDungeon()->GetShortLevelDescription(game::GetCurrentLevelIndex()).CStr());
    return;
  }

  {
    festring BoneName;
    int BoneIndex = (int)time(NULL);
    int trycount = 50;
    while (trycount != 0) {
      BoneName.Empty();
      BoneName << game::GetBonePath() << "bones_" << BoneIndex;
      BoneName << ".bone";
      if (!inputfile::fileExists(BoneName)) break;
      trycount -= 1;
    }

    if (!BoneName.IsEmpty()) {
      if (OpenSaveArchive(BoneName, true)) {
        #ifndef SHITDOZE
        ConLogf("creating bone file for 'dungeon_%d_%d' (%s) (%s)...",
                CurrentDungeonIndex, CurrentLevelIndex,
                game::GetCurrentDungeon()->GetShortLevelDescription(game::GetCurrentLevelIndex()).CStr(),
                BoneName.CStr());
        #endif
        outputfile BoneFile(-1, GenBoneFileName(CurrentDungeonIndex, CurrentLevelIndex),
                            9/*maximum compression, why not?*/, true);
        BoneFile << int(BONE_FILE_VERSION);
        SaveModuleList(BoneFile);
        BoneFile
          << CurrentDungeonIndex
          << CurrentLevelIndex
          << PlayerName
          << CurrentLevel
        ;
      }
      SaveArchiveVacuum(); // just in case
      if (!CloseSaveArchive()) {
        remove(BoneName.CStr());
      }
    }
  }
}


//==========================================================================
//
//  game::TryLoadBone
//
//==========================================================================
bool game::TryLoadBone (cfestring &boneFileName, int LevelIndex) {
  inputfile BoneFile(-1, boneFileName, true);
  if (!BoneFile.IsOpen()) return false;

  if (ReadType(int, BoneFile) != BONE_FILE_VERSION) return false;

  if (!LoadAndCheckModuleList(BoneFile)) return false;

  int32_t didx, lidx;
  BoneFile >> didx >>lidx;
  if (didx != CurrentDungeonIndex || lidx != LevelIndex) return false;

  festring Name;
  BoneFile >> Name;
  level *NewLevel = GetCurrentDungeon()->LoadLevel(BoneFile, LevelIndex);
  if (!NewLevel->PostProcessForBone()) {
    delete NewLevel;
    GetBoneItemIDMap().clear();
    GetBoneCharacterIDMap().clear();
    return false;
  }

  BoneFile.Close();

  NewLevel->FinalProcessForBone();
  GetBoneItemIDMap().clear();
  GetBoneCharacterIDMap().clear();
  SetCurrentArea(NewLevel);
  CurrentLevel = NewLevel;
  CurrentLSquareMap = NewLevel->GetMap();
  GetCurrentDungeon()->SetIsGenerated(LevelIndex, true);

  if (Name == PlayerName) {
    ADD_MESSAGE("This place is oddly familiar. Like you had been here in one of your past lives.");
  } else if (Player && Player->StateIsActivated(GAS_IMMUNITY)) {
    ADD_MESSAGE("You feel the cool breeze of death.");
  } else {
    ADD_MESSAGE("You smell the stench of death.");
  }

  return true;
}


//==========================================================================
//
//  game::PrepareRandomBone
//
//==========================================================================
truth game::PrepareRandomBone (int LevelIndex) {
  if (/*k8:WizardModeIsActive() ||*/ GetCurrentDungeon()->IsGenerated(LevelIndex) ||
      !*GetCurrentDungeon()->GetLevelScript(LevelIndex)->CanGenerateBone())
  {
    return false;
  }

  // bone loading chance
  if (!BONE_DEBUG || !DEBUG_ALWAYS_LOAD_BONES) {
    if (NG_RAND_8) {
      return false;
    }
  }

  // set by the caller
  //Generating = true;

  // scan bone files
  std::vector<festring> goodBones;
  festring bnx = GenBoneFileName(CurrentDungeonIndex, LevelIndex);

  DIR *dp = opendir(game::GetBonePath().CStr());
  if (dp) {
    struct dirent *ep;
    while ((ep = readdir(dp))) {
      festring buf;
      buf << ep->d_name;
      if (buf.StartsWithCI("bones_") && buf.EndsWithCI(".bone")) {
        bool ok = false;
        festring b2 = game::GetBonePath() + buf;
        SQArchive sqa;
        sqa.Open(b2, false);
        if (sqa.IsOpen()) {
          ok = sqa.FileExists(bnx);
          sqa.Close();
          if (ok) {
            ConLogf("possible bone file for 'dungeon_%d_%d' (%s)...",
                    CurrentDungeonIndex, LevelIndex, b2.CStr());
          } else {
            ConLogf("rejected bone file for 'dungeon_%d_%d' (%s).",
                    CurrentDungeonIndex, LevelIndex, b2.CStr());
          }
        }
        if (ok) goodBones.push_back(b2);
      }
    }
    closedir(dp);
  }

  if (goodBones.empty()) return false;

  // load a bone
  const int BoneIndex = NG_RAND_N((int)goodBones.size());

  SQArchive *sqbone = new SQArchive();
  sqbone->Open(goodBones[BoneIndex], false);
  if (!sqbone->IsOpen()) {
    // just in case
    delete sqbone;
    remove(goodBones[BoneIndex].CStr());
    return false;
  }

  ConLogf("loading bone file for 'dungeon_%d_%d' (%s)...",
          CurrentDungeonIndex, LevelIndex, goodBones[BoneIndex].CStr());

  bool ldr;
  {
    SaveArchiveTempSwap sqsx(&sqbone); // will delete `sqbone` object
    ldr = game::TryLoadBone(bnx, LevelIndex);
  }
  IvanAssert(!sqbone);
  remove(goodBones[BoneIndex].CStr());

  return ldr;
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const massacreid &MI) {
  SaveFile << MI.Type << MI.Config << MI.Name;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, massacreid &MI) {
  SaveFile >> MI.Type >> MI.Config >> MI.Name;
  return SaveFile;
}


//==========================================================================
//
//  game::PlayerIsRunning
//
//==========================================================================
truth game::PlayerIsRunning () {
  return PlayerRunning && Player->CanMove();
}


//==========================================================================
//
//  game::AddSpecialCursor
//
//==========================================================================
void game::AddSpecialCursor (v2 Pos, int Data) {
  SpecialCursorPos.push_back(Pos);
  SpecialCursorData.push_back(Data);
  SpecialCursorsEnabled = true; // just in case
}


//==========================================================================
//
//  game::RemoveSpecialCursors
//
//==========================================================================
void game::RemoveSpecialCursors () {
  SpecialCursorPos.clear();
  SpecialCursorData.clear();
  SpecialCursorsEnabled = true; // just in case
}


//==========================================================================
//
//  game::CheckDropLeftover
//
//==========================================================================
truth game::CheckDropLeftover (item *i) {
  if (i->IsBottle()) {
    return (ivanconfig::GetAutoDropBottles() && !i->GetSecondaryMaterial());
  }
  if (i->IsCan()) {
    return (ivanconfig::GetAutoDropCans() && !i->GetSecondaryMaterial());
  }
  if (!ivanconfig::GetAutoDropLeftOvers()) return false;
  return true;
}


//==========================================================================
//
//  game::LearnAbout
//
//==========================================================================
void game::LearnAbout (god *Who) {
  Who->SetIsKnown(true);
  /* slightly slow, but doesn't matter since this is run so rarely */
  if (PlayerKnowsAllGods() && !game::PlayerHasReceivedAllGodsKnownBonus) {
    GetPlayer()->ApplyAllGodsKnownBonus();
    game::PlayerHasReceivedAllGodsKnownBonus = true;
  }
}


//==========================================================================
//
//  game::PlayerKnowsAllGods
//
//==========================================================================
truth game::PlayerKnowsAllGods () {
  for (int c = 1; c <= GODS; ++c) if (!GetGod(c)->IsKnown()) return false;
  return true;
}


//==========================================================================
//
//  game::AdjustRelationsToAllGods
//
//==========================================================================
void game::AdjustRelationsToAllGods (int Amount) {
  for (int c = 1; c <= GODS; ++c) GetGod(c)->AdjustRelation(Amount);
}


//==========================================================================
//
//  game::SetRelationsToAllGods
//
//==========================================================================
void game::SetRelationsToAllGods (int Amount) {
  for (int c = 1; c <= GODS; ++c) GetGod(c)->SetRelation(Amount);
}


//==========================================================================
//
//  game::HighlightSquare
//
//==========================================================================
void game::HighlightSquare (v2 pos) {
  if (GetCurrentArea() && GetCurrentArea()->IsValidPos(pos)) {
    square *Square = GetCurrentArea()->GetSquare(pos);
    if (Square) {
      Square->SendStrongNewDrawRequest();
      /*
      game::DrawEverythingNoBlit();
      //HACK: temporarily bump luminance
      const col24 oldLumi = ivanconfig::GetContrastLuminance();
      int newLumi = Clamp((int)(oldLumi & 0xff) + 96, 0, 255);
      ivanconfig::SetTempContrastLuminance(MakeRGB24(newLumi, newLumi, newLumi / 4));
      Square->SendStrongNewDrawRequest();
      GetCurrentArea()->Draw(false);
      ivanconfig::SetTempContrastLuminance(oldLumi);
      graphics::BlitDBToScreen();
      */
      HiSquare = pos;
      graphics::BlitDBToScreen();
    }
  }
}


//==========================================================================
//
//  game::UnHighlightSquare
//
//==========================================================================
void game::UnHighlightSquare () {
  if (HiSquare != ERROR_V2) {
    if (GetCurrentArea() && GetCurrentArea()->IsValidPos(HiSquare)) {
      square *Square = GetCurrentArea()->GetSquare(HiSquare);
      if (Square) {
        Square->SendStrongNewDrawRequest();
        //game::DrawEverythingNoBlit();
      }
    }
    HiSquare = ERROR_V2;
  }
}


//==========================================================================
//
//  game::ShowDeathSmiley
//
//==========================================================================
void game::ShowDeathSmiley (bitmap *Buffer, truth) {
  static blitdata B = {
    0,
    { 0, 0 },
    { (RES.X >> 1) - 24, RES.Y * 4 / 7 - 24 },
    { 48, 48 },
    { 0 },
    TRANSPARENT_COLOR,
    0
  };
  int Tick = globalwindowhandler::UpdateTick();
  if (((Tick >> 1) & 31) == 1) B.Src.X = 48;
  else if (((Tick >> 1) & 31) == 2) B.Src.X = 96;
  else B.Src.X = 0;
  B.Bitmap = Buffer;
  igraph::GetSmileyGraphic()->NormalBlit(B);
  if (Buffer == DOUBLE_BUFFER) {
    graphics::BlitDBToScreen();
  }
}


//==========================================================================
//
//  DoListSelector
//
//==========================================================================
static int DoListSelector (felist &list, int defsel, int cnt) {
  game::SetStandardListAttributes(list);
  list.AddFlags(SELECTABLE | FELIST_NO_BADKEY_EXIT);
  if (defsel > 0) list.SetSelected(defsel);
  uInt sel = list.Draw();
  list.Empty();
  list.RemoveFlags(SELECTABLE | FELIST_NO_BADKEY_EXIT);
  if (sel & FELIST_ERROR_BIT) return -1;
  if (sel >= (uInt)cnt) return -1;
  return (int)sel;
}


//==========================================================================
//
//  game::ListSelector
//
//==========================================================================
int game::ListSelector (int defsel, const cfestring title, ...) {
  int cnt = 0;
  va_list items;
  va_start(items, title);
  felist list(title);
  for (;;) {
    const char *s = va_arg(items, const char *);
    if (!s) break;
    list.AddEntry(CONST_S(s), LIGHT_GRAY);
    cnt++;
  }
  va_end(items);
  return DoListSelector(list, defsel, cnt);
}


//==========================================================================
//
//  game::ListSelectorArray
//
//==========================================================================
int game::ListSelectorArray (int defsel, cfestring &title, cfestring &msg, const char *items[]) {
  int cnt = 0;
  felist list(title);
  if (!msg.IsEmpty()) {
    list.AddDescription(CONST_S(""));
    festring s = CONST_S("Endgame cause: \1Y") + msg;
    if (!strchr(".?!", s[s.GetSize() - 1])) s.AppendChar('.');
    game::SetStandardListAttributes(list); // i need to know the width
    const int wdt = (int)list.GetWidth() - 64;
    if (wdt > 64) {
      std::vector<festring> lines;
      FONT->WordWrap(s, lines, wdt, 4);
      for (size_t f = 0; f != lines.size(); f += 1) {
        list.AddDescription(lines[f]);
      }
    } else {
      list.AddDescription(s);
    }
  }
  while (items[cnt]) {
    list.AddEntry(CONST_S(items[cnt]), LIGHT_GRAY);
    cnt += 1;
  }
  return DoListSelector(list, defsel, cnt);
}


//==========================================================================
//
//  game::GetWord
//
//==========================================================================
truth game::GetWord (festring &w) {
  for (;;) {
    TextInput *fl = mFEStack.top();
    fl->ReadWord(w, false);
    if (w == "" && fl->Eof()) {
      delete fl;
      mFEStack.pop();
      if (mFEStack.empty()) return false;
      continue;
    }
    if (w == "Include") {
      fl->ReadWord(w, true);
      if (fl->ReadWord() != ";") ABORT("Invalid terminator in file %s at line %d!", fl->GetFileName().CStr(), fl->TokenLine());
      TextInput *flx = new TextInputFile(inputfile::buildIncludeName(fl->GetFileName(), w), &game::GetGlobalValueMap(), true);
      flx->SetGetVarCB(&game::ldrGetVar);
      mFEStack.push(flx);
      continue;
    }
    if (w == "Message") {
      fl->ReadWord(w, true);
      if (fl->ReadWord() != ";") ABORT("Invalid terminator in file %s at line %d!", fl->GetFileName().CStr(), fl->TokenLine());
      ConLogf("MESSAGE: %s", w.CStr());
      continue;
    }
    return true;
  }
}


struct CleanuperTextInput {
  TextInput *fl;
  CleanuperTextInput (TextInput *afl) { fl = afl; }
  ~CleanuperTextInput () { delete fl; }
};


template<typename cltp> struct Cleanuper {
  cltp *var;
  cltp oval;
  Cleanuper (cltp *avar) { var = avar; oval = *avar; }
  Cleanuper (cltp *avar, cltp vnew) { var = avar; oval = *avar; *avar = vnew; }
  ~Cleanuper () { *var = oval; }
};


using ContextSaver = Cleanuper<EventContext>;


EventHandlerMap game::mGlobalEvents;


//==========================================================================
//
//  game::LoadGlobalEvents
//
//==========================================================================
void game::LoadGlobalEvents () {
  mGlobalEvents.Clear();

  if (mFEStack.size() != 0) ABORT("Ooops...");

  // add module files
  for (auto &modname : mModuleList) {
    festring infname = game::GetGamePath()+"script/"+modname+"/onevent.def";
    if (!inputfile::DataFileExists(infname)) continue;
    TextInput *ifl = new TextInputFile(infname, &game::GetGlobalValueMap());
    ifl->SetGetVarCB(&game::ldrGetVar);
    mFEStack.push(ifl);
  }

  festring w;
  while (GetWord(w)) {
    if (w != "on") ABORT("'on' expected in file %s line %d!", mFEStack.top()->GetFileName().CStr(), mFEStack.top()->TokenLine());
    TextInput *ti = mFEStack.top();
    mGlobalEvents.CollectSource(mFEStack, &ti, -1);
  }
}


//==========================================================================
//
//  game::ParseFuncArgs
//
//  '.': string or number
//  'n': number
//  's': string
//  'c': named constant
//  '*': collect all args
//
//==========================================================================
int game::ParseFuncArgs (TextInput *ifl, const char *typesz, std::vector<FuncArg> &args, truth noterm) {
  festring types = CONST_S(typesz);
  festring s;
  sLong n;
  truth isStr;
  if (!ifl) ABORT("The thing that should not be");

  if (noterm) {
    s = ifl->ReadWord();
    if (s != "(") {
      ABORT("'(' expected in file %s line %d!", ifl->GetFileName().CStr(), ifl->TokenLine());
    }
  }

  args.clear();

  for (size_t f = 0; f < types.GetSize(); f++) {
    if (f != 0) {
      s = ifl->ReadWord();
      if (s != ",") ABORT("',' expected in file %s line %d!", ifl->GetFileName().CStr(), ifl->TokenLine());
    }
    switch (types[f]) {
      case '.':
        s = ifl->ReadStringOrNumber(&n, &isStr, true);
        if (isStr) args.push_back(FuncArg(s)); else args.push_back(FuncArg(n));
        break;
      case 'n':
        n = ifl->ReadNumber(0xFF, true);
        args.push_back(FuncArg(n));
        break;
      case 'c': // named constant
        {
          s = ifl->ReadWord();
          sLong cc = GetGlobalConst(s);
          args.push_back(FuncArg(cc));
          break;
        }
      case '*':
        for (;;) {
          s = ifl->ReadStringOrNumber(&n, &isStr, true);
          if (isStr) args.push_back(FuncArg(s)); else args.push_back(FuncArg(n));
          s = ifl->ReadWord();
          if (s == ";") return args.size();
          if (s != ",") ABORT("',' expected in file %s line %d!", ifl->GetFileName().CStr(), ifl->TokenLine());
        }
        // never reached
      case 's':
      default:
        s = ifl->ReadWord();
        args.push_back(FuncArg(s));
        break;
    }
  }

  s = ifl->ReadWord();
  char exp = 0;
  if (noterm) {
    if (s != ")") exp = ')';
  } else {
    if (s != ";") exp = ';';
  }

  if (exp) {
    ABORT("'%c' expected in file %s line %d!", exp, ifl->GetFileName().CStr(), ifl->TokenLine());
  }

  return args.size();
}


//==========================================================================
//
//  game::DoOnEvent
//
//==========================================================================
void game::DoOnEvent (const EventHandlerMap &emap, sLong configNumber, cfestring &ename, truth defaultAllow) {
  if (!curevctx.allowed()) return;

  if (defaultAllow) {
    curevctx.allow();
  } else {
    curevctx.disallow();
  }

  auto ifl = emap.OpenHandler(ename, configNumber, &game::GetGlobalValueMap());
  if (!ifl) return;

  auto clnp = CleanuperTextInput(ifl);
  ifl->SetGetVarCB(&game::ldrGetVar);

  int brclevel = 0;
  festring w;

  for (;;) {
    if (!ifl->ReadWord(w, false)) break;
    if (w == "{") { ++brclevel; continue; }
    if (w == "}") { if (--brclevel <= 0) break; continue; }
    if (w == ";") continue;
    if (w == "@") {
      w = ifl->ReadWord();
      if (ifl->ReadWord() != "=") {
        ABORT("'=' expected in file %s at line %d!", ifl->GetFileName().CStr(), ifl->TokenLine());
      }
      if (w == "money") {
        sLong n = ifl->ReadNumber(true);
        if (n < 0) n = 0;
        if (curevctx.actor) curevctx.actor->SetMoney(n);
        continue;
      }
      if (w == "result") {
        curevctx.result = ifl->ReadNumber(true);
        continue;
      }
      ABORT("Unknown var [%s] in file %s at line %d!", w.CStr(),
            ifl->GetFileName().CStr(), ifl->TokenLine());
    } else {
      std::vector<FuncArg> args;

      if (w == "SetMoney") {
        ParseFuncArgs(ifl, "n", args);
        sLong n = args[0].ival;
        if (n < 0) n = 0;
        if (curevctx.actor) curevctx.actor->SetMoney(n);
        continue;
      }

      if (w == "EditMoney") {
        ParseFuncArgs(ifl, "n", args);
        sLong n = args[0].ival;
        if (curevctx.actor) curevctx.actor->EditMoney(n);
        continue;
      }

      if (w == "EditAP") {
        ParseFuncArgs(ifl, "n", args);
        sLong n = args[0].ival;
        if (curevctx.actor) curevctx.actor->EditAP(n);
        continue;
      }

      if (w == "AddMessage") {
        ParseFuncArgs(ifl, "*", args);
        festring s;
        for (uInt f = 0; f < args.size(); f++) {
          const FuncArg &a = args[f];
          if (a.type == FARG_STRING) s << a.sval; else s << a.ival;
        }
        ADD_MESSAGE("%s", s.CStr());
        continue;
      }

      if (w == "AddItem") {
        ParseFuncArgs(ifl, "s", args);
        if (curevctx.actor) {
          item *newItem = protosystem::CreateItemForScript(args[0].sval);
          if (newItem) {
            curevctx.actor->GetStack()->AddItem(newItem);
            newItem->SpecialGenerationHandler();
            /*
            if (newItem->HandleInPairs()) {
              ADD_MESSAGE(MsgPair, newItem->CHAR_NAME(PLURAL));
            } else {
              ADD_MESSAGE(MsgSingle, newItem->CHAR_NAME(INDEFINITE));
            }
            */
          }
        }
        continue;
      }

      if (w == "SetOtherToSumo") {
        ParseFuncArgs(ifl, "", args);
        curevctx.secondActor = GetSumo();
        continue;
      }

      if (w == "YesNoQuery") {
        ParseFuncArgs(ifl, "s", args);
        if (game::TruthQuestion(args[0].sval)) {
          curevctx.lastQueryRes = 1;
        } else {
          curevctx.lastQueryRes = 0;
        }
        continue;
      }

      if (w == "Disallow") {
        ParseFuncArgs(ifl, "", args);
        curevctx.disallow();
        return;
      }
      if (w == "Allow") {
        ParseFuncArgs(ifl, "", args);
        curevctx.allow();
        return;
      }
      if (w == "AllowStop") {
        ParseFuncArgs(ifl, "", args);
        curevctx.allowStop();
        return;
      }
      ABORT("Unknown function [%s] in file %s at line %d!", w.CStr(),
            ifl->GetFileName().CStr(), ifl->TokenLine());
    }
  }
}


//==========================================================================
//
//  game::ldrGetVar
//
//==========================================================================
festring game::ldrGetVar (TextInput *fl, cfestring &name, truth scvar) {
  //ConLogf("GETVAR: [%s]", name.CStr());
  if (scvar) {
    if (name == "player_name") {
      return game::GetPlayerName();
    }

    if (name == "money") {
      festring res;
      if (!curevctx.actor) return CONST_S("0");
      res << curevctx.actor->GetMoney();
      return res;
    }

    if (name == "name") {
      if (!curevctx.actor) return festring();
      return curevctx.actor->GetAssignedName();
    }

    if (name == "team") {
      festring res;
      if (!curevctx.actor) return festring();
      res << curevctx.actor->GetTeam()->GetID();
      return res;
    }

    if (name == "friendly") {
      festring res;
      if (!curevctx.actor || !PLAYER || curevctx.actor->GetRelation(PLAYER) != HOSTILE) {
        return CONST_S("tan");
      }
      return festring();
    }

    if (name == "hostile") {
      festring res;
      if (!curevctx.actor || !PLAYER) return festring();
      if (curevctx.actor->GetRelation(PLAYER) == HOSTILE) return CONST_S("tan");
      return festring();
    }

    if (name == "has_item") {
      std::vector<FuncArg> args;
      ParseFuncArgs(fl, "s", args, true);
      return CONST_S(curevctx.actor && curevctx.actor->FindFirstItem(args[0].sval) ? "tan" : "");
    }

    if (name == "player_has_item") {
      std::vector<FuncArg> args;
      ParseFuncArgs(fl, "s", args, true);
      return CONST_S(PLAYER && PLAYER->FindFirstItem(args[0].sval) ? "tan" : "");
    }

    if (name == "last_query_bool") {
      return CONST_S(curevctx.lastQueryRes != 0 ? "tan" : "");
    }

    if (name == "item_name") {
      festring res;
      if (!curevctx.thing) return festring();
      res << curevctx.thing->GetNameSingular();
      return res;
    }

    if (name == "item_config") {
      festring res;
      if (!curevctx.thing) return CONST_S("0");
      res << curevctx.thing->GetConfig();
      return res;
    }

    if (name == "stairs_config") {
      festring res;
      if (!curevctx.strs) return CONST_S("0");
      res << curevctx.strs->GetConfig();
      return res;
    }

    if (name == "xinroch_story_state") {
      festring res;
      res << game::GetXinrochTombStoryState();
      return res;
    }

    if (name == "player_move_etherial") {
      return CONST_S(PLAYER->GetMoveType() & ETHEREAL ? "tan" : "");
    }

    if (name == "other_actor") {
      return CONST_S(curevctx.secondActor ? "tan" : "");
    }

    if (name == "other_enabled") {
      return CONST_S(curevctx.secondActor && curevctx.secondActor->IsEnabled() ? "tan" : "");
    }

    if (name == "other_hostile") {
      return CONST_S(curevctx.secondActor && curevctx.secondActor->GetRelation(Player) == HOSTILE ? "tan" : "");
    }

    if (name == "other_can_be_seen") {
      return CONST_S(curevctx.secondActor && Player->CanBeSeenBy(curevctx.secondActor) ? "tan" : "");
    }

    if (name == "tweraif_is_free") {
      return CONST_S(TweraifIsFree() ? "tan" : "");
    }

    if (name == "player_sumo_champion") {
      return CONST_S(PlayerIsSumoChampion() ? "tan" : "");
    }

    if (name == "player_sumo_wrestling") {
      return CONST_S(IsSumoWrestling() ? "tan" : "");
    }

    if (name == "player_polymorphed") {
      return CONST_S(Player->IsPolymorphed() ? "tan" : "");
    }

    if (name == "player_hunger_state") {
      festring ires;
      ires << Player->GetHungerState();
      return ires;
    }

    if (name == "team_relation_to_team") {
      std::vector<FuncArg> args;
      ParseFuncArgs(fl, "cc", args, true);
      team *t0 = GetTeam(args[0].ival);
      if (!t0) fl->Error("cannot find first team");
      team *t1 = GetTeam(args[1].ival);
      if (!t1) fl->Error("cannot find second team");
      festring srel;
      srel << t0->GetRelation(t1);
      return srel;
    }

    //if (name == "type") return mVarType;
    fl->Error("unknown variable: %s", name.CStr());
    return festring();
  } else {
    auto it = GlobalValueMap.find(name);
    if (it == GlobalValueMap.end()) return name;
    festring res;
    res << it->second;
    return res;
  }
}


//==========================================================================
//
//  game::RunEventWithCtx
//
//==========================================================================
truth game::RunEventWithCtx (EventContext &ctx, const EventHandlerMap &evmap,
                             sLong configNumber, cfestring &ename,
                             truth defaultAllow)
{
  auto ctsv = ContextSaver(&curevctx, ctx);
  DoOnEvent(evmap, configNumber, ename, defaultAllow);
  ctx = curevctx;
  return curevctx.allowed();
}


//==========================================================================
//
//  game::HasGlobalEvent
//
//==========================================================================
truth game::HasGlobalEvent (cfestring &ename) {
  auto ifl = mGlobalEvents.OpenHandler(ename, -1, &game::GetGlobalValueMap());
  const bool res = !!ifl;
  return res;
}


//==========================================================================
//
//  game::RunGlobalEvent
//
//==========================================================================
truth game::RunGlobalEvent (cfestring &ename, truth defaultAllow) {
  EventContext ctx;
  ctx.actor = PLAYER;
  return RunEventWithCtx(ctx, mGlobalEvents, -1, ename, defaultAllow);
}


//==========================================================================
//
//  game::RunEnterEvent
//
//==========================================================================
truth game::RunEnterEvent (cfestring &ename, character *who, stairs *strs) {
  if (!who) return false;
  EventContext ctx;
  ctx.actor = who;
  ctx.secondActor = 0;
  ctx.strs = strs;
  if (!RunEventWithCtx(ctx, who->mOnEvents, who->GetConfig(), ename, true)) return false;
  if (!ctx.wantnext()) return true;
  return RunEventWithCtx(ctx, strs->GetProtoType()->mOnEvents, strs->GetConfig(), ename, true);
}


//==========================================================================
//
//  game::RunCharEvent
//
//==========================================================================
truth game::RunCharEvent (cfestring &ename, character *who, character *second, item *it) {
  if (!who) return true;
  EventContext ctx;
  ctx.actor = who;
  ctx.secondActor = second;
  ctx.thing = it;
  if (!RunEventWithCtx(ctx, who->mOnEvents, who->GetConfig(), ename, true)) return false;
  if (!ctx.wantnext()) return true;
  return RunEventWithCtx(ctx, who->GetProtoType()->mOnEvents, who->GetConfig(), ename, true);
}


//==========================================================================
//
//  game::RunItemEvent
//
//==========================================================================
truth game::RunItemEvent (cfestring &ename, item *what, character *holder, character *second) {
  if (!what) return true;
  EventContext ctx;
  ctx.actor = holder;
  ctx.secondActor = second;
  ctx.thing = what;
  if (!RunEventWithCtx(ctx, what->mOnEvents, what->GetConfig(), ename, true)) return false;
  if (!ctx.wantnext()) return true;
  return RunEventWithCtx(ctx, what->GetProtoType()->mOnEvents, what->GetConfig(), ename, true);
}


//==========================================================================
//
//  game::RunCharAllowScript
//
//==========================================================================
truth game::RunCharAllowScript (character *tospawn, const EventHandlerMap &evmap, cfestring &ename) {
  EventContext ctx;
  ctx.actor = tospawn;
  auto ctsv = ContextSaver(&curevctx, ctx);
  DoOnEvent(evmap, tospawn->GetConfig(), ename, true);
  return curevctx.allowed();
}


//==========================================================================
//
//  game::RunItemAllowScript
//
//==========================================================================
truth game::RunItemAllowScript (item *tospawn, const EventHandlerMap &evmap, cfestring &ename) {
  EventContext ctx;
  ctx.thing = tospawn;
  auto ctsv = ContextSaver(&curevctx, ctx);
  DoOnEvent(evmap, tospawn->GetConfig(), ename, true);
  return curevctx.allowed();
}


// ////////////////////////////////////////////////////////////////////////// //
// minimap handler
// ////////////////////////////////////////////////////////////////////////// //


//==========================================================================
//
//  game::CalcMiniSquareScrSize
//
//==========================================================================
v2 game::CalcMiniSquareScrSize () {
  const int tsz = (mMiniMapSmall ? 4 : 8);
  return v2(tsz, tsz);
}


//==========================================================================
//
//  game::CalcMiniSquareScrCoords
//
//  return `ERROR_V2` if out of screen
//
//==========================================================================
v2 game::CalcMiniSquareScrCoords (v2 mappos) {
  if (!GetCurrentArea() || mappos.X < 0 || mappos.Y < 0 ||
      mappos.X >= GetCurrentArea()->GetXSize() || mappos.Y >= GetCurrentArea()->GetYSize())
  {
    return ERROR_V2;
  }

  if (mappos.X < mMiniMapXY0.X || mappos.Y < mMiniMapXY0.Y ||
      mappos.X >= mMiniMapXY0.X + GetMiniMapXSize() ||
      mappos.Y >= mMiniMapXY0.Y + GetMiniMapYSize())
  {
    return ERROR_V2;
  }

  const v2 tsz = CalcMiniSquareScrSize();
  v2 ScrPos = mMiniMapScrPos;
  ScrPos.X += (mappos.X - mMiniMapXY0.X) * tsz.X;
  ScrPos.Y += (mappos.Y - mMiniMapXY0.Y) * tsz.Y;

  return ScrPos;
}


//==========================================================================
//
//  game::DrawMiniMapMarks
//
//==========================================================================
void game::DrawMiniMapMarks () {
  const int mcount = GetCurrentArea()->GetMapMarkCount();
  for (int f = 0; f != mcount; f += 1) {
    area::MapMark mark;
    if (!GetCurrentArea()->GetMapMark(f, &mark)) {
      ABORT("internal error in map mark manager: mark #%d should be present, but it is stolen!",
            f);
    }
    const v2 ScrPos = CalcMiniSquareScrCoords(mark.pos);
    if (ScrPos != ERROR_V2) {
      // draw "+" symbol
      blitdata BlitData = {
        Bitmap:DOUBLE_BUFFER,
        Src:{ 0, 16 }, /* src: '+' symbol position */
        Dest:ScrPos, /* dest */
        Border:{8, 8}, /* size */
        { Flags:0 }, /* lumi; flags/stretch */
        MaskColor:TRANSPARENT_COLOR, /* mask color */
        CustomData:0/*ALLOW_ANIMATE|ALLOW_ALPHA*/, /* custom data */
      };
      if (mMiniMapSmall) {
        BlitData.Src = v2(0, 352);
      } else {
        BlitData.Src = v2(16, 352);
      }
      BlitData.Luminance = ivanconfig::GetContrastLuminance();
      igraph::GetSymbolGraphic()->LuminanceMaskedBlit(BlitData);
    }
  }
}


//==========================================================================
//
//  game::DrawMiniMapCursors
//
//==========================================================================
void game::DrawMiniMapCursors () {
  v2 ScrPos;
  const v2 tsz = CalcMiniSquareScrSize();

  const int xmul = (((GET_TICK()<<2)/3)>>2)%16;
  int alpha;
  if (xmul < 8) {
    alpha = 160 * xmul / 8;
  } else {
    alpha = 160 * (15 - xmul) / 8;
  }

  // exits (special cursors)
  for (size_t f = 0; f != SpecialCursorPos.size(); f += 1) {
    ScrPos = CalcMiniSquareScrCoords(SpecialCursorPos[f]);
    if (ScrPos != ERROR_V2) {
      for (int dx = 0; dx != tsz.X; dx += 1) {
        for (int dy = 0; dy != tsz.Y; dy += 1) {
          DOUBLE_BUFFER->AlphaPutPixel(ScrPos.X + dx, ScrPos.Y + dy,
                                       MakeRGB16(255, 255, 0),
                                       MakeRGB24(255, 255, 0), alpha);
        }
      }
    }
  }

  // main cursor
  ScrPos = CalcMiniSquareScrCoords(mMiniMapCurPos);
  if (ScrPos != ERROR_V2) {
    for (int dx = 0; dx != tsz.X; dx += 1) {
      for (int dy = 0; dy != tsz.Y; dy += 1) {
        DOUBLE_BUFFER->AlphaPutPixel(ScrPos.X + dx, ScrPos.Y + dy,
                                     MakeRGB16(255, 127, 0),
                                     MakeRGB24(255, 127, 0), 96 + alpha / 2);
      }
    }
  }
}


struct MMHelpTopic {
  cchar *key;
  cchar *text;
};

static const MMHelpTopic mmHelp[] = {
  {key:"Esc", text:"Exit"},
  {key:"Insert", text:"Insert/edit map marker"},
  {key:"Delete", text:"Delete map marker"},
  {key:"Tab", text:"Next map marker"},
  {key:"S-Tab", text:"Previous map marker"},
  {key:"@", text:"Return cursor to player position"},
  {key:"L", text:"Describe square under cursor"},
  {key:NULL, text:NULL},
};


//==========================================================================
//
//  game::MiniMapHelp
//
//==========================================================================
void game::MiniMapHelp () {
  festring Buffer;

  felist List(CONST_S("Keyboard Layout"));
  List.AddDescription(CONST_S(""));

  // calculate maximum length of key name
  int xww = 0;
  for (int c = 0; mmHelp[c].text; c += 1) {
    festring kname = CONST_S(mmHelp[c].key);
    if (!kname.IsEmpty()) {
      xww = Max(xww, FONT->TextWidth(kname));
    }
  }

  festring hdr = CONST_S("Key");
  int kkwdt = Max(FONT->TextWidth(hdr) + 6, xww + 6);
  FONT->PadCenter(hdr, kkwdt);
  hdr << "Description";
  List.AddDescription(hdr);

  const char *c1[2] = { "\1#aa0|", "\1#ff0|" };
  const col16 c2[2] = { LIGHT_GRAY, MakeRGB16(192 + 16, 192 + 16, 192 + 16) };
  int cidx = 0;

  for (int c = 0; mmHelp[c].text; c += 1) {
    festring kname = CONST_S(mmHelp[c].key);
    if (!kname.IsEmpty()) {
      Buffer.Empty();
      Buffer << c1[cidx];
      Buffer << kname;
      Buffer << "\2";
      FONT->PadCenter(Buffer, xww);
      //FONT->LPadToPixWidth(Buffer, xww);
      FONT->RPadToPixWidth(Buffer, kkwdt);
      Buffer << mmHelp[c].text;
      List.AddEntry(Buffer, c2[cidx]); cidx ^= 1;
    }
  }

  game::SetStandardListAttributes(List);
  List.Draw();
}


//==========================================================================
//
//  game::MiniMapAppendMarkAt
//
//==========================================================================
void game::MiniMapAppendMarkAt (v2 pos) {
  if (pos.X < 0 || pos.Y < 0 ||
      pos.X >= GetCurrentArea()->GetXSize() || pos.Y >= GetCurrentArea()->GetYSize())
  {
    return;
  }

  int oldidx = GetCurrentArea()->FindMapMarkAt(pos);
  festring text;
  area::MapMark mark;
  if (oldidx >= 0) {
    if (!GetCurrentArea()->GetMapMark(oldidx, &mark)) {
      ABORT("internal error in map mark manager: mark #%d should be present, but it is stolen!",
            oldidx);
    }
    text << "[ \1Y" << mark.text << "\2 ]";
  } else {
    text = "New marker text, or \1REsc\2";
  }
  truth aborted = false;
  text = StringQuestionEx(text, mark.text, WHITE, 0, 128, /*AllowExit*/true, 0, &aborted);
  if (!aborted) {
    mark.pos = pos;
    mark.color = TRANSPARENT_COLOR;
    mark.text = text;
    mark.type = 0;
    GetCurrentArea()->AppendMapMark(&mark);
  }
}


//==========================================================================
//
//  game::MiniMapDeleteMarkAt
//
//==========================================================================
void game::MiniMapDeleteMarkAt (v2 pos) {
  int oldidx = GetCurrentArea()->FindMapMarkAt(pos);
  if (oldidx >= 0) {
    if (TruthQuestion(CONST_S("Do you want to delete this marker?"))) {
      GetCurrentArea()->DeleteMapMark(oldidx);
    }
  }
}


//==========================================================================
//
//  game::MiniMapNextMarkFrom
//
//==========================================================================
v2 game::MiniMapNextMarkFrom (v2 pos) {
  int markidx = GetCurrentArea()->FindMapMarkAt(pos);
  if (markidx >= 0) {
    markidx = (markidx + 1) % GetCurrentArea()->GetMapMarkCount();
  } else {
    markidx = GetCurrentArea()->FindNearestMapMark(pos);
  }
  if (markidx >= 0) {
    area::MapMark mark;
    if (!GetCurrentArea()->GetMapMark(markidx, &mark)) {
      ABORT("internal error in map mark manager: mark #%d should be present, but it is stolen!",
            markidx);
    }
    return mark.pos;
  } else {
    return ERROR_V2;
  }
}


//==========================================================================
//
//  game::MiniMapPrevMarkFrom
//
//==========================================================================
v2 game::MiniMapPrevMarkFrom (v2 pos) {
  int markidx = GetCurrentArea()->FindMapMarkAt(pos);
  if (markidx >= 0) {
    markidx = (markidx + GetCurrentArea()->GetMapMarkCount() - 1) % GetCurrentArea()->GetMapMarkCount();
  } else {
    markidx = GetCurrentArea()->FindNearestMapMark(pos);
  }
  if (markidx >= 0) {
    area::MapMark mark;
    if (!GetCurrentArea()->GetMapMark(markidx, &mark)) {
      ABORT("internal error in map mark manager: mark #%d should be present, but it is stolen!",
            markidx);
    }
    return mark.pos;
  } else {
    return ERROR_V2;
  }
}


//==========================================================================
//
//  game::MiniMapDrawNoteAtText
//
//==========================================================================
void game::MiniMapDrawNoteAtText (v2 pos) {
  EraseTopArea();
  const int cursorMarkIdx = GetCurrentArea()->FindMapMarkAt(pos);
  if (cursorMarkIdx >= 0) {
    area::MapMark mark;
    if (!GetCurrentArea()->GetMapMark(cursorMarkIdx, &mark)) {
      ABORT("internal error in map mark manager: mark #%d should be present, but it is stolen!",
            cursorMarkIdx);
    }
    if (!mark.text.IsEmpty()) {
      v2 scrpos, scrsize;
      GetTopAreaPosSize(&scrpos, &scrsize);
      std::vector<festring> text;
      festring ss = CONST_S("Map note: \1Y");
      ss << mark.text;
      FONT->WordWrap(ss, text, scrsize.X - 8, 2);
      if (text.size() == 1) {
        FONT->PrintStr(DOUBLE_BUFFER, scrpos.X, scrpos.Y + (scrsize.Y - 6) / 2, WHITE, text[0]);
      } else if (text.size() > 1) {
        int y = scrpos.Y + Max(0, scrsize.Y - 16) / 2;
        for (size_t f = 0; f != 2; f += 1) {
          FONT->PrintStr(DOUBLE_BUFFER, scrpos.X, y, WHITE, text[f]);
          y += 10;
        }
      }
    }
  }
}


//==========================================================================
//
//  game::MiniMapProcessor
//
//==========================================================================
void game::MiniMapProcessor (bool small) {
  int Key = 0;
  SetDoZoom(false);
  SetMiniMapSmall(small);
  SetMiniMapActive(true);
  SetCursorPos(v2(-1, -1));

  RemoveSpecialCursors();
  if (!IsInWilderness() && PLAYER && PLAYER->IsEnabled()) {
    PLAYER->GetLevel()->AddSpecialCursors();
  }
  SpecialCursorsEnabled = false; // nope

  v2 curpos = PLAYER->GetPos();
  //CursorData = RED_CURSOR;
  auto stpos = curpos;
  for (;;) {
    if (curpos.X < mMiniMapXY0.X + 4) {
      mMiniMapXY0.X = curpos.X - 8;
    }
    if (curpos.Y < mMiniMapXY0.Y + 4) {
      mMiniMapXY0.Y = curpos.Y - 8;
    }
    if (curpos.X > mMiniMapXY0.X + (GetMiniMapXSize() - 4)) {
      mMiniMapXY0.X = curpos.X - (GetMiniMapXSize() - 8);
    }
    if (curpos.Y > mMiniMapXY0.Y + (GetMiniMapYSize() - 4)) {
      mMiniMapXY0.Y = curpos.Y - (GetMiniMapYSize() - 8);
    }

    const int lw = Max(0, GetCurrentArea()->GetXSize() - GetMiniMapXSize());
    const int lh = Max(0, GetCurrentArea()->GetYSize() - GetMiniMapYSize());
    mMiniMapXY0.X = Clamp(mMiniMapXY0.X, 0, lw);
    mMiniMapXY0.Y = Clamp(mMiniMapXY0.Y, 0, lh);

    mMiniMapCurPos = curpos;
    //SetCursorPos(curpos);
    DrawEverythingNoBlit();

    MiniMapDrawNoteAtText(curpos);

    // zoom
    if (true) {
      v2 ScreenCoord = curpos - mMiniMapXY0;
      if (ScreenCoord.X >= 0 && ScreenCoord.Y >= 0 &&
          ScreenCoord.X < GetMiniMapXSize() && ScreenCoord.Y < GetMiniMapYSize())
      {
        ScreenCoord *= TILE_SIZE;
        blitdata B = {
          DOUBLE_BUFFER,
          { 0, 0 },
          { ScreenCoord.X, ScreenCoord.Y },
          { TILE_SIZE, TILE_SIZE },
          { 0 },
          TRANSPARENT_COLOR,
          ALLOW_ANIMATE|ALLOW_ALPHA
        };
        B.Src = B.Dest;
        if (ivanconfig::GetStatusOnLeft()) {
          B.Dest.X = 16;
        } else {
          B.Dest.X = RES.X - 96;
        }
        B.Dest.Y = RES.Y - 96;
        B.Stretch = 5;
        mmBitmap->StretchBlit(B);
      }
    }

    graphics::BlitDBToScreen();

    Key = GET_KEY();
    LookKeyHandler(curpos, Key);

    if (Key == KEY_ESC /*|| (Key & KEY_STRIP_MOD_MASK) == KEY_TAB*/) break;

    // return back to start
    if (KEY_EQU(Key, "S-2")) {
      curpos = stpos;
    } else if (KEY_EQU(Key, "L") || Key == KEY_ENTER) {
      LookHandler(curpos);
    } else if (Key == KEY_INS) {
      MiniMapAppendMarkAt(curpos);
    } else if (Key == KEY_DEL) {
      MiniMapDeleteMarkAt(curpos);
    } else if (Key == KEY_TAB) {
      v2 newpos = MiniMapNextMarkFrom(curpos);
      if (newpos != ERROR_V2) curpos = newpos;
    } else if (KEY_EQU(Key, "S-Tab")) {
      v2 newpos = MiniMapPrevMarkFrom(curpos);
      if (newpos != ERROR_V2) curpos = newpos;
    } else if (Key == KEY_F1) {
      MiniMapHelp();
    } else {
      int Dir = MoveKeyToDir(Key, /*allowFastExt*/true);
      if (Dir >= 0 && (Dir % 100) < DIRECTION_COMMAND_KEYS) {
        v2 dv = GetMoveVector(Dir % 100);
        if (Dir >= 100) dv *= 6;
        curpos += dv;
      }
    }

    // just in case
    curpos.X = (curpos.X + GetCurrentArea()->GetXSize() * 6) % GetCurrentArea()->GetXSize();
    curpos.Y = (curpos.Y + GetCurrentArea()->GetYSize() * 6) % GetCurrentArea()->GetYSize();
  }

  EraseTopArea();

  SetDoZoom(false);
  SetMiniMapActive(false);
  SetCursorPos(v2(-1, -1));
  RemoveSpecialCursors();
  mMiniMapCurPos = v2(-1, -1);
  DrawEverything();
}


//==========================================================================
//
//  game::WipeItemNotes
//
//==========================================================================
void game::WipeItemNotes () {
  mItemNotes.clear();
}


//==========================================================================
//
//  game::RemoveItemNote
//
//==========================================================================
void game::RemoveItemNote (citem *Item) {
  if (Item) {
    auto it = mItemNotes.find(Item->GetID());
    if (it != mItemNotes.end()) {
      mItemNotes.erase(it);
    }
  }
}


//==========================================================================
//
//  game::HasItemNote
//
//==========================================================================
truth game::HasItemNote (citem *Item) {
  bool res = false;
  if (Item) {
    auto it = mItemNotes.find(Item->GetID());
    res = (it != mItemNotes.end());
  }
  return res;
}


//==========================================================================
//
//  game::GetItemNote
//
//==========================================================================
festring game::GetItemNote (citem *Item) {
  festring res;
  if (Item) {
    auto it = mItemNotes.find(Item->GetID());
    if (it != mItemNotes.end()) {
      res = it->second;
    }
  }
  return res;
}


//==========================================================================
//
//  game::SetItemNote
//
//==========================================================================
void game::SetItemNote (citem *Item, cfestring &note) {
  if (Item) {
    if (!note.IsEmpty()) {
      mItemNotes[Item->GetID()] = note;
    } else {
      RemoveItemNote(Item);
    }
  }
}
