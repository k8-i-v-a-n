/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

#ifndef __GAME_H__
#define __GAME_H__

#include <ctime>
#include <map>
#include <stack>
#include <vector>

#include "femath.h"
#include "festring.h"
#include "feparse.h"
#include "hscore.h"
#include "ivandef.h"


#ifndef LIGHT_BORDER
# define LIGHT_BORDER 80
#endif

#define PLAYER  game::GetPlayer()


class area;
class level;
class dungeon;
class felist;
class team;
class character;
class gamescript;
class item;
class outputfile;
class inputfile;
class worldmap;
class god;
class square;
class wsquare;
class lsquare;
class bitmap;
class festring;
class rain;
class liquid;
class entity;
class olterrain;
class owterrain;
struct explosion;

//typedef std::map<festring, sLong> valuemap;
typedef std::unordered_set<festring> mutablemap;
typedef truth (*stringkeyhandler)(int, festring&);
typedef v2 (*positionkeyhandler)(v2, int);
typedef void (*positionhandler)(v2);
typedef void (*bitmapeditor)(bitmap*, truth);


struct EventContext {
  enum ExecState {
    ESAllow,
    ESAllowStop,
    ESDisallow,
  };
  character *actor = nullptr; // "acting" actor, or item holder
  character *secondActor = nullptr; // actor that takes hit, or attacks, or...
  item *thing = nullptr; // used in `RunItemEvent()`
  class stairs *strs = nullptr;
  int result = 0;
  ExecState state = ESAllow;
  int lastQueryRes = 0;
  festring lastQueryResStr;

  inline void clear () {
    actor = nullptr;
    secondActor = nullptr; // actor that takes hit, or attacks, or...
    thing = nullptr; // used in `RunItemEvent()`
    strs = nullptr;
    result = 0;
    state = ESAllow;
    lastQueryRes = 0;
    lastQueryResStr.Empty();
  }

  inline truth allowed () const { return (state != ESDisallow); }
  inline truth wantnext () const { return (state == ESAllow); }

  inline void allow () { state = ESAllow; }
  inline void allowStop () { state = ESAllowStop; }
  inline void disallow () { state = ESDisallow; }
};


struct homedata {
  v2 Pos;
  int Dungeon;
  int Level;
  int Room;
};

outputfile &operator << (outputfile &, const homedata *);
inputfile &operator >> (inputfile &, homedata *&);


struct configid {
public:
  int Type;
  int Config;

public:
  FORCE_INLINE configid () : Type(0), Config(0) {}
  FORCE_INLINE configid (int Type, int Config) : Type(Type), Config(Config) {}
  FORCE_INLINE configid (const configid &src) : Type(src.Type), Config(src.Config) {}

  FORCE_INLINE configid &operator = (const configid &src) {
    Type = src.Type; Config = src.Config; return *this;
  }

  FORCE_INLINE bool operator < (const configid &CI) const {
    //return memcmp(this, &CI, sizeof(configid)) < 0;
    if (Type < CI.Type) return true;
    if (Type == CI.Type && Config < CI.Config) return true;
    return false;
  }

  FORCE_INLINE bool operator == (const configid &MI) const {
    return
      Type == MI.Type &&
      Config == MI.Config;
  }

  FORCE_INLINE uint32_t Hash () const noexcept {
    uint32_t h = joaatHashBufPart(&Type, sizeof(Type), 0x29a);
    h = joaatHashBufPart(&Config, sizeof(Config), h);
    return joaatHashBufFinish(h);
  }
};

outputfile &operator << (outputfile &, const configid &);
inputfile &operator >> (inputfile &, configid &);

MAKE_STD_HASHER(configid)


struct dangerid {
public:
  double NakedDanger;
  double EquippedDanger;

public:
  inline dangerid () : NakedDanger(0), EquippedDanger(0) {}
  inline dangerid (double NakedDanger, double EquippedDanger) : NakedDanger(NakedDanger), EquippedDanger(EquippedDanger) {}
  inline dangerid (const dangerid &src) : NakedDanger(src.NakedDanger), EquippedDanger(src.EquippedDanger) {}

  inline dangerid &operator = (const dangerid &src) { NakedDanger = src.NakedDanger; EquippedDanger = src.EquippedDanger; return *this; }
};

outputfile &operator << (outputfile &, const dangerid &);
inputfile &operator >> (inputfile &, dangerid &);


struct ivantime {
  int Day;
  int Hour;
  int Min;
};


struct massacreid {
public:
  int Type;
  int Config;
  festring Name;

public:
  FORCE_INLINE massacreid () : Type(0), Config(0), Name() {}
  FORCE_INLINE massacreid (int Type, int Config, cfestring &Name) : Type(Type), Config(Config), Name(Name) {}

  FORCE_INLINE massacreid &operator = (const massacreid &rhs) {
    Type = rhs.Type;
    Config = rhs.Config;
    Name = rhs.Name;
    return *this;
  }

  FORCE_INLINE bool operator < (const massacreid &MI) const {
    if (Type != MI.Type) return (Type < MI.Type);
    if (Config != MI.Config) return (Config < MI.Config);
    //return (Name < MI.Name);
    return (Name.CompareCI(MI.Name) < 0);
  }

  FORCE_INLINE bool operator == (const massacreid &MI) const {
    return
      Type == MI.Type &&
      Config == MI.Config &&
      Name.EquCI(MI.Name);
  }

  FORCE_INLINE uint32_t Hash () const noexcept {
    uint32_t h = joaatHashBufPart(&Type, sizeof(Type), 0x29a);
    h = joaatHashBufPart(&Config, sizeof(Config), h);
    h = Name.HashCI(joaatHashBufFinish(h));
    return joaatHashBufFinish(h);
  }
};

outputfile &operator << (outputfile &, const massacreid &);
inputfile &operator >> (inputfile &, massacreid &);

MAKE_STD_HASHER(massacreid)


struct killreason {
public:
  festring String;
  int Amount;

public:
  killreason () {}
  killreason (cfestring &String, int Amount) : String(String), Amount(Amount) {}
};

outputfile &operator << (outputfile &, const killreason &);
inputfile &operator >> (inputfile &, killreason &);


struct killdata {
public:
  int Amount;
  double DangerSum;
  std::vector<killreason> Reason;

public:
  killdata (int Amount=0, double DangerSum=0) : Amount(Amount), DangerSum(DangerSum) {}
};

outputfile &operator << (outputfile &, const killdata &);
inputfile &operator >> (inputfile &, killdata &);


typedef std::unordered_map<configid, dangerid> dangermap;
typedef std::unordered_map<feuLong, character*> characteridmap;
typedef std::unordered_map<feuLong, item*> itemidmap;
typedef std::unordered_map<feuLong, entity*> trapidmap;
typedef std::unordered_map<massacreid, killdata> massacremap;
typedef std::unordered_map<feuLong, feuLong> boneidmap;
typedef std::vector<item*> itemvector;
typedef std::vector<itemvector> itemvectorvector;
typedef std::vector<character*> charactervector;


class quitrequest {};
class areachangerequest {};


enum ArgTypes {
  FARG_UNDEFINED,
  FARG_STRING,
  FARG_NUMBER,
};

struct FuncArg {
public:
  ArgTypes type;
  festring sval;
  sLong ival;

public:
  FuncArg () : type(FARG_UNDEFINED), sval(""), ival(0) {}
  FuncArg (cfestring &aVal) : type(FARG_STRING), sval(aVal), ival(0) {}
  FuncArg (sLong aVal) : type(FARG_NUMBER), sval(""), ival(aVal) {}
};


struct massacresetentry {
  festring Key;
  festring String;
  std::vector<festring> Details;
  int ImageKey;

  FORCE_INLINE bool operator < (const massacresetentry &MSE) const { return festring::CompareCILess(Key, MSE.Key); }

  FORCE_INLINE bool operator == (const massacresetentry &MSE) const { return Key.EquCI(MSE.Key); }

  FORCE_INLINE uint32_t Hash () const noexcept { return Key.HashCI(); }
};

MAKE_STD_HASHER(massacresetentry)


class game {
public:
  enum { PickTimeout = 32 };
public:
  static void InitPlaces ();
  static void DeInitPlaces ();

  static void GetGameHash (highscore::Hash hash) { memcpy(hash, GameHash, sizeof(highscore::Hash)); }

  static void ShowPrologueStory ();
  static void InitializeGameState ();
  static void ClearSomeListsOnNewGame ();
  static void XmasPresents ();
  static truth RunNewGameEvents ();
  static truth Init (cfestring &Name=CONST_S(""));
  static void DeInit ();
  static void Run ();

  // return 0 on invalid dir; dir 8 is "stay still"
  //static int GetMoveCommandKey (int Dir);
  // return -1, or dir; if ctrl is allowed and pressed, return 100+dir
  static int MoveKeyToDir (int Key, bool allowFastExt=false);
  static int MoveVectorToDirection (cv2 &mv); // -1: none
  static FORCE_INLINE cv2 GetMoveVector (int I) { return MoveVector[I]; }
  static FORCE_INLINE cv2 GetRelativeMoveVector (int I) { return RelativeMoveVector[I]; }
  static FORCE_INLINE cv2 GetBasicMoveVector (int I) { return BasicMoveVector[I]; }
  static FORCE_INLINE cv2 GetLargeMoveVector (int I) { return LargeMoveVector[I]; }
  static FORCE_INLINE area *GetCurrentArea () { return CurrentArea; }
  static FORCE_INLINE level *GetCurrentLevel () { return CurrentLevel; }
  static FORCE_INLINE int GetCurrentLevelIndex () { return CurrentLevelIndex; }
  static FORCE_INLINE uChar ***GetLuxTable () { return LuxTable; }
  static FORCE_INLINE character *GetPlayer () { return Player; }
  static void SetPlayer (character *);
  static FORCE_INLINE v2 GetCamera () { return Camera; }
  static void UpdateCameraX ();
  static void UpdateCameraY ();
  static FORCE_INLINE truth IsLoading () { return Loading; }
  static FORCE_INLINE void SetIsLoading (truth What) { Loading = What; }
  static FORCE_INLINE truth ForceJumpToPlayerBe () { return JumpToPlayerBe; }
  static FORCE_INLINE void SetForceJumpToPlayerBe (truth What) { JumpToPlayerBe = What; }
  static level *GetLevel (int);
  static void InitLuxTable ();
  static void DeInitLuxTable ();
  static cchar *Insult ();
  static truth TruthQuestion (cfestring &String, int DefaultAnswer=0, int OtherKeyForTrue=0);
  static void DrawEverything ();
  static truth AutoSave ();
  static truth Save (bool needVacuum);
  static int Load (cfestring &SaveNamePath=SaveName(festring::EmptyStr()));
  static FORCE_INLINE truth IsRunning () { return Running; }
  static FORCE_INLINE void SetIsRunning (truth What) { Running = What; }
  static void UpdateCameraX (int X);
  static void UpdateCameraY (int Y);
  static int GetMoveCommandKeyBetweenPoints (v2 A, v2 B);
  static void DrawComparisonInfo ();
  static void DrawEverythingNoBlit (truth AnimationDraw=false);
  static void DrawMiniMapNoBlit ();
  static void DrawMiniMapMarks ();
  static void DrawMiniMapCursors ();
  static god *GetGod (int I) { return God[I]; }
  static FORCE_INLINE cchar *GetAlignment (int I) { return Alignment[I]; }
  static int GetMaxAlignmentWidth ();
  static int GetMaxGodNameWidth ();
  static void ApplyDivineTick ();
  static void ApplyDivineAlignmentBonuses (god *CompareTarget, int Multiplier, truth Good);
  static v2 GetDirectionVectorForKey (int);
  static festring SaveName (cfestring &Base=CONST_S(""));
  static void ShowLevelMessage ();
  static double GetMinDifficulty ();
  static void TriggerQuestForGoldenEagleShirt ();
  static void CalculateGodNumber ();
  static void IncrementTick () { ++Tick; }
  static feuLong GetTick () { return Tick; }
  static int DirectionQuestion (cfestring &Topic, truth RequireAnswer, truth AcceptYourself);
  static void RemoveSaves ();
  static FORCE_INLINE truth IsInWilderness () { return InWilderness; }
  static FORCE_INLINE void SetIsInWilderness (truth What) { InWilderness = What; }
  static FORCE_INLINE worldmap *GetWorldMap () { return WorldMap; }
  static FORCE_INLINE void SetAreaInLoad (area *What) { AreaInLoad = What; }
  static FORCE_INLINE void SetSquareInLoad (square *What) { SquareInLoad = What; }
  static FORCE_INLINE area *GetAreaInLoad () { return AreaInLoad; }
  static FORCE_INLINE square *GetSquareInLoad () { return SquareInLoad; }
  static int GetLevels ();
  static FORCE_INLINE dungeon *GetCurrentDungeon () { return Dungeon[CurrentDungeonIndex]; }
  static FORCE_INLINE dungeon *GetDungeon (int I) { return Dungeon[I]; }
  static FORCE_INLINE int GetCurrentDungeonIndex () { return CurrentDungeonIndex; }
  static void InitDungeons ();
  static truth OnScreen (v2);
  static void DoEvilDeed (int);
  static void UpdateCamera ();
  static feuLong CreateNewCharacterID (character *);
  static feuLong CreateNewItemID (item *);
  static feuLong CreateNewTrapID (entity *);
  static FORCE_INLINE team *GetTeam (int I) { return Team[I]; }
  static FORCE_INLINE int GetTeams () { return Teams; }
  static void Hostility (team*, team*);
  static void CreateTeams ();
  static festring StringQuestion (cfestring &Topic, col16 Color,
                                  festring::sizetype MinLetters, festring::sizetype MaxLetters,
                                  truth AllowExit, stringkeyhandler KeyHandler=0, truth *aborted=0);
  static festring StringQuestionEx (cfestring &Topic, cfestring &InitStr, col16 Color,
                                  festring::sizetype MinLetters, festring::sizetype MaxLetters,
                                  truth AllowExit, stringkeyhandler KeyHandler=0, truth *aborted=0);
  static sLong NumberQuestion (cfestring &Topic, int Color, truth ReturnZeroOnEsc=false,
                               truth *escaped=0);
  static feuLong IncreaseLOSTick ();
  static FORCE_INLINE feuLong GetLOSTick () { return LOSTick; }
  static FORCE_INLINE void SendLOSUpdateRequest () { LOSUpdateRequested = true; }
  static FORCE_INLINE void RemoveLOSUpdateRequest () { LOSUpdateRequested = false; }
  static FORCE_INLINE character *GetPetrus () { return Petrus; }
  static FORCE_INLINE void SetPetrus (character *What) { Petrus = What; }
  static truth HandleQuitMessage ();
  static int GetDirectionForVector (v2);
  static int GetPlayerAlignmentSum ();
  static int GetPlayerAlignment ();
  static cchar *GetVerbalPlayerAlignment ();
  static void CreateGods ();
  static int GetScreenXSize ();
  static int GetScreenYSize ();
  static int GetLeftStatsPos ();
  static int GetVertDollOfs ();
  static int GetVertStatOfs ();
  static v2 CalculateScreenCoordinates (v2);
  static void BusyAnimation ();
  static void BusyAnimation (bitmap *Buffer, truth ForceDraw);
  static void SetBusyAnimationMessage (cfestring &msg) { mBusyMessage = msg; mBusyMessageTime = 0; }
  static v2 PositionQuestion (cfestring&, v2, positionhandler = 0, positionkeyhandler = 0, truth = true);
  static void LookHandler (v2);
  static int AskForKeyPress (cfestring &Topic);
  static int AskForSpacePress (cfestring &Topic);
  static void AskForEscPress (cfestring &Topic);
  static truth AnimationController ();
  static FORCE_INLINE gamescript *GetGameScript () { return GameScript; }
  static void InitScript ();

  static void MiniMapProcessor (bool small);

  static FORCE_INLINE valuemap &GetGlobalValueMap () { return GlobalValueMap; }
  static FORCE_INLINE mutablemap &GetGlobalMutableMap () { return GlobalMutableMap; }
  static truth HasGlobalValue (cfestring &name);
  static sLong FindGlobalValue (cfestring &name, sLong defval=-1, truth *found=0);
  static sLong FindGlobalValue (cfestring &name, truth *found=0);

  static truth IsMutableGlobal (cfestring &name);
  static void MakeGlobalMutable (cfestring &name);
  static truth SetGlobalValue (cfestring &name, sLong value);

  static void RegisterGlobalVars ();

  // this will fail if there is no such constant
  //TODO: cache values
  static sLong GetGlobalConst (cfestring &name);

  static void LoadModuleList ();
  static const std::vector<festring> &GetModuleList ();

  static void SaveModuleList (outputfile &ofile);
  static truth LoadAndCheckModuleList (inputfile &ifile); // false: incomaptible, ifile left in undefined state

  static void InitGlobalValueMap ();
  static void LoadGlobalValueMap (TextInput &SaveFile, bool allowRedefine=false);

  static void TextScreen (cfestring &Text, v2 Displacement=ZERO_V2, col16 Color=0xFFFF,
                          truth GKey=true, truth Fade=true, bitmapeditor BitmapEditor=0);
  static void SetCursorPos (v2 What) { CursorPos = What; }
  static FORCE_INLINE truth DoZoom () { return Zoom; }
  static void SetDoZoom (truth What) { Zoom = What; }
  static int KeyQuestion (cfestring &Message, int DefaultAnswer, ...); // end list with `0`
  static v2 LookKeyHandler (v2, int);
  static v2 NameKeyHandler (v2, int);
  static void End (festring DeathMessage, truth Permanently=true, truth AndGoToMenu=true);
  static int CalculateRoughDirection (v2);
  static sLong ScrollBarQuestion (cfestring &Topic, sLong BeginValue, sLong Step,
                                  sLong Min, sLong Max, sLong AbortValue,
                                  col16 TopicColor, col16 Color1, col16 Color2,
                                  void (*Handler)(sLong)=0);
  static FORCE_INLINE truth IsGenerating () { return Generating; }
  static void SetIsGenerating (truth What) { Generating = What; }
  static void CalculateNextDanger ();
  static int Menu (bitmap *BackGround, v2 Pos, cfestring &Topic, cfestring &sMS,
                   col16 Color, cfestring &SmallText1=CONST_S(""), cfestring &SmallText2=CONST_S(""));
  static void InitDangerMap ();
  static const dangermap &GetDangerMap ();
  static truth TryTravel (int Dungeon, int Area, int EntryIndex,
                          truth AllowHostiles/*=false*/, truth AlliesFollow/*=true*/);
  static truth LeaveArea (charactervector &Group, truth AllowHostiles, truth AlliesFollow);
  static void EnterArea (charactervector &Group, int Area, int EntryIndex);

  static inline int CompareLights (col24 L1, col24 L2);
  static inline int CompareLightToInt (col24 L, col24 Int);
  static inline void CombineLights (col24 &L1, col24 L2);
  static inline col24 CombineConstLights (col24 L1, col24 L2);
  static inline truth IsDark (col24 Light);

  static void SetStandardListAttributes (felist &);

  static FORCE_INLINE double GetAveragePlayerArmStrengthExperience () { return AveragePlayerArmStrengthExperience; }
  static FORCE_INLINE double GetAveragePlayerLegStrengthExperience () { return AveragePlayerLegStrengthExperience; }
  static FORCE_INLINE double GetAveragePlayerDexterityExperience () { return AveragePlayerDexterityExperience; }
  static FORCE_INLINE double GetAveragePlayerAgilityExperience () { return AveragePlayerAgilityExperience; }

  static void InitPlayerAttributeAverage ();
  static void UpdatePlayerAttributeAverage ();

  static void CallForAttention (v2 Pos, int RangeSquare);

  static character *SearchCharacter (feuLong ID);
  static item *SearchItem (feuLong ID);
  static entity *SearchTrap (feuLong ID);

  static void AddCharacterID (character *, feuLong);
  static void RemoveCharacterID (feuLong);
  static void AddItemID (item *, feuLong);
  static void RemoveItemID (feuLong);
  static void UpdateItemID (item *, feuLong);
  static void AddTrapID (entity *, feuLong);
  static void RemoveTrapID (feuLong);
  static void UpdateTrapID (entity *, feuLong);

  static FORCE_INLINE int GetStoryState () { return StoryState; }
  static FORCE_INLINE void SetStoryState (int What) { StoryState = What; }
  static FORCE_INLINE int GetXinrochTombStoryState () { return XinrochTombStoryState; }
  static FORCE_INLINE void SetXinrochTombStoryState (int What) { XinrochTombStoryState = What; }
  static FORCE_INLINE int GetMondedrPass () { return MondedrPass; }
  static FORCE_INLINE void SetMondedrPass (int What) { MondedrPass = What; }
  static FORCE_INLINE int GetRingOfThieves () { return RingOfThieves; }
  static FORCE_INLINE void SetRingOfThieves (int What) { RingOfThieves = What; }

  static FORCE_INLINE int GetMasamune () { return Masamune; }
  static FORCE_INLINE void SetMasamune (int What) { Masamune = What; }
  static FORCE_INLINE int GetMuramasa () { return Muramasa; }
  static FORCE_INLINE void SetMuramasa (int What) { Muramasa = What; }
  static FORCE_INLINE int GetLoricatusHammer () { return LoricatusHammer; }
  static FORCE_INLINE void SetLoricatusHammer (int What) { LoricatusHammer = What; }
  static FORCE_INLINE int GetLiberator () { return Liberator; }
  static FORCE_INLINE void SetLiberator (int What) { Liberator = What; }
  static FORCE_INLINE int GetOmmelBloodMission () { return OmmelBloodMission; }
  static FORCE_INLINE void SetOmmelBloodMission (int What) { OmmelBloodMission = What; }
  static FORCE_INLINE int GetRegiiTalkState () { return RegiiTalkState; }
  static FORCE_INLINE void SetRegiiTalkState (int What) { RegiiTalkState = What; }

  static FORCE_INLINE int GetGloomyCaveStoryState () { return GloomyCaveStoryState; }
  static FORCE_INLINE void SetGloomyCaveStoryState (int What) { GloomyCaveStoryState = What; }
  static FORCE_INLINE int GetFreedomStoryState () { return FreedomStoryState; }
  static FORCE_INLINE void SetFreedomStoryState (int What) { FreedomStoryState = What; }
  static FORCE_INLINE int GetAslonaStoryState () { return AslonaStoryState; }
  static FORCE_INLINE void SetAslonaStoryState (int What) { AslonaStoryState = What; }
  static FORCE_INLINE int GetRebelStoryState () { return RebelStoryState; }
  static FORCE_INLINE void SetRebelStoryState (int What) { RebelStoryState = What; }
  static FORCE_INLINE truth PlayerIsGodChampion () { return PlayerIsChampion; }
  static FORCE_INLINE void MakePlayerGodChampion () { PlayerIsChampion = true; } // No way to switch that back, only one championship per game.
  static FORCE_INLINE truth PlayerHasBoat () { return HasBoat; }
  static FORCE_INLINE void GivePlayerBoat () { HasBoat = true; }
  static FORCE_INLINE truth PlayerOnBoat () { return OnBoat; }
  static FORCE_INLINE void SetPlayerOnBoat (truth ison) { OnBoat = ison; }

  //static void SetIsInGetCommand (truth What) { InGetCommand = What; }
  //static truth IsInGetCommand () { return InGetCommand; }

  static festring GetHomeDir ();
  static festring GetConfigPath ();
  static festring GetGamePath ();
  static festring GetSavePath ();
  static festring GetBonePath ();

  static festring GetDeathshotPath ();
  static festring GetScreenshotPath ();
  static FORCE_INLINE truth PlayerWasHurtByExplosion () { return PlayerHurtByExplosion; }
  static FORCE_INLINE void SetPlayerWasHurtByExplosion (truth What) { PlayerHurtByExplosion = What; }
  static FORCE_INLINE void SetCurrentArea (area *What) { CurrentArea = What; }
  static FORCE_INLINE void SetCurrentLevel (level *What) { CurrentLevel = What; }
  static FORCE_INLINE void SetCurrentWSquareMap (wsquare ***What) { CurrentWSquareMap = What; }
  static FORCE_INLINE void SetCurrentLSquareMap (lsquare ***What) { CurrentLSquareMap = What; }
  static FORCE_INLINE festring &GetDefaultPolymorphTo () { return DefaultPolymorphTo; }
  static FORCE_INLINE festring &GetDefaultSummonMonster () { return DefaultSummonMonster; }
  static FORCE_INLINE festring &GetDefaultChangeMaterial () { return DefaultChangeMaterial; }
  static FORCE_INLINE festring &GetDefaultDetectMaterial () { return DefaultDetectMaterial; }
  static FORCE_INLINE festring &GetDefaultTeam () { return DefaultTeam; }
  static void SignalDeath (ccharacter *Ghost, ccharacter *Murderer, festring DeathMsg);
  static void DisplayMassacreLists ();
  static void DisplayMassacreList (const massacremap &MassacreMap, cchar *Reason, sLong Amount);
  static truth MassacreListsEmpty ();
  static void SaveMassacreLists ();
  static void SaveMassacreList (int type, const massacremap &MassacreMap, cchar *Reason,
                                sLong Amount);

  static void BuildMassacreList (std::unordered_set<massacresetentry> &MassacreSet,
                                 charactervector &GraveYard, festring &FirstPronoun,
                                 const massacremap &MassacreMap, cchar *Reason, sLong Amount);

  static FORCE_INLINE void ActivateWizardMode () { WizardMode = true; }
  static FORCE_INLINE void DeactivateWizardMode () { WizardMode = false; }
  static FORCE_INLINE truth WizardModeIsActive () { return WizardMode; }
  static void SetSkipHiScrore ();
  static void ResetSkipHiScrore ();
  static FORCE_INLINE truth IsCheating () { return (WizardMode || SkipHiScore); }
  static void SeeWholeMap ();
  static FORCE_INLINE int GetSeeWholeMapCheatMode () { return SeeWholeMapCheatMode; }
  static FORCE_INLINE void SetSeeWholeMapCheatMode (int v) { SeeWholeMapCheatMode = v; }
  static FORCE_INLINE truth GoThroughWallsCheatIsActive () { return GoThroughWallsCheat; }
  static FORCE_INLINE void GoThroughWalls () { GoThroughWallsCheat = !GoThroughWallsCheat; }
  static FORCE_INLINE void SetGoThroughWalls (bool v) { GoThroughWallsCheat = v; }
  static FORCE_INLINE truth WizardModeIsReallyActive () { return WizardMode; }
  static void CreateBone ();
  static FORCE_INLINE int GetQuestMonstersFound () { return QuestMonstersFound; }
  static FORCE_INLINE void SignalQuestMonsterFound () { ++QuestMonstersFound; }
  static FORCE_INLINE void SetQuestMonstersFound (int What) { QuestMonstersFound = What; }
  static truth PrepareRandomBone (int);
  static FORCE_INLINE boneidmap &GetBoneItemIDMap () { return BoneItemIDMap; }
  static FORCE_INLINE boneidmap &GetBoneCharacterIDMap () { return BoneCharacterIDMap; }
  static double CalculateAverageDanger(const charactervector&, character*);
  static double CalculateAverageDangerOfAllNormalEnemies();
  static character *CreateGhost();
  static FORCE_INLINE truth TooGreatDangerFound () { return TooGreatDangerFoundTruth; }
  static FORCE_INLINE void SetTooGreatDangerFound (truth What) { TooGreatDangerFoundTruth = What; }
  static void CreateBusyAnimationCache ();
  static sLong GetScore ();
  static truth TweraifIsFree ();
  static truth IsXMas ();
  static int AddToItemDrawVector (const itemvector&);
  static void ClearItemDrawVector ();
  static void ItemEntryDrawer (bitmap *Bitmap, v2 Pos, uInt I);
  static int AddToCharacterDrawVector (character *What);
  static void ClearCharacterDrawVector ();
  static void CharacterEntryDrawer (bitmap *Bitmap, v2 Pos, uInt I);
  static void GodEntryDrawer (bitmap *Bitmap, v2 Pos, uInt I);
  static FORCE_INLINE itemvectorvector &GetItemDrawVector () { return ItemDrawVector; }
  static FORCE_INLINE charactervector &GetCharacterDrawVector () { return CharacterDrawVector; }
  static FORCE_INLINE truth IsSumoWrestling () { return SumoWrestling; }
  static FORCE_INLINE void SetIsSumoWrestling (truth What) { SumoWrestling = What; }
  static FORCE_INLINE truth AllowHostilities () { return !SumoWrestling; }
  static FORCE_INLINE truth AllBodyPartsVanish () { return SumoWrestling; }
  static truth TryToEnterSumoArena ();
  static truth TryToExitSumoArena ();
  static truth EndSumoWrestling (int);
  static character *GetSumo ();
  static FORCE_INLINE cfestring &GetPlayerName () { return PlayerName; }
  static rain *ConstructGlobalRain ();
  static FORCE_INLINE void SetGlobalRainLiquid (liquid *What) { GlobalRainLiquid = What; }
  static FORCE_INLINE void SetGlobalRainSpeed (v2 What) { GlobalRainSpeed = What; }
  static FORCE_INLINE truth PlayerIsSumoChampion () { return PlayerSumoChampion; }
  static FORCE_INLINE truth PlayerIsSolicitusChampion () { return PlayerSolicitusChampion; }
  static FORCE_INLINE void MakePlayerSolicitusChampion () { PlayerSolicitusChampion = true; }
  static FORCE_INLINE truth ChildTouristHasSpider () { return TouristHasSpider; }
  static FORCE_INLINE void SetTouristHasSpider () { TouristHasSpider = true; }
  static v2 GetSunLightDirectionVector ();
  static int CalculateMinimumEmitationRadius (col24);
  static feuLong IncreaseSquarePartEmitationTicks ();
  static FORCE_INLINE cint GetLargeMoveDirection (int I) { return LargeMoveDirection[I]; }
  static bool Wish (character *Wisher, cchar *MsgSingle, cchar *MsgPair, bool canAbort=false);
  static festring DefaultQuestion (festring Topic, festring &Default,
                                   stringkeyhandler KeyHandler=0);
  static void GetTime (ivantime &);
  static FORCE_INLINE sLong GetTurn () { return Turn; }
  static void IncreaseTurn () { ++Turn; }
  static FORCE_INLINE int GetTotalMinutes () { return Tick * 60 / 2000; }
  static truth PolymorphControlKeyHandler (int Key, festring &String);
  static FORCE_INLINE feuLong *GetEquipmentMemory () { return EquipmentMemory; }
  static truth PlayerIsRunning ();
  static FORCE_INLINE void SetPlayerIsRunning (truth What) { PlayerRunning = What; }
  static truth FillPetVector (cchar*);
  static truth CommandQuestion ();
  static void NameQuestion ();
  static v2 CommandKeyHandler (v2, int);
  static void CommandScreen (cfestring &Topic, feuLong PossibleFlags, feuLong ConstantFlags,
                             feuLong &VaryFlags, feuLong &Flags);
  static truth CommandAll ();
  static FORCE_INLINE double GetDangerFound () { return DangerFound; }
  static FORCE_INLINE void SetDangerFound (double What) { DangerFound = What; }
  static col16 GetAttributeColor (int);
  static void UpdateAttributeMemory ();
  static void InitAttributeMemory ();
  static void TeleportHandler (v2);
  static void PetHandler (v2);
  static truth SelectPet (int);
  static double GetGameSituationDanger ();
  static FORCE_INLINE olterrain *GetMonsterPortal () { return MonsterPortal; }
  static FORCE_INLINE void SetMonsterPortal (olterrain *What) { MonsterPortal = What; }
  static FORCE_INLINE truth GetCausePanicFlag () { return CausePanicFlag; }
  static FORCE_INLINE void SetCausePanicFlag (truth What) { CausePanicFlag = What; }
  static time_t GetTimeSpent ();
  static void AddSpecialCursor (v2, int);
  static void RemoveSpecialCursors ();
  static void LearnAbout (god *);
  static truth PlayerKnowsAllGods ();
  static void AdjustRelationsToAllGods (int);
  static void SetRelationsToAllGods (int);
  static void ShowDeathSmiley (bitmap *, truth);
  static FORCE_INLINE void SetEnterImage (cbitmap *What) { EnterImage = What; }
  static FORCE_INLINE void SetEnterTextDisplacement (v2 What) { EnterTextDisplacement = What; }

  static int ListSelector (int defsel, const cfestring title, ...); // defsel<0: first
  static int ListSelectorArray (int defsel, cfestring &title, cfestring &msg, const char *items[]); // defsel<0: first

  //static char GetNormalMoveKey (int idx);
  //static void SetNormalMoveKey (int idx, char ch);

  static truth CheckDropLeftover (item *i);

  static team *FindTeam (cfestring &name);
  //static team *FindTeam (const char *name);

  static truth HasGlobalEvent (cfestring &ename); // global game event
  // return 'true' if `Disallow` was called
  static truth RunGlobalEvent (cfestring &ename, truth defaultAllow=true); // global game event

  static truth RunEventWithCtx (EventContext &ctx, const EventHandlerMap &evmap,
                                sLong configNumber, cfestring &ename,
                                truth defaultAllow);

  static truth RunCharEvent (cfestring &ename, character *who, character *second=0, item *it=0);
  static truth RunItemEvent (cfestring &ename, item *what, character *holder, character *second=0);

  static truth RunEnterEvent (cfestring &ename, character *who, stairs *str);

  static truth RunCharAllowScript (character *tospawn, const EventHandlerMap &evmap, cfestring &ename);
  static truth RunItemAllowScript (item *tospawn, const EventHandlerMap &evmap, cfestring &ename);

  static festring ldrGetVar (TextInput *fl, cfestring &name, truth scvar);

public:
  static v2 HiSquare; // highlighted square position

  static void HighlightSquare (v2 pos);
  static void UnHighlightSquare ();

public:
  static void OpenCurrentSave ();
  static void CloseCurrentSave ();

  static void SaveScreenshot (cfestring &basefname=festring::EmptyStr(), bool doRedraw=true);

public:
  static void AbortGame ();
  static bool InGetPlayerCommand;

  static bool inScreenShoter;
  static bool Screenshoter (int Key);

private:
  static bool TryLoadBone (cfestring &boneFileName, int LevelIndex);

private:
  // used only with SQLite archive
  static int CurrentSaveOpenCount;

  static truth OpenSaveFileForWrite (bool abortOnError);
  static truth CloseSaveFileForWrite (bool doVacuum, bool abortOnError);

  static truth RunSaveUpdater (truth (*updaterFn) (void *udata), void *udata, bool abortOnError);

  static truth GameSaveFn (void *udata);

  static truth DungeonSaveFn (void *udata);
  static truth WorldMapSaveFn (void *udata);

  static void SaveWorldMap (truth DeleteAfterwards);
  static worldmap *LoadWorldMap ();

  static truth SaveGameState ();
  static truth LoadGameState ();

  static void SaveCurrentDungeonLevel (int levelIndex);

private:
  // fractional rate part
  static int mDMapAccum;
  static int mDMLastRate;
  static feuLong mDMLastWrapAroundTick;
  static feuLong mDMLastWrapAroundTurn;

  // always > 0; but `-1` means "one config at a time"
  static int CalculateDangerMapUpdateRate ();
  static void AdvanceDangerIDType ();
  static void AdvanceDangerIDConfig (); // calls `AdvanceDangerIDType()` if necessary

  static void DebugPrintCurrDangerID (const char *pfx);

public:
  static void GetTopAreaPosSize (v2 *scrpos, v2 *size);
  static v2 GetTopAreaTextPos (int lineCount);
  static void EraseTopArea ();

private:
  static truth GetWord (festring &w);
  static void UpdateCameraCoordinate (int &, int, int, int);

  static void DoOnEvent (const EventHandlerMap &emap, sLong configNumber, cfestring &ename, truth defaultAllow);
  static int ParseFuncArgs (TextInput *ifl, const char *types, std::vector<FuncArg> &args, truth noterm=false);

  static void LoadGlobalEvents ();

  static void WipeTeamMembers ();

  static void MiniMapHelp ();

  static void MiniMapDrawNoteAtText (v2 pos);

  static void MiniMapAppendMarkAt (v2 pos);
  static void MiniMapDeleteMarkAt (v2 pos);
  static v2 MiniMapNextMarkFrom (v2 pos);
  static v2 MiniMapPrevMarkFrom (v2 pos);

  static EventContext curevctx;

private:
  static cchar *const Alignment[];
  static god **God;
  static int32_t CurrentLevelIndex;
  static int32_t CurrentDungeonIndex;
  //static cint MoveNormalCommandKey[];
  static cv2 MoveVector[];
  static cv2 RelativeMoveVector[];
  static cv2 BasicMoveVector[];
  static cv2 LargeMoveVector[];
  static uChar ***LuxTable;
  static truth Running;
  static character *Player;
  static v2 Camera;
  static feuLong Tick;
  //static festring AutoSaveFileName;
  static truth InWilderness;
  static worldmap *WorldMap;
  static area *AreaInLoad;
  static square *SquareInLoad;
  static dungeon **Dungeon;
  static feuLong NextCharacterID;
  static feuLong NextItemID;
  static feuLong NextTrapID;
  static team **Team;
  static feuLong LOSTick;
  static truth LOSUpdateRequested;
  static character *Petrus;
  static truth Loading;
  static truth JumpToPlayerBe;
  static gamescript *GameScript;
  static valuemap GlobalValueMap;
  static mutablemap GlobalMutableMap;
  static v2 CursorPos;
  static truth Zoom;
  static truth Generating;
  static dangermap DangerMap;
  //static int FirstDangerIDType;
  //static int FirstDangerIDConfigIndex;
  static int DangerMapItemCount;
  static int NextDangerIDType;
  static int NextDangerIDConfigIndex;
  static double AveragePlayerArmStrengthExperience;
  static double AveragePlayerLegStrengthExperience;
  static double AveragePlayerDexterityExperience;
  static double AveragePlayerAgilityExperience;
  static characteridmap CharacterIDMap;
  static itemidmap ItemIDMap;
  static trapidmap TrapIDMap;
  static int Teams;
  static int Dungeons;
  static int StoryState;
  static int XinrochTombStoryState;
  static int MondedrPass;
  static int RingOfThieves;
  static int Masamune;
  static int Muramasa;
  static int LoricatusHammer;
  static int Liberator;
  static int OmmelBloodMission;
  static int RegiiTalkState;

  static int GloomyCaveStoryState;
  static int FreedomStoryState;
  static int AslonaStoryState;
  static int RebelStoryState;

  //static truth InGetCommand;
  static truth PlayerHurtByExplosion;
  static area *CurrentArea;
  static level *CurrentLevel;
  static wsquare ***CurrentWSquareMap;
  static lsquare ***CurrentLSquareMap;
  static festring DefaultPolymorphTo;
  static festring DefaultSummonMonster;
  static festring DefaultWish;
  static festring DefaultChangeMaterial;
  static festring DefaultDetectMaterial;
  static festring DefaultTeam;
  static massacremap PlayerMassacreMap;
  static massacremap PetMassacreMap;
  static massacremap MiscMassacreMap;
  static sLong PlayerMassacreAmount;
  static sLong PetMassacreAmount;
  static sLong MiscMassacreAmount;
  static truth WizardMode;
  static truth SkipHiScore;
  static int SeeWholeMapCheatMode;
  static truth GoThroughWallsCheat;
  static int QuestMonstersFound;
  static boneidmap BoneItemIDMap;
  static boneidmap BoneCharacterIDMap;
  static truth TooGreatDangerFoundTruth;
  static bitmap *BusyAnimationCache[32];
  static itemvectorvector ItemDrawVector;
  static charactervector CharacterDrawVector;
  static truth SumoWrestling;
  static festring PlayerName;
  static liquid *GlobalRainLiquid;
  static v2 GlobalRainSpeed;
  static sLong GlobalRainTimeModifier;
  static truth PlayerSumoChampion;
  static truth PlayerSolicitusChampion;
  static truth PlayerIsChampion; // This marks the player as a champion of some god.
  static truth HasBoat; // Whether the player can sail the oceans of world map.
  static truth OnBoat; // Is the player on boat right now? used to display the proper icon.
  static truth TouristHasSpider;
  static feuLong SquarePartEmitationTick;
  static cint LargeMoveDirection[];
  static sLong Turn;
  static feuLong EquipmentMemory[MAX_EQUIPMENT_SLOTS];
  static truth PlayerRunning;
  static character *LastPetUnderCursor;
  static charactervector PetVector;
  static double DangerFound;
  static int OldAttribute[ATTRIBUTES];
  static int NewAttribute[ATTRIBUTES];
  static int LastAttributeChangeTick[ATTRIBUTES];
  static int NecroCounter;
  static int CursorData;
  static olterrain *MonsterPortal;
  static truth CausePanicFlag;
  static time_t TimePlayedBeforeLastLoad;
  static time_t LastLoad;
  static time_t GameBegan;
  static std::vector<v2> SpecialCursorPos;
  static std::vector<int> SpecialCursorData;
  static bool SpecialCursorsEnabled;
  static truth PlayerHasReceivedAllGodsKnownBonus;
  static cbitmap *EnterImage;
  static v2 EnterTextDisplacement;

  static int MonsterGenDelay;

  static std::stack<TextInput *> mFEStack;
  static std::vector<festring> mModuleList;
  static EventHandlerMap mGlobalEvents;

private:
  static void LoadModuleListFile (TextInput &fl);

  static void DrawUnderCursor (truth AnimationDraw);

  // worldmap "places of interest"
private:
  static owterrain **pois;
  static int poisSize;
  static festring mBusyMessage;
  static time_t mBusyMessageTime;

  static highscore::Hash GameHash;

  // should be called after player name is set
  static void CalcGameHash ();
  static void DumpGameHash ();

public:
  static int poiCount ();
  static owterrain *poiByIndex (int pcfg, truth abortOnNotFound=true); // can return nullptr

  // not by POI name, but by POI constant name from "define.def"!
  static owterrain *poi (cfestring &name, truth abortOnNotFound=true); // can return nullptr

  static void RevealPOI (owterrain *terra);

  static owterrain *FindPOIByConfig (int configid);

public:
  // convenient accessors
  //static owterrain *alienvesselPOI ();
  static owterrain *attnamPOI ();
  static owterrain *darkforestPOI ();
  //static owterrain *dragontowerPOI ();
  static owterrain *elpuricavePOI ();
  static owterrain *mondedrPOI ();
  static owterrain *muntuoPOI ();
  static owterrain *newattnamPOI ();
  static owterrain *underwatertunnelPOI ();
  static owterrain *underwatertunnelexitPOI ();
  static owterrain *xinrochtombPOI ();
  static owterrain *goblinfortPOI ();
  static owterrain *fungalcavePOI ();
  static owterrain *pyramidPOI ();
  static owterrain *aslonaPOI ();
  static owterrain *rebelCampPOI ();
  static owterrain *blackmarketPOI ();

  static int GetPOIConfigIndex (owterrain *terra);

public:
  static void ResetAutoSaveTimer ();
  static bool CheckAutoSaveTimer (); // increments, resets

  static inline void ScheduleImmediateSave () { PerformImmSave = true; }

private:
  static int AutoSaveTimer;
  static bool PerformImmSave;
  static bool JustLoaded;

public:
  static void SetMiniMapActive (bool active);
  static bool IsMiniMapActive () { return mMiniMapActive; }

  static void SetMiniMapSmall (bool small) { mMiniMapSmall = small; }
  static bool IsMiniMapSmall () { return mMiniMapSmall; }

  static int GetMiniMapXSize ();
  static int GetMiniMapYSize ();

  static v2 CalcMiniMapXY0 (v2 center);
  static void ShiftMiniMap (v2 delta);

  static bitmap *PrepareMiniMapBitmap (v2 Size);

  // return `ERROR_V2` if out of screen
  static v2 CalcMiniSquareScrCoords (v2 mappos);
  static v2 CalcMiniSquareScrSize ();

private:
  static bool mMiniMapActive;
  static bool mMiniMapSmall;
  static v2 mMiniMapXY0;
  static v2 mMiniMapCurPos;
  static v2 mMiniMapScrPos; // set by renderer

private:
  static std::unordered_map<feuLong, festring> mItemNotes;

public:
  static void WipeItemNotes ();
  static void RemoveItemNote (citem *Item);
  static truth HasItemNote (citem *Item);
  static festring GetItemNote (citem *Item);
  static void SetItemNote (citem *Item, cfestring &note);
};



//K8 WARNING! ABSOLUTELY NON-PORTABLE AND CAUSES UB!
//#define BCLAMP(c)  (((c)&0xff)|(255-((-(int)((c) < 256))>>24)))
#define BCLAMP(c)  ((c) > 255 ? 255 : (c) < 0 ? 0 : (c))
inline void game::CombineLights (col24 &L1, col24 L2) {
  if (L2) {
    if (L1) {
#if! defined(IVAN_NEW_LIGHTS)
# if !defined(IVAN_NEW_INTENSITY)
      sLong Red1 = (L1&0xFF0000), Red2 = (L2&0xFF0000);
      sLong New = (Red1 >= Red2 ? Red1 : Red2);
      sLong Green1 = (L1&0xFF00), Green2 = (L2&0xFF00);
      New |= (Green1 >= Green2 ? Green1 : Green2);
      sLong Blue1 = (L1&0xFF), Blue2 = (L2&0xFF);
      L1 = New|(Blue1 >= Blue2 ? Blue1 : Blue2);
# else
      int l1i = (int)(0.2126*((L1>>16)&0xFF)+0.7152*((L1>>8)&0xFF)+0.0722*(L1&0xFF)+0.5);
      int l2i = (int)(0.2126*((L2>>16)&0xFF)+0.7152*((L2>>8)&0xFF)+0.0722*(L2&0xFF)+0.5);
      //if (l2i > l1i) L1 = L2;
      l1i = BCLAMP(l1i);
      l2i = BCLAMP(l2i);
      int r = (int)(((L1>>16)&0xFF)*(l1i/255.0f)+((L2>>16)&0xFF)*(l2i/255.0f));
      int g = (int)(((L1>>8)&0xFF)*(l1i/255.0f)+((L2>>8)&0xFF)*(l2i/255.0f));
      int b = (int)((L1&0xFF)*(l1i/255.0f)+(L2&0xFF)*(l2i/255.0f));
      r = BCLAMP(r);
      g = BCLAMP(g);
      b = BCLAMP(b);
      L1 = (r<<16)|(g<<8)|b;
# endif
#else
# define L_COEFF (0.05f)
      int r = (int)(((L1>>16)&0xFF)*L_COEFF+((L2>>16)&0xFF)*L_COEFF);
      int g = (int)(((L1>>8)&0xFF)*L_COEFF+((L2>>8)&0xFF)*L_COEFF);
      int b = (int)((L1&0xFF)*L_COEFF+(L2&0xFF)*L_COEFF);
      r = BCLAMP(r);
      g = BCLAMP(g);
      b = BCLAMP(b);
      L1 = (r<<16)|(g<<8)|b;
# undef L_COEFF
#endif
    } else {
      L1 = L2;
    }
  }
}
#undef BCLAMP


inline col24 game::CombineConstLights (col24 L1, col24 L2) {
  CombineLights(L1, L2);
  return L1;
}


inline truth game::IsDark (col24 Light) {
  return
    !Light ||
    ((Light & 0xFF0000) < (LIGHT_BORDER << 16) &&
     (Light & 0x00FF00) < (LIGHT_BORDER << 8) &&
     (Light & 0x0000FF) < LIGHT_BORDER);
}

inline int game::CompareLights (col24 L1, col24 L2) {
  if (L1) {
    if ((L1 & 0xFF0000) > (L2 & 0xFF0000) ||
        (L1 & 0x00FF00) > (L2 & 0x00FF00) ||
        (L1 & 0x0000FF) > (L2 & 0x0000FF))
      return 1;
    if ((L1 & 0xFF0000) == (L2 & 0xFF0000) ||
        (L1 & 0x00FF00) == (L2 & 0x00FF00) ||
        (L1 & 0x0000FF) == (L2 & 0x0000FF))
      return 0;
    return -1;
  }
  return -int(!!L2);
}

inline int game::CompareLightToInt (col24 L, col24 Int) {
  if ((L & 0xFF0000) > Int || (L & 0xFF00) > Int || (L & 0xFF) > Int) return 1;
  if ((L & 0xFF0000) == Int || (L & 0xFF00) == Int || (L & 0xFF) == Int) return 0;
  return -1;
}


#endif
