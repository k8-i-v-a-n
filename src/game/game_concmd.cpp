/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
// directly included from "game.cpp"
#include "command.h"

#ifdef SHITDOZE
// fuck you, shitdoze!
# ifdef CharUpper
#  undef CharUpper
# endif
#endif


//==========================================================================
//
//  game::SaveScreenshot
//
//==========================================================================
void game::SaveScreenshot (cfestring &basefname, bool doRedraw) {
  festring fname;

  if (basefname.IsEmpty()) {
    festring dir;
    dir << game::GetScreenshotPath();
    festring timestr;
    time_t t = time(NULL);
    struct tm *ts = localtime(&t);
    if (ts) {
      timestr << (int)(ts->tm_year%100);
      int t = ts->tm_mon + 1;
      if (t < 10) timestr << '0'; timestr << t;
      t = ts->tm_mday; if (t < 10) timestr << '0'; timestr << t;
      timestr << '_';
      t = ts->tm_hour; if (t < 10) timestr << '0'; timestr << t;
      t = ts->tm_min; if (t < 10) timestr << '0'; timestr << t;
      t = ts->tm_sec; if (t < 10) timestr << '0'; timestr << t;
    } else {
      timestr = "heh";
    }
    festring ext = CONST_S(".png");
    fname = dir + "screenshot_" + timestr;
    if (inputfile::fileExists(fname + ext)) {
      for (int f = 0; f < 1000; f += 1) {
        char buf[16];
        sprintf(buf, "%03d", f);
        festring fn = fname + buf;
        if (!inputfile::fileExists(fn + ext)) {
          fname = fn;
          break;
        }
      }
    }
    fname << ext;
  } else {
    festring dir;
    dir << game::GetScreenshotPath();
    fname = dir + basefname;
    if (!fname.EndsWithCI(".png")) fname << ".png";
  }

  if (doRedraw) {
    game::DrawEverything();
  }

  ConLogf("saving screenshot to '%s'...", fname.CStr());
  DOUBLE_BUFFER->SavePNG(fname);
}


bool game::inScreenShoter = false;

//==========================================================================
//
//  game::Screenshoter
//
//==========================================================================
bool game::Screenshoter (int Key) {
  //fprintf(stderr, "PRE: 0x%08x\n", Key);
  if (!inScreenShoter && !game::InGetPlayerCommand) {
    command *cmd = commandsystem::FindCommandByKey(Key);
    if (cmd && cmd->GetName() == "SaveScreenshot") {
      /*
      const bool doRedraw = (game::InGetPlayerCommand && PLAYER && PLAYER->IsEnabled());
      if (doRedraw) {
        inScreenShoter = true;
        try {
          if (!game::TruthQuestion(CONST_S("Do you want to save screenshot?"), REQUIRES_ANSWER)) {
            inScreenShoter = false;
            return true;
          }
        } catch (...) {
          inScreenShoter = false;
          throw;
        }
      }
      */
      game::SaveScreenshot(festring::EmptyStr(), /*doRedraw*/false);
      return true;
    }
  }
  return false;
}



// ////////////////////////////////////////////////////////////////////////// //
// ConGVar
// ////////////////////////////////////////////////////////////////////////// //
class ConGVar : public ConCmd {
private:
  festring varname;
  bool readOnly;

public:
  ConGVar () = delete;
  ConGVar (const char *name, const char *avarname);

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConGVar::ConGVar
//
//==========================================================================
ConGVar::ConGVar (const char *name, const char *avarname)
  : ConCmd(name, false)
{
  IvanAssert(avarname && avarname[0]);
  varname << avarname;
  IvanAssert(game::HasGlobalValue(varname));
  readOnly = !game::IsMutableGlobal(varname);
}


//==========================================================================
//
//  ConGVar::Do
//
//==========================================================================
void ConGVar::Do (const std::vector<festring> &args) {
  if (args.size() == 1) {
    ConLogf("%s: %d", args[0].CStr(), (int)game::FindGlobalValue(varname, 0));
  } else if (args.size() == 2 && readOnly) {
    ConLogf("ERROR: variable `%s` is read-only", args[0].CStr());
  } else if (args.size() == 2 && !readOnly) {
    int v = 0;
    if (ConParseInt(args[1], &v)) {
      if (!game::SetGlobalValue(varname, v)) {
        ConLogf("ERROR: invalid value `%s` for variable `%s` (cannot set)",
                args[1].CStr(), args[0].CStr());
      } else {
        ConLogf("%s: %d", args[0].CStr(), (int)game::FindGlobalValue(varname, 0));
      }
    } else {
      ConLogf("ERROR: invalid value `%s` for variable `%s` (integer expected)",
              args[1].CStr(), args[0].CStr());
    }
  } else {
    ConLogf("ERROR: variable `%s` requires one integer value", args[0].CStr());
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdExec
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdExec : public ConCmd {
private:
  int level;

public:
  ConCmdExec () : ConCmd("exec"), level(0) {}

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdExec::Do
//
//==========================================================================
void ConCmdExec::Do (const std::vector<festring> &args) {
  if (args.size() == 2) {
    FILE *fl = fopen(args[1].CStr(), "r");
    if (!fl) {
      ConLogf("ERROR: no file '%s'!", args[1].CStr());
    } else {
      if (level > 32) {
        fclose(fl);
        ABORT("exec is too deeply nested");
      }
      level += 1;
      char buf[1024];
      try {
        while (fgets(buf, (int)sizeof(buf) - 1, fl)) {
          buf[sizeof(buf) - 1] = 0; // just in case, lol
          festring s; s << buf;
          s.TrimAll();
          if (!s.IsEmpty() && s.CStr()[0] != '#') {
            ConExecute(s.CStr());
          }
        }
      } catch (...) {
        fclose(fl);
        level -= 1;
        throw;
      }
      fclose(fl);
      level -= 1;
    }
  } else {
    ConLogf("ERROR: exec what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdScreenshot
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdScreenshot : public ConCmd {
public:
  ConCmdScreenshot () : ConCmd("Screenshot") {}

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdScreenshot::Do
//
//==========================================================================
void ConCmdScreenshot::Do (const std::vector<festring> &args) {
  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s [filename]", args[0].CStr());
    return;
  }

  const bool doRedraw = (game::InGetPlayerCommand && PLAYER && PLAYER->IsEnabled());

  graphics::SetConsoleActive(false);
  if (args.size() == 1) {
    game::SaveScreenshot(festring::EmptyStr(), doRedraw);
  } else {
    game::SaveScreenshot(args[1], doRedraw);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConInGameCmd
// ////////////////////////////////////////////////////////////////////////// //

class ConInGameCmd : public ConCmd {
public:
  ConInGameCmd (const char *name) : ConCmd(name) {}
  virtual bool Enabled (bool verbose) override;
protected:
  static void CheckMode ();
};


//==========================================================================
//
//  ConInGameCmd::Enabled
//
//==========================================================================
bool ConInGameCmd::Enabled (bool verbose) {
  if (game::InGetPlayerCommand && PLAYER && PLAYER->IsEnabled()) {
    return true;
  } else {
    if (verbose) {
      ConLogf("ERROR: cannot execute command '%s' now.", GetName());
    }
    return false;
  }
}


//==========================================================================
//
//  ConInGameCmd::CheckMode
//
//==========================================================================
void ConInGameCmd::CheckMode () {
  IvanAssert(game::InGetPlayerCommand);
  IvanAssert(PLAYER);
  IvanAssert(PLAYER->IsEnabled());
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdTicks
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdTicks : public ConInGameCmd {
public:
  ConCmdTicks () : ConInGameCmd("Ticks") {}

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdTicks::Do
//
//==========================================================================
void ConCmdTicks::Do (const std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 1) {
    ivantime Time;
    game::GetTime(Time);
    ConLogf("Current game time: tick %u, turn %d, day %d, %d:%02d",
            game::GetTick(), game::GetTurn(),
            Time.Day, Time.Hour, Time.Min);
  } else {
    ConLogf("ERROR: ticks what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class ConCmdMyPos : public ConInGameCmd {
public:
  ConCmdMyPos () : ConInGameCmd("MyPos") {}

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdMyPos::Do
//
//==========================================================================
void ConCmdMyPos::Do (const std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 1) {
    v2 xy = PLAYER->GetPos();
    if (!game::IsInWilderness()) {
      ConLogf("Dungeon #%d; Level #%d; Pos: (%d, %d)",
              game::GetCurrentDungeonIndex(), game::GetCurrentLevelIndex() + 1,
              xy.X, xy.Y);
    } else {
      ConLogf("Coordinates: X=%d; Y=%d", xy.X, xy.Y);
    }
  } else {
    ConLogf("ERROR: mypos what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdSave
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdSave : public ConInGameCmd {
public:
  ConCmdSave () : ConInGameCmd("Save") {}

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdSave::Do
//
//==========================================================================
void ConCmdSave::Do (const std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 1) {
    ConLogf("saving the game...");
    graphics::SetConsoleActive(false);
    game::Save(true/*vacuum*/);
    SKIP_GET_KEY(KEY_CC_REDRAW);
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdAbort
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdAbort : public ConInGameCmd {
public:
  ConCmdAbort () : ConInGameCmd("AbortGame") {}

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdAbort::Do
//
//==========================================================================
void ConCmdAbort::Do (const std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 1) {
    graphics::SetConsoleActive(false);
    game::AbortGame();
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdLevelTeleport
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdLevelTeleport : public ConInGameCmd {
public:
  ConCmdLevelTeleport () : ConInGameCmd("LevelTeleport") {}

  virtual void Do (const std::vector<festring> &args) override;
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdLevelTeleport::AutocompleteLastArg
//
//==========================================================================
int ConCmdLevelTeleport::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2 && !game::IsInWilderness()) {
    std::vector<festring> list;
    for (int f = 1; f <= game::GetLevels(); f += 1) {
      festring s;
      s << f;
      list.push_back(s);
    }
    return ConCalculateACVector(args[1], list);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdLevelTeleport::Do
//
//==========================================================================
void ConCmdLevelTeleport::Do (const std::vector<festring> &args) {
  if (args.size() == 1) {
    if (!game::IsInWilderness()) {
      ConLogf("Dungeon #%d; Level #%d",
              game::GetCurrentDungeonIndex(), game::GetCurrentLevelIndex() + 1);
    } else {
      ConLogf("in wilderness.");
    }
  } else if (args.size() == 2 && game::IsInWilderness()) {
    ConLogf("There is no planet 'B'.");
  } else if (args.size() == 2) {
    int newlevel = -1;
    game::SetSkipHiScrore();
    if (ConParseInt(args[1], &newlevel)) {
      if (newlevel <= 0 || newlevel > game::GetLevels()) {
        ConLogf("There is no level %d in this dungeon, %s!", newlevel, game::Insult());
      } else if (newlevel == game::GetCurrentLevelIndex() + 1) {
        ConLogf("You are already here, %s!", game::Insult());
      } else {
        game::TryTravel(game::GetCurrentDungeonIndex(), newlevel - 1, RANDOM,
                        /*AllowHostiles*/true, /*AlliesFollow*/true);
        SKIP_GET_KEY(KEY_CC_REDRAW);
      }
    } else {
      ConLogf("You %s.", game::Insult());
    }
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdWizardMode
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdWizardMode : public ConInGameCmd {
public:
  ConCmdWizardMode () : ConInGameCmd("CheatingWizard") {}

  virtual void Do (const std::vector<festring> &args) override;
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdWizardMode::AutocompleteLastArg
//
//==========================================================================
int ConCmdWizardMode::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2) {
    return ConCalculateAC(args[1], "on", "off", NULL);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdWizardMode::Do
//
//==========================================================================
void ConCmdWizardMode::Do (const std::vector<festring> &args) {
  if (args.size() == 1) {
    ConLogf("Wizard mode is %sactive.", (game::WizardModeIsActive() ? "" : "not "));
  } else if (args.size() == 2 && args[1] == "YABBADABBADOO!") {
    game::ResetSkipHiScrore();
  } else if (args.size() == 2) {
    bool v = false;
    if (ConParseBool(args[1], &v)) {
      game::SetSkipHiScrore();
      if (v) {
        game::ActivateWizardMode();
      } else {
        game::DeactivateWizardMode();
      }
    } else {
      ConLogf("wut?!");
      return;
    }
    ConLogf("Wizard mode is %sactive.", (game::WizardModeIsActive() ? "" : "not "));
    if (!game::WizardModeIsActive()) {
      // turn off some cheats
      game::SetSeeWholeMapCheatMode(0);
      if (game::GoThroughWallsCheatIsActive()) {
        game::GoThroughWalls();
      }
    }
    SKIP_GET_KEY(KEY_CC_REDRAW);
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdAddItem
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdAddItem : public ConInGameCmd {
public:
  ConCmdAddItem () : ConInGameCmd("Gimme") {}

  virtual void Do (const std::vector<festring> &args) override;
  // change last arg.
  // return 0 if no autocompletion, -1 if partial, 1 if full
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


//==========================================================================
//
//  MkMaterial
//
//==========================================================================
static material *MkMaterial (const char *name) {
  return protosystem::CreateMaterialByName(name);
}


//==========================================================================
//
//  GetEnchant
//
//==========================================================================
static int GetEnchant (festring &sname) {
  festring::sizetype slen = sname.GetSize();
  festring::sizetype spos = slen;
  int n = 0;
  while (spos != 0 && sname[spos - 1] >= '0' && sname[spos - 1] <= '9') {
    spos -= 1;
  }
  if (spos != 0 && (sname[spos - 1] == '-' || sname[spos - 1] == '+')) {
    festring::sizetype cpos = spos;
    const bool neg = (sname[spos - 1] == '-');
    while (cpos != slen) {
      n = n * 10 + sname[cpos] - '0';
      cpos += 1;
    }
    if (neg) n = -n;
    sname.Left((int)cpos);
  }
  sname.TrimAll();
  return n;
}


//==========================================================================
//
//  ConCmdAddItem::AutocompleteLastArg
//
//  change last arg.
//  return 0 if no autocompletion, -1 if partial, 1 if full
//
//==========================================================================
int ConCmdAddItem::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2 || args.size() == 3) {
    std::vector<festring> list;
    protosystem::GetMaterialNamesWithPrefix(list, args[args.size() - 1]);
    if (list.size() == 0) return 0;
    if (list.size() == 1) {
      args[args.size() - 1] = list[0];
      return 1;
    }
    return ConCalculateACVector(args[args.size() - 1], list);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdAddItem::Do
//
//==========================================================================
void ConCmdAddItem::Do (const std::vector<festring> &args) {
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s [material] [secondary] name [+ench/-ench]", args[0].CStr());
    return;
  }

  if (args.size() == 1) {
    ConLogf("give what?");
  } else if (args.size() > 1) {
    material *mt1 = 0;
    material *mt2 = 0;
    size_t aidx = 1;
    while (aidx != args.size() && !mt2) {
      material *mt = MkMaterial(args[aidx].CStr());
      if (!mt) break;
      if (!mt1) mt1 = mt; else mt2 = mt;
      aidx += 1;
    }
    if (aidx == args.size()) {
      ConLogf("ERROR: no item, only materials. wtf?!");
    } else {
      festring iname;
      while (aidx != args.size()) {
        if (!iname.IsEmpty()) iname.AppendChar(' ');
        iname << args[aidx];
        aidx += 1;
      }
      int ench = GetEnchant(iname);
      item *newItem = protosystem::CreateItemForScript(iname);
      if (newItem) {
        if (mt1) {
          if (newItem->GetMainMaterial()) {
            newItem->ChangeMainMaterial(mt1);
          } else {
            delete mt1;
          }
        }
        if (mt2) {
          if (newItem->GetSecondaryMaterial()) {
            newItem->ChangeSecondaryMaterial(mt2);
          } else {
            delete mt2;
          }
        }
        if (ench != 0) newItem->SetEnchantment(ench);
        game::SetSkipHiScrore();
        PLAYER->GetStack()->AddItem(newItem);
        newItem->SpecialGenerationHandler();
        if (newItem->HandleInPairs()) {
          ConLogf("given: %s", newItem->CHAR_NAME(PLURAL));
        } else {
          ConLogf("given: %s", newItem->CHAR_NAME(INDEFINITE));
        }
        SKIP_GET_KEY(KEY_CC_REDRAW);
      } else {
        delete mt1;
        delete mt2;
        ConLogf("unknown item: %s", iname.CStr());
      }
    }
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdPlayerCommand
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdPlayerCommand : public ConInGameCmd {
private:
  command *cmd;

public:
  ConCmdPlayerCommand (const char *name, const char *ccname);

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdPlayerCommand::ConCmdPlayerCommand
//
//==========================================================================
ConCmdPlayerCommand::ConCmdPlayerCommand (const char *name, const char *ccname)
  : ConInGameCmd(name)
{
  cmd = commandsystem::FindCommand(ccname);
  IvanAssert(cmd);
}


//==========================================================================
//
//  ConCmdPlayerCommand::Do
//
//==========================================================================
void ConCmdPlayerCommand::Do (const std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 1) {
    graphics::SetConsoleActive(false);
    game::SetSkipHiScrore();
    if (game::IsInWilderness() && !cmd->IsUsableInWilderness()) {
      ConLogf("ERROR: this command is not usable in wilderness.");
    } else if (!game::WizardModeIsActive() && cmd->IsWizardModeFunction()) {
      ConLogf("ERROR: this command requires WizardMode!");
    } else {
      if (cmd->GetLinkedFunction()(PLAYER)) {
        SKIP_GET_KEY(KEY_CC_ACTED);
      } else {
        SKIP_GET_KEY(KEY_CC_REDRAW);
      }
    }
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdSummon
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdSummon : public ConInGameCmd {
public:
  ConCmdSummon () : ConInGameCmd("Summon") {}

  virtual void Do (const std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdSummon::Do
//
//==========================================================================
void ConCmdSummon::Do (const std::vector<festring> &args) {
  int friendly = -1;
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s <friendly|hostile|neutral> name", args[0].CStr());
    ConLogf("usage: %s [+|-|*]name", args[0].CStr());
    return;
  }

  if (args.size() > 1) {
    bool seenSpec = true;
    size_t aidx = 2;
         if (args[1].EquCI("friendly")) friendly = 1;
    else if (args[1].EquCI("hostile")) friendly = -1;
    else if (args[1].EquCI("neutral")) friendly = 0;
    else { seenSpec = false; aidx = 1; }

    int gold = 0;
    festring name;
    while (aidx != args.size()) {
      if (aidx == args.size() - 2 && args[aidx].EquCI("gold")) {
        gold = Max(0, atoi(args[aidx + 1].CStr()));
        aidx += 2;
      } else {
        if (!name.IsEmpty()) name.AppendChar(' ');
        name << args[aidx];
        aidx += 1;
      }
    }

    if (name.IsEmpty()) {
      ConLogf("ERROR: exec what?");
    } else {
      switch (name[0]) {
        case '-': if (!seenSpec) friendly = -1; name.Erase(0, 1); break;
        case '+': if (!seenSpec) friendly = 1; name.Erase(0, 1); break;
        case '*': if (!seenSpec) friendly = 0; name.Erase(0, 1); break;
        default:
          /*
          if (!seenSpec) {
            ConLogf("hostility?");
            return;
          }
          */
          break;
      }
      name = name.UndersToSpacesCopy();
      name.TrimAll();
      if (name.IsEmpty()) {
        ConLogf("ERROR: exec what?");
      } else {
        character *Summoned = protosystem::CreateMonsterCheat(name);
        if (!Summoned) {
          ConLogf("i don't know what '%s' is.", name.CStr());
        } else {
          game::SetSkipHiScrore();
          if (friendly > 0) {
            Summoned->SetTeam(game::GetTeam(PLAYER_TEAM));
          } else if (friendly < 0) {
            Summoned->SetTeam(game::GetTeam(MONSTER_TEAM));
          } else {
            //FIXME: this is wrong!
            Summoned->SetTeam(game::GetTeam(INDIFFERENT_TEAM));
          }

          if (!Summoned->PutNear(PLAYER->GetPos(), true)) {
            //Summoned->Disable();
            //Summoned->SendToHell();
            delete Summoned;
            ConLogf("This place is too crowded.");
          } else {
            Summoned->EditMoney(gold);
            SKIP_GET_KEY(KEY_CC_REDRAW);
          }
        }
      }
    }
  } else {
    ConLogf("ERROR: exec what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdEdit
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdEdit : public ConInGameCmd {
public:
  ConCmdEdit () : ConInGameCmd("Edit") {}

  virtual void Do (const std::vector<festring> &args) override;
  // change last arg.
  // return 0 if no autocompletion, -1 if partial, 1 if full
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdEdit::AutocompleteLastArg
//
//  change last arg.
//  return 0 if no autocompletion, -1 if partial, 1 if full
//
//==========================================================================
int ConCmdEdit::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2) {
    return ConCalculateAC(args[1],
      "Agility",
      "AStr",
      "Charisma",
      "Dexterity",
      "Endurance",
      "Gold",
      "Intelligence",
      "LStr",
      "Mana",
      "Perception",
      "Wisdom",
      NULL);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdEdit::Do
//
//==========================================================================
void ConCmdEdit::Do (const std::vector<festring> &args) {
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s attr value", args[0].CStr());
    return;
  }

  if (args.size() == 2 || args.size() == 3) {
    if (args[1].GetSize() == 1) {
      if (args[1][0] == 'A') {
        ConPrintf("ERROR: edit what? agility or arm_strength?");
        return;
      }
    }

    if (args[1].EquCI("GOLD")) {
      if (args.size() == 3) {
        int val = 0;
        if (ConParseInt(args[2], &val)) {
          game::SetSkipHiScrore();
          PLAYER->SetMoney(Clamp(val, 0, 1000000));
          SKIP_GET_KEY(KEY_CC_REDRAW);
        } else {
          ConLogf("ERROR: '%s' is not a number!", args[2].CStr());
        }
      }
      ConLogf("GOLD: %d", PLAYER->GetMoney());
      return;
    }

    int aidx = -1;

         if (args[1].EquCI("AGILITY")) aidx = AGILITY;
    else if (args[1].EquCI("ASTR")) aidx = ARM_STRENGTH;
    else if (args[1].EquCI("CHARISMA")) aidx = CHARISMA;
    else if (args[1].EquCI("DEXTERITY")) aidx = DEXTERITY;
    else if (args[1].EquCI("ENDURANCE")) aidx = ENDURANCE;
    else if (args[1].EquCI("INTELLIGENCE")) aidx = INTELLIGENCE;
    else if (args[1].EquCI("LSTR")) aidx = LEG_STRENGTH;
    else if (args[1].EquCI("MANA")) aidx = MANA;
    else if (args[1].EquCI("PERCEPTION")) aidx = PERCEPTION;
    else if (args[1].EquCI("WISDOM")) aidx = WISDOM;
    else { ConLogf("ERROR: what is '%s'?", args[1].CStr()); return; }

    if (args.size() == 3) {
      int val = 0;
      if (ConParseInt(args[2], &val)) {
        game::SetSkipHiScrore();
        PLAYER->EditAttribute(aidx, val);
        SKIP_GET_KEY(KEY_CC_REDRAW);
      } else {
        ConLogf("ERROR: '%s' is not a number!", args[2].CStr());
      }
    }
    ConLogf("%s is %d", args[1].CStr(), PLAYER->GetAttribute(aidx));
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdState
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdState : public ConInGameCmd {
public:
  ConCmdState () : ConInGameCmd("State") {}

  virtual void Do (const std::vector<festring> &args) override;
  // change last arg.
  // return 0 if no autocompletion, -1 if partial, 1 if full
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


struct StateName {
  const char *constname;
  const char *name;
};

static const StateName ConStateList[] = {
  {.constname="HASTE", .name="Haste"},
  {.constname="SLOW", .name="Slow"},
  {.constname="POLYMORPH_CONTROL", .name="PolyControl"},
  {.constname="LYCANTHROPY", .name="Lycanthropy"},
  {.constname="INVISIBLE", .name="Invisible"},
  {.constname="INFRA_VISION", .name="Infravision"},
  {.constname="ESP", .name="ESP"},
  {.constname="POISONED", .name="Poisoned"},
  {.constname="POLYMORPH", .name="Polymorph"},
  {.constname="TELEPORT_CONTROL", .name="TeleControl"},
  {.constname="PANIC", .name="Panic"},
  {.constname="CONFUSED", .name="Confused"},
  {.constname="PARASITE_TAPE_WORM", .name="TapeWorm"},
  {.constname="SEARCHING", .name="Searching"},
  {.constname="GAS_IMMUNITY", .name="GasImmunity"},
  {.constname="LEVITATION", .name="Levitation"},
  {.constname="LEPROSY", .name="Leprosy"},
  {.constname="HICCUPS", .name="Hiccups"},
  {.constname="VAMPIRISM", .name="Vampirism"},
  {.constname="SWIMMING", .name="Swimming"},
  {.constname="DETECTING", .name="Detecting"},
  {.constname="ETHEREAL_MOVING", .name="Ethereal"},
  {.constname="FEARLESS", .name="Fearless"},
  {.constname="POLYMORPH_LOCK", .name="PolyLock"},
  {.constname="REGENERATION", .name="Regeneration"},
  {.constname="DISEASE_IMMUNITY", .name="DiseaseImmunity"},
  {.constname="TELEPORT_LOCK", .name="TeleLock"},
  {.constname="FASTING", .name="Fasting"},
  {.constname="PARASITE_MIND_WORM", .name="MindWorm"},
  {.constname=NULL, .name=NULL},
};


//==========================================================================
//
//  ConCmdState::AutocompleteLastArg
//
//==========================================================================
int ConCmdState::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2) {
    std::vector<festring> names;
    names.push_back(CONST_S("show"));
    for (int f = 0; ConStateList[f].constname; f += 1) {
      if (ConStateList[f].name) {
        names.push_back(CONST_S(ConStateList[f].name));
      }
    }
    return ConCalculateACVector(args[1], names);
  } else if (args.size() == 3) {
    return ConCalculateAC(args[2], "set", "reset", NULL);
  } else {
    return 0;
  }
}


//==========================================================================
//
//  ConCmdState::Do
//
//==========================================================================
void ConCmdState::Do (const std::vector<festring> &args) {
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s name [set|reset]", args[0].CStr());
    return;
  }

  if (args.size() == 2 && args[1].EquCI("show")) {
    // list states
    game::SetSkipHiScrore();
    ConLogf("---------------");
    for (int c = 0; c < STATES; ++c) {
      if (PLAYER->StateIsActivated(1 << c) &&
          ((1 << c) != HASTE || !PLAYER->StateIsActivated(SLOW)) &&
          ((1 << c) != SLOW || !PLAYER->StateIsActivated(HASTE)))
      {
        ConPrintf("state %s: ", PLAYER->GetStateDesctiption(c));
        festring ss;
        int counter = PLAYER->GetTempStateCounterInternal(c);
        if (counter >= PERMANENT) {
          ConLogf("permanent");
        } else {
          ConLogf("%d", counter);
        }
      } else {
        //ConLogf("state %s: not set", PLAYER->GetStateDesctiption(c));
      }
    }
  } else if (args.size() == 2 || args.size() == 3) {
    const char *sname = 0;
    int sidx = -1;
    for (int f = 0; ConStateList[f].constname && sidx == -1; f += 1) {
      if (ConStateList[f].name && args[1].EquCI(ConStateList[f].name)) {
        sidx = game::GetGlobalConst(CONST_S(ConStateList[f].constname));
      }
    }
    if (sidx != -1) {
      for (int c = 0; c < STATES; ++c) {
        if (sidx == (1 << c)) {
          sname = PLAYER->GetStateDesctiption(c);
          break;
        }
      }
    }
    if (sidx == -1) {
      ConLogf("ERROR: unknown state '%s'!", args[1].CStr());
    } else if (args.size() == 3) {
      if (args[2].EquCI("set")) {
        game::SetSkipHiScrore();
        PLAYER->BeginTemporaryState(sidx, PERMANENT);
        ConLogf("activated state %s", sname);
        SKIP_GET_KEY(KEY_CC_REDRAW);
      } else if (args[2].EquCI("reset")) {
        game::SetSkipHiScrore();
        if (PLAYER->TemporaryStateIsActivated(sidx)) {
          PLAYER->DeActivateTemporaryState(sidx);
          ConLogf("deactivated state %s", sname);
          SKIP_GET_KEY(KEY_CC_REDRAW);
        } else {
          ConLogf("state %s was not active", sname);
        }
      } else {
        ConLogf("ERROR: what '%s' means?", args[2].CStr());
      }
    } else {
      game::SetSkipHiScrore();
      ConPrintf("state %s: ", sname);
      if (PLAYER->StateIsActivated(sidx)) {
        int counter = PLAYER->GetTempStateCounterInternal(sidx);
        if (counter >= PERMANENT) {
          ConLogf("permanent");
        } else {
          ConLogf("%d", counter);
        }
      } else {
        ConLogf("not set");
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdMaterialInfo
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdMaterialInfo : public ConInGameCmd {
public:
  ConCmdMaterialInfo () : ConInGameCmd("MaterialInfo") {}

  virtual void Do (const std::vector<festring> &args) override;
  // change last arg.
  // return 0 if no autocompletion, -1 if partial, 1 if full
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;

private:
  static void ShowListAndFree (std::vector<material *> &list);
};


//==========================================================================
//
//  ConCmdMaterialInfo::AutocompleteLastArg
//
//  change last arg.
//  return 0 if no autocompletion, -1 if partial, 1 if full
//
//==========================================================================
int ConCmdMaterialInfo::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2) {
    std::vector<festring> list;
    protosystem::GetMaterialNamesWithPrefix(list, args[args.size() - 1]);
    if (list.size() == 0) return 0;
    if (list.size() == 1) {
      args[args.size() - 1] = list[0];
      return 1;
    }
    return ConCalculateACVector(args[args.size() - 1], list);
  } else if (args.size() == 3) {
    return ConCalculateAC(args[args.size() - 1], "soften", "harden", "chain", NULL);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdMaterialInfo::ShowListAndFree
//
//==========================================================================
void ConCmdMaterialInfo::ShowListAndFree (std::vector<material *> &list) {
  if (list.size() != 0) {
    festring ss;

    // calculate maximum name length, for nicer output
    int maxnn = 0;
    for (size_t f = 0; f != list.size(); f += 1) {
      ss.Empty(); list[f]->AddName(ss, false, false);
      maxnn = Max(maxnn, ss.Length());
    }
    maxnn += 2;

    // print the list
    ConPrintf("NAME");
    for (int c = 4; c < maxnn; c += 1) ConPrintf(" ");
    ConLogf(" STR FLEX DENS");
    for (int c = 1; c < maxnn; c += 1) ConPrintf("-");
    ConLogf(" ---- ---- ----");
    for (size_t f = 0; f != list.size(); f += 1) {
      ss.Empty(); list[f]->AddName(ss, false, false);
      ConPrintf("%s", ss.CStr());
      for (int c = ss.Length(); c < maxnn; c += 1) ConPrintf(" ");
      ConLogf("%4d %4d %4d", list[f]->GetStrengthValue(), list[f]->GetFlexibility(),
              list[f]->GetDensity());
      // and delete the material, why not
      delete list[f]; list[f] = 0;
    }
  }
}


//==========================================================================
//
//  ConCmdMaterialInfo::Do
//
//==========================================================================
void ConCmdMaterialInfo::Do (const std::vector<festring> &args) {
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s material", args[0].CStr());
    return;
  }

  if (args.size() == 1) {
    ConLogf("info about what?");
  } else if (args.size() == 2 || args.size() == 3) {
    material *mt = MkMaterial(args[1].CStr());
    if (mt) {
      game::SetSkipHiScrore();
      //festring omtname; mt->AddName(omtname, false, false);

      if (args.size() == 3) {
        if (args[2].EquCI("chain")) {
          ConLogf("========= material chain =========");
          // first, find the weakest one
          while (mt->GetDataBase()->SoftenedMaterial) {
            const int cfg = mt->GetDataBase()->SoftenedMaterial;
            delete mt;
            mt = MAKE_MATERIAL(cfg);
          }
          // now collect the whole chain
          std::vector<material *> list;
          do {
            list.push_back(mt);
            const int cfg = mt->GetDataBase()->HardenedMaterial;
            if (cfg) mt = MAKE_MATERIAL(cfg); else mt = 0;
          } while (mt);
          // and print
          ShowListAndFree(list);
        } else if (args[2].EquCI("soften")) {
          ConLogf("========= soften chain =========");
          std::vector<material *> list;
          do {
            list.push_back(mt);
            const int cfg = mt->GetDataBase()->SoftenedMaterial;
            if (cfg) mt = MAKE_MATERIAL(cfg); else mt = 0;
          } while (mt);
          ShowListAndFree(list);
        } else if (args[2].EquCI("harden")) {
          ConLogf("========= harden chain =========");
          std::vector<material *> list;
          do {
            list.push_back(mt);
            const int cfg = mt->GetDataBase()->HardenedMaterial;
            if (cfg) mt = MAKE_MATERIAL(cfg); else mt = 0;
          } while (mt);
          ShowListAndFree(list);
        } else {
          ConLogf("wut?");
        }
      } else {
        festring ss; mt->AddName(ss, false, false);
        ConLogf("*** material '%s': str=%d; flex=%d; dens=%d",
                ss.CStr(), mt->GetStrengthValue(), mt->GetFlexibility(), mt->GetDensity());
        // just show soften/harden materials
        if (mt->GetDataBase()->SoftenedMaterial) {
          material *xmt = MAKE_MATERIAL(mt->GetDataBase()->SoftenedMaterial);
          ss.Empty(); xmt->AddName(ss, false, false);
          ConLogf("softened to '%s': str=%d; flex=%d; dens=%d", ss.CStr(),
                  xmt->GetStrengthValue(), xmt->GetFlexibility(), xmt->GetDensity());
          delete xmt;
        }
        if (mt->GetDataBase()->HardenedMaterial) {
          material *xmt = MAKE_MATERIAL(mt->GetDataBase()->HardenedMaterial);
          ss.Empty(); xmt->AddName(ss, false, false);
          ConLogf("hardened to '%s': str=%d; flex=%d; dens=%d", ss.CStr(),
                  xmt->GetStrengthValue(), xmt->GetFlexibility(), xmt->GetDensity());
          delete xmt;
        }
        delete mt;
      }
    } else {
      ConLogf("ERROR: unknown material '%s'!", args[1].CStr());
    }
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdMiniMap
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdMiniMap : public ConInGameCmd {
public:
  ConCmdMiniMap () : ConInGameCmd("MiniMap") {}

  virtual void Do (const std::vector<festring> &args) override;
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdMiniMap::AutocompleteLastArg
//
//==========================================================================
int ConCmdMiniMap::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2) {
    return ConCalculateAC(args[args.size() - 1], "toggle", "normal", "small", "off", "on", NULL);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdMiniMap::Do
//
//==========================================================================
void ConCmdMiniMap::Do (const std::vector<festring> &args) {
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s <toggle|normal|small|off|on>", args[0].CStr());
    return;
  }

  if (args.size() == 2) {
    if (args[1].EquCI("toggle")) {
      game::SetMiniMapActive(!game::IsMiniMapActive());
    } else if (args[1].EquCI("on")) {
      game::SetMiniMapActive(true);
    } else if (args[1].EquCI("off")) {
      game::SetMiniMapActive(false);
    } else if (args[1].EquCI("normal")) {
      game::SetMiniMapSmall(false);
      game::SetMiniMapActive(true);
    } else if (args[1].EquCI("small")) {
      game::SetMiniMapSmall(true);
      game::SetMiniMapActive(true);
    } else {
      ConLogf("what does '%s' mean?", args[1].CStr());
    }
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdRevealMap
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdRevealMap : public ConInGameCmd {
public:
  ConCmdRevealMap () : ConInGameCmd("RevealMap") {}

  virtual void Do (const std::vector<festring> &args) override;
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdRevealMap::AutocompleteLastArg
//
//==========================================================================
int ConCmdRevealMap::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2) {
    return ConCalculateAC(args[args.size() - 1], "normal", "ignore_darkness", NULL);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdRevealMap::Do
//
//==========================================================================
void ConCmdRevealMap::Do (const std::vector<festring> &args) {
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s [normal|ignore_darkness]", args[0].CStr());
    return;
  }

  if (args.size() == 2) {
    if (args[1].EquCI("normal")) {
      if (game::GetCurrentArea()->IsLevel()) {
        game::SetSkipHiScrore();
        ((level *)game::GetCurrentArea())->Reveal();
        SKIP_GET_KEY(KEY_CC_REDRAW);
      } else {
        ConLogf("ERROR: cannot reveal this map!");
      }
    } else if (args[1].EquCI("ignore_darkness")) {
      if (game::GetCurrentArea()->IsLevel()) {
        game::SetSkipHiScrore();
        ((level *)game::GetCurrentArea())->Reveal(true);
        SKIP_GET_KEY(KEY_CC_REDRAW);
      } else {
        ConLogf("ERROR: cannot reveal this map!");
      }
    } else {
      ConLogf("what does '%s' mean?", args[1].CStr());
    }
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdSeeMap
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdSeeMap : public ConInGameCmd {
public:
  ConCmdSeeMap () : ConInGameCmd("SeeMap") {}

  virtual void Do (const std::vector<festring> &args) override;
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdSeeMap::AutocompleteLastArg
//
//==========================================================================
int ConCmdSeeMap::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2) {
    return ConCalculateAC(args[args.size() - 1],
      "toggle", "hide", "normal", "no_light", NULL);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdSeeMap::Do
//
//==========================================================================
void ConCmdSeeMap::Do (const std::vector<festring> &args) {
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s [toggle|hide|normal|no_light]", args[0].CStr());
    return;
  }

  if (args.size() == 1) {
    game::SetSkipHiScrore();
    game::SeeWholeMap();
    game::GetCurrentArea()->SendNewDrawRequest();
    SKIP_GET_KEY(KEY_CC_REDRAW);
  } else if (args.size() == 2) {
    if (args[1].EquCI("toggle")) {
      game::SetSkipHiScrore();
      game::SeeWholeMap();
      game::GetCurrentArea()->SendNewDrawRequest();
      SKIP_GET_KEY(KEY_CC_REDRAW);
    } else if (args[1].EquCI("normal")) {
      game::SetSkipHiScrore();
      game::SetSeeWholeMapCheatMode(SHOW_MAP_IN_UNIFORM_LIGHT);
      game::GetCurrentArea()->SendNewDrawRequest();
      SKIP_GET_KEY(KEY_CC_REDRAW);
    } else if (args[1].EquCI("no_light")) {
      game::SetSkipHiScrore();
      game::SetSeeWholeMapCheatMode(SHOW_MAP_IN_TRUE_LIGHT);
      game::GetCurrentArea()->SendNewDrawRequest();
      SKIP_GET_KEY(KEY_CC_REDRAW);
    } else if (args[1].EquCI("hide")) {
      game::SetSkipHiScrore();
      game::SetSeeWholeMapCheatMode(MAP_HIDDEN);
      game::GetCurrentArea()->SendNewDrawRequest();
      SKIP_GET_KEY(KEY_CC_REDRAW);
    } else {
      ConLogf("what does '%s' mean?", args[1].CStr());
    }
  } else {
    ConLogf("ERROR: what?");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// ConCmdNoClip
// ////////////////////////////////////////////////////////////////////////// //
class ConCmdNoClip : public ConInGameCmd {
public:
  ConCmdNoClip () : ConInGameCmd("NoClip") {}

  virtual void Do (const std::vector<festring> &args) override;
  virtual int AutocompleteLastArg (std::vector<festring> &args) override;
};


//==========================================================================
//
//  ConCmdNoClip::AutocompleteLastArg
//
//==========================================================================
int ConCmdNoClip::AutocompleteLastArg (std::vector<festring> &args) {
  CheckMode();
  if (args.size() == 2) {
    return ConCalculateAC(args[args.size() - 1], "toggle", "on", "off", NULL);
  }
  return 0;
}


//==========================================================================
//
//  ConCmdNoClip::Do
//
//==========================================================================
void ConCmdNoClip::Do (const std::vector<festring> &args) {
  CheckMode();

  if (args.size() == 2 && args[1] == "?") {
    ConLogf("usage: %s [toggle|on|off]", args[0].CStr());
    return;
  }

  if (args.size() == 1) {
    game::SetSkipHiScrore();
    game::GoThroughWalls();
  } else if (args.size() == 2) {
    if (args[1].EquCI("toggle")) {
      game::SetSkipHiScrore();
      game::GoThroughWalls();
    } else {
      bool val;
      if (ConParseBool(args[1], &val)) {
        game::SetSkipHiScrore();
        game::SetGoThroughWalls(val);
      } else {
        ConLogf("what does '%s' mean?", args[1].CStr());
        return;
      }
    }
  } else {
    ConLogf("ERROR: what?");
    return;
  }

  ConLogf("NoClip: %s", (game::GoThroughWallsCheatIsActive() ? "on" : "off"));
}
