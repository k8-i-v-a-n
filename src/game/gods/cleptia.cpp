#ifdef HEADER_PHASE
GOD(cleptia, god)
{
public:
  virtual cchar *GetName () const override;
  virtual cchar *GetDescription () const override;
  virtual int GetAlignment () const override;
  virtual int GetBasicAlignment () const override;
  virtual col16 GetColor () const override;
  virtual col16 GetEliteColor () const override;
  virtual int GetSex () const override;

protected:
  virtual void PrayGoodEffect () override;
  virtual void PrayBadEffect () override;
};


#else


int cleptia::GetSex () const { return FEMALE; }
cchar *cleptia::GetName () const { return "Cleptia"; }
cchar *cleptia::GetDescription () const { return "goddess of assassins and thieves"; }
int cleptia::GetAlignment () const { return ACP; }
int cleptia::GetBasicAlignment () const { return EVIL; }
col16 cleptia::GetColor () const { return CHAOS_BASIC_COLOR; }
col16 cleptia::GetEliteColor () const { return CHAOS_ELITE_COLOR; }


void cleptia::PrayGoodEffect () {
  PLAYER->RestoreStamina();
  if (PLAYER->StateIsActivated(HASTE) && PLAYER->StateIsActivated(INVISIBLE)) {
    ADD_MESSAGE("Cleptia helps you, but you really don't know how.");
    int StateToActivate = (RAND_2 ? HASTE : INVISIBLE);
    PLAYER->BeginTemporaryState(StateToActivate, 2500);
  } else if (!PLAYER->StateIsActivated(HASTE)) {
    ADD_MESSAGE("%s gives you the talent for speed.", GetName());
    PLAYER->BeginTemporaryState(HASTE, 2500);
  } else if (!PLAYER->StateIsActivated(INVISIBLE)) {
    ADD_MESSAGE("%s helps you to avoid your enemies by making you invisible.", GetName());
    PLAYER->BeginTemporaryState(INVISIBLE, 2500);
  }
}


void cleptia::PrayBadEffect () {
  ADD_MESSAGE("%s slows you down.", GetName());
  PLAYER->BeginTemporaryState(SLOW, 250);
}


#endif
