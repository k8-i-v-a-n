#ifdef HEADER_PHASE
GOD(seges, god)
{
public:
  virtual cchar *GetName () const override;
  virtual cchar *GetDescription () const override;
  virtual int GetAlignment () const override;
  virtual int GetBasicAlignment () const override;
  virtual col16 GetColor () const override;
  virtual col16 GetEliteColor () const override;
  virtual truth ForceGiveBodyPart () const override;
  virtual truth HealRegeneratingBodyParts () const override;
  virtual truth LikesMaterial (const materialdatabase *, ccharacter *) const override;
  virtual int GetSex () const override;

protected:
  virtual void PrayGoodEffect () override;
  virtual void PrayBadEffect () override;
};


#else


int seges::GetSex () const { return FEMALE; }
truth seges::ForceGiveBodyPart () const { return true; }
truth seges::HealRegeneratingBodyParts () const { return true; }
cchar *seges::GetName () const { return "Seges"; }
cchar *seges::GetDescription () const { return "goddess of health and nutrition"; }
int seges::GetAlignment () const { return AL; }
int seges::GetBasicAlignment () const { return GOOD; }
col16 seges::GetColor () const { return LAWFUL_BASIC_COLOR; }
col16 seges::GetEliteColor () const { return LAWFUL_ELITE_COLOR; }


void seges::PrayGoodEffect () {
  struct HealInfo {
    int state;
    const char *message;
  };

  const HealInfo healable[] = {
    { .state=POISONED, .message="%s removes the foul liquid in your veins."},
    { .state=LEPROSY, .message="%s cures your leprosy."},
    { .state=LYCANTHROPY, .message="%s cures your animalistic urges."},
    { .state=VAMPIRISM, .message="%s cures your bloodlust."},
    { .state=PARASITE_TAPE_WORM, .message="%s removes the evil hidden in your guts."},
    { .state=PARASITE_MIND_WORM, .message="%s removes the evil hidden in your brain."},
    { .state=0, .message=0},
  };


  if (PLAYER->IsInBadCondition()) {
    ADD_MESSAGE("%s cures your wounds.", GetName());
    PLAYER->RestoreLivingHP();
    return;
  }


  // choose random temporary state to heal
  int cc = 0, count = 0;
  while (healable[cc].state != 0) {
    if (PLAYER->TemporaryStateIsActivated(healable[cc].state)) {
      count += 1;
    }
    cc += 1;
  }

  if (count != 0) {
    // heal random state
    count = RAND_N(count);
    cc = 0;
    while (healable[cc].state) {
      if (PLAYER->TemporaryStateIsActivated(healable[cc].state)) {
        if (count == 0) {
          ADD_MESSAGE(healable[cc].message, GetName());
          PLAYER->DeActivateTemporaryState(healable[cc].state);
          return;
        }
        count -= 1;
      }
      cc += 1;
    }
  }


  if (PLAYER->GetNP() < SATIATED_LEVEL) {
    ADD_MESSAGE("Your stomach feels full again.");
    PLAYER->SetNP(BLOATED_LEVEL);
    return;
  }

  if (PLAYER->GetStamina() < PLAYER->GetMaxStamina() >> 1) {
    ADD_MESSAGE("You don't feel a bit tired anymore.");
    PLAYER->RestoreStamina();
    return;
  }
}


void seges::PrayBadEffect () {
  if (PLAYER->UsesNutrition()) {
    ADD_MESSAGE("You feel Seges altering the contents of your stomach in an eerie way.");
    PLAYER->EditNP(-10000);
    PLAYER->CheckStarvationDeath(CONST_S("starved by ") + GetName());
  } else {
    ADD_MESSAGE("Seges tries to alter the contents of your stomach, but fails.");
  }
}


truth seges::LikesMaterial (const materialdatabase *MDB, ccharacter *Char) const {
  return MDB->BodyFlags & IS_ALIVE && Char->GetTorso()->GetMainMaterial()->GetConfig() == MDB->Config;
}


#endif
