#ifdef HEADER_PHASE
GOD(silva, god)
{
public:
  virtual cchar *GetName () const override;
  virtual cchar *GetDescription () const override;
  virtual int GetAlignment () const override;
  virtual int GetBasicAlignment () const override;
  virtual col16 GetColor () const override;
  virtual col16 GetEliteColor () const override;
  virtual int GetSex () const override;

protected:
  virtual void PrayGoodEffect () override;
  virtual void PrayBadEffect () override;
};


#else


int silva::GetSex () const { return FEMALE; }
cchar *silva::GetName () const { return "Silva"; }
cchar *silva::GetDescription () const { return "goddess of nature"; }
int silva::GetAlignment () const { return ANP; }
int silva::GetBasicAlignment () const { return NEUTRAL; }
col16 silva::GetColor () const { return NEUTRAL_BASIC_COLOR; }
col16 silva::GetEliteColor () const { return NEUTRAL_ELITE_COLOR; }


void silva::PrayGoodEffect () {
  if (PLAYER->GetNP() < HUNGER_LEVEL) {
    ADD_MESSAGE("Your stomach feels full again.");
    PLAYER->SetNP(SATIATED_LEVEL);
  }
  if (!game::GetCurrentLevel()->IsOnGround()) {
    ADD_MESSAGE("Suddenly a horrible earthquake shakes the level.");
    game::GetCurrentLevel()->PerformEarthquake();
  } else {
    int TryToCreate = 1+RAND_N(7);
    int Created = 0;
    for (; Created < TryToCreate; ++Created) {
      wolf *Wolf = wolf::Spawn();
      v2 Pos = game::GetCurrentLevel()->GetNearestFreeSquare(Wolf, PLAYER->GetPos());
      if (Pos == ERROR_V2) { delete Wolf; break; }
      Wolf->SetTeam(PLAYER->GetTeam());
      Wolf->PutTo(Pos);
    }
    if (!Created && PLAYER->CanHear()) ADD_MESSAGE("You hear a sad howling of a wolf imprisoned in the earth.");
    if (Created == 1) ADD_MESSAGE("Suddenly a tame wolf materializes beside you.");
    if (Created > 1) ADD_MESSAGE("Suddenly some tame wolves materialize around you.");
  }
}


void silva::PrayBadEffect () {
  switch (RAND_N(3)) {
    case 0: PLAYER->Polymorph(largerat::Spawn(), 1000+RAND_N(1000)); break;
    case 1: PLAYER->Polymorph(ass::Spawn(), 1000+RAND_N(1000)); break;
    case 2: PLAYER->Polymorph(jackal::Spawn(), 1000+RAND_N(1000)); break;
  }
}


#endif
