#ifdef HEADER_PHASE
GOD(valpurus, god)
{
public:
  virtual cchar *GetName () const override;
  virtual cchar *GetDescription () const override;
  virtual int GetAlignment () const override;
  virtual void Pray () override;
  virtual int GetBasicAlignment () const override;
  virtual col16 GetColor () const override;
  virtual col16 GetEliteColor () const override;
  virtual int GetSex () const override;

protected:
  virtual void PrayGoodEffect () override;
  virtual void PrayBadEffect () override;
};


#else


int valpurus::GetSex () const { return MALE; }
cchar *valpurus::GetName () const { return "Valpurus"; }
cchar *valpurus::GetDescription () const { return "King of Gods"; }
int valpurus::GetAlignment () const { return ALPP; }
int valpurus::GetBasicAlignment () const { return GOOD; }
col16 valpurus::GetColor () const { return LAWFUL_BASIC_COLOR; }
col16 valpurus::GetEliteColor () const { return LAWFUL_ELITE_COLOR; }


void valpurus::PrayGoodEffect () {
  /*
  if (RAND_2) {
    shield *Shield = shield::Spawn();
    Shield->InitMaterials(MAKE_MATERIAL(VALPURIUM));
    PLAYER->GetGiftStack()->AddItem(Shield);
    ADD_MESSAGE("You hear booming voice: \"THIS WILL PROTECT THEE FROM MORTIFER, MY PALADIN!\" A shield glittering with holy might appears from nothing.");
  } else {
    meleeweapon *Weapon = meleeweapon::Spawn(TWO_HANDED_SWORD);
    Weapon->InitMaterials(MAKE_MATERIAL(VALPURIUM), MAKE_MATERIAL(VALPURIUM), true);
    PLAYER->GetGiftStack()->AddItem(Weapon);
    ADD_MESSAGE("You hear booming voice: \"DEFEAT MORTIFER WITH THIS, MY PALADIN!\" A sword glittering with holy might appears from nothing.");
  }
  */
  if (!game::PlayerIsGodChampion()) {
    ADD_MESSAGE("You hear a booming voice: \"I RECOGNIZETH THEE AS MINE OWN CHAMPION! "
                "JOURNEY FORTH WITH THESE ARMAMENTS TO DEFEAT MORTIFER AND ALL "
                "THE CHAOS HE HADST SOWN!\" A set of holy arms appear from nothing.");

    meleeweapon *Weapon = meleeweapon::Spawn(TWO_HANDED_SWORD);
    Weapon->InitMaterials(MAKE_MATERIAL(VALPURIUM), MAKE_MATERIAL(VALPURIUM), true);
    PLAYER->GetGiftStack()->AddItem(Weapon);

    shield *Shield = shield::Spawn();
    Shield->InitMaterials(MAKE_MATERIAL(VALPURIUM));
    PLAYER->GetGiftStack()->AddItem(Shield);

    game::MakePlayerGodChampion();
  } else {
    // Player already received championship gift, give holy handgrenade instead.
    ADD_MESSAGE("You hear a booming voice: \"I GRANT THEE THIS HOLY HAND GRENADE "
                "THAT WITH IT THOU MAYEST BLOW THY ENEMIES TO TINY BITS, MY PALADIN!\"");
    PLAYER->GetGiftStack()->AddItem(holyhandgrenade::Spawn());
  }
}


void valpurus::PrayBadEffect () {
  ADD_MESSAGE("Valpurus smites you with a small hammer.");
  PLAYER->ReceiveDamage(0, 10, PHYSICAL_DAMAGE, HEAD, RAND_8);
  PLAYER->CheckDeath(CONST_S("faced the hammer of Justice from the hand of ") + GetName(), 0);
}


void valpurus::Pray () {
  LastPray = 0;
  if (!Timer && Relation == 1000) {
    ADD_MESSAGE("You feel %s is very pleased.", GetName());
    PrayGoodEffect();
    AdjustTimer(100000);
    AdjustRelation(-500);
    game::ApplyDivineAlignmentBonuses(this, 100, true);
    PLAYER->EditExperience(WISDOM, 400, 1 << 11);
    if (Relation > 250 && !RAND_N(2)) {
      character *Angel = CreateAngel(PLAYER->GetTeam());
      if (Angel) {
        ADD_MESSAGE("%s seems to be very friendly towards you.",
                    Angel->CHAR_DESCRIPTION(DEFINITE));
      }
    }
  } else if (Relation < 0 || (!TryToAttachBodyPart(PLAYER) && !TryToHardenBodyPart(PLAYER))) {
    ADD_MESSAGE("You feel you are not yet worthy enough for %s.", GetName());
    PrayBadEffect();
    AdjustTimer(50000);
    AdjustRelation(-100);
    game::ApplyDivineAlignmentBonuses(this, 20, false);
    PLAYER->EditExperience(WISDOM, -50, 1 << 10);
    if (Relation < -250 && !RAND_N(3)) {
      character *Angel = CreateAngel(game::GetTeam(VALPURUS_ANGEL_TEAM), 10000);
      if (Angel) {
        ADD_MESSAGE("%s seems to be hostile.", Angel->CHAR_DESCRIPTION(DEFINITE));
      }
    }
  }
}


#endif
