/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "iconf.h"
#include "game.h"
#include "fesave.h"
#include "feparse.h"
#include "feio.h"
#include "area.h"
#include "graphics.h"
#include "bitmap.h"
#include "igraph.h"
#include "felist.h"

#include "message.h"
#include "command.h"


//WARNING! adding more options will overflow the options page!

int ivanconfig::currWinWidth = 0;
int ivanconfig::currWinHeight = 0;

stringoption ivanconfig::DefaultName("DefaultName", "player's default name", CONST_S(""), &configsystem::NormalStringDisplayer, &DefaultNameChangeInterface);
stringoption ivanconfig::DefaultPetName("DefaultPetName", "starting pet's default name", CONST_S("Kenny"), &configsystem::NormalStringDisplayer, &DefaultPetNameChangeInterface);
truthoption ivanconfig::StartWithNoPet("StartWithNoPet", "start without a pet", false);
numberoption ivanconfig::AutoSaveInterval("AutoSaveInterval", "autosave interval", 300, &AutoSaveIntervalDisplayer, &AutoSaveIntervalChangeInterface, &AutoSaveIntervalChanger);
scrollbaroption ivanconfig::Contrast("Contrast", "contrast", 100, &ContrastDisplayer, &ContrastChangeInterface, &ContrastChanger, &ContrastHandler);
truthoption ivanconfig::WarnAboutDanger("WarnAboutVeryDangerousMonsters", "warn about very dangerous monsters", true);
truthoption ivanconfig::AttackIndicator("AttackIndicator", "show attack indicator", true);
numberoption ivanconfig::AttackIndicatorDelay("AttackIndicatorDelay", "attack indicator delay", 100, &AttackFlashDelayDisplayer, &AttackFlashDelayChangeInterface, &AttackFlashDelayChanger);
truthoption ivanconfig::AutoDropLeftOvers("AutoDropLeftOvers", "drop food leftovers automatically", true);
truthoption ivanconfig::AutoDropBottles("AutoDropBottles", "automatically drop empty bottles after drinking", true);
truthoption ivanconfig::AutoDropCans("AutoDropCans", "automatically drop empty cans after eating/drinking", false);
truthoption ivanconfig::LookZoom("LookZoom", "zoom feature in look mode", true);
//truthoption ivanconfig::UseAlternativeKeys("UseAlternativeKeys", "use alternative direction keys", false);
truthoption ivanconfig::BeNice("BeNice", "be nice to pets", true);
truthoption ivanconfig::FullScreenMode("FullScreenMode", "run the game in full screen mode", false, &configsystem::NormalTruthDisplayer, &configsystem::NormalTruthChangeInterface, &FullScreenModeChanger);
/*k8*/
scrollbaroption ivanconfig::DoubleResModifier("DoubleResModifier", "resolution multiplier (1+n/10)", 10, &ResModDisp, &ResModChangeIntf, &ResModChanger, &ResModHandler);
truthoption ivanconfig::BlurryScale("BlurryScale", "use filtered scaling", true, &configsystem::NormalTruthDisplayer, &configsystem::NormalTruthChangeInterface, &BlurryChanger);
truthoption ivanconfig::OversampleBlurry("OversampleBlurry", "use interpolation for oversampling", true, &configsystem::NormalTruthDisplayer, &configsystem::NormalTruthChangeInterface, &BlurryChanger);
truthoption ivanconfig::StatusOnLeft("StatusOnLeft", "!draw status panel on the left side", false); //hidden!
truthoption ivanconfig::StatusOnBottom("StatusOnBottom", "!position status panel slightly lower", false); //hidden!
truthoption ivanconfig::KickDownDoors("KickDownDoors", "kick closed doors", true);
truthoption ivanconfig::AutoCenterMap("AutoCenterMap", "automatically center map when player moves", true);
truthoption ivanconfig::AutoCenterMapOnLook("AutoCenterMapOnLook", "automatically center map when player looks", true);
truthoption ivanconfig::FastListMode("FastLists", "instantly select list items with alpha keys", false, &configsystem::NormalTruthDisplayer, &configsystem::NormalTruthChangeInterface, &FastListChanger);
truthoption ivanconfig::PlaySounds("PlaySounds", "use sounds", true);
scrollbaroption ivanconfig::SoundVolume("SoundVolume", "sound volume", 128, &SoundVolumeDisplayer, &SoundVolumeChangeInterface, &SoundChanger, &SoundVolumeHandler);
truthoption ivanconfig::ConfirmCorpses("ConfirmCorpses", "confirm corpse pickup", true);
numberoption ivanconfig::GoStopOnKind("GoStopOnKind", "abort \"Go\" when near", 0, &GoStopDisplayer, &GoStopChangeInterface, &GoStopChanger);
truthoption ivanconfig::ConfirmScrollReading("ConfirmScrollReading", "confirm scroll reading", true);
numberoption ivanconfig::GoingDelay("GoingDelay", "delay betwen steps in 'go' command", 80, &GoingDelayDisplayer, &GoingDelayChangeInterface, &GoingDelayChanger);
scrollbaroption ivanconfig::CompressionLevel("CompressionLevel", "save compression level", 2, &CompLevelDisplayer, &CompLevelChangeInterface, &CompLevelChanger, &CompLevelHandler);
truthoption ivanconfig::ShowFullItemDesc("ShowFullItemDesc", "show detailed description of lying item", true);
truthoption ivanconfig::SaveQuitScore("SaveQuitScore", "save \"cowardly quit\" to hiscore table", false);

truthoption ivanconfig::ShorterListsX("ShorterListsX", "!make lists horizontally shorter (you don't need this)", false);
truthoption ivanconfig::SkipIntro("SkipIntro", "skip new game intro text", false);

truthoption ivanconfig::OutlineItems("OutlineItems", "draw outlines around items", true);
truthoption ivanconfig::OutlineMonsters("OutlineMonsters", "!draw outlines around monsters", false);

truthoption ivanconfig::ShowWeaponCategory("ShowWeaponCategory", "show weapon caterory", true);
numberoption ivanconfig::ShowItemPopups("ShowItemPopups", "show helper item popups", 3, &ItemPopupDisplayer, &ItemPopupChangeInterface, &ItemPopupChanger);

numberoption ivanconfig::OrigWinWidth("WinWidth", "window width", 800);
numberoption ivanconfig::OrigWinHeight("WinHeight", "window height", 600);

numberoption ivanconfig::SaveLogToDB("SaveLogToDB", "save log messages to the database", 0, &SaveLogDisplayer, &SaveLogChangeInterface, &SaveLogChanger);

truthoption ivanconfig::EscQuits("EscQuitsFromMainMenu", "\1YEsc\2 in main menu quits", true);

truthoption ivanconfig::BlurFOW("BluredFogOfWar", "Use blur for fog of war", true);


void ivanconfig::SoundVolumeDisplayer (const numberoption *O, festring &Entry) {
  Entry << O->Value << "/128";
}


truth ivanconfig::SoundVolumeChangeInterface (numberoption *O) {
  iosystem::ScrollBarQuestion(CONST_S("Set new sound volume (0-128, '<' and '>' move the slider):"),
    GetQuestionPos(), O->Value, 16, 0, 128, O->Value, WHITE, LIGHT_GRAY, DARK_GRAY,
    KEY_LEFT, //game::GetMoveCommandKey(KEY_LEFT_INDEX),
    KEY_RIGHT, //game::GetMoveCommandKey(KEY_RIGHT_INDEX),
    !game::IsRunning(),
    static_cast<scrollbaroption*>(O)->BarHandler);
  if (game::IsRunning()) igraph::BlitBackGround(v2(16, 6), v2(game::GetScreenXSize() << 4, 23));
  return false; // re-fade
}


void ivanconfig::SoundVolumeChanger (numberoption *O, sLong What) {
  if (What < 0) What = 0;
  if (What > 128) What = 128;
  O->Value = What;
  soundsystem::SetVolume(What);
}


void ivanconfig::SoundVolumeHandler (sLong Value) {
  SoundVolumeChanger(&SoundVolume, Value);
}
/*k8*/


col24 ivanconfig::ContrastLuminance = NORMAL_LUMINANCE;
v2 ivanconfig::GetQuestionPos () { return game::IsRunning() ? v2(16, 6) : v2(30, 30); }
void ivanconfig::BackGroundDrawer () { game::DrawEverythingNoBlit(); }


void ivanconfig::AutoSaveIntervalDisplayer (const numberoption *O, festring &Entry) {
  if (O->Value) {
    Entry << O->Value << " turn";
    if (O->Value != 1) Entry << 's';
  } else {
    Entry << "disabled";
  }
}


void ivanconfig::ResModDisp (const numberoption *O, festring &Entry) {
  Entry << O->Value << "/10";
}

truth ivanconfig::ResModChangeIntf (numberoption *O) {
  iosystem::ScrollBarQuestion(CONST_S("Set new multiplier value (0-10, '<' and '>' move the slider):"),
    GetQuestionPos(), O->Value, 1, 0, 10, O->Value, WHITE, LIGHT_GRAY, DARK_GRAY,
    KEY_LEFT, //game::GetMoveCommandKey(KEY_LEFT_INDEX),
    KEY_RIGHT, //game::GetMoveCommandKey(KEY_RIGHT_INDEX),
    !game::IsRunning(),
    static_cast<scrollbaroption*>(O)->BarHandler);
  return false; // re-fade
}

void ivanconfig::ResModChanger (numberoption *O, sLong What) {
  if (What < 0) What = 0;
  if (What > 10) What = 10;
  O->Value = What;
  graphics::ReinitMode(What);
}

void ivanconfig::ResModHandler (sLong Value) {
  ResModChanger(&DoubleResModifier, Value);
  if (game::IsRunning()) {
    game::GetCurrentArea()->SendNewDrawRequest();
    game::DrawEverythingNoBlit();
  }
}


void ivanconfig::ContrastDisplayer (const numberoption *O, festring &Entry) {
  Entry << O->Value << "/100";
}


truth ivanconfig::DefaultNameChangeInterface (stringoption *O) {
  festring String;
  if (iosystem::StringQuestion(String, CONST_S("Set new default name (1-20 letters):"), GetQuestionPos(), WHITE, 0, 20, !game::IsRunning(), true) == NORMAL_EXIT)
    O->ChangeValue(String);
  if (game::IsRunning()) igraph::BlitBackGround(v2(16, 6), v2(game::GetScreenXSize() << 4, 23));
  return false; // re-fade
}


truth ivanconfig::DefaultPetNameChangeInterface (stringoption *O) {
  festring String;
  if (iosystem::StringQuestion(String, CONST_S("Set new default name for the starting pet (1-20 letters):"), GetQuestionPos(), WHITE, 0, 20, !game::IsRunning(), true) == NORMAL_EXIT)
    O->ChangeValue(String);
  if (game::IsRunning()) igraph::BlitBackGround(v2(16, 6), v2(game::GetScreenXSize() << 4, 23));
  return false; // re-fade
}


truth ivanconfig::AutoSaveIntervalChangeInterface (numberoption *O) {
  truth escaped;
  sLong val = iosystem::NumberQuestion(CONST_S("Set new autosave interval (1-50000 turns, 0 for never):"),
                  GetQuestionPos(), WHITE, (!game::IsRunning() ? iosystem::FADE_IN : iosystem::DEFAULT),
                  &escaped);
  if (!escaped) O->ChangeValue(val);
  if (game::IsRunning()) igraph::BlitBackGround(v2(16, 6), v2(game::GetScreenXSize() << 4, 23));
  return false; // re-fade
}


truth ivanconfig::ContrastChangeInterface (numberoption *O) {
  iosystem::ScrollBarQuestion(CONST_S("Set new contrast value (0-200, '<' and '>' move the slider):"),
    GetQuestionPos(), O->Value, 5, 0, 200, O->Value, WHITE, LIGHT_GRAY, DARK_GRAY,
    KEY_LEFT, //game::GetMoveCommandKey(KEY_LEFT_INDEX),
    KEY_RIGHT, //game::GetMoveCommandKey(KEY_RIGHT_INDEX),
    !game::IsRunning(),
    static_cast<scrollbaroption*>(O)->BarHandler);
  if (game::IsRunning()) igraph::BlitBackGround(v2(16, 6), v2(game::GetScreenXSize() << 4, 23));
  return false; // re-fade
}


void ivanconfig::AutoSaveIntervalChanger (numberoption *O, sLong What) {
  if (What < 0) What = 0;
  if (What > 50000) What = 50000;
  O->Value = What;
}


void ivanconfig::ContrastChanger (numberoption *O, sLong What) {
  if (What < 0) What = 0;
  if (What > 200) What = 200;
  O->Value = What;
  CalculateContrastLuminance();
}


void ivanconfig::SoundChanger (numberoption *O, sLong What) {
  if (What < 0) What = 0;
  if (What > 128) What = 128;
  O->Value = What;
}


void ivanconfig::FullScreenModeChanger (truthoption *, truth) {
  graphics::SwitchMode();
}


void ivanconfig::BlurryChanger (truthoption *g, truth value) {
  g->Value = value;
  graphics::ReinitBlurry(BlurryScale.Value, OversampleBlurry.Value);
}


void ivanconfig::FastListChanger (truthoption *O, truth value) {
  //ConLogf("FLC: %s", value ? "TAN" : "ona");
  FastListMode.Value = !FastListMode.Value;
  felist::SetFastListMode(!FastListMode.Value);
}


void ivanconfig::Show () {
  configsystem::Show(&BackGroundDrawer, &game::SetStandardListAttributes, game::IsRunning());
}


void ivanconfig::ContrastHandler (sLong Value) {
  ContrastChanger(&Contrast, Value);
  if (game::IsRunning()) {
    game::GetCurrentArea()->SendNewDrawRequest();
    game::DrawEverythingNoBlit();
  }
}


void ivanconfig::SwitchModeHandler () {
  FullScreenMode.Value = !FullScreenMode.Value;
  Save();
}


void ivanconfig::CalculateContrastLuminance () {
  int Element = Min<sLong>((GetContrast() << 7) / 100, 255);
  ContrastLuminance = MakeRGB24(Element, Element, Element);
}


// ////////////////////////////////////////////////////////////////////////// //
void ivanconfig::GoingDelayDisplayer (const numberoption *O, festring &Entry) {
  if (O->Value) {
    Entry << O->Value << " ms";
  } else {
    Entry << "disabled";
  }
}


truth ivanconfig::GoingDelayChangeInterface (numberoption *O) {
  truth escaped;
  sLong val = iosystem::NumberQuestion(CONST_S("Set new 'go' delay (0-2000 msecs):"),
                  GetQuestionPos(), WHITE, (!game::IsRunning() ? iosystem::FADE_IN : iosystem::DEFAULT),
                  &escaped);
  if (!escaped) O->ChangeValue(val);
  if (game::IsRunning()) igraph::BlitBackGround(v2(16, 6), v2(game::GetScreenXSize() << 4, 23));
  return false; // re-fade
}


void ivanconfig::GoingDelayChanger (numberoption *O, sLong What) {
  if (What < 0) What = 0;
  if (What > 2000) What = 2000;
  O->Value = What;
}


// ////////////////////////////////////////////////////////////////////////// //
void ivanconfig::AttackFlashDelayDisplayer (const numberoption *O, festring &Entry) {
  if (O->Value) {
    Entry << O->Value << " ms";
  } else {
    Entry << "disabled";
  }
}


truth ivanconfig::AttackFlashDelayChangeInterface (numberoption *O) {
  truth escaped;
  sLong val = iosystem::NumberQuestion(CONST_S("Set new attack flash delay (10-1000 msecs):"),
                  GetQuestionPos(), WHITE, (!game::IsRunning() ? iosystem::FADE_IN : iosystem::DEFAULT),
                  &escaped);
  if (!escaped) O->ChangeValue(val);
  if (game::IsRunning()) igraph::BlitBackGround(v2(16, 6), v2(game::GetScreenXSize() << 4, 23));
  return false; // re-fade
}


void ivanconfig::AttackFlashDelayChanger (numberoption *O, sLong What) {
  if (What < 10) What = 10;
  if (What > 1000) What = 1000;
  O->Value = What;
}


// ////////////////////////////////////////////////////////////////////////// //
void ivanconfig::ItemPopupDisplayer (const numberoption *O, festring &Entry) {
  switch (O->Value) {
    case 0: Entry << "none"; break;
    case 1: Entry << "gear comparison"; break;
    case 2: Entry << "other items list"; break;
    case 3: Entry << "gear & items"; break;
    default: Entry << "wtf"; break;
  }
}


truth ivanconfig::ItemPopupChangeInterface (numberoption *O) {
  //O->ChangeValue(iosystem::NumberQuestion(CONST_S("Set new 'go' delay (0-2000):"), GetQuestionPos(), WHITE, !game::IsRunning()));
  //if (game::IsRunning()) igraph::BlitBackGround(v2(16, 6), v2(game::GetScreenXSize() << 4, 23));
  //return false;

  felist List(CONST_S("What item popups do you want to see?"));
  //List.SetPos(v2(300, 250));
  //List.SetWidth(200);
  List.RemoveFlags(FADE);
  //List.AddFlags(DRAW_BACKGROUND_AFTERWARDS);

  List.AddEntry(CONST_S("none"), (O->Value == 0 ? WHITE : LIGHT_GRAY));
  List.AddEntry(CONST_S("gear comparison"), (O->Value == 1 ? WHITE : LIGHT_GRAY));
  List.AddEntry(CONST_S("other items list"), (O->Value == 2 ? WHITE : LIGHT_GRAY));
  List.AddEntry(CONST_S("gear & items"), (O->Value == 3 ? WHITE : LIGHT_GRAY));
  List.SetSelected(O->Value);

  game::SetStandardListAttributes(List);
  List.AddFlags(SELECTABLE);

  int Selected;
  while ((Selected = List.Draw()) & FELIST_ERROR_BIT) {
    if (Selected == ESCAPED) return true;
  }

  O->Value = Clamp(Selected, 0, 3);

  // do not re-fade
  return true;
}


void ivanconfig::ItemPopupChanger (numberoption *O, sLong What) {
  What = Clamp(What, 0, 3);
  O->Value = What;
}


// ////////////////////////////////////////////////////////////////////////// //
void ivanconfig::SaveLogDisplayer (const numberoption *O, festring &Entry) {
  switch (O->Value) {
    case 0: Entry << "none"; break;
    case 1: Entry << "fast"; break;
    case 2: Entry << "safe"; break;
    default: Entry << "wtf"; break;
  }
}


truth ivanconfig::SaveLogChangeInterface (numberoption *O) {
  felist List(CONST_S("What log saving mode do you prefer?"));
  //List.SetPos(v2(300, 250));
  //List.SetWidth(200);
  List.RemoveFlags(FADE);
  //List.AddFlags(DRAW_BACKGROUND_AFTERWARDS);

  List.AddEntry(CONST_S("none"), (O->Value == 0 ? WHITE : LIGHT_GRAY));
  List.AddEntry(CONST_S("fast"), (O->Value == 1 ? WHITE : LIGHT_GRAY));
  List.AddEntry(CONST_S("safe"), (O->Value == 2 ? WHITE : LIGHT_GRAY));
  List.SetSelected(O->Value);

  game::SetStandardListAttributes(List);
  List.AddFlags(SELECTABLE);

  int Selected;
  while ((Selected = List.Draw()) & FELIST_ERROR_BIT) {
    if (Selected == ESCAPED) return true;
  }

  O->Value = Clamp(Selected, 0, 2);

  // do not re-fade
  return true;
}


void ivanconfig::SaveLogChanger (numberoption *O, sLong What) {
  What = Clamp(What, 0, 2);
  O->Value = What;
}


// ////////////////////////////////////////////////////////////////////////// //
static festring BuildGoString (int v) {
  festring res;
  v &= 7;
  if (v == 0) {
    res << "none seen";
  } else {
    if (v&0x01) res << "seen corpses";
    if (v&0x02) {
      if (!res.IsEmpty()) res << ", "; else res << "seen ";
      res << "items";
    }
    if (v&0x04) {
      if (!res.IsEmpty()) res << ", "; else res << "seen ";
      res << "doors";
    }
  }
  return res;
}


void ivanconfig::GoStopDisplayer (const numberoption *O, festring &Entry) {
  Entry << BuildGoString(O->Value);
}


truth ivanconfig::GoStopChangeInterface (numberoption *O) {
  felist List(CONST_S("Stop automatic \"Go\" on what?"));
  //List.SetPos(v2(300, 250));
  //List.SetWidth(200);
  List.RemoveFlags(FADE);
  //List.AddFlags(DRAW_BACKGROUND_AFTERWARDS);

  for (int f = 0; f <= 7; f += 1) {
    festring ee = BuildGoString(f);
    List.AddEntry(ee, (O->Value == f ? WHITE : LIGHT_GRAY));
  }
  List.SetSelected(O->Value);

  game::SetStandardListAttributes(List);
  List.AddFlags(SELECTABLE);

  int Selected;
  while ((Selected = List.Draw()) & FELIST_ERROR_BIT) {
    if (Selected == ESCAPED) return true;
  }

  O->Value = Clamp(Selected, 0, 7);

  // do not re-fade
  return true;
}


void ivanconfig::GoStopChanger (numberoption *O, sLong What) {
  What = Clamp(What, 0, 7);
  O->Value = What;
}


// ////////////////////////////////////////////////////////////////////////// //
void ivanconfig::CompLevelDisplayer (const numberoption *O, festring &Entry) {
       if (O->Value <= 0) Entry << "\1Rno compression\2";
  else if (O->Value == 1) Entry << "\1Yfast (why?!)\2";
  else if (O->Value == 2) Entry << "normal";
  else /*if (O->Value >= 3)*/ Entry << "\1Rbest (why?!)\2";
}

truth ivanconfig::CompLevelChangeInterface (numberoption *O) {
  /*
  iosystem::ScrollBarQuestion(CONST_S("Set new compression level (0-9, '<' and '>' move the slider):"),
                              GetQuestionPos(), O->Value, 1, 0, 9, O->Value,
                              WHITE, LIGHT_GRAY, DARK_GRAY,
                              KEY_LEFT, //game::GetMoveCommandKey(KEY_LEFT_INDEX),
                              KEY_RIGHT, //game::GetMoveCommandKey(KEY_RIGHT_INDEX),
                              !game::IsRunning(),
                              static_cast<scrollbaroption*>(O)->BarHandler);
  #if 0
  ConLogf("val=%d", O->Value);
  #endif
  */

  felist List(CONST_S("Select compression level:"));
  //List.SetPos(v2(300, 250));
  //List.SetWidth(200);
  List.RemoveFlags(FADE);
  //List.AddFlags(DRAW_BACKGROUND_AFTERWARDS);

  // do not allow to select "no compression", it is the worst possible case.
  // it uses A LOT of memory, and not really faster then other methods.
  //List.AddEntry(CONST_S("no compression"), (O->Value <= 0 ? WHITE : LIGHT_GRAY));
  List.AddEntry(CONST_S("fast"), (O->Value <= 1 ? WHITE : LIGHT_GRAY));
  List.AddEntry(CONST_S("normal"), (O->Value == 2 ? WHITE : LIGHT_GRAY));
  List.AddEntry(CONST_S("\1Rbest\2"), (O->Value >= 3 ? WHITE : LIGHT_GRAY));
  List.SetSelected(Clamp(O->Value - 1, 0, 2));

  game::SetStandardListAttributes(List);
  List.AddFlags(SELECTABLE);

  int Selected;
  while ((Selected = List.Draw()) & FELIST_ERROR_BIT) {
    if (Selected == ESCAPED) return true;
  }

  O->Value = Clamp(Selected + 1, 1, 3);

  return true;
}

void ivanconfig::CompLevelChanger (numberoption *O, sLong What) {
  if (What == 0 || What > 3) What = 2;
  O->Value = Clamp(What, 1, 3);
}

void ivanconfig::CompLevelHandler (sLong Value) {
  CompLevelChanger(&CompressionLevel, Value);
}


// ////////////////////////////////////////////////////////////////////////// //
int ivanconfig::WinWidth () {
  if (!currWinWidth) {
    currWinWidth = Clamp(GetWinWidth(), 640, 1280);
  }
  return currWinWidth;
}


int ivanconfig::WinHeight () {
  if (!currWinHeight) {
    currWinHeight = Clamp(GetWinHeight(), 480, 1280);
  }
  return currWinHeight;
}


// ////////////////////////////////////////////////////////////////////////// //
festring ivanconfig::GetMyDir (void) { return inputfile::GetMyDir(); }


void ivanconfig::Initialize () {
  configsystem::AddOption(&DefaultName, CONST_S(
    "Pick a character name for all your future games. "
    "When left empty, the game will ask you to pick a name whenever you start a new game. "
    "If you won't type a new name, the game will generate a random one for you."));
  configsystem::AddOption(&DefaultPetName, CONST_S(
    "Choose a name for your puppy."));
  configsystem::AddOption(&StartWithNoPet, CONST_S(
    "You may want to travel alone, leaving your puppy safe. This might look "
    "like a good choice, but the puppy can help you fighting some annoying creatures!"));

  configsystem::AddOption(&PlaySounds, CONST_S(
    "Play sounds based on log messages?"));
  configsystem::AddOption(&SoundVolume, CONST_S(
    "Select sound volume."));

  configsystem::AddOption(&AutoSaveInterval, CONST_S(
    "Automatically saves your game after certain amount of turns.\n\n"
    "Any game has bugs, and I.V.A.N. is not an exception. Autosaving might "
    "save you from exploring the dungeon again in case of crash. Note that "
    "the game is automatically saved when you move to another dungeon level, "
    "or enter/exit the overworld."));

  configsystem::AddOption(&WarnAboutDanger, CONST_S(
    "The game can display a warning prompt when you encounter an unusually "
    "dangerous monster.\nIt is better to leave this enabled."));

  configsystem::AddOption(&AttackIndicator, CONST_S(
    "Flash fighting monster (and PC, if PC is attacked). Fights look more fun this way.\n\n"
    "This is purely visual effect, it doesn't affect gameplay."));
  configsystem::AddOption(&AttackIndicatorDelay, CONST_S(
    "Flash attack indicator delay, in milliseconds."));

  configsystem::AddOption(&AutoDropLeftOvers, CONST_S(
    "Automatically drop food leftovers (like banana peels) after eating."));
  configsystem::AddOption(&AutoDropBottles, CONST_S(
    "Automatically drop empty bottles after drinking.\n\n"
    "Note that there are certain ways to get a free drink with an empty bottle, "
    "so choose carefullly."));
  configsystem::AddOption(&AutoDropCans, CONST_S(
    "Automatically drop empty cans after consuming.\n\n"
    "Note that there are certain ways to get a free fool with an empty can, "
    "so choose carefullly. Also, you can use empty cans to hold food and liquids."));

  configsystem::AddOption(&KickDownDoors, CONST_S(
    "Automatically kick closed doors if they refuse to open? Note that the game "
    "will ask you again if you are going to kick a closed shop door or something "
    "like that."));
  configsystem::AddOption(&BeNice, CONST_S(
    "Don't let your sadistic tendencies hurt your pets.\n\n"
    "If you have a masochistic pet, and try to attack it with sadist weapon, the game "
    "will ask for the confirmation. Note that such attack will not turn the pet hostile."));

  configsystem::AddOption(&ConfirmCorpses, CONST_S(
    "Confirm picking up corpses. Corpses are mostly useless, and you "
    "usually picking them up only accidentally."));
  configsystem::AddOption(&ConfirmScrollReading, CONST_S(
    "Confirm reading scrolls. Sometimes you may accidentally select a scroll "
    "instead of a book."));

  configsystem::AddOption(&GoStopOnKind, CONST_S(
    "Abort automatic \"Go\" command on... Someday I'll write a better description here."));
  configsystem::AddOption(&GoingDelay, CONST_S(
    "Animation delay for \"Go\" command."));

  configsystem::AddOption(&ShowFullItemDesc, CONST_S(
    "Show full description for the item on the floor in log window (i.e. include stats)."));
  configsystem::AddOption(&ShowWeaponCategory, CONST_S(
    "Show weapon category in weapon description.\n\n"
    "This info can be obtained by wielding a wepon and looking at weapon stats, "
    "so it is just a convenience feature, not a cheat."));
  configsystem::AddOption(&ShowItemPopups, CONST_S(
    "The game can show helper popups on entering the square, listing items there.\n"
    "\1Gnone\2 means \"no popups\".\n"
    "\1Ggear comparison\2 will show only wearable gear (equipped and lying).\n"
    "\1Gitem list\2 show non-wearable items list popup only.\n"
    "\1Ggears & items\2 show gear comparison, and other items in separate popup.\n"
    "\n"
    "Sorry for the stupid names and explanations. If in doubt, just use the last choice."
    ));

  configsystem::AddOption(&SaveQuitScore, CONST_S(
    "Save your score if you 'Q'uit the game?\n\n"
    "Actually, all gameovers are saved, but it is usually not fun to see highscore "
    "table filled with \"...cowardly quit...\" entries. Don't worry, no history will "
    "be lost in any case."));

  configsystem::AddOption(&LookZoom, CONST_S(
    "Display zoomed tile in \"Look\" mode?\n\n"
    "Sometimes it is funny to look at the magnified pixels of the tile. Why not?"));
  configsystem::AddOption(&AutoCenterMap, CONST_S(
    "Keep gameplay field centered on the player?\n\n"
    "Keep the player in the center of the view area. This way you will have better "
    "view of your surroundings. Do not forget that there are two kinds of minimaps too, "
    "so you can view the whole level by using a minimap. You can also use minimap to "
    "mark interesting places. Press \1GF1\2 while in minimap mode to learn more."));
  configsystem::AddOption(&AutoCenterMapOnLook, CONST_S(
    "Keep gameplay field centered in \"Look\" mode?"));

  configsystem::AddOption(&StatusOnLeft, CONST_S(
    "Show statusbar on the left?"));
  configsystem::AddOption(&StatusOnBottom, CONST_S(
    "Move status panel slightly lower?"));

  configsystem::AddOption(&OutlineItems, CONST_S(
    "Draw dark outlines around items? This way items are better visible.\n\n"
    "\1GPlease, note that this option may cause only partial outlines until you start a new game.\2"));
  configsystem::AddOption(&OutlineMonsters, CONST_S(
    "Draw dark outlines around monsters?\nThis way monsters are better visible.\n\n"
    "\1RTHIS OPTION DOESN'T WORK YET!\2"));

  configsystem::AddOption(&FastListMode, CONST_S(
    "If not set, first \"quick key\" press will move the cursor, and second will select the item."));

  configsystem::AddOption(&Contrast, CONST_S(
    "Change image contrast. Run the game and play with this to see."));
  configsystem::AddOption(&FullScreenMode, CONST_S(
    "Run the game in fullscreen mode with letterboxing?"));
  configsystem::AddOption(&DoubleResModifier, CONST_S(
    "Game window scale factor."));
  configsystem::AddOption(&BlurryScale, CONST_S(
    "Scaling option. It needs a better description."));
  configsystem::AddOption(&OversampleBlurry, CONST_S(
    "Scaling option. It needs a better description."));

  configsystem::AddOption(&BlurFOW, CONST_S(
    "Use blurred images for fog of war instead of simple checkered pattern.\n\n"
    "This may or may not look better than the standard pattern. Try for yourself."));

  configsystem::AddOption(&OrigWinWidth, CONST_S(
    "Set internal game rendering width.\n"
    "\1WNote that this doesn't set the size of the OS window: use scaling option for that!\2\n\n"
    "Internally the game is rendered in smaller resoultion, and then scaled. "
    "Default is 800x600."));
  configsystem::AddOption(&OrigWinHeight, CONST_S(
    "Set internal game rendering width.\n"
    "\1WNote that this doesn't set the size of the OS window: use scaling option for that!\2\n\n"
    "Internally the game is rendered in smaller resoultion, and then scaled. "
    "Default is 800x600."));

  configsystem::AddOption(&SkipIntro, CONST_S(
    "As you will die quite often (believe me!), you might want to skip intro text."));

  configsystem::AddOption(&CompressionLevel, CONST_S(
    "Set compression level for saved games. The higher the level, the smaller "
    "saved game files are.\n\n"
    "Use \1Gnormal\2 level, this gives you great compression and good speed.\n"
    "Note that uncompressed saves are not much faster, but require almost 10 times more RAM!"));
  configsystem::AddOption(&SaveLogToDB, CONST_S(
    "Save all log messages to SQLite database.\n"
    "\"Fast\" mode does less disk writes, but log db will be corrupted if the game crashes.\n"
    "\"Safe\" mode is slower and does more writes, but db will survive crashes."));
  configsystem::AddOption(&ShorterListsX, CONST_S(
    "You don't need this."));
  configsystem::AddOption(&EscQuits, CONST_S(
    "Pressing \1GEsc\2 in main menu immediately quits."));

  DoubleResModifier.Value = 0;
  configsystem::SetConfigFileName(game::GetConfigPath() + "k8ivan.rc");
  configsystem::Load();
  felist::SetFastListMode(!FastListMode.Value);
  CalculateContrastLuminance();

  commandsystem::ConfigureKeys();
  commandsystem::SaveKeys(false);
}
