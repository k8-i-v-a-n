/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __ICONF_H__
#define __ICONF_H__

#include "config.h"
#include "v2.h"


/*
#if 0
#define WIN_WIDTH  (800 + 128)
#define WIN_HEIGHT (600 + 128)
#endif

#if 1
#define WIN_WIDTH  (700)
#define WIN_HEIGHT (568)
#endif
*/

class ivanconfig {
public:
  static cfestring &GetDefaultName () { return DefaultName.Value; }
  static cfestring &GetDefaultPetName () { return DefaultPetName.Value; }
  static truth GetStartWithNoPet () { return StartWithNoPet.Value; }
  static sLong GetAutoSaveInterval () { return AutoSaveInterval.Value; }
  static sLong GetContrast () { return Contrast.Value; }
  static truth GetWarnAboutDanger () { return WarnAboutDanger.Value; }
  static truth GetAttackIndicator () { return AttackIndicator.Value; }
  static int GetAttackIndicatorDelay () { return AttackIndicatorDelay.Value; }
  static truth GetAutoDropLeftOvers () { return AutoDropLeftOvers.Value; }
  static truth GetAutoDropBottles () { return AutoDropBottles.Value; }
  static truth GetAutoDropCans () { return AutoDropCans.Value; }
  static truth GetLookZoom () { return LookZoom.Value; }
  static truth GetBeNice () { return BeNice.Value; }
  static truth GetFullScreenMode () { return FullScreenMode.Value; }
  static sLong GetDoubleResModifier () { return DoubleResModifier.Value; }
  static truth GetBlurryScale () { return BlurryScale.Value; }
  static truth GetOversampleBlurry () { return OversampleBlurry.Value; }
  static void SwitchModeHandler ();
  static truth GetKickDownDoors () { return KickDownDoors.Value; }
  static truth GetAutoCenterMap () { return AutoCenterMap.Value; }
  static truth GetAutoCenterMapOnLook () { return AutoCenterMapOnLook.Value; }
  static truth GetPlaySounds () { return PlaySounds.Value; }
  static sLong GetSoundVolume () { return SoundVolume.Value; }
  static truth GetConfirmCorpses () { return ConfirmCorpses.Value; }
  static int GetGoingDelay () { return GoingDelay.Value; }
  static truth GetStopOnCorpses () { return ((GoStopOnKind.Value & 0x01) != 0); }
  static truth GetStopOnSeenItems () { return ((GoStopOnKind.Value & 0x02) != 0); }
  static truth GetStopOnSeenDoors () { return ((GoStopOnKind.Value & 0x04) != 0); }
  static truth GetConfirmScrollReading () { return ConfirmScrollReading.Value; }
  static int GetCompressionLevel () { return CompressionLevel.Value; }
  static truth GetShowFullItemDesc () { return ShowFullItemDesc.Value; }
  static truth GetSaveQuitScore () { return SaveQuitScore.Value; }
  static truth GetStatusOnLeft () { return StatusOnLeft.Value; }
  static truth GetStatusOnBottom () { return StatusOnBottom.Value; }
  static truth GetShorterListsX () { return ShorterListsX.Value; }
  static truth GetSkipIntro () { return SkipIntro.Value; }
  static truth GetOutlineItems () { return OutlineItems.Value; }
  static truth GetOutlineMonsters () { return OutlineMonsters.Value; }
  static truth GetShowWeaponCategory () { return ShowWeaponCategory.Value; }
  static truth GetShowItemComparison () { return ((ShowItemPopups.Value & 1) != 0); }
  static truth GetShowOtherItemPopups () { return ((ShowItemPopups.Value & 2) != 0); }
  static truth GetEscQuits () { return EscQuits.Value; }
  static truth GetBlurFOW () { return BlurFOW.Value; }

  // 0:none; 1:fast; 2:safe
  static sLong GetSaveLogToDB () { return SaveLogToDB.Value; }

  static sLong ApplyContrastTo (sLong);
  static void Save () { configsystem::Save(); }
  static void Load () { configsystem::Load(); }
  static void CalculateContrastLuminance ();
  static col24 GetContrastLuminance () { return ContrastLuminance; }
  static void Initialize ();
  static void Show ();

  static void SetTempContrastLuminance (col24 v) { ContrastLuminance = v; }

  static int GetWinWidth () { return OrigWinWidth.Value; }
  static int GetWinHeight () { return OrigWinHeight.Value; }

  static festring GetMyDir ();

private:
  static v2 GetQuestionPos ();
  static void AutoSaveIntervalDisplayer (const numberoption *, festring &);
  static void ContrastDisplayer (const numberoption *, festring &);
  static truth DefaultNameChangeInterface (stringoption *);
  static truth DefaultPetNameChangeInterface (stringoption *);
  static truth AutoSaveIntervalChangeInterface (numberoption *);
  static truth ContrastChangeInterface (numberoption *);
  static void AutoSaveIntervalChanger (numberoption *, sLong);
  static void ContrastChanger (numberoption *, sLong);
  static void SoundChanger (numberoption *, sLong);
  static void FullScreenModeChanger (truthoption *, truth);
  static void FastListChanger (truthoption *, truth);
  static void ContrastHandler (sLong);
  static void BackGroundDrawer ();

  static void CompLevelDisplayer (const numberoption *O, festring &Entry);
  static truth CompLevelChangeInterface (numberoption *O);
  static void CompLevelChanger (numberoption *O, sLong What);
  static void CompLevelHandler (sLong Value);

  static void ResModDisp (const numberoption *O, festring &Entry);
  static truth ResModChangeIntf (numberoption *O);
  static void ResModChanger (numberoption *O, sLong What);
  static void ResModHandler (sLong Value);

  static void SoundVolumeDisplayer (const numberoption *, festring &);
  static truth SoundVolumeChangeInterface (numberoption *);
  static void SoundVolumeChanger (numberoption *, sLong);
  static void SoundVolumeHandler (sLong);

  static void GoingDelayDisplayer (const numberoption *O, festring &Entry);
  static truth GoingDelayChangeInterface (numberoption *O);
  static void GoingDelayChanger (numberoption *O, sLong What);

  static void AttackFlashDelayDisplayer (const numberoption *O, festring &Entry);
  static truth AttackFlashDelayChangeInterface (numberoption *O);
  static void AttackFlashDelayChanger (numberoption *O, sLong What);

  static void ItemPopupDisplayer (const numberoption *O, festring &Entry);
  static truth ItemPopupChangeInterface (numberoption *O);
  static void ItemPopupChanger (numberoption *O, sLong What);

  static void SaveLogDisplayer (const numberoption *O, festring &Entry);
  static truth SaveLogChangeInterface (numberoption *O);
  static void SaveLogChanger (numberoption *O, sLong What);

  static void GoStopDisplayer (const numberoption *O, festring &Entry);
  static truth GoStopChangeInterface (numberoption *O);
  static void GoStopChanger (numberoption *O, sLong What);

  static void BlurryChanger (truthoption *, truth value);

public:
  static int WinWidth ();
  static int WinHeight ();

private:
  static int currWinWidth;
  static int currWinHeight;

  static stringoption DefaultName;
  static stringoption DefaultPetName;
  static truthoption StartWithNoPet;
  static numberoption AutoSaveInterval;
  static scrollbaroption Contrast;
  static truthoption WarnAboutDanger;
  static truthoption AttackIndicator;
  static numberoption AttackIndicatorDelay;
  static truthoption AutoDropLeftOvers;
  static truthoption AutoDropBottles;
  static truthoption AutoDropCans;
  static truthoption LookZoom;
  static truthoption BeNice;
  static truthoption FullScreenMode;
  /*k8*/
  static scrollbaroption DoubleResModifier;
  static truthoption BlurryScale;
  static truthoption OversampleBlurry;
  static truthoption KickDownDoors;
  static truthoption AutoCenterMap;
  static truthoption AutoCenterMapOnLook;
  static truthoption FastListMode;
  static truthoption PlaySounds;
  static scrollbaroption SoundVolume;
  static truthoption ConfirmCorpses;
  static numberoption GoingDelay;
  static numberoption GoStopOnKind;
  static truthoption ConfirmScrollReading;
  static scrollbaroption CompressionLevel;
  static col24 ContrastLuminance;
  static truthoption ShowFullItemDesc;
  static truthoption SaveQuitScore;
  static truthoption StatusOnLeft;
  static truthoption StatusOnBottom;
  static truthoption ShorterListsX;
  static truthoption SkipIntro;
  static truthoption OutlineItems;
  static truthoption OutlineMonsters;
  static truthoption ShowWeaponCategory;
  static numberoption SaveLogToDB;
  static numberoption ShowItemPopups;
  static numberoption OrigWinWidth;
  static numberoption OrigWinHeight;
  static truthoption EscQuits;
  static truthoption BlurFOW;
};


inline sLong ivanconfig::ApplyContrastTo (sLong L) {
  sLong C = Contrast.Value;
  if (C == 100) return L;
  return MakeRGB24(41*GetRed24(L)*C>>12, 41*GetGreen24(L)*C>>12, 41*GetBlue24(L)*C>>12);
}


#endif
