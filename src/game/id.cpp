/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "id.h"
#include "game.h"
#include "god.h"
#include "festring.h"


//==========================================================================
//
//  id::DebugLogName
//
//==========================================================================
festring id::DebugLogName (int Flags) const {
  festring res;
  AddAdjective(res, false/*no article*/);
  if ((Flags & STRIP_MATERIAL) == 0 && ShowMaterial()) {
    AddMaterialDescription(res, false/*no article*/);
  }
  AddNameSingular(res, false/*no article*/);
  AddPostFix(res, STRIP_STATES | NO_FLUIDS);
  return res.NormalizeSpaces(true);
}


//==========================================================================
//
//  id::GetArticle
//
//==========================================================================
cchar *id::GetArticle () const {
  if (GetArticleMode() == FORCE_THE) {
    return "the";
  } else {
    return (UsesLongArticle() ? "an" : "a");
  }
}


//==========================================================================
//
//  id::GetArticleMode
//
//==========================================================================
int id::GetArticleMode () const {
  return 0;
}


//==========================================================================
//
//  id::ShowMaterial
//
//==========================================================================
truth id::ShowMaterial () const {
  return false;
}


//==========================================================================
//
//  id::AddRustLevelDescription
//
//==========================================================================
truth id::AddRustLevelDescription (festring &String, truth Articled) const {
  return false;
}


//==========================================================================
//
//  id::AddStateDescription
//
//==========================================================================
truth id::AddStateDescription (festring &String, truth Articled) const {
  return false;
}


//==========================================================================
//
//  id::AddMaterialDescription
//
//==========================================================================
truth id::AddMaterialDescription (festring &String, truth Articled) const {
  return false;
}


//==========================================================================
//
//  id::AddNameSingular
//
//==========================================================================
void id::AddNameSingular (festring &String, truth Articled) const {
  if (Articled) {
    if (UsesLongArticle()) String << "an "; else String << "a ";
  }
  String << GetNameSingular();
}


//==========================================================================
//
//  id::AddName
//
//==========================================================================
void id::AddName (festring &Name, int Case) const {
  truth Articled;
  if ((Case & ARTICLE_BIT) &&
      (GetArticleMode() == FORCE_THE ||
       (!GetArticleMode() && !(Case & INDEFINE_BIT))))
  {
    Name << "the\x18"; /*NBSP*/
    Articled = false;
  } else {
    Articled = !(Case & PLURAL) &&
               (Case & ARTICLE_BIT) &&
               (Case & INDEFINE_BIT) &&
               (GetArticleMode() != NO_ARTICLE);
  }
  if ((Case & STRIP_STATES) == 0) {
    if (AddRustLevelDescription(Name, Articled)) Articled = false;
    if (AddStateDescription(Name, Articled)) Articled = false;
  }
  if (AddAdjective(Name, Articled)) Articled = false;
  if ((Case & STRIP_MATERIAL) == 0 && ShowMaterial() && AddMaterialDescription(Name, Articled)) {
    Articled = false;
  }
  if (Case & PLURAL) {
    Name << GetNamePlural();
  } else {
    AddNameSingular(Name, Articled);
  }
  AddPostFix(Name, Case);
}


//==========================================================================
//
//  id::GetName
//
//==========================================================================
festring id::GetName (int Case) const {
  festring Name;
  AddName(Name, Case);
  return Name;
}


//==========================================================================
//
//  id::AddName
//
//==========================================================================
void id::AddName (festring &Name, int Case, int Amount) const {
  if (Amount == 1) {
    AddName(Name, Case&~PLURAL);
  } else {
    if ((Case & ARTICLE_BIT) &&
        (GetArticleMode() == FORCE_THE || (!GetArticleMode() && !(Case & INDEFINE_BIT))))
    {
      Name << "the\x18";
    }
    Name << Amount << ' ';
    AddName(Name, (Case&~ARTICLE_BIT)|PLURAL); //k8: ???
  }
}


//==========================================================================
//
//  id::GetName
//
//==========================================================================
festring id::GetName (int Case, int Amount) const {
  festring Name;
  AddName(Name, Case, Amount);
  return Name;
}


//==========================================================================
//
//  id::AddAdjective
//
//==========================================================================
truth id::AddAdjective (festring &String, truth Articled) const {
  if (GetAdjective().GetSize()) {
    if (Articled) {
      if (UsesLongAdjectiveArticle()) String << "an "; else String << "a ";
    }
    String << GetAdjective() << ' ';
    return true;
  }
  return false;
}


//==========================================================================
//
//  id::AddPostFix
//
//==========================================================================
void id::AddPostFix (festring &String, int) const {
  if (GetPostFix().GetSize()) {
    String << ' ' << GetPostFix();
  }
}


//==========================================================================
//
//  id::AddActiveAdjective
//
//==========================================================================
truth id::AddActiveAdjective (festring &String, truth Articled) const {
  String << (Articled ? "an active " : "active ");
  return true;
}
