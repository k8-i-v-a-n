/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

#ifndef __ID_H__
#define __ID_H__

#include "typedef.h"


class festring;


#define CHAR_NAME(Case)         GetName(Case).CStr()
#define CHAR_DESCRIPTION(Case)  GetDescription(Case).CStr()


class id {
public:
  virtual ~id () {}

  virtual void AddName (festring &, int Flags, int Amount) const;
  virtual festring GetName (int Flags, int Amount) const;
  virtual void AddName (festring &, int Flags) const;
  virtual festring GetName (int Flags) const;
  //inline cchar *GetArticle () const { return (UsesLongArticle() ? "an" : "a"); }
  cchar *GetArticle () const;

  // understands only `STRIP_MATERIAL`
  festring DebugLogName (int Flags=0) const;

protected:
  virtual cfestring &GetNameSingular () const = 0;
  virtual cfestring &GetNamePlural () const = 0;
  virtual cfestring &GetAdjective () const = 0;
  virtual cfestring &GetPostFix () const = 0;

  virtual int GetArticleMode () const; // `0`, `FORCE_THE`, `NO_ARTICLE`; default is `0`
  virtual truth ShowMaterial () const; // default is `false`
  virtual truth UsesLongArticle () const = 0;
  virtual truth UsesLongAdjectiveArticle () const = 0;

  virtual void AddNameSingular (festring &String, truth Articled) const;
  virtual truth AddRustLevelDescription (festring &String, truth Articled) const; // default is nothing
  virtual truth AddStateDescription (festring &String, truth Articled) const; // default is nothing
  virtual truth AddMaterialDescription (festring &String, truth Articled) const; // default is nothing
  virtual truth AddAdjective (festring &String, truth Articled) const;
  virtual void AddPostFix (festring &String, int Flags) const;
  virtual truth AddActiveAdjective (festring &String, truth Articled) const;
};


#endif
