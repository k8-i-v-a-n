/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

#include "igraph.h"
#include "felist.h"
#include "bitmap.h"
#include "graphics.h"
#include "iconf.h"
#include "rawbit.h"
#include "game.h"
#include "fesave.h"
#include "feparse.h"
#include "object.h"
#include "allocate.h"
#include "whandler.h"

// see "ivandef.h"
cv2 SILHOUETTE_SIZE(48, 64);

rawbitmap *igraph::RawGraphic[RAW_TYPES];
bitmap *igraph::Graphic[GRAPHIC_TYPES];
bitmap *igraph::TileBuffer;
bitmap *igraph::FlagBuffer;
cchar *igraph::RawGraphicFileName[] = {
  "graphics/GLTerra.pcx",
  "graphics/OLTerra.pcx",
  "graphics/Item.pcx",
  "graphics/Char.pcx",
  "graphics/Humanoid.pcx",
  "graphics/Effect.pcx",
  "graphics/Cursor.pcx",
};
cchar *igraph::GraphicFileName[] = {
  "graphics/WTerra.pcx",
  "graphics/FOW.pcx",
  "graphics/Symbol.pcx",
  "graphics/Smiley.pcx",
  "graphics/Humanoid.pcx",
};
//tilemap igraph::TileMap;
//tilemapgids igraph::TileMapGIs;
//revtilemapgids igraph::RevTileMapGIs;
TMapGrxID igraph::tilesGrx;
TMapUID igraph::tilesUID;
uChar igraph::RollBuffer[256];
int **igraph::BodyBitmapValidityMap;
bitmap *igraph::Menu;
bitmap *igraph::SilhouetteCache[HUMANOID_BODYPARTS][CONDITION_COLORS][SILHOUETTE_TYPES];
rawbitmap *igraph::ColorizeBuffer[2] = { new rawbitmap(TILE_V2), new rawbitmap(TILE_V2) };
bitmap *igraph::Cursor[CURSOR_TYPES];
bitmap *igraph::BigCursor[CURSOR_TYPES];
col16 igraph::CursorColor[CURSOR_TYPES] = {
  MakeRGB16(40, 40, 40),    // dark cursor
  MakeRGB16(255, 0, 0),     // red cursor
  MakeRGB16(100, 100, 255), // blue cursor
  MakeRGB16(200, 200, 0),   // yellow cursor
  MakeRGB16(64, 200, 64),   // green cursor
};
bitmap *igraph::BackGround;
int igraph::CurrentColorType = -1;
GfxUID igraph::lastUID = 0;


//==========================================================================
//
//  igraph::Init
//
//==========================================================================
void igraph::Init () {
  static truth AlreadyInstalled = false;

  if (!AlreadyInstalled) {
    AlreadyInstalled = true;
    graphics::Init();
    graphics::SetMode("k8 I.V.A.N. " IVAN_VERSION,
                      "graphics/Icon.bmp",
                      v2(ivanconfig::WinWidth(), ivanconfig::WinHeight()),
                      ivanconfig::GetFullScreenMode(), ivanconfig::GetDoubleResModifier(),
                      ivanconfig::GetBlurryScale(), ivanconfig::GetOversampleBlurry());
    DOUBLE_BUFFER->ClearToColor(0);
    graphics::BlitDBToScreen();
    graphics::SetSwitchModeHandler(ivanconfig::SwitchModeHandler);
    graphics::LoadDefaultFont(game::GetGamePath() + "graphics/Font.pcx");
    graphics::LoadConsoleFont(game::GetGamePath() + "graphics/");
    object::InitSparkleValidityArrays();
    int c;

    for (c = 0; c != RAW_FILE_TYPES; c += 1) {
      RawGraphic[c] = new rawbitmap(game::GetGamePath() + RawGraphicFileName[c]);
    }

    for (c = 0; c < GRAPHIC_TYPES; c += 1) {
      Graphic[c] = new bitmap(game::GetGamePath() + GraphicFileName[c]);
      Graphic[c]->ActivateFastFlag();
    }

    ColorizeBuffer[0]->CopyPaletteFrom(RawGraphic[0]);
    ColorizeBuffer[1]->CopyPaletteFrom(RawGraphic[0]);
    TileBuffer = new bitmap(TILE_V2);
    TileBuffer->ActivateFastFlag();
    TileBuffer->InitPriorityMap(0);
    FlagBuffer = new bitmap(TILE_V2);
    FlagBuffer->ActivateFastFlag();
    FlagBuffer->CreateAlphaMap(0);
    Alloc2D(BodyBitmapValidityMap, 8, 16);
    CreateBodyBitmapValidityMaps();
    CreateSilhouetteCaches();

    for (c = 0; c < CURSOR_TYPES; c += 1) {
      packcol16 Color = CursorColor[c];
      Cursor[c] = new bitmap(v2(48, 16), TRANSPARENT_COLOR);
      Cursor[c]->CreateAlphaMap(255);
      GetCursorRawGraphic()->MaskedBlit(Cursor[c], ZERO_V2, ZERO_V2, v2(48, 16), &Color);
      BigCursor[c] = new bitmap(v2(96, 32), TRANSPARENT_COLOR);
      BigCursor[c]->CreateAlphaMap(255);
      GetCursorRawGraphic()->MaskedBlit(BigCursor[c], v2(0, 16), ZERO_V2, v2(96, 32), &Color);
    }
  }
}


//==========================================================================
//
//  igraph::DeInit
//
//==========================================================================
void igraph::DeInit () {
  int c;

  for (c = 0; c < RAW_TYPES; ++c) delete RawGraphic[c];
  for (c = 0; c < GRAPHIC_TYPES; ++c) delete Graphic[c];

  delete TileBuffer;
  delete FlagBuffer;
  delete [] BodyBitmapValidityMap;
  delete BackGround;
}


//==========================================================================
//
//  igraph::DrawCursor
//
//==========================================================================
void igraph::DrawCursor (v2 Pos, int CursorData, int Index) {
  /* Inefficient gum solution */
  blitdata BlitData = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { Pos.X, Pos.Y },
    { TILE_SIZE, TILE_SIZE },
    { (int)ivanconfig::GetContrastLuminance() }, //k8:UB, i don't fuckin' care
    TRANSPARENT_COLOR,
    0
  };

  bitmap *CursorBitmap;
  int SrcX = 0;

  if (CursorData & CURSOR_BIG) {
    CursorBitmap = BigCursor[CursorData&~CURSOR_FLAGS];
    BlitData.Src.X = SrcX = (Index & 1) << 4;
    BlitData.Src.Y = (Index & 2) << 3;
  } else {
    CursorBitmap = Cursor[CursorData&~CURSOR_FLAGS];
  }

  if (CursorData & CURSOR_SHADE) {
    //auto sz = CursorBitmap->GetSize();
    int xmul = (((GET_TICK()<<2)/3)>>4)%2;
    for (int dy = 0; dy < 16; ++dy) {
      for (int dx = 0; dx < 16; ++dx) {
        DOUBLE_BUFFER->AlphaPutPixel(Pos.X+dx, Pos.Y+dy,
                                     MakeRGB16(255, 127, 0),
                                     MakeRGB24(255, 127, 0), 160*xmul);
      }
    }
  }

  if (!(CursorData&(CURSOR_FLASH|CURSOR_TARGET))) {
    CursorBitmap->LuminanceMaskedBlit(BlitData);
    return;
  }

  if (!(CursorData&CURSOR_TARGET)) {
    int Tick = GET_TICK() & 31;
    CursorBitmap->FillAlpha(95 + 10 * abs(Tick - 16));
    CursorBitmap->AlphaLuminanceBlit(BlitData);
    return;
  }


  int Tick = (GET_TICK()<<2)/3;
  int Frame = (Tick >> 4)%3;
  int Base = Frame<<4;
  BlitData.Src.X = SrcX + (CursorData & CURSOR_BIG ? Base << 1 : Base);
  CursorBitmap->FillAlpha(255 - (Tick & 0xF) * 16);
  CursorBitmap->AlphaLuminanceBlit(BlitData);
  Base = ((Frame + 1) % 3) << 4;
  BlitData.Src.X = SrcX + (CursorData & CURSOR_BIG ? Base << 1 : Base);
  CursorBitmap->FillAlpha((Tick & 0xF) * 16);
  CursorBitmap->AlphaLuminanceBlit(BlitData);

  {
    int xalpha = (((GET_TICK()<<2)/3)>>3)%8;
    xalpha = (xalpha < 4 ? 32+16*xalpha : 32+16*(7-xalpha));
    for (int dy = 0; dy < (CursorData&CURSOR_BIG ? 32 : 16); ++dy) {
      for (int dx = 0; dx < (CursorData&CURSOR_BIG ? 32 : 16); ++dx) {
        DOUBLE_BUFFER->AlphaPutPixel(Pos.X+dx, Pos.Y+dy,
                                     MakeRGB16(255, 255, 0), MakeRGB24(255, 255, 0),
                                     xalpha);
      }
    }
  }
}


//==========================================================================
//
//  igraph::allocUID
//
//==========================================================================
GfxUID igraph::allocUID () {
  for (;;) {
    lastUID += 1;
    if (lastUID != 0) {
      if (tilesUID.find(lastUID) == tilesUID.end()) {
        return lastUID;
      }
    }
  }
}


//==========================================================================
//
//  igraph::WipeTiles
//
//==========================================================================
void igraph::WipeTiles () {
  for (TMapUID::iterator uit = tilesUID.begin(); uit != tilesUID.end(); ++uit) {
    delete uit->second->Bitmap;
    delete uit->second;
    uit->second = 0;
  }
  tilesUID.clear();
  tilesGrx.clear();
}


//==========================================================================
//
//  igraph::AppendNewTile
//
//==========================================================================
GfxUID igraph::AppendNewTile (const graphicid &GI, bitmap *bmp, TileGfxInfo *&gi) {
  TMapGrxID::iterator it = tilesGrx.find(GI);

  if (it != tilesGrx.end()) {
    TMapUID::iterator uit = tilesUID.find(it->second->uid);
    if (uit != tilesUID.end()) {
      ConLogf("!!! WARNING: duplicate `graphicid` tile (uid=%u) -- memory leak!",
              it->second->uid);
    } else {
      ConLogf("!!! WARNING: duplicate `graphicid` tile -- memory leak!");
    }
    bool registered = false;
    for (TMapUID::iterator uit = tilesUID.begin();
         !registered && uit != tilesUID.end(); ++uit)
    {
      if (uit->second == it->second) {
        ConLogf("         ...and it is still registered!");
        registered = true;
      }
    }
    if (!registered) {
      delete it->second;
    }
    tilesGrx.erase(it);
  }

  TileGfxInfo *Tile = new TileGfxInfo();
  Tile->uid = allocUID();
  Tile->gid = GI;
  Tile->Bitmap = bmp;
  Tile->Users = 1;

  tilesGrx[Tile->gid] = Tile;
  tilesUID[Tile->uid] = Tile;

  gi = Tile;
  return Tile->uid;
}


//==========================================================================
//
//  igraph::RemoveUser
//
//==========================================================================
void igraph::RemoveUser (GfxUID id) {
  TMapUID::iterator uit = tilesUID.find(id);
  if (uit != tilesUID.end()) {
    TMapGrxID::iterator it = tilesGrx.find(uit->second->gid);
    if (it != tilesGrx.end()) {
      TileGfxInfo *Tile = it->second;
      IvanAssert(Tile == uit->second);
      Tile->Users -= 1;
      IvanAssert(Tile->Users >= 0);
      if (Tile->Users == 0) {
        tilesGrx.erase(it);
        tilesUID.erase(uit);
        delete Tile->Bitmap;
        delete Tile;
      }
    } else {
      ConLogf("WARNING: orphaned gfx id #%u! (0)", id);
      tilesUID.erase(uit);
    }
  } else {
    ConLogf("WARNING: orphaned gfx id #%u! (1)", id);
  }
}


//==========================================================================
//
//  igraph::FindUserByUID
//
//==========================================================================
TileGfxInfo *igraph::FindUserByUID (GfxUID uid) {
  TMapUID::iterator git = tilesUID.find(uid);
  if (git != tilesUID.end()) {
    return git->second;
  } else {
    return 0;
  }
}


//==========================================================================
//
//  igraph::AddUser
//
//==========================================================================
GfxUID igraph::AddUserEx (const graphicid &GI, TileGfxInfo *&gi) {
  TMapGrxID::iterator git = tilesGrx.find(GI);

  if (git != tilesGrx.end()) {
    TileGfxInfo *Tile = git->second;
    IvanAssert(Tile);
    IvanAssert(Tile->uid != 0);
    IvanAssert(Tile->Users > 0);
    IvanAssert(Tile->gid == GI);
    gi = Tile;
    Tile->Users += 1;
    return Tile->uid;
  } else {
    cint SpecialFlags = GI.SpecialFlags;
    cint BodyPartFlags = SpecialFlags & 0x78;
    cint RotateFlags = SpecialFlags & 0x7;
    cint Frame = GI.Frame;
    v2 SparklePos = v2(GI.SparklePosX, GI.SparklePosY);
    uChar fileidx = GI.FileIndex;
    rawbitmap *RawBitmap = RawGraphic[fileidx];
    v2 RawPos = v2(GI.BitmapPosX, GI.BitmapPosY);

    if (BodyPartFlags && BodyPartFlags < ST_OTHER_BODYPART) {
      ColorizeBuffer[0]->Clear();
      EditBodyPartTile(RawBitmap, ColorizeBuffer[0], RawPos, BodyPartFlags);
      RawBitmap = ColorizeBuffer[0];
      RawPos.X = RawPos.Y = 0;

      if (RotateFlags) {
        ColorizeBuffer[1]->Clear();
        SparklePos = RotateTile(RawBitmap, ColorizeBuffer[1], RawPos, SparklePos, RotateFlags);
        RawBitmap = ColorizeBuffer[1];
      }
    } else if (RotateFlags) {
      ColorizeBuffer[0]->Clear();
      SparklePos = RotateTile(RawBitmap, ColorizeBuffer[0], RawPos, SparklePos, RotateFlags);
      RawBitmap = ColorizeBuffer[0];
      RawPos.X = RawPos.Y = 0;
    }

    const bool outlineEnabled = !!ivanconfig::GetOutlineItems();
    bitmap *Bitmap = RawBitmap->Colorize(RawPos,
                                         TILE_V2,
                                         GI.Position,
                                         GI.Color,
                                         GI.BaseAlpha,
                                         GI.Alpha,
                                         GI.RustData,
                                         !(GI.SpecialFlags & ST_DISALLOW_R_COLORS),
                                         !!(GI.SpecialFlags & ST_SIMPLE_ITEM_OUTLINE) && outlineEnabled);
    Bitmap->ActivateFastFlag();

    if (BodyPartFlags) {
      Bitmap->InitPriorityMap(SpecialFlags & ST_CLOAK ? CLOAK_PRIORITY : AVERAGE_PRIORITY);
    }

    if (GI.OutlineColor != TRANSPARENT_COLOR) {
      Bitmap->Outline(GI.OutlineColor, GI.OutlineAlpha,
                      BodyPartFlags != ST_WIELDED ? ARMOR_OUTLINE_PRIORITY : AVERAGE_PRIORITY);
    }

    if (SparklePos.X != SPARKLE_POS_X_ERROR) {
      Bitmap->CreateSparkle(SparklePos + GI.Position, GI.SparkleFrame);
    }

    if (GI.FlyAmount) {
      Bitmap->CreateFlies(GI.Seed, Frame, GI.FlyAmount);
    }

    cint WobbleData = GI.WobbleData;

    if (WobbleData & WOBBLE) {
      int Speed = (WobbleData & WOBBLE_SPEED_RANGE) >> WOBBLE_SPEED_SHIFT;
      int Freq = (WobbleData & WOBBLE_FREQ_RANGE) >> WOBBLE_FREQ_SHIFT;
      int WobbleMask = 7 >> Freq << (6 - Speed);

      if (!(Frame & WobbleMask)) {
        Bitmap->Wobble(Frame & ((1 << (6 - Speed)) - 1), Speed, WobbleData & WOBBLE_HORIZONTALLY_BIT);
      }
    }

    if (SpecialFlags & ST_FLAMES) {
      const feuLong SeedNFlags = (SpecialFlags >> ST_FLAME_SHIFT & 3) | (GI.Seed << 4);
      Bitmap->CreateFlames(RawBitmap, RawPos - GI.Position, SeedNFlags, Frame);
    }

    if (SpecialFlags & ST_LIGHTNING && !((Frame + 1) & 7)) {
      Bitmap->CreateLightning(GI.Seed + Frame, WHITE);
    }

    return AppendNewTile(GI, Bitmap, gi);
  }
}


//==========================================================================
//
//  igraph::EditBodyPartTile
//
//==========================================================================
void igraph::EditBodyPartTile (rawbitmap *Source, rawbitmap *Dest, v2 Pos, int BodyPartFlags) {
  if (BodyPartFlags == ST_RIGHT_ARM) {
    Source->NormalBlit(Dest, Pos, ZERO_V2, v2(8, 16));
  } else if (BodyPartFlags == ST_LEFT_ARM) {
    Source->NormalBlit(Dest, v2(Pos.X + 8, Pos.Y), v2(8, 0), v2(8, 16));
  } else if (BodyPartFlags == ST_GROIN) {
    Source->NormalBlit(Dest, v2(Pos.X, Pos.Y + 8), v2(0, 8), v2(16, 2));
    //int i;
    v2 V;
    for (V.Y = 10/*, i = 0*/; V.Y < 13; ++V.Y) {
      for (V.X = V.Y - 5; V.X < 20 - V.Y; ++V.X) {
        Dest->PutPixel(V, Source->GetPixel(Pos + V));
      }
    }
  } else if (BodyPartFlags == ST_RIGHT_LEG) {
    /* Right leg from the character's, NOT the player's point of view */
    Source->NormalBlit(Dest, v2(Pos.X, Pos.Y + 10), v2(0, 10), v2(8, 6));
    Dest->PutPixel(v2(5, 10), TRANSPARENT_PALETTE_INDEX);
    Dest->PutPixel(v2(6, 10), TRANSPARENT_PALETTE_INDEX);
    Dest->PutPixel(v2(7, 10), TRANSPARENT_PALETTE_INDEX);
    Dest->PutPixel(v2(6, 11), TRANSPARENT_PALETTE_INDEX);
    Dest->PutPixel(v2(7, 11), TRANSPARENT_PALETTE_INDEX);
    Dest->PutPixel(v2(7, 12), TRANSPARENT_PALETTE_INDEX);
  } else if (BodyPartFlags == ST_LEFT_LEG) {
    /* Left leg from the character's, NOT the player's point of view */
    Source->NormalBlit(Dest, v2(Pos.X + 8, Pos.Y + 10), v2(8, 10), v2(8, 6));
    Dest->PutPixel(v2(8, 10), TRANSPARENT_PALETTE_INDEX);
    Dest->PutPixel(v2(9, 10), TRANSPARENT_PALETTE_INDEX);
    Dest->PutPixel(v2(8, 11), TRANSPARENT_PALETTE_INDEX);
  }
}


//==========================================================================
//
//  igraph::RotateTile
//
//==========================================================================
v2 igraph::RotateTile (rawbitmap *Source, rawbitmap *Dest, v2 Pos, v2 SparklePos, int RotateFlags) {
  Source->NormalBlit(Dest, Pos, ZERO_V2, TILE_V2, RotateFlags);

  if (SparklePos.X != SPARKLE_POS_X_ERROR) {
    if (RotateFlags & ROTATE) {
      cint T = SparklePos.X;
      SparklePos.X = 15 - SparklePos.Y;
      SparklePos.Y = T;
    }

    if (RotateFlags & MIRROR) {
      SparklePos.X = 15 - SparklePos.X;
    }

    if (RotateFlags & FLIP) {
      SparklePos.Y = 15 - SparklePos.Y;
    }
  }

  return SparklePos;
}


//==========================================================================
//
//  operator <<
//
//  k8: i HAET this!
//
//==========================================================================
outputfile &operator << (outputfile& SaveFile, const graphicid &Value) {
  //SaveFile.Write(reinterpret_cast<cchar*>(&Value), sizeof(Value));
  SaveFile
    << Value.BitmapPosX
    << Value.BitmapPosY
    << Value.Frame
    << Value.FileIndex
    << Value.SpecialFlags
    << Value.BaseAlpha
    << Value.SparkleFrame
    << Value.SparklePosX
    << Value.SparklePosY
    << Value.OutlineColor
    << Value.OutlineAlpha
    << Value.FlyAmount
    << Value.Position
    << Value.Seed
    << Value.WobbleData
    << Value.Color[0]
    << Value.Color[1]
    << Value.Color[2]
    << Value.Color[3]
    << Value.Alpha[0]
    << Value.Alpha[1]
    << Value.Alpha[2]
    << Value.Alpha[3]
    << Value.RustData[0]
    << Value.RustData[1]
    << Value.RustData[2]
    << Value.RustData[3];
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//  k8: i HAET this!
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, graphicid &Value) {
  //SaveFile.Read(reinterpret_cast<char*>(&Value), sizeof(Value));
  SaveFile
    >> Value.BitmapPosX
    >> Value.BitmapPosY
    >> Value.Frame
    >> Value.FileIndex
    >> Value.SpecialFlags
    >> Value.BaseAlpha
    >> Value.SparkleFrame
    >> Value.SparklePosX
    >> Value.SparklePosY
    >> Value.OutlineColor
    >> Value.OutlineAlpha
    >> Value.FlyAmount
    >> Value.Position
    >> Value.Seed
    >> Value.WobbleData
    >> Value.Color[0]
    >> Value.Color[1]
    >> Value.Color[2]
    >> Value.Color[3]
    >> Value.Alpha[0]
    >> Value.Alpha[1]
    >> Value.Alpha[2]
    >> Value.Alpha[3]
    >> Value.RustData[0]
    >> Value.RustData[1]
    >> Value.RustData[2]
    >> Value.RustData[3];
  return SaveFile;
}


//==========================================================================
//
//  graphicdata::~graphicdata
//
//==========================================================================
graphicdata::~graphicdata () {
  IvanAssert(AnimationFrames >= 0);
  if (AnimationFrames) {
    for (int c = 0; c != AnimationFrames; c += 1) {
      igraph::RemoveUser(GraphicIterator[c]);
    }
    delete [] Picture;
    delete [] GraphicIterator;
    AnimationFrames = 0;
    Picture = 0;
    GraphicIterator = 0;
  }
}


//==========================================================================
//
//  graphicdata::Save
//
//==========================================================================
void graphicdata::Save (outputfile &SaveFile) const {
  IvanAssert(AnimationFrames >= 0);
  SaveFile << (int)AnimationFrames;
  for (int c = 0; c != AnimationFrames; c += 1) {
    //SaveFile << GraphicIterator[c];
    TileGfxInfo *Tile = igraph::FindUserByUID(GraphicIterator[c]);
    IvanAssert(Tile);
    IvanAssert(Tile->Users > 0);
    IvanAssert(Tile->Bitmap);
    SaveFile << Tile->gid;
  }
}


//==========================================================================
//
//  graphicdata::Load
//
//==========================================================================
void graphicdata::Load (inputfile &SaveFile) {
  SaveFile >> (int&)AnimationFrames;
  IvanAssert(AnimationFrames >= 0);
  if (AnimationFrames) {
    Picture = new bitmap*[AnimationFrames];
    GraphicIterator = new GfxUID[AnimationFrames];
    graphicid GraphicID;
    for (int c = 0; c != AnimationFrames; c += 1) {
      GraphicID.Wipe();
      SaveFile >> GraphicID;
      TileGfxInfo *ti = 0;
      GraphicIterator[c] = igraph::AddUserEx(GraphicID, ti);
      IvanAssert(ti);
      Picture[c] = ti->Bitmap;
    }
  } else {
    // just in case. should not leak.
    Picture = 0;
    GraphicIterator = 0;
  }
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile& SaveFile, const graphicdata &Data) {
  Data.Save(SaveFile);
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile& SaveFile, graphicdata &Data) {
  Data.Load(SaveFile);
  return SaveFile;
}


//==========================================================================
//
//  graphicdata::Retire
//
//==========================================================================
void graphicdata::Retire () {
  IvanAssert(AnimationFrames >= 0);
  if (AnimationFrames) {
    for (int c = 0; c < AnimationFrames; ++c) {
      igraph::RemoveUser(GraphicIterator[c]);
    }
    delete [] Picture;
    delete [] GraphicIterator;
    AnimationFrames = 0;
    Picture = 0;
    GraphicIterator = 0;
  }
}


//==========================================================================
//
//  igraph::GetBodyBitmapValidityMap
//
//==========================================================================
cint *igraph::GetBodyBitmapValidityMap (int SpecialFlags) {
  return BodyBitmapValidityMap[(SpecialFlags & 0x38) >> 3];
}


//==========================================================================
//
//  igraph::CreateBodyBitmapValidityMaps
//
//==========================================================================
void igraph::CreateBodyBitmapValidityMaps () {
  memset(BodyBitmapValidityMap[0], 0xFF, 128 * sizeof(int));
  int x, y;

  int *Map = BodyBitmapValidityMap[ST_RIGHT_ARM >> 3];
  for (x = 8; x < 16; ++x) Map[x] = 0;

  Map = BodyBitmapValidityMap[ST_LEFT_ARM >> 3];
  for (x = 0; x < 8; ++x) Map[x] = 0;

  Map = BodyBitmapValidityMap[ST_GROIN >> 3];
  memset(Map, 0, 16 * sizeof(int));

  for (y = 10; y < 13; ++y) {
    for (x = y - 5; x < 20 - y; ++x) {
      Map[x] |= 1 << y;
    }
  }

  Map = BodyBitmapValidityMap[ST_RIGHT_LEG >> 3];
  for (x = 8; x < 16; ++x) Map[x] = 0;

  Map[5] &= ~(1 << 10);
  Map[6] &= ~(1 << 10);
  Map[7] &= ~(1 << 10);
  Map[6] &= ~(1 << 11);
  Map[7] &= ~(1 << 11);
  Map[7] &= ~(1 << 12);

  Map = BodyBitmapValidityMap[ST_LEFT_LEG >> 3];
  for (x = 0; x < 8; ++x) Map[x] = 0;

  Map[8] &= ~(1 << 10);
  Map[9] &= ~(1 << 10);
  Map[8] &= ~(1 << 11);
}


//==========================================================================
//
//  igraph::LoadMenuData
//
//==========================================================================
void igraph::LoadMenuData () {
  Menu = new bitmap(game::GetGamePath() + "graphics/Menu.pcx");
}


//==========================================================================
//
//  igraph::UnLoadMenu
//
//==========================================================================
void igraph::UnLoadMenu () {
  delete Menu;
}


//==========================================================================
//
//  igraph::CreateSilhouetteCaches
//
//==========================================================================
void igraph::CreateSilhouetteCaches () {
  int BodyPartSilhouetteMColorIndex[HUMANOID_BODYPARTS] = { 3, 0, 1, 2, 1, 2, 3 };
  col24 ConditionColor[CONDITION_COLORS] = { MakeRGB16(48, 48, 48),
               MakeRGB16(120, 0, 0),
               MakeRGB16(180, 0, 0),
               MakeRGB16(180, 120, 120),
               MakeRGB16(180, 180, 180) };
  v2 V(8, 64);

  for (int c1 = 0; c1 < HUMANOID_BODYPARTS; ++c1) {
    if (c1 == 4) V.X = 72;

    for (int c2 = 0; c2 < CONDITION_COLORS; ++c2) {
      packcol16 Color[4] = { 0, 0, 0, 0 };
      Color[BodyPartSilhouetteMColorIndex[c1]] = ConditionColor[c2];

      for (int c3 = 0; c3 < SILHOUETTE_TYPES; ++c3) {
        SilhouetteCache[c1][c2][c3] = new bitmap(SILHOUETTE_SIZE, 0);
        RawGraphic[GR_CHARACTER]->MaskedBlit(SilhouetteCache[c1][c2][c3],
                                             V, ZERO_V2,
                                             SILHOUETTE_SIZE, Color);
      }

      SilhouetteCache[c1][c2][SILHOUETTE_INTER_LACED]->InterLace();
    }
  }
}


//==========================================================================
//
//  igraph::CreateBackGround
//
//==========================================================================
void igraph::CreateBackGround (int ColorType) {
  if (CurrentColorType == ColorType) return;

  CurrentColorType = ColorType;
  delete BackGround;
  BackGround = new bitmap(RES);
  int Side = 1025;
  int **Map;
  Alloc2D(Map, Side, Side);
  femath::GenerateFractalMap(Map, Side, Side - 1, 800);

  for (int x = 0; x < RES.X; x += 1) {
    for (int y = 0; y < RES.Y; y += 1) {
      int E = Limit<int>(abs(Map[1024 - x][1024 - (RES.Y - y)]) / 30, 0, 100);
      BackGround->PutPixel(x, y, GetBackGroundColor(E));
    }
  }

  delete [] Map;
}


//==========================================================================
//
//  igraph::GetBackGroundColor
//
//==========================================================================
col16 igraph::GetBackGroundColor (int Element) {
  switch (CurrentColorType) {
    case GRAY_FRACTAL: return MakeRGB16(Element, Element, Element);
    case RED_FRACTAL: return MakeRGB16(Element + (Element >> 1), Element / 3, Element / 3);
    case GREEN_FRACTAL: return MakeRGB16(Element / 3, Element + (Element >> 2), Element / 3);
    case BLUE_FRACTAL: return MakeRGB16(Element / 3, Element / 3, Element + (Element >> 1));
    case YELLOW_FRACTAL: return MakeRGB16(Element + (Element >> 1), Element + (Element >> 1), Element / 3);
  }
  return 0;
}


//==========================================================================
//
//  igraph::BlitBackGround
//
//==========================================================================
void igraph::BlitBackGround (v2 Pos, v2 Border) {
  blitdata B = { DOUBLE_BUFFER,
      { Pos.X, Pos.Y },
      { Pos.X, Pos.Y },
      { Border.X, Border.Y },
      { 0 },
      0,
      0 };
  BackGround->NormalBlit(B);
}


//==========================================================================
//
//  igraph::GenerateScarBitmap
//
//==========================================================================
bitmap *igraph::GenerateScarBitmap (int BodyPart, int Severity, int Color) {
  bitmap *CacheBitmap = SilhouetteCache[BodyPart][0][SILHOUETTE_NORMAL];
  bitmap *Scar = new bitmap(SILHOUETTE_SIZE, 0);

  v2 StartPos;
  while (true) {
    StartPos = v2(RAND_N(SILHOUETTE_SIZE.X),RAND_N(SILHOUETTE_SIZE.Y));
    if (CacheBitmap->GetPixel(StartPos) != 0) break;
  }

  v2 EndPos;
  while (true) {
    double Angle = 2 * FPI * RAND_256 / 256;
    EndPos.X = int(StartPos.X + cos(Angle) * (Severity + 1));
    EndPos.Y = int(StartPos.Y + sin(Angle) * (Severity + 1));
    if (CacheBitmap->IsValidPos(EndPos) && CacheBitmap->GetPixel(EndPos) != 0) {
      break;
    }
  }

  Scar->DrawLine(StartPos, EndPos, Color);
  return Scar;
}
