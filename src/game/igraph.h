/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

#ifndef __IGRAPH_H__
#define __IGRAPH_H__

#include <map>
#include <unordered_map>

#include "ivandef.h"
#include "femath.h"
#include "iconf.h"


class bitmap;
class rawbitmap;
class outputfile;
class inputfile;
class festring;


struct graphicid {
public:
  FORCE_INLINE graphicid () { Wipe(); }

  FORCE_INLINE graphicid (const graphicid &src) {
    memcpy((void *)this, (const void *)&src, sizeof(graphicid));
  }

  FORCE_INLINE ~graphicid () { Wipe(); }

  FORCE_INLINE graphicid &operator = (const graphicid &src) {
    if ((const void *)this != (const void *)&src) {
      memcpy((void *)this, (const void *)&src, sizeof(graphicid));
    }
    return *this;
  }

  FORCE_INLINE void Wipe () {
    memset((void *)this, 0, sizeof(graphicid));
  }

  //k8: i HAET this!
  FORCE_INLINE int Compare (const graphicid &rhs) const {
    const int res = memcmp((const void *)this, (const void *)&rhs, sizeof(graphicid));
    return (res < 0 ? -1 : res > 0 ? 1 : 0);
  }

  FORCE_INLINE bool operator < (const graphicid &GI) const { return (Compare(GI) < 0); }
  FORCE_INLINE bool operator <= (const graphicid &GI) const { return (Compare(GI) <= 0); }
  FORCE_INLINE bool operator > (const graphicid &GI) const { return (Compare(GI) > 0); }
  FORCE_INLINE bool operator >= (const graphicid &GI) const { return (Compare(GI) >= 0); }
  FORCE_INLINE bool operator == (const graphicid &GI) const { return (Compare(GI) == 0); }
  FORCE_INLINE bool operator != (const graphicid &GI) const { return (Compare(GI) != 0); }

  FORCE_INLINE uint32_t Hash () const noexcept {
    return joaatHashBuf((const void *)this, sizeof(graphicid), 0x29a);
  }

public:
  uShort BitmapPosX;
  uShort BitmapPosY;
  packcol16 Color[4];
  uChar Frame;
  uChar FileIndex;
  uShort SpecialFlags;
  packalpha Alpha[4];
  packalpha BaseAlpha;
  uChar SparkleFrame;
  uChar SparklePosX;
  uChar SparklePosY;
  packcol16 OutlineColor;
  packalpha OutlineAlpha;
  uChar FlyAmount;
  v2 Position;
  uChar RustData[4];
  uShort Seed;
  uChar WobbleData;
};

outputfile &operator << (outputfile &, const graphicid &);
inputfile &operator >> (inputfile &, graphicid &);

MAKE_STD_HASHER(graphicid)


typedef uint32_t GfxUID;

struct TileGfxInfo {
public:
  GfxUID uid;
  graphicid gid;
  bitmap *Bitmap;
  sLong Users;

public:
  FORCE_INLINE TileGfxInfo () : uid(0), gid(), Bitmap(0), Users(0) {}
  FORCE_INLINE ~TileGfxInfo () {}

  FORCE_INLINE TileGfxInfo (const TileGfxInfo &other)
    : uid(other.uid)
    , gid(other.gid)
    , Bitmap(other.Bitmap)
    , Users(other.Users)
  {}

  FORCE_INLINE TileGfxInfo &operator = (const TileGfxInfo &other) {
    uid = other.uid;
    gid = other.gid;
    Bitmap = other.Bitmap;
    Users = other.Users;
    return *this;
  }
};


typedef std::unordered_map<graphicid, TileGfxInfo *> TMapGrxID;
typedef std::unordered_map<GfxUID, TileGfxInfo *> TMapUID;


struct graphicdata {
public:
  int AnimationFrames;
  bitmap **Picture; // [AnimationFrames]
  GfxUID *GraphicIterator; // [AnimationFrames]

public:
  FORCE_INLINE graphicdata () : AnimationFrames(0), Picture(0), GraphicIterator(0) {}
  ~graphicdata ();
  void Save (outputfile &) const;
  void Load (inputfile &);
  void Retire ();
};

outputfile &operator << (outputfile &, const graphicdata &);
inputfile &operator >> (inputfile &, graphicdata &);


class igraph {
public:
  virtual ~igraph () {}

  static void Init ();
  static void DeInit ();

  static void WipeTiles ();

  static cbitmap *GetWTerrainGraphic () { return Graphic[GR_WTERRAIN]; }
  static cbitmap *GetFOWGraphic () { return Graphic[GR_FOW]; }
  static const rawbitmap *GetCursorRawGraphic () { return RawGraphic[GR_CURSOR]; }
  static cbitmap *GetSymbolGraphic () { return Graphic[GR_SYMBOL]; }
  static cbitmap *GetHumanoidGraphic () { return Graphic[GR_HUMANOID]; }
  static bitmap *GetTileBuffer () { return TileBuffer; }
  static void DrawCursor (v2 Pos, int CursorData, int Index=0);
  static GfxUID AddUserEx (const graphicid &GI, TileGfxInfo *&gi);
  static GfxUID AddUser (const graphicid &GI) { TileGfxInfo *gi; return AddUserEx(GI, gi); }
  static void RemoveUser (GfxUID);
  static const rawbitmap *GetHumanoidRawGraphic () { return RawGraphic[GR_HUMANOID]; }
  static const rawbitmap *GetCharacterRawGraphic () { return RawGraphic[GR_CHARACTER]; }
  static const rawbitmap *GetEffectRawGraphic () { return RawGraphic[GR_EFFECT]; }
  static const rawbitmap *GetRawGraphic (int I) { return RawGraphic[I]; }
  static cint *GetBodyBitmapValidityMap (int);
  static bitmap *GetFlagBuffer () { return FlagBuffer; }
  static cbitmap *GetMenuGraphic () { return Menu; }
  static void LoadMenuData ();
  static void UnLoadMenu ();
  static bitmap *GetSilhouetteCache (int I1, int I2, int I3) { return SilhouetteCache[I1][I2][I3]; }
  static cbitmap *GetBackGround () { return BackGround; }
  static void BlitBackGround (v2 Pos, v2 Border);
  static void CreateBackGround(int);
  static bitmap *GenerateScarBitmap (int, int, int);
  static cbitmap *GetSmileyGraphic () { return Graphic[GR_SMILEY]; }

  static TileGfxInfo *FindUserByUID (GfxUID uid);

private:
  static void EditBodyPartTile (rawbitmap *Source, rawbitmap *Dest, v2 Pos, int BodyPartFlags);
  static v2 RotateTile (rawbitmap *Source, rawbitmap *Dest, v2 Pos, v2 SparklePos, int RotateFlags);
  static void CreateBodyBitmapValidityMaps ();
  static void CreateSilhouetteCaches ();
  static col16 GetBackGroundColor (int Element);

private:
  static rawbitmap *RawGraphic[RAW_TYPES];
  static bitmap *Graphic[GRAPHIC_TYPES];
  static bitmap *TileBuffer;
  static cchar *RawGraphicFileName[];
  static cchar *GraphicFileName[];
  static uChar RollBuffer[256];
  static bitmap *FlagBuffer;
  static int **BodyBitmapValidityMap;
  static bitmap *Menu;
  static bitmap *SilhouetteCache[HUMANOID_BODYPARTS][CONDITION_COLORS][SILHOUETTE_TYPES];
  static rawbitmap *ColorizeBuffer[2];
  static bitmap *Cursor[CURSOR_TYPES];
  static bitmap *BigCursor[CURSOR_TYPES];
  static col16 CursorColor[CURSOR_TYPES];
  static bitmap *BackGround;
  static int CurrentColorType;

  static TMapGrxID tilesGrx;
  static TMapUID tilesUID;
  static GfxUID lastUID;

private:
  static GfxUID allocUID ();
  static GfxUID AppendNewTile (const graphicid &GI, bitmap *bmp, TileGfxInfo *&gi);
};

#endif
