/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through itemset.cpp */

static cchar *ToHitValueDescription[] = {
  "unbelievably inaccurate",
  "extremely inaccurate",
  "inaccurate",
  "decently accurate",
  "accurate",
  "highly accurate",
  "extremely accurate",
  "unbelievably accurate"
};

static cchar *StrengthValueDescription[] = {
  "fragile",
  "rather sturdy",
  "sturdy",
  "strong",
  "very strong",
  "extremely strong",
  "almost unbreakable"
};


// ////////////////////////////////////////////////////////////////////////// //
// itemdatabase
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  itemdatabase::AllowRandomInstantiation
//
//==========================================================================
truth itemdatabase::AllowRandomInstantiation () const {
  return !(Config & S_LOCK_ID);
}


//==========================================================================
//
//  itemdatabase::InitDefaults
//
//==========================================================================
void itemdatabase::InitDefaults (const itemprototype *NewProtoType, int NewConfig,
                                 cfestring &acfgstrname, const itemdatabase *aParentDB)
{
  IsAbstract = false;
  ProtoType = NewProtoType;
  Config = NewConfig;
  CfgStrName = acfgstrname;
  CopyFieldInfoFrom(aParentDB);

  if (NewConfig & BROKEN) {
    if (Adjective.GetSize()) {
      Adjective.Insert(0, "broken ");
    } else {
      Adjective = CONST_S("broken");
    }
    DefaultSize >>= 1;
    FormModifier >>= 2;
    StrengthModifier >>= 1;
  }
}


//==========================================================================
//
//  itemdatabase::PostProcess
//
//==========================================================================
void itemdatabase::PostProcess (const TextFileLocation &loc) {
  if (!IsAbstract) {
    if (MaterialConfigChances.GetSize() != 0) {
      if (/*MainMaterialConfig.GetSize() != 0 &&*/
          MainMaterialConfig.GetSize() != 1 &&
          MainMaterialConfig.GetSize() < MaterialConfigChances.GetSize())
      {
        ABORT("%s:%d: item '%s' config #%d%s has invalid main material define (%u : %u)",
              loc.fname.CStr(), loc.line,
              ProtoType->GetClassID(), (Config & 0x7f), (Config & BROKEN ? " [broken]" : ""),
              (unsigned)MainMaterialConfig.GetSize(),
              (unsigned)MaterialConfigChances.GetSize());
      }
      if (SecondaryMaterialConfig.GetSize() != 0 &&
          SecondaryMaterialConfig.GetSize() != 1 &&
          SecondaryMaterialConfig.GetSize() < MaterialConfigChances.GetSize())
      {
        ABORT("%s:%d: item '%s' config #%d%s has invalid secondary material define (%u : %u)",
              loc.fname.CStr(), loc.line,
              ProtoType->GetClassID(), (Config & 0x7f), (Config & BROKEN ? " [broken]" : ""),
              (unsigned)SecondaryMaterialConfig.GetSize(),
              (unsigned)MaterialConfigChances.GetSize());
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// itemprototype
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  itemprototype::itemprototype
//
//==========================================================================
itemprototype::itemprototype (const itemprototype *Base, itemspawner Spawner,
                              itemcloner Cloner, cchar *ClassID)
  : Base(Base)
  , Spawner(Spawner)
  , Cloner(Cloner)
  , ClassID(ClassID)
{
  Index = protocontainer<item>::Add(this);
}


//==========================================================================
//
//  itemprototype::SpawnAndLoad
//
//==========================================================================
item *itemprototype::SpawnAndLoad (inputfile &SaveFile) const {
  item *Item = Spawner(0, LOAD);
  Item->Load(SaveFile);
  Item->CalculateAll();
  return Item;
}


//==========================================================================
//
//  itemprototype::ChooseBaseForConfig
//
//==========================================================================
const itemdatabase *itemprototype::ChooseBaseForConfig (itemdatabase **TempConfig, int Configs,
                                                        int ConfigNumber)
{
  if (!(ConfigNumber & BROKEN)) {
    return *TempConfig;
  } else {
    ConfigNumber ^= BROKEN;
    for (int c = 0; c < Configs; ++c) {
      if (TempConfig[c]->Config == ConfigNumber) return TempConfig[c];
    }
    return *TempConfig;
  }
}


//==========================================================================
//
//  itemprototype::CreateSpecialConfigurations
//
//==========================================================================
int itemprototype::CreateSpecialConfigurations (itemdatabase **TempConfig, int Configs,
                                                int Level)
{
  if (Level) return Configs;

  if (TempConfig[0]->CreateDivineConfigurations) {
    Configs = databasecreator<item>::CreateDivineConfigurations(this, TempConfig, Configs);
  }

  /* Gum solution */
  if (TempConfig[0]->CreateLockConfigurations) {
    const item::database*const* KeyConfigData = key::ProtoType.GetConfigData();
    int KeyConfigSize = key::ProtoType.GetConfigSize();
    int OldConfigs = Configs;
    for (int c1 = 0; c1 < OldConfigs; ++c1) {
      if (!TempConfig[c1]->IsAbstract) {
        int BaseConfig = TempConfig[c1]->Config;
        int NewConfig = BaseConfig | BROKEN_LOCK;
        itemdatabase* ConfigDataBase = new itemdatabase(*TempConfig[c1]);
        festring lcfgname = CONST_S("locked-broken ");
        lcfgname << TempConfig[c1]->CfgStrName;

        ConfigDataBase->InitDefaults(this, NewConfig, lcfgname, TempConfig[c1]);
        ConfigDataBase->PostFix << "with a broken lock";
        ConfigDataBase->Possibility = 0;
        TempConfig[Configs++] = ConfigDataBase;

        for (int c2 = 0; c2 < KeyConfigSize; ++c2) {
          festring xcfgname = CONST_S("locked(");
          xcfgname << KeyConfigData[c2]->CfgStrName;
          xcfgname << ":";
          xcfgname << TempConfig[c1]->CfgStrName;
          xcfgname << ")";
          NewConfig = BaseConfig|KeyConfigData[c2]->Config;
          //ConLogf("LOCKED %d [%s] <%s>", NewConfig, xcfgname.CStr(), this->GetClassID());
          ConfigDataBase = new itemdatabase(*TempConfig[c1]);
          ConfigDataBase->InitDefaults(this, NewConfig, xcfgname, TempConfig[c1]);
          ConfigDataBase->PostFix << "with ";
          if (KeyConfigData[c2]->UsesLongAdjectiveArticle) {
            ConfigDataBase->PostFix << "an ";
          } else {
            ConfigDataBase->PostFix << "a ";
          }
          ConfigDataBase->PostFix << KeyConfigData[c2]->Adjective << " lock";
          ConfigDataBase->Possibility = 0;
          TempConfig[Configs++] = ConfigDataBase;
        }
      }
    }
  }

  return Configs;
}


// ////////////////////////////////////////////////////////////////////////// //
// item
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  item::item
//
//==========================================================================
item::item ()
  : object()
  , Slot(0)
  , CloneMotherID(0)
  , Fluid(0)
  , LifeExpectancy(0)
  , ItemFlags(0)
  , mIsStepedOn(false)
  , DisableFluids(false)
  , pickupTime(0)
{
}


//==========================================================================
//
//  item::item
//
//==========================================================================
item::item (citem &Item)
  : object(Item)
  , Slot(0)
  , Size(Item.Size)
  , DataBase(Item.DataBase)
  , Volume(Item.Volume)
  , Weight(Item.Weight)
  , Fluid(0)
  , FluidCount(0)
  , SquaresUnder(Item.SquaresUnder)
  , LifeExpectancy(Item.LifeExpectancy)
  , ItemFlags(Item.ItemFlags)
  , mIsStepedOn(false)
  , DisableFluids(false)
  , pickupTime(0)
{
  Flags &= ENTITY_FLAGS|SQUARE_POSITION_BITS;
  ID = game::CreateNewItemID(this);
  CloneMotherID = new idholder(Item.ID);
  idholder *TI = CloneMotherID;

  for (idholder *II = Item.CloneMotherID; II; II = II->Next) {
    TI = TI->Next = new idholder(II->ID);
  }
  TI->Next = 0;

  Slot = new slot*[SquaresUnder];
  for (int c = 0; c < SquaresUnder; ++c) {
    Slot[c] = 0;
  }
}


//==========================================================================
//
//  item::~item
//
//==========================================================================
item::~item () {
  delete [] Slot;
  game::RemoveItemNote(this);
  game::RemoveItemID(ID);
  fluid **FP = Fluid;
  if (FP) {
    for (int c = 0; c < /*SquaresUnder*/FluidCount; ++c) {
      deleteList(FP[c]);
    }
    delete [] FP;
  }
  deleteList(CloneMotherID);
}


truth item::IsOnGround () const { return Slot[0]->IsOnGround(); }
truth item::IsSimiliarTo (item *Item) const { return Item->GetType() == GetType() && Item->GetConfig() == GetConfig(); }
double item::GetBaseDamage () const { return Max(0.0, sqrt(5e-5*GetWeaponStrength())+GetDamageBonus()); }
int item::GetBaseMinDamage () const { return int(GetBaseDamage()*0.75); }
int item::GetBaseMaxDamage () const { return int(GetBaseDamage()*1.25)+1; }
int item::GetBaseToHitValue () const { return int(10000.0/(1000+GetWeight())+GetTHVBonus()); }
int item::GetBaseBlockValue () const { return int((10000.0/(1000+GetWeight())+GetTHVBonus())*GetBlockModifier()/10000.0); }
truth item::IsInCorrectSlot (int I) const { return I == RIGHT_WIELDED_INDEX || I == LEFT_WIELDED_INDEX; }
truth item::IsInCorrectSlot () const { return IsInCorrectSlot(static_cast<gearslot *>(*Slot)->GetEquipmentIndex()); }
int item::GetEquipmentIndex () const { return static_cast<gearslot *>(*Slot)->GetEquipmentIndex(); }
int item::GetGraphicsContainerIndex () const { return GR_ITEM; }
truth item::IsBroken () const { return !!(GetConfig () & BROKEN); }
truth item::IsFood() const { return !!(DataBase->Category & FOOD); }
cchar *item::GetBreakVerb () const { return "breaks"; }
square *item::GetSquareUnderEntity (int I) const { return GetSquareUnder(I); }
square *item::GetSquareUnder (int I) const { return Slot[I] ? Slot[I]->GetSquareUnder() : 0; }
lsquare *item::GetLSquareUnder (int I) const { return static_cast<lsquare *>(Slot[I]->GetSquareUnder()); }
void item::SignalStackAdd (stackslot *StackSlot, void (stack::*)(item*, truth)) { Slot[0] = StackSlot; }
truth item::IsAnimated () const { return GraphicData.AnimationFrames > 1 || (Fluid && ShowFluids()); }
truth item::IsRusted () const { return (MainMaterial && MainMaterial->GetRustLevel () != NOT_RUSTED); }
truth item::IsEatable (ccharacter *Eater) const { return GetConsumeMaterial(Eater, &material::IsSolid) && IsConsumable(); }
truth item::IsDrinkable (ccharacter *Eater) const { return GetConsumeMaterial(Eater, &material::IsLiquid) && IsConsumable(); }
pixelpredicate item::GetFluidPixelAllowedPredicate () const { return &rawbitmap::IsTransparent; }
void item::Cannibalize () { Flags |= CANNIBALIZED; }
void item::SetMainMaterial (material *NewMaterial, int SpecialFlags) { SetMaterial(MainMaterial, NewMaterial, GetDefaultMainVolume(), SpecialFlags); }
void item::ChangeMainMaterial (material *NewMaterial, int SpecialFlags) { ChangeMaterial(MainMaterial, NewMaterial, GetDefaultMainVolume(), SpecialFlags); }
void item::InitMaterials (const materialscript *M, const materialscript *, truth CUP) { InitMaterials(M->Instantiate(), CUP); }
int item::GetMainMaterialRustLevel () const { return (MainMaterial ? MainMaterial->GetRustLevel() : 0); }
stack *item::GetContained() const { return nullptr; }


//==========================================================================
//
//  item::GetConfigName
//
//==========================================================================
festring item::GetConfigName () const {
  if (DataBase) {
    return DataBase->CfgStrName;
  }
  /*
  if (GetProtoType() &&
      GetProtoType()->GetConfigData() &&
      GetConfig() < GetProtoType()->GetConfigSize() &&
      GetProtoType()->GetConfigData()[GetConfig()])
  {
    return GetProtoType()->GetConfigData()[GetConfig()]->CfgStrName;
  } else
  */
  {
    festring res;
    res << "WTF<" << GetConfig() << ">";
    return res;
  }
}


//==========================================================================
//
//  item::DebugLogNameCfg
//
//==========================================================================
festring item::DebugLogNameCfg () const {
  festring res;
  res << GetClassID() << GetConfigName();
  return res;
}


//==========================================================================
//
//  item::DebugGetName
//
//==========================================================================
festring item::DebugGetName () {
  festring res;
  res << "item:";
  AddName(res, DEFINITE);
  return res;
}


//==========================================================================
//
//  item::Fly
//
//==========================================================================
void item::Fly (character *Thrower, int Direction, int Force) {
  int Range = Force * 25 / Max(sLong(sqrt(GetWeight())), 1);
  lsquare *LSquareUnder = GetLSquareUnder();
  RemoveFromSlot();
  LSquareUnder->GetStack()->AddItem(this, false);
  if (!Range || GetSquaresUnder() != 1) {
    if (GetLSquareUnder()->GetRoom()) GetLSquareUnder()->GetRoom()->AddItemEffect(this);
    return;
  }
  if (Direction == RANDOM_DIR) Direction = RAND_8;
  v2 StartingPos = GetPos();
  v2 Pos = StartingPos;
  v2 DirVector = game::GetMoveVector(Direction);
  truth Breaks = false;
  double BaseDamage, BaseToHitValue;

  /*** check ***/
  if (Thrower) {
    int Bonus = (Thrower->IsHumanoid() ? Thrower->GetCWeaponSkill(GetWeaponCategory())->GetBonus() : 1000);
    BaseDamage = sqrt(5e-12 * GetWeaponStrength() * Force / Range) * Bonus;
    BaseToHitValue = 10 * Bonus * Thrower->GetMoveEase() / (500 + GetWeight()) *
          Thrower->GetAttribute(DEXTERITY) * sqrt(2.5e-8 * Thrower->GetAttribute(PERCEPTION)) / Range;
  } else {
    BaseDamage = sqrt(5e-6 * GetWeaponStrength() * Force / Range);
    BaseToHitValue = 10 * 100 / (500 + GetWeight()) / Range;
  }

  int RangeLeft;
  for (RangeLeft = Range; RangeLeft; --RangeLeft) {
    if (!GetLevel()->IsValidPos(Pos+DirVector)) break;
    lsquare *JustHit = GetNearLSquare(Pos+DirVector);
    if (!JustHit->IsFlyable()) {
      Breaks = true;
      JustHit->GetOLTerrain()->HasBeenHitByItem(Thrower, this, int(BaseDamage*sqrt(RangeLeft)));
      break;
    } else {
      clock_t StartTime = clock();
      Pos += DirVector;
      RemoveFromSlot();
      JustHit->GetStack()->AddItem(this, false);
      truth Draw = game::OnScreen(JustHit->GetPos()) && JustHit->CanBeSeenByPlayer();
      if (Draw) game::DrawEverything();
      if (JustHit->GetCharacter()) {
        int Damage = int(BaseDamage * sqrt(RangeLeft));
        double ToHitValue = BaseToHitValue*RangeLeft;
        int Returned = HitCharacter(Thrower, JustHit->GetCharacter(), Damage, ToHitValue, Direction);
        if (Returned == HIT) Breaks = true;
        if (Returned != MISSED) break;
      }
      if (Draw) { while (clock()-StartTime < 0.03 * CLOCKS_PER_SEC) {} } //FIXME
    }
  }

  if (Breaks) {
    ReceiveDamage(Thrower, int(sqrt(GetWeight()*RangeLeft) / 10), THROW|PHYSICAL_DAMAGE, Direction);
  }

  if (Exists() && GetLSquareUnder()->GetRoom()) {
    GetLSquareUnder()->GetRoom()->AddItemEffect(this);
  }
}


//==========================================================================
//
//  item::HitCharacter
//
//==========================================================================
int item::HitCharacter (character *Thrower, character *Dude, int Damage, double ToHitValue,
                        int Direction)
{
  if (Dude->Catches(this)) return CATCHED;
  if (Thrower && !EffectIsGood()) Thrower->Hostility(Dude);
  if (Dude->DodgesFlyingItem(this, ToHitValue)) {
    if (Dude->IsPlayer()) {
      ADD_MESSAGE("%s misses you.", CHAR_NAME(DEFINITE));
    } else if (Dude->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s misses %s.", CHAR_NAME(DEFINITE), Dude->CHAR_NAME(DEFINITE));
    }
    return MISSED;
  }
  if (HitByTrap(Thrower, Dude)) return HIT; //k8: for flying activated beartraps
  Dude->HasBeenHitByItem(Thrower, this, Damage, ToHitValue, Direction);
  return HIT;
}


//==========================================================================
//
//  item::GetWeaponStrength
//
//==========================================================================
double item::GetWeaponStrength () const {
  return GetFormModifier() * GetMainMaterial()->GetStrengthValue() * sqrt(GetMainMaterial()->GetWeight());
}


//==========================================================================
//
//  item::GetStrengthRequirement
//
//==========================================================================
int item::GetStrengthRequirement () const {
  double WeightTimesSize = GetWeight() * GetSize();
  return int(1.25e-10 * WeightTimesSize * WeightTimesSize);
}


//==========================================================================
//
//  item::Apply
//
//==========================================================================
truth item::Apply (character *Applier) {
  if (Applier->IsPlayer()) ADD_MESSAGE("You can't apply this!");
  return false;
}


//==========================================================================
//
//  item::Polymorph
//
//  Returns truth that tells whether the Polymorph really happened
//
//==========================================================================
truth item::Polymorph (character *Polymorpher, stack *CurrentStack) {
  if (!IsPolymorphable()) return false;
  if (Polymorpher && IsOnGround()) {
    room *Room = GetRoom();
    if (Room) Room->HostileAction(Polymorpher);
  }
  if (GetSquarePosition() != CENTER) {
    stack *Stack = CurrentStack->GetLSquareUnder()->GetStackOfAdjacentSquare(GetSquarePosition());
    if (Stack) CurrentStack = Stack;
  }
  CurrentStack->AddItem(protosystem::BalancedCreateItem(0, 0, MAX_PRICE, ANY_CATEGORY, 0, 0, 0, true));
  RemoveFromSlot();
  SendToHell();
  return true;
}


//==========================================================================
//
//  item::Polymorph
//
//  from comm. fork
//
//==========================================================================
truth item::Polymorph (character *Polymorpher, character *Wielder) {
  if (!IsPolymorphable()) return false;
  if (!Wielder->Equips(this)) return false;

  if (Polymorpher && Wielder) {
    Polymorpher->Hostility(Wielder);
  }

  #if 0
  ConLogf("item polymorph: item is `%s` (%s : %s)", CHAR_NAME(UNARTICLED),
          GetClassID(), GetConfigName().CStr());
  #endif

  item *NewItem = protosystem::BalancedCreateItem(
                    0/*level*/,
                    0, MAX_PRICE,
                    ANY_CATEGORY,
                    0/*SpecialFlags*/,
                    0/*ConfigFlags*/,
                    0/*RequiredGod*/,
                    true/*Polymorph*/);
  int EquipSlot = GetEquipmentIndex();

  if (Wielder->IsPlayer()) {
    ADD_MESSAGE("Your %s polymorphs into %s.", CHAR_NAME(UNARTICLED),
                NewItem->CHAR_NAME(INDEFINITE));
  } else if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s's %s polymorphs into %s.", Wielder->CHAR_NAME(DEFINITE),
                CHAR_NAME(UNARTICLED), NewItem->CHAR_NAME(INDEFINITE));
  }

  RemoveFromSlot();
  SendToHell();

  switch (EquipSlot) {
    /*
    case HELMET_INDEX: Wielder->SetHelmet(NewItem); break;
    case AMULET_INDEX: Wielder->SetAmulet(NewItem); break;
    case CLOAK_INDEX: Wielder->SetCloak(NewItem); break;
    case BODY_ARMOR_INDEX: Wielder->SetBodyArmor(NewItem); break;
    case BELT_INDEX: Wielder->SetBelt(NewItem); break;
    */
    case RIGHT_WIELDED_INDEX: Wielder->SetRightWielded(NewItem); break;
    case LEFT_WIELDED_INDEX: Wielder->SetLeftWielded(NewItem); break;
    /*
    case RIGHT_RING_INDEX: Wielder->SetRightRing(NewItem); break;
    case LEFT_RING_INDEX: Wielder->SetLeftRing(NewItem); break;
    case RIGHT_GAUNTLET_INDEX: Wielder->SetRightGauntlet(NewItem); break;
    case LEFT_GAUNTLET_INDEX: Wielder->SetLeftGauntlet(NewItem); break;
    case RIGHT_BOOT_INDEX: Wielder->SetRightBoot(NewItem); break;
    case LEFT_BOOT_INDEX: Wielder->SetLeftBoot(NewItem); break;
    */
    default: Wielder->ReceiveItemAsPresent(NewItem); break;
  }

  if (Wielder->IsPlayer()) {
    festring msg = ProcessMessage(CONST_S("!F:@Hsp @nu is polymorphed!"), PLAYER);
    game::AskForEscPress(msg);
    //game::AskForEscPress(CONST_S("Equipment polymorphed! (") + CHAR_NAME(DEFINITE) + ")");
  }

  return true;
}


//==========================================================================
//
//  item::Alchemize
//
//  Returns truth that tells whether the alchemical conversion really happened.
//
//==========================================================================
truth item::Alchemize (character *Midas, stack *CurrentStack) {
  if (IsQuestItem()) {
    return false;
  }

  // why? alchemizing items in underground shops should not be allowed too!
  if (Midas /*&& IsOnGround()*/) {
    room *Room = GetRoom();
    if (Room) Room->HostileAction(Midas);
  }

  sLong Price = GetTruePrice();

  if (Price) {
    if (Midas) {
      Price /= 4; /* slightly lower than with 10 Cha */
      if (Midas->IsPlayer()) {
        ADD_MESSAGE("The Bank Corporation adds %dgp to your account.", Price);
      } else if (Midas->CanBeSeenByPlayer()) {
        ADD_MESSAGE("The Bank Corporation adds some sum to %s account.",
                    Midas->CHAR_NAME(INDEFINITE));
      }
      Midas->EditMoney(Price);
    }
  }

  RemoveFromSlot();
  SendToHell();
  return true;
}


//==========================================================================
//
//  item::SoftenMaterial
//
//==========================================================================
truth item::SoftenMaterial () {
  if (!IsMaterialChangeable() || !CanBeSoftened()) {
    return false;
  }

  int Config = GetMainMaterial()->GetSoftenedMaterial(this);

  if (!Config) {
    /* Should not be possible. */
    return false;
  }

  msgsystem::EnterBigMessageMode();

  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("Suddenly %s starts glowing dull yellow.", CHAR_NAME(INDEFINITE));
  }

  material *TempMaterial = MAKE_MATERIAL(Config);
  material *MainMaterial = GetMainMaterial();
  material *SecondaryMaterial = GetSecondaryMaterial();

  if (SecondaryMaterial && SecondaryMaterial->IsSameAs(MainMaterial)) {
    SetSecondaryMaterial(TempMaterial->SpawnMore());
  }

  SetMainMaterial(TempMaterial);

  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("It softens into %s!", GetMainMaterial()->GetName(false, false).CStr());
  }

  msgsystem::LeaveBigMessageMode();

  /*
  SignalVolumeAndWeightChange();
  CalculateEmitation(); // dunno; might be needed, might be not
  UpdatePictures();
  */
  SendNewDrawAndMemorizedUpdateRequest();

  return true;
}


//==========================================================================
//
//  item::Consume
//
//  Returns whether the Eater must stop eating the item
//
//==========================================================================
truth item::Consume (character *Eater, sLong Amount) {
  material *ConsumeMaterial = GetConsumeMaterial(Eater);
  if (!ConsumeMaterial) return true;

  if (Eater->IsPlayer() && !(Flags & CANNIBALIZED) && Eater->CheckCannibalism(ConsumeMaterial)) {
    game::DoEvilDeed(25);
    ADD_MESSAGE("You feel that this was an evil deed.");
    Cannibalize();
  }

  feuLong ID = GetID();
  material *Garbage = ConsumeMaterial->EatEffect(Eater, Amount);
  item *NewConsuming = GetID() ? this : game::SearchItem(ID);
  material *NewConsumeMaterial = NewConsuming->GetConsumeMaterial(Eater);

  if (!NewConsuming->Exists() || !NewConsumeMaterial || !NewConsumeMaterial->IsSameAs(ConsumeMaterial)) {
    ConsumeMaterial->FinishConsuming(Eater);
  }

  delete Garbage;

  return !NewConsuming->Exists() || !NewConsumeMaterial;
}


//==========================================================================
//
//  item::CanBeEatenByAI
//
//==========================================================================
truth item::CanBeEatenByAI (ccharacter *Eater) const {
  // do not allow AI to consume potions.
  // `CheckDrink()` doesn't use this method, so it is safe.
  // we need to disable potions so AI will not try to go to them
  // only to find that they are not edible.
  if (IsQuestItem() || IsBottle() || IsCan()) return false;
  if (!IsConsumable()) return false;
  material *ConsumeMaterial = GetConsumeMaterial(Eater);
  if (!ConsumeMaterial) return false;
  if (IsValuable() && Eater->IsPet() &&
      (Eater->GetCommandFlags() & DONT_CONSUME_ANYTHING_VALUABLE))
  {
    return false;
  }
  return ConsumeMaterial->CanBeEatenByAI(Eater);
}


//==========================================================================
//
//  item::Save
//
//==========================================================================
void item::Save (outputfile &SaveFile) const {
  SaveFile << DataBase->CfgStrName;

  int acfgid = databasecreator<item>::FindConfigByName(FindProtoType(), DataBase->CfgStrName);
  if (acfgid != GetConfig()) {
    ConLogf("Fucked config %d (%d) in '%s [%s]' (%s)!",
            GetConfig(), acfgid, GetTypeID(), FindProtoType()->GetClassID(),
            DataBase->CfgStrName.CStr());
  }

  object::Save(SaveFile);
  SaveFile << (uShort)0;
  SaveFile << mIsStepedOn;
  //SaveFile << (uShort)GetConfig();
  SaveFile << (uShort)Flags;
  SaveFile << Size << ID << LifeExpectancy << ItemFlags;
  SaveFile << pickupTime;
  SaveLinkedList(SaveFile, CloneMotherID);
  if (Fluid) {
    SaveFile.Put(true);
    SaveFile << FluidCount;
    for (int c = 0; c < /*SquaresUnder*/FluidCount; ++c) SaveLinkedList(SaveFile, Fluid[c]);
  } else {
    SaveFile.Put(false);
  }
}


//==========================================================================
//
//  item::Load
//
//==========================================================================
void item::Load (inputfile &SaveFile) {
  festring acfgname;
  SaveFile >> acfgname;
  int acfgid = databasecreator<item>::FindConfigByName(FindProtoType(), acfgname);
  if (acfgid == -1) ABORT("Cannot find '%s' config '%s'!", FindProtoType()->GetClassID(), acfgname.CStr());

  object::Load(SaveFile);
  int ver = ReadType(uShort, SaveFile);
  if (ver != 0) ABORT("invalid item version in savefile: %d", ver);
  SaveFile >> mIsStepedOn;

  //databasecreator<item>::InstallDataBase(this, ReadType(uShort, SaveFile));
  databasecreator<item>::InstallDataBase(this, acfgid);

  Flags |= ReadType(uShort, SaveFile) & ~ENTITY_FLAGS;
  SaveFile >> Size >> ID >> LifeExpectancy >> ItemFlags;
  SaveFile >> pickupTime;
  LoadLinkedList(SaveFile, CloneMotherID);
  if (LifeExpectancy) Enable();
  game::AddItemID(this, ID);
  if (SaveFile.Get()) {
    SaveFile >> FluidCount;
    Fluid = new fluid*[/*SquaresUnder*/FluidCount];
    for (int c = 0; c < /*SquaresUnder*/FluidCount; ++c) {
      LoadLinkedList(SaveFile, Fluid[c]);
      for (fluid *F = Fluid[c]; F; F = F->Next) F->SetMotherItem(this);
    }
  } else {
    if (Fluid) Fluid = 0; //FIXME: MEMORY LEAK
    FluidCount = 0;
  }
}


//==========================================================================
//
//  item::TeleportRandomly
//
//==========================================================================
void item::TeleportRandomly () {
  if (GetSquaresUnder() == 1) {
    // gum solution
    v2 pos = GetLevel()->GetRandomSquare();
    if (pos == ERROR_V2) {
      ConLogf("oops: `item::TeleportRandomly()` failed! possible memleak!");
      return;
    }
    lsquare *Square = GetNearLSquare(pos);
    MoveTo(Square->GetStack());
    if (Square->CanBeSeenByPlayer()) ADD_MESSAGE("Suddenly %s appears!", CHAR_NAME(INDEFINITE));
  }
}


//==========================================================================
//
//  item::GetStrengthValue
//
//==========================================================================
int item::GetStrengthValue () const {
  return sLong(GetStrengthModifier()) * GetMainMaterial()->GetStrengthValue() / 2000;
}


//==========================================================================
//
//  item::RemoveFromSlot
//
//==========================================================================
void item::RemoveFromSlot () {
  for (int c = 0; c < SquaresUnder; ++c) {
    if (Slot[c]) {
      try {
        Slot[c]->Empty();
      } catch (quitrequest) {
        SendToHell();
        throw;
      }
      Slot[c] = 0;
    }
  }
}


//==========================================================================
//
//  item::MoveTo
//
//==========================================================================
void item::MoveTo (stack *Stack, truth setPickupTime) {
  RemoveFromSlot();
  Stack->AddItem(this);
  if (setPickupTime) {
    if (setPickupTime > 0 || pickupTime == 0) pickupTime = game::GetTick();
    //ConLogf("item '%s'; pickuptime set to %u", GetNameSingular().CStr(), pickupTime);
  }
}


//==========================================================================
//
//  item::GetItemCategoryName
//
//==========================================================================
cchar *item::GetItemCategoryName (sLong Category) {
  // convert to array
  if (Category == HELMET) return "Helmets";
  if (Category == AMULET) return "Amulets";
  if (Category == CLOAK) return "Cloaks";
  if (Category == BODY_ARMOR) return "Body armors";
  if (Category == WEAPON) return "Weapons";
  if (Category == SHIELD) return "Shields";
  if (Category == RING) return "Rings";
  if (Category == GAUNTLET) return "Gauntlets";
  if (Category == BELT) return "Belts";
  if (Category == BOOT) return "Boots";
  if (Category == FOOD) return "Food";
  if (Category == POTION) return "Potions";
  if (Category == SCROLL) return "Scrolls";
  if (Category == BOOK) return "Books";
  if (Category == WAND) return "Wands";
  if (Category == TOOL) return "Tools";
  if (Category == VALUABLE) return "Valuables";
  if (Category == MISC) return "Miscellaneous items";
  return "Warezzz";
}


//==========================================================================
//
//  item::GetResistance
//
//==========================================================================
int item::GetResistance (int Type) const {
  const int ddt = Type&0xFFF;
  if (ddt == PHYSICAL_DAMAGE) return GetStrengthValue();
  if (ddt == ENERGY) return GetEnergyResistance();
  if (ddt == DRAIN || ddt == MUSTARD_GAS_DAMAGE || ddt == PSI) return 0;
  if (ddt == FIRE) return GetFireResistance();
  if (ddt == POISON) return GetPoisonResistance();
  if (ddt == ELECTRICITY) return GetElectricityResistance();
  if (ddt == ACID) return GetAcidResistance();
  if (ddt == SOUND) return GetSoundResistance();
  ABORT("Resistance lack detected! (damage type is %d [masked:%d])", Type, ddt);
  return 0;
}


//==========================================================================
//
//  item::Open
//
//==========================================================================
truth item::Open (character *Char) {
  if (Char->IsPlayer()) ADD_MESSAGE("You can't open %s.", CHAR_NAME(DEFINITE));
  return false;
}


//==========================================================================
//
//  item::LoadDataBaseStats
//
//==========================================================================
void item::LoadDataBaseStats () {
  SetSize(GetDefaultSize());
}


//==========================================================================
//
//  item::Initialize
//
//==========================================================================
void item::Initialize (int NewConfig, int SpecialFlags) {
  CalculateSquaresUnder();
  Slot = new slot*[SquaresUnder];
  for (int c = 0; c < SquaresUnder; ++c) Slot[c] = 0;
  if (!(SpecialFlags & LOAD)) {
    ID = game::CreateNewItemID(this);
    databasecreator<item>::InstallDataBase(this, NewConfig);
    LoadDataBaseStats();
    RandomizeVisualEffects();
    Flags |= CENTER << SQUARE_POSITION_SHIFT;
    if (!(SpecialFlags & NO_MATERIALS)) GenerateMaterials();
  }
  if (!(SpecialFlags & LOAD)) PostConstruct();
  if (!(SpecialFlags & (LOAD|NO_MATERIALS))) {
    CalculateAll();
    if (!(SpecialFlags & NO_PIC_UPDATE)) UpdatePictures();
  }
}


//==========================================================================
//
//  item::ShowMaterial
//
//==========================================================================
truth item::ShowMaterial () const {
  if (!GetMainMaterial()) return false; // for debug
  if (GetMainMaterialConfig().Size == 1) {
    return GetMainMaterial()->GetConfig() != GetMainMaterialConfig()[0];
  }
  //FIXME: gum solution
  if (GetNameSingular() == "bone") {
    // never show the material for 'bone bone'
    if (GetMainMaterial()->GetConfig() == BONE) return false;
  }
  return true;
}


//==========================================================================
//
//  item::GetBlockModifier
//
//==========================================================================
sLong item::GetBlockModifier () const {
  if (!IsShield(0)) {
    // k8: frying pan is a great shield!
    if (GetConfig() != FRYING_PAN) {
      return GetSize() * GetRoundness() << 1;
    }
  }
  return GetSize() * GetRoundness() << 2;
}


//==========================================================================
//
//  item::CanBeSeenByPlayer
//
//==========================================================================
truth item::CanBeSeenByPlayer () const {
  return CanBeSeenBy(PLAYER);
}


//==========================================================================
//
//  item::CanBeSeenBy
//
//==========================================================================
truth item::CanBeSeenBy (ccharacter *Who) const {
  for (int c = 0; c < SquaresUnder; ++c) {
    if (Slot[c] && Slot[c]->CanBeSeenBy(Who)) {
      return true;
    }
  }
  return Who->IsPlayer() && game::GetSeeWholeMapCheatMode();
}


//==========================================================================
//
//  item::GetDescription
//
//==========================================================================
festring item::GetDescription (int Case) const {
  if (CanBeSeenByPlayer()) return GetName(Case);
  return CONST_S("something");
}


//==========================================================================
//
//  item::SignalVolumeAndWeightChange
//
//==========================================================================
void item::SignalVolumeAndWeightChange () {
  CalculateVolumeAndWeight();
  for (int c = 0; c < SquaresUnder; ++c) {
    if (Slot[c]) {
      Slot[c]->SignalVolumeAndWeightChange();
    }
  }
}


//==========================================================================
//
//  item::CalculateVolumeAndWeight
//
//==========================================================================
void item::CalculateVolumeAndWeight () {
  Volume = Weight = 0;
  for (int c = 0; c < GetMaterials(); ++c) {
    cmaterial *Material = GetMaterial(c);
    if (Material) {
      Volume += Material->GetVolume();
      Weight += Material->GetWeight();
    }
  }
}


//==========================================================================
//
//  item::SignalEmitationIncrease
//
//==========================================================================
void item::SignalEmitationIncrease (col24 EmitationUpdate) {
  if (game::CompareLights(EmitationUpdate, Emitation) > 0) {
    game::CombineLights(Emitation, EmitationUpdate);
    for (int c = 0; c < SquaresUnder; ++c) {
      if (Slot[c]) {
        Slot[c]->SignalEmitationIncrease(EmitationUpdate);
      }
    }
  }
}


//==========================================================================
//
//  item::SignalEmitationDecrease
//
//==========================================================================
void item::SignalEmitationDecrease (col24 EmitationUpdate) {
  if (game::CompareLights(EmitationUpdate, Emitation) >= 0 && Emitation) {
    col24 Backup = Emitation;
    CalculateEmitation();
    if (Backup != Emitation) {
      for (int c = 0; c < SquaresUnder; ++c) {
        if (Slot[c]) {
          Slot[c]->SignalEmitationDecrease(EmitationUpdate);
        }
      }
    }
  }
}


//==========================================================================
//
//  item::CalculateAll
//
//==========================================================================
void item::CalculateAll () {
  CalculateVolumeAndWeight();
  CalculateEmitation();
}


//==========================================================================
//
//  item::WeaponSkillHit
//
//  Temporary and buggy.
//
//==========================================================================
void item::WeaponSkillHit (int Hits) {
  if (Slot[0] && Slot[0]->IsGearSlot()) {
    static_cast<arm*>(static_cast<gearslot*>(*Slot)->GetBodyPart())->WieldedSkillHit(Hits);
  }
}


//==========================================================================
//
//  item::Duplicate
//
//  Returns 0 if item cannot be cloned
//
//==========================================================================
item *item::Duplicate (feuLong Flags) {
  //k8: don't even ask me.
  if (!(Flags & IGNORE_PROHIBITIONS)) {
    if ((!(Flags & MIRROR_IMAGE) && !CanBeCloned()) ||
        ((Flags & MIRROR_IMAGE) &&
         (!CanBeMirrored() ||
          (MainMaterial && !(MainMaterial->GetCommonFlags() & CAN_BE_MIRRORED)) ||
          (GetSecondaryMaterial() && !(GetSecondaryMaterial()->GetCommonFlags() & CAN_BE_MIRRORED)))))
    {
      return 0;
    }
  }

  item *Clone = GetProtoType()->Clone(this);

  if (Flags & MIRROR_IMAGE) {
    Clone->SetLifeExpectancy(Flags >> LE_BASE_SHIFT & LE_BASE_RANGE,
                             Flags >> LE_RAND_SHIFT & LE_RAND_RANGE);
  }

  idholder *I = new idholder(ID);
  I->Next = CloneMotherID;
  CloneMotherID = I;
  game::RemoveItemID(ID);
  ID = game::CreateNewItemID(this);
  Clone->UpdatePictures();
  return Clone;
}


//==========================================================================
//
//  item::AddSpecialInfo
//
//==========================================================================
void item::AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight) const {
  if (includeWeight) {
    Entry << " [";
    Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
    Entry << "]";
  }
}


//==========================================================================
//
//  item::AddInventoryEntry
//
//==========================================================================
void item::AddInventoryEntry (ccharacter *CC, festring &Entry, int Amount,
                              truth ShowSpecialInfo) const
{
  if (Amount == 1) {
    AddName(Entry, INDEFINITE);
  } else {
    Entry << Amount << ' ';
    AddName(Entry, PLURAL);
  }

  if (ShowSpecialInfo) {
    AddSpecialInfo(CC, Entry, Amount, true);
  }
}


//==========================================================================
//
//  item::AddDescriptionEntry
//
//==========================================================================
void item::AddDescriptionEntry (ccharacter *CC, festring &Entry, int Amount, truth ShowSpecialInfo) const {
  AddInventoryEntry(CC, Entry, Amount, ShowSpecialInfo);
}


//==========================================================================
//
//  item::ReceiveDamage
//
//==========================================================================
truth item::ReceiveDamage (character *Damager, int Damage, int Type, int Dir) {
  if (CanBeBroken() && !IsBroken() && Type & (PHYSICAL_DAMAGE|SOUND|ENERGY|ACID)) {
    int StrengthValue = GetStrengthValue();
    if (!StrengthValue) StrengthValue = 1;
    if (Damage > StrengthValue << 2 && RAND_4 && RAND_N(25 * Damage / StrengthValue) >= 100) {
      Break(Damager, Dir);
      return true;
    }
  }

  if ((Type & ACID) && IsBroken() && IsDestroyable(Damager)) {
    int StrengthValue = GetStrengthValue();
    if (!StrengthValue) StrengthValue = 1;
    if (Damage > StrengthValue << 4 && !RAND_4 && RAND_N(100 * Damage / StrengthValue) >= 100) {
      Destroy(Damager, Dir);
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  item::GetNutritionValue
//
//==========================================================================
sLong item::GetNutritionValue () const {
  sLong NV = 0;
  for (int c = 0; c < GetMaterials(); ++c) {
    if (GetMaterial(c)) {
      NV += GetMaterial(c)->GetTotalNutritionValue();
    }
  }
  return NV;
}


//==========================================================================
//
//  item::SignalSpoil
//
//==========================================================================
void item::SignalSpoil (material *) {
  if (!Exists()) return;

  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s spoils completely.", GetExtendedDescription().CStr());
  }

  truth Equipped = PLAYER->Equips(this);
  Disappear();

  if (Equipped) {
    festring msg = ProcessMessage(CONST_S("!F:@Hsp @nu is destroyed!"), PLAYER);
    game::AskForEscPress(msg);
    //game::AskForEscPress(CONST_S("Equipment destroyed! (") + Item->CHAR_NAME(DEFINITE) + ")");
  }
}


//==========================================================================
//
//  item::DuplicateToStack
//
//==========================================================================
item *item::DuplicateToStack (stack *CurrentStack, feuLong Flags) {
  item *Duplicated = Duplicate(Flags);
  if (!Duplicated) return 0;
  CurrentStack->AddItem(Duplicated);
  return Duplicated;
}


//==========================================================================
//
//  item::CanBePiledWith
//
//==========================================================================
truth item::CanBePiledWith (citem *Item, ccharacter *Viewer) const {
  return (GetType() == Item->GetType()
    && GetConfig() == Item->GetConfig()
    && ItemFlags == Item->ItemFlags
    && (WeightIsIrrelevant() || Weight == Item->Weight)
    && MainMaterial->IsSameAs(Item->MainMaterial)
    && MainMaterial->GetSpoilLevel() == Item->MainMaterial->GetSpoilLevel()
    && MainMaterial->GetRustLevel() == Item->MainMaterial->GetRustLevel()
    && Viewer->GetCWeaponSkillLevel(this) == Viewer->GetCWeaponSkillLevel(Item)
    && Viewer->GetSWeaponSkillLevel(this) == Viewer->GetSWeaponSkillLevel(Item)
    && !Fluid && !Item->Fluid
    && !LifeExpectancy == !Item->LifeExpectancy);
}


//==========================================================================
//
//  item::Break
//
//==========================================================================
void item::Break (character *Breaker, int) {
  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s %s.", GetExtendedDescription().CStr(), GetBreakVerb());
  }

  if (Breaker && IsOnGround()) {
    room *Room = GetRoom();
    if (Room) Room->HostileAction(Breaker);
  }

  item *Broken = GetProtoType()->Clone(this);
  Broken->SetConfig(GetConfig() | BROKEN);
  Broken->SetSize(Broken->GetSize() >> 1);
  DonateFluidsTo(Broken);
  DonateIDTo(Broken);
  DonateSlotTo(Broken);
  SendToHell();

  if (PLAYER->Equips(Broken)) {
    festring msg = ProcessMessage(CONST_S("!F:@Hsp @nu is broken!"), PLAYER);
    game::AskForEscPress(msg);
  }
}


//==========================================================================
//
//  item::Be
//
//==========================================================================
void item::Be () {
  MainMaterial->Be(ItemFlags);
  if (Exists() && LifeExpectancy) {
    if (LifeExpectancy == 1) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s disappears.", GetExtendedDescription().CStr());
      }
      truth Equipped = PLAYER->Equips(this);
      Disappear();
      if (Equipped) {
        festring msg = ProcessMessage(CONST_S("!F:@Hsp @nu is destroyed!"), PLAYER);
        game::AskForEscPress(msg);
      }
    } else {
      LifeExpectancy -= 1;
    }
  }
}


int item::GetOfferValue (int Receiver) const {
  /* Temporary */
  int OfferValue = int(sqrt(GetTruePrice()));
  if (Receiver == GetAttachedGod()) {
    OfferValue <<= 1;
  } else {
    OfferValue >>= 1;
  }
  return OfferValue;
}


//==========================================================================
//
//  item::SignalEnchantmentChange
//
//==========================================================================
void item::SignalEnchantmentChange () {
  for (int c = 0; c < SquaresUnder; ++c) {
    if (Slot[c]) {
      Slot[c]->SignalEnchantmentChange();
    }
  }
}


//==========================================================================
//
//  item::GetEnchantedPrice
//
//==========================================================================
sLong item::GetEnchantedPrice (int Enchantment) const {
  return !PriceIsProportionalToEnchantment()
            ? item::GetPrice()
            : Max<int>(item::GetPrice() * Enchantment * Enchantment, 0);
}


//==========================================================================
//
//  item::Fix
//
//==========================================================================
item *item::Fix () {
  item *Fixed = this;
  if (IsBroken()) {
    Fixed = GetProtoType()->Clone(this);
    Fixed->SetConfig(GetConfig() ^ BROKEN);
    Fixed->SetSize(Fixed->GetSize() << 1);
    DonateFluidsTo(Fixed);
    DonateIDTo(Fixed);
    DonateSlotTo(Fixed);
    SendToHell();
  }
  return Fixed;
}


//==========================================================================
//
//  item::DonateSlotTo
//
//==========================================================================
void item::DonateSlotTo (item *Item) {
  if (Slot[0]) {
    Slot[0]->DonateTo(Item);
    Slot[0] = 0;
    for (int c = 1; c < SquaresUnder; ++c) {
      if (Slot[c]) {
        Slot[c]->Empty();
        Slot[c] = 0;
      }
    }
  }
}


//==========================================================================
//
//  item::GetSpoilLevel
//
//==========================================================================
int item::GetSpoilLevel () const {
  //k8: might happen for invalid configs!
  if (!MainMaterial) {
    ConLogf("WARNING! item `%s` (config #%08x) has no main material in `item::GetSpoilLevel()`!",
            GetNameSingular().CStr(), GetConfig());
    return 0;
  }
  return MainMaterial->GetSpoilLevel();
}


//==========================================================================
//
//  item::SignalSpoilLevelChange
//
//==========================================================================
void item::SignalSpoilLevelChange (material *) {
  if (!IsAnimated() && GetSpoilLevel() && Slot[0] && Slot[0]->IsVisible()) {
    for (int c = 0; c < SquaresUnder; ++c) {
      GetSquareUnder(c)->IncStaticAnimatedEntities();
    }
  }
  SignalVolumeAndWeightChange(); // gum
  UpdatePictures();
}


//==========================================================================
//
//  item::AllowSpoil
//
//==========================================================================
truth item::AllowSpoil () const {
  if (IsOnGround()) {
    lsquare *Square = GetLSquareUnder();
    int RoomNumber = Square->GetRoomIndex();
    return !RoomNumber || Square->GetLevel()->GetRoom(RoomNumber)->AllowSpoil(this);
  } else {
    return true;
  }
}


//==========================================================================
//
//  item::ResetSpoiling
//
//==========================================================================
void item::ResetSpoiling () {
  for (int c = 0; c < GetMaterials(); ++c) {
    if (GetMaterial(c)) {
      GetMaterial(c)->ResetSpoiling();
    }
  }
}


//==========================================================================
//
//  item::GetBaseToHitValueDescription
//
//==========================================================================
cchar *item::GetBaseToHitValueDescription () const {
  if (GetBaseToHitValue() < 10) {
    return ToHitValueDescription[Min(GetBaseToHitValue(), 6)];
  } else {
    return ToHitValueDescription[7];
  }
}


//==========================================================================
//
//  item::GetBaseBlockValueDescription
//
//==========================================================================
cchar *item::GetBaseBlockValueDescription () const {
  if (GetBaseBlockValue() < 20) {
    return ToHitValueDescription[Min(GetBaseBlockValue() >> 1, 6)];
  } else {
    return ToHitValueDescription[7];
  }
}


//==========================================================================
//
//  item::GetStrengthValueDescription
//
//==========================================================================
cchar *item::GetStrengthValueDescription () const {
  if (CanBeBroken()) {
    const int SV = GetStrengthValue();
    if (SV < 3) return StrengthValueDescription[0];
    if (SV < 5) return StrengthValueDescription[1];
    if (SV < 8) return StrengthValueDescription[2];
    if (SV < 11) return StrengthValueDescription[3];
    if (SV < 16) return StrengthValueDescription[4];
    if (SV < 20) return StrengthValueDescription[5];
    return StrengthValueDescription[6];
  } else {
    return "unbreakable";
  }
}


void item::SpecialGenerationHandler () {
  if (HandleInPairs()) {
    item *Second = Duplicate(IGNORE_PROHIBITIONS);
    if (Second) Slot[0]->AddFriendItem(Second);
  }
}


//==========================================================================
//
//  item::SortAllItems
//
//==========================================================================
void item::SortAllItems (const sortdata &SortData) const {
  if (SortData.Sorter == 0 || (this->*SortData.Sorter)(SortData.Character)) {
    SortData.AllItems.push_back(const_cast<item*>(this));
  }
}


//==========================================================================
//
//  item::GetAttachedGod
//
//==========================================================================
int item::GetAttachedGod () const {
  return DataBase->AttachedGod ? DataBase->AttachedGod : MainMaterial->GetAttachedGod();
}


//==========================================================================
//
//  item::GetMaterialPrice
//
//==========================================================================
sLong item::GetMaterialPrice () const {
  return MainMaterial->GetRawPrice();
}


//==========================================================================
//
//  item::GetTruePrice
//
//==========================================================================
sLong item::GetTruePrice () const {
  if (LifeExpectancy) return 0;

  sLong Price = Max(GetPrice(), GetMaterialPrice());

  if (Spoils()) {
    Price = Price * (100 - GetMaxSpoilPercentage()) / 500;
  }

  return Price;
}


//==========================================================================
//
//  item::AddAttackInfo
//
//==========================================================================
void item::AddAttackInfo (felist &List) const {
  festring Entry(40, ' ');
  Entry << int(GetWeight());
  Entry.Resize(50);
  Entry << int(GetSize());
  Entry.Resize(60);
  Entry << int(GetStrengthRequirement());
  Entry.Resize(70);
  Entry << GetBaseMinDamage() << '-' << GetBaseMaxDamage();
  List.AddEntry(Entry, LIGHT_GRAY);
}


//==========================================================================
//
//  item::AddMiscellaneousInfo
//
//==========================================================================
void item::AddMiscellaneousInfo (felist &List) const {
  festring Entry(40, ' ');
  Entry << int(GetTruePrice());
  Entry.Resize(55);
  Entry << GetOfferValue(0);
  Entry.Resize(70);
  Entry << int(GetNutritionValue());
  List.AddEntry(Entry, LIGHT_GRAY);
}


//==========================================================================
//
//  item::PreProcessForBone
//
//==========================================================================
void item::PreProcessForBone () {
  if (IsQuestItem()) {
    RemoveFromSlot();
    SendToHell();
  } else {
    game::RemoveItemID(ID);
    ID = -ID;
    game::AddItemID(this, ID);
    SetSteppedOn(false);
  }
}


//==========================================================================
//
//  item::PostProcessForBone
//
//==========================================================================
void item::PostProcessForBone () {
  boneidmap::iterator BI = game::GetBoneItemIDMap().find(-ID);
  game::RemoveItemID(ID);

  if (BI == game::GetBoneItemIDMap().end()) {
    feuLong NewID = game::CreateNewItemID(this);
    game::GetBoneItemIDMap().insert(std::make_pair(-ID, NewID));
    ID = NewID;
  } else {
    if (game::SearchItem(BI->second)) {
      ID = BI->second;
      game::AddItemID(this, ID);
    }
  }
  for (idholder* I = CloneMotherID; I; I = I->Next) {
    BI = game::GetBoneItemIDMap().find(I->ID);
    if (BI == game::GetBoneItemIDMap().end()) {
      feuLong NewCloneMotherID = game::CreateNewItemID(0);
      game::GetBoneItemIDMap().insert(std::make_pair(I->ID, NewCloneMotherID));
      I->ID = NewCloneMotherID;
    } else {
      I->ID = BI->second;
    }
  }
}


//==========================================================================
//
//  item::SetConfig
//
//==========================================================================
void item::SetConfig (int NewConfig, int SpecialFlags) {
  databasecreator<item>::InstallDataBase(this, NewConfig);
  CalculateAll();

  if (!(SpecialFlags & NO_PIC_UPDATE)) {
    UpdatePictures();
  }
}


//==========================================================================
//
//  item::GetMasterGod
//
//==========================================================================
god *item::GetMasterGod () const {
  return game::GetGod(GetConfig());
}


//==========================================================================
//
//  item::Draw
//
//==========================================================================
void item::Draw (blitdata &BlitData, col16 MonoColor) const {
  cint AF = GraphicData.AnimationFrames;
  cint F = (!(BlitData.CustomData & ALLOW_ANIMATE) || AF == 1 ? 0 : GET_TICK() & (AF - 1));
  cbitmap *P = GraphicData.Picture[F];

  if (MonoColor != TRANSPARENT_COLOR) {
    P->BlitMono(BlitData, MonoColor);
  } else {
    if (BlitData.CustomData & ALLOW_ALPHA) {
      P->AlphaLuminanceBlit(BlitData);
    } else {
      P->LuminanceMaskedBlit(BlitData);
    }

    if (Fluid && ShowFluids()) {
      DrawFluids(BlitData);
    }
  }
}


//==========================================================================
//
//  item::GetLargeBitmapPos
//
//==========================================================================
v2 item::GetLargeBitmapPos (v2 BasePos, int I) const {
  /*
  cint SquareIndex = (I ? I / (GraphicData.AnimationFrames >> 2) : 0);
  return v2(BasePos.X+(SquareIndex&1 ? 16 : 0), BasePos.Y+(SquareIndex&2 ? 16 : 0));
  */
  //ConLogf("GetLargeBitmapPos(0x%08x): I=%d; AF=%u", (unsigned)this, I, GraphicData.AnimationFrames);
  if (I && GraphicData.AnimationFrames > 4) I /= GraphicData.AnimationFrames/4;
  return v2(BasePos.X+(I&1)*16, BasePos.Y+(I&2)*8);
}


//==========================================================================
//
//  item::LargeDraw
//
//==========================================================================
void item::LargeDraw (blitdata &BlitData, col16 MonoColor) const {
  cint TrueAF = GraphicData.AnimationFrames>>2;
  cint SquareIndex = BlitData.CustomData&SQUARE_INDEX_MASK;
  cint F = ((BlitData.CustomData&ALLOW_ANIMATE) == 0
                  ? SquareIndex*TrueAF
                  : SquareIndex*TrueAF+(GET_TICK()&(TrueAF-1)));
  cbitmap *P = GraphicData.Picture[F];
  //ConLogf("LargeDraw(0x%08x): SquareIndex=%u (anim:%d); AF=%u", (unsigned)this, (unsigned)(BlitData.CustomData&SQUARE_INDEX_MASK), (BlitData.CustomData&ALLOW_ANIMATE ? 1 : 0), GraphicData.AnimationFrames);

  if (MonoColor != TRANSPARENT_COLOR) {
    P->BlitMono(BlitData, MonoColor);
  } else {
    if (BlitData.CustomData&ALLOW_ALPHA) {
      P->AlphaLuminanceBlit(BlitData);
    } else {
      P->LuminanceMaskedBlit(BlitData);
    }
  }
}


//==========================================================================
//
//  item::DonateIDTo
//
//==========================================================================
void item::DonateIDTo (item *Item) {
  game::RemoveItemID(Item->ID);
  game::UpdateItemID(Item, ID);
  Item->ID = ID;
  ID = 0;
}


//==========================================================================
//
//  item::SignalRustLevelChange
//
//==========================================================================
void item::SignalRustLevelChange () {
  SignalVolumeAndWeightChange();
  UpdatePictures();
  SendNewDrawAndMemorizedUpdateRequest();
}


//==========================================================================
//
//  item::GetRawPicture
//
//==========================================================================
const rawbitmap *item::GetRawPicture () const {
  return igraph::GetRawGraphic(GetGraphicsContainerIndex());
}


//==========================================================================
//
//  item::RemoveFluid
//
//==========================================================================
void item::RemoveFluid (fluid *ToBeRemoved) {
  truth WasAnimated = IsAnimated();
  truth HasFluids = false;
  if (Fluid && ToBeRemoved) {
    for (int c = 0; c < /*SquaresUnder*/FluidCount; ++c) {
      fluid *F = Fluid[c];
      if (F == ToBeRemoved) {
        Fluid[c] = F->Next;
      } else if (F) {
        fluid *LF = F;
        for (F = F->Next; F; LF = F, F = F->Next) {
          if (F == ToBeRemoved) { LF->Next = F->Next; break; }
        }
      }
      if (Fluid[c]) HasFluids = true;
    }
  }

  UpdatePictures();

  if (!HasFluids && Fluid) {
    delete [] Fluid;
    Fluid = 0;
    if (!IsAnimated() != !WasAnimated && Slot[0]->IsVisible()) GetSquareUnder()->DecStaticAnimatedEntities();
  }

  if (ToBeRemoved) {
    SignalEmitationDecrease(ToBeRemoved->GetEmitation());
  }
  SignalVolumeAndWeightChange();
}


//==========================================================================
//
//  item::RemoveAllFluids
//
//==========================================================================
void item::RemoveAllFluids () {
  if (Fluid) {
    for (int c = 0; c < /*SquaresUnder*/FluidCount; ) {
      //ConLogf("c: %d; SquaresUnder: %d", c, SquaresUnder);
      fluid *F = Fluid[c];
      if (F) {
        RemoveFluid(F);
        c = 0;
        if (!Fluid) break;
      } else {
        c += 1;
      }
    }
  }
}


//==========================================================================
//
//  item::AddFluid
//
//==========================================================================
void item::AddFluid (liquid *ToBeAdded, festring LocationName, int SquareIndex, truth IsInside) {
  truth WasAnimated = IsAnimated();

  if (SquareIndex < 0) ABORT("item::AddFluid(): invalid SquareIndex: %d", SquareIndex);

  if (Fluid) {
    fluid *F = Fluid[SquareIndex];
    if (SquareIndex >= FluidCount) ABORT("item::AddFluid(): invalid SquareIndex: %d", SquareIndex);
    if (!F) {
      Fluid[SquareIndex] = new fluid(ToBeAdded, this, LocationName, IsInside);
    } else {
      fluid *LF;
      do {
        if (ToBeAdded->IsSameAs(F->GetLiquid())) {
          F->AddLiquidAndVolume(ToBeAdded->GetVolume());
          //delete ToBeAdded; //k8: this is BUG!
          ToBeAdded->SendToHell();
          return;
        }
        LF = F;
        F = F->Next;
      } while(F);
      LF->Next = new fluid(ToBeAdded, this, LocationName, IsInside);
    }
  } else {
    FluidCount = SquaresUnder;
    Fluid = new fluid*[/*SquaresUnder*/FluidCount];
    if (SquareIndex >= FluidCount) ABORT("item::AddFluid(): invalid SquareIndex: %d", SquareIndex);
    memset(Fluid, 0, SquaresUnder*sizeof(fluid *));
    Fluid[SquareIndex] = new fluid(ToBeAdded, this, LocationName, IsInside);
  }

  UpdatePictures();
  SignalVolumeAndWeightChange();
  SignalEmitationIncrease(ToBeAdded->GetEmitation());

  if (Slot[0]) {
    if (!IsAnimated() != !WasAnimated && Slot[0]->IsVisible()) {
      GetSquareUnder()->IncStaticAnimatedEntities();
    }
    SendNewDrawAndMemorizedUpdateRequest();
  }
}


//==========================================================================
//
//  item::SendNewDrawAndMemorizedUpdateRequest
//
//==========================================================================
void item::SendNewDrawAndMemorizedUpdateRequest () const {
  if (!game::IsInWilderness()) {
    for (int c = 0; c < SquaresUnder; ++c) {
      if (Slot[c]) {
        lsquare *Square = GetLSquareUnder(c);
        if (Square) {
          Square->SendNewDrawRequest();
          Square->SendMemorizedUpdateRequest();
        }
      }
    }
  }
}


//==========================================================================
//
//  item::CalculateEmitation
//
//==========================================================================
void item::CalculateEmitation () {
  object::CalculateEmitation();
  if (Fluid) {
    for (int c = 0; c < /*SquaresUnder*/FluidCount; ++c) {
      for (const fluid *F = Fluid[c]; F; F = F->Next) {
        game::CombineLights(Emitation, F->GetEmitation());
      }
    }
  }
}


//==========================================================================
//
//  item::FillFluidVector
//
//==========================================================================
void item::FillFluidVector (fluidvector &Vector, int SquareIndex) const {
  if (Fluid) {
    if (SquareIndex < 0 || SquareIndex >= FluidCount) {
      ABORT("item::FillFluidVector(): invalid SquareIndex: %d\n", SquareIndex);
    }
    for (fluid *F = Fluid[SquareIndex]; F; F = F->Next) {
      Vector.push_back(F);
    }
  }
}


//==========================================================================
//
//  item::SpillFluid
//
//==========================================================================
void item::SpillFluid (character *, liquid *Liquid, int SquareIndex) {
  if (AllowFluids() && Liquid->GetVolume()) {
    AddFluid(Liquid, CONST_S(""), SquareIndex, false);
  } else {
    //ConLogf("!!!!!!!!!!! (04)");
    //delete Liquid; //k8: this is BUG!
    Liquid->SendToHell();
  }
}


//==========================================================================
//
//  item::TryToRust
//
//==========================================================================
void item::TryToRust (sLong LiquidModifier) {
  if (MainMaterial->TryToRust(LiquidModifier)) {
    if (CanBeSeenByPlayer()) {
      if (MainMaterial->GetRustLevel() == NOT_RUSTED) ADD_MESSAGE("%s rusts.", CHAR_NAME(DEFINITE));
      else ADD_MESSAGE("%s rusts more.", CHAR_NAME(DEFINITE));
    }
    MainMaterial->SetRustLevel(MainMaterial->GetRustLevel() + 1);
  }
}


//==========================================================================
//
//  item::CheckFluidGearPictures
//
//==========================================================================
void item::CheckFluidGearPictures (v2 ShadowPos, int SpecialFlags, truth BodyArmor) {
  if (Fluid) {
    for (fluid *F = Fluid[0]; F; F = F->Next) {
      F->CheckGearPicture(ShadowPos, SpecialFlags, BodyArmor);
    }
  }
}


//==========================================================================
//
//  item::DrawFluidGearPictures
//
//==========================================================================
void item::DrawFluidGearPictures (blitdata &BlitData, int SpecialFlags) const {
  if (Fluid) {
    for (const fluid *F = Fluid[0]; F; F = F->Next) {
      F->DrawGearPicture(BlitData, SpecialFlags);
    }
  }
}


//==========================================================================
//
//  item::DrawFluidBodyArmorPictures
//
//==========================================================================
void item::DrawFluidBodyArmorPictures (blitdata &BlitData, int SpecialFlags) const {
  if (Fluid) {
    for (const fluid *F = Fluid[0]; F; F = F->Next) {
      F->DrawBodyArmorPicture(BlitData, SpecialFlags);
    }
  }
}


//==========================================================================
//
//  item::DrawFluids
//
//==========================================================================
void item::DrawFluids (blitdata &BlitData) const {
  if (Fluid) {
    cint SquareIndex = BlitData.CustomData & SQUARE_INDEX_MASK;
    for (const fluid *F = Fluid[SquareIndex]; F; F = F->Next) {
      F->Draw(BlitData);
    }
  }
}


//==========================================================================
//
//  item::ReceiveAcid
//
//==========================================================================
void item::ReceiveAcid (material *, cfestring &, sLong Modifier) {
  if (GetMainMaterial()->GetInteractionFlags() & CAN_DISSOLVE) {
    int Damage = Modifier / 1000;
    if (Damage) {
      Damage += RAND_N(Damage);
      ReceiveDamage(0, Damage, ACID);
    }
  }
}


//==========================================================================
//
//  item::DonateFluidsTo
//
//==========================================================================
void item::DonateFluidsTo (item *Item) {
  if (Fluid) {
    for (int c = 0; c < GetSquaresUnder(); ++c) {
      for (fluid *F = Fluid[c]; F; F = F->Next) {
        liquid *Liquid = F->GetLiquid();
        Item->AddFluid(Liquid->SpawnMoreLiquid(Liquid->GetVolume()), F->GetLocationName(), c, F->IsInside());
      }
    }
  }
}


//==========================================================================
//
//  item::Destroy
//
//==========================================================================
void item::Destroy (character *Destroyer, int) {
  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s is destroyed.", GetExtendedDescription().CStr());
  }

  if (Destroyer && IsOnGround()) {
    room *Room = GetRoom();
    if (Room) Room->HostileAction(Destroyer);
  }

  truth Equipped = PLAYER->Equips(this);
  RemoveFromSlot();
  SendToHell();

  if (Equipped) {
    festring msg = ProcessMessage(CONST_S("!F:@Hsp @nu is destroyed!"), PLAYER);
    game::AskForEscPress(msg);
    //game::AskForEscPress(CONST_S("Equipment destroyed!"));
  }
}


//==========================================================================
//
//  item::RemoveRust
//
//==========================================================================
void item::RemoveRust () {
  for (int c = 0; c < GetMaterials(); ++c) {
    if (GetMaterial(c)) {
      GetMaterial(c)->SetRustLevel(NOT_RUSTED);
    }
  }
}


void item::SetSpoilPercentage (int Value) {
  for (int c = 0; c < GetMaterials(); ++c) {
    material *Material = GetMaterial(c);
    if (Material && Material->CanSpoil()) {
      Material->SetSpoilCounter(Material->GetSpoilModifier() * Value / 100);
    }
  }
}


//==========================================================================
//
//  item::RedistributeFluids
//
//==========================================================================
void item::RedistributeFluids () {
  if (Fluid) {
    for (int c = 0; c < GetSquaresUnder(); ++c) {
      if (c < FluidCount) {
        for (fluid *F = Fluid[c]; F; F = F->Next) {
          F->Redistribute();
        }
      }
    }
  }
}


//==========================================================================
//
//  item::GetConsumeMaterial
//
//==========================================================================
material *item::GetConsumeMaterial (ccharacter *Consumer, materialpredicate Predicate) const {
  return (MainMaterial->*Predicate)() && Consumer->CanConsume(MainMaterial) ? MainMaterial : 0;
}


//==========================================================================
//
//  item::RemoveMaterial
//
//  The parameter can only be MainMaterial
//
//==========================================================================
material *item::RemoveMaterial (material *) {
  RemoveFromSlot();
  SendToHell();
  return 0;
}


//==========================================================================
//
//  item::InitMaterials
//
//==========================================================================
void item::InitMaterials (material *FirstMaterial, truth CallUpdatePictures) {
  InitMaterial(MainMaterial, FirstMaterial, GetDefaultMainVolume());
  SignalVolumeAndWeightChange();
  if (CallUpdatePictures) {
    UpdatePictures();
  }
}


//==========================================================================
//
//  item::GenerateMaterials
//
//==========================================================================
void item::GenerateMaterials () {
  int Chosen = RandomizeMaterialConfiguration();
  const fearray<sLong>& MMC = GetMainMaterialConfig();
  InitMaterial(MainMaterial,
               MAKE_MATERIAL(MMC.Data[MMC.Size == 1 ? 0 : Chosen]),
               GetDefaultMainVolume());
}


//==========================================================================
//
//  item::SignalSquarePositionChange
//
//==========================================================================
void item::SignalSquarePositionChange (int Position) {
  Flags &= ~SQUARE_POSITION_BITS;
  Flags |= Position << SQUARE_POSITION_SHIFT;
}


//==========================================================================
//
//  item::Read
//
//==========================================================================
truth item::Read (character *Reader) {
  Reader->StartReading(this, GetReadDifficulty());
  return true;
}


//==========================================================================
//
//  item::CanBeHardened
//
//==========================================================================
truth item::CanBeHardened (ccharacter *) const {
  return (MainMaterial->GetHardenedMaterial(this) != NONE);
}


//==========================================================================
//
//  item::CanBeSoftened
//
//==========================================================================
truth item::CanBeSoftened () const {
  return (MainMaterial->GetSoftenedMaterial(this) != NONE);
}


//==========================================================================
//
//  item::SetLifeExpectancy
//
//==========================================================================
void item::SetLifeExpectancy (int Base, int RandPlus) {
  LifeExpectancy = (RandPlus > 1 ? Base + RAND_N(RandPlus) : Base);
  Enable();
}


//==========================================================================
//
//  item::IsVeryCloseToSpoiling
//
//  FIXME:k8: really? not vice versa?
//
//==========================================================================
truth item::IsVeryCloseToSpoiling () const {
  for (int c = 0; c < GetMaterials(); ++c) {
    if (GetMaterial(c) && !GetMaterial(c)->IsVeryCloseToSpoiling()) {
      return false;
    }
  }
  return true;
}


//==========================================================================
//
//  item::IsValuable
//
//==========================================================================
truth item::IsValuable () const {
  if (DataBase->IsValuable) return true;
  for (int c = 0; c < GetMaterials(); ++c) {
    material *M = GetMaterial(c);
    if (M && (M->GetCommonFlags() & IS_VALUABLE)) return true;
  }
  return false;
}


//==========================================================================
//
//  item::GetHinderVisibilityBonus
//
//==========================================================================
int item::GetHinderVisibilityBonus (ccharacter *Char) const {
  int Bonus = 0;

  if ((GetGearStates() & INFRA_VISION) && !Char->TemporaryStateIsActivated(INFRA_VISION)) {
    Bonus += 20000;
  }

  if ((GetGearStates() & ESP) && !Char->TemporaryStateIsActivated(ESP)) {
    Bonus += 20000;
  }

  if (!game::IsDark(GetEmitation())) {
    Bonus += 5000;
  }

  return Bonus;
}


//==========================================================================
//
//  item::GetFixPrice
//
//==========================================================================
sLong item::GetFixPrice () const {
  item *Clone = GetProtoType()->Clone(this);
  Clone = Clone->Fix();
  Clone->RemoveRust();
  sLong FixPrice = Clone->GetTruePrice();
  Clone->SendToHell();
  return Max(sLong(3.5 * sqrt(FixPrice)), 10);
}


//==========================================================================
//
//  item::AddTrapName
//
//==========================================================================
void item::AddTrapName (festring &String, int Amount) const {
  if (Amount == 1) {
    AddName(String, DEFINITE);
  } else {
    String << Amount << ' ';
    AddName(String, PLURAL);
  }
}


//==========================================================================
//
//  item::Spoils
//
//==========================================================================
truth item::Spoils () const {
  for (int c = 0; c < GetMaterials(); ++c) {
    cmaterial *Material = GetMaterial(c);
    if (Material && Material->Spoils()) return true;
  }
  return false;
}


//==========================================================================
//
//  item::GetMaxSpoilPercentage
//
//==========================================================================
int item::GetMaxSpoilPercentage () const {
  int MaxPercentage = 0;
  for (int c = 0; c < GetMaterials(); ++c) {
    cmaterial *Material = GetMaterial(c);
    if (Material) {
      MaxPercentage = Max(MaxPercentage, Material->GetSpoilPercentage());
    }
  }
  return MaxPercentage;
}


//==========================================================================
//
//  item::HasPrice
//
//==========================================================================
truth item::HasPrice () const {
  return (GetPrice() || GetMaterialPrice());
}


//==========================================================================
//
//  item::Disappear
//
//==========================================================================
void item::Disappear () {
  RemoveFromSlot();
  SendToHell();
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const idholder *IdHolder) {
  SaveFile << IdHolder->ID;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, idholder* &IdHolder) {
  IdHolder = new idholder(ReadType(feuLong, SaveFile));
  return SaveFile;
}


//==========================================================================
//
//  item::GetExtendedDescription
//
//==========================================================================
festring item::GetExtendedDescription () const {
  if (!CanBeSeenByPlayer()) return CONST_S("something");

  festring Desc;
  ccharacter *Carrier = FindCarrier();

  if (Carrier) {
    if (Carrier->IsPlayer()) {
      Desc << "your ";
      AddName(Desc, UNARTICLED);
      return Desc;
    } else if (Carrier->CanBeSeenByPlayer()) {
      Carrier->AddName(Desc, DEFINITE);
      Desc << "'s ";
      AddName(Desc, UNARTICLED);
      return Desc;
    }
  }

  AddName(Desc, DEFINITE);

  if (IsOnGround()) {
    GetLSquareUnder()->AddLocationDescription(Desc);
  }

  return Desc;
}


//==========================================================================
//
//  item::FindCarrier
//
//==========================================================================
ccharacter *item::FindCarrier () const {
  return Slot[0]->FindCarrier();
}


//==========================================================================
//
//  item::GetWearer
//
//  returns 0 if not worn or wielded else the wearer
//
//==========================================================================
const character *item::GetWearer () const {
  if (!GetSlot()->IsGearSlot()) return 0;
  return FindCarrier();
}


//==========================================================================
//
//  itemlock::PostConstruct
//
//==========================================================================
void itemlock::PostConstruct () {
  /* Terrible gum solution! */
  if (!(GetVirtualConfig() & LOCK_BITS)) {
    int NormalLockTypes = 0;
    const itemdatabase * const *ConfigData = GetVirtualProtoType()->GetConfigData();
    int c, ConfigSize = GetVirtualProtoType()->GetConfigSize();

    for (c = 0; c < ConfigSize; ++c) {
      if ((ConfigData[c]->Config & LOCK_BITS) &&
          (ConfigData[c]->Config & ~LOCK_BITS) == GetVirtualConfig() &&
          !(ConfigData[c]->Config & S_LOCK_ID))
      {
        NormalLockTypes += 1;
      }
    }

    int ChosenLock = RAND_N(NormalLockTypes);

    for (c = 0; c < ConfigSize; ++c) {
      if ((ConfigData[c]->Config & LOCK_BITS) &&
          (ConfigData[c]->Config & ~LOCK_BITS) == GetVirtualConfig() &&
          !(ConfigData[c]->Config & S_LOCK_ID) &&
          !ChosenLock--)
      {
        SetVirtualConfig(ConfigData[c]->Config, NO_PIC_UPDATE);
        break;
      }
    }
  }
}


//==========================================================================
//
//  itemlock::KeyCanOpen
//
//==========================================================================
truth itemlock::KeyCanOpen (item *Key, character *Applier) const {
  if (GetVirtualConfig() & BROKEN_LOCK) {
    return false;
  }
  if (Key->CanOpenLockType(GetVirtualConfig() & LOCK_BITS)) {
    return true;
  }
  return true;
}


//==========================================================================
//
//  itemlock::TryKey
//
//==========================================================================
truth itemlock::TryKey (item *Key, character *Applier, bool silent) {
  if (GetVirtualConfig() & BROKEN_LOCK) {
    if (!silent) ADD_MESSAGE("The lock is broken.");
    return true;
  }

  if (Key->CanOpenLockType(GetVirtualConfig()&LOCK_BITS)) {
    if (!silent) {
      if (Locked) {
        if (Applier->IsPlayer()) {
          ADD_MESSAGE("You unlock %s.", GetVirtualDescription(DEFINITE).CStr());
        } else if (Applier->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s unlocks %s.", Applier->CHAR_NAME(DEFINITE), GetVirtualDescription(DEFINITE).CStr());
        }
      } else {
        if (Applier->IsPlayer()) {
          ADD_MESSAGE("You lock %s.", GetVirtualDescription(DEFINITE).CStr());
        } else if (Applier->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s locks %s.", Applier->CHAR_NAME(DEFINITE), GetVirtualDescription(DEFINITE).CStr());
        }
      }
    }
    Locked = !Locked;
  } else {
    if (!silent) {
      if (Applier->IsPlayer()) {
        ADD_MESSAGE("%s doesn't fit in the lock.", Key->CHAR_NAME(DEFINITE));
      } else if (Applier->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s tries to fit %s in the lock, but fails.",
                    Applier->CHAR_NAME(DEFINITE), Key->CHAR_NAME(DEFINITE));
      }
    }
  }

  return true;
}


//==========================================================================
//
//  itemlock::Save
//
//==========================================================================
void itemlock::Save (outputfile &SaveFile) const {
  SaveFile << Locked;
}


//==========================================================================
//
//  itemlock::Load
//
//==========================================================================
void itemlock::Load (inputfile &SaveFile) {
  SaveFile >> Locked;
}


//==========================================================================
//
//  item::IsBeverage
//
//==========================================================================
truth item::IsBeverage (ccharacter *) const {
  for (int c = 0; c < GetMaterials(); ++c) {
    cmaterial *Material = GetMaterial(c);
    if (Material && (Material->GetCategoryFlags() & IS_BEVERAGE)) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  item::Haste
//
//==========================================================================
void item::Haste () {
  ItemFlags |= HASTE;
  ItemFlags &= ~SLOW;
  SendMemorizedUpdateRequest();
}


//==========================================================================
//
//  item::Slow
//
//==========================================================================
void item::Slow () {
  ItemFlags |= SLOW;
  ItemFlags &= ~HASTE;
  SendMemorizedUpdateRequest();
}


//==========================================================================
//
//  item::SendMemorizedUpdateRequest
//
//==========================================================================
void item::SendMemorizedUpdateRequest () const {
  if (!game::IsInWilderness()) {
    for (int c = 0; c < SquaresUnder; ++c) {
      if (Slot[c]) {
        lsquare* Square = GetLSquareUnder(c);
        Square->SendMemorizedUpdateRequest();
      }
    }
  }
}


//==========================================================================
//
//  item::AddStateDescription
//
//==========================================================================
truth item::AddStateDescription (festring &Name, truth Articled) const {
  truth res = false;
  if (Spoils()) {
    if ((ItemFlags&(HASTE|SLOW)) && Articled) { res = true; Name << "a\x18"; }
    if (ItemFlags&HASTE) { res = true; Name << "hasted "; }
    if (ItemFlags&SLOW) { res = true; Name << "slowed "; }
  }
  return res;
}


//==========================================================================
//
//  item::Burn
//
//==========================================================================
truth item::Burn (character *who, v2 where, int dir) {
  //who->EditExperience(PERCEPTION, 150, 1 << 10);
  if (dir != -666) {
    where += game::GetMoveVector(dir);
  }
  area *ca = who->GetSquareUnder()->GetArea();
  if (where.X < 0 || where.Y < 0 || where.X >= ca->GetXSize() || where.Y >= ca->GetYSize()) return false;
  lsquare *sq = static_cast<lsquare *>(ca->GetSquare(where.X, where.Y));
  if (sq) {
    sq->ReceiveTrapDamage(who, 50, FIRE, dir);
    return true;
  }
  return false;
}


//==========================================================================
//
//  item::ProcessMessage
//
//==========================================================================
festring item::ProcessMessage (cfestring &xmsg, ccharacter *hitter, ccharacter *enemy) const {
  festring msg = xmsg;
  auto ffs = TempSaveDisableFluids();
  if (msg.StartsWith("!F:")) {
    msg.Erase(0, 3);
    DisableFluids = true;
  }
  SEARCH_N_REPLACE(msg, CONST_S("@nu"), GetName(UNARTICLED));
  SEARCH_N_REPLACE(msg, CONST_S("@ni"), GetName(INDEFINITE));
  SEARCH_N_REPLACE(msg, CONST_S("@nd"), GetName(DEFINITE));
  SEARCH_N_REPLACE(msg, CONST_S("@du"), GetDescription(UNARTICLED));
  SEARCH_N_REPLACE(msg, CONST_S("@di"), GetDescription(INDEFINITE));
  SEARCH_N_REPLACE(msg, CONST_S("@dd"), GetDescription(DEFINITE));
  //SEARCH_N_REPLACE(msg, CONST_S("@pp"), GetPersonalPronoun());
  //SEARCH_N_REPLACE(msg, CONST_S("@sp"), GetPossessivePronoun());
  //SEARCH_N_REPLACE(msg, CONST_S("@op"), GetObjectPronoun());
  SEARCH_N_REPLACE(msg, CONST_S("@Nu"), GetName(UNARTICLED).CapitalizeCopy());
  SEARCH_N_REPLACE(msg, CONST_S("@Ni"), GetName(INDEFINITE).CapitalizeCopy());
  SEARCH_N_REPLACE(msg, CONST_S("@Nd"), GetName(DEFINITE).CapitalizeCopy());
  SEARCH_N_REPLACE(msg, CONST_S("@Du"), GetDescription(UNARTICLED).CapitalizeCopy());
  SEARCH_N_REPLACE(msg, CONST_S("@Di"), GetDescription(INDEFINITE).CapitalizeCopy());
  SEARCH_N_REPLACE(msg, CONST_S("@Dd"), GetDescription(DEFINITE).CapitalizeCopy());
  //SEARCH_N_REPLACE(msg, CONST_S("@Pp"), GetPersonalPronoun().CapitalizeCopy());
  //SEARCH_N_REPLACE(msg, CONST_S("@Sp"), GetPossessivePronoun().CapitalizeCopy());
  //SEARCH_N_REPLACE(msg, CONST_S("@Op"), GetObjectPronoun().CapitalizeCopy());
  SEARCH_N_REPLACE(msg, CONST_S("@Gd"), CONST_S(GetMasterGod()->GetName()));
  if (hitter) {
    SEARCH_N_REPLACE(msg, CONST_S("@H"), CONST_S("@"));
    msg = hitter->ProcessMessage(msg);
  }
  if (enemy) {
    SEARCH_N_REPLACE(msg, CONST_S("@E"), CONST_S("@"));
    msg = enemy->ProcessMessage(msg);
  }
  return msg;
}


//==========================================================================
//
//  item::ProcessAndAddMessage
//
//==========================================================================
void item::ProcessAndAddMessage (cfestring &xmsg, ccharacter *hitter, ccharacter *enemy) const {
  festring msg = ProcessMessage(xmsg, hitter, enemy);
  if (msg.IsEmpty()) return;
  ADD_MESSAGE("%s", ProcessMessage(msg, hitter, enemy).CStr());
}


//==========================================================================
//
//  item::ProcessAndAddRandomMessage
//
//==========================================================================
void item::ProcessAndAddRandomMessage (const fearray<festring> &list, ccharacter *hitter, ccharacter *enemy) const {
  if (list.Size == 0) return;
  cfestring msg = list.Data[list.Size > 1 ? RAND_N(list.Size) : 0];
  ProcessAndAddMessage(msg, hitter, enemy);
}



//==========================================================================
//
//  GetCanMaterial
//
//  *NOT* produce a new one!
//
//==========================================================================
material *item::GetCanMaterial () const {
  if (!CanBeCanned()) return 0;
  material *mat = GetMainMaterial();
  if (mat && mat->CanBeCanned()) return mat;
  mat = GetSecondaryMaterial();
  if (mat && mat->CanBeCanned()) return mat;
  return 0;
}


//==========================================================================
//
//  item::PlantTrap
//
//  this is common code to plant or throw various traps, like mines and beartraps
//
//==========================================================================
truth item::PlantTrap (character *User) {
  IvanAssert(User);
  // throw it away, why not?
  #if 1
  if (User->IsPlayer()) {
    int Answer = game::DirectionQuestion(CONST_S("In what direction do you wish to throw?"),
                                         true/*RequireAnswer*/, true/*AcceptYourself*/);
    if (Answer != DIR_ERROR && Answer != YOURSELF) {
      User->ThrowItem(Answer, this);
      User->EditExperience(ARM_STRENGTH, 75, 1 << 8);
      User->EditExperience(DEXTERITY, 75, 1 << 8);
      User->EditExperience(PERCEPTION, 75, 1 << 8);
      User->EditNP(-50);
      User->DexterityAction(5);
      return true;
    }
  }
  #endif
  RemoveFromSlot();
  User->GetStackUnder()->AddItem(this);
  return true;
}


//==========================================================================
//
//  item::IsFullOf
//
//  used for descriptions
//
//==========================================================================
truth item::IsFullOf () const {
  material *ss = GetSecondaryMaterial();
  if (!ss || GetDefaultSecondaryVolume() <= 0) return false;
  return (ss->GetVolume() >= GetDefaultSecondaryVolume());
}


//==========================================================================
//
//  item::MaxSecondaryVolume
//
//==========================================================================
int item::MaxSecondaryVolume () const {
  material *ss = GetSecondaryMaterial();
  return (ss ? Max(0, GetDefaultSecondaryVolume()) : 0);
}


//==========================================================================
//
//  item::CurrSecondaryVolume
//
//==========================================================================
int item::CurrSecondaryVolume () const {
  material *ss = GetSecondaryMaterial();
  return (ss ? Max(0, ss->GetVolume()) : 0);
}


//==========================================================================
//
//  item::IsGoodWarriorPrincessItemNoLux
//
//==========================================================================
truth item::IsGoodWarriorPrincessItemNoLux (ccharacter *CC) const {
  return
    IsHelmet(CC) ||
    IsAmulet(CC) ||
    IsCloak(CC) ||
    IsBodyArmor(CC) ||
    IsRing(CC) ||
    IsGauntlet(CC) ||
    IsBelt(CC) ||
    IsBoot(CC) ||
    IsShield(CC) ||
    IsWeapon(CC) ||
    IsArmor(CC) ||
    IsDrinkable(CC) ||
    IsBeverage(CC) ||
    false;
}


//==========================================================================
//
//  item::IsGoodWarriorPrincessItem
//
//==========================================================================
truth item::IsGoodWarriorPrincessItem (ccharacter *CC) const {
  return (IsLuxuryItem(CC) || IsGoodWarriorPrincessItemNoLux(CC));
}
