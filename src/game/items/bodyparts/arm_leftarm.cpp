#ifdef HEADER_PHASE
ITEM(leftarm, arm)
{
public:
  leftarm ();
  leftarm (const leftarm &);

  virtual int GetBodyPartIndex () const override;
  virtual arm *GetPairArm () const override;
  virtual truth IsRightArm () const override;
  virtual int GetSpecialFlags () const override;

protected:
  virtual sweaponskill **GetCurrentSWeaponSkill () const override;
};


#else


leftarm::leftarm () {
  WieldedSlot.Init(this, LEFT_WIELDED_INDEX);
  GauntletSlot.Init(this, LEFT_GAUNTLET_INDEX);
  RingSlot.Init(this, LEFT_RING_INDEX);
}


leftarm::leftarm (const leftarm &Arm) : mybase(Arm) {
  WieldedSlot.Init(this, LEFT_WIELDED_INDEX);
  GauntletSlot.Init(this, LEFT_GAUNTLET_INDEX);
  RingSlot.Init(this, LEFT_RING_INDEX);
}


truth leftarm::IsRightArm () const { return false; }
int leftarm::GetBodyPartIndex () const { return LEFT_ARM_INDEX; }
int leftarm::GetSpecialFlags () const { return SpecialFlags|ST_LEFT_ARM; }
arm *leftarm::GetPairArm () const { return GetHumanoidMaster() ? GetHumanoidMaster()->GetRightArm() : 0; }
sweaponskill **leftarm::GetCurrentSWeaponSkill () const { return &GetHumanoidMaster()->CurrentLeftSWeaponSkill; }


#endif
