#ifdef HEADER_PHASE
ITEM(rightarm, arm)
{
public:
  rightarm ();
  rightarm (const rightarm &);

  virtual int GetBodyPartIndex() const override;
  virtual arm* GetPairArm() const override;
  virtual truth IsRightArm() const override;
  virtual int GetSpecialFlags() const override;

protected:
  virtual sweaponskill **GetCurrentSWeaponSkill () const override;
};


#else


rightarm::rightarm () {
  WieldedSlot.Init(this, RIGHT_WIELDED_INDEX);
  GauntletSlot.Init(this, RIGHT_GAUNTLET_INDEX);
  RingSlot.Init(this, RIGHT_RING_INDEX);
}


rightarm::rightarm (const rightarm &Arm) : mybase(Arm) {
  WieldedSlot.Init(this, RIGHT_WIELDED_INDEX);
  GauntletSlot.Init(this, RIGHT_GAUNTLET_INDEX);
  RingSlot.Init(this, RIGHT_RING_INDEX);
}


truth rightarm::IsRightArm () const { return true; }
int rightarm::GetBodyPartIndex () const { return RIGHT_ARM_INDEX; }
int rightarm::GetSpecialFlags () const { return SpecialFlags|ST_RIGHT_ARM; }
arm *rightarm::GetPairArm() const { return GetHumanoidMaster() ? GetHumanoidMaster()->GetLeftArm() : 0; }
sweaponskill **rightarm::GetCurrentSWeaponSkill () const { return &GetHumanoidMaster()->CurrentRightSWeaponSkill; }


#endif
