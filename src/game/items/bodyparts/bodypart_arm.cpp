#ifdef HEADER_PHASE
ITEM(arm, bodypart)
{
 public:
  arm () : StrengthBonus(0), DexterityBonus(0) {}
  arm (const arm &);
  virtual ~arm ();

  virtual arm *GetPairArm () const { return 0; }
  virtual double GetTypeDamage (ccharacter *) const;
  virtual int GetEquipments () const override { return 3; }
  virtual int GetTotalResistance (int) const override;
  virtual item *GetArmorToReceiveFluid (truth) const override;
  virtual item *GetEquipment (int) const override;
  virtual truth DamageArmor (character *, int, int) override;
  virtual truth EditAllAttributes (int) override;
  virtual truth IsAnimated () const override;
  virtual truth IsRightArm () const { return false; }
  virtual void CalculateAPCost () override;
  virtual void CalculateAttributeBonuses () override;
  virtual void CalculateDamage () override;
  virtual void CalculateToHitValue () override;
  virtual void CopyAttributes (const bodypart *) override;
  virtual void DropEquipment (stack* = 0) override;
  virtual void InitSpecialAttributes () override;
  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual void Mutate () override;
  virtual void SignalEquipmentAdd (gearslot *) override;
  virtual void SignalEquipmentRemoval (gearslot *, citem *) override;
  virtual void SignalPossibleUsabilityChange () override;
  virtual void SignalVolumeAndWeightChange () override;
  virtual void UpdatePictures () override;

  virtual int GetToHitValueRoundedMul10 () const override;

  double GetBlockChance (double) const;
  double GetBlockValue () const;
  double GetDamage () const;
  double GetDexterityExperience () const;
  double GetStrengthExperience () const;
  double GetToHitValue () const;
  double GetUnarmedDamage () const;
  double GetUnarmedToHitValue () const;
  double GetWieldedDamage () const;
  double GetWieldedToHitValue () const;
  int GetAttribute (int, truth = true) const;
  int GetBaseUnarmedStrength () const;
  int GetBlockCapability () const;
  int GetMaxDamage () const;
  int GetMinDamage () const;
  int GetWieldedHitStrength () const;
  item *GetGauntlet () const;
  item *GetRing () const;
  item *GetWielded () const;
  sLong GetAPCost () const;
  sLong GetUnarmedAPCost () const;
  sLong GetWieldedAPCost () const;
  int GetWeaponTooHeavyRate () const;
  //festring GetIfWeaponTooHeavyString (cchar *WeaponDescription) const;
  truth CheckIfWeaponTooHeavy (cchar *WeaponDescription) const;
  truth EditAttribute (int, int);
  truth HasSadistWeapon () const;
  truth PairArmAllowsMelee () const;
  truth TwoHandWieldIsActive () const;
  void AddAttackInfo (felist &) const;
  void AddDefenceInfo (felist &) const;
  void ApplyDexterityBonus (item *Item);
  void ApplyDexterityPenalty (item *);
  void ApplyEquipmentAttributeBonuses (item *);
  void ApplyStrengthBonus (item *Item);
  void DrawWielded (blitdata &) const;
  void EditExperience (int, double, double);
  void Hit (character *, v2, int, int = 0);
  void SetBaseUnarmedStrength (sLong What);
  void SetDexterity (int What);
  void SetGauntlet (item *What);
  void SetRing (item *What);
  void SetStrength (int What);
  void SetWielded (item *What);
  void UpdateWieldedPicture ();
  void WieldedSkillHit (int);

protected:
  virtual sweaponskill **GetCurrentSWeaponSkill () const { return 0; }

  void UpdateArmArmorPictures (graphicdata &, graphicdata&, int) const;
  int GetCurrentSWeaponSkillBonus () const;

protected:
  gearslot WieldedSlot;
  gearslot GauntletSlot;
  gearslot RingSlot;
  double StrengthExperience;
  double DexterityExperience;
  int BaseUnarmedStrength;
  double Damage;
  double ToHitValue;
  sLong APCost;
  int StrengthBonus;
  int DexterityBonus;
  graphicdata WieldedGraphicData;
};


#else


int arm::GetMinDamage () const { return int(Damage * 0.75); }
int arm::GetMaxDamage () const { return int(Damage * 1.25 + 1); }
double arm::GetBlockValue () const { return GetToHitValue() * GetWielded()->GetBlockModifier() / 10000; }
double arm::GetDamage () const { return Damage; }

double arm::GetDexterityExperience () const { return DexterityExperience; }
double arm::GetStrengthExperience () const { return StrengthExperience; }
double arm::GetToHitValue () const { return ToHitValue; }
int arm::GetBaseUnarmedStrength () const { return BaseUnarmedStrength; }
item *arm::GetGauntlet () const { return *GauntletSlot; }
item *arm::GetRing () const { return *RingSlot; }
item *arm::GetWielded () const { return *WieldedSlot; }
sLong arm::GetAPCost () const { return APCost; }
void arm::SetBaseUnarmedStrength (sLong What) { BaseUnarmedStrength = What; }
void arm::SetDexterity (int What) { DexterityExperience = What * EXP_MULTIPLIER; }
void arm::SetGauntlet (item *What) { GauntletSlot.PutInItem(What); }
void arm::SetRing (item *What) { RingSlot.PutInItem(What); }
void arm::SetStrength (int What) { StrengthExperience = What * EXP_MULTIPLIER; }
void arm::SetWielded (item *What) { WieldedSlot.PutInItem(What); }


//==========================================================================
//
//  arm::arm
//
//==========================================================================
arm::arm (const arm &Arm) :
  mybase(Arm),
  StrengthExperience(Arm.StrengthExperience),
  DexterityExperience(Arm.DexterityExperience),
  BaseUnarmedStrength(Arm.BaseUnarmedStrength)
{
}


//==========================================================================
//
//  arm::~arm
//
//==========================================================================
arm::~arm () {
  delete GetWielded();
  delete GetGauntlet();
  delete GetRing();
}


//==========================================================================
//
//  arm::Save
//
//==========================================================================
void arm::Save (outputfile &SaveFile) const {
  bodypart::Save(SaveFile);
  SaveFile << (int)BaseUnarmedStrength;
  SaveFile << StrengthExperience << DexterityExperience;
  SaveFile << WieldedSlot << GauntletSlot << RingSlot;
  SaveFile << WieldedGraphicData;
}


//==========================================================================
//
//  arm::Load
//
//==========================================================================
void arm::Load (inputfile &SaveFile) {
  bodypart::Load(SaveFile);
  SaveFile >> (int&)BaseUnarmedStrength;
  SaveFile >> StrengthExperience >> DexterityExperience;
  SaveFile >> WieldedSlot >> GauntletSlot >> RingSlot;
  SaveFile >> WieldedGraphicData;
}


//==========================================================================
//
//  arm::GetTotalResistance
//
//==========================================================================
int arm::GetTotalResistance (int Type) const {
  if (Master) {
    int Resistance = GetResistance(Type) + Master->GetGlobalResistance(Type);
    if (GetExternalBodyArmor()) Resistance += GetExternalBodyArmor()->GetResistance(Type) >> 1;
    if (GetGauntlet()) Resistance += GetGauntlet()->GetResistance(Type);
    return Resistance;
  }
  return GetResistance(Type);
}


//==========================================================================
//
//  arm::GetUnarmedDamage
//
//==========================================================================
double arm::GetUnarmedDamage () const {
  double WeaponStrength = GetBaseUnarmedStrength() * GetBaseUnarmedStrength();

  item *Gauntlet = GetGauntlet();
  if (Gauntlet) WeaponStrength += Gauntlet->GetWeaponStrength();

  double Base = sqrt(5e-5 * WeaponStrength);
  if (Gauntlet) Base += Gauntlet->GetDamageBonus();

  double Damage = Base*sqrt(1e-7*GetAttribute(ARM_STRENGTH))*GetHumanoidMaster()->GetCWeaponSkill(UNARMED)->GetBonus();

  return Damage;
}


//==========================================================================
//
//  arm::GetUnarmedToHitValue
//
//==========================================================================
double arm::GetUnarmedToHitValue () const {
  double BonusMultiplier = 10.;
  item *Gauntlet = GetGauntlet();

  if (Gauntlet) BonusMultiplier += Gauntlet->GetTHVBonus();

  return
    GetAttribute(DEXTERITY)*
    sqrt(2.5 * Master->GetAttribute(PERCEPTION))*
    GetHumanoidMaster()->GetCWeaponSkill(UNARMED)->GetBonus()*
    Master->GetMoveEase()*
    BonusMultiplier / 5000000;
}


//==========================================================================
//
//  arm::GetUnarmedAPCost
//
//==========================================================================
sLong arm::GetUnarmedAPCost () const {
  return
    sLong(10000000000.0 /
           (APBonus(GetAttribute(DEXTERITY)) * Master->GetMoveEase() *
            Master->GetCWeaponSkill(UNARMED)->GetBonus()));
}


//==========================================================================
//
//  arm::CalculateDamage
//
//==========================================================================
void arm::CalculateDamage () {
  if (!Master) return;
       if (!IsUsable()) Damage = 0;
  else if (GetWielded()) Damage = GetWieldedDamage();
  else if (PairArmAllowsMelee()) Damage = GetUnarmedDamage();
  else Damage = 0;
}


//==========================================================================
//
//  arm::CalculateToHitValue
//
//==========================================================================
void arm::CalculateToHitValue () {
  if (!Master) return;
       if (GetWielded()) ToHitValue = GetWieldedToHitValue();
  else if (PairArmAllowsMelee()) ToHitValue = GetUnarmedToHitValue();
  else ToHitValue = 0;
}


//==========================================================================
//
//  arm::CalculateAPCost
//
//==========================================================================
void arm::CalculateAPCost () {
  if (!Master) return;
       if (GetWielded()) APCost = GetWieldedAPCost();
  else if (PairArmAllowsMelee()) APCost = GetUnarmedAPCost();
  else return;
  if (APCost < 100) APCost = 100;
}


//==========================================================================
//
//  arm::PairArmAllowsMelee
//
//==========================================================================
truth arm::PairArmAllowsMelee () const {
  const arm *PairArm = GetPairArm();
  return !PairArm || !PairArm->IsUsable() || !PairArm->GetWielded() ||
         PairArm->GetWielded()->IsShield(Master);
}


//==========================================================================
//
//  arm::GetWieldedDamage
//
// k8: this *seems* to be used in damage calculation for danger factor.
//
//==========================================================================
double arm::GetWieldedDamage () const {
  citem *Wielded = GetWielded();

  if (!Wielded || Wielded->IsShield(Master)) return 0;

  int HitStrength = GetAttribute(ARM_STRENGTH);
  int Requirement = Wielded->GetStrengthRequirement();

  if (TwoHandWieldIsActive()) {
    HitStrength += GetPairArm()->GetAttribute(ARM_STRENGTH);
    Requirement >>= 2;
  }

  if (HitStrength > Requirement) {
    /* I have no idea whether this works. It needs to be checked */
    return
      Wielded->GetBaseDamage() *
      sqrt(1e-13 * HitStrength) *
      GetCurrentSWeaponSkillBonus() *
      GetHumanoidMaster()->GetCWeaponSkill(Wielded->GetWeaponCategory())->GetBonus();
  }
  #if 0
  ConLogf("W-DAMAGE-zero(char %s:%s; item %s:%s): HitStrength=%d; Requirement=%d; delta=%d",
          Master->GetClassID(), Master->GetConfigName().CStr(),
          Wielded->GetClassID(), Wielded->GetConfigName().CStr(),
          HitStrength, Requirement, HitStrength - Requirement);
  #endif

  return 0;
}


//==========================================================================
//
//  arm::GetToHitValueRoundedMul10
//
//==========================================================================
int arm::GetToHitValueRoundedMul10 () const {
  int res = 0;
  if (GetWieldedDamage() > 0) {
    const double toHitVal = GetToHitValue();
    res = (int)((toHitVal + 0.05) * 10);
  }
  return res;
}


//==========================================================================
//
//  arm::GetWieldedToHitValue
//
//==========================================================================
double arm::GetWieldedToHitValue () const {
  int HitStrength = GetWieldedHitStrength();

  if (HitStrength <= 0) return 0;

  citem *Wielded = GetWielded();

  double Base =
    2e-11 *
    Min(HitStrength, 10) *
    GetHumanoidMaster()->GetCWeaponSkill(Wielded->GetWeaponCategory())->GetBonus() *
    GetCurrentSWeaponSkillBonus() *
    Master->GetMoveEase() * (10000.0/(1000+Wielded->GetWeight())+Wielded->GetTHVBonus());

  double ThisToHit = GetAttribute(DEXTERITY) * sqrt(2.5 * Master->GetAttribute(PERCEPTION));
  const arm *PairArm = GetPairArm();

  if (PairArm && PairArm->IsUsable()) {
    citem *PairWielded = PairArm->GetWielded();
    if (!PairWielded) {
      if (Wielded->IsTwoHanded() && !Wielded->IsShield(Master)) {
        return Base*(ThisToHit+PairArm->GetAttribute(DEXTERITY)*sqrt(2.5*Master->GetAttribute(PERCEPTION)))/2;
      }
    } else if (!Wielded->IsShield(Master) && !PairWielded->IsShield(Master)) {
      return Base*ThisToHit/(1.0+(500.0+PairWielded->GetWeight())/(1000.0+(Wielded->GetWeight()<<1)));
    }
  }

  return Base * ThisToHit;
}


//==========================================================================
//
//  arm::GetWieldedAPCost
//
//==========================================================================
sLong arm::GetWieldedAPCost () const {
  citem *Wielded = GetWielded();
  if (Wielded->IsShield(Master)) return 0;

  int HitStrength = GetWieldedHitStrength();
  if (HitStrength <= 0) return 0;

  return sLong(1 / (1e-14 * APBonus(GetAttribute(DEXTERITY)) *
                    Master->GetMoveEase() *
                    GetHumanoidMaster()->GetCWeaponSkill(Wielded->GetWeaponCategory())->GetBonus() *
                    (GetCurrentSWeaponSkillBonus() * Min(HitStrength, 10))));
}


//==========================================================================
//
//  arm::DropEquipment
//
//==========================================================================
void arm::DropEquipment (stack *Stack) {
  if (Stack) {
    if (GetWielded()) GetWielded()->MoveTo(Stack);
    if (GetGauntlet()) GetGauntlet()->MoveTo(Stack);
    if (GetRing()) GetRing()->MoveTo(Stack);
  } else {
    if (GetWielded()) GetSlot()->AddFriendItem(GetWielded());
    if (GetGauntlet()) GetSlot()->AddFriendItem(GetGauntlet());
    if (GetRing()) GetSlot()->AddFriendItem(GetRing());
  }
}


//==========================================================================
//
//  arm::Hit
//
//==========================================================================
void arm::Hit (character *Enemy, v2 HitPos, int Direction, int Flags) {
  sLong StrExp = 50, DexExp = 50;
  truth THW = false;
  item *Wielded = GetWielded();

  if (Wielded) {
    sLong Weight = Wielded->GetWeight();
    StrExp = Limit(15 * Weight / 200, 75, 300);
    DexExp = (Weight ? Limit(75000 / Weight, 75, 300) : 300);
    THW = TwoHandWieldIsActive();
  }

  const double toHitVal = GetToHitValue();
  const truth critHit = !RAND_N(Master->GetCriticalModifier());
  auto hitres = Enemy->TakeHit(Master, (Wielded ? Wielded : GetGauntlet()), this, HitPos,
                               GetTypeDamage(Enemy), toHitVal,
                               RAND_N(26) - RAND_N(26),
                               (Wielded ? WEAPON_ATTACK : UNARMED_ATTACK), Direction,
                               critHit, (Flags & SADIST_HIT));

  #if 0
  if (Wielded) {
    ConLogf("%s:%s hits %s:%s with %s:%s, toHitVal=%g%s; res=%s",
            Master->GetClassID(), Master->GetConfigName().CStr(),
            Enemy->GetClassID(), Enemy->GetConfigName().CStr(),
            Wielded->GetClassID(), Wielded->GetConfigName().CStr(),
            toHitVal, (critHit ? " (critical)" : ""),
            GetHitResultCStr(hitres));
  }
  #endif

  if (hitres == HAS_HIT || hitres == HAS_BLOCKED ||
      hitres == HAS_DIED || hitres == DID_NO_DAMAGE || hitres == HAS_DODGED)
  {
    if (hitres != HAS_DODGED) {
      // success hit increases arm strength
      EditExperience(ARM_STRENGTH, StrExp, 1 << 9);
      if (THW && GetPairArm()) GetPairArm()->EditExperience(ARM_STRENGTH, StrExp, 1 << 9);
    }
    // dodged part
    EditExperience(DEXTERITY, DexExp, 1 << 9);
    if (THW && GetPairArm()) GetPairArm()->EditExperience(DEXTERITY, DexExp, 1 << 9);
  }
}


//==========================================================================
//
//  arm::GetAttribute
//
//==========================================================================
int arm::GetAttribute (int Identifier, truth AllowBonus) const {
  int res, Base;
  if (Identifier == ARM_STRENGTH) {
    Base = (!UseMaterialAttributes() ? int(StrengthExperience * EXP_DIVISOR) : GetMainMaterial()->GetStrengthValue());
    if (AllowBonus) Base += StrengthBonus;
    res = Max(!IsBadlyHurt() || !AllowBonus ? Base : Base / 3, 1);
  } else if (Identifier == DEXTERITY) {
    Base = (!UseMaterialAttributes() ? int(DexterityExperience * EXP_DIVISOR) : GetMainMaterial()->GetFlexibility() << 2);
    if (AllowBonus) Base += DexterityBonus;
    res = Max(IsUsable() || !AllowBonus ? Base : Base / 3, 1);
  } else {
    ABORT("Illegal arm attribute %d request!", Identifier);
    res = 0xACCA;
  }
  //k8: it is used for divisions, so avoid returning 0 here.
  //    this should not happen, ever, but...
  if (res == 0) res = 1;
  return res;
}


//==========================================================================
//
//  arm::EditAttribute
//
//==========================================================================
truth arm::EditAttribute (int Identifier, int Value) {
  if (!Master) return false;

  if (Identifier == ARM_STRENGTH) {
    if (!UseMaterialAttributes() && Master->RawEditAttribute(StrengthExperience, Value)) {
      Master->CalculateBattleInfo();
      if (Master->IsPlayerKind()) UpdatePictures();
      return true;
    }
  } else if (Identifier == DEXTERITY) {
    if (!UseMaterialAttributes() && Master->RawEditAttribute(DexterityExperience, Value)) {
      Master->CalculateBattleInfo();
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  arm::EditExperience
//
//==========================================================================
void arm::EditExperience (int Identifier, double Value, double Speed) {
  if (!Master) return;

  if (Identifier == ARM_STRENGTH) {
    if (!UseMaterialAttributes()) {
      int Change = Master->RawEditExperience(StrengthExperience, Master->GetNaturalExperience(ARM_STRENGTH), Value, Speed);
      if (Change) {
        cchar *Adj = (Change > 0 ? "stronger" : "weaker");
             if (Master->IsPlayer()) ADD_MESSAGE("Your %s feels %s!", GetBodyPartName().CStr(), Adj);
        else if (Master->IsPet() && Master->CanBeSeenByPlayer()) ADD_MESSAGE("Suddenly %s looks %s.", Master->CHAR_NAME(DEFINITE), Adj);
        Master->CalculateBattleInfo();
        if (Master->IsPlayerKind()) UpdatePictures();
      }
    }
  } else if (Identifier == DEXTERITY) {
    if (!UseMaterialAttributes()) {
      int Change = Master->RawEditExperience(DexterityExperience, Master->GetNaturalExperience(DEXTERITY), Value, Speed);
      if (Change) {
        cchar *Adj = (Change > 0 ? "quite dextrous" : "clumsy");
             if (Master->IsPlayer()) ADD_MESSAGE("Your %s feels %s!", GetBodyPartName().CStr(), Adj);
        else if (Master->IsPet() && Master->CanBeSeenByPlayer()) ADD_MESSAGE("Suddenly %s looks %s.", Master->CHAR_NAME(DEFINITE), Adj);
        Master->CalculateBattleInfo();
      }
    }
  } else {
    ABORT("Illegal arm attribute %d experience edit request!", Identifier);
  }
}


//==========================================================================
//
//  arm::InitSpecialAttributes
//
//==========================================================================
void arm::InitSpecialAttributes () {
  if (!Master->IsHuman() || Master->IsInitializing()) {
    StrengthExperience = Master->GetNaturalExperience(ARM_STRENGTH);
    DexterityExperience = Master->GetNaturalExperience(DEXTERITY);
  } else {
    StrengthExperience = game::GetAveragePlayerArmStrengthExperience();
    DexterityExperience = game::GetAveragePlayerDexterityExperience();
  }
  LimitRef(StrengthExperience, MIN_EXP, MAX_EXP);
  LimitRef(DexterityExperience, MIN_EXP, MAX_EXP);
  BaseUnarmedStrength = Master->GetBaseUnarmedStrength();
}


//==========================================================================
//
//  arm::Mutate
//
//==========================================================================
void arm::Mutate () {
  bodypart::Mutate();
  //StrengthExperience = StrengthExperience * (1.5 - (RAND() & 1023) / 1023.0);
  StrengthExperience = StrengthExperience * (1.5 - RAND_N(1024) / 1023.0);
  //DexterityExperience = DexterityExperience * (1.5 - (RAND() & 1023) / 1023.0);
  DexterityExperience = DexterityExperience * (1.5 - RAND_N(1024) / 1023.0);
  LimitRef(StrengthExperience, MIN_EXP, MAX_EXP);
  LimitRef(DexterityExperience, MIN_EXP, MAX_EXP);
}


//==========================================================================
//
//  arm::GetEquipment
//
//==========================================================================
item *arm::GetEquipment (int I) const {
  switch (I) {
    case 0: return GetWielded();
    case 1: return GetGauntlet();
    case 2: return GetRing();
  }
  return 0;
}


//==========================================================================
//
//  arm::SignalVolumeAndWeightChange
//
//==========================================================================
void arm::SignalVolumeAndWeightChange () {
  bodypart::SignalVolumeAndWeightChange();
  if (Master && !Master->IsInitializing()) {
    GetHumanoidMaster()->EnsureCurrentSWeaponSkillIsCorrect(*GetCurrentSWeaponSkill(), GetWielded());
    CalculateAttributeBonuses();
    CalculateAttackInfo();
    UpdateWieldedPicture();
    if (GetSquareUnder()) GetSquareUnder()->SendNewDrawRequest();
  }
}


//==========================================================================
//
//  arm::TwoHandWieldIsActive
//
//==========================================================================
truth arm::TwoHandWieldIsActive () const {
  citem *Wielded = GetWielded();
  if (Wielded->IsTwoHanded() && !Wielded->IsShield(Master)) {
    arm *PairArm = GetPairArm();
    return PairArm && PairArm->IsUsable() && !PairArm->GetWielded();
  }
  return false;
}


//==========================================================================
//
//  arm::GetBlockChance
//
//==========================================================================
double arm::GetBlockChance (double EnemyToHitValue) const {
  citem *Wielded = GetWielded();
  return Wielded ? Min(1.0 / (1 + EnemyToHitValue / (GetToHitValue() * Wielded->GetBlockModifier()) * 10000), 1.0) : 0;
}


//==========================================================================
//
//  arm::GetBlockCapability
//
//==========================================================================
int arm::GetBlockCapability () const {
  citem *Wielded = GetWielded();
  if (!Wielded) return 0;

  int HitStrength = GetWieldedHitStrength();
  if (HitStrength <= 0) return 0;

  return Min(HitStrength, 10) * Wielded->GetStrengthValue() * GetHumanoidMaster()->GetCWeaponSkill(Wielded->GetWeaponCategory())->GetBonus() * (*GetCurrentSWeaponSkill())->GetBonus() / 10000000;
}


//==========================================================================
//
//  arm::WieldedSkillHit
//
//==========================================================================
void arm::WieldedSkillHit (int Hits) {
  item *Wielded = GetWielded();

  if (Master->GetCWeaponSkill(Wielded->GetWeaponCategory())->AddHit(Hits)) {
    CalculateAttackInfo();
    if (Master->IsPlayer()) {
      int Category = Wielded->GetWeaponCategory();
      GetHumanoidMaster()->GetCWeaponSkill(Category)->AddLevelUpMessage(Category);
    }
  }

  if ((*GetCurrentSWeaponSkill())->AddHit(Hits)) {
    CalculateAttackInfo();
    if (Master->IsPlayer()) {
      auto ffs = Wielded->TempDisableFluids();
      (*GetCurrentSWeaponSkill())->AddLevelUpMessage(Wielded->CHAR_NAME(UNARTICLED));
    }
  }
}


//==========================================================================
//
//  arm::SignalEquipmentAdd
//
//==========================================================================
void arm::SignalEquipmentAdd (gearslot *Slot) {
  int EquipmentIndex = Slot->GetEquipmentIndex();

  if (Master && !Master->IsInitializing()) {
    item *Equipment = Slot->GetItem();

    if (Equipment->IsInCorrectSlot(EquipmentIndex)) ApplyEquipmentAttributeBonuses(Equipment);
    if (EquipmentIndex == RIGHT_GAUNTLET_INDEX || EquipmentIndex == LEFT_GAUNTLET_INDEX) ApplyDexterityPenalty(Equipment);
    if (EquipmentIndex == RIGHT_WIELDED_INDEX || EquipmentIndex == LEFT_WIELDED_INDEX) {
      UpdateWieldedPicture();
      GetSquareUnder()->SendNewDrawRequest();
    }
  }

  if (Master) Master->SignalEquipmentAdd(EquipmentIndex);
}


//==========================================================================
//
//  arm::SignalEquipmentRemoval
//
//==========================================================================
void arm::SignalEquipmentRemoval (gearslot *Slot, citem *Item) {
  int EquipmentIndex = Slot->GetEquipmentIndex();

  if (Master && !Master->IsInitializing()) {
    if (EquipmentIndex == RIGHT_WIELDED_INDEX || EquipmentIndex == LEFT_WIELDED_INDEX) {
      UpdateWieldedPicture();
      square *Square = GetSquareUnder();
      if (Square) Square->SendNewDrawRequest();
    }
  }

  if (Master) Master->SignalEquipmentRemoval(EquipmentIndex, Item);
}


//==========================================================================
//
//  arm::ApplyEquipmentAttributeBonuses
//
//==========================================================================
void arm::ApplyEquipmentAttributeBonuses (item *Item) {
  if (Item->AffectsArmStrength()) StrengthBonus += Item->GetEnchantment();
  if (Item->AffectsDexterity()) DexterityBonus += Item->GetEnchantment();
}


//==========================================================================
//
//  arm::CalculateAttributeBonuses
//
//==========================================================================
void arm::CalculateAttributeBonuses () {
  StrengthBonus = DexterityBonus = 0;

  for (int c = 0; c < GetEquipments(); ++c) {
    item *Equipment = GetEquipment(c);
    if (Equipment && Equipment->IsInCorrectSlot()) {
      ApplyEquipmentAttributeBonuses(Equipment);
    }
  }

  ApplyDexterityPenalty(GetGauntlet());

  if (Master) {
    ApplyDexterityPenalty(GetExternalCloak());
    ApplyDexterityPenalty(GetExternalBodyArmor());
    /* */
    ApplyStrengthBonus(GetExternalHelmet());
    ApplyStrengthBonus(GetExternalCloak());
    ApplyStrengthBonus(GetExternalBodyArmor());
    ApplyStrengthBonus(GetExternalBelt());
    ApplyDexterityBonus(GetExternalHelmet());
    ApplyDexterityBonus(GetExternalCloak());
    ApplyDexterityBonus(GetExternalBodyArmor());
    ApplyDexterityBonus(GetExternalBelt());
  }

  if (!UseMaterialAttributes()) {
    StrengthBonus -= CalculateScarAttributePenalty(GetAttribute(ARM_STRENGTH, false));
    DexterityBonus -= CalculateScarAttributePenalty(GetAttribute(DEXTERITY, false));
  }
}


//==========================================================================
//
//  arm::GetWieldedHitStrength
//
//==========================================================================
int arm::GetWieldedHitStrength () const {
  int HitStrength = GetAttribute(ARM_STRENGTH);
  int Requirement = GetWielded()->GetStrengthRequirement();

  if (TwoHandWieldIsActive()) {
    HitStrength += GetPairArm()->GetAttribute(ARM_STRENGTH);
    Requirement >>= 2;
  }

  return HitStrength - Requirement;
}


//==========================================================================
//
//  arm::ApplyDexterityPenalty
//
//==========================================================================
void arm::ApplyDexterityPenalty (item *Item) {
  if (Item) DexterityBonus -= Item->GetInElasticityPenalty(GetAttribute(DEXTERITY, false));
}


//==========================================================================
//
//  arm::DamageArmor
//
//==========================================================================
truth arm::DamageArmor (character *Damager, int Damage, int Type) {
  sLong AV[3] = { 0, 0, 0 }, AVSum = 0;
  item *Armor[3];
  if ((Armor[0] = GetGauntlet())) AVSum += AV[0] = Max(Armor[0]->GetStrengthValue(), 1);
  if ((Armor[1] = GetExternalBodyArmor())) AVSum += AV[1] = Max(Armor[1]->GetStrengthValue() >> 1, 1);
  if ((Armor[2] = GetExternalCloak())) AVSum += AV[2] = Max(Armor[2]->GetStrengthValue(), 1);
  return AVSum ? Armor[femath::WeightedRand(AV, AVSum)]->ReceiveDamage(Damager, Damage, Type) : false;
}


//==========================================================================
//
//  arm::GetWeaponTooHeavyRate
//
//==========================================================================
int arm::GetWeaponTooHeavyRate () const {
  int res = WEAPON_OK;
  if (GetWielded()) {
    if (!IsUsable()) {
      res = WEAPON_UNUSABLE;
    } else {
      int HitStrength = GetAttribute(ARM_STRENGTH);
      int Requirement = GetWielded()->GetStrengthRequirement();

      if (TwoHandWieldIsActive()) {
        HitStrength += GetPairArm()->GetAttribute(ARM_STRENGTH);
        Requirement >>= 2;

        if (HitStrength - Requirement < 10) {
          if (HitStrength <= Requirement) {
            res = WEAPON_UNUSABLE;
          } else if (HitStrength - Requirement < 4) {
            res = WEAPON_EXTREMLY_DIFFICULT;
          } else if (HitStrength - Requirement < 7) {
            res = WEAPON_MUCH_TROUBLE;
          } else {
            res = WEAPON_SOMEWHAT_DIFFICULT;
          }
        }
      } else {
        if (HitStrength - Requirement < 10) {
          if (HitStrength <= Requirement) {
            res = WEAPON_UNUSABLE;
          } else if (HitStrength - Requirement < 4) {
            res = WEAPON_EXTREMLY_DIFFICULT;
          } else if (HitStrength - Requirement < 7) {
            res = WEAPON_MUCH_TROUBLE;
          } else {
            res = WEAPON_SOMEWHAT_DIFFICULT;
          }
        }
      }
    }
  }

  return res;
}


//==========================================================================
//
//  arm::GetIfWeaponTooHeavyString
//
//==========================================================================
/*
festring arm::GetIfWeaponTooHeavyString (cchar *WeaponDescription) const {
  festring ss;
  if (GetWielded()) {
    if (!IsUsable()) {
      ss << Master->CHAR_POSSESSIVE_PRONOUN << " " << GetBodyPartName() << " is not usable.";
    } else {
      int HitStrength = GetAttribute(ARM_STRENGTH);
      int Requirement = GetWielded()->GetStrengthRequirement();

      if (TwoHandWieldIsActive()) {
        HitStrength += GetPairArm()->GetAttribute(ARM_STRENGTH);
        Requirement >>= 2;

        if (HitStrength - Requirement < 10) {
          if (HitStrength <= Requirement) {
            ss << Master->CHAR_DESCRIPTION(DEFINITE) << " cannot use " << WeaponDescription;
            ss << " Wielding it with two hands requires \1Y" << (Requirement >> 1) + 1 << "\2 strength.";
          } else if (HitStrength - Requirement < 4) {
            ss << "Using " << WeaponDescription << " even with two hands is extremely difficult for ";
            ss << Master->CHAR_DESCRIPTION(DEFINITE) << ".";
          } else if (HitStrength - Requirement < 7) {
            ss << Master->CHAR_DESCRIPTION(DEFINITE);
            if (Master->IsPlayer()) ss << " have "; else ss << " has ";
            ss << "much trouble using " << WeaponDescription << " even with two hands.";
          } else {
            ss << "It is somewhat difficult for " << Master->CHAR_DESCRIPTION(DEFINITE);
            ss << " to use " << WeaponDescription << " even with two hands.";
          }
        }
      } else {
        if (HitStrength - Requirement < 10) {
          festring OtherHandInfo;
          cchar *HandInfo = "";

          if (GetWielded()->IsTwoHanded()) {
            if (GetPairArm() && !GetPairArm()->IsUsable()) {
              OtherHandInfo = Master->GetPossessivePronoun() + " other arm is unusable. ";
            }
            HandInfo = " with one hand";
          }

          if (HitStrength <= Requirement) {
            ss << OtherHandInfo << Master->GetDescription(DEFINITE).CapitalizeCopy();
            ss << " cannot use " << WeaponDescription << ". Wielding it";
            ss << HandInfo << " requires \1Y" << Requirement + 1 << "\2 strength.";
          } else if (HitStrength - Requirement < 4) {
            ss << OtherHandInfo << "Using ";
            ss << WeaponDescription << HandInfo << " is extremely difficult for ";
            ss << Master->CHAR_DESCRIPTION(DEFINITE) << ".";
          } else if (HitStrength - Requirement < 7) {
            ss << OtherHandInfo << Master->GetDescription(DEFINITE).CapitalizeCopy();
            if (Master->IsPlayer()) ss << " have "; else ss << " has ";
            ss << "much trouble using " << WeaponDescription << HandInfo << ".";
          } else {
            ss << OtherHandInfo << "It is somewhat difficult for ";
            ss << Master->CHAR_DESCRIPTION(DEFINITE) << " to use ";
            ss << WeaponDescription << HandInfo << ".";
          }
        }
      }
    }

    return ss;
  }

  return festring::EmptyStr();
}
*/


//==========================================================================
//
//  arm::CheckIfWeaponTooHeavy
//
//  this is not using `GetIfWeaponTooHeavyString()` because
//  i may want to have different descriptions for equipment screen.
//
//==========================================================================
truth arm::CheckIfWeaponTooHeavy (cchar *WeaponDescription) const {
  festring ss;
  if (!IsUsable()) {
    ss << Master->CHAR_POSSESSIVE_PRONOUN << " " << GetBodyPartName() << " is not usable.";
  } else {
    int HitStrength = GetAttribute(ARM_STRENGTH);
    int Requirement = GetWielded()->GetStrengthRequirement();

    if (TwoHandWieldIsActive()) {
      HitStrength += GetPairArm()->GetAttribute(ARM_STRENGTH);
      Requirement >>= 2;

      if (HitStrength - Requirement < 10) {
        if (HitStrength <= Requirement) {
          //ADD_MESSAGE("%s cannot use %s. Wielding it with two hands requires %d strength.", Master->CHAR_DESCRIPTION(DEFINITE), WeaponDescription, (Requirement >> 1) + 1);
          ss << Master->CHAR_DESCRIPTION(DEFINITE) << " cannot use " << WeaponDescription;
          ss << " Wielding it with two hands requires \1Y" << (Requirement >> 1) + 1 << "\2 strength.";
        } else if (HitStrength - Requirement < 4) {
          //ADD_MESSAGE("Using %s even with two hands is extremely difficult for %s.", WeaponDescription, Master->CHAR_DESCRIPTION(DEFINITE));
          ss << "Using " << WeaponDescription << " even with two hands is extremely difficult for ";
          ss << Master->CHAR_DESCRIPTION(DEFINITE) << ".";
        } else if (HitStrength - Requirement < 7) {
          //ADD_MESSAGE("%s %s much trouble using %s even with two hands.", Master->CHAR_DESCRIPTION(DEFINITE), Master->IsPlayer() ? "have" : "has", WeaponDescription);
          ss << Master->CHAR_DESCRIPTION(DEFINITE);
          if (Master->IsPlayer()) ss << " have "; else ss << " has ";
          ss << "much trouble using " << WeaponDescription << " even with two hands.";
        } else {
          //ADD_MESSAGE("It is somewhat difficult for %s to use %s even with two hands.", Master->CHAR_DESCRIPTION(DEFINITE), WeaponDescription);
          ss << "It is somewhat difficult for " << Master->CHAR_DESCRIPTION(DEFINITE);
          ss << " to use " << WeaponDescription << " even with two hands.";
        }
      }
    } else {
      if (HitStrength - Requirement < 10) {
        festring OtherHandInfo;
        cchar *HandInfo = "";

        if (GetWielded()->IsTwoHanded()) {
          if (GetPairArm() && !GetPairArm()->IsUsable()) {
            OtherHandInfo = Master->GetPossessivePronoun() + " other arm is unusable. ";
          }
          HandInfo = " with one hand";
        }

        if (HitStrength <= Requirement) {
          //ADD_MESSAGE("%s%s cannot use %s. Wielding it%s requires %d strength.", OtherHandInfo.CStr(), Master->GetDescription(DEFINITE).CapitalizeCopy().CStr(), WeaponDescription, HandInfo, Requirement + 1);
          ss << OtherHandInfo << Master->GetDescription(DEFINITE).CapitalizeCopy();
          ss << " cannot use " << WeaponDescription << ". Wielding it";
          ss << HandInfo << " requires \1Y" << Requirement + 1 << "\2 strength.";
        } else if (HitStrength - Requirement < 4) {
          //ADD_MESSAGE("%sUsing %s%s is extremely difficult for %s.", OtherHandInfo.CStr(), WeaponDescription, HandInfo, Master->CHAR_DESCRIPTION(DEFINITE));
          ss << OtherHandInfo << "Using ";
          ss << WeaponDescription << HandInfo << " is extremely difficult for ";
          ss << Master->CHAR_DESCRIPTION(DEFINITE) << ".";
        } else if (HitStrength - Requirement < 7) {
          //ADD_MESSAGE("%s%s %s much trouble using %s%s.", OtherHandInfo.CStr(), Master->GetDescription(DEFINITE).CapitalizeCopy().CStr(), Master->IsPlayer() ? "have" : "has", WeaponDescription, HandInfo);
          ss << OtherHandInfo << Master->GetDescription(DEFINITE).CapitalizeCopy();
          if (Master->IsPlayer()) ss << " have "; else ss << " has ";
          ss << "much trouble using " << WeaponDescription << HandInfo << ".";
        } else {
          //ADD_MESSAGE("%sIt is somewhat difficult for %s to use %s%s.", OtherHandInfo.CStr(), Master->CHAR_DESCRIPTION(DEFINITE), WeaponDescription, HandInfo);
          ss << OtherHandInfo << "It is somewhat difficult for ";
          ss << Master->CHAR_DESCRIPTION(DEFINITE) << " to use ";
          ss << WeaponDescription << HandInfo << ".";
        }
      }
    }
  }

  if (!ss.IsEmpty()) {
    ADD_MESSAGE("%s", ss.CStr());
    ss << "\nContinue anyway?";
    return !game::TruthQuestion(ss);
  }

  return false;
}


//==========================================================================
//
//  arm::EditAllAttributes
//
//==========================================================================
truth arm::EditAllAttributes (int Amount) {
  LimitRef(StrengthExperience += Amount * EXP_MULTIPLIER, MIN_EXP, MAX_EXP);
  LimitRef(DexterityExperience += Amount * EXP_MULTIPLIER, MIN_EXP, MAX_EXP);
  return (Amount < 0 && (StrengthExperience != MIN_EXP || DexterityExperience != MIN_EXP)) ||
         (Amount > 0 && (StrengthExperience != MAX_EXP || DexterityExperience != MAX_EXP));
}


//==========================================================================
//
//  arm::AddAttackInfo
//
//==========================================================================
void arm::AddAttackInfo (felist &List) const {
  if (GetDamage()) {
    festring Entry = CONST_S("   ");
    if (GetWielded()) {
      GetWielded()->AddName(Entry, UNARTICLED);
      if (TwoHandWieldIsActive()) Entry << " (b)";
    } else {
      Entry << "melee attack";
    }
    Entry.Resize(50);
    Entry << GetMinDamage() << '-' << GetMaxDamage();
    Entry.Resize(60);
    Entry << int(GetToHitValue());
    Entry.Resize(70);
    Entry << GetAPCost();
    List.AddEntry(Entry, LIGHT_GRAY);
  }
}


//==========================================================================
//
//  arm::AddDefenceInfo
//
//==========================================================================
void arm::AddDefenceInfo (felist &List) const {
  if (GetWielded()) {
    festring Entry = CONST_S("   ");
    GetWielded()->AddName(Entry, UNARTICLED);
    Entry.Resize(50);
    Entry << int(GetBlockValue());
    Entry.Resize(70);
    Entry << GetBlockCapability();
    List.AddEntry(Entry, LIGHT_GRAY);
  }
}


//==========================================================================
//
//  arm::UpdateWieldedPicture
//
//==========================================================================
void arm::UpdateWieldedPicture () {
  if (!Master || !Master->PictureUpdatesAreForbidden()) {
    truth WasAnimated = MasterIsAnimated();
    item *Wielded = GetWielded();
    if (Wielded && Master) {
      int SpecialFlags = (IsRightArm() ? 0 : MIRROR)|ST_WIELDED|(Wielded->GetSpecialFlags()&~0x3F);
      Wielded->UpdatePictures(WieldedGraphicData, Master->GetWieldedPosition(), SpecialFlags,
                              GetMaxAlpha(), GR_HUMANOID, static_cast<bposretriever>(&item::GetWieldedBitmapPos));
      if (ShowFluids()) Wielded->CheckFluidGearPictures(Wielded->GetWieldedBitmapPos(0), SpecialFlags, false);
    } else {
      WieldedGraphicData.Retire();
    }
    if (!WasAnimated != !MasterIsAnimated()) {
      SignalAnimationStateChange(WasAnimated);
    }
  }
}


//==========================================================================
//
//  arm::DrawWielded
//
//==========================================================================
void arm::DrawWielded (blitdata &BlitData) const {
  DrawEquipment(WieldedGraphicData, BlitData);
  if (ShowFluids() && GetWielded()) {
    GetWielded()->DrawFluidGearPictures(BlitData, IsRightArm() ? 0 : MIRROR);
  }
}


//==========================================================================
//
//  arm::UpdatePictures
//
//==========================================================================
void arm::UpdatePictures () {
  bodypart::UpdatePictures();
  UpdateWieldedPicture();
}


//==========================================================================
//
//  arm::GetTypeDamage
//
//==========================================================================
double arm::GetTypeDamage (ccharacter *Enemy) const {
  //if (!GetWielded() || !GetWielded()->IsGoodWithPlants() || !Enemy->IsPlant()) return Damage;
  //return Damage * 1.5;
  double ActualDamage = GetDamage();

  if (Enemy && GetWielded()) {
    if (GetWielded()->IsGoodWithPlants() && Enemy->IsPlant()) ActualDamage *= 1.5;
    if (GetWielded()->IsGoodWithUndead() && Enemy->IsUndead()) ActualDamage *= 1.5;
    if (HasSadistWeapon() && Enemy->IsMasochist()) ActualDamage *= 0.75;
  }

  return ActualDamage;
}


//==========================================================================
//
//  arm::GetArmorToReceiveFluid
//
//==========================================================================
item *arm::GetArmorToReceiveFluid (truth) const {
  item *Cloak = GetExternalCloak();
  if (Cloak && !RAND_N(3)) return Cloak;

  item *Wielded = GetWielded();
  if (Wielded && !RAND_N(3)) return Wielded;

  item *Gauntlet = GetGauntlet();
  if (Gauntlet && RAND_2) return Gauntlet;

  item *BodyArmor = GetExternalBodyArmor();
  return (BodyArmor ? BodyArmor : 0);
}


//==========================================================================
//
//  arm::UpdateArmArmorPictures
//
//==========================================================================
void arm::UpdateArmArmorPictures (graphicdata &ArmArmorGraphicData,
                                  graphicdata &GauntletGraphicData,
                                  int SpecialFlags) const
{
  if (!Master || !Master->PictureUpdatesAreForbidden()) {
    UpdateArmorPicture(ArmArmorGraphicData, (Master ? GetExternalBodyArmor() : 0), SpecialFlags, (GetAttribute(ARM_STRENGTH, false) >= 20 ? &item::GetAthleteArmArmorBitmapPos : &item::GetArmArmorBitmapPos), true);
    UpdateArmorPicture(GauntletGraphicData, GetGauntlet(), SpecialFlags, &item::GetGauntletBitmapPos);
  }
}


//==========================================================================
//
//  arm::CopyAttributes
//
//==========================================================================
void arm::CopyAttributes (const bodypart *BodyPart) {
  const arm *Arm = static_cast<const arm *>(BodyPart);
  StrengthExperience = Arm->StrengthExperience;
  DexterityExperience = Arm->DexterityExperience;
}


//==========================================================================
//
//  arm::SignalPossibleUsabilityChange
//
//==========================================================================
void arm::SignalPossibleUsabilityChange () {
  feuLong OldFlags = Flags;
  UpdateFlags();
  if (Flags != OldFlags && !Master->IsInitializing()) Master->CalculateBattleInfo();
}


//==========================================================================
//
//  arm::IsAnimated
//
//==========================================================================
truth arm::IsAnimated () const {
  return WieldedGraphicData.AnimationFrames > 1;
}


//==========================================================================
//
//  arm::GetCurrentSWeaponSkillBonus
//
//==========================================================================
int arm::GetCurrentSWeaponSkillBonus () const {
  return (*GetCurrentSWeaponSkill() ? (*GetCurrentSWeaponSkill())->GetBonus() : 1);
}


//==========================================================================
//
//  arm::HasSadistWeapon
//
//==========================================================================
truth arm::HasSadistWeapon () const {
  item *Wielded = GetWielded();
  return Wielded && Wielded->IsSadistWeapon();
}


//==========================================================================
//
//  arm::ApplyStrengthBonus
//
//==========================================================================
void arm::ApplyStrengthBonus (item *Item) {
  if (Item && Item->AffectsArmStrength()) {
    if (Item->AffectsArmStrengthNoDiv2()) {
      StrengthBonus += Item->GetEnchantment();
    } else {
      StrengthBonus += Div2Ass(Item->GetEnchantment());
    }
  }
}


//==========================================================================
//
//  arm::ApplyDexterityBonus
//
//==========================================================================
void arm::ApplyDexterityBonus (item *Item) {
  if (Item && Item->AffectsDexterity()) {
    if (Item->AffectsDexterityNoDiv2()) {
      DexterityBonus += Item->GetEnchantment();
    } else {
      DexterityBonus += Div2Ass(Item->GetEnchantment());
    }
  }
}


#endif
