#ifdef HEADER_PHASE
ITEM(groin, bodypart)
{
public:
  virtual int GetTotalResistance (int) const override;
  virtual int GetBodyPartIndex () const override;
  virtual truth DamageArmor (character *, int, int) override;
  virtual int GetSpecialFlags () const override;
  virtual item *GetArmorToReceiveFluid (truth) const override;

  void UpdateGroinArmorPictures (graphicdata &) const;
};


#else


int groin::GetBodyPartIndex () const { return GROIN_INDEX; }
int groin::GetSpecialFlags () const { return SpecialFlags|ST_GROIN; }


int groin::GetTotalResistance (int Type) const {
  if (Master) {
    int Resistance = GetResistance(Type) + Master->GetGlobalResistance(Type);
    if (GetExternalBodyArmor()) Resistance += GetExternalBodyArmor()->GetResistance(Type);
    if (GetHumanoidMaster()->GetBelt()) Resistance += GetHumanoidMaster()->GetBelt()->GetResistance(Type);
    return Resistance;
  }
  return GetResistance(Type);
}


truth groin::DamageArmor (character *Damager, int Damage, int Type) {
  return Master->GetTorso()->DamageArmor(Damager, Damage, Type);
}


item *groin::GetArmorToReceiveFluid (truth) const {
  item *Cloak = GetExternalCloak();
  if (Cloak && !RAND_N(3)) return Cloak;

  item *BodyArmor = GetExternalBodyArmor();
  return BodyArmor ? BodyArmor : 0;
}


void groin::UpdateGroinArmorPictures (graphicdata &GroinArmorGraphicData) const {
  if (!Master || !Master->PictureUpdatesAreForbidden()) {
    UpdateArmorPicture(GroinArmorGraphicData, (Master ? GetExternalBodyArmor() : 0), ST_GROIN, &item::GetLegArmorBitmapPos, true);
  }
}


#endif
