#ifdef HEADER_PHASE
ITEM(lobhsetorso, largetorso)
{
protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetMaterialColorD (int) const override;
};


#else


int lobhsetorso::GetClassAnimationFrames () const { return 32; }


col16 lobhsetorso::GetMaterialColorD (int Frame) const {
  Frame &= 31;
  int Modifier = Frame * (31 - Frame);
  return MakeRGB16(220 - (Modifier >> 2), 220 - (Modifier >> 1), 0);
}


#endif
