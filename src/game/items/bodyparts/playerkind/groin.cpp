#ifdef HEADER_PHASE
ITEM(playerkindgroin, groin)
{
public:
  playerkindgroin () {}
  playerkindgroin (const playerkindgroin &Groin) : mybase(Groin) {}

  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth UpdateArmorPictures () override;
  virtual void DrawArmor (blitdata &) const override;
  virtual truth ShowFluids () const override;
  virtual truth IsAnimated () const override;

protected:
  graphicdata GroinArmorGraphicData;
};


#else


truth playerkindgroin::ShowFluids () const { return true; }
truth playerkindgroin::IsAnimated () const { return true; }


truth playerkindgroin::UpdateArmorPictures () {
  UpdateGroinArmorPictures(GroinArmorGraphicData);
  return true;
}


void playerkindgroin::DrawArmor (blitdata &BlitData) const {
  DrawEquipment(GroinArmorGraphicData, BlitData);
  if (Master && GetExternalBodyArmor()) {
    GetExternalBodyArmor()->DrawFluidBodyArmorPictures(BlitData, ST_GROIN);
  }
}


void playerkindgroin::Save (outputfile &SaveFile) const {
  groin::Save(SaveFile);
  SaveFile << GroinArmorGraphicData;
}


void playerkindgroin::Load (inputfile &SaveFile) {
  groin::Load(SaveFile);
  SaveFile >> GroinArmorGraphicData;
}


#endif
