#ifdef HEADER_PHASE
ITEM(playerkindhead, head)
{
public:
  playerkindhead () {}
  playerkindhead (const playerkindhead &Head) : mybase(Head) {}

  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth UpdateArmorPictures () override;
  virtual void DrawArmor (blitdata &) const override;
  virtual truth ShowFluids() const override;
  virtual truth IsAnimated() const override;

protected:
  graphicdata HelmetGraphicData;
};


#else


truth playerkindhead::ShowFluids () const { return true; }
truth playerkindhead::IsAnimated () const { return true; }


truth playerkindhead::UpdateArmorPictures () {
  UpdateHeadArmorPictures(HelmetGraphicData);
  return true;
}


void playerkindhead::DrawArmor (blitdata &BlitData) const {
  DrawEquipment(HelmetGraphicData, BlitData);
  if (GetHelmet()) GetHelmet()->DrawFluidGearPictures(BlitData);
}


void playerkindhead::Save (outputfile &SaveFile) const {
  head::Save(SaveFile);
  SaveFile << HelmetGraphicData;
}


void playerkindhead::Load (inputfile &SaveFile) {
  head::Load(SaveFile);
  SaveFile >> HelmetGraphicData;
}


#endif
