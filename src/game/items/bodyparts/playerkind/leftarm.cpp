#ifdef HEADER_PHASE
ITEM(playerkindleftarm, leftarm)
{
public:
  playerkindleftarm () {}
  playerkindleftarm (const playerkindleftarm &Arm) : mybase(Arm) {}

  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth UpdateArmorPictures () override;
  virtual void DrawArmor (blitdata &) const override;
  virtual truth ShowFluids() const override;
  virtual truth IsAnimated() const override;

protected:
  graphicdata ArmArmorGraphicData;
  graphicdata GauntletGraphicData;
};


#else


truth playerkindleftarm::ShowFluids () const { return true; }
truth playerkindleftarm::IsAnimated () const { return true; }


truth playerkindleftarm::UpdateArmorPictures () {
  UpdateArmArmorPictures(ArmArmorGraphicData, GauntletGraphicData, ST_LEFT_ARM);
  return true;
}


void playerkindleftarm::DrawArmor (blitdata &BlitData) const {
  DrawEquipment(ArmArmorGraphicData, BlitData);
  if (Master && GetExternalBodyArmor()) {
    GetExternalBodyArmor()->DrawFluidBodyArmorPictures(BlitData, ST_LEFT_ARM);
  }
  DrawEquipment(GauntletGraphicData, BlitData);
  if (GetGauntlet()) GetGauntlet()->DrawFluidGearPictures(BlitData);
}


void playerkindleftarm::Save (outputfile &SaveFile) const {
  leftarm::Save(SaveFile);
  SaveFile << ArmArmorGraphicData << GauntletGraphicData;
}


void playerkindleftarm::Load (inputfile &SaveFile) {
  leftarm::Load(SaveFile);
  SaveFile >> ArmArmorGraphicData >> GauntletGraphicData;
}


#endif
