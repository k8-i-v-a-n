#ifdef HEADER_PHASE
ITEM(playerkindrightarm, rightarm)
{
public:
  playerkindrightarm () {}
  playerkindrightarm (const playerkindrightarm &Arm) : mybase(Arm) {}

  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth UpdateArmorPictures () override;
  virtual void DrawArmor (blitdata &) const override;
  virtual truth ShowFluids () const override;
  virtual truth IsAnimated () const override;

protected:
  graphicdata ArmArmorGraphicData;
  graphicdata GauntletGraphicData;
};


#else


truth playerkindrightarm::ShowFluids () const { return true; }
truth playerkindrightarm::IsAnimated () const { return true; }


truth playerkindrightarm::UpdateArmorPictures () {
  UpdateArmArmorPictures(ArmArmorGraphicData, GauntletGraphicData, ST_RIGHT_ARM);
  return true;
}


void playerkindrightarm::DrawArmor (blitdata &BlitData) const {
  DrawEquipment(ArmArmorGraphicData, BlitData);
  if (Master && GetExternalBodyArmor()) {
    GetExternalBodyArmor()->DrawFluidBodyArmorPictures(BlitData, ST_RIGHT_ARM);
  }
  DrawEquipment(GauntletGraphicData, BlitData);
  if (GetGauntlet()) GetGauntlet()->DrawFluidGearPictures(BlitData);
}


void playerkindrightarm::Save (outputfile &SaveFile) const {
  rightarm::Save(SaveFile);
  SaveFile << ArmArmorGraphicData << GauntletGraphicData;
}


void playerkindrightarm::Load (inputfile &SaveFile) {
  rightarm::Load(SaveFile);
  SaveFile >> ArmArmorGraphicData >> GauntletGraphicData;
}


#endif
