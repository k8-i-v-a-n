#include "base_humanoidtorso.cpp"
#include "base_normaltorso.cpp"

#include "battorso.cpp"
#include "dogtorso.cpp"
#include "eddytorso.cpp"
#include "magicmushroomtorso.cpp"
#include "mysticfrogtorso.cpp"
#include "spidertorso.cpp"
#include "magpietorso.cpp"
#include "thunderbirdtorso.cpp"
#include "snaketorso.cpp"

#include "blinkdogtorso.cpp"
