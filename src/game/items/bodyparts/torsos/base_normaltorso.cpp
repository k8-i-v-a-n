#ifdef HEADER_PHASE
ITEM(normaltorso, torso)
{
public:
  virtual int GetGraphicsContainerIndex () const override;
  virtual int GetTotalResistance (int) const override;
};


#else


int normaltorso::GetGraphicsContainerIndex () const { return GR_CHARACTER; }


int normaltorso::GetTotalResistance (int Type) const {
  if (Master) return GetResistance(Type) + Master->GetGlobalResistance(Type);
  return GetResistance(Type);
}


#endif
