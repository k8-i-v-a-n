#ifdef HEADER_PHASE
ITEM(blinkdogtorso, dogtorso)
{
protected:
  virtual alpha GetAlphaA (int) const override;
  virtual int GetClassAnimationFrames() const override;
};


#else


int blinkdogtorso::GetClassAnimationFrames () const { return 64; }
alpha blinkdogtorso::GetAlphaA(int Frame) const { return (Frame & 31) != 31 ? 255 : 0; }


#endif
