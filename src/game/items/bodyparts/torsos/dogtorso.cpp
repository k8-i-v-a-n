#ifdef HEADER_PHASE
ITEM(dogtorso, normaltorso)
{
protected:
  virtual void Draw (blitdata &BlitData, col16 MonoColor=TRANSPARENT_COLOR) const override;
  virtual int GetClassAnimationFrames () const override;
  virtual v2 GetBitmapPos (int) const override;
};


#else


int dogtorso::GetClassAnimationFrames () const { return 16; }


v2 dogtorso::GetBitmapPos (int Frame) const {
  v2 BasePos = torso::GetBitmapPos(Frame);
  if (Frame >= GraphicData.AnimationFrames >> 1) BasePos.X += 32;
  return v2(BasePos.X + ((Frame & 4) << 2), BasePos.Y);
}


void dogtorso::Draw (blitdata &BlitData, col16 MonoColor) const {
  cint AF = GraphicData.AnimationFrames >> 1;
  int Index = !(BlitData.CustomData & ALLOW_ANIMATE) || AF == 1 ? 0 : GET_TICK() & (AF - 1);

  if (GetHP() << 1 <= GetMaxHP()) Index += AF;

  cbitmap *P = GraphicData.Picture[Index];

  if (MonoColor != TRANSPARENT_COLOR) {
    P->BlitMono(BlitData, MonoColor);
  } else {
    if (BlitData.CustomData & ALLOW_ALPHA) {
      P->AlphaPriorityBlit(BlitData);
    } else {
      P->MaskedPriorityBlit(BlitData);
    }
  }
}


#endif
