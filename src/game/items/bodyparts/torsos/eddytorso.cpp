#ifdef HEADER_PHASE
ITEM(eddytorso, normaltorso)
{
protected:
  virtual int GetClassAnimationFrames () const override;
  virtual v2 GetBitmapPos (int) const override;
};


#else


int eddytorso::GetClassAnimationFrames() const { return 8; }
v2 eddytorso::GetBitmapPos(int Frame) const { return torso::GetBitmapPos(Frame) + v2((Frame&0x6) << 3, 0); }


#endif
