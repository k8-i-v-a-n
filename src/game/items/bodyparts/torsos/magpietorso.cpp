#ifdef HEADER_PHASE
ITEM(magpietorso, normaltorso)
{
protected:
  virtual int GetClassAnimationFrames () const override;
  virtual v2 GetBitmapPos (int) const override;
};


#else

//==========================================================================
//
//  magpietorso::GetClassAnimationFrames
//
//==========================================================================
int magpietorso::GetClassAnimationFrames () const {
  return 16;
}


//==========================================================================
//
//  magpietorso::GetBitmapPos
//
//  GetClassAnimationFrames() is the total animation frames per second,
//  so Frame is from 0 to 15 here.
//  Magpie has 8 pictures, so it will draw the same picture for each 2 frames.
//  GetBitmapPos() method will be called only once and stored on the savegame file,
//  therefore there is no need for speed.
//  TODO: correct?
//
//==========================================================================
v2 magpietorso::GetBitmapPos (int Frame) const {
  v2 BasePos = torso::GetBitmapPos(Frame);
  Frame &= 0xF;

  float fPicTot = 8.0;
  float fDiv = GetClassAnimationFrames() / fPicTot; //each 2 frames
  int iPic = Frame / fDiv; //DBG3(Frame, fDiv, iPic);
  return v2(BasePos.X + iPic * TILE_SIZE, BasePos.Y);
}


#endif
