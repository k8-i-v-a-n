#ifdef HEADER_PHASE
ITEM(snaketorso, normaltorso)
{
protected:
  virtual int GetClassAnimationFrames () const override;
  virtual v2 GetBitmapPos (int) const override;
};


#else

//==========================================================================
//
//  snaketorso::GetClassAnimationFrames
//
//==========================================================================
int snaketorso::GetClassAnimationFrames () const {
  return 32;
}


//==========================================================================
//
//  snaketorso::GetBitmapPos
//
//==========================================================================
v2 snaketorso::GetBitmapPos (int Frame) const {
  v2 BasePos = torso::GetBitmapPos(Frame);

  if (Frame < 16) {
    return v2(BasePos.X + ((Frame / 2) % 2) * TILE_SIZE, BasePos.Y);
  } else if (Frame < 24) {
    return v2(BasePos.X + TILE_SIZE, BasePos.Y);
  }

  return BasePos;
}


#endif
