#ifdef HEADER_PHASE
ITEM(spidertorso, normaltorso)
{
protected:
  virtual int GetClassAnimationFrames () const override;
  virtual v2 GetBitmapPos (int) const override;
};


#else


int spidertorso::GetClassAnimationFrames () const { return 16; }


v2 spidertorso::GetBitmapPos (int Frame) const {
  v2 BasePos = torso::GetBitmapPos(Frame);
  Frame &= 0xF;
  return v2(BasePos.X + ((Frame &~ 7) << 1), BasePos.Y);
}


#endif
