#ifdef HEADER_PHASE
ITEM(corpse, item)
{
 public:
  corpse () : Deceased(nullptr) {}
  corpse (const corpse &);
  virtual ~corpse ();

  virtual int GetOfferValue (int) const override;
  virtual double GetWeaponStrength () const override;
  virtual truth CanBeEatenByAI (ccharacter *) const override;
  virtual int GetStrengthValue () const override;
  character *GetDeceased () const;
  void SetDeceased (character *);
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth IsDestroyable (ccharacter *) const override;
  virtual sLong GetTruePrice () const override;
  virtual int GetMaterials () const override;
  virtual truth RaiseTheDead (character *) override;
  virtual void CalculateVolumeAndWeight () override;
  virtual void CalculateEmitation () override;
  virtual void SignalSpoil (material *) override;
  virtual truth CanBePiledWith (citem *, ccharacter *) const override;
  virtual int GetSpoilLevel () const override;
  virtual material *GetMaterial (int) const override;
  virtual head *Behead () override;
  virtual truth CanBeCloned () const override;
  virtual int GetAttachedGod () const override;
  virtual void PreProcessForBone () override;
  virtual void PostProcessForBone () override;
  virtual void FinalProcessForBone () override;
  virtual truth SuckSoul (character *, character * = 0) override;
  virtual character *TryNecromancy (character *) override;
  virtual void Cannibalize () override;
  virtual material *GetConsumeMaterial (ccharacter *, materialpredicate) const override;
  virtual truth DetectMaterial (cmaterial *) const override;
  virtual void SetLifeExpectancy (int, int) override;
  virtual void Be () override;
  virtual void SignalDisappearance () override;
  virtual truth IsValuable () const override;
  virtual truth AddRustLevelDescription (festring &, truth) const override;
  virtual truth Necromancy (character *) override;
  virtual int GetSparkleFlags () const override;
  virtual truth IsRusted () const override;
  virtual truth CanBeHardened (ccharacter *) const override;
  virtual truth IsCorpse () const override;

protected:
  virtual void SendToHellHelper () override;

  virtual void GenerateMaterials () override {}
  virtual col16 GetMaterialColorA (int) const override;
  virtual col16 GetMaterialColorB (int) const override;
  virtual alpha GetAlphaA (int) const override;
  virtual alpha GetAlphaB (int) const override;
  virtual truth ShowMaterial () const override;
  virtual void AddPostFix (festring &, int) const override;
  virtual v2 GetBitmapPos (int) const override;
  virtual int GetSize () const override;
  virtual int GetArticleMode () const override;
  virtual int GetRustDataA () const override;
  virtual truth AddStateDescription (festring &, truth) const override;
  virtual truth IsButchyCorpse (ccharacter *) const;

protected:
  character *Deceased;
};


#else


//==========================================================================
//
//  corpse::corpse
//
//==========================================================================
corpse::corpse (const corpse &Corpse) : mybase(Corpse) {
  Deceased = Corpse.Deceased->Duplicate();
  Deceased->SetMotherEntity(this);
}


//==========================================================================
//
//  corpse::~corpse
//
//==========================================================================
corpse::~corpse () {
  // we should never have the owner alive here.
  // the corpse should be properly sent to hell, and send its
  // owner to hell too.
  // as the owner is in hell, and could be deleted before the corpse,
  // there is nothing we could do here, and no checks are possible.
  // and again: we need the owner to be removed *AFTER* the corpse.
  // there is no way to ensure this with the current pool manager.
  // sic transit gloria mundi: revert to brutal deletion.
  delete Deceased;
}


//==========================================================================
//
//  corpse::SendToHellHelper
//
//  send our owner to hell. this is more correct than simply
//  deleting it in the destructor.
//
//==========================================================================
void corpse::SendToHellHelper () {
  #if 0
  festring sof;
  AddPostFix(sof, INDEFINITE);
  ConLogf("DEBUG: the corpse%s is sent to hell.", sof.CStr());
  #endif
  /* see the comment above.
  if (Deceased && Deceased->Exists()) {
    Deceased->SetMotherEntity(0);
    Deceased->SendToHell();
    // do not reset `Deceased`; the code may still need to get corpse materials.
    //Deceased = 0;
  }
  */
}


int corpse::GetMaterials () const { return 2; } //k8: why 2?
character *corpse::GetDeceased () const { return Deceased; }
truth corpse::AddRustLevelDescription (festring &, truth) const { return false; }
truth corpse::ShowMaterial () const { return false; }
truth corpse::CanBeHardened (ccharacter *) const { return false; }
truth corpse::IsRusted () const { return false; }
truth corpse::IsCorpse () const { return true; }
head* corpse::Behead () { return Deceased->Behead(); }
truth corpse::CanBeCloned () const { return GetDeceased()->CanBeCloned(); }
int corpse::GetAttachedGod () const { return GetDeceased()->GetTorso()->GetAttachedGod(); }
double corpse::GetWeaponStrength () const { return GetFormModifier() * GetDeceased()->GetTorso()->GetMainMaterial()->GetStrengthValue() * sqrt(GetDeceased()->GetTorso()->GetMainMaterial()->GetWeight()); }
int corpse::GetStrengthValue() const { return sLong(GetStrengthModifier()) * GetDeceased()->GetTorso()->GetMainMaterial()->GetStrengthValue() / 2000; }
col16 corpse::GetMaterialColorA (int) const { return GetDeceased()->GetTorso()->GetMainMaterial()->GetColor(); }
material *corpse::GetMaterial (int I) const { return GetDeceased()->GetTorso()->GetMaterial(I); }
void corpse::CalculateVolumeAndWeight () { Volume = Deceased->GetVolume(); Weight = Deceased->GetWeight(); }
void corpse::CalculateEmitation () { Emitation = Deceased->GetEmitation(); }
void corpse::SignalSpoil (material *) { GetDeceased()->Disappear(this, "spoil", &item::IsVeryCloseToSpoiling); }
void corpse::SignalDisappearance () { GetDeceased()->Disappear(this, "disappear", &item::IsVeryCloseToDisappearance); }
int corpse::GetArticleMode () const { return Deceased->LeftOversAreUnique() ? FORCE_THE : 0; }
alpha corpse::GetAlphaA (int) const { return GetDeceased()->GetTorso()->GetMainMaterial()->GetAlpha(); }
int corpse::GetSize() const { return Max(1, GetDeceased()->GetSize()); }
int corpse::GetRustDataA () const { return Deceased->GetTorso()->GetMainMaterial()->GetRustData(); }
truth corpse::DetectMaterial (const material *Material) const { return GetDeceased()->DetectMaterial(Material); }
void corpse::SetLifeExpectancy (int Base, int RandPlus) { Deceased->SetLifeExpectancy(Base, RandPlus); }


//==========================================================================
//
//  corpse::GetMaterialColorB
//
//==========================================================================
col16 corpse::GetMaterialColorB (int) const {
  torso *Torso = GetDeceased()->GetTorso();
  return Torso->IsAlive() ? material::GetDataBase(GetDeceased()->GetBloodMaterial())->Color : Torso->GetMainMaterial()->GetColor();
}


//==========================================================================
//
//  corpse::GetAlphaB
//
//==========================================================================
alpha corpse::GetAlphaB (int) const {
  torso *Torso = GetDeceased()->GetTorso();
  return Torso->IsAlive() ? 175 : Torso->GetMainMaterial()->GetAlpha();
}


//==========================================================================
//
//  corpse::GetSparkleFlags
//
//==========================================================================
int corpse::GetSparkleFlags () const {
  torso *Torso = GetDeceased()->GetTorso();
  material *Material = Torso->GetMainMaterial();
  return Material->IsSparkling() ? SPARKLING_A|(!Torso->IsAlive() ? SPARKLING_B : 0) : 0;
}


//==========================================================================
//
//  corpse::GetBitmapPos
//
//  FIXME: this should not be hardcoded!
//
//==========================================================================
v2 corpse::GetBitmapPos (int) const {
  if (GetDeceased()->GetSize() < 50) return v2(32, 64);
  if (GetDeceased()->GetSize() < 150) return v2(16, 192);
  return v2(16, 272);
}


//==========================================================================
//
//  corpse::Save
//
//==========================================================================
void corpse::Save (outputfile& SaveFile) const {
  item::Save(SaveFile);
  SaveFile << Deceased;
}


//==========================================================================
//
//  corpse::Load
//
//==========================================================================
void corpse::Load (inputfile& SaveFile) {
  item::Load(SaveFile);
  SaveFile >> Deceased;
  Deceased->SetMotherEntity(this);
  Enable();
}


//==========================================================================
//
//  corpse::IsButchyCorpse
//
//  can this corpse be butchered?
//
//==========================================================================
truth corpse::IsButchyCorpse (ccharacter *) const {
  character *origin = GetDeceased();
  // 'cmon, zombies are... zombies, there's nothing to salvage
  if (!origin || origin->IsZombie()) return false;
  if (!origin->GetFleshMaterial()) return false;
  if (GetSpoilLevel() > 0) return false;
  return true;
}


//==========================================================================
//
//  corpse::AddPostFix
//
//==========================================================================
void corpse::AddPostFix (festring &String, int) const {
  String << " of ";
  GetDeceased()->AddName(String, INDEFINITE);
}


//==========================================================================
//
//  corpse::AddStateDescription
//
//==========================================================================
truth corpse::AddStateDescription (festring &Name, truth Articled) const {
  truth res = false;
  if (Spoils()) {
    truth Hasted = true, Slowed = true;
    for (int c = 0; c < GetDeceased()->GetBodyParts(); ++c) {
      bodypart* BodyPart = GetDeceased()->GetBodyPart(c);
      if (BodyPart) {
        if (!(BodyPart->ItemFlags & HASTE)) Hasted = false;
        if (!(BodyPart->ItemFlags & SLOW)) Slowed = false;
      }
    }
    if ((Hasted | Slowed) && Articled) { res = true; Name << "a\x18";/*NBSP*/ }
    if (Hasted) { res = true; Name << "hasted "; }
    if (Slowed) { res = true; Name << "slowed "; }
  }
  return res;
}


//==========================================================================
//
//  corpse::GetOfferValue
//
//==========================================================================
int corpse::GetOfferValue (int Receiver) const {
  int OfferValue = 0;
  for (int c = 0; c < GetDeceased()->GetBodyParts(); ++c) {
    bodypart *BodyPart = GetDeceased()->GetBodyPart(c);
    if (BodyPart) OfferValue += BodyPart->GetOfferValue(Receiver);
  }
  return OfferValue;
}


//==========================================================================
//
//  corpse::CanBeEatenByAI
//
//==========================================================================
truth corpse::CanBeEatenByAI (ccharacter *Eater) const {
  bool res = true;
  IvanAssert(Eater);

  // intelligent monsters will not eat zombie corpses
  if (Eater->GetAttribute(INTELLIGENCE) >= Max(5, REFUSE_ZOMBIE_EATING_INT)) {
    character *owner = GetDeceased();
    if (!owner || owner->IsZombie()) {
      //ConLogf("%s refused to eat zombie corpse due to high Int.", Eater->CHAR_NAME(DEFINITE));
      res = false;
    }
  }

  for (int c = 0; res && c < GetDeceased()->GetBodyParts(); c += 1) {
    bodypart *BodyPart = GetDeceased()->GetBodyPart(c);
    if (BodyPart && !BodyPart->CanBeEatenByAI(Eater)) {
      res = false;
    }
  }

  return res;
}


//==========================================================================
//
//  corpse::SetDeceased
//
//==========================================================================
void corpse::SetDeceased (character *What) {
  IvanAssert(What);
  IvanAssert(!pool::IsInHell(What));
  Deceased = What;
  Deceased->SetMotherEntity(this);
  SignalVolumeAndWeightChange();
  SignalEmitationIncrease(Deceased->GetEmitation());
  UpdatePictures();
  Enable();
}


//==========================================================================
//
//  corpse::IsDestroyable
//
//==========================================================================
truth corpse::IsDestroyable (ccharacter *Char) const {
  for (int c = 0; c < GetDeceased()->GetBodyParts(); ++c) {
    bodypart *BodyPart = GetDeceased()->GetBodyPart(c);
    if (BodyPart && !BodyPart->IsDestroyable(Char)) return false;
  }
  return true;
}


//==========================================================================
//
//  corpse::GetTruePrice
//
//==========================================================================
sLong corpse::GetTruePrice () const {
  sLong Price = 0;
  for (int c = 0; c < GetDeceased()->GetBodyParts(); ++c) {
    bodypart *BodyPart = GetDeceased()->GetBodyPart(c);
    if (BodyPart) Price += BodyPart->GetTruePrice();
  }
  return Price;
}


//==========================================================================
//
//  corpse::RaiseTheDead
//
//==========================================================================
truth corpse::RaiseTheDead (character *Summoner) {
  if (Summoner && Summoner->IsPlayer()) game::DoEvilDeed(50);
  GetDeceased()->Enable();
  if (GetDeceased()->TryToRiseFromTheDead()) {
    v2 Pos = GetPos();
    RemoveFromSlot();
    GetDeceased()->SetMotherEntity(0);
    if (Summoner && GetDeceased()->CanTameWithResurrection(Summoner) && !GetDeceased()->IsPlayer()) {
      GetDeceased()->ChangeTeam(Summoner->GetTeam());
    }
    if (GetDeceased()->PutToOrNear(Pos, true)) {
      GetDeceased()->SignalStepFrom(0);
      Deceased = 0;
      SendToHell();
      return true;
    }
  }
  GetDeceased()->Disable();
  return false;
}


//==========================================================================
//
//  corpse::CanBePiledWith
//
//==========================================================================
truth corpse::CanBePiledWith (citem *Item, ccharacter *Viewer) const {
  if (GetType() != Item->GetType() || GetConfig() != Item->GetConfig() || GetWeight() != Item->GetWeight() ||
      Viewer->GetCWeaponSkillLevel(this) != Viewer->GetCWeaponSkillLevel(Item) ||
      Viewer->GetSWeaponSkillLevel(this) != Viewer->GetSWeaponSkillLevel(Item))
  {
    return false;
  }

  const corpse *Corpse = static_cast<const corpse *>(Item);

  if (Deceased->GetBodyParts() != Corpse->Deceased->GetBodyParts()) return false;

  for (int c = 0; c < Deceased->GetBodyParts(); ++c) {
    bodypart *BodyPart1 = Deceased->GetBodyPart(c);
    bodypart *BodyPart2 = Corpse->Deceased->GetBodyPart(c);
    if (!BodyPart1 && !BodyPart2) continue;
    if (!BodyPart1 || !BodyPart2 || !BodyPart1->CanBePiledWith(BodyPart2, Viewer)) return false;
  }

  if (Deceased->GetName(UNARTICLED) != Corpse->Deceased->GetName(UNARTICLED)) return false;

  return true;
}


//==========================================================================
//
//  corpse::GetSpoilLevel
//
//==========================================================================
int corpse::GetSpoilLevel () const {
  int FlyAmount = 0;
  for (int c = 0; c < GetDeceased()->GetBodyParts(); ++c) {
    bodypart *BodyPart = GetDeceased()->GetBodyPart(c);
    if (BodyPart) {
      FlyAmount = Max(FlyAmount, BodyPart->GetSpoilLevel());
    }
  }
  return FlyAmount;
}


//==========================================================================
//
//  corpse::PreProcessForBone
//
//==========================================================================
void corpse::PreProcessForBone () {
  item::PreProcessForBone();
  if (!Deceased->PreProcessForBone()) {
    RemoveFromSlot();
    SendToHell();
  }
}


//==========================================================================
//
//  corpse::PostProcessForBone
//
//==========================================================================
void corpse::PostProcessForBone () {
  item::PostProcessForBone();
  if (!Deceased->PostProcessForBone()) {
    RemoveFromSlot();
    SendToHell();
  }
}


//==========================================================================
//
//  corpse::FinalProcessForBone
//
//==========================================================================
void corpse::FinalProcessForBone () {
  item::FinalProcessForBone();
  Deceased->FinalProcessForBone();
}


//==========================================================================
//
//  corpse::SuckSoul
//
//==========================================================================
truth corpse::SuckSoul (character* Soul, character* Summoner) {
  v2 Pos = Soul->GetPos();
  if (Deceased->SuckSoul(Soul)) {
    Soul->Remove();
    character *Deceased = GetDeceased();
    if (RaiseTheDead(Summoner)) {
      Soul->SendToHell();
      return true;
    }
    Deceased->SetSoulID(Soul->GetID());
    Soul->PutTo(Pos);
    return false;
  }
  return false;
}


//==========================================================================
//
//  corpse::TryNecromancy
//
//==========================================================================
character *corpse::TryNecromancy (character *Summoner) {
  if (Summoner && Summoner->IsPlayer()) {
    game::DoEvilDeed(50);
  }

  character *Zombie = GetDeceased()->CreateZombie();
  v2 Pos = GetPos();

  if (Zombie) {
    RemoveFromSlot();
    Zombie->ChangeTeam(Summoner ? Summoner->GetTeam() : game::GetTeam(MONSTER_TEAM));
    if (Zombie->PutToOrNear(Pos, true)) {
      Zombie->SignalStepFrom(0);
      SendToHell();
    } else {
      //Zombie->Disable();
      //Zombie->SendToHell();
      // it is not registered anywhere, so it is safe to delete it
      Zombie->ChangeTeam(0);
      delete Zombie;
      Zombie = 0;
    }
  }

  return Zombie;
}


//==========================================================================
//
//  corpse::GetConsumeMaterial
//
//==========================================================================
material *corpse::GetConsumeMaterial (ccharacter *Consumer, materialpredicate Predicate) const {
  for (int c = GetDeceased()->GetBodyParts()-1; c >= 0; --c) {
    bodypart *BodyPart = GetDeceased()->GetBodyPart(c);
    if (BodyPart) {
      material *CM = BodyPart->GetConsumeMaterial(Consumer, Predicate);
      if (CM) return CM;
    }
  }
  return GetDeceased()->GetTorso()->GetConsumeMaterial(Consumer, Predicate);
}


//==========================================================================
//
//  corpse::Cannibalize
//
//==========================================================================
void corpse::Cannibalize () {
  item::Cannibalize();
  for (int c = 0; c < GetDeceased()->GetBodyParts(); ++c) {
    bodypart *BodyPart = GetDeceased()->GetBodyPart(c);
    if (BodyPart) BodyPart->Cannibalize();
  }
}


//==========================================================================
//
//  corpse::Be
//
//==========================================================================
void corpse::Be () {
  for (int c = 0; c < Deceased->GetBodyParts(); ++c) {
    bodypart *BodyPart = Deceased->GetBodyPart(c);
    if (BodyPart) BodyPart->Be();
  }
}


//==========================================================================
//
//  corpse::IsValuable
//
//==========================================================================
truth corpse::IsValuable () const {
  for (int c = 0; c < Deceased->GetBodyParts(); ++c) {
    bodypart *BodyPart = Deceased->GetBodyPart(c);
    if (BodyPart && BodyPart->IsValuable()) return true;
  }
  return false;
}


//==========================================================================
//
//  corpse::Necromancy
//
//==========================================================================
truth corpse::Necromancy (character *Necromancer) {
  if (Necromancer && Necromancer->IsPlayer()) {
    game::DoEvilDeed(50);
  }
  character *Zombie = GetDeceased()->CreateZombie();
  if (Zombie) {
    Zombie->ChangeTeam(Necromancer ? Necromancer->GetTeam() : game::GetTeam(MONSTER_TEAM));
    if (Zombie->PutToOrNear(GetPos(), true)) {
      RemoveFromSlot();
      SendToHell();
      if (Zombie->CanBeSeenByPlayer()) ADD_MESSAGE("%s rises back to cursed undead life.", Zombie->CHAR_DESCRIPTION(INDEFINITE));
      Zombie->SignalStepFrom(0);
      return true;
    }
    //Zombie->Disable();
    //Zombie->SendToHell();
    delete Zombie;
  }
  if (CanBeSeenByPlayer()) ADD_MESSAGE("%s vibrates for some time.", CHAR_NAME(DEFINITE));
  return false;
}


#endif
