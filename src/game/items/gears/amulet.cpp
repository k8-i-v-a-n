#ifdef HEADER_PHASE
ITEM(amulet, item)
{
public:
  virtual truth IsAmulet (ccharacter *) const override;
  virtual truth IsInCorrectSlot (int) const override;
  virtual truth IsLuxuryItem (ccharacter *) const override;

protected:
  virtual col16 GetMaterialColorB (int) const override;
};


#else


truth amulet::IsAmulet (ccharacter *) const { return true; }
truth amulet::IsLuxuryItem (ccharacter *) const { return true; }
truth amulet::IsInCorrectSlot (int I) const { return I == AMULET_INDEX; }
col16 amulet::GetMaterialColorB (int) const { return MakeRGB16(111, 64, 37); }


#endif
