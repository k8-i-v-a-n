#ifdef HEADER_PHASE
ITEM(armor, item)
{
public:
  virtual sLong GetPrice () const override;
  virtual void AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight=true) const override;
  virtual void AddInventoryEntry (ccharacter *, festring &, int, truth) const override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth IsArmor (ccharacter *) const override;
  virtual int GetEnchantment() const override;
  virtual void SetEnchantment (int) override;
  virtual void EditEnchantment (int) override;
  virtual int GetStrengthValue () const override;
  virtual truth CanBePiledWith (citem *, ccharacter *) const override;
  virtual int GetInElasticityPenalty (int) const override;
  virtual int GetCarryingBonus () const override;
  virtual truth IsFixableBySmith (ccharacter *) const override;
  virtual truth IsFixableByTailor (ccharacter *) const override;
  virtual truth IsUpgradeableBySmith (ccharacter *) const override;
  virtual truth IsUpgradeableByTailor (ccharacter *) const override;
  virtual truth AllowFluids () const override;
  virtual void CalculateEnchantment () override;
  virtual double GetTHVBonus () const override;
  virtual double GetDamageBonus () const override;
  virtual truth AllowDetailedDescription () const override;

protected:
  virtual void AddPostFix (festring &, int) const override;
  virtual void PostConstruct () override;

protected:
  int Enchantment;
};


#else


truth armor::AllowFluids () const { return true; }
truth armor::IsArmor (ccharacter *) const { return true; }
int armor::GetEnchantment () const { return Enchantment; }
truth armor::IsFixableBySmith (ccharacter *) const { return IsBroken() || IsRusted(); }
truth armor::IsFixableByTailor (ccharacter *) const { return IsBroken(); }
truth armor::IsUpgradeableBySmith (ccharacter *) const { return !IsBroken() && !IsRusted(); }
truth armor::IsUpgradeableByTailor (ccharacter *) const { return !IsBroken(); }
truth armor::AllowDetailedDescription () const { return true; }
int armor::GetCarryingBonus () const { return Enchantment << 1; }
double armor::GetTHVBonus () const { return Enchantment * .5; }
double armor::GetDamageBonus () const { return Enchantment; }


sLong armor::GetPrice () const {
  double StrengthValue = GetStrengthValue();
  return sLong(StrengthValue * StrengthValue * StrengthValue * 20 / sqrt(GetWeight()));
}


void armor::AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight) const {
  //Entry << " [\1C" << GetWeight() * Amount << "g\2, AV\x18\1Y" << GetStrengthValue() << "\2]";
  Entry << " [";
  if (includeWeight) {
    Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
    Entry << ", ";
  }
  Entry << "AV\x18\1Y" << GetStrengthValue() << "\2]";
}


void armor::AddInventoryEntry (ccharacter *CC, festring &Entry, int Amount,
                               truth ShowSpecialInfo) const
{
  if (Amount == 1) {
    AddName(Entry, INDEFINITE);
  } else {
    Entry << Amount << ' ';
    AddName(Entry, PLURAL);
  }
  if (ShowSpecialInfo) {
    AddSpecialInfo(CC, Entry, Amount, true);
  }
}


truth armor::CanBePiledWith (citem *Item, ccharacter *Viewer) const {
  return item::CanBePiledWith(Item, Viewer) && Enchantment == static_cast<const armor*>(Item)->Enchantment;
}


void armor::Save (outputfile &SaveFile) const {
  item::Save(SaveFile);
  SaveFile << Enchantment;
}


void armor::Load (inputfile &SaveFile) {
  item::Load(SaveFile);
  SaveFile >> Enchantment;
}


void armor::AddPostFix (festring &String, int Case) const {
  item::AddPostFix(String, Case);

  if ((Case & NO_FLUIDS) == 0) {
    if (Fluid && !DisableFluids) {
      String << " covered with ";
      fluid::AddFluidInfo(Fluid[0], String);
    }
  }

       if (Enchantment > 0) String << "\x18\1G+" << Enchantment << "\2";
  else if (Enchantment < 0) String << "\x18\1R" << Enchantment << "\2";
}


void armor::SetEnchantment (int Amount) {
  Enchantment = Amount;
  SignalEnchantmentChange();
}


void armor::EditEnchantment (int Amount) {
  Enchantment += Amount;
  SignalEnchantmentChange();
}


int armor::GetStrengthValue () const {
  return Max<sLong>(sLong(GetStrengthModifier()) * GetMainMaterial()->GetStrengthValue() / 2000 + Enchantment, 0);
}


void armor::PostConstruct () {
  Enchantment = GetBaseEnchantment();
}


int armor::GetInElasticityPenalty (int Attribute) const {
  return Attribute * GetInElasticityPenaltyModifier() / (GetMainMaterial()->GetFlexibility() * 100);
}


void armor::CalculateEnchantment () {
  Enchantment -= femath::LoopRoll(game::GetCurrentLevel()->GetEnchantmentMinusChance(), 5);
  Enchantment += femath::LoopRoll(game::GetCurrentLevel()->GetEnchantmentPlusChance(), 5);
  Enchantment -= femath::LoopRoll(GetEnchantmentMinusChance(), 5);
  Enchantment += femath::LoopRoll(GetEnchantmentPlusChance(), 5);
}


#endif
