#ifdef HEADER_PHASE
ITEM(belt, armor)
{
public:
  virtual sLong GetPrice () const override;
  virtual truth IsBelt (ccharacter *) const override;
  virtual int GetFormModifier () const override;
  virtual truth IsInCorrectSlot (int) const override;
  virtual col16 GetMaterialColorB (int Frame) const override;
};


#else


truth belt::IsBelt (ccharacter *) const { return true; }
col16 belt::GetMaterialColorB (int Frame) const { return GetMaterialColorA(Frame); }
sLong belt::GetPrice () const { return armor::GetPrice() * 5 + GetEnchantedPrice(Enchantment); }
truth belt::IsInCorrectSlot (int I) const { return I == BELT_INDEX; }
int belt::GetFormModifier () const { return item::GetFormModifier() * GetMainMaterial()->GetFlexibility(); }


#endif
