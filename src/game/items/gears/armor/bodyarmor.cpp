#ifdef HEADER_PHASE
ITEM(bodyarmor, armor)
{
public:
  virtual sLong GetPrice () const override;
  virtual truth IsBodyArmor (ccharacter *) const override;
  virtual truth IsInCorrectSlot (int) const override;

protected:
  virtual cchar *GetBreakVerb () const override;
  virtual truth AddAdjective (festring &, truth) const override;
  virtual cfestring& GetNameSingular () const override;
};


#else


truth bodyarmor::IsBodyArmor (ccharacter *) const { return true; }
sLong bodyarmor::GetPrice () const { return (armor::GetPrice() << 3) + GetEnchantedPrice(Enchantment); }
truth bodyarmor::IsInCorrectSlot (int I) const { return I == BODY_ARMOR_INDEX; }
cfestring& bodyarmor::GetNameSingular () const { return GetMainMaterial()->GetFlexibility() >= 5 ? item::GetFlexibleNameSingular() : item::GetNameSingular(); }
cchar* bodyarmor::GetBreakVerb () const { return GetMainMaterial()->GetFlexibility() >= 5 ? "is torn apart" : "breaks"; }


truth bodyarmor::AddAdjective (festring &String, truth Articled) const {
  if (IsBroken()) {
    if (Articled) String << "a ";
    if (GetMainMaterial()->GetFlexibility() >= 5) String << "torn"; else String << "broken";
    String << ' ';
    return true;
  }
  return false;
}


#endif
