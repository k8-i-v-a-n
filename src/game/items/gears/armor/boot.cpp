#ifdef HEADER_PHASE
ITEM(boot, armor)
{
public:
  virtual sLong GetPrice () const override;
  virtual truth IsBoot (ccharacter *) const override;
  virtual truth IsInCorrectSlot (int) const override;
};


#else


truth boot::IsBoot (ccharacter *) const { return true; }
sLong boot::GetPrice () const { return armor::GetPrice() / 5 + GetEnchantedPrice(Enchantment); }
truth boot::IsInCorrectSlot (int I) const { return I == RIGHT_BOOT_INDEX || I == LEFT_BOOT_INDEX; }


#endif
