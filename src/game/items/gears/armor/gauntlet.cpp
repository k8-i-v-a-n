#ifdef HEADER_PHASE
ITEM(gauntlet, armor)
{
public:
  virtual sLong GetPrice () const override;
  virtual truth IsGauntlet (ccharacter *) const override;
  virtual truth IsInCorrectSlot (int) const override;
};


#else


truth gauntlet::IsGauntlet (ccharacter *) const { return true; }
sLong gauntlet::GetPrice () const { return armor::GetPrice() / 3 + GetEnchantedPrice(Enchantment); }
truth gauntlet::IsInCorrectSlot (int I) const { return I == RIGHT_GAUNTLET_INDEX || I == LEFT_GAUNTLET_INDEX; }


#endif
