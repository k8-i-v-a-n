#ifdef HEADER_PHASE
ITEM(shield, armor)
{
public:
  virtual sLong GetPrice () const override;
  virtual truth IsShield (ccharacter *) const override;
  virtual void AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight=true) const override;
  virtual void AddInventoryEntry (ccharacter *, festring &, int, truth) const override;
};


#else


truth shield::IsShield (ccharacter *) const { return true; }


void shield::AddSpecialInfo (ccharacter *Viewer, festring &Entry, int Amount, truth includeWeight) const {
  //Entry << " [\1C" << GetWeight() << "g\2, "  << GetBaseBlockValueDescription();
  Entry << " [";
  if (includeWeight) {
    Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
    Entry << ", ";
  }
  Entry << "AV \1Y" << GetStrengthValue() << "\2";
  Entry << ", " << GetBaseBlockValueDescription();
  if (!IsBroken()) Entry << ", " << GetStrengthValueDescription();
  if (Viewer) {
    int CWeaponSkillLevel = Viewer->GetCWeaponSkillLevel(this);
    int SWeaponSkillLevel = Viewer->GetSWeaponSkillLevel(this);
    if (CWeaponSkillLevel || SWeaponSkillLevel) {
      Entry << ", skill\x18\1C" << CWeaponSkillLevel << '/' << SWeaponSkillLevel;
    }
  }
  Entry << "\2]";
}


// never piled
void shield::AddInventoryEntry (ccharacter *Viewer, festring &Entry, int Amount, truth ShowSpecialInfo) const {
  AddName(Entry, INDEFINITE);
  if (ShowSpecialInfo) {
    AddSpecialInfo(Viewer, Entry, Amount, true);
  }
}


/* temporary... */
sLong shield::GetPrice () const {
  double StrengthValue = GetStrengthValue();
  return sLong(sqrt(GetBaseBlockValue()) * StrengthValue * StrengthValue) + item::GetPrice();
}


#endif
