#ifdef HEADER_PHASE
ITEM(chastitybelt, lockablebelt)
{
public:
  virtual int GetFormModifier () const override;
};


#else


int chastitybelt::GetFormModifier () const { return item::GetFormModifier(); }


#endif
