#ifdef HEADER_PHASE
ITEM(decosadshirt, bodyarmor)
{
public:
  decosadshirt ();

  virtual void Be () override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth IsDecosAdShirt (ccharacter *) const override;

  feuLong GetEquippedTicks ();
  void SetEquippedTicks (feuLong What);

protected:
  virtual truth CalculateHasBe () const override;

protected:
  feuLong EquippedTicks;
};


#else


truth decosadshirt::IsDecosAdShirt (ccharacter *) const { return true; }
truth decosadshirt::CalculateHasBe () const { return true; }

feuLong decosadshirt::GetEquippedTicks () { return EquippedTicks; }
void decosadshirt::SetEquippedTicks (feuLong What) { EquippedTicks = What; }


decosadshirt::decosadshirt () : EquippedTicks(0) {
  Enable();
}


void decosadshirt::Be () {
  if (PLAYER->Equips(this)) ++EquippedTicks;
  bodyarmor::Be();
}


void decosadshirt::Save (outputfile &SaveFile) const {
  bodyarmor::Save(SaveFile);
  SaveFile << EquippedTicks;
}


void decosadshirt::Load (inputfile &SaveFile) {
  bodyarmor::Load(SaveFile);
  SaveFile >> EquippedTicks;
  Enable();
}


#endif
