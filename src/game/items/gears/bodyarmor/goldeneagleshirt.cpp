#ifdef HEADER_PHASE
ITEM(goldeneagleshirt, bodyarmor)
{
public:
  virtual truth IsGoldenEagleShirt () const override;
  virtual truth IsConsumable () const override;
  virtual truth AllowAlphaEverywhere () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth goldeneagleshirt::IsGoldenEagleShirt () const { return true; }
truth goldeneagleshirt::IsConsumable () const { return false; }
truth goldeneagleshirt::AllowAlphaEverywhere () const { return true; }
int goldeneagleshirt::GetClassAnimationFrames () const { return 32; }
col16 goldeneagleshirt::GetOutlineColor (int) const { return MakeRGB16(0, 255, 255); }


alpha goldeneagleshirt::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


#endif
