#ifdef HEADER_PHASE
ITEM(goldenjaguarshirt, bodyarmor)
{
public:
  virtual truth IsConsumable () const override;
  virtual truth AllowAlphaEverywhere () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth goldenjaguarshirt::IsConsumable () const { return false; }
truth goldenjaguarshirt::AllowAlphaEverywhere () const { return true; }
int goldenjaguarshirt::GetClassAnimationFrames () const { return 32; }
col16 goldenjaguarshirt::GetOutlineColor (int) const { return MakeRGB16(255, 255, 128); }


alpha goldenjaguarshirt::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


#endif
