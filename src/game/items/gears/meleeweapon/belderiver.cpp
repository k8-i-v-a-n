#ifdef HEADER_PHASE
ITEM(belderiver, meleeweapon)
{
public:
  virtual truth AllowAlphaEverywhere () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth belderiver::AllowAlphaEverywhere () const { return true; }
int belderiver::GetClassAnimationFrames () const { return 32; }
col16 belderiver::GetOutlineColor (int) const { return MakeRGB16(180, 50, 50); }


alpha belderiver::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


#endif
