#ifdef HEADER_PHASE
ITEM(cleaver, meleeweapon)
{
public:
  virtual truth Apply (character *) override;
  virtual truth IsAppliable (ccharacter *) const override;
};


#else


truth cleaver::IsAppliable (ccharacter *Who) const {
  return (!IsBroken() && Who->CanWield());
}


truth cleaver::Apply (character *User) {
  if (IsBroken()) {
    ADD_MESSAGE("%s is totally broken.", CHAR_NAME(DEFINITE));
    return false;
  }

  itemvector Butchy;

  stack *Stack = 0;
  {
    lsquare *Square = User->GetLSquareUnder();
    Stack = (Square ? Square->GetStack() : nullptr);
    if (Stack) {
      sortdata SortData(Butchy, User, false, &item::IsButchyCorpse);
      Stack->SortAllItems(SortData);
    }
  }

  item *Corpse = 0;

  if (Butchy.empty()) {
    ADD_MESSAGE("There is nothing you can butch.");
    return false;
  }

  IvanAssert(Stack);
  Corpse = Stack->DrawContents(User, CONST_S("What do you want to butch?"),
                               NO_MULTI_SELECT /*| NONE_AS_CHOICE*/, &item::IsButchyCorpse);
  if (!Corpse) return false;
  if (!Corpse->IsCorpse()) return false;

  corpse *cc = (corpse *)Corpse;
  character *owner = cc->GetDeceased();
  if (!owner) {
    ConLogf("ERROR: no corpse owner!");
    return false;
  }

  if (owner->IsZombie()) {
    ADD_MESSAGE("This zombie is too rot to be butchered.");
    return false;
  }

  int FleshCfg = owner->GetFleshMaterial();
  if (!FleshCfg) {
    ADD_MESSAGE("You don't know how to butch this corpse.");
    return false;
  }

  int vol = Corpse->GetVolume() / 3;
  if (vol < 500) {
    ADD_MESSAGE("There is nothing to butch in this corpse.");
    return false;
  }

  if (Corpse->GetSpoilLevel() > 0) {
    ADD_MESSAGE("The corpse is too spoiled.");
    return false;
  }

  material *FleshMat = MAKE_MATERIAL(FleshCfg);
  if (!FleshMat->IsFlesh()) {
    ADD_MESSAGE("Only flesh can be butched.");
    delete FleshMat;
    return false;
  }

  delete FleshMat;

  User->SwitchToButchery(this, Corpse);
  User->DexterityAction(5);
  return true;
}


#endif
