#ifdef HEADER_PHASE
ITEM(demonhead, meleeweapon)
{
public:
  virtual truth AllowAlphaEverywhere () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth demonhead::AllowAlphaEverywhere () const { return true; }
int demonhead::GetClassAnimationFrames () const { return 32; }
col16 demonhead::GetOutlineColor (int) const { return MakeRGB16(255, 0, 0); }


alpha demonhead::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


#endif
