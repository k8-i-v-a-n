#ifdef HEADER_PHASE
ITEM(gorovitsweapon, meleeweapon)
{
public:
  virtual truth IsGorovitsFamilyRelic () const override;
  virtual truth AllowAlphaEverywhere () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth gorovitsweapon::IsGorovitsFamilyRelic () const { return true; }
truth gorovitsweapon::AllowAlphaEverywhere () const { return true; }
int gorovitsweapon::GetClassAnimationFrames () const { return 32; }
col16 gorovitsweapon::GetOutlineColor (int) const { return MakeRGB16(255, 0, 0); }


alpha gorovitsweapon::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


#endif
