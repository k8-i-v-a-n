#ifdef HEADER_PHASE
ITEM(justifier, meleeweapon)
{
public:
  virtual truth AllowAlphaEverywhere() const override;

protected:
  virtual int GetClassAnimationFrames() const override;
  virtual col16 GetOutlineColor(int) const override;
  virtual alpha GetOutlineAlpha(int) const override;
};


#else


truth justifier::AllowAlphaEverywhere () const { return true; }
int justifier::GetClassAnimationFrames () const { return 32; }
col16 justifier::GetOutlineColor (int) const { return MakeRGB16(0, 255, 0); }


alpha justifier::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


#endif
