#ifdef HEADER_PHASE
ITEM(loricatushammer, meleeweapon)
{
public:
  virtual truth AllowAlphaEverywhere () const override;

protected:
  virtual int GetClassAnimationFrames() const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth loricatushammer::AllowAlphaEverywhere () const { return true; }
int loricatushammer::GetClassAnimationFrames () const { return 32; }
col16 loricatushammer::GetOutlineColor (int) const { return MakeRGB16(0, 0, 255); }


alpha loricatushammer::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


#endif
