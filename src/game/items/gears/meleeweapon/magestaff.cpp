#ifdef HEADER_PHASE
ITEM(magestaff, meleeweapon)
{
public:
  magestaff ();
  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual truth Zap (character *, v2, int) override;
  virtual truth IsZappable (const character *) const override;
  virtual void FinalProcessForBone () override;
  virtual int GetCooldown (int, character *);
protected:
  feuLong LastUsed;
};


#else

magestaff::magestaff () : LastUsed(0) {}
truth magestaff::IsZappable (const character *) const { return true; }


void magestaff::Save(outputfile &SaveFile) const {
  meleeweapon::Save(SaveFile);
  SaveFile << LastUsed;
}


void magestaff::Load (inputfile &SaveFile) {
  meleeweapon::Load(SaveFile);
  SaveFile >> LastUsed;
}


void magestaff::FinalProcessForBone () {
  meleeweapon::FinalProcessForBone();
  LastUsed = 0;
}


int magestaff::GetCooldown (int BaseCooldown, character* User) {
  int Attribute = User->GetMana();
  if (Attribute > 1) {
    return BaseCooldown / log10(Attribute);
  } else {
    return BaseCooldown / 0.20;
  }
}


truth magestaff::Zap (character *Zapper, v2, int Direction) {
  int Cooldown;
  if (GetConfig() == ROYAL_STAFF) {
    Cooldown = 2500;
  } else {
    Cooldown = 1000;
  }

  if (!LastUsed || game::GetTick() - LastUsed >= (feuLong)GetCooldown(Cooldown, Zapper)) {
    LastUsed = game::GetTick();
    ADD_MESSAGE("You zap %s!", CHAR_NAME(DEFINITE));
    Zapper->EditExperience(MANA, 150, 1 << 10);

    // Prepare for magical staves with different effects.
    if (GetConfig() == ROYAL_STAFF) {
      // Infinite polymorph, but with range of only 1.
      beamdata Beam(
        Zapper,
        CONST_S("killed by ") + GetName(DEFINITE) + " zapped @bk",
        Zapper->GetPos(),
        GetBeamColor(),
        GetBeamEffect(),
        Direction,
        GetBeamRange(),
        0,
        this
      );
      (GetLevel()->*level::GetBeam(PARTICLE_BEAM))(Beam);
    } else {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s releases a shower of polychromatic sparks.", CHAR_NAME(DEFINITE));
      }
    }
  } else {
    ADD_MESSAGE("Nothing happens.");
  }

  return true;
}

#endif
