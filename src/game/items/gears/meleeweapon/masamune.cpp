#ifdef HEADER_PHASE
ITEM(masamune, meleeweapon)
{
public:
  virtual truth HitEffect (character *, character *, v2, int, int, truth) override;
  virtual truth AllowAlphaEverywhere () const override;
  virtual truth IsMasamune () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth masamune::IsMasamune () const { return true; }
truth masamune::AllowAlphaEverywhere () const { return true; }
int masamune::GetClassAnimationFrames () const { return 32; }
col16 masamune::GetOutlineColor (int) const { return MakeRGB16(0, 0, 255); }


alpha masamune::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50+(Frame*(31-Frame)>>1);
}


truth masamune::HitEffect (character *Enemy, character *Hitter, v2 HitPos, int BodyPartIndex, int Direction, truth BlockedByArmour) {
  truth BaseSuccess = meleeweapon::HitEffect(Enemy, Hitter, HitPos, BodyPartIndex, Direction, BlockedByArmour);

  #if 0 // old code
  if (!IsBroken() && Enemy->IsEnabled() && !RAND_N(4)) {
    if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer())
      ADD_MESSAGE("%s Masamune's slash cuts %s so deep you thought it was cut in half for a moment.", Hitter->CHAR_POSSESSIVE_PRONOUN, Enemy->CHAR_DESCRIPTION(DEFINITE));
    return Enemy->ReceiveBodyPartDamage(Hitter, 4 + (RAND_N(4)), PHYSICAL_DAMAGE, BodyPartIndex, Direction) || BaseSuccess;
  }
  #else
  // new code
  if (!IsBroken() && Enemy->IsEnabled()) {
    // Special effect against chaotic creatures.
    bool IsEvil = false;

    if (Enemy->IsPlayer() && game::GetPlayerAlignment() < 0) {
      IsEvil = true;
    } else if(!Enemy->IsPlayer()) {
      const int agod = Enemy->GetAttachedGod();
      if (agod == MELLIS ||
          agod == CLEPTIA ||
          agod == NEFAS ||
          agod == SCABIES ||
          agod == INFUSCOR ||
          agod == CRUENTUS ||
          agod == MORTIFER)
      {
        IsEvil = true;
      }
    }

    if (IsEvil) {
      for(int c = 0; c < STATES; ++c) {
        // Remove most temporary status effects, leaving only some negative ones.
        if (((1 << c) != SLOW) && ((1 << c) != POISONED) &&
            ((1 << c) != PANIC) && ((1 << c) != CONFUSED) &&
            ((1 << c) != TELEPORT_LOCK) && ((1 << c) != PARASITE_TAPE_WORM) &&
            ((1 << c) != PARASITE_MIND_WORM))
        {
          Enemy->DeActivateTemporaryState(1 << c);
          if (!IsEnabled()) break;
        }
      }

      // Terrify the evil doer and prevent them from escaping.
      if (!Enemy->TemporaryStateIsActivated(PANIC) && !Enemy->TemporaryStateIsActivated(FEARLESS) &&
          Enemy->GetPanicLevel() > RAND_N(50 - Min(Hitter->GetMana(), 49)))
      {
        Enemy->BeginTemporaryState(PANIC, 200 + RAND_N(100));
      }

      if (!Enemy->TemporaryStateIsActivated(TELEPORT_LOCK)) {
        Enemy->BeginTemporaryState(TELEPORT_LOCK, 200 + RAND_N(500));
      }

      if (Hitter) {
        if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s %s rebukes %s.", Hitter->CHAR_POSSESSIVE_PRONOUN,
                      CHAR_NAME(UNARTICLED), Enemy->CHAR_DESCRIPTION(DEFINITE));
        }
      } else {
        if (Enemy->IsPlayer() || Enemy->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s rebukes %s.", CHAR_NAME(DEFINITE), Enemy->CHAR_DESCRIPTION(DEFINITE));
        }
      }
    } else if (!RAND_N(4)) {
      // Striking a good creature.
      ADD_MESSAGE("%s seems reluctant to strike %s.", CHAR_NAME(DEFINITE), Enemy->CHAR_DESCRIPTION(DEFINITE));
    }

    //k8: old effect retained
    if (!RAND_N(5)) {
      if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s Masamune's slash cuts %s so deep you thought it was cut in half for a moment.",
                    Hitter->CHAR_POSSESSIVE_PRONOUN, Enemy->CHAR_DESCRIPTION(DEFINITE));
      }
      return Enemy->ReceiveBodyPartDamage(Hitter, 4 + (RAND_N(4)), PHYSICAL_DAMAGE, BodyPartIndex, Direction) || BaseSuccess;
    }
  }
  #endif

  return BaseSuccess;
}



#endif
