#ifdef HEADER_PHASE
ITEM(muramasa, meleeweapon)
{
public:
  virtual truth HitEffect (character *, character *, v2, int, int, truth) override;
  virtual truth AllowAlphaEverywhere() const override;
  virtual truth IsMuramasa () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth muramasa::IsMuramasa () const { return true; }

truth muramasa::AllowAlphaEverywhere () const { return true; }
int muramasa::GetClassAnimationFrames () const { return 32; }
col16 muramasa::GetOutlineColor (int) const { return MakeRGB16(255, 0, 0); }


alpha muramasa::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


truth muramasa::HitEffect (character *Enemy, character *Hitter, v2 HitPos, int BodyPartIndex, int Direction, truth BlockedByArmour) {
  truth BaseSuccess = meleeweapon::HitEffect(Enemy, Hitter, HitPos, BodyPartIndex, Direction, BlockedByArmour);

  #if 0 // old muramasa
  if (!IsBroken() && Enemy->IsEnabled() && !RAND_N(5)) {
    if (Hitter->IsPlayer()) game::DoEvilDeed(10);
    if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s Muramasa's life-draining energies swallow %s!", Hitter->CHAR_POSSESSIVE_PRONOUN, Enemy->CHAR_DESCRIPTION(DEFINITE));
    }
    return Enemy->ReceiveBodyPartDamage(Hitter, 10 + (RAND_N(11)), DRAIN, BodyPartIndex, Direction) || BaseSuccess;
  }
  #else
  // from comm. fork
  if (!IsBroken() && Enemy->IsEnabled()) {
    // Special effect against lawful and neutral creatures.
    bool IsGoodly = false;

    if (Enemy->IsPlayer() && game::GetPlayerAlignment() < 0) {
      IsGoodly = true;
    } else if (!Enemy->IsPlayer()) {
      const int agod = Enemy->GetAttachedGod();
      if (agod == VALPURUS ||
          agod == LEGIFER ||
          agod == ATAVUS ||
          agod == DULCIS ||
          agod == SEGES ||
          agod == SOPHOS ||
          agod == SILVA ||
          agod == LORICATUS)
      {
        IsGoodly = true;
      }
    }

    if (IsGoodly) {
      if (!RAND_N(10)) {
        switch (RAND_N(7)) {
          case 0: Enemy->BeginTemporaryState(LYCANTHROPY, 6000 + RAND_N(2000)); break;
          case 1: Enemy->BeginTemporaryState(VAMPIRISM, 5000 + RAND_N(2500)); break;
          case 2: Enemy->BeginTemporaryState(PARASITE_TAPE_WORM, 6000 + RAND_N(3000)); break;
          case 3: Enemy->BeginTemporaryState(PARASITE_MIND_WORM, 400 + RAND_N(200)); break;
          case 4: Enemy->BeginTemporaryState(HICCUPS, 1000 + RAND_N(2000)); break;
          default: Enemy->GainIntrinsic(LEPROSY); break;
        }
      } else {
        switch (RAND_N(3)) {
          case 0: Enemy->BeginTemporaryState(SLOW, 400 + RAND_N(200)); break;
          case 1: Enemy->BeginTemporaryState(POISONED, 80 + RAND_N(40)); break;
          case 2: Enemy->BeginTemporaryState(CONFUSED, 400 + RAND_N(1000)); break;
        }
      }

      if (Hitter) {
        if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s %s defiles %s.", Hitter->CHAR_POSSESSIVE_PRONOUN,
                      CHAR_NAME(UNARTICLED), Enemy->CHAR_DESCRIPTION(DEFINITE));
        }
      } else {
        if (Enemy->IsPlayer() || Enemy->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s defiles %s.", CHAR_NAME(DEFINITE), Enemy->CHAR_DESCRIPTION(DEFINITE));
        }
      }
    } else if (!RAND_N(4)) {
      // Striking a chaotic creature.
      ADD_MESSAGE("%s seems reluctant to strike %s.", CHAR_NAME(DEFINITE), Enemy->CHAR_DESCRIPTION(DEFINITE));
    }

    //k8: old effect retained
    if (!RAND_N(5)) {
      // Life drain from chaotic creature is "evil"
      if (!IsGoodly && Hitter->IsPlayer()) {
        game::DoEvilDeed(10);
      }
      if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s Muramasa's life-draining energies swallow %s!",
                    Hitter->CHAR_POSSESSIVE_PRONOUN, Enemy->CHAR_DESCRIPTION(DEFINITE));
      }
      return Enemy->ReceiveBodyPartDamage(Hitter, 10 + (RAND_N(11)), DRAIN, BodyPartIndex, Direction) || BaseSuccess;
    }
  }
  #endif

  return BaseSuccess;
}


#endif
