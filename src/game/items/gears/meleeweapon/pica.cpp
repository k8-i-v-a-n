#ifdef HEADER_PHASE
ITEM(pica, meleeweapon) {
public:
  pica ();
  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual truth Zap (character *, v2, int) override;
  virtual truth IsZappable (const character *) const override;
  virtual void FinalProcessForBone () override;
  virtual int GetCooldown (int, character *);

protected:
  feuLong LastUsed;
};


#else

pica::pica () : LastUsed(0) {}
truth pica::IsZappable (const character *) const { return true; }


void pica::Save (outputfile &SaveFile) const {
  meleeweapon::Save(SaveFile);
  SaveFile << LastUsed;
}


void pica::Load (inputfile &SaveFile) {
  meleeweapon::Load(SaveFile);
  SaveFile >> LastUsed;
}


void pica::FinalProcessForBone () {
  meleeweapon::FinalProcessForBone();
  LastUsed = 0;
}


int pica::GetCooldown (int BaseCooldown, character *User) {
  int Attribute = User->GetMana();
  if (Attribute > 1) {
    return BaseCooldown / log10(Attribute);
  } else {
    return BaseCooldown / 0.20;
  }
}


truth pica::Zap (character *Zapper, v2, int Direction) {
  if (!IsBroken() && (!LastUsed || game::GetTick() - LastUsed >= (feuLong)GetCooldown(50, Zapper))) {
    LastUsed = game::GetTick();
    ADD_MESSAGE("You zap %s!", CHAR_NAME(DEFINITE));
    Zapper->EditExperience(MANA, 50, 1 << 10);
    for (int i = 0; i < RAND_N(Max(GetEnchantment(), 1)) + 1; i += 1) {
      meleeweapon *ToBeThrown = meleeweapon::Spawn(DAGGER);
      ToBeThrown->InitMaterials(MAKE_MATERIAL(STAR_METAL), MAKE_MATERIAL(MOON_SILVER), true);
      ToBeThrown->SetEnchantment(GetEnchantment());
      ToBeThrown->SetLifeExpectancy(50, Zapper->GetMana());
      Zapper->ReceiveItemAsPresent(ToBeThrown);
      ToBeThrown->Fly(Zapper, Direction, Zapper->GetMana());
    }
  } else {
    ConLogf("Pica cooldown: %u (left: %u)", GetCooldown(50, Zapper),
            (game::GetTick() - LastUsed) - (feuLong)GetCooldown(50, Zapper));
    ADD_MESSAGE("Nothing happens.");
  }
  return true;
}


#endif
