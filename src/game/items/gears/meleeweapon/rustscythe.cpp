#ifdef HEADER_PHASE
ITEM(rustscythe, meleeweapon)
{
public:
  virtual truth HitEffect (character *, character *, v2, int, int, truth) override;
  virtual void BlockEffect (character *, character *, item *, int Type) override;
};


#else


//==========================================================================
//
//  rustscythe::HitEffect
//
//==========================================================================
truth rustscythe::HitEffect (character* Enemy, character* Hitter, v2 HitPos,
                             int BodyPartIndex, int Direction, truth BlockedByArmour)
{
  truth BaseSuccess = meleeweapon::HitEffect(Enemy, Hitter, HitPos, BodyPartIndex, Direction, BlockedByArmour);

  if (!IsBroken() && Enemy->IsEnabled() && Enemy->IsHumanoid()) {
    bodypart *BodyPartHit = Enemy->GetBodyPart(BodyPartIndex);
    item *MainArmor = 0;
    int xidx1 = -1;
    switch (BodyPartIndex) {
      case TORSO_INDEX: xidx1 = BODY_ARMOR_INDEX; break;
      case HEAD_INDEX: xidx1 = HELMET_INDEX; break;
      case RIGHT_ARM_INDEX: xidx1 = (RAND_2 ? RIGHT_WIELDED_INDEX : RIGHT_GAUNTLET_INDEX); break;
      case LEFT_ARM_INDEX: xidx1 = (RAND_2 ? LEFT_WIELDED_INDEX : LEFT_GAUNTLET_INDEX); break;
      case GROIN_INDEX: xidx1 = BELT_INDEX; break;
      case RIGHT_LEG_INDEX: xidx1 = RIGHT_BOOT_INDEX; break;
      case LEFT_LEG_INDEX: xidx1 = LEFT_BOOT_INDEX; break;
    }

    if (xidx1 >= 0) MainArmor = Enemy->GetEquipment(xidx1);

    if (MainArmor/* && BlockedByArmor */) {
      MainArmor->TryToRust(10000000);
    } else if (BodyPartHit) {
      BodyPartHit->TryToRust(10000000);
    }
  }

  return BaseSuccess;
}


//==========================================================================
//
//  rustscythe::BlockEffect
//
//==========================================================================
void rustscythe::BlockEffect (character *Blocker, character *Attacker, item *Weapon, int Type) {
  if (!IsBroken() && Weapon) {
    Weapon->TryToRust(10000000);
  }
}


#endif
