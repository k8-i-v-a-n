#ifdef HEADER_PHASE
ITEM(sharpkatana, meleeweapon)
{
public:
  virtual truth HitEffect (character *, character *, v2, int, int, truth);
};


#else


truth sharpkatana::HitEffect (character *Enemy, character *Hitter, v2 HitPos,
                              int BodyPartIndex, int Direction, truth BlockedByArmour)
{
  truth BaseSuccess = meleeweapon::HitEffect(Enemy, Hitter, HitPos, BodyPartIndex, Direction, BlockedByArmour);

  if (!IsBroken() && Enemy->IsEnabled() && Enemy->IsHumanoid() &&
      (!Enemy->IsUnique() || Enemy->IsPlayer()))
  {
    bodypart *ToBeSevered = Enemy->GetBodyPart(BodyPartIndex);

    if (ToBeSevered && Enemy->BodyPartCanBeSevered(BodyPartIndex)) {
      if (Enemy->IsPlayer()) {
        ADD_MESSAGE("Your %s is severed off!", ToBeSevered->GetBodyPartName().CStr());
      } else if (Enemy->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s %s is severed off!",
                    Enemy->GetPossessivePronoun().CStr(), ToBeSevered->GetBodyPartName().CStr());
      }

      item *Severed = Enemy->SevereBodyPart(BodyPartIndex);
      Enemy->SendNewDrawRequest();

      if (Severed) {
        Enemy->GetStack()->AddItem(Severed);
        Severed->DropEquipment();
      } else if (Enemy->IsPlayer() || Enemy->CanBeSeenByPlayer()) {
        ADD_MESSAGE("It vanishes.");
      }

      if (Enemy->IsPlayer()) {
        game::AskForEscPress(CONST_S("Bodypart severed!"));
      }
    }

    return true;
  } else {
    return BaseSuccess;
  }
}


#endif
