#ifdef HEADER_PHASE
ITEM(taiaha, meleeweapon)
{
public:
  taiaha ();

  virtual truth AllowAlphaEverywhere () const override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void ChargeFully (character *) override;
  virtual truth IsAppliable (ccharacter *) const override;
  virtual truth IsZappable (ccharacter *) const override;
  virtual truth IsChargeable (ccharacter *) const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth Zap (character *, v2, int) override;
  virtual void AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight=true) const override;
  virtual void AddInventoryEntry (ccharacter *, festring &, int, truth) const override;
  virtual truth IsExplosive () const override;
  virtual void BreakEffect (character *, cfestring &) override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
  virtual void PostConstruct () override;

protected:
  int Charges;
  int TimesUsed;
};


#else


truth taiaha::AllowAlphaEverywhere () const { return true; }
void taiaha::ChargeFully (character *) { TimesUsed = 0; }
truth taiaha::IsAppliable (ccharacter *) const { return false; }
truth taiaha::IsZappable (ccharacter *) const { return true; }
truth taiaha::IsChargeable (ccharacter *) const { return true; }
truth taiaha::IsExplosive () const { return true; }
int taiaha::GetClassAnimationFrames () const { return !IsBroken() ? 128 : 1; }


taiaha::taiaha () : Charges(0), TimesUsed(0) {
}


void taiaha::Save (outputfile &SaveFile) const {
  meleeweapon::Save(SaveFile);
  SaveFile << TimesUsed << Charges;
}


void taiaha::Load (inputfile &SaveFile) {
  meleeweapon::Load(SaveFile);
  SaveFile >> TimesUsed >> Charges;
}


truth taiaha::Zap (character *Zapper, v2, int Direction) {
  //k8: dunno. broken taiaha still works as a ranged weapon.
  //k8: for me, it makes taiahas more valuable.
  if (Charges <= TimesUsed) {
    ADD_MESSAGE("Nothing happens.");
    return true;
  }

  Zapper->EditExperience(PERCEPTION, 150, 1 << 10);
  int TaiahaBeamEffect = RAND_4;

  // Just hard-code this
  beamdata Beam (
    Zapper,
    CONST_S("killed by ") + GetName(INDEFINITE) + " zapped @bk",
    Zapper->GetPos(),
    GREEN, //was GetBeamColor()
    // k8: was `(RAND() & 2)`
    TaiahaBeamEffect ? (RAND_2 ? BEAM_FIRE_BALL : BEAM_STRIKE) : BEAM_LIGHTNING, //was GetBeamEffect()
    Direction,
    15, // 10 is the lowest beamrange out of the three
    0, //was GetSpecialParameters()
    this
  );
  (GetLevel()->*level::GetBeam(!TaiahaBeamEffect))(Beam); // BeamStyle = !TaiahaBeamEffect;
  ++TimesUsed;
  return true;
}


void taiaha::AddSpecialInfo (ccharacter *Viewer, festring &Entry, int Amount, truth includeWeight) const {
  //Entry << " [\1C" << GetWeight() << "g";
  Entry << " [";
  if (includeWeight) {
    Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
    Entry << ", ";
  }
  Entry << "DAM\x18\1Y" << GetBaseMinDamage() << "\2-\1Y" << GetBaseMaxDamage() << "\2";
  Entry << ", " << GetBaseToHitValueDescription();
  if (!IsBroken() && !IsWhip()) Entry << ", " << GetStrengthValueDescription();
  if (Viewer) {
    int CWeaponSkillLevel = Viewer->GetCWeaponSkillLevel(this);
    int SWeaponSkillLevel = Viewer->GetSWeaponSkillLevel(this);
    if (CWeaponSkillLevel || SWeaponSkillLevel) {
      Entry << ", skill\x18\1C" << CWeaponSkillLevel << '/' << SWeaponSkillLevel << "\2";
    }
  }
  if (TimesUsed == 1) {
    Entry << ", zapped\x18\1C1\2\x18time]";
  } else if (TimesUsed) {
    Entry << ", zapped\x18\1C" << TimesUsed << "\2\x18times]";
  } else {
    Entry << "\2]";
  }
}


// never piled
void taiaha::AddInventoryEntry (ccharacter *Viewer, festring &Entry, int Amount, truth ShowSpecialInfo) const {
  AddName(Entry, INDEFINITE);
  if (ShowSpecialInfo) {
    AddSpecialInfo(Viewer, Entry, Amount, ShowSpecialInfo);
  }
}


void taiaha::BreakEffect (character *Terrorist, cfestring &DeathMsg) {
  v2 Pos = GetPos();
  level *Level = GetLevel();

  RemoveFromSlot();
  feuLong StackSize = Level->AddRadiusToSquareStack(Pos, 2); //hardcode, default is 2 for most wands, but zero for fireballs
  lsquare **SquareStack = Level->GetSquareStack();

  for (size_t c = 0; c < StackSize; ++c) SquareStack[c]->RemoveFlags(IN_SQUARE_STACK);

  fearray<lsquare *> Stack(SquareStack, StackSize);
  (Level->*level::GetBeamEffectVisualizer(PARTICLE_BEAM))(Stack, YELLOW); //beamstyle

  beamdata Beam (
    Terrorist,
    DeathMsg,
    YOURSELF,
    0 //was GetSpecialParameters()
  );
  for (size_t c = 0; c < Stack.Size; ++c) {
    (Stack[c]->*lsquare::GetBeamEffect(BEAM_FIRE_BALL))(Beam); // beam effect
  }

  SendToHell(); //removes the taiaha from existence
}


truth taiaha::ReceiveDamage (character *Damager, int Damage, int Type, int) {
  if (IsBroken() && (Type & (FIRE|ENERGY|PHYSICAL_DAMAGE)) &&
      Damage && (Damage > 125 || !RAND_N(250/Damage)))
  {
    festring DeathMsg = CONST_S("killed by an explosion of ");
    AddName(DeathMsg, INDEFINITE);
    if (Damager) DeathMsg << " caused @bk";
    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s %s.", GetExtendedDescription().CStr(), GetBreakMsg().CStr());
    BreakEffect(Damager, DeathMsg);
    return true;
  }
  return false;
}


void taiaha::PostConstruct () {
  Charges = GetMinCharges()+RAND_N(GetMaxCharges()-GetMinCharges()+1);
  TimesUsed = 0;
  meleeweapon::PostConstruct();
}


alpha taiaha::GetOutlineAlpha (int Frame) const {
  if (!IsBroken()) {
    Frame &= 31;
    return Frame*(31-Frame)>>1;
  }
  return 255;
}


col16 taiaha::GetOutlineColor (int Frame) const {
  if (!IsBroken()) {
    switch ((Frame&127)>>5) {
     case 0: return BLUE;
     case 1: return GREEN;
     case 2: return RED;
     case 3: return YELLOW;
    }
  }
  return TRANSPARENT_COLOR;
}


#endif
