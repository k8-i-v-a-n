#ifdef HEADER_PHASE
ITEM(whip, meleeweapon)
{
public:
  virtual truth IsWhip () const override;

protected:
  virtual int GetFormModifier () const override;
};


#else


truth whip::IsWhip () const { return true; }
int whip::GetFormModifier () const { return item::GetFormModifier() * GetMainMaterial()->GetFlexibility(); }


#endif
