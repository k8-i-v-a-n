#ifdef HEADER_PHASE
ITEM(ring, item)
{
public:
  virtual truth IsRing (ccharacter *) const override;
  virtual truth IsInCorrectSlot (int) const override;
  virtual truth IsLuxuryItem (ccharacter *) const override;

protected:
  virtual col16 GetMaterialColorB (int) const override;
};


#else


truth ring::IsRing (ccharacter *) const { return true; }
truth ring::IsLuxuryItem (ccharacter *) const { return true; }
truth ring::IsInCorrectSlot (int I) const { return I == RIGHT_RING_INDEX || I == LEFT_RING_INDEX; }
col16 ring::GetMaterialColorB (int) const { return MakeRGB16(200, 200, 200); }


#endif
