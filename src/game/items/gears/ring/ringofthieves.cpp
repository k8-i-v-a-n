#ifdef HEADER_PHASE
ITEM(ringofthieves, ring)
{
public:
  virtual truth IsRingOfThieves () const override;
};


#else


truth ringofthieves::IsRingOfThieves () const { return true; }


#endif
