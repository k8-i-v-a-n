#ifdef HEADER_PHASE
ITEM(avatarofvalpurus, item)
{
 public:
  virtual void Be () override;
  virtual truth IsTheAvatar() const override;
  virtual truth IsConsumable() const override;
};


#else


void avatarofvalpurus::Be () {}
truth avatarofvalpurus::IsTheAvatar () const { return true; }
truth avatarofvalpurus::IsConsumable () const { return false; }


#endif
