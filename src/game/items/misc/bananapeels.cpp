#ifdef HEADER_PHASE
ITEM(bananapeels, item)
{
public:
  virtual item *BetterVersion () const override;
  virtual truth HasBetterVersion () const override;
  virtual void StepOnEffect (character *) override;
  virtual truth IsBananaPeel () const override;
  virtual truth IsDangerous (ccharacter *) const override;
  virtual truth RaiseTheDead (character *) override;
};


#else


truth bananapeels::HasBetterVersion () const { return true; }
truth bananapeels::IsBananaPeel () const { return true; }
truth bananapeels::IsDangerous (ccharacter *Stepper) const { return Stepper->HasALeg(); }
item *bananapeels::BetterVersion () const { return banana::Spawn(); }


void bananapeels::StepOnEffect (character *Stepper) {
  if (Stepper->HasALeg() && !RAND_N(5)) {
    if (Stepper->IsPlayer()) {
      ADD_MESSAGE("Ouch. Your feet slip on %s and you fall down.", CHAR_NAME(INDEFINITE));
    } else if (Stepper->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s steps on %s and falls down.", Stepper->CHAR_NAME(DEFINITE), CHAR_NAME(INDEFINITE));
    }

    /* Do damage against any random bodypart except legs */
    Stepper->ReceiveDamage(0, 1 + RAND_2, PHYSICAL_DAMAGE, ALL&~LEGS);
    Stepper->CheckDeath(CONST_S("slipped on a banana peel"), 0);
    Stepper->EditAP(-500);
  }
}


truth bananapeels::RaiseTheDead (character *) {
  GetSlot()->AddFriendItem(banana::Spawn());
  RemoveFromSlot();
  SendToHell();
  return true;
}


#endif
