#ifdef HEADER_PHASE
ITEM(bone, item)
{
public:
  virtual truth DogWillCatchAndConsume (ccharacter *Doggie) const override;
  virtual truth IsABone () const override;
  virtual truth IsBodyPart () const override;
  virtual truth Necromancy (character *Necromancer) override;
};


#else


//==========================================================================
//
//  bone::IsABone
//
//==========================================================================
truth bone::IsABone () const {
  return true;
}


//==========================================================================
//
//  bone::IsBodyPart
//
//==========================================================================
truth bone::IsBodyPart () const {
  return true;
}


//==========================================================================
//
//  bone::DogWillCatchAndConsume
//
//==========================================================================
truth bone::DogWillCatchAndConsume (ccharacter *Doggie) const {
  return (GetConsumeMaterial(Doggie)->GetConfig() == BONE && !GetConsumeMaterial(Doggie)->GetSpoilLevel());
}


//==========================================================================
//
//  bone::Necromancy
//
//==========================================================================
truth bone::Necromancy (character *Necromancer) {
  int NumberOfBones = 0;
  truth HasNormalSkull = false;
  truth HasPuppySkull = false;
  lsquare *LSquareUnder = GetLSquareUnder();

  itemvector ItemVector;
  LSquareUnder->GetStack()->FillItemVector(ItemVector);

  // First count the number of bones, and find a skull
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->IsABone()) {
      NumberOfBones += 1;
    } else if (ItemVector[c]->IsASkull()) {
      if (ItemVector[c]->GetConfig() == PUPPY_SKULL) {
        HasPuppySkull = true;
      } else {
        HasNormalSkull = true;
      }
    }
  }

  // if we don't have the requisite number of bones, nor a skull then get out of here
  if (!HasNormalSkull && !HasPuppySkull) {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s vibrates for some time.", CHAR_NAME(DEFINITE));
    }
    return false;
  }

  if (HasPuppySkull) {
    if (NumberOfBones < 3) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s vibrates for some time.", CHAR_NAME(DEFINITE));
      }
      return false;
    }
    if (HasNormalSkull && NumberOfBones >= 5) {
      NumberOfBones = 5;
      HasPuppySkull = false;
    } else {
      NumberOfBones = 3;
      HasNormalSkull = false;
    }
  } else {
    IvanAssert(HasNormalSkull);
    if (NumberOfBones < 5) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s vibrates for some time.", CHAR_NAME(DEFINITE));
      }
      return false;
    }
    NumberOfBones = 5;
  }

  character *Skeleton;
  if (HasPuppySkull) {
    Skeleton = dog::Spawn(SKELETON_DOG);
  } else {
    Skeleton = skeleton::Spawn(Necromancer->GetAttribute(INTELLIGENCE) < 30 ? 0 : WARRIOR, NO_EQUIPMENT);
  }
  //humanoid *Skeleton = skeleton::Spawn(Necromancer->GetAttribute(INTELLIGENCE) < 30 ? 0 : WARRIOR, NO_EQUIPMENT);
  //character *Skeleton = skeleton::CreateSkeleton(Necromancer);

  if (Skeleton) {
    Skeleton->ChangeTeam(Necromancer ? Necromancer->GetTeam() : game::GetTeam(MONSTER_TEAM));
    if (Skeleton->PutToOrNear(GetPos(), true)) {
      // then remove the bones, and the skull from the floor
      for (uInt c = 0; c < ItemVector.size(); ++c) {
        if (ItemVector[c]->IsABone() && NumberOfBones > 0) {
          ItemVector[c]->RemoveFromSlot();
          ItemVector[c]->SendToHell();
          --NumberOfBones;
        } else if (ItemVector[c]->IsASkull()) {
          if (ItemVector[c]->GetConfig() == PUPPY_SKULL) {
            if (HasPuppySkull) {
              HasPuppySkull = false;
              ItemVector[c]->RemoveFromSlot();
              ItemVector[c]->SendToHell();
            }
          } else {
            if (HasNormalSkull) {
              HasNormalSkull = false;
              ItemVector[c]->RemoveFromSlot();
              ItemVector[c]->SendToHell();
            }
          }
        }
      }

      if (Skeleton->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s rises from the ground.", Skeleton->CHAR_DESCRIPTION(INDEFINITE));
      }

      if (Necromancer && Necromancer->IsPlayer()) {
        game::DoEvilDeed(50);
      }

      Skeleton->GetLSquareUnder()->DrawParticles(RED);

      Skeleton->SignalStepFrom(0);
      return true;
    } else {
      //Skeleton->Disable();
      //Skeleton->SendToHell();
      delete Skeleton;
    }
  }

  if (CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s vibrates for some time.", CHAR_NAME(DEFINITE));
  }
  return false;
}


#endif
