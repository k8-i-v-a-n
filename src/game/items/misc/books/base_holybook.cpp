#ifdef HEADER_PHASE
ITEM(holybook, item)
{
public:
  virtual truth CanBeRead (character *) const override;
  virtual truth IsReadable (ccharacter *) const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual void FinishReading (character *) override;

protected:
  virtual col16 GetMaterialColorA (int) const override;
  virtual truth ShowMaterial() const override;
};


#else


//==========================================================================
//
//  holybook::IsReadable
//
//==========================================================================
truth holybook::IsReadable (ccharacter *) const {
  return true;
}


//==========================================================================
//
//  holybook::ShowMaterial
//
//==========================================================================
truth holybook::ShowMaterial () const {
  return false;
}


//==========================================================================
//
//  holybook::CanBeRead
//
//==========================================================================
truth holybook::CanBeRead (character *Reader) const {
  return (Reader->CanRead() || game::GetSeeWholeMapCheatMode());
}


//==========================================================================
//
//  holybook::GetMaterialColorA
//
//==========================================================================
col16 holybook::GetMaterialColorA (int) const {
  return GetMasterGod()->GetColor();
}


//==========================================================================
//
//  holybook::FinishReading
//
//==========================================================================
void holybook::FinishReading (character *Reader) {
  if (Reader->IsPlayer()) {
    PLAYER->EditExperience(INTELLIGENCE, 75, 1 << 12);
    PLAYER->EditExperience(WISDOM, 150, 1 << 12);
    if (GetMasterGod()->IsKnown()) {
      ADD_MESSAGE("The book reveals many divine secrets of %s to you.", GetMasterGod()->GetName());
      GetMasterGod()->AdjustRelation(75);
      game::ApplyDivineAlignmentBonuses(GetMasterGod(), 15, true);
      if (!RAND_N(3)) {
        ADD_MESSAGE("But then it disappears.");
        RemoveFromSlot();
        SendToHell();
      }
    } else {
      game::LearnAbout(GetMasterGod());
      game::LearnAbout(GetMasterGod());
      ADD_MESSAGE("You feel you master the magical rites of %s.", GetMasterGod()->GetName());
    }
  }
}


//==========================================================================
//
//  holybook::ReceiveDamage
//
//==========================================================================
truth holybook::ReceiveDamage (character *, int Damage, int Type, int) {
  if ((Type & FIRE) && Damage && (GetMainMaterial()->GetInteractionFlags() & CAN_BURN) &&
      (Damage > 125 || !RAND_N(250 / Damage)))
  {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s catches fire!", GetExtendedDescription().CStr());
    }
    RemoveFromSlot();
    SendToHell();
    return true;
  }
  return false;
}


#endif
