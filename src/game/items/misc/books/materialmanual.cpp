#ifdef HEADER_PHASE
ITEM(materialmanual, holybook)
{
public:
  virtual void FinishReading (character *) override;
protected:
  virtual col16 GetMaterialColorA (int) const override;
};


#else

col16 materialmanual::GetMaterialColorA (int) const { return MakeRGB16(111, 64, 37); }


void materialmanual::FinishReading (character *Reader) {
  if (Reader->IsPlayer()) {
    /*
    felist List(CONST_S("Morgo's magnificent manual of materials"));
    std::vector<material*> Material;
    protosystem::CreateEveryMaterial(Material);
    game::SetStandardListAttributes(List);
    List.SetPageLength(30);
    List.AddDescription(CONST_S("                                        Strength       Flexibility    Density"));
    uInt c;
    festring Entry;

    for(c = 0; c < Material.size(); ++c)
    {
      Entry.Empty();
      Material[c]->AddName(Entry, false, false);
      Entry.Resize(40);
      Entry << Material[c]->GetStrengthValue();
      Entry.Resize(55);
      Entry << Material[c]->GetFlexibility();
      Entry.Resize(70);
      Entry << Material[c]->GetDensity();
      List.AddEntry(Entry, Material[c]->GetColor());
    }

    List.Draw();

    for(c = 0; c < Material.size(); ++c)
      delete Material[c];
    */

    //k8: nope
    ADD_MESSAGE("You learned a lot about banana materials... Actually, no, you learnt nothing at all. "
                "You spent most of your life growing bananas, you know everything about them.");

    // half of the usual holy book
    PLAYER->EditExperience(INTELLIGENCE, 75 / 2, 1 << 12);
    PLAYER->EditExperience(WISDOM, 150 / 2, 1 << 12);

    if (!RAND_N(3)) {
      ADD_MESSAGE("The book was in so bad state that it just fell apart.");
      RemoveFromSlot();
      SendToHell();
    }
  }
}


#endif
