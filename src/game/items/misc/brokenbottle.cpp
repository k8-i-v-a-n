#ifdef HEADER_PHASE
ITEM(brokenbottle, item)
{
public:
  virtual truth IsBroken () const override;
  virtual item *BetterVersion () const override;
  virtual truth HasBetterVersion () const override;
  virtual void StepOnEffect (character *) override;
  virtual item *Fix () override;
  virtual truth IsDangerous (ccharacter *) const override;
  virtual truth IsBottle () const override;
};


#else


truth brokenbottle::IsBroken () const { return true; }
truth brokenbottle::HasBetterVersion() const { return true; }
truth brokenbottle::IsBottle () const { return true; }
truth brokenbottle::IsDangerous (ccharacter *Stepper) const { return Stepper->HasALeg(); }
item *brokenbottle::BetterVersion () const { return potion::Spawn(); }


void brokenbottle::StepOnEffect (character *Stepper) {
  if (Stepper->HasALeg() && !RAND_N(5)) {
         if (Stepper->IsPlayer()) ADD_MESSAGE("Ouch. You step on sharp glass splinters.");
    else if (Stepper->CanBeSeenByPlayer()) ADD_MESSAGE("%s steps on sharp glass splinters.", Stepper->CHAR_NAME(DEFINITE));

    Stepper->ReceiveDamage(0, 1 + RAND_N(3), PHYSICAL_DAMAGE, LEGS);
    Stepper->CheckDeath(CONST_S("stepped on a broken bottle"), 0);
  }
}


item* brokenbottle::Fix () {
  potion *Potion = potion::Spawn(0, NO_MATERIALS);
  Potion->InitMaterials(GetMainMaterial(), 0);
  DonateFluidsTo(Potion);
  DonateIDTo(Potion);
  DonateSlotTo(Potion);
  SetMainMaterial(0, NO_PIC_UPDATE|NO_SIGNALS);
  SendToHell();
  return Potion;
}


#endif
