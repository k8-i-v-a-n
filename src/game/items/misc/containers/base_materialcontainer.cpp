#ifdef HEADER_PHASE
ITEM(materialcontainer, item)
{
 public:
  materialcontainer () {}
  materialcontainer (const materialcontainer &);
  virtual ~materialcontainer ();

  virtual material *GetSecondaryMaterial () const override;
  virtual void SetSecondaryMaterial (material *, int = 0) override;
  virtual void ChangeSecondaryMaterial (material *, int = 0) override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual int GetMaterials () const override;
  virtual void SignalSpoil (material *) override;
  virtual truth CanBePiledWith (citem *, ccharacter *) const override;
  virtual void Be () override;
  virtual int GetSpoilLevel () const override;
  virtual material *GetMaterial (int) const override;
  virtual int GetAttachedGod () const override;
  virtual material *GetConsumeMaterial (ccharacter *, materialpredicate = TrueMaterialPredicate) const override;
  virtual material *RemoveMaterial (material *) override;
  virtual void CalculateEmitation () override;
  virtual void InitMaterials (const materialscript *, const materialscript *, truth) override;
  virtual int GetSparkleFlags () const override;
  virtual truth DumpTo (character *dumper, v2 dest, int volume=-1) override;

  void InitMaterials (material *, material *, truth = true);
  material *RemoveMainMaterial ();
  virtual material *RemoveSecondaryMaterial ();

protected:
  virtual sLong GetMaterialPrice () const override;
  virtual truth CalculateHasBe () const override;
  virtual void GenerateMaterials () override;
  virtual col16 GetMaterialColorB (int) const override;
  virtual alpha GetAlphaB (int) const override;
  virtual int GetRustDataB () const override;

protected:
  material *SecondaryMaterial;
};


#else


material *materialcontainer::GetSecondaryMaterial () const { return SecondaryMaterial; }
int materialcontainer::GetMaterials () const { return 2; }


materialcontainer::materialcontainer (const materialcontainer &MC) : mybase(MC) {
  CopyMaterial(MC.SecondaryMaterial, SecondaryMaterial);
}


materialcontainer::~materialcontainer () {
  delete SecondaryMaterial;
}


void materialcontainer::SetSecondaryMaterial (material *What, int SpecialFlags) { SetMaterial(SecondaryMaterial, What, GetDefaultSecondaryVolume(), SpecialFlags); }
void materialcontainer::ChangeSecondaryMaterial(material *What, int SpecialFlags) { ChangeMaterial(SecondaryMaterial, What, GetDefaultSecondaryVolume(), SpecialFlags); }
void materialcontainer::InitMaterials (material *M1, material *M2, truth CUP) { ObjectInitMaterials(MainMaterial, M1, GetDefaultMainVolume(), SecondaryMaterial, M2, GetDefaultSecondaryVolume(), CUP); }
void materialcontainer::InitMaterials (const materialscript *M, const materialscript *C, truth CUP) { InitMaterials(M->Instantiate(), C->Instantiate(), CUP); }

material *materialcontainer::GetMaterial (int I) const { return (!I ? MainMaterial : SecondaryMaterial); }
col16 materialcontainer::GetMaterialColorB (int Frame) const { return (GetSecondaryMaterial() ? GetSecondaryMaterial()->GetColor() : GetMaterialColorA(Frame)); }
alpha materialcontainer::GetAlphaB (int Frame) const { return (GetSecondaryMaterial() && GetSecondaryMaterial()->GetAlpha() > GetAlphaA(Frame) ? GetSecondaryMaterial()->GetAlpha() : GetAlphaA(Frame)); }

int materialcontainer::GetSparkleFlags () const { return (MainMaterial->IsSparkling() ? SPARKLING_A : 0)|(SecondaryMaterial && SecondaryMaterial->IsSparkling() ? SPARKLING_B : 0); }
int materialcontainer::GetSpoilLevel () const { return Max(MainMaterial->GetSpoilLevel(), SecondaryMaterial ? SecondaryMaterial->GetSpoilLevel() : 0); }
int materialcontainer::GetAttachedGod () const { return (DataBase->AttachedGod ? DataBase->AttachedGod : SecondaryMaterial ? SecondaryMaterial->GetAttachedGod() : MainMaterial->GetAttachedGod()); }
int materialcontainer::GetRustDataB () const { return (SecondaryMaterial ? SecondaryMaterial->GetRustData() : GetRustDataA()); }


truth materialcontainer::DumpTo (character *dumper, v2 dest, int volume) {
  if (!dumper || (!IsCan() && !IsBottle()) || !GetSecondaryMaterial()) return false;
  if (!dumper->GetArea()->IsValidPos(dest)) return false;
  if (volume == 0) return false;
  /*
  if (Item->IsOnGround()) {
    room *Room = Item->GetRoom();
    if (Room) Room->HostileAction(Char);
  }
  */
  lsquare *sqr = dumper->GetNearLSquare(dest);

  if (GetSecondaryMaterial()->IsLiquid()) {
    festring sam(CONST_S("all"));
    liquid *Liquid;
    if (volume < 0 || volume >= CurrSecondaryVolume()) {
      Liquid = static_cast<liquid *>(RemoveSecondaryMaterial());
    } else {
      material *mat = GetSecondaryMaterial();
      IvanAssert(mat);
      const int mvol = mat->GetVolume();
      // just in case
      if (volume >= mvol) {
        Liquid = static_cast<liquid *>(RemoveSecondaryMaterial());
      } else {
        mat->SetVolume(mvol - volume);
        SignalVolumeAndWeightChange();
        mat = mat->SpawnMore(volume);;
        IvanAssert(mat->IsLiquid());
        Liquid = (liquid *)mat;
        sam.Empty(); sam << mat->GetVolume() << " ml of";
      }
    }
    if (Liquid->GetVolume() > 0) {
      if (dumper->IsPlayer()) {
        ADD_MESSAGE("Dumping %s %s.", sam.CStr(), Liquid->GetName(false, false).CStr());
      }
      sqr->SpillFluid(dumper, Liquid, false, true); // ForceHit, ShowMsg
    }
  } else {
    if (sqr) {
      bool ok = sqr->IsTransparent();
      if (ok) {
        olterrain *terra = sqr->GetOLTerrain();
        if (terra) {
          if ((terra->GetConfig() & WINDOW) || terra->IsBarWall() || terra->IsWall()) {
            ok = false;
          }
        }
      }
      if (!ok) {
        ADD_MESSAGE("Cannot dump there.");
        return false;
      }
    }
    item *Lump = lump::Spawn(0, NO_MATERIALS);
    Lump->InitMaterials(RemoveSecondaryMaterial());
    sqr->AddItem(Lump);
  }
  dumper->DexterityAction(10);
  return true;
}


void materialcontainer::Save (outputfile &SaveFile) const {
  item::Save(SaveFile);
  SaveFile << SecondaryMaterial;
}


void materialcontainer::Load (inputfile &SaveFile) {
  item::Load(SaveFile);
  LoadMaterial(SaveFile, SecondaryMaterial);
}


void materialcontainer::GenerateMaterials () {
  int Chosen = RandomizeMaterialConfiguration();
  const fearray<sLong>& MMC = GetMainMaterialConfig();
  InitMaterial(MainMaterial, MAKE_MATERIAL(MMC.Data[MMC.Size == 1 ? 0 : Chosen]), GetDefaultMainVolume());
  const fearray<sLong>& SMC = GetSecondaryMaterialConfig();
  InitMaterial(SecondaryMaterial, MAKE_MATERIAL(SMC.Data[SMC.Size == 1 ? 0 : Chosen]), GetDefaultSecondaryVolume());
}


void materialcontainer::SignalSpoil (material *Material) {
  if (!Exists()) return;
  if (Material == MainMaterial) {
    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s becomes so spoiled that it cannot hold its contents anymore.", CHAR_NAME(DEFINITE));
    RemoveMainMaterial();
  } else {
    if (CanBeSeenByPlayer()) ADD_MESSAGE("The contents of %s spoil completely.", CHAR_NAME(DEFINITE));
    delete RemoveSecondaryMaterial();
  }
}


truth materialcontainer::CanBePiledWith (citem *Item, ccharacter *Viewer) const {
  if (!item::CanBePiledWith(Item, Viewer)) return false;

  const materialcontainer *Weapon = static_cast<const materialcontainer *>(Item);

  if (!SecondaryMaterial && !Weapon->SecondaryMaterial) return true;

  return
    SecondaryMaterial &&
    Weapon->SecondaryMaterial &&
    SecondaryMaterial->IsSameAs(Weapon->SecondaryMaterial) &&
    SecondaryMaterial->GetSpoilLevel() == Weapon->SecondaryMaterial->GetSpoilLevel();
}


void materialcontainer::Be () {
  item::Be();
  if (Exists() && SecondaryMaterial) SecondaryMaterial->Be(ItemFlags);
}


material *materialcontainer::GetConsumeMaterial (ccharacter *Consumer, materialpredicate Predicate) const {
  if (SecondaryMaterial && (SecondaryMaterial->*Predicate)() && Consumer->CanConsume(SecondaryMaterial)) {
    return SecondaryMaterial;
  }
  return item::GetConsumeMaterial(Consumer, Predicate);
}


material *materialcontainer::RemoveMaterial (material *Material) {
  return (Material == MainMaterial ? RemoveMainMaterial() : RemoveSecondaryMaterial());
}


material *materialcontainer::RemoveMainMaterial () {
  truth Equipped = PLAYER->Equips(this);

  if (!SecondaryMaterial) {
    RemoveFromSlot();
  } else if (SecondaryMaterial->IsLiquid()) {
    if (!game::IsInWilderness()) {
      lsquare *Square = GetLSquareUnder();
      RemoveFromSlot();
      Square->SpillFluid(0, static_cast<liquid*>(SecondaryMaterial));
      SetSecondaryMaterial(0, NO_PIC_UPDATE|NO_SIGNALS);
    } else {
      RemoveFromSlot();
    }
  } else {
    item *Lump = lump::Spawn(0, NO_MATERIALS);
    Lump->InitMaterials(SecondaryMaterial);
    DonateFluidsTo(Lump);
    DonateIDTo(Lump);
    DonateSlotTo(Lump);
    SetSecondaryMaterial(0, NO_PIC_UPDATE|NO_SIGNALS);
  }

  SendToHell();

  if (Equipped) {
    festring msg = ProcessMessage(CONST_S("!F:@Hsp @nu is destroyed!"), PLAYER);
    game::AskForEscPress(msg);
    //game::AskForEscPress(CONST_S("Equipment destroyed!"));
  }

  return 0;
}


material *materialcontainer::RemoveSecondaryMaterial () {
  material *Material = SecondaryMaterial;
  SetSecondaryMaterial(0);
  SendNewDrawAndMemorizedUpdateRequest();
  return Material;
}


void materialcontainer::CalculateEmitation () {
  Emitation = GetBaseEmitation();
  if (MainMaterial) game::CombineLights(Emitation, MainMaterial->GetEmitation());
  if (SecondaryMaterial) game::CombineLights(Emitation, SecondaryMaterial->GetEmitation());
}


truth materialcontainer::CalculateHasBe () const {
  return
    LifeExpectancy ||
    (MainMaterial && MainMaterial->HasBe()) ||
    (SecondaryMaterial && SecondaryMaterial->HasBe());
}


sLong materialcontainer::GetMaterialPrice () const {
  return MainMaterial->GetRawPrice()+(SecondaryMaterial ? SecondaryMaterial->GetRawPrice() : 0);
}


#endif
