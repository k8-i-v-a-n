#ifdef HEADER_PHASE
ITEM(can, materialcontainer)
{
public:
  virtual item *BetterVersion () const override;
  virtual void DipInto (liquid *, character *) override;
  virtual truth CanItem (character *Char, item *what) override;
  virtual truth IsDippable (ccharacter *) const override;
  virtual truth IsMeatCannable (ccharacter *) const override;
  virtual truth IsDipDestination (ccharacter *) const override;
  virtual truth IsDumpable (ccharacter *) const override;
  virtual liquid *CreateDipLiquid () override;
  virtual truth AllowSpoil () const override;
  virtual truth Spoils () const override;
  virtual truth HasBetterVersion () const override;
  virtual truth IsCan () const override;

protected:
  virtual void AddPostFix (festring &String, int) const override;
  virtual truth AddAdjective (festring &, truth) const override;
  virtual v2 GetBitmapPos (int) const override;
};


#else


truth can::IsDippable (ccharacter *) const { return !SecondaryMaterial; }
truth can::IsMeatCannable (ccharacter *) const { return !SecondaryMaterial; }
truth can::IsDumpable (ccharacter *) const { return !!SecondaryMaterial; }
truth can::AllowSpoil () const { return false; } // temporary
truth can::Spoils () const { return false; } // temporary
truth can::HasBetterVersion () const { return !SecondaryMaterial; }
truth can::IsCan () const { return true; }
void can::AddPostFix (festring &String, int) const { AddContainerPostFix2(String, CurrSecondaryVolume(), MaxSecondaryVolume()); }
truth can::AddAdjective (festring &String, truth Articled) const { return AddEmptyAdjective(String, Articled); }
v2 can::GetBitmapPos (int) const { return v2(16, SecondaryMaterial ? 288 : 304); }
truth can::IsDipDestination (ccharacter *) const { return (SecondaryMaterial && SecondaryMaterial->IsLiquid()); }
liquid *can::CreateDipLiquid () { return static_cast<liquid *>(GetSecondaryMaterial()->TakeDipVolumeAway()); }


item *can::BetterVersion () const {
  if (!GetSecondaryMaterial()) return can::Spawn();
  return 0;
}


void can::DipInto (liquid *Liquid, character *Dipper) {
  //TODO: add alchemy
  if (Dipper->IsPlayer()) {
    ADD_MESSAGE("%s is now filled with %s.",
                CHAR_NAME(DEFINITE), Liquid->GetName(false, false).CStr());
  }
  ChangeSecondaryMaterial(Liquid);
  Dipper->DexterityAction(10);
}


truth can::CanItem (character *Char, item *what) {
  IvanAssert(what);
  if (!Char || !IsCan() || GetSecondaryMaterial() || !what->CanBeCanned()) return false;

  material *mat = what->GetCanMaterial();
  if (!mat) return false;

  int vol = mat->GetVolume();
  if (vol < 100) {
    if (Char && Char->IsPlayer()) {
      ADD_MESSAGE("not enough %s to fill the can.", mat->GetName(false).CStr());
    }
    return false;
  }

  if (mat->GetSpoilLevel() > 0) {
    if (Char->IsPlayer()) {
      ADD_MESSAGE("This thing is starting to get spoiled. There is no reason to can it.");
    }
    return false;
  }

  // do not move the message down, we need to print "empty can" here!
  if (Char && Char->IsPlayer()) {
    ADD_MESSAGE("%s is now filled with %d grams of %s.", CHAR_NAME(DEFINITE),
                (vol > 600 ? 600 : vol), mat->GetName(true).CStr());
  }

  //FIXME: k8: there is prolly a better way to do this, but i don't know it yet.
  bool removeItem;
  material *mat2;
  if (vol > 600) {
    mat2 = mat->SpawnMore(600);
    mat->EditVolume(-600);
    removeItem = false;
    if (mat->GetVolume() < 150) removeItem = true;
  } else {
    mat2 = mat->SpawnMore(vol);
    removeItem = true;
  }

  ChangeSecondaryMaterial(mat2);
  Char->DexterityAction(40);

  // destroy canned item
  if (removeItem) {
    if (what->IsOnGround()) {
      room *Room = what->GetRoom();
      if (Room) {
        Room->HostileAction(Char);
      }
    }

    if ((what->GetMainMaterial() == mat && !what->GetSecondaryMaterial()) ||
        (what->GetSecondaryMaterial() == mat && !what->GetMainMaterial()))
    {
      what->RemoveFromSlot();
      what->SendToHell();
    } else {
      delete what->RemoveMaterial(mat);
    }
  }

  return true;
}


#endif
