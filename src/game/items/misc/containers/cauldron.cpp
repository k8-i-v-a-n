#ifdef HEADER_PHASE
ITEM(cauldron, materialcontainer)
{
public:
  virtual item *BetterVersion () const override;
  virtual void DipInto (liquid *, character *) override;
  virtual liquid *CreateDipLiquid () override;
  virtual truth IsDippable (ccharacter *) const override;
  virtual truth IsDumpable (ccharacter *) const override;
  virtual truth IsDipDestination (ccharacter *) const override;
  virtual truth IsExplosive () const override;
  virtual truth HasBetterVersion () const override;
  virtual truth EffectIsGood () const override;
  virtual truth IsKamikazeWeapon (ccharacter *) const override;

protected:
  virtual void AddPostFix (festring &String, int) const override;
  virtual truth AddAdjective (festring &, truth) const override;
};


#else


truth cauldron::IsDippable (ccharacter *) const { return !SecondaryMaterial; }
truth cauldron::IsDumpable (ccharacter *) const { return !!SecondaryMaterial; }
truth cauldron::HasBetterVersion () const { return !SecondaryMaterial; }
truth cauldron::IsKamikazeWeapon (ccharacter *) const { return IsExplosive(); }
void cauldron::AddPostFix (festring &String, int) const { AddContainerPostFix(String, true); }
truth cauldron::IsExplosive () const { return GetSecondaryMaterial() && GetSecondaryMaterial()->IsExplosive(); }
truth cauldron::AddAdjective (festring & String, truth Articled) const { return AddEmptyAdjective(String, Articled); }
truth cauldron::EffectIsGood () const { return GetSecondaryMaterial() && GetSecondaryMaterial()->GetInteractionFlags() & EFFECT_IS_GOOD; }
truth cauldron::IsDipDestination (ccharacter *) const { return SecondaryMaterial && SecondaryMaterial->IsLiquid(); }
liquid *cauldron::CreateDipLiquid () { return static_cast<liquid *>(GetSecondaryMaterial()->TakeDipVolumeAway()); }


void cauldron::DipInto (liquid *Liquid, character *Dipper) {
  //TODO: add alchemy
  if (Dipper->IsPlayer()) {
    ADD_MESSAGE("%s is now filled with %s.",
                CHAR_NAME(DEFINITE), Liquid->GetName(false, false).CStr());
  }
  ChangeSecondaryMaterial(Liquid);
  Dipper->DexterityAction(10);
}


item *cauldron::BetterVersion () const {
  if (!GetSecondaryMaterial()) return cauldron::Spawn();
  return 0;
}


#endif
