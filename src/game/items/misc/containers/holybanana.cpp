#ifdef HEADER_PHASE
ITEM(holybanana, banana)
{
public:
  virtual truth HitEffect (character *, character *, v2, int, int, truth) override;
  virtual truth Zap (character *, v2, int) override;
  virtual void Be () override;
  virtual int GetSpecialFlags () const override;
  virtual void AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight=true) const override;
  virtual void AddInventoryEntry (ccharacter *, festring &, int, truth) const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth IsFlaming (ccharacter *) const override;
};


#else


int holybanana::GetSpecialFlags () const { return ST_FLAME_1; }
truth holybanana::IsFlaming (ccharacter *) const { return true; }


void holybanana::Be () {
}


truth holybanana::HitEffect (character *Enemy, character *Hitter, v2 HitPos, int BodyPartIndex,
                             int Direction, truth BlockedByArmour)
{
  truth BaseSuccess = banana::HitEffect(Enemy, Hitter, HitPos, BodyPartIndex, Direction, BlockedByArmour);
  if (Enemy->IsEnabled() && RAND_2) {
    if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s banana burns %s.", Hitter->CHAR_POSSESSIVE_PRONOUN, Enemy->CHAR_DESCRIPTION(DEFINITE));
    }
    return Enemy->ReceiveBodyPartDamage(Hitter, 2 + RAND_4, FIRE, BodyPartIndex, Direction) || BaseSuccess;
  }
  return BaseSuccess;
}


truth holybanana::Zap (character *Zapper, v2, int Direction) {
  if (Charges > TimesUsed) {
    ADD_MESSAGE("BANG! You zap %s!", CHAR_NAME(DEFINITE));
    Zapper->EditExperience(PERCEPTION, 150, 1 << 10);
    beamdata Beam (
      Zapper,
      CONST_S("killed by ") + GetName(INDEFINITE),
      Zapper->GetPos(),
      YELLOW,
      BEAM_FIRE_BALL,
      Direction,
      50,
      0,
      this
    );
    (GetLevel()->*level::GetBeam(PARTICLE_BEAM))(Beam);
    ++TimesUsed;
  } else {
    ADD_MESSAGE("Click!");
  }
  return true;
}


void holybanana::AddSpecialInfo (ccharacter *Viewer, festring &Entry, int Amount, truth includeWeight) const {
  //Entry << " [\1C" << GetWeight() << "g\2, DAM\x18\1Y" << GetBaseMinDamage() << "\2-\1Y" << GetBaseMaxDamage() << "\2";
  Entry << " [";
  if (includeWeight) {
    Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
    Entry << ", ";
  }
  Entry << "DAM\x18\1Y" << GetBaseMinDamage() << "\2-\1Y" << GetBaseMaxDamage() << "\2";
  Entry << ", " << GetBaseToHitValueDescription();
  if (!IsBroken()) Entry << ", " << GetStrengthValueDescription();
  if (Viewer) {
    int CWeaponSkillLevel = Viewer->GetCWeaponSkillLevel(this);
    int SWeaponSkillLevel = Viewer->GetSWeaponSkillLevel(this);
    if (CWeaponSkillLevel || SWeaponSkillLevel) {
      Entry << ", skill\x18\1C" << CWeaponSkillLevel << '/' << SWeaponSkillLevel << "\2";
    }
  }
  if (TimesUsed == 1) {
    Entry << ", used\x18\1C1\2\x18time\2]";
  } else if (TimesUsed) {
    Entry << ", used\x18\1C" << TimesUsed << "\2\x18times\2]";
  } else {
    Entry << "\2]";
  }
}


// never piled
void holybanana::AddInventoryEntry (ccharacter *Viewer, festring &Entry, int Amount, truth ShowSpecialInfo) const {
  AddName(Entry, INDEFINITE);
  if (ShowSpecialInfo) {
    AddSpecialInfo(Viewer, Entry, Amount, true);
  }
}


truth holybanana::ReceiveDamage (character *Damager, int Damage, int Type, int) {
  if (TimesUsed != 6 && (Type & (PHYSICAL_DAMAGE|FIRE|ENERGY)) &&
      Damage && (Damage > 50 || !RAND_N(100 / Damage)))
  {
    festring DeathMsg = CONST_S("killed by an explosion of ");
    AddName(DeathMsg, INDEFINITE);
    if (Damager) DeathMsg << " caused @bk";
    if (GetSquareUnder()->CanBeSeenByPlayer(true)) ADD_MESSAGE("%s explodes!", GetExtendedDescription().CStr());
    lsquare *Square = GetLSquareUnder();
    RemoveFromSlot();
    SendToHell();
    Square->GetLevel()->Explosion(Damager, DeathMsg, Square->GetPos(), (6 - TimesUsed) * 50);
    return true;
  }
  return false;
}


#endif
