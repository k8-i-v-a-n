#ifdef HEADER_PHASE
ITEM(kleinbottle, materialcontainer)
{
public:
  virtual void DipInto (liquid *, character *) override;
  virtual liquid *CreateDipLiquid () override;
  virtual truth IsDippable (ccharacter *) const override;
  virtual truth IsDumpable (ccharacter *) const override;
  virtual material *GetConsumeMaterial (ccharacter *, materialpredicate = TrueMaterialPredicate) const override;
  virtual truth IsKleinBottle () const override;

protected:
  virtual void AddPostFix (festring &String, int) const override;
  virtual truth AddAdjective (festring &, truth) const override;
};


#else


truth kleinbottle::IsKleinBottle () const { return true; }
truth kleinbottle::IsDippable (ccharacter *) const { return !SecondaryMaterial; }
truth kleinbottle::IsDumpable (ccharacter *) const { return false; } // can't dump!
void kleinbottle::AddPostFix (festring &String, int) const { AddContainerPostFix(String, true); }
truth kleinbottle::AddAdjective (festring &String, truth Articled) const { return AddEmptyAdjective(String, Articled); }
liquid *kleinbottle::CreateDipLiquid () { return static_cast<liquid *>(GetSecondaryMaterial()->TakeDipVolumeAway()); }


void kleinbottle::DipInto (liquid *Liquid, character *Dipper) {
  //TODO: add alchemy
  if (Dipper->IsPlayer()) {
    ADD_MESSAGE("%s is now filled with %s.",
                CHAR_NAME(DEFINITE), Liquid->GetName(false, false).CStr());
  }
  ChangeSecondaryMaterial(Liquid);
  Dipper->DexterityAction(100);
}


material *kleinbottle::GetConsumeMaterial (ccharacter *Consumer, materialpredicate Predicate) const {
  if (SecondaryMaterial && (SecondaryMaterial->*Predicate)() &&
      Consumer->CanConsume(SecondaryMaterial) && Consumer->StateIsActivated(TELEPORT))
  {
    return SecondaryMaterial;
  }
  return item::GetConsumeMaterial(Consumer, Predicate);
}


#endif
