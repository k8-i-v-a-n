#ifdef HEADER_PHASE
ITEM(nuke, materialcontainer)
{
public:
  virtual truth Apply (character *) override;
  virtual truth IsAppliable(ccharacter*) const override;
  virtual truth IsNuke () const override;
  virtual truth IsExplosive () const override;
  virtual sLong GetTotalExplosivePower () const override;
};


#else


truth nuke::IsExplosive () const { return (GetSecondaryMaterial() && GetSecondaryMaterial()->IsExplosive()); }
truth nuke::IsAppliable (ccharacter *) const { return true; }
truth nuke::IsNuke () const { return true; }


sLong nuke::GetTotalExplosivePower () const {
  return (GetSecondaryMaterial() ? GetSecondaryMaterial()->GetTotalExplosivePower() : 0);
}


truth nuke::Apply (character *Terrorist) {
  if (IsExplosive()) {
    if (Terrorist->IsPlayer()) {
      ADD_MESSAGE("You attempt to arm %s, but you don't have the required eighteen-digit activation code.",
                  CHAR_NAME(DEFINITE));
    } else if (Terrorist->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s fiddles with %s, but nothing seems to happen.",
                  Terrorist->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
    }
    return true;
  } else if (Terrorist->IsPlayer()) {
    ADD_MESSAGE("%s unfortunately seems to be inoperative.", CHAR_NAME(DEFINITE));
  }
  return false;
}


#endif
