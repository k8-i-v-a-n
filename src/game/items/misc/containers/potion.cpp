#ifdef HEADER_PHASE
ITEM(potion, materialcontainer)
{
public:
  virtual item *BetterVersion () const override;
  virtual void DipInto (liquid *Liquid, character *Dipper) override;
  virtual liquid *CreateDipLiquid () override;
  virtual truth IsDippable (ccharacter *) const override;
  virtual void Break (character *Breaker, int Dir) override;
  virtual truth IsDipDestination (ccharacter *) const override;
  virtual truth IsDumpable (ccharacter *) const override;
  virtual truth IsExplosive () const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth HasBetterVersion () const override;
  virtual truth EffectIsGood () const override;
  virtual truth IsKamikazeWeapon (ccharacter *) const override;
  virtual truth IsBottle () const override;

protected:
  virtual void AddPostFix (festring &String, int) const override;
  virtual truth AddAdjective (festring &, truth) const override;
};


#else


truth potion::IsDippable (ccharacter *) const { return !SecondaryMaterial; }
truth potion::IsDumpable (ccharacter *) const { return !!SecondaryMaterial; }
truth potion::HasBetterVersion () const { return !SecondaryMaterial; }
truth potion::IsKamikazeWeapon (ccharacter *) const { return IsExplosive(); }
truth potion::IsBottle () const { return true; }
void potion::AddPostFix (festring &String, int) const { AddContainerPostFix2(String, CurrSecondaryVolume(), MaxSecondaryVolume()); }
truth potion::IsExplosive () const { return (GetSecondaryMaterial() && GetSecondaryMaterial()->IsExplosive()); }
truth potion::AddAdjective (festring &String, truth Articled) const { return AddEmptyAdjective(String, Articled); }
truth potion::EffectIsGood () const { return (GetSecondaryMaterial() && (GetSecondaryMaterial()->GetInteractionFlags()&EFFECT_IS_GOOD)); }
truth potion::IsDipDestination (ccharacter *) const { return (SecondaryMaterial && SecondaryMaterial->IsLiquid()); }
liquid *potion::CreateDipLiquid () { return static_cast<liquid*>(GetSecondaryMaterial()->TakeDipVolumeAway()); }


void potion::DipInto (liquid *Liquid, character *Dipper) {
  //TODO: add alchemy
  if (Dipper->IsPlayer()) {
    ADD_MESSAGE("%s is now filled with %s.",
                CHAR_NAME(DEFINITE), Liquid->GetName(false, false).CStr());
  }
  ChangeSecondaryMaterial(Liquid);
  Dipper->DexterityAction(10);
}


item *potion::BetterVersion () const {
  return (!GetSecondaryMaterial() ? potion::Spawn() : 0);
}


void potion::Break (character *Breaker, int Dir) {
       if (CanBeSeenByPlayer()) ADD_MESSAGE("%s shatters to pieces.", GetExtendedDescription().CStr());
  else if (PLAYER->CanHear()) ADD_MESSAGE("You hear something shattering.");

  if (Breaker && IsOnGround()) {
    room *Room = GetRoom();
    if (Room) Room->HostileAction(Breaker);
  }

  item *Remains = brokenbottle::Spawn(0, NO_MATERIALS);
  Remains->InitMaterials(GetMainMaterial()->SpawnMore());
  DonateFluidsTo(Remains);
  DonateIDTo(Remains);
  DonateSlotTo(Remains);
  SendToHell();

  if (GetSecondaryMaterial() && GetSecondaryMaterial()->IsLiquid()) {
    liquid *Liquid = static_cast<liquid*>(GetSecondaryMaterial());

    if (Dir != YOURSELF) {
      v2 Pos = Remains->GetPos()+game::GetMoveVector(Dir);
      if (Remains->GetLevel()->IsValidPos(Pos)) {
        sLong HalfVolume = GetSecondaryMaterial()->GetVolume() >> 1;
        Liquid->EditVolume(-HalfVolume);
        liquid *lq = Liquid->SpawnMoreLiquid(Liquid->GetVolume());
        if (lq->GetVolume() > 0) {
          Remains->GetNearLSquare(Pos)->SpillFluid(Breaker, lq);
        } else {
          delete lq;
        }
      }
    }
    if (Remains->Exists() && Liquid->GetVolume() > 0) {
      liquid *lq = Liquid->SpawnMoreLiquid(Liquid->GetVolume());
      if (lq->GetVolume() > 0) {
        Remains->GetLSquareUnder()->SpillFluid(Breaker, lq);
      } else {
        delete lq;
      }
    }
  }

  if (PLAYER->Equips(Remains)) {
    festring msg = ProcessMessage(CONST_S("!F:@Hsp @nu is broken!"), PLAYER);
    game::AskForEscPress(msg);
    //game::AskForEscPress(CONST_S("Equipment broken!"));
  }
}


truth potion::ReceiveDamage (character *Damager, int Damage, int Type, int Dir) {
  if ((Type & FIRE) && Damage && IsExplosive() && (Damage > 50 || !RAND_N(100 / Damage))) {
    festring DeathMsg = CONST_S("killed by an explosion of ");
    AddName(DeathMsg, INDEFINITE);

    if (Damager) DeathMsg << " caused @bk";

    if (GetSquareUnder()->CanBeSeenByPlayer(true)) ADD_MESSAGE("%s explodes!", GetExtendedDescription().CStr());

    lsquare *Square = GetLSquareUnder();
    RemoveFromSlot();
    SendToHell();
    Square->GetLevel()->Explosion(Damager, DeathMsg, Square->GetPos(), GetSecondaryMaterial()->GetTotalExplosivePower());
    return true;
  }

  if (Type&THROW) {
    int StrengthValue = GetStrengthValue();
    if (!StrengthValue) StrengthValue = 1;

    if (Damage > StrengthValue << 2 && RAND_N(50 * Damage / StrengthValue) >= 100) {
      Break(Damager, Dir);
      return true;
    }
  }

  return item::ReceiveDamage(Damager, Damage, Type, Dir);
}


#endif
