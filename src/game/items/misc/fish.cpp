#ifdef HEADER_PHASE
ITEM(fish, item)
{
public:
  virtual material *RemoveMaterial (material *Material) override;
  virtual truth Necromancy (character *) override;
  virtual truth RaiseTheDead (character *) override;
  virtual truth CatWillCatchAndConsume (ccharacter *) const override;
  virtual truth IsFish () const override;
};


#else


truth fish::IsFish () const {
  return true;
}


material *fish::RemoveMaterial (material *Material) {
  if (GetConfig() == DEAD_FISH || GetConfig() == SMOKED_FISH) {
    item *Bones = fish::Spawn(BONE_FISH);
    DonateSlotTo(Bones);
    DonateIDTo(Bones);
    SendToHell();
    return 0;
  } else {
    return item::RemoveMaterial(Material);
  }
}


truth fish::Necromancy (character*) {
  if (GetConfig() == BONE_FISH) {
    GetSlot()->AddFriendItem(fish::Spawn(DEAD_FISH));
    RemoveFromSlot();
    SendToHell();
    return true;
  }
  return false;
}


truth fish::RaiseTheDead (character *) {
  // TODO: Spawn a fish creature if done on WATER liquidterrain.
  ADD_MESSAGE("%s suddenly comes back to life, but quickly suffocates again.",
              CHAR_NAME(DEFINITE));
  if (GetConfig() == BONE_FISH) {
    GetSlot()->AddFriendItem(fish::Spawn(DEAD_FISH));
    RemoveFromSlot();
    SendToHell();
    return true;
  }
  return false;
}


truth fish::CatWillCatchAndConsume (ccharacter *Kitty) const {
  if (GetConfig() != DEAD_FISH && GetConfig() != SMOKED_FISH) return false;
  material *mat = GetConsumeMaterial(Kitty);
  if (!mat) return false;
  if (mat->GetConfig() != SARDINE &&
      mat->GetConfig() != SMOKED_SARDINE &&
      mat->GetConfig() != TUNA)
  {
    return false;
  }
  if (GetConsumeMaterial(Kitty)->GetSpoilLevel()) return false;
  return true;
}

#endif
