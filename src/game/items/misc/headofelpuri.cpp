#ifdef HEADER_PHASE
ITEM(headofelpuri, item) // can't wear equipment, so not "head"
{
public:
  virtual truth IsHeadOfElpuri () const override;
  virtual truth IsConsumable () const override;
  virtual void Be () override;
};


#else


truth headofelpuri::IsHeadOfElpuri () const { return true; }
truth headofelpuri::IsConsumable () const { return false; }
void headofelpuri::Be () {}


#endif
