#ifdef HEADER_PHASE
ITEM(holymango, item)
{
public:
  virtual truth HitEffect (character *, character *, v2, int, int, truth) override;
  virtual int GetSpecialFlags () const override;
  virtual void AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight=true) const override;
  virtual void AddInventoryEntry (ccharacter *, festring &, int, truth) const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth IsMango () const override;
  virtual truth IsFlaming (ccharacter *) const override;
};


#else


truth holymango::IsMango () const { return true; }
int holymango::GetSpecialFlags () const { return ST_FLAME_1; }
truth holymango::IsFlaming (ccharacter *) const { return true; }


truth holymango::HitEffect (character *Enemy, character *Hitter, v2 HitPos, int BodyPartIndex,
                            int Direction, truth BlockedByArmour)
{
  truth BaseSuccess = 1;//mango::HitEffect(Enemy, Hitter, HitPos, BodyPartIndex, Direction, BlockedByArmour);
  if (Enemy->IsEnabled() && RAND_2) {
    if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s mango burns %s.", Hitter->CHAR_POSSESSIVE_PRONOUN, Enemy->CHAR_DESCRIPTION(DEFINITE));
    }
    return Enemy->ReceiveBodyPartDamage(Hitter, 2 + RAND_4, FIRE, BodyPartIndex, Direction) || BaseSuccess;
  }
  return BaseSuccess;
}


void holymango::AddSpecialInfo (ccharacter *Viewer, festring &Entry, int Amount, truth includeWeight) const {
  //Entry << " [\1C" << GetWeight() << "g;"
  Entry << " [";
  if (includeWeight) {
    Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
    Entry << ", ";
  }
  Entry << "DAM\x18\1Y" << GetBaseMinDamage() << "\2-\1Y" << GetBaseMaxDamage() << "\2";
  Entry << ", " << GetBaseToHitValueDescription();
  if (!IsBroken()) Entry << ", " << GetStrengthValueDescription();
  if (Viewer) {
    int CWeaponSkillLevel = Viewer->GetCWeaponSkillLevel(this);
    int SWeaponSkillLevel = Viewer->GetSWeaponSkillLevel(this);
    if (CWeaponSkillLevel || SWeaponSkillLevel) {
      Entry << ", skill\x18\1C" << CWeaponSkillLevel << '/' << SWeaponSkillLevel << "\2";
    }
  }
  Entry << "\2]";
}


// never piled
void holymango::AddInventoryEntry (ccharacter *Viewer, festring &Entry, int Amount, truth ShowSpecialInfo) const {
  AddName(Entry, INDEFINITE);
  if (ShowSpecialInfo) {
    AddSpecialInfo(Viewer, Entry, Amount, true);
  }
}


truth holymango::ReceiveDamage (character *Damager, int Damage, int Type, int) {
  if ((Type & (PHYSICAL_DAMAGE|FIRE|ENERGY)) && Damage && (Damage > 50 || !RAND_N(100/Damage))) {
    lsquare *Square = GetLSquareUnder();
    festring DeathMsg = CONST_S("killed by an explosion of ");
    AddName(DeathMsg, INDEFINITE);
    if (Damager) DeathMsg << " caused @bk";
    if (GetSquareUnder()->CanBeSeenByPlayer(true)) ADD_MESSAGE("%s explodes!", GetExtendedDescription().CStr());
    RemoveFromSlot();
    SendToHell();
    Square->GetLevel()->Explosion(Damager, DeathMsg, Square->GetPos(), 6*50);
    return true;
  }
  return false;
}


#endif
