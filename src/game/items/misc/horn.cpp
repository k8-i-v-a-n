#ifdef HEADER_PHASE
ITEM(horn, item)
{
 public:
  horn ();

  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual truth Apply (character *) override;
  virtual truth IsAppliable (ccharacter *) const override;
  virtual void FinalProcessForBone () override;

protected:
  feuLong LastUsed;
};


#else


truth horn::IsAppliable (ccharacter *) const { return true; }


horn::horn() : LastUsed(0) {
}


truth horn::Apply (character *Blower) {
  if (!Blower->HasHead()) {
    if (Blower->IsPlayer()) ADD_MESSAGE("You need a head to do this.");
    return false;
  }

  if (!LastUsed || game::GetTick() - LastUsed >= 2500) {
    LastUsed = game::GetTick();
    //cchar* SoundDescription = GetConfig() == BRAVERY ? "loud but calming" : "frightening, almost scream-like";
    cchar *SoundDescription;
    auto cfg = GetConfig();
         if (cfg == BRAVERY) SoundDescription = "loud but calming";
    else if (cfg == FEAR) SoundDescription = "frightening, almost scream-like";
    else if (cfg == CONFUSION) SoundDescription = "strange and dissonant";
    else if (cfg == HEALING) SoundDescription = "deep and soothing";
    else if (cfg == PLENTY) SoundDescription = "dull and muffled";
    else SoundDescription = "never-before heard";

    if (Blower->IsPlayer()) {
      if (Blower->CanHear()) {
        ADD_MESSAGE("You produce a %s sound.", SoundDescription);
      } else {
        ADD_MESSAGE("You blow %s.", CHAR_NAME(DEFINITE));
      }
    } else if (Blower->CanBeSeenByPlayer()) {
      if (PLAYER->CanHear()) {
        ADD_MESSAGE("%s blows %s and produces a %s sound.", Blower->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE), SoundDescription);
      } else {
        ADD_MESSAGE("%s blows %s.", Blower->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
      }
    } else if (PLAYER->CanHear()) {
      ADD_MESSAGE("You hear a %s sound echoing everywhere.", SoundDescription);
    }

    rect Rect;
    femath::CalculateEnvironmentRectangle(Rect, GetLevel()->GetBorder(), GetPos(), 10);

    for (int x = Rect.X1; x <= Rect.X2; ++x) {
      for (int y = Rect.Y1; y <= Rect.Y2; ++y) {
        character *Audience = GetNearSquare(x, y)->GetCharacter();
        if (Audience) {
          if (GetConfig() == BRAVERY && Audience->CanHear() && Audience->TemporaryStateIsActivated(PANIC) && Blower->IsAlly(Audience)) {
                 if (Audience->IsPlayer()) ADD_MESSAGE("You calm down.");
            else if (CanBeSeenByPlayer()) ADD_MESSAGE("%s calms down.", Audience->CHAR_NAME(DEFINITE));
            Audience->DeActivateTemporaryState(PANIC);
          } else if (GetConfig() == FEAR && !Audience->TemporaryStateIsActivated(PANIC) && Blower->GetRelation(Audience) == HOSTILE &&
                     Audience->HornOfFearWorks() && !Audience->StateIsActivated(FEARLESS))
          {
            Audience->BeginTemporaryState(PANIC, 500 + RAND_N(500));
          } else if (GetConfig() == CONFUSION && Blower->GetRelation(Audience) == HOSTILE && Audience->CanHear()) {
            Audience->BeginTemporaryState(CONFUSED, 500 + RAND_N(500));
          } else if (GetConfig() == HEALING && Audience->CanHear() && Blower->IsAlly(Audience)) {
            if (Audience->IsPlayer()) {
              ADD_MESSAGE("Your wounds are healed.");
            } else if (Audience->CanBeSeenByPlayer()) {
              ADD_MESSAGE("%s looks sound and hale again.", Audience->CHAR_NAME(DEFINITE));
            }
            Audience->RestoreLivingHP();
          }
        }
      }
    }

    // non-area horns
    if (GetConfig() == PLENTY) {
      item *Food;
      switch (RAND_N(15)) {
       case 0: Food = carrot::Spawn(); break;
       case 1: Food = sausage::Spawn(); break;
       case 2: Food = mango::Spawn(); break;
       case 3: case 4: case 5: Food = can::Spawn(); break;
       case 6: Food = lump::Spawn(); break;
       case 7: Food = loaf::Spawn(); break;
       case 8: Food = kiwi::Spawn(); break;
       case 9: Food = pineapple::Spawn(); break;
       default: Food = banana::Spawn(); break;
      }
      if (Blower->IsPlayer()) {
        ADD_MESSAGE("Suddenly, %s falls out of the horn.", Food->CHAR_NAME(INDEFINITE));
      }
      Blower->GetStack()->AddItem(Food);
    }
  } else {
    if (Blower->IsPlayer()) {
      if (Blower->CanHear()) ADD_MESSAGE("You produce a mighty sound.");
      else ADD_MESSAGE("You blow %s.", CHAR_NAME(DEFINITE));
    } else if (Blower->CanBeSeenByPlayer()) {
      if (PLAYER->CanHear()) ADD_MESSAGE("%s blows %s and produces a mighty sound.", Blower->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
      else ADD_MESSAGE("%s blows %s.", Blower->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
    } else if (PLAYER->CanHear()) {
      ADD_MESSAGE("You hear a horn being blown.");
    }
  }

  game::CallForAttention(GetPos(), 900);
  Blower->EditAP(-1000);
  return true;
}


void horn::Save(outputfile& SaveFile) const
{
  item::Save(SaveFile);
  SaveFile << LastUsed;
}


void horn::Load(inputfile& SaveFile)
{
  item::Load(SaveFile);
  SaveFile >> LastUsed;
}


void horn::FinalProcessForBone()
{
  item::FinalProcessForBone();
  LastUsed = 0;
}


#endif
