#ifdef HEADER_PHASE
ITEM(key, item)
{
public:
  virtual truth Apply (character *) override;
  virtual truth IsAppliable (ccharacter *) const override;
  virtual truth IsKey (ccharacter*) const override { return true; }
  //virtual truth CanOpenDoors () const override;
  virtual truth CanOpenLockType (int AnotherLockType) const override;
};


#else


truth key::IsAppliable (ccharacter *) const { return true; }
//truth key::CanOpenDoors () const { return true; }
truth key::CanOpenLockType (int AnotherLockType) const { return GetConfig() == AnotherLockType; }


truth key::Apply (character *User) {
  if (User->IsPlayer()) {
    if (!User->CanOpen()) {
      ADD_MESSAGE("This monster type cannot use keys.");
      return false;
    }

    int Key;
    truth OpenableItems = User->GetStack()->SortedItems(User, &item::HasLock);

    if (OpenableItems) {
      Key = game::AskForKeyPress(CONST_S("What do you wish to lock or unlock? [press a direction key, \1Gspace\2 or \1Gi\2]"));
    } else {
      Key = game::AskForKeyPress(CONST_S("What do you wish to lock or unlock? [press a direction key or \1Gspace\2]"));
    }

    if (OpenableItems && KEY_EQU(Key, "I")) {
      item *Item = User->GetStack()->DrawContents(User, CONST_S("What do you want to lock or unlock?"),
                                                  0, &item::IsOpenable);
      #if 0
      if (Item) {
        ConLogf("OO: %s", Item->GetClassID());
      }
      #endif
      return Item && Item->TryKey(this, User);
    }

    v2 DirVect = game::GetDirectionVectorForKey(Key);
    if (DirVect != ERROR_V2 && User->GetArea()->IsValidPos(User->GetPos() + DirVect)) {
      return GetLevel()->GetLSquare(User->GetPos() + DirVect)->TryKey(this, User);
    }
  }

  return true;
}


#endif
