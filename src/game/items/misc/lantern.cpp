#ifdef HEADER_PHASE
ITEM(lantern, item)
{
public:
  virtual void SignalSquarePositionChange (int) override;
  virtual truth AllowAlphaEverywhere () const override;
  virtual int GetSpecialFlags () const override;
  virtual truth IsLanternOnWall () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetMaterialColorA (int) const override;
  virtual col16 GetMaterialColorB (int) const override;
  virtual col16 GetMaterialColorC (int) const override;
  virtual col16 GetMaterialColorD (int) const override;
  virtual alpha GetAlphaA (int) const override;
  virtual alpha GetAlphaB (int) const override;
  virtual alpha GetAlphaC (int) const override;
  virtual alpha GetAlphaD (int) const override;
  virtual v2 GetBitmapPos (int) const override;
};


#else


truth lantern::AllowAlphaEverywhere () const { return true; }
truth lantern::IsLanternOnWall () const { return GetSquarePosition() != CENTER; }
int lantern::GetClassAnimationFrames () const { return !IsBroken() ? 32 : 1; }
alpha lantern::GetAlphaA (int) const { return 255; }
col16 lantern::GetMaterialColorA (int) const { return MakeRGB16(255, 255, 240); }
col16 lantern::GetMaterialColorB (int) const { return MakeRGB16(255, 255, 100); }
col16 lantern::GetMaterialColorC (int) const { return MakeRGB16(255, 255, 100); }
col16 lantern::GetMaterialColorD (int) const { return MakeRGB16(255, 255, 100); }


void lantern::SignalSquarePositionChange (int SquarePosition) {
  item::SignalSquarePositionChange(SquarePosition);
  UpdatePictures();
}


int lantern::GetSpecialFlags () const {
  auto sqpos = GetSquarePosition();
  if (sqpos == LEFT) return ROTATE|MIRROR;
  if (sqpos == DOWN) return FLIP;
  if (sqpos == UP) return 0;
  if (sqpos == RIGHT) return ROTATE;
  return 0;
}


alpha lantern::GetAlphaB (int Frame) const {
  Frame &= 31;
  return (Frame * (31 - Frame) >> 1);
}


alpha lantern::GetAlphaC (int Frame) const {
  Frame &= 31;
  return (Frame * (31 - Frame) >> 2);
}


alpha lantern::GetAlphaD (int Frame) const {
  Frame &= 31;
  return (Frame * (31 - Frame) >> 3);
}


v2 lantern::GetBitmapPos(int Frame) const {
  return (GetSquarePosition() == CENTER ? item::GetBitmapPos(Frame) : item::GetWallBitmapPos(Frame));
}


#endif
