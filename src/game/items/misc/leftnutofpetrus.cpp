#ifdef HEADER_PHASE
ITEM(leftnutofpetrus, nut)
{
public:
  virtual void Be () override;
  virtual truth IsPetrussNut () const override;
  virtual truth IsConsumable () const override;
};


#else


void leftnutofpetrus::Be () {}
truth leftnutofpetrus::IsPetrussNut () const { return true; }
truth leftnutofpetrus::IsConsumable () const { return false; }


#endif
