#ifdef HEADER_PHASE
ITEM(loaf, item)
{
protected:
  virtual void AddPostFix (festring &String, int) const override;
  virtual truth ShowMaterial () const override;
};


#else


void loaf::AddPostFix (festring &String, int) const { AddLumpyPostFix(String); }
truth loaf::ShowMaterial () const { return false; }


#endif
