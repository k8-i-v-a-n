#ifdef HEADER_PHASE
ITEM(itemcontainer, lockableitem)
{
 public:
  itemcontainer ();
  itemcontainer (const itemcontainer &);
  virtual ~itemcontainer ();

  virtual truth Open (character *) override;
  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual truth Polymorph (character *, stack *) override;
  virtual void CalculateVolumeAndWeight () override;
  virtual truth ContentsCanBeSeenBy (ccharacter *) const override;
  virtual sLong GetTruePrice () const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual void DrawContents (ccharacter *) override;
  virtual truth Apply (character *Applier) override;
  virtual truth IsAppliable (ccharacter *) const override;
  virtual void SetItemsInside (const fearray<itemcontentscript> &, int) override;
  virtual truth AllowContentEmitation () const override;
  virtual truth IsDestroyable (ccharacter *) const override;
  virtual int GetOfferValue (int) const override;
  virtual void SortAllItems (const sortdata &) const override;
  virtual void PreProcessForBone () override;
  virtual void PostProcessForBone () override;
  virtual void FinalProcessForBone () override;
  virtual material *RemoveMaterial (material *) override;
  virtual void SetLifeExpectancy (int, int) override;
  virtual void CalculateEnchantment () override;
  virtual int GetTeleportPriority () const override;
  virtual void SetParameters (int) override;
  virtual void Disappear () override;
  virtual stack *GetContained () const override;
  virtual truth IsLockableContainer () const override;
  virtual truth IsFixableBySmith (ccharacter *) const override;

protected:
  virtual col16 GetMaterialColorB (int) const override;
  virtual void PostConstruct () override;

protected:
  stack *Contained;
};


#else


truth itemcontainer::Apply (character *Applier) { return Open(Applier); }
truth itemcontainer::IsAppliable (ccharacter *) const { return true; }
truth itemcontainer::AllowContentEmitation () const { return false; }
stack *itemcontainer::GetContained () const { return Contained; }
truth itemcontainer::IsLockableContainer () const { return true; }
col16 itemcontainer::GetMaterialColorB (int) const { return MakeRGB16(80, 80, 80); }


//==========================================================================
//
//  itemcontainer::itemcontainer
//
//==========================================================================
itemcontainer::itemcontainer () {
  Contained = new stack(0, this, HIDDEN);
}


//==========================================================================
//
//  itemcontainer::itemcontainer
//
//==========================================================================
itemcontainer::itemcontainer (const itemcontainer &Container)
  : mybase(Container)
{
  Contained = new stack(0, this, HIDDEN);
  CalculateAll();
}


//==========================================================================
//
//  itemcontainer::~itemcontainer
//
//==========================================================================
itemcontainer::~itemcontainer () {
  delete Contained;
}


//==========================================================================
//
//  itemcontainer::PostConstruct
//
//==========================================================================
void itemcontainer::PostConstruct () {
  lockableitem::PostConstruct();
  if (GetConfig() != EMPTY_STRONG_BOX && GetConfig() != MAGIC_CHEST) {
    SetIsLocked(RAND_N(3));
    sLong ItemNumber = RAND_N(GetMaxGeneratedContainedItems() + 1);
    for (int c = 0; c < ItemNumber; ++c) {
      item *NewItem = protosystem::BalancedCreateItem(0);
      sLong Volume = NewItem->GetVolume();
      if (NewItem->HandleInPairs()) Volume <<= 1;
      if (NewItem->CanBeGeneratedInContainer() &&
          (GetStorageVolume() - GetContained()->GetVolume()) >= Volume)
      {
        GetContained()->AddItem(NewItem);
        NewItem->SpecialGenerationHandler();
      } else {
        delete NewItem;
      }
    }
  } else {
    SetIsLocked(0);
    SetIsLocked(false);
  }
}


//==========================================================================
//
//  itemcontainer::Open
//
//==========================================================================
truth itemcontainer::Open (character *Opener) {
  if (IsLocked()) {
    ADD_MESSAGE("%s seems to be locked.", CHAR_NAME(DEFINITE));
    return false;
  }
  truth Success = false;
  /*
  festring Question = CONST_S("Do you want to \1Gt\2ake something from or \1Gp\2ut something in this container?");
  switch (game::KeyQuestion(Question, REQUIRES_ANSWER, 'T', 'P', KEY_ESC, 0)) {
    case 'T': Success = GetContained()->TakeSomethingFrom(Opener, GetName(DEFINITE)); break;
    case 'P': Success = GetContained()->PutSomethingIn(Opener, GetName(DEFINITE), GetStorageVolume(), GetID()); break;
    default: return false;
  }
  */
  if (!GetContained()->GetItems()) {
    ADD_MESSAGE("There is nothing in %s.", GetName(DEFINITE).CStr());
  }
  bool doTake = true;
  int selTake = -1, selPut = -1;
  for (;;) {
    int res;
    /*
    if (!GetContained()->GetItems()) {
      if (!Opener->GetStack()->GetItems()) {
        ADD_MESSAGE("You have nothing to take and nothing put in %s.", GetName(DEFINITE).CStr());
        break;
      }
      doTake = false;
    } else if (!Opener->GetStack()->GetItems()) {
      doTake = true;
    }
    */
    if (doTake) {
      res = GetContained()->TakeSomethingFrom(Opener, GetName(DEFINITE), Success, selTake);
    } else {
      res = GetContained()->PutSomethingIn(Opener, GetName(DEFINITE), GetStorageVolume(),
                                           GetID(), Success, selPut);
    }
    if (res < 0) break;
    doTake = !doTake;
  }
  if (Success) {
    Opener->DexterityAction(Opener->OpenMultiplier() * 5);
  }
  return Success;
}


//==========================================================================
//
//  itemcontainer::Save
//
//==========================================================================
void itemcontainer::Save (outputfile &SaveFile) const {
  lockableitem::Save(SaveFile);
  Contained->Save(SaveFile);
}


//==========================================================================
//
//  itemcontainer::Load
//
//==========================================================================
void itemcontainer::Load (inputfile &SaveFile) {
  lockableitem::Load(SaveFile);
  Contained->Load(SaveFile);
}


//==========================================================================
//
//  itemcontainer::Polymorph
//
//==========================================================================
truth itemcontainer::Polymorph (character *Polymorpher, stack *CurrentStack) {
  GetContained()->MoveItemsTo(CurrentStack);
  item::Polymorph(Polymorpher, CurrentStack);
  return true;
}


//==========================================================================
//
//  itemcontainer::CalculateVolumeAndWeight
//
//==========================================================================
void itemcontainer::CalculateVolumeAndWeight () {
  item::CalculateVolumeAndWeight();
  Volume += Contained->GetVolume();
  Weight += Contained->GetWeight();
}


//==========================================================================
//
//  itemcontainer::ContentsCanBeSeenBy
//
//==========================================================================
truth itemcontainer::ContentsCanBeSeenBy (ccharacter *Viewer) const {
  return GetMainMaterial()->IsTransparent() && CanBeSeenBy(Viewer);
}


//==========================================================================
//
//  itemcontainer::GetTruePrice
//
//==========================================================================
sLong itemcontainer::GetTruePrice () const {
  return GetContained()->GetTruePrice() + item::GetTruePrice();
}


//==========================================================================
//
//  itemcontainer::ReceiveDamage
//
//==========================================================================
truth itemcontainer::ReceiveDamage (character *Damager, int Damage, int Type, int) {
  if (Type & (PHYSICAL_DAMAGE|SOUND|ENERGY)) {
    Contained->ReceiveDamage(Damager, Damage/GetDamageDivider(), Type);
    int SV = Max(GetStrengthValue(), 1);
    if (IsLocked() && Damage > SV && RAND_N(100*Damage/SV) >= 100) {
      SetIsLocked(false);
      SetConfig((GetConfig()&~LOCK_BITS)|BROKEN_LOCK);
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("The %s's lock shatters to pieces.", GetNameSingular().CStr());
      }
      return true;
    }
    if (Damager && Damager->IsPlayer()) ADD_MESSAGE("THUMP!");
  }
  return false;
}


//==========================================================================
//
//  itemcontainer::IsFixableBySmith
//
//  smith can break locks
//
//==========================================================================
truth itemcontainer::IsFixableBySmith (ccharacter *) const {
  return IsLocked();
}


//==========================================================================
//
//  itemcontainer::DrawContents
//
//==========================================================================
void itemcontainer::DrawContents (ccharacter *Char) {
  festring Topic = CONST_S("Contents of your ") + GetName(UNARTICLED);
  GetContained()->DrawContents(Char, Topic, NO_SELECT);
  for (stackiterator i = GetContained()->GetBottom(); i.HasItem(); ++i) i->DrawContents(Char);
}


//==========================================================================
//
//  itemcontainer::SetItemsInside
//
//==========================================================================
void itemcontainer::SetItemsInside (const fearray<itemcontentscript> &ItemArray, int SpecialFlags) {
  GetContained()->Clean();
  for (uInt c1 = 0; c1 < ItemArray.Size; ++c1) {
    if (ItemArray[c1].IsValid()) {
      const interval *TimesPtr = ItemArray[c1].GetTimes();
      int Times = (TimesPtr ? TimesPtr->Randomize() : 1);
      for (int c2 = 0; c2 < Times; ++c2) {
        item *Item = ItemArray[c1].Instantiate(SpecialFlags);
        if (Item) {
          Contained->AddItem(Item);
          Item->SpecialGenerationHandler();
        }
      }
    }
  }
}


//==========================================================================
//
//  itemcontainer::GetOfferValue
//
//==========================================================================
int itemcontainer::GetOfferValue (int Receiver) const {
  int Sum = 0;
  for (int c = 0; c < GetContained()->GetItems(); ++c) Sum += GetContained()->GetItem(c)->GetOfferValue(Receiver);
  return item::GetOfferValue(Receiver) + Sum;
}


//==========================================================================
//
//  itemcontainer::IsDestroyable
//
//==========================================================================
truth itemcontainer::IsDestroyable (ccharacter *Char) const {
  for (int c = 0; c < GetContained()->GetItems(); ++c) {
    if (!GetContained()->GetItem(c)->IsDestroyable(Char)) return false;
  }
  return true;
}


//==========================================================================
//
//  itemcontainer::SortAllItems
//
//==========================================================================
void itemcontainer::SortAllItems (const sortdata &SortData) const {
  item::SortAllItems(SortData);
  if (SortData.Recurse) GetContained()->SortAllItems(SortData);
}


//==========================================================================
//
//  itemcontainer::PreProcessForBone
//
//==========================================================================
void itemcontainer::PreProcessForBone () {
  item::PreProcessForBone();
  Contained->PreProcessForBone();
}


//==========================================================================
//
//  itemcontainer::PostProcessForBone
//
//==========================================================================
void itemcontainer::PostProcessForBone () {
  item::PostProcessForBone();
  Contained->PostProcessForBone();
}


//==========================================================================
//
//  itemcontainer::FinalProcessForBone
//
//==========================================================================
void itemcontainer::FinalProcessForBone () {
  item::FinalProcessForBone();
  Contained->FinalProcessForBone();
}


//==========================================================================
//
//  itemcontainer::RemoveMaterial
//
//==========================================================================
material *itemcontainer::RemoveMaterial (material *Material) {
  Contained->MoveItemsTo(GetSlot());
  return item::RemoveMaterial(Material);
}


//==========================================================================
//
//  itemcontainer::SetLifeExpectancy
//
//==========================================================================
void itemcontainer::SetLifeExpectancy (int Base, int RandPlus) {
  LifeExpectancy = (RandPlus > 1 ? Base + RAND_N(RandPlus) : Base);
  Enable();
  Contained->SetLifeExpectancy(Base, RandPlus);
}


//==========================================================================
//
//  itemcontainer::CalculateEnchantment
//
//==========================================================================
void itemcontainer::CalculateEnchantment () {
  Contained->CalculateEnchantments();
}


//==========================================================================
//
//  itemcontainer::GetTeleportPriority
//
//==========================================================================
int itemcontainer::GetTeleportPriority () const {
  sLong Priority = item::GetTeleportPriority();
  for (stackiterator i = Contained->GetBottom(); i.HasItem(); ++i) Priority += i->GetTeleportPriority();
  return Priority;
}


//==========================================================================
//
//  itemcontainer::SetParameters
//
//==========================================================================
void itemcontainer::SetParameters (int Param) {
  SetIsLocked(Param & LOCKED);
}


//==========================================================================
//
//  itemcontainer::Disappear
//
//==========================================================================
void itemcontainer::Disappear () {
  Contained->MoveItemsTo(GetSlot());
  item::Disappear();
}


#endif
