#ifdef HEADER_PHASE
ITEM(locationmapbm, locationmap)
{
public:
  virtual void FinishReading (character *) override;
};


#else


void locationmapbm::FinishReading (character *Reader) {
  if (Reader->IsPlayer()) {
    /*
     * Note that read command cannot be used in wilderness, so we don't need to
     * special case the code below. If we ever let the player read in wilderness,
     * change it to take that into account.
     */
    game::RevealPOI(game::blackmarketPOI());
    GetArea()->SendNewDrawRequest();
    ADD_MESSAGE("The map reveals to you a secret site. You quickly commit the "
                "location to memory as the map burns up.");
  }

  RemoveFromSlot();
  SendToHell();
  Reader->EditExperience(INTELLIGENCE, 150, 1 << 12);
}


#endif
