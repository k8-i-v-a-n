#ifdef HEADER_PHASE
ITEM(lump, item)
{
protected:
  virtual void AddPostFix (festring &String, int) const override;
  virtual truth ShowMaterial () const override;
  virtual truth WeightIsIrrelevant () const override;
};


#else


void lump::AddPostFix (festring &String, int) const { AddLumpyPostFix(String); }
truth lump::ShowMaterial () const { return false; }
truth lump::WeightIsIrrelevant () const { return true; }


#endif
