#ifdef HEADER_PHASE
ITEM(magicalwhistle, whistle)
{
public:
  magicalwhistle ();

  virtual void BlowEffect (character *) override;
  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual void FinalProcessForBone () override;

protected:
  feuLong LastUsed;
};


#else


struct distancepair {
  inline distancepair (sLong aDistance, character *aChar) : Distance(aDistance), Char(aChar) {}
  inline bool operator < (const distancepair& D) const { return Distance > D.Distance; }

  sLong Distance;
  character *Char;
};


magicalwhistle::magicalwhistle() : LastUsed(0) {
}


void magicalwhistle::BlowEffect (character *Whistler) {
  if (LastUsed && game::GetTick() - LastUsed < 2000) {
    whistle::BlowEffect(Whistler);
    return;
  }

  LastUsed = game::GetTick();

  if (Whistler->IsPlayer()) {
    if (Whistler->CanHear()) ADD_MESSAGE("You produce a peculiar sound."); else ADD_MESSAGE("You blow %s.", CHAR_NAME(DEFINITE));
  } else if (Whistler->CanBeSeenByPlayer()) {
    if (PLAYER->CanHear()) {
      ADD_MESSAGE("%s blows %s and produces a peculiar sound.", Whistler->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
    } else {
      ADD_MESSAGE("%s blows %s.", Whistler->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
    }
  } else if (PLAYER->CanHear()) {
    ADD_MESSAGE("You hear a strange tune playing.");
  }

  const std::list<character *> &Member = Whistler->GetTeam()->GetMember();
  std::vector<distancepair> ToSort;
  v2 Pos = Whistler->GetPos();

  for (auto &it : Member) {
    if (it->IsEnabled() && Whistler != it) ToSort.push_back(distancepair((Pos-it->GetPos()).GetLengthSquare(), it));
  }

  if (ToSort.size() > 5) std::sort(ToSort.begin(), ToSort.end());

  for (uInt c = 0; c < 5 && c < ToSort.size(); ++c) ToSort[c].Char->TeleportNear(Whistler);

  game::CallForAttention(GetPos(), 400);
}


void magicalwhistle::Save (outputfile &SaveFile) const {
  whistle::Save(SaveFile);
  SaveFile << LastUsed;
}


void magicalwhistle::Load (inputfile &SaveFile) {
  whistle::Load(SaveFile);
  SaveFile >> LastUsed;
}


void magicalwhistle::FinalProcessForBone () {
  whistle::FinalProcessForBone();
  LastUsed = 0;
}


#endif
