#ifdef HEADER_PHASE
ITEM(mango, item)
{
public:
  virtual truth IsMango () const override;
};


#else


truth mango::IsMango () const { return true; }


#endif
