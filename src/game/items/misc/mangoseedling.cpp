#ifdef HEADER_PHASE
ITEM(mangoseedling, item)
{
public:
  truth Apply (character *Applier) override;
  virtual truth AllowAlphaEverywhere () const override;
  virtual truth IsMangoSeedling () const override;
  virtual truth IsAppliable (ccharacter *) const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth mangoseedling::IsAppliable (ccharacter *) const { return true; }
truth mangoseedling::AllowAlphaEverywhere () const { return true; }
truth mangoseedling::IsMangoSeedling () const { return true; }
int mangoseedling::GetClassAnimationFrames () const { return 32; }
col16 mangoseedling::GetOutlineColor (int) const { return MakeRGB16(118, 158, 226); }


truth mangoseedling::Apply (character *Applier) {
  if ((game::GetCurrentDungeonIndex() == NEW_ATTNAM)) {
    if (Applier->IsPlayer() && game::TweraifIsFree()) {
      Applier->EditAP(-1000);
      game::TextScreen(CONST_S(
        "You plant the seedling of the first new mango tree in New Attnam.\n"
        "The people of your home village gather around you cheering! Tweraif is\n"
        "now restored to its former glory and you remain there as honourary\n"
        "spiritual leader and hero of the new republic. You ensure that free\n"
        "and fair elections quickly ensue.\n"
        "\n"
        "You are victorious!"));
      game::GetCurrentArea()->SendNewDrawRequest();
      game::DrawEverything();
      festring Msg = CONST_S("restored Tweraif to independence and continued to further adventures");
      PLAYER->AddPolymorphedText(Msg, "while");
      Applier->AddScoreEntry(Msg, 1.15, false);
      PLAYER->ShowAdventureInfo(Msg);
      game::End(Msg);
    }
    RemoveFromSlot();
    SendToHell();
  } else {
    if (Applier->IsPlayer()) {
      ADD_MESSAGE("You feel that the climate is not quite right for growing mangoes.");
    }
  }
  Applier->EditAP(-1000);
  return true;
}


alpha mangoseedling::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50+(Frame*(31-Frame)>>1);
}


#endif
