#ifdef HEADER_PHASE
ITEM(scroll, item)
{
public:
  scroll ();
  virtual truth CanBeRead (character *) const override;
  virtual truth IsReadable (ccharacter *) const override;
  virtual truth IsScroll () const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual void AddDescriptionEntry (ccharacter *, festring &Entry, int Amount, truth ShowSpecialInfo) const override;
  virtual truth IsGenericScrollDescr () const override;
  virtual void SetScrollInspected () override;
  void Save (outputfile &saveFile) const override;
  void Load (inputfile &saveFile) override;
protected:
  truth WasInspected;
};


#else

scroll::scroll () : WasInspected(false) {}

truth scroll::IsReadable (ccharacter *) const { return true; }
truth scroll::IsScroll () const { return true; }
truth scroll::CanBeRead (character *Reader) const { return Reader->CanRead() || game::GetSeeWholeMapCheatMode(); }
truth scroll::IsGenericScrollDescr () const { return !WasInspected; }
void scroll::SetScrollInspected () { WasInspected = true; }


void scroll::Save (outputfile &saveFile) const {
  item::Save(saveFile);
  saveFile << WasInspected;
}


void scroll::Load (inputfile &saveFile) {
  item::Load(saveFile);
  saveFile >> WasInspected;
}


truth scroll::ReceiveDamage (character*, int Damage, int Type, int) {
  if ((Type&FIRE) && Damage && (GetMainMaterial()->GetInteractionFlags()&CAN_BURN) &&
      (Damage > 125 || !RAND_N(250 / Damage)))
  {
    if (CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s catches fire!", GetExtendedDescription().CStr());
    }
    RemoveFromSlot();
    SendToHell();
    return true;
  }
  return false;
}


void scroll::AddDescriptionEntry (ccharacter *CC, festring &Entry, int Amount, truth ShowSpecialInfo) const {
  if (IsGenericScrollDescr()) {
    if (Amount == 1) {
      Entry << " a scroll";
    } else {
      Entry << Amount << ' ';
      Entry << " scrolls";
    }
    /*
    if (ShowSpecialInfo) {
      //Entry << " [\1C" << GetWeight() * Amount << "g\2]";
      Entry << " [";
      Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
      Entry << "]";
    }
    */
  } else {
    item::AddDescriptionEntry(CC, Entry, Amount, ShowSpecialInfo);
  }
}


#endif
