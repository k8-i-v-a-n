#ifdef HEADER_PHASE
ITEM(encryptedscroll, scroll)
{
public:
  virtual void Be () override;
  virtual truth Read (character *) override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth IsEncryptedScroll () const override;
  virtual truth IsGenericScrollDescr () const override;
};


#else


void encryptedscroll::Be () {}
truth encryptedscroll::ReceiveDamage (character *, int, int, int) { return false; }
truth encryptedscroll::IsEncryptedScroll () const { return true; }
truth encryptedscroll::IsGenericScrollDescr () const { return false; }


truth encryptedscroll::Read (character *) {
  ADD_MESSAGE("You could never hope to decipher this top secret message. It is meant for Petrus's eyes only.");
  return false;
}


#endif
