#ifdef HEADER_PHASE
ITEM(mondedrpass, scroll)
{
public:
  virtual void FinishReading (character *) override;
  virtual truth IsMondedrPass () const override;
  virtual truth IsGenericScrollDescr () const override;
};


#else


truth mondedrpass::IsMondedrPass () const { return true; }
truth mondedrpass::IsGenericScrollDescr () const { return false; }


void mondedrpass::FinishReading (character *Reader) {
  ADD_MESSAGE("This sheet of paper contains many interesting facts about Mondedr. After finishing reading the pass burns up.");
  RemoveFromSlot();
  SendToHell();
  Reader->EditExperience(INTELLIGENCE, 3000, 1 << 12);
}


#endif
