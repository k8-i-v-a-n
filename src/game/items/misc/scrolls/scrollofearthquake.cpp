#ifdef HEADER_PHASE
ITEM(scrollofearthquake, scroll)
{
public:
  virtual void FinishReading (character *) override;
};


#else


void scrollofearthquake::FinishReading (character *Reader) {
  if (!game::GetCurrentLevel()->IsOnGround()) {
    ADD_MESSAGE("Suddenly a horrible earthquake shakes the level!");
    game::GetCurrentLevel()->PerformEarthquake();
  } else {
    ADD_MESSAGE("The ground shakes slightly.");
  }

  RemoveFromSlot();
  SendToHell();
  Reader->EditExperience(INTELLIGENCE, 150, 1 << 12);
}


#endif
