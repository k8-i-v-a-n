#ifdef HEADER_PHASE
ITEM(scrollofrepair, scroll)
{
public:
  virtual void FinishReading (character *) override;
};


#else


void scrollofrepair::FinishReading (character *Reader) {
  for (;;) {
    itemvector Item;
    if (!Reader->SelectFromPossessions(Item, CONST_S("Which item do you wish to repair?"),
                                       NO_MULTI_SELECT|SELECT_PAIR|STACK_ALLOW_NAMING,
                                       &item::IsRepairable))
    {
      ADD_MESSAGE("You notice you haven't got anything to repair.");
      return;
    }
    if (!Item.empty()) {
      if (Item[0]->HandleInPairs() && Item.size() == 1) {
        festring msg;
        msg << "Only one " << Item[0]->CHAR_NAME(UNARTICLED) << " will be repaired.";
        ADD_MESSAGE("%s", msg.CStr());
        if (!game::TruthQuestion(msg + "\nStill continue?")) continue;
      }
      if (Item.size() == 1) {
        ADD_MESSAGE("As you read the scroll, %s glows green and %s.", Item[0]->CHAR_NAME(DEFINITE), Item[0]->IsBroken() ? "fixes itself" : "its rust vanishes");
      } else {
        ADD_MESSAGE("As you read the scroll, %s glow green and %s.", Item[0]->CHAR_NAME(PLURAL), Item[0]->IsBroken() ? "fix themselves" : "their rust vanishes");
      }
      for (uInt c = 0; c < Item.size(); ++c) {
        Item[c]->RemoveRust();
        Item[c]->Fix();
      }
      break;
    } else {
      if (game::TruthQuestion(CONST_S("Really cancel read?"))) return;
    }
  }
  RemoveFromSlot();
  SendToHell();
  Reader->EditExperience(INTELLIGENCE, 300, 1 << 12);
}


#endif
