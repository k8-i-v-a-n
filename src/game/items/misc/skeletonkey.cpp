#ifdef HEADER_PHASE
ITEM(skeletonkey, key)
{
public:
  virtual truth CanOpenLockType (int AnotherLockType) const override;
  virtual truth AllowAlphaEverywhere () const override;
 protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const;
  virtual alpha GetOutlineAlpha (int) const;
};


#else

truth skeletonkey::CanOpenLockType (int AnotherLockType) const { return true; }
truth skeletonkey::AllowAlphaEverywhere () const { return true; }
int skeletonkey::GetClassAnimationFrames () const { return 32; }


alpha skeletonkey::GetOutlineAlpha (int Frame) const {
  if (!IsBroken()) {
    Frame &= 31;
    return Frame * (31 - Frame) >> 1;
  }
  return 0;
}


col16 skeletonkey::GetOutlineColor (int Frame) const {
  switch ((Frame&127) >> 5) {
    case 0: return BLUE;
    case 1: return GREEN;
    case 2: return RED;
    case 3: return YELLOW;
  }
  return TRANSPARENT_COLOR;
}


#endif
