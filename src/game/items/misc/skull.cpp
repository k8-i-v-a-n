#ifdef HEADER_PHASE
ITEM(skull, item)
{
  virtual truth IsBodyPart () const override;
  virtual truth IsASkull () const override;
  virtual truth IsHelmet (ccharacter *) const override;
  virtual truth IsInCorrectSlot (int) const override;
  virtual void AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight=true) const override;
  virtual void AddInventoryEntry (ccharacter *Viewer, festring &Entry, int Amount, truth ShowSpecialInfo) const override;
};


#else


truth skull::IsBodyPart () const { return true; }
truth skull::IsASkull () const { return true; }
truth skull::IsHelmet (ccharacter *) const { return true; }
truth skull::IsInCorrectSlot (int I) const { return I == HELMET_INDEX; }


void skull::AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight) const {
  Entry << " [";
  if (includeWeight) {
    Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
    Entry << ", ";
  }
  Entry << "AV\x18\1Y" << GetStrengthValue() << "\2]";
}


void skull::AddInventoryEntry (ccharacter *Viewer, festring &Entry, int Amount, truth ShowSpecialInfo) const {
  if (Amount == 1) {
    AddName(Entry, INDEFINITE);
  } else {
    Entry << Amount << ' ';
    AddName(Entry, PLURAL);
  }
  if (ShowSpecialInfo) {
    AddSpecialInfo(Viewer, Entry, Amount, true);
  }
}


#endif
