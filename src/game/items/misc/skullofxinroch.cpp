#ifdef HEADER_PHASE
ITEM(skullofxinroch, item)
{
public:
  virtual truth IsASkull () const override;
  virtual void Be () override;
  virtual bool SpecialOfferEffect (int) override;
  virtual truth AllowSpoil () const override;
  virtual truth Spoils () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth skullofxinroch::IsASkull () const { return true; }
void skullofxinroch::Be () {}
truth skullofxinroch::AllowSpoil () const { return false; }
truth skullofxinroch::Spoils () const { return false; }
int skullofxinroch::GetClassAnimationFrames () const { return 32; }
col16 skullofxinroch::GetOutlineColor (int) const { return MakeRGB16(180, 0, 0); }


alpha skullofxinroch::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50+(Frame*(31-Frame)>>1);
}


bool skullofxinroch::SpecialOfferEffect (int GodNumber) {
  if (GodNumber == INFUSCOR) {
    god *Receiver = game::GetGod(GodNumber);
    Receiver->AdjustRelation(500);
    ADD_MESSAGE("You sacrifice %s. %s appreciates your generous offer truly.", CHAR_NAME(DEFINITE), Receiver->GetName());
    return true;
  }
  return false;
}


#endif
