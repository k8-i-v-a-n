#ifdef HEADER_PHASE
ITEM(stick, item)
{
protected:
  virtual void AddPostFix (festring &String, int) const override;
  virtual truth ShowMaterial () const override;
  virtual truth WeightIsIrrelevant () const override;
};


#else


void stick::AddPostFix (festring &String, int) const { AddLumpyPostFix(String); }
truth stick::ShowMaterial () const { return false; }
truth stick::WeightIsIrrelevant () const { return true; }


#endif
