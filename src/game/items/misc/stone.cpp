#ifdef HEADER_PHASE
ITEM(stone, item)
{
public:
  virtual sLong GetTruePrice () const override;
  virtual truth IsLuxuryItem (ccharacter *) const override;

protected:
  virtual truth WeightIsIrrelevant () const override;
};


#else


truth stone::IsLuxuryItem (ccharacter *) const { return GetTruePrice() > 0; }
truth stone::WeightIsIrrelevant() const { return true; }
sLong stone::GetTruePrice () const { return item::GetTruePrice() << 1; }


#endif
