#ifdef HEADER_PHASE
ITEM(trinket, item)
{
protected:
  virtual col16 GetMaterialColorB (int) const override;
  virtual col16 GetMaterialColorC (int) const override;
};


#else


col16 trinket::GetMaterialColorB (int) const {
  sLong cfg = GetConfig();
  if (cfg == POTTED_CACTUS) return MakeRGB16(87, 59, 12);
  if (cfg == POTTED_PLANT) return MakeRGB16(200, 0, 0);
  if (cfg == SMALL_CLOCK) return MakeRGB16(124, 50, 16);
  if (cfg == LARGE_CLOCK) return MakeRGB16(124, 50, 16);
  return MakeRGB16(0, 0, 0);
}

col16 trinket::GetMaterialColorC(int) const {
  sLong cfg = GetConfig();
  if (cfg == POTTED_CACTUS) return MakeRGB16(0, 160, 0);
  if (cfg == POTTED_PLANT) return MakeRGB16(0, 160, 0);
  return MakeRGB16(0, 0, 0);
}
#endif
