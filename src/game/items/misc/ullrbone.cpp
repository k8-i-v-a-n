#ifdef HEADER_PHASE
ITEM(ullrbone, item)
{
public:
  ullrbone ();
  virtual truth Zap (character *, v2, int) override;
  virtual void ChargeFully (character *) override;
  virtual truth IsZappable (const character *) const override;
  virtual truth IsChargeable (const character*) const override;
  virtual truth HitEffect (character *, character *, v2, int, int, truth) override;
  virtual void Be () override;
  virtual void AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight=true) const override;
  virtual void AddInventoryEntry (ccharacter *, festring &, int, truth) const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth AllowAlphaEverywhere () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;

protected:
  int TimesUsed;
  int Charges;
};


#else


void ullrbone::ChargeFully (character *) { TimesUsed = 0; }
truth ullrbone::IsZappable (const character *) const { return true; }
truth ullrbone::IsChargeable (const character*) const { return true; }
void ullrbone::Be () {}
truth ullrbone::AllowAlphaEverywhere () const { return true; }
int ullrbone::GetClassAnimationFrames () const { return 32; }
col16 ullrbone::GetOutlineColor (int) const { return MakeRGB16(210, 210, 210); }


ullrbone::ullrbone () : TimesUsed(0), Charges(12) {
}


truth ullrbone::HitEffect (character *Enemy, character *Hitter, v2 HitPos, int BodyPartIndex, int Direction, truth BlockedByArmour) {
  truth BaseSuccess = item::HitEffect(Enemy, Hitter, HitPos, BodyPartIndex, Direction, BlockedByArmour);
  if (Enemy->IsEnabled() && RAND_2) {
    if (Enemy->IsPlayer() || Hitter->IsPlayer() || Enemy->CanBeSeenByPlayer() || Hitter->CanBeSeenByPlayer())
      ADD_MESSAGE("A burst of %s bone of Ullr's unholy energy fries %s.", Hitter->CHAR_POSSESSIVE_PRONOUN, Enemy->CHAR_DESCRIPTION(DEFINITE));
    return Enemy->ReceiveBodyPartDamage(Hitter, 3 + RAND_4, ENERGY, BodyPartIndex, Direction) || BaseSuccess;
  }
  return BaseSuccess;
}


truth ullrbone::Zap (character *Zapper, v2, int Direction) {
  if (Charges > TimesUsed) {
    ADD_MESSAGE("BANG! You zap %s!", CHAR_NAME(DEFINITE));
    Zapper->EditExperience(PERCEPTION, 150, 1 << 10);
    beamdata Beam(
      Zapper,
      CONST_S("killed by ") + GetName(INDEFINITE),
      Zapper->GetPos(),
      YELLOW,
      BEAM_LIGHTNING,
      Direction,
      50,
      0,
      this
    );
    (GetLevel()->*level::GetBeam(PARTICLE_BEAM))(Beam);
    ++TimesUsed;
  } else {
    ADD_MESSAGE("Click!");
  }
  return true;
}


void ullrbone::AddSpecialInfo (ccharacter *Viewer, festring &Entry, int Amount, truth includeWeight) const {
  //Entry << " [\1C" << GetWeight() << "g\2, DAM\x18\1Y" << GetBaseMinDamage() << "\2-\1Y" << GetBaseMaxDamage() << "\2";
  Entry << " [";
  if (includeWeight) {
    Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
    Entry << ", ";
  }
  Entry << "DAM\x18\1Y" << GetBaseMinDamage() << "\2-\1Y" << GetBaseMaxDamage() << "\2";
  Entry << ", " << GetBaseToHitValueDescription();
  if (!IsBroken()) Entry << ", " << GetStrengthValueDescription();
  if (Viewer) {
    int CWeaponSkillLevel = Viewer->GetCWeaponSkillLevel(this);
    int SWeaponSkillLevel = Viewer->GetSWeaponSkillLevel(this);
    if (CWeaponSkillLevel || SWeaponSkillLevel) {
      Entry << ", skill\x18\1C" << CWeaponSkillLevel << '/' << SWeaponSkillLevel << "\2";
    }
  }
  if (TimesUsed == 1) {
    Entry << ", used\x18\1C1\2\x18time";
  } else if (TimesUsed) {
    Entry << ", used\x18\1C" << TimesUsed << "\2\x18times";
  }
  Entry << "\2]";
}


void ullrbone::AddInventoryEntry (ccharacter *Viewer, festring &Entry, int Amount, truth ShowSpecialInfo) const {
  // never piled
  AddName(Entry, INDEFINITE);
  if (ShowSpecialInfo) {
    AddSpecialInfo(Viewer, Entry, Amount, true);
  }
}


truth ullrbone::ReceiveDamage (character *Damager, int Damage, int Type, int) {
  if (TimesUsed != 12 && (Type & (PHYSICAL_DAMAGE|FIRE|ENERGY)) &&
      Damage && (Damage > 50 || !RAND_N(100 / Damage)))
  {
    festring DeathMsg = CONST_S("killed by an explosion of ");
    AddName(DeathMsg, INDEFINITE);
    if (Damager) DeathMsg << " caused @bk";
    if (GetSquareUnder()->CanBeSeenByPlayer(true)) ADD_MESSAGE("%s explodes!", GetExtendedDescription().CStr());
    lsquare *Square = GetLSquareUnder();
    RemoveFromSlot();
    SendToHell();
    Square->GetLevel()->Explosion(Damager, DeathMsg, Square->GetPos(), (6-TimesUsed)*50);
    return true;
  }
  return false;
}


alpha ullrbone::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50+(Frame*(31-Frame)>>1);
}


#endif
