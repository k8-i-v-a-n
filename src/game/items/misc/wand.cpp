#ifdef HEADER_PHASE
ITEM(wand, item)
{
public:
  virtual truth Apply (character *) override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void ChargeFully (character *) override;
  virtual truth IsAppliable (ccharacter *) const override;
  virtual truth IsZappable (ccharacter *) const override;
  virtual truth IsChargeable (ccharacter *) const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth Zap (character *, v2, int) override;
  virtual void AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight=true) const override;
  virtual void AddInventoryEntry (ccharacter *, festring&, int, truth) const override;
  virtual sLong GetPrice () const override;
  virtual truth IsExplosive () const override;
  virtual void BreakEffect (character *, cfestring &) override;

protected:
  virtual void PostConstruct () override;

  feuLong GetSpecialParameters () const;

protected:
  int Charges;
  int TimesUsed;
};


#else


void wand::ChargeFully (character *) { TimesUsed = 0; }
truth wand::IsAppliable (ccharacter *) const { return true; }
truth wand::IsZappable (ccharacter *) const { return true; }
truth wand::IsChargeable (ccharacter *) const { return true; }
truth wand::IsExplosive () const { return true; }
sLong wand::GetPrice () const { return (Charges > TimesUsed ? item::GetPrice() : 0); }


truth wand::Apply (character *Terrorist) {
  if (Terrorist->IsPlayer() &&
      !game::TruthQuestion(CONST_S("Are you sure you want to break ")+GetName(DEFINITE)+"?"))
  {
    return false;
  }

  if (Terrorist->IsPlayer()) {
    ADD_MESSAGE("You bend %s with all your strength.", CHAR_NAME(DEFINITE));
  } else if (Terrorist->CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s bends %s with all %s strength.", Terrorist->CHAR_NAME(DEFINITE),
                CHAR_NAME(INDEFINITE), Terrorist->CHAR_POSSESSIVE_PRONOUN);
  }

  if (Terrorist->IsPlayer() || Terrorist->CanBeSeenByPlayer()) {
    ADD_MESSAGE("%s %s.", CHAR_NAME(DEFINITE), GetBreakMsg().CStr());
  }

  BreakEffect(Terrorist, CONST_S("killed by ")+GetName(INDEFINITE)+" broken @bk");
  Terrorist->DexterityAction(5);
  return true;
}


void wand::Save (outputfile& SaveFile) const {
  item::Save(SaveFile);
  SaveFile << TimesUsed << Charges;
}


void wand::Load (inputfile& SaveFile) {
  item::Load(SaveFile);
  SaveFile >> TimesUsed >> Charges;
}


truth wand::ReceiveDamage (character *Damager, int Damage, int Type, int) {
  if ((Type&(FIRE|ENERGY|PHYSICAL_DAMAGE)) && Damage && (Damage > 125 || !RAND_N(250/Damage))) {
    festring DeathMsg = CONST_S("killed by an explosion of ");
    AddName(DeathMsg, INDEFINITE);

    if (Damager) DeathMsg << " caused @bk";

    if (CanBeSeenByPlayer()) ADD_MESSAGE("%s %s.", GetExtendedDescription().CStr(), GetBreakMsg().CStr());

    BreakEffect(Damager, DeathMsg);
    return true;
  }

  return false;
}


void wand::PostConstruct () {
  Charges = GetMinCharges()+RAND_N(GetMaxCharges()-GetMinCharges()+1);
  TimesUsed = 0;
}


truth wand::Zap (character *Zapper, v2, int Direction) {
  if (Charges <= TimesUsed) { ADD_MESSAGE("Nothing happens."); return true; }

  Zapper->EditExperience(PERCEPTION, 150, 1 << 10);

  beamdata Beam (
    Zapper,
    CONST_S("killed by ") + GetName(INDEFINITE) + " zapped @bk",
    Zapper->GetPos(),
    GetBeamColor(),
    GetBeamEffect(),
    Direction,
    GetBeamRange(),
    GetSpecialParameters(),
    this
  );

  (GetLevel()->*level::GetBeam(GetBeamStyle()))(Beam);
  ++TimesUsed;
  return true;
}


void wand::AddSpecialInfo (ccharacter *CC, festring &Entry, int Amount, truth includeWeight) const {
  //Entry << " [\1C" << GetWeight();
  if (includeWeight || TimesUsed != 0) {
    Entry << " [";
    if (includeWeight) {
      Entry.PutWeight(GetWeight() * Amount, "\1C", "\2");
      if (TimesUsed != 0) Entry << ", ";
    }
    if (TimesUsed == 1) {
      Entry << "used\x18\1C1\2\x18time]";
    } else if (TimesUsed) {
      Entry << "used\x18\1C" << TimesUsed << "\2\x18times]";
    } else {
      Entry << "\2]";
    }
  }
}


void wand::AddInventoryEntry (ccharacter *CC, festring &Entry, int Amount, truth ShowSpecialInfo) const { // never piled
  AddName(Entry, INDEFINITE);
  if (ShowSpecialInfo) {
    AddSpecialInfo(CC, Entry, Amount, true);
  }
}


void wand::BreakEffect (character *Terrorist, cfestring &DeathMsg) {
  v2 Pos = GetPos();
  level *Level = GetLevel();
  RemoveFromSlot();
  feuLong StackSize = Level->AddRadiusToSquareStack(Pos, GetBreakEffectRangeSquare());
  lsquare **SquareStack = Level->GetSquareStack();
  feuLong c;

  for (c = 0; c < StackSize; ++c) SquareStack[c]->RemoveFlags(IN_SQUARE_STACK);

  fearray<lsquare*> Stack(SquareStack, StackSize);
  (Level->*level::GetBeamEffectVisualizer(GetBeamStyle()))(Stack, GetBeamColor());

  beamdata Beam (
    Terrorist,
    DeathMsg,
    YOURSELF,
    GetSpecialParameters()
  );

  for (c = 0; c < Stack.Size; ++c) (Stack[c]->*lsquare::GetBeamEffect(GetBeamEffect()))(Beam);

  SendToHell();
}


feuLong wand::GetSpecialParameters () const {
  if (GetConfig() == WAND_OF_MIRRORING) return (MIRROR_IMAGE|(1000 << LE_BASE_SHIFT)|(1000 << LE_RAND_SHIFT));
  return 0;
}


#endif
