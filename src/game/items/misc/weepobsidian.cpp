#ifdef HEADER_PHASE
ITEM(weepobsidian, stone) {
public:
  weepobsidian ();
  virtual void Be () override;
  virtual truth IsWeepObsidian () const override;
protected:
  virtual truth CalculateHasBe () const override;
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const;
  virtual alpha GetOutlineAlpha (int) const;
};


#else

weepobsidian::weepobsidian () { Enable(); }
truth weepobsidian::IsWeepObsidian () const { return true; }
truth weepobsidian::CalculateHasBe () const { return true; }
int weepobsidian::GetClassAnimationFrames () const { return 32; }
col16 weepobsidian::GetOutlineColor (int) const { return MakeRGB16(0, 0, 180); }

alpha weepobsidian::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50 + (Frame * (31 - Frame) >> 1);
}


void weepobsidian::Be () {
  stone::Be();

  if (Exists() && !game::IsInWilderness()) {
    if (!RAND_N(1000)) {
      beamdata Beam (
        0,
        CONST_S("drowned by the tears of ") + CHAR_NAME(DEFINITE),
        YOURSELF,
        0
      );
      GetLSquareUnder()->LiquidRain(Beam, WATER);

      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s releases torrential rain.", CHAR_NAME(DEFINITE));
      }
    }
  }
}


#endif
