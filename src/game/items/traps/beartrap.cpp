#ifdef HEADER_PHASE
ITEM(beartrap, itemtrap<item>)
{
public:
  beartrap ();
  beartrap (const beartrap &);
  virtual ~beartrap ();

  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual void StepOnEffect (character *) override;
  virtual truth CheckPickUpEffect (character *) override;
  virtual truth IsPickable (character *) const override;
  virtual truth Apply (character *) override;
  virtual v2 GetBitmapPos (int) const override;
  virtual truth IsDangerous (ccharacter *) const override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth NeedDangerSymbol () const override;
  virtual void Fly (character *, int, int) override;
  virtual feuLong GetTrapID () const override;
  virtual feuLong GetVictimID () const override;
  virtual void UnStick () override;
  virtual void UnStick (int I) override;
  virtual truth TryToUnStick (character *, v2) override;
  virtual void RemoveFromSlot () override;
  virtual int GetTrapType () const override;
  virtual void PreProcessForBone () override;
  virtual void PostProcessForBone () override;
  virtual void DonateSlotTo (item *) override;
  virtual truth HitByTrap (character *Thrower, character *Dude) override;
  virtual truth TryToDeactivateTrap (character *Char) override;
  virtual truth IsTrap () const override;
  virtual truth IsAppliable (ccharacter *) const override;

protected:
  virtual truth AddAdjective (festring &, truth) const override;
  truth IsStuck() const;
  int GetBaseTrapDamage () const;
  void StepOnEffectInternal (character *Stepper, bool IgnoreSearching);

protected:
  trapdata TrapData;
};


#else


//==========================================================================
//
//  beartrap::beartrap
//
//==========================================================================
beartrap::beartrap () {
  if (!game::IsLoading()) {
    TrapData.TrapID = game::CreateNewTrapID(this);
    TrapData.VictimID = 0;
  }
}


//==========================================================================
//
//  beartrap::beartrap
//
//==========================================================================
beartrap::beartrap (const beartrap &Trap) : mybase(Trap) {
  TrapData.TrapID = game::CreateNewTrapID(this);
  TrapData.VictimID = 0;
}


//==========================================================================
//
//  beartrap::~beartrap
//
//==========================================================================
beartrap::~beartrap () {
  game::RemoveTrapID(TrapData.TrapID);
}


//==========================================================================
//
//  beartrap::Load
//
//==========================================================================
void beartrap::Load (inputfile &SaveFile) {
  mybase::Load(SaveFile);
  SaveFile >> TrapData;
  game::AddTrapID(this, TrapData.TrapID);
}


//==========================================================================
//
//  beartrap::Save
//
//==========================================================================
void beartrap::Save (outputfile &SaveFile) const {
  mybase::Save(SaveFile);
  SaveFile << TrapData;
}


//==========================================================================
//
//  beartrap::IsTrap
//
//==========================================================================
truth beartrap::IsTrap () const {
  return true;
}


//==========================================================================
//
//  beartrap::IsAppliable
//
//==========================================================================
truth beartrap::IsAppliable (ccharacter *) const {
  return !IsBroken();
}


//==========================================================================
//
//  beartrap::IsDangerous
//
//==========================================================================
truth beartrap::IsDangerous (ccharacter *) const {
  return Active;
}


//==========================================================================
//
//  beartrap::NeedDangerSymbol
//
//==========================================================================
truth beartrap::NeedDangerSymbol () const {
  return IsActive();
}


//==========================================================================
//
//  beartrap::GetTrapID
//
//==========================================================================
feuLong beartrap::GetTrapID () const {
  return TrapData.TrapID;
}


//==========================================================================
//
//  beartrap::GetVictimID
//
//==========================================================================
feuLong beartrap::GetVictimID () const {
  return TrapData.VictimID;
}


//==========================================================================
//
//  beartrap::UnStick
//
//==========================================================================
void beartrap::UnStick () {
  TrapData.VictimID = 0;
}


//==========================================================================
//
//  beartrap::UnStick
//
//==========================================================================
void beartrap::UnStick (int I) {
  TrapData.BodyParts &= ~(1 << I);
}


//==========================================================================
//
//  beartrap::GetTrapType
//
//==========================================================================
int beartrap::GetTrapType () const {
  return GetType() | ITEM_TRAP;
}


//==========================================================================
//
//  beartrap::IsStuck
//
//==========================================================================
truth beartrap::IsStuck () const {
  return TrapData.VictimID;
}


//==========================================================================
//
//  beartrap::AddAdjective
//
//==========================================================================
truth beartrap::AddAdjective (festring& String, truth Articled) const {
  return
    (IsActive() && AddActiveAdjective(String, Articled)) ||
    (!IsActive() && item::AddAdjective(String, Articled));
}


//==========================================================================
//
//  beartrap::TryToUnStick
//
//==========================================================================
truth beartrap::TryToUnStick (character *Victim, v2) {
  feuLong TrapID = GetTrapID();
  int Modifier = GetBaseTrapDamage() * 40 /
                    Max(Victim->GetAttribute(DEXTERITY) + Victim->GetAttribute(ARM_STRENGTH), 1);

  if (!RAND_N(Max(Modifier, 2))) {
    Victim->RemoveTrap(TrapID);
    TrapData.VictimID = 0;
    if (Victim->IsPlayer()) {
      ADD_MESSAGE("You manage to free yourself from %s.", CHAR_NAME(DEFINITE));
    } else if (Victim->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s manages to free %sself from %s.", Victim->CHAR_NAME(DEFINITE),
                  Victim->CHAR_OBJECT_PRONOUN, CHAR_NAME(DEFINITE));
    }
    Victim->EditAP(-500);
    return true;
  }

  if (!RAND_N(Max(Modifier << 1, 2))) {
    Victim->RemoveTrap(TrapID);
    TrapData.VictimID = 0;
    Break(Victim);
    if (Victim->IsPlayer()) {
      ADD_MESSAGE("You are freed.");
    } else if (Victim->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s is freed.", Victim->CHAR_NAME(DEFINITE));
    }
    Victim->EditAP(-500);
    return true;
  }

  Modifier = Victim->GetAttribute(DEXTERITY) + Victim->GetAttribute(ARM_STRENGTH) * 3 / 20;

  if (!RAND_N(Max(Modifier, 2))) {
    int BodyPart = Victim->RandomizeHurtBodyPart(TrapData.BodyParts);
    if (Victim->IsPlayer()) {
      ADD_MESSAGE("You manage to hurt your %s even more.", Victim->GetBodyPartName(BodyPart).CStr());
    } else if (Victim->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s hurts %s %s more with %s.", Victim->CHAR_NAME(DEFINITE),
                  Victim->GetPossessivePronoun().CStr(),
                  Victim->GetBodyPartName(BodyPart).CStr(), CHAR_NAME(DEFINITE));
    }
    Victim->ReceiveBodyPartDamage(0, GetBaseTrapDamage(), PHYSICAL_DAMAGE, BodyPart, YOURSELF, false, false, false);
    Victim->CheckDeath(CONST_S("died while trying to escape from ") + GetName(INDEFINITE), 0, IGNORE_TRAPS);
    Victim->EditAP(-1000);
    return false;
  }

  if (!RAND_N(Max(Modifier << 1, 2))) {
    int VictimBodyPart = Victim->RandomizeTryToUnStickBodyPart(ALL_BODYPART_FLAGS&~TrapData.BodyParts);
    if (VictimBodyPart != NONE_INDEX) {
      TrapData.BodyParts |= 1 << VictimBodyPart;
      Victim->AddTrap(GetTrapID(), 1 << VictimBodyPart);
      if (Victim->IsPlayer()) {
        ADD_MESSAGE("You fail to free yourself from %s and your %s is stuck in it in the attempt.",
                    CHAR_NAME(DEFINITE), Victim->GetBodyPartName(VictimBodyPart).CStr());
      } else if (Victim->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s tries to free %sself from %s but is stuck more tightly in it in the attempt.",
                    Victim->CHAR_NAME(DEFINITE), Victim->CHAR_OBJECT_PRONOUN, CHAR_NAME(DEFINITE));
      }
      Victim->ReceiveBodyPartDamage(0, GetBaseTrapDamage() << 1, PHYSICAL_DAMAGE, VictimBodyPart, YOURSELF, false, false, false);
      Victim->CheckDeath(CONST_S("died while trying to escape from ") + GetName(INDEFINITE), 0, IGNORE_TRAPS);
      Victim->EditAP(-1000);
      return true;
    }
  }

  if (Victim->IsPlayer()) {
    ADD_MESSAGE("You are unable to escape from %s.", CHAR_NAME(DEFINITE));
  }

  Victim->EditAP(-1000);
  return false;
}


//==========================================================================
//
//  beartrap::HitByTrap
//
//  thrown
//
//==========================================================================
truth beartrap::HitByTrap (character *Thrower, character *Dude) {
  //k8: disabled characters are usually dead ones.
  //k8: but there is no need to check for this, because `item::HitCharacter()`
  //k8: should perform all the checks for us.
  if (Dude && /*Dude->IsEnabled() &&*/ IsActive() && !IsBroken()) {
    #if 0
    ConLogf("DBG: bear trap hits '%s'", Dude->GetNameSingular().CStr());
    #endif
    StepOnEffectInternal(Dude, true);
    if (!IsActive()) return true;
  }
  return false;
}


//==========================================================================
//
//  beartrap::StepOnEffect
//
//==========================================================================
void beartrap::StepOnEffect (character *Stepper) {
  StepOnEffectInternal(Stepper, false);
}


//==========================================================================
//
//  beartrap::StepOnEffectInternal
//
//==========================================================================
void beartrap::StepOnEffectInternal (character *Stepper, bool IgnoreSearching) {
  if (IsActive() && !IsBroken()) {
    // NPCs should get some benefit from searching too, so make them immune to
    // traps if they have it.
    if (!IgnoreSearching && !Stepper->IsPlayer() &&
        Stepper->StateIsActivated(SEARCHING) && RAND_N(100) <= 84)
    {
      return;
    }

    int StepperBodyPart = Stepper->GetRandomStepperBodyPart();
    if (StepperBodyPart == NONE_INDEX) return;

    TrapData.VictimID = Stepper->GetID();
    TrapData.BodyParts = 1 << StepperBodyPart;
    Stepper->AddTrap(GetTrapID(), 1 << StepperBodyPart);

    if (Stepper->IsPlayer()) {
      ADD_MESSAGE("You step in %s and it traps your %s.", CHAR_NAME(INDEFINITE),
                  Stepper->GetBodyPartName(StepperBodyPart).CStr());
    } else if (Stepper->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s is trapped in %s.", Stepper->CHAR_NAME(DEFINITE), CHAR_NAME(INDEFINITE));
    }

    SetIsActive(false);
    SendNewDrawAndMemorizedUpdateRequest();

    if (Stepper->IsPlayer()) {
      game::AskForEscPress(CONST_S("Trap activated!"));
    }

    Stepper->ReceiveBodyPartDamage(0, GetBaseTrapDamage() << 1, PHYSICAL_DAMAGE,
                                   StepperBodyPart, YOURSELF, false, false, false);
    Stepper->CheckDeath(CONST_S("died by stepping to ") + GetName(INDEFINITE), 0, IGNORE_TRAPS);
  }
}


//==========================================================================
//
//  beartrap::CheckPickUpEffect
//
//==========================================================================
truth beartrap::CheckPickUpEffect (character *Picker) {
  if (Picker->IsStuckToTrap(GetTrapID())) {
    if (Picker->IsPlayer()) {
      ADD_MESSAGE("You are tightly stuck in %s.", CHAR_NAME(DEFINITE));
    }
    return false;
  }
  SetIsActive(false);
  return true;
}


//==========================================================================
//
//  beartrap::TryToDeactivateTrap
//
//==========================================================================
truth beartrap::TryToDeactivateTrap (character *User) {
  if (User->IsStuckToTrap(GetTrapID())) {
    if (User->IsPlayer()) {
      ADD_MESSAGE("You are tightly stuck in %s.", CHAR_NAME(DEFINITE));
    }
    return false;
  }

  if (IsBroken() || !IsActive()) {
    if (User->IsPlayer()) {
      ADD_MESSAGE("%s is useless.", CHAR_NAME(DEFINITE));
    }
    return false;
  }

  if (User->IsPlayer() &&
      !game::TruthQuestion(CONST_S("Are you sure you want to deactivate ") + GetName(DEFINITE) + "?"))
  {
    return false;
  }

  room *Room = GetRoom();
  if (Room) Room->HostileAction(User);

  if (User->GetAttribute(DEXTERITY) < femath::LoopRoll(90, 1000)) {
    // you can break it too
    User->EditAP(-1000);
    int Modifier = GetBaseTrapDamage() * 40 /
                      Max(User->GetAttribute(DEXTERITY) + User->GetAttribute(ARM_STRENGTH), 1);
    if (!RAND_N(Max(Modifier << 1, 2))) {
      SetIsActive(false);
      Break(User);
      SendNewDrawAndMemorizedUpdateRequest();
      if (User->IsPlayer()) {
        ADD_MESSAGE("You broke %s.", CHAR_NAME(DEFINITE));
      } else if (User->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s broke %s.", User->CHAR_NAME(DEFINITE), CHAR_NAME(DEFINITE));
      }
    } else {
      int UserBodyPart = User->GetRandomApplyBodyPart();
      if (User->IsPlayer()) {
        ADD_MESSAGE("Somehow you manage to hurt your %s in %s.",
                    User->GetBodyPartName(UserBodyPart).CStr(), CHAR_NAME(DEFINITE));
      } else if (User->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s somehow hurts %sself in %s.", User->CHAR_NAME(DEFINITE),
                    User->CHAR_OBJECT_PRONOUN, CHAR_NAME(DEFINITE));
      }
      if (User->IsPlayer()) {
        game::AskForEscPress(CONST_S("Trap activated!"));
      }
      User->ReceiveBodyPartDamage(0, 2 + RAND_4, PHYSICAL_DAMAGE, UserBodyPart,
                                  YOURSELF, false, false, false);
      User->CheckDeath(CONST_S("died by failing to deactivate ") + GetName(INDEFINITE), 0, IGNORE_TRAPS);
    }
  } else {
    SetIsActive(!IsActive());
    if (User->IsPlayer()) {
      ADD_MESSAGE("%s is now %sactive.", CHAR_NAME(DEFINITE), IsActive() ? "" : "in");
    }
    IvanAssert(!IsActive());
    SendNewDrawAndMemorizedUpdateRequest();
    User->DexterityAction(10);
    User->EditAP(-500);
  }

  return true;
}


//==========================================================================
//
//  beartrap::Apply
//
//==========================================================================
truth beartrap::Apply (character *User) {
  if (IsBroken()) {
    if (User->IsPlayer()) {
      ADD_MESSAGE("%s is useless.", CHAR_NAME(DEFINITE));
    }
    return false;
  }

  if (User->IsPlayer() &&
      !game::TruthQuestion(CONST_S("Are you sure you want to activate ") + GetName(DEFINITE) + "?"))
  {
    return false;
  }

  room *Room = GetRoom();
  if (Room) Room->HostileAction(User);

  if (User->GetAttribute(DEXTERITY) < femath::LoopRoll(90, 1000)) {
    int UserBodyPart = User->GetRandomApplyBodyPart();
    if (User->IsPlayer()) {
      ADD_MESSAGE("Somehow you manage to trap your %s in %s.",
                  User->GetBodyPartName(UserBodyPart).CStr(), CHAR_NAME(DEFINITE));
    } else if (User->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s somehow traps %sself in %s.", User->CHAR_NAME(DEFINITE),
                  User->CHAR_OBJECT_PRONOUN, CHAR_NAME(DEFINITE));
    }
    RemoveFromSlot();
    User->GetStackUnder()->AddItem(this);
    TrapData.VictimID = User->GetID();
    TrapData.BodyParts = 1 << UserBodyPart;
    User->AddTrap(GetTrapID(), 1 << UserBodyPart);
    SendNewDrawAndMemorizedUpdateRequest();
    if (User->IsPlayer()) {
      game::AskForEscPress(CONST_S("Trap activated!"));
    }
    User->ReceiveBodyPartDamage(0, 1 + RAND_2, PHYSICAL_DAMAGE, UserBodyPart,
                                YOURSELF, false, false, false);
    User->CheckDeath(CONST_S("died failing to set ") + GetName(INDEFINITE), 0, IGNORE_TRAPS);
  } else {
    if (User->IsPlayer()) {
      ADD_MESSAGE("%s is now %sactive.", CHAR_NAME(DEFINITE), IsActive() ? "in" : "");
    }
    SetIsActive(!IsActive());
    User->DexterityAction(10);
    if (IsActive()) {
      Team = User->GetTeam()->GetID();
      PlantTrap(User);
      //RemoveFromSlot();
      //User->GetStackUnder()->AddItem(this);
    }
  }

  return true;
}


//==========================================================================
//
//  beartrap::GetBitmapPos
//
//==========================================================================
v2 beartrap::GetBitmapPos (int Frame) const {
  if (!IsBroken()) return IsActive() ? v2(32, 304) : v2(32, 320);
  return item::GetBitmapPos(Frame);
}


//==========================================================================
//
//  beartrap::IsPickable
//
//==========================================================================
truth beartrap::IsPickable (character *Picker) const {
  return !IsActive() && !Picker->IsStuckToTrap(GetTrapID());
}


//==========================================================================
//
//  beartrap::ReceiveDamage
//
//==========================================================================
truth beartrap::ReceiveDamage (character *Damager, int Damage, int Type, int) {
  if (!IsBroken() && (Type & PHYSICAL_DAMAGE) && Damage) {
    if (Damage > 125 || !RAND_N(250 / Damage)) {
      SetIsActive(false);
      Break(Damager);
      return true;
    }
    if (IsActive()) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s snaps shut.", CHAR_NAME(DEFINITE));
      }
      SetIsActive(false);
      SendNewDrawAndMemorizedUpdateRequest();
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  beartrap::Fly
//
//==========================================================================
void beartrap::Fly (character *Thrower, int Direction, int Force) {
  if (!IsStuck()) {
    item::Fly(Thrower, Direction, Force);
  }
}


//==========================================================================
//
//  beartrap::PreProcessForBone
//
//==========================================================================
void beartrap::PreProcessForBone () {
  mybase::PreProcessForBone();
  game::RemoveTrapID(TrapData.TrapID);
  TrapData.TrapID = 0;
}


//==========================================================================
//
//  beartrap::PostProcessForBone
//
//==========================================================================
void beartrap::PostProcessForBone () {
  mybase::PostProcessForBone();
  TrapData.TrapID = game::CreateNewTrapID(this);
}


//==========================================================================
//
//  beartrap::GetBaseTrapDamage
//
//==========================================================================
int beartrap::GetBaseTrapDamage () const {
  int Modifier = GetMainMaterial()->GetStrengthValue() / 50;
  Modifier *= Modifier;
  Modifier >>= 1;
  return (Modifier ? Modifier + RAND_N(Modifier) : 1);
}


//==========================================================================
//
//  beartrap::RemoveFromSlot
//
//==========================================================================
void beartrap::RemoveFromSlot () {
  character *Char = game::SearchCharacter(GetVictimID());
  if (Char) Char->RemoveTrap(GetTrapID());
  TrapData.VictimID = 0;
  item::RemoveFromSlot();
}


//==========================================================================
//
//  beartrap::DonateSlotTo
//
//==========================================================================
void beartrap::DonateSlotTo (item *Item) {
  character *Char = game::SearchCharacter(GetVictimID());
  if (Char) Char->RemoveTrap(GetTrapID());
  TrapData.VictimID = 0;
  item::DonateSlotTo(Item);
}


#endif
