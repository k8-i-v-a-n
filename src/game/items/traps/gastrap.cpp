#ifdef HEADER_PHASE
ITEM(gastrap, itemtrap<materialcontainer>)
{
public:
  virtual void StepOnEffect (character *) override;
  virtual truth ReceiveDamage (character *, int, int, int) override;
  virtual truth Apply (character *User) override;
  virtual truth IsDangerous (ccharacter *) const override;
  virtual truth CheckPickUpEffect (character *) override;
  virtual truth TryToDeactivateTrap (character *Char) override;
  virtual truth IsTrap () const override;

protected:
  virtual truth AddAdjective (festring &, truth) const override;
  virtual void AddPostFix (festring &String, int) const override;
};


#else


//==========================================================================
//
//  gastrap::IsTrap
//
//==========================================================================
truth gastrap::IsTrap () const {
  return true;
}


//==========================================================================
//
//  gastrap::IsDangerous
//
//==========================================================================
truth gastrap::IsDangerous (ccharacter *) const {
  return Active;
}


//==========================================================================
//
//  gastrap::AddPostFix
//
//==========================================================================
void gastrap::AddPostFix (festring &String, int) const {
  AddContainerPostFix(String, true);
}


//==========================================================================
//
//  gastrap::AddAdjective
//
//==========================================================================
truth gastrap::AddAdjective (festring &String, truth Articled) const {
  return (IsActive() && AddActiveAdjective(String, Articled)) ||
         (!IsActive() && item::AddAdjective(String, Articled));
}


//==========================================================================
//
//  gastrap::StepOnEffect
//
//==========================================================================
void gastrap::StepOnEffect (character *Stepper) {
  // NPCs should get some benefit from searching too, so make them immune to
  // traps if they have it.
  if (!Stepper->IsPlayer() && Stepper->StateIsActivated(SEARCHING) && RAND_N(100) <= 84) return;

  if (IsActive() && !IsBroken()) {
    if (Stepper->IsPlayer()) {
      ADD_MESSAGE("You step on %s.", GetExtendedDescription().CStr());
    } else if (Stepper->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s steps on %s.", Stepper->CHAR_NAME(DEFINITE), GetExtendedDescription().CStr());
    }

    if (Stepper->IsPlayer()) {
      game::AskForEscPress(CONST_S("Trap activated!"));
    }

    // Reveal the trap when we step on it.
    int ViewerTeam = Stepper->GetTeam()->GetID();
    if (ViewerTeam != Team && DiscoveredByTeam.find(ViewerTeam) == DiscoveredByTeam.end()) {
      DiscoveredByTeam.insert(ViewerTeam);
      SendNewDrawAndMemorizedUpdateRequest();
    }

    if (GetSecondaryMaterial()) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("It releases some gas!");
      }
      material *GasMaterial = GetSecondaryMaterial();
      GetLevel()->GasExplosion(static_cast<gas*>(GasMaterial), GetLSquareUnder(), Stepper);
    } else {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("Luckily, it's empty.");
      }
    }

    if (!RAND_N(10)) {
      if (CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s gets jammed.", CHAR_NAME(DEFINITE));
      }
      SetIsActive(false);
      Break(Stepper);
    }
  }
}


//==========================================================================
//
//  gastrap::ReceiveDamage
//
//==========================================================================
truth gastrap::ReceiveDamage (character *Damager, int Damage, int Type, int) {
  if ((Type & PHYSICAL_DAMAGE) && Damage) {
    if (Damage > 125 || !RAND_N(250 / Damage)) {
      if (GetSquareUnder()->CanBeSeenByPlayer(true)) {
        ADD_MESSAGE("%s shatters!", GetExtendedDescription().CStr());
      }

      if (GetSecondaryMaterial()) {
        if (CanBeSeenByPlayer()) ADD_MESSAGE("It releases some gas!");
        material *GasMaterial = GetSecondaryMaterial();
        GetLevel()->GasExplosion(static_cast<gas*>(GasMaterial), GetLSquareUnder(), Damager);
      } else {
        if (CanBeSeenByPlayer()) ADD_MESSAGE("Luckily, it's empty.");
      }

      RemoveFromSlot();
      SendToHell();
      return true;
    }

    if (IsActive()) {
      if (CanBeSeenByPlayer()) ADD_MESSAGE("%s gets jammed.", CHAR_NAME(DEFINITE));
      SetIsActive(false);
      Break(Damager);
      return true;
    }
  }

  if (Type & (ENERGY|SOUND) && Damage) {
    if (Damage > 50 || !RAND_N(100 / Damage)) {
      if (GetSquareUnder()->CanBeSeenByPlayer(true)) {
        ADD_MESSAGE("%s shatters!", GetExtendedDescription().CStr());
      }

      if (GetSecondaryMaterial()) {
        if (CanBeSeenByPlayer()) ADD_MESSAGE("It releases some gas!");
        material *GasMaterial = GetSecondaryMaterial();
        GetLevel()->GasExplosion(static_cast<gas*>(GasMaterial), GetLSquareUnder(), Damager);
      } else {
        if (CanBeSeenByPlayer()) ADD_MESSAGE("Luckily, it's empty.");
      }

      RemoveFromSlot();
      SendToHell();
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  gastrap::TryToDeactivateTrap
//
//==========================================================================
truth gastrap::TryToDeactivateTrap (character *User) {
  if (IsBroken() || !IsActive()) {
    if (User->IsPlayer()) {
      ADD_MESSAGE("%s is useless.", CHAR_NAME(DEFINITE));
    }
    return false;
  }

  if (User->IsPlayer() &&
      !game::TruthQuestion(CONST_S("Are you sure you want to deactivate ") + GetName(DEFINITE) + "?"))
  {
    return false;
  }

  room *Room = GetRoom();
  if (Room) Room->HostileAction(User);

  if (User->GetAttribute(DEXTERITY) < femath::LoopRoll(90, 1000)) {
    User->EditAP(-1000);
    ReceiveDamage(User, 666, PHYSICAL_DAMAGE, 0);
  } else {
    SetIsActive(!IsActive());
    if (User->IsPlayer()) {
      ADD_MESSAGE("%s is now %sactive.", CHAR_NAME(DEFINITE), IsActive() ? "" : "in");
    }
    IvanAssert(!IsActive());
    SendNewDrawAndMemorizedUpdateRequest();
    User->DexterityAction(10);
    User->EditAP(-500);
  }

  return true;
}


//==========================================================================
//
//  gastrap::Apply
//
//==========================================================================
truth gastrap::Apply (character *User) {
  if (IsBroken()) {
    if (User->IsPlayer()) {
      ADD_MESSAGE("%s is jammed and useless.", CHAR_NAME(DEFINITE));
    }
    return false;
  }

  if (User->IsPlayer() &&
      !game::TruthQuestion(CONST_S("Are you sure you want to activate ") + GetName(DEFINITE) + "?"))
  {
    return false;
  }

  room *Room = GetRoom();
  if (Room) Room->HostileAction(User);

  if (User->IsPlayer()) {
    ADD_MESSAGE("%s is now %sactive.", CHAR_NAME(DEFINITE), IsActive() ? "in" : "");
  }

  SetIsActive(!IsActive());
  User->DexterityAction(10);

  if (IsActive()) {
    Team = User->GetTeam()->GetID();
    PlantTrap(User);
    //RemoveFromSlot();
    //User->GetStackUnder()->AddItem(this);
  }

  return true;
}


//==========================================================================
//
//  gastrap::CheckPickUpEffect
//
//==========================================================================
truth gastrap::CheckPickUpEffect (character *) {
  SetIsActive(false);
  return true;
}


#endif
