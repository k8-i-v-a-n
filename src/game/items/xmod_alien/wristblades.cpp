#ifdef HEADER_PHASE
ITEM(wristblades, meleeweapon)
{
public:
  virtual truth AllowAlphaEverywhere () const override;

protected:
  virtual int GetClassAnimationFrames () const override;
  virtual col16 GetOutlineColor (int) const override;
  virtual alpha GetOutlineAlpha (int) const override;
};


#else


truth wristblades::AllowAlphaEverywhere () const { return true; }
int wristblades::GetClassAnimationFrames () const { return 32; }
col16 wristblades::GetOutlineColor (int) const { return MakeRGB16(128, 128, 192); }

alpha wristblades::GetOutlineAlpha (int Frame) const {
  Frame &= 31;
  return 50+(Frame*(31-Frame)>>1);
}


#endif
