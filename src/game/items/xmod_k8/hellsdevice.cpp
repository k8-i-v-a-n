#ifdef HEADER_PHASE
ITEM(hellsdevice, item) {
public:
  virtual truth IsAppliable (ccharacter *User) const override;
  virtual truth Apply (character *User) override;

private:
  void explode (character *User, v2 pos);
};


#else


truth hellsdevice::IsAppliable (ccharacter *User) const { return true; }


truth hellsdevice::Apply (character *User) {
  // cannot be broken
  //if (IsBroken()) { ADD_MESSAGE("%s is totally broken.", CHAR_NAME(DEFINITE)); return false; }

  v2 FireBallPos = ERROR_V2;
  beamdata Beam(
    User,
    CONST_S("killed by the spells of ")+User->GetName(INDEFINITE),
    YOURSELF,
    0
  );
  ADD_MESSAGE("This could be loud...");

  v2 Input = game::PositionQuestion(CONST_S("Where do you wish to send the fireball? "
                                            "[direction keys move cursor, space accepts]"),
                                    User->GetPos(), &game::TeleportHandler, 0, ivanconfig::GetLookZoom());

  if (Input == ERROR_V2) {
    // esc pressed
    ADD_MESSAGE("You choose not to summon a fireball... phew!");
    return false;
  }

  lsquare *Square = GetNearLSquare(Input);

  FireBallPos = Input;
  if (FireBallPos == ERROR_V2) return false;

  if (Square->GetPos() == User->GetPos()) ADD_MESSAGE("The scroll explodes in your face!");

  //User->EditExperience(INTELLIGENCE, 150, 1<<12);
  explode(User, FireBallPos);

  return true;
}


void hellsdevice::explode (character *User, v2 pos) {
  festring dmsg = CONST_S("killed by the spells of ")+User->GetName(INDEFINITE);
  lsquare *Square = GetNearLSquare(pos);
  if (Square) {
    Square->DrawParticles(RED);
    GetLevel()->Explosion(User, dmsg, pos, /*75+RAND_N(25)-RAND_N(25)*/400+RAND_N(100));
  }
  for (int f = 0; f < 8; ++f) {
    if (RAND_N(100) > 30) {
      v2 dest = pos+game::GetMoveVector(f);
      Square = GetNearLSquare(dest);
      if (Square) {
        Square->DrawParticles(RED);
        GetLevel()->Explosion(User, dmsg, dest, /*75+RAND_N(25)-RAND_N(25)*/400+RAND_N(100));
      }
    }
  }
}


#endif
