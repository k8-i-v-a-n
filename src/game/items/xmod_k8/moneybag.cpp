#ifdef HEADER_PHASE
ITEM(moneybag, item)
{
public:
  moneybag ();

  virtual truth Apply (character *Beggar) override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth IsConsumable () const override;
  virtual truth IsAppliable (ccharacter *Beggar) const override;
  virtual truth CanBeHardened (ccharacter *) const override;
  virtual truth IsMoneyBag () const override;

  virtual sLong GetTruePrice () const override;

protected:
  int moneyAmount;
};


#else


moneybag::moneybag ()
  : moneyAmount(1+RAND_N(300))
{
  if (RAND_N(100) < 10) moneyAmount += 1000;
}

truth moneybag::IsConsumable () const { return false; }
truth moneybag::CanBeHardened (ccharacter *) const { return false; }
truth moneybag::IsMoneyBag () const { return true; }
sLong moneybag::GetTruePrice () const { return moneyAmount; }
truth moneybag::IsAppliable (ccharacter *Beggar) const { return Beggar->IsPlayer(); }


truth moneybag::Apply (character *Beggar) {
  if (!Beggar->IsPlayer()) return false;
  if (game::TruthQuestion(CONST_S("Do you really want to open ") + GetName(DEFINITE) + "?")) {
    ADD_MESSAGE("You opened %s and found \1Y%i\2 gold coins.%s", CHAR_NAME(DEFINITE),
                moneyAmount, (moneyAmount > 500 ? " Yay!" : ""));
    Beggar->EditMoney(moneyAmount);
    // opening this requires at least some amount of dexterity ;-)
    Beggar->DexterityAction(5);
    Beggar->EditAP(-100);
    RemoveFromSlot();
    SendToHell();
    return true;
  } else {
    return false;
  }
}


void moneybag::Save (outputfile &saveFile) const {
  item::Save(saveFile);
  saveFile << moneyAmount;
}


void moneybag::Load (inputfile &saveFile) {
  item::Load(saveFile);
  saveFile >> moneyAmount;
}


#endif
