/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __IVANDEF_H__
#define __IVANDEF_H__

/*
 * Global defines for the project IVAN.
 * This file is created to decrease the need of including headers in
 * other headers just for the sake of some silly macros, because it
 * decreases compilation efficiency and may cause cross-including
 *
 * List of macros that should be gathered here:
 * 1. all numeric defines used in multiple .cpp or .h files
 *    except those #defined in balance.h and confdef.h
 * 2. all inline functions used in multiple .cpp or .h files
 *    and independent enough (do not require other headers)
 * 3. class construction macros used in multiple .h files
 */

#include "v2.h"
#include "festring.h"

#define IVAN_VERSION "0.666.8"


class item;
class material;
class character;

typedef const item citem;
typedef const material cmaterial;
typedef const character ccharacter;


struct databasebase {
  int Config;
  feuLong CommonFlags;
  feuLong NameFlags;
  festring CfgStrName;
  std::unordered_set<cchar *, ccharhasher, ccharequ> FieldsSet;
  std::unordered_set<cchar *, ccharhasher, ccharequ> FieldsCopied;

  FORCE_INLINE void CopyFieldInfoFrom (const databasebase *aParent) {
    FieldsSet.clear();
    FieldsCopied.clear();
    if (aParent) {
      for (auto it : aParent->FieldsCopied) FieldsCopied.insert(it);
      for (auto it : aParent->FieldsSet) FieldsCopied.insert(it);
    }
  }

  FORCE_INLINE bool IsFieldSet (cchar *fname) const {
    return (fname && fname[0] && FieldsSet.find(fname) != FieldsSet.end());
  }

  FORCE_INLINE bool IsFieldSet (cfestring &fname) const {
    return IsFieldSet(fname.CStr());
  }

  FORCE_INLINE bool IsFieldCopied (cchar *fname) const {
    return (fname && fname[0] && FieldsCopied.find(fname) != FieldsCopied.end());
  }

  FORCE_INLINE bool IsFieldCopied (cfestring &fname) const {
    return IsFieldCopied(fname.CStr());
  }

  FORCE_INLINE bool IsFieldDefined (cchar *fname) const {
    return (IsFieldSet(fname) || IsFieldCopied(fname));
  }

  FORCE_INLINE bool IsFieldDefined (cfestring &fname) const {
    return (IsFieldSet(fname) || IsFieldCopied(fname));
  }
};


template <class type, class base, class prototype>
class sysbase : public base {
public:
  typedef sysbase<type, base, prototype> mybase;

  static type *Spawn (int Config=0, int SpecialFlags=0) {
    type *T = new type;
    T->Initialize(Config, SpecialFlags);
    return T;
  }

  static type *Clone (const type *T) { return new type(*T); }

  virtual ~sysbase () {}
  virtual const prototype *FindProtoType () const { return &ProtoType; }

  static const prototype ProtoType;
};


template <class type, class base, class prototype>
class simplesysbase : public base {
public:
  typedef simplesysbase<type, base, prototype> mybase;

  static type *Spawn () { return new type; }

  virtual ~simplesysbase () {}
  virtual const prototype *GetProtoType () const { return &ProtoType; }

  static const prototype ProtoType;
};


#define SYSTEM_SPECIALIZATIONS(name)\
  name##prototype** name##_ProtoData;\
  valuemap name##_CodeNameMap;\
  int name##_ProtoSize;\
  \
  template<> name##prototype**& protocontainer<name>::GetProtoData() { return name##_ProtoData; }\
  template<> valuemap& protocontainer<name>::GetCodeNameMap() { return name##_CodeNameMap; }\
  template<> int& protocontainer<name>::GetSizeRef() { return name##_ProtoSize; }

#define EXTENDED_SYSTEM_SPECIALIZATIONS(name)\
  SYSTEM_SPECIALIZATIONS(name)\
  databasecreator<name>::databasemembermap name##_DataBaseMemberMap;\
  template<> databasecreator<name>::databasemembermap&\
  databasecreator<name>::GetDataBaseMemberMap() { return name##_DataBaseMemberMap; }\
  const name##prototype name::ProtoType

#define DATA_BASE_VALUE(type, data) type Get##data() const { return DataBase->data; }
#define DATA_BASE_VALUE_WITH_PARAMETER(type, data, param) type Get##data(param) const { return DataBase->data; }
#define DATA_BASE_TRUTH(data) truth data() const { return DataBase->data; }
#define DATA_BASE_TRUTH_WITH_PARAMETER(data, param) truth data(param) const { return DataBase->data; }


#ifndef GCONST
# define GCONST(name_)  (game::GetGlobalConst(CONST_S(#name_)))
#endif

#ifndef GMODCONST
# define GMODCONST(name_)  (game::FindGlobalValue(CONST_S(#name_), -666))
#endif

#ifndef GCONSTOR0
# define GCONSTOR0(name_)  (game::FindGlobalValue(CONST_S(#name_), 0))
#endif

#ifndef GCONSTDEF
# define GCONSTDEF(name_,defval_)  (game::FindGlobalValue(CONST_S(#name_), defval_))
#endif

#define LOADER_DEBUG                GCONSTOR0(LOADER_DEBUG)
#define POI_PLACEMENT_DEBUG         GCONSTOR0(POI_PLACEMENT_DEBUG)
#define BONE_DEBUG                  GCONSTOR0(BONE_DEBUG)
#define DEBUG_ALWAYS_CREATE_BONES   GCONSTOR0(DEBUG_ALWAYS_CREATE_BONES)
#define DEBUG_ALWAYS_LOAD_BONES     GCONSTOR0(DEBUG_ALWAYS_LOAD_BONES)

#define DEBUG_GO              GCONSTOR0(DEBUG_GO)

#define DEBUG_HELL_VERBOSE  GCONSTOR0(DEBUG_HELL_VERBOSE)

#define DEBUG_SHOW_STETHOSCOPE_COUNTERS   GCONSTOR0(DEBUG_SHOW_STETHOSCOPE_COUNTERS)

#define DEBUG_PICKUP_AI   GCONSTOR0(DEBUG_PICKUP_AI)


#define MAX_GHOST_CREATION_BUFFS      GCONSTOR0(MAX_GHOST_CREATION_BUFFS)
#define DANGERMAP_REFRESH_RATE        GCONSTOR0(DANGERMAP_REFRESH_RATE)
#define DANGERMAP_UPDATE_IN_WORLDMAP  GCONSTOR0(DANGERMAP_UPDATE_IN_WORLDMAP)
#define DEBUG_DANGER_MAP_UPDATES      GCONSTOR0(DEBUG_DANGER_MAP_UPDATES)

#define DEBUG_ATTACK_FLASH_PAUSE      GCONSTOR0(DEBUG_ATTACK_FLASH_PAUSE)

#define MONSTER_GEN_DELAY   GCONST(HAS_HIT)

#define DEBUG_MONSTER_SPAWN   GCONSTOR0(DEBUG_MONSTER_SPAWN)
#define DMS_SHOW_SKIPS        (1<<0)
#define DMS_SHOW_CONSIDERS    (1<<1)
#define DMS_SHOW_SPAWNS       (1<<2)
#define DMS_SHOW_GEN_MSG      (1<<8)

#define DEBUG_ITEM_SPAWN    GCONSTOR0(DEBUG_ITEM_SPAWN)
#define DIS_SHOW_POSSIBLE   (1<<0)
#define DIS_SHOW_ACCEPT     (1<<1)
#define DIS_SHOW_REJECT     (1<<2)
#define DIS_SHOW_CATSEARCH  (1<<7)
#define DIS_SHOW_GEN_MSG    (1<<8)

#define DEBUG_SOUND   GCONSTOR0(DEBUG_SOUND)


/*
#define HAS_HIT         GCONST(HAS_HIT)
#define HAS_BLOCKED     GCONST(HAS_BLOCKED)
#define HAS_DODGED      GCONST(HAS_DODGED)
#define HAS_DIED        GCONST(HAS_DIED)
#define DID_NO_DAMAGE   GCONST(DID_NO_DAMAGE)
#define HAS_FAILED      GCONST(HAS_FAILED)
*/
#define HAS_HIT         (0)
#define HAS_BLOCKED     (1)
#define HAS_DODGED      (2)
#define HAS_DIED        (3)
#define DID_NO_DAMAGE   (4)
#define HAS_FAILED      (5)

const char *GetHitResultCStr (int hitres);

/* weapon usability rate */
#define WEAPON_OK                   (0)
#define WEAPON_SOMEWHAT_DIFFICULT   (1)
#define WEAPON_MUCH_TROUBLE         (2)
#define WEAPON_EXTREMLY_DIFFICULT   (3)
#define WEAPON_UNUSABLE             (4)


#define OVER_FED_LEVEL      GCONST(OVER_FED_LEVEL)
#define BLOATED_LEVEL       GCONST(BLOATED_LEVEL)
#define SATIATED_LEVEL      GCONST(SATIATED_LEVEL)
#define NOT_HUNGER_LEVEL    GCONST(NOT_HUNGER_LEVEL)
#define HUNGER_LEVEL        GCONST(HUNGER_LEVEL)
#define VERY_HUNGER_LEVEL   GCONST(VERY_HUNGER_LEVEL)

#define OVER_LOADED   GCONST(OVER_LOADED)
#define STRESSED      GCONST(STRESSED)
#define BURDENED      GCONST(BURDENED)
#define UNBURDENED    GCONST(UNBURDENED)

#define STARVING      GCONST(STARVING)
#define VERY_HUNGRY   GCONST(VERY_HUNGRY)
#define HUNGRY        GCONST(HUNGRY)
#define NOT_HUNGRY    GCONST(NOT_HUNGRY)
#define SATIATED      GCONST(SATIATED)
#define BLOATED       GCONST(BLOATED)
#define OVER_FED      GCONST(OVER_FED)

//WARNING! state count and state order MUST be synced with "define.def"
#define STATES  32

#define POLYMORPHED         GCONST(POLYMORPHED)
#define HASTE               GCONST(HASTE)
#define SLOW                GCONST(SLOW)
#define POLYMORPH_CONTROL   GCONST(POLYMORPH_CONTROL)
#define LIFE_SAVED          GCONST(LIFE_SAVED)
#define LYCANTHROPY         GCONST(LYCANTHROPY)
#define INVISIBLE           GCONST(INVISIBLE)
#define INFRA_VISION        GCONST(INFRA_VISION)
#define ESP                 GCONST(ESP)
#define POISONED            GCONST(POISONED)
#define TELEPORT            GCONST(TELEPORT)
#define POLYMORPH           GCONST(POLYMORPH)
#define TELEPORT_CONTROL    GCONST(TELEPORT_CONTROL)
#define PANIC               GCONST(PANIC)
#define CONFUSED            GCONST(CONFUSED)
#define PARASITE_TAPE_WORM  GCONST(PARASITE_TAPE_WORM)
#define SEARCHING           GCONST(SEARCHING)
#define GAS_IMMUNITY        GCONST(GAS_IMMUNITY)
#define LEVITATION          GCONST(LEVITATION)
#define LEPROSY             GCONST(LEPROSY)
#define HICCUPS             GCONST(HICCUPS)
#define VAMPIRISM           GCONST(VAMPIRISM)
#define SWIMMING            GCONST(SWIMMING)
#define DETECTING           GCONST(DETECTING)
#define ETHEREAL_MOVING     GCONST(ETHEREAL_MOVING)
#define FEARLESS            GCONST(FEARLESS)
#define POLYMORPH_LOCK      GCONST(POLYMORPH_LOCK)
#define REGENERATION        GCONST(REGENERATION)
#define DISEASE_IMMUNITY    GCONST(DISEASE_IMMUNITY)
#define TELEPORT_LOCK       GCONST(TELEPORT_LOCK)
#define FASTING             GCONST(FASTING)
#define PARASITE_MIND_WORM  GCONST(PARASITE_MIND_WORM)

// for mind worm
#define BOIL        GCONST(BOIL)
#define HATCHLING   GCONST(HATCHLING)

#define THROW_ITEM_TYPES   GCONST(THROW_ITEM_TYPES)
/*ThrowFlags */
#define THROW_BONE          GCONST(THROW_BONE)
#define THROW_POTION        GCONST(THROW_POTION)
#define THROW_AXE           GCONST(THROW_AXE)
#define THROW_GAS_GRENADE   GCONST(THROW_GAS_GRENADE)
#define THROW_WAND          GCONST(THROW_WAND)
#define THROW_DAGGER        GCONST(THROW_DAGGER)


#define POTTED_CACTUS   GCONST(POTTED_CACTUS)
#define POTTED_PLANT    GCONST(POTTED_PLANT)
#define SMALL_CLOCK     GCONST(SMALL_CLOCK)
#define LARGE_CLOCK     GCONST(LARGE_CLOCK)
#define DEAD_FISH       GCONST(DEAD_FISH)
#define BONE_FISH       GCONST(BONE_FISH)
#define SMOKED_FISH     GCONST(SMOKED_FISH)


#define TUNA            GCONST(TUNA)
#define SARDINE         GCONST(SARDINE)
#define SMOKED_SARDINE  GCONST(SMOKED_SARDINE)


#define RED_SNAKE     GCONST(RED_SNAKE)
#define GREEN_SNAKE   GCONST(GREEN_SNAKE)
#define BLUE_SNAKE    GCONST(BLUE_SNAKE)


#define LARGE       GCONST(LARGE)
#define GIANT       GCONST(GIANT)
#define ARANEA      GCONST(ARANEA)
#define PHASE       GCONST(PHASE)
#define GIANT_GOLD  GCONST(GIANT_GOLD)


#define REFUSE_ZOMBIE_EATING_INT  GCONSTOR0(REFUSE_ZOMBIE_EATING_INT)


/*
#define TORSO     GCONST(TORSO)
#define HEAD      GCONST(HEAD)
#define RIGHT_ARM GCONST(RIGHT_ARM)
#define LEFT_ARM  GCONST(LEFT_ARM)
#define ARMS      GCONST(ARMS)
#define GROIN     GCONST(GROIN)
#define RIGHT_LEG GCONST(RIGHT_LEG)
#define LEFT_LEG  GCONST(LEFT_LEG)
#define LEGS      GCONST(LEGS)
#define OTHER     GCONST(OTHER)
#define ALL       GCONST(ALL)
*/
#define HEAD        (1)
#define TORSO       (2)
#define RIGHT_ARM   (4)
#define LEFT_ARM    (8)
#define ARMS        (RIGHT_ARM | LEFT_ARM)
#define GROIN       (16)
#define RIGHT_LEG   (32)
#define LEFT_LEG    (64)
#define LEGS        (RIGHT_LEG | LEFT_LEG)
#define OTHER       (128)
#define ALL         (255)

/*
#define PHYSICAL_DAMAGE     GCONST(PHYSICAL_DAMAGE)
#define SOUND               GCONST(SOUND)
#define ACID                GCONST(ACID)
#define FIRE                GCONST(FIRE)
#define ELECTRICITY         GCONST(ELECTRICITY)
#define ENERGY              GCONST(ENERGY)
#define POISON              GCONST(POISON)
#define DRAIN               GCONST(DRAIN)
#define MUSTARD_GAS_DAMAGE  GCONST(MUSTARD_GAS_DAMAGE)
#define PSI                 GCONST(PSI)
#define THROW               GCONST(THROW)
*/
#define PHYSICAL_DAMAGE     (1)
#define SOUND               (2)
#define ACID                (4)
#define FIRE                (8)
#define ELECTRICITY         (16)
#define ENERGY              (32)
#define POISON              (64)
#define DRAIN               (128)
#define MUSTARD_GAS_DAMAGE  (256)
#define PSI                 (512)
#define THROW               (32768)

/*
#define UNDEFINED     GCONST(UNDEFINED)
#define MALE          GCONST(MALE)
#define FEMALE        GCONST(FEMALE)
#define TRANSSEXUAL   GCONST(TRANSSEXUAL)
*/
#define UNDEFINED     (0)
#define MALE          (1)
#define FEMALE        (2)
#define TRANSSEXUAL   (3)

#define ALL_BODYPART_FLAGS  (0x7F)

/* The maximum bodyparts a character can have */

#define MAX_BODYPARTS       (7)
#define HUMANOID_BODYPARTS  (7)

//FIXME: sadly, used in `switch`
#define TORSO_INDEX       (0)
#define HEAD_INDEX        (1)
#define RIGHT_ARM_INDEX   (2)
#define LEFT_ARM_INDEX    (3)
#define GROIN_INDEX       (4)
#define RIGHT_LEG_INDEX   (5)
#define LEFT_LEG_INDEX    (6)

#define NONE_INDEX  MAX_BODYPARTS

#define DIRECTION_COMMAND_KEYS  (8)
#define EXTENDED_DIRECTION_COMMAND_KEYS   (9)
#define YOURSELF                (8) /* special direction for damage method */
#define RANDOM_DIR              (9)

#ifndef LIGHT_BORDER
#define LIGHT_BORDER  (80)
#endif

/* alignments */
#define ALPP  (0)
#define ALP   (1)
#define AL    (2)
#define ALM   (3)
#define ANP   (4)
#define AN    (5)
#define ANM   (6)
#define ACP   (7)
#define AC    (8)
#define ACM   (9)
#define ACMM  (10)

/* flags for `GetName()` */
#define UNARTICLED      (0) /* singular, no article */
#define PLURAL          (1) /* plural, no article */
#define ARTICLE_BIT     (2) /* force article */
#define DEFINITE        (2) /* definite: singular + article */
#define INDEFINE_BIT    (4) /* indefinite, no article */
#define INDEFINITE      (6) /* indefinite, with article */
#define STRIP_STATES    (8) /* skip rust state and hasted/slowed state */
#define STRIP_MATERIAL  (16) /* skip rust state and hasted/slowed state */
#define STRIPPED        (8 + 16) /* skip rust state, hasted/slowed state, and material */
/* this is also flag for `AddPostFix()` */
#define NO_FLUIDS       (32) /* do not add fluid info */

//#define TRANSPARENT_COLOR   (0xF81F) /* pink */

/* number of raw bitmap files with graphics */
#define RAW_TYPES       (7)
#define RAW_FILE_TYPES  (7)

/* graphic file indices */
#define GR_GLTERRAIN  (0)
#define GR_OLTERRAIN  (1)
#define GR_ITEM       (2)
#define GR_CHARACTER  (3)
#define GR_HUMANOID   (4)
#define GR_EFFECT     (5)
#define GR_CURSOR     (6)

/* number of raw bitmap files with graphics for other things */
#define GRAPHIC_TYPES   (5)

#define GR_WTERRAIN   (0)
#define GR_FOW        (1)
#define GR_SYMBOL     (2)
#define GR_SMILEY     (3)
#define GR_HUMANOID   (4)

/* SpecialFlags for graphics system. No one knows what "ST_" means... */

#define ST_NORMAL             (0)
#define ST_RIGHT_ARM          (8)
#define ST_LEFT_ARM           (16)
#define ST_GROIN              (24)
#define ST_RIGHT_LEG          (32)
#define ST_LEFT_LEG           (40)
#define ST_OTHER_BODYPART     (48)
#define ST_WIELDED            (56)
#define ST_CLOAK              (64)
#define ST_LIGHTNING          (128)
#define ST_DISALLOW_R_COLORS  (256)
#define ST_FLAME_1            (512)
#define ST_FLAME_2            (1024)
#define ST_FLAME_3            (2048)
#define ST_FLAME_4            (4096)
#define ST_FLAMES (ST_FLAME_1|ST_FLAME_2|ST_FLAME_3|ST_FLAME_4)
#define ST_FLAME_SHIFT  (9)
#define ST_SIMPLE_ITEM_OUTLINE  (16384)

#define ST_NO_OUTLINE_FLAGS  (ST_RIGHT_ARM | \
                              ST_LEFT_ARM | \
                              ST_GROIN | \
                              ST_RIGHT_LEG | \
                              ST_LEFT_LEG | \
                              ST_OTHER_BODYPART | \
                              ST_WIELDED | \
                              ST_CLOAK)


#define WOBBLE                    (1)
#define WOBBLE_HORIZONTALLY_BIT   (2)
#define WOBBLE_VERTICALLY         WOBBLE
#define WOBBLE_HORIZONTALLY       (WOBBLE|WOBBLE_HORIZONTALLY_BIT)
#define WOBBLE_SPEED_SHIFT        (2)
#define WOBBLE_SPEED_RANGE        (3<<WOBBLE_SPEED_SHIFT)
#define WOBBLE_FREQ_SHIFT         (4)
#define WOBBLE_FREQ_RANGE         (3<<WOBBLE_FREQ_SHIFT)

// declaration moved to "igraph"
extern cv2 SILHOUETTE_SIZE;

//WARNING! keep in sync with "define.def"!
#define ITEM_CATEGORIES   (18)

#define ANY_CATEGORY  GCONST(ANY_CATEGORY)
#define HELMET        GCONST(HELMET)
#define AMULET        GCONST(AMULET)
#define CLOAK         GCONST(CLOAK)
#define BODY_ARMOR    GCONST(BODY_ARMOR)
#define WEAPON        GCONST(WEAPON)
#define SHIELD        GCONST(SHIELD)
#define RING          GCONST(RING)
#define GAUNTLET      GCONST(GAUNTLET)
#define BELT          GCONST(BELT)
#define BOOT          GCONST(BOOT)
#define FOOD          GCONST(FOOD)
#define POTION        GCONST(POTION)
#define SCROLL        GCONST(SCROLL)
#define BOOK          GCONST(BOOK)
#define WAND          GCONST(WAND)
#define TOOL          GCONST(TOOL)
#define VALUABLE      GCONST(VALUABLE)
#define MISC          GCONST(MISC)

#define GOOD      GCONST(GOOD)
#define NEUTRAL   GCONST(NEUTRAL)
#define EVIL      GCONST(EVIL)
// new
#define TOPPLED   GCONST(TOPPLED)

/* ConsumeTypes */

#define CT_FRUIT          GCONST(CT_FRUIT)
#define CT_MEAT           GCONST(CT_MEAT)
#define CT_METAL          GCONST(CT_METAL)
#define CT_MINERAL        GCONST(CT_MINERAL)
#define CT_LIQUID         GCONST(CT_LIQUID)
#define CT_BONE           GCONST(CT_BONE)
#define CT_PROCESSED      GCONST(CT_PROCESSED)
#define CT_MISC_PLANT     GCONST(CT_MISC_PLANT)
#define CT_MISC_ANIMAL    GCONST(CT_MISC_ANIMAL)
#define CT_PLASTIC        GCONST(CT_PLASTIC)
#define CT_GAS            GCONST(CT_GAS)
#define CT_MAGIC          GCONST(CT_MAGIC)

/* Possible square positions for item. The first four are used for items on walls */
#define LEFT    GCONST(LEFT)
#define DOWN    GCONST(DOWN)
#define UP      GCONST(UP)
#define RIGHT   GCONST(RIGHT)
#define CENTER  GCONST(CENTER) /* item on ground */

#define HOSTILE     (1)
#define UNCARING    (2)
#define FRIEND      (4)
#define PROTECTIVE  (8)

#define MARTIAL_SKILL_CATEGORIES  (3)
#define WEAPON_SKILL_CATEGORIES   (11)

#define UNARMED         GCONST(UNARMED)
#define KICK            GCONST(KICK)
#define BITE            GCONST(BITE)
#define UNCATEGORIZED   GCONST(UNCATEGORIZED)
#define SMALL_SWORDS    GCONST(SMALL_SWORDS)
#define LARGE_SWORDS    GCONST(LARGE_SWORDS)
#define BLUNT_WEAPONS   GCONST(BLUNT_WEAPONS)
#define AXES            GCONST(AXES)
#define POLE_ARMS       GCONST(POLE_ARMS)
#define WHIPS           GCONST(WHIPS)
#define SHIELDS         GCONST(SHIELDS)

#define LOCKED  GCONST(LOCKED)

#define EFFECT_NOTHING                    GCONST(EFFECT_NOTHING)
#define EFFECT_POISON                     GCONST(EFFECT_POISON)
#define EFFECT_DARKNESS                   GCONST(EFFECT_DARKNESS)
#define EFFECT_OMMEL_URINE                GCONST(EFFECT_OMMEL_URINE)
#define EFFECT_PEPSI                      GCONST(EFFECT_PEPSI)
#define EFFECT_KOBOLD_FLESH               GCONST(EFFECT_KOBOLD_FLESH)
#define EFFECT_HEAL                       GCONST(EFFECT_HEAL)
#define EFFECT_LYCANTHROPY                GCONST(EFFECT_LYCANTHROPY)
#define EFFECT_SCHOOL_FOOD                GCONST(EFFECT_SCHOOL_FOOD)
#define EFFECT_ANTIDOTE                   GCONST(EFFECT_ANTIDOTE)
#define EFFECT_CONFUSE                    GCONST(EFFECT_CONFUSE)
#define EFFECT_POLYMORPH                  GCONST(EFFECT_POLYMORPH)
#define EFFECT_ESP                        GCONST(EFFECT_ESP)
#define EFFECT_SKUNK_SMELL                GCONST(EFFECT_SKUNK_SMELL)
#define EFFECT_MAGIC_MUSHROOM             GCONST(EFFECT_MAGIC_MUSHROOM)
#define EFFECT_TRAIN_PERCEPTION           GCONST(EFFECT_TRAIN_PERCEPTION)
#define EFFECT_HOLY_BANANA                GCONST(EFFECT_HOLY_BANANA)
#define EFFECT_EVIL_WONDER_STAFF_VAPOUR   GCONST(EFFECT_EVIL_WONDER_STAFF_VAPOUR)
#define EFFECT_GOOD_WONDER_STAFF_VAPOUR   GCONST(EFFECT_GOOD_WONDER_STAFF_VAPOUR)
#define EFFECT_PEA_SOUP                   GCONST(EFFECT_PEA_SOUP)
#define EFFECT_BLACK_UNICORN_FLESH        GCONST(EFFECT_BLACK_UNICORN_FLESH)
#define EFFECT_GRAY_UNICORN_FLESH         GCONST(EFFECT_GRAY_UNICORN_FLESH)
#define EFFECT_WHITE_UNICORN_FLESH        GCONST(EFFECT_WHITE_UNICORN_FLESH)
#define EFFECT_TELEPORT_CONTROL           GCONST(EFFECT_TELEPORT_CONTROL)
#define EFFECT_MUSHROOM                   GCONST(EFFECT_MUSHROOM)
#define EFFECT_OMMEL_CERUMEN              GCONST(EFFECT_OMMEL_CERUMEN)
#define EFFECT_OMMEL_SWEAT                GCONST(EFFECT_OMMEL_SWEAT)
#define EFFECT_OMMEL_TEARS                GCONST(EFFECT_OMMEL_TEARS)
#define EFFECT_OMMEL_SNOT                 GCONST(EFFECT_OMMEL_SNOT)
#define EFFECT_OMMEL_BONE                 GCONST(EFFECT_OMMEL_BONE)
#define EFFECT_MUSTARD_GAS                GCONST(EFFECT_MUSTARD_GAS)
#define EFFECT_MUSTARD_GAS_LIQUID         GCONST(EFFECT_MUSTARD_GAS_LIQUID)
// new; wtf is EFFECT_PANIC?
#define EFFECT_PANIC                      GCONST(EFFECT_PANIC)
#define EFFECT_TELEPORT                   GCONST(EFFECT_TELEPORT)
#define EFFECT_VAMPIRISM                  GCONST(EFFECT_VAMPIRISM)
#define EFFECT_DETECTING                  GCONST(EFFECT_DETECTING)
#define EFFECT_HOLY_MANGO                 GCONST(EFFECT_HOLY_MANGO)
// new effects from comm. fork
#define EFFECT_TRAIN_WISDOM               GCONST(EFFECT_TRAIN_WISDOM)
#define EFFECT_OMMEL_BLOOD                GCONST(EFFECT_OMMEL_BLOOD)
#define EFFECT_PANACEA                    GCONST(EFFECT_PANACEA)
#define EFFECT_LAUGH                      GCONST(EFFECT_LAUGH)
#define EFFECT_POLYJUICE                  GCONST(EFFECT_POLYJUICE)
#define EFFECT_SICKNESS                   GCONST(EFFECT_SICKNESS)
#define EFFECT_PHASE                      GCONST(EFFECT_PHASE)
#define EFFECT_ACID_GAS                   GCONST(EFFECT_ACID_GAS)
#define EFFECT_REGENERATION               GCONST(EFFECT_REGENERATION)

/* CEM = Consume End Message */

#define CEM_NOTHING               GCONST(CEM_NOTHING)
#define CEM_SCHOOL_FOOD           GCONST(CEM_SCHOOL_FOOD)
#define CEM_BONE                  GCONST(CEM_BONE)
#define CEM_FROG_FLESH            GCONST(CEM_FROG_FLESH)
#define CEM_OMMEL                 GCONST(CEM_OMMEL)
#define CEM_PEPSI                 GCONST(CEM_PEPSI)
#define CEM_KOBOLD_FLESH          GCONST(CEM_KOBOLD_FLESH)
#define CEM_HEALING_LIQUID        GCONST(CEM_HEALING_LIQUID)
#define CEM_ANTIDOTE              GCONST(CEM_ANTIDOTE)
#define CEM_ESP                   GCONST(CEM_ESP)
#define CEM_HOLY_BANANA           GCONST(CEM_HOLY_BANANA)
#define CEM_PEA_SOUP              GCONST(CEM_PEA_SOUP)
#define CEM_BLACK_UNICORN_FLESH   GCONST(CEM_BLACK_UNICORN_FLESH)
#define CEM_GRAY_UNICORN_FLESH    GCONST(CEM_GRAY_UNICORN_FLESH)
#define CEM_WHITE_UNICORN_FLESH   GCONST(CEM_WHITE_UNICORN_FLESH)
#define CEM_OMMEL_BONE            GCONST(CEM_OMMEL_BONE)
// new
#define CEM_LIQUID_HORROR         GCONST(CEM_LIQUID_HORROR)
#define CEM_HOLY_MANGO            GCONST(CEM_HOLY_MANGO)
#define CEM_COCA_COLA             GCONST(CEM_COCA_COLA)
// alien mod
#define CEM_ALIEN_FLESH  (game::FindGlobalValue(CONST_S("CEM_ALIEN_FLESH"), -666))

/* HM = Hit Message */

#define HM_NOTHING          GCONST(HM_NOTHING)
#define HM_SCHOOL_FOOD      GCONST(HM_SCHOOL_FOOD)
#define HM_FROG_FLESH       GCONST(HM_FROG_FLESH)
#define HM_OMMEL            GCONST(HM_OMMEL)
#define HM_PEPSI            GCONST(HM_PEPSI)
#define HM_KOBOLD_FLESH     GCONST(HM_KOBOLD_FLESH)
#define HM_HEALING_LIQUID   GCONST(HM_HEALING_LIQUID)
#define HM_ANTIDOTE         GCONST(HM_ANTIDOTE)
#define HM_CONFUSE          GCONST(HM_CONFUSE)
#define HM_HOLY_BANANA      GCONST(HM_HOLY_BANANA)
#define HM_HOLY_MANGO       GCONST(HM_HOLY_MANGO)
// alien mod
#define HM_ALIEN_FLESH  (game::FindGlobalValue(CONST_S("HM_ALIEN_FLESH"), -666))

#define UNARMED_ATTACK  GCONST(UNARMED_ATTACK)
#define WEAPON_ATTACK   GCONST(WEAPON_ATTACK)
#define KICK_ATTACK     GCONST(KICK_ATTACK)
#define BITE_ATTACK     GCONST(BITE_ATTACK)
#define THROW_ATTACK    GCONST(THROW_ATTACK)

#define USE_ARMS   GCONST(USE_ARMS)
#define USE_LEGS   GCONST(USE_LEGS)
#define USE_HEAD   GCONST(USE_HEAD)

#define ATTRIBUTES  (11)
#define BASE_ATTRIBUTES  (7)

#define ENDURANCE     (0)
#define PERCEPTION    (1)
#define INTELLIGENCE  (2)
#define WISDOM        (3)
#define WILL_POWER    (4)
#define CHARISMA      (5)
#define MANA          (6)

#define ARM_STRENGTH  (7)
#define LEG_STRENGTH  (8)
#define DEXTERITY     (9)
#define AGILITY       (10)

#define F_ENDURANCE     (1<<ENDURANCE)
#define F_PERCEPTION    (1<<PERCEPTION)
#define F_INTELLIGENCE  (1<<INTELLIGENCE)
#define F_WISDOM        (1<<WISDOM)
#define F_WILL_POWER    (1<<WILL_POWER)
#define F_CHARISMA      (1<<CHARISMA)
#define F_MANA          (1<<MANA)

#define F_ARM_STRENGTH  (1<<ARM_STRENGTH)
#define F_LEG_STRENGTH  (1<<LEG_STRENGTH)
#define F_DEXTERITY     (1<<DEXTERITY)
#define F_AGILITY       (1<<AGILITY)

#define NO    (0)
#define YES   (1)
#define REQUIRES_ANSWER   (-1)

#define DIR_ERROR   (0xFF)

#define MAX_EQUIPMENT_SLOTS   (13)

#define HELMET_INDEX          (0)
#define AMULET_INDEX          (1)
#define CLOAK_INDEX           (2)
#define BODY_ARMOR_INDEX      (3)
#define BELT_INDEX            (4)
#define RIGHT_WIELDED_INDEX   (5)
#define LEFT_WIELDED_INDEX    (6)
#define RIGHT_RING_INDEX      (7)
#define LEFT_RING_INDEX       (8)
#define RIGHT_GAUNTLET_INDEX  (9)
#define LEFT_GAUNTLET_INDEX   (10)
#define RIGHT_BOOT_INDEX      (11)
#define LEFT_BOOT_INDEX       (12)

#define WORLD_MAP   (255)

#define DEFAULT_TEAM  (0xFF)

/* Hard-coded teams */
#define PLAYER_TEAM               GCONST(PLAYER_TEAM)
#define MONSTER_TEAM              GCONST(MONSTER_TEAM)
#define ATTNAM_TEAM               GCONST(ATTNAM_TEAM)
#define SUMO_TEAM                 GCONST(SUMO_TEAM)
#define VALPURUS_ANGEL_TEAM       GCONST(VALPURUS_ANGEL_TEAM)
#define GC_SHOPKEEPER_TEAM        GCONST(GC_SHOPKEEPER_TEAM)
#define IVAN_TEAM                 GCONST(IVAN_TEAM)
#define NEW_ATTNAM_TEAM           GCONST(NEW_ATTNAM_TEAM)
#define COLONIST_TEAM             GCONST(COLONIST_TEAM)
#define TOURIST_GUIDE_TEAM        GCONST(TOURIST_GUIDE_TEAM)
#define TOURIST_TEAM              GCONST(TOURIST_TEAM)
#define BETRAYED_TEAM             GCONST(BETRAYED_TEAM)
#define MONDEDR_TEAM              GCONST(MONDEDR_TEAM)
#define KHARAZ_ARAD_TEAM          GCONST(KHARAZ_ARAD_TEAM)
#define FORESTMAN_TEAM            GCONST(FORESTMAN_TEAM)
#define SOLICITUS_TEAM            GCONST(SOLICITUS_TEAM)
#define MORBE_TEAM                GCONST(MORBE_TEAM)
#define XINROCH_TOMB_ENTRY_TEAM   GCONST(XINROCH_TOMB_ENTRY_TEAM)
#define XINROCH_TOMB_NECRO_TEAM   GCONST(XINROCH_TOMB_NECRO_TEAM)
#define XINROCH_TOMB_KAMIKAZE_DWARF_TEAM   GCONST(XINROCH_TOMB_KAMIKAZE_DWARF_TEAM)
#define PRISONER_TEAM             GCONST(PRISONER_TEAM)
#define REBEL_TEAM                GCONST(REBEL_TEAM)
#define ASLONA_TEAM               GCONST(ASLONA_TEAM)
#define INDIFFERENT_TEAM          GCONST(INDIFFERENT_TEAM)
#define SHOP_PERPETRATOR_TEAM     GCONST(SHOP_PERPETRATOR_TEAM)
//#define NO_TEAM 0xFFFF
#define NO_TEAM   GCONST(NO_TEAM)

#define LOAD              (1)
#define NO_PIC_UPDATE     (2)
#define NO_EQUIPMENT_PIC_UPDATE   (NO_PIC_UPDATE<<1)
#define NO_MATERIALS      (8)
#define NO_EQUIPMENT      (16)
#define NO_SIGNALS        (32)
#define NO_SEVERED_LIMBS  (64)

#define NOT_WALKABLE      GCONST(NOT_WALKABLE)
#define HAS_CHARACTER     GCONST(HAS_CHARACTER)
#define IN_ROOM           GCONST(IN_ROOM)
#define NOT_IN_ROOM       GCONST(NOT_IN_ROOM)
#define ATTACHABLE        GCONST(ATTACHABLE) /* overrides IN_ROOM */
#define HAS_NO_OTERRAIN   GCONST(HAS_NO_OTERRAIN)

#define DEFAULT_ATTACHED_AREA   (0xFE)
#define DEFAULT_ATTACHED_ENTRY  (0xFE)
#define NO_ENTRY  (0)

#define RANDOM                    GCONST(RANDOM)
#define NEW_ATTNAM                GCONST(NEW_ATTNAM)
#define UNDER_WATER_TUNNEL        GCONST(UNDER_WATER_TUNNEL)
#define UNDER_WATER_TUNNEL_EXIT   GCONST(UNDER_WATER_TUNNEL_EXIT)
#define ATTNAM                    GCONST(ATTNAM)
#define ELPURI_CAVE               GCONST(ELPURI_CAVE)
#define MONDEDR                   GCONST(MONDEDR)
#define MUNTUO                    GCONST(MUNTUO)
#define DARK_FOREST               GCONST(DARK_FOREST)
#define XINROCH_TOMB              GCONST(XINROCH_TOMB)
#define DRAGON_TOWER              GCONST(DRAGON_TOWER)
#define KHARAZ_ARAD_SHOP          GCONST(KHARAZ_ARAD_SHOP)
#define FUNGAL_CAVE               GMODCONST(FUNGAL_CAVE)
#define PYRAMID                   GMODCONST(PYRAMID)
#define BLACK_MARKET              GMODCONST(BLACK_MARKET)
#define REBEL_CAMP                GMODCONST(REBEL_CAMP)
#define ASLONA_CASTLE             GMODCONST(ASLONA_CASTLE)
#define ALIEN_VESSEL              GMODCONST(ALIEN_VESSEL)

#define attnam                (game::attnamPOI())
#define aslona                (game::aslonaPOI())
#define darkforest            (game::darkforestPOI())
#define elpuricave            (game::elpuricavePOI())
#define mondedr               (game::mondedrPOI())
#define muntuo                (game::muntuoPOI())
#define newattnam             (game::newattnamPOI())
#define underwatertunnel      (game::underwatertunnelPOI())
#define underwatertunnelexit  (game::underwatertunnelexitPOI())
#define xinrochtomb           (game::xinrochtombPOI())


//#define VESANA_LEVEL  (game::GetGlobalConst("VESANA_LEVEL"))
//#define CRYSTAL_LEVEL  (game::GetGlobalConst("CRYSTAL_LEVEL"))
//#define SPIDER_LEVEL  (game::GetGlobalConst("SPIDER_LEVEL"))
//#define ENNER_BEAST_LEVEL  (game::GetGlobalConst("ENNER_BEAST_LEVEL"))
#define ZOMBIE_LEVEL  GCONST(ZOMBIE_LEVEL)
//#define IVAN_LEVEL  (game::GetGlobalConst("IVAN_LEVEL"))
#define DARK_LEVEL    GCONST(DARK_LEVEL)
#define OREE_LAIR     GCONST(OREE_LAIR)
#define KING_LEVEL    GCONST(KING_LEVEL)

//#define DUAL_ENNER_BEAST_LEVEL  (game::GetGlobalConst("DUAL_ENNER_BEAST_LEVEL"))
//#define NECRO_CHAMBER_LEVEL  (game::GetGlobalConst("NECRO_CHAMBER_LEVEL"))

/* stack::DrawContents flags */

#define NO_SELECT                   (1) /* only show items */
#define NO_MULTI_SELECT             (2) /* select only one item */
#define NO_SPECIAL_INFO             (4) /* show only name and amount */
#define REMEMBER_SELECTED           (8) /* if DrawContents will be called multiple times, remember the selected item */
#define NONE_AS_CHOICE             (16) /* "none" is a choice, for instance when wielding */
#define SELECT_PAIR                (32) /* if NO_MULTI_SELECT is on, selects a pair if appropriate */
#define SKIP_FIRST_IF_NO_OLD       (64) /* skip first list item if `hiitem` in `stack::DrawContents()` is non-empty */
#define SELECT_MOST_RECENT        (128) /* select most recent picked item in `stack::DrawContents()` (with timeout) */
#define SELECT_ZEROPICK_FIRST     (256) /* select first non-picked item in `stack::DrawContents()` */
#define DONT_SELECT_CONTAINERS    (512)
#define STACK_ALLOW_TAB           (1024)
#define STACK_ALLOW_NAMING        (2048)
//
#define SS_SHOW_SELL_PRICE        (4096)
// experimental
#define SS_STACK_TOP              (8192)
#define SS_STACK_BOTTOM           (16384)
#define SS_FIRST_ITEM             (32768)
#define SS_LAST_ITEM              (65536)

#define RECTANGLE       GCONST(RECTANGLE)
#define ROUND_CORNERS   GCONST(ROUND_CORNERS)
#define MAZE_ROOM       GCONST(MAZE_ROOM)

/* Gods, 0 == none */

//#define GODS 15
#define GODS        GCONST(LAST_REAL_GOD)

#define VALPURUS    GCONST(VALPURUS)
#define LEGIFER     GCONST(LEGIFER)
#define ATAVUS      GCONST(ATAVUS)
#define DULCIS      GCONST(DULCIS)
#define SEGES       GCONST(SEGES)
#define SOPHOS      GCONST(SOPHOS)
#define TERRA       GCONST(TERRA)
#define SILVA       GCONST(SILVA)
#define LORICATUS   GCONST(LORICATUS)
#define MELLIS      GCONST(MELLIS)
#define CLEPTIA     GCONST(CLEPTIA)
#define NEFAS       GCONST(NEFAS)
#define SCABIES     GCONST(SCABIES)
#define INFUSCOR    GCONST(INFUSCOR)
#define CRUENTUS    GCONST(CRUENTUS)
#define MORTIFER    GCONST(MORTIFER)
#define ATHEIST     GCONST(ATHEIST)
#define SOLICITU    GCONST(SOLICITU)

#define MAX_PRICE   (2147483647)

#define PERMANENT   (0xFFFF)

#define MISSED    (0)
#define HIT       (1)
#define CATCHED   (2)

#define BEAM_EFFECTS  (17)

#define BEAM_POLYMORPH      GCONST(BEAM_POLYMORPH)
#define BEAM_STRIKE         GCONST(BEAM_STRIKE)
#define BEAM_FIRE_BALL      GCONST(BEAM_FIRE_BALL)
#define BEAM_TELEPORT       GCONST(BEAM_TELEPORT)
#define BEAM_HASTE          GCONST(BEAM_HASTE)
#define BEAM_SLOW           GCONST(BEAM_SLOW)
#define BEAM_RESURRECT      GCONST(BEAM_RESURRECT)
#define BEAM_INVISIBILITY   GCONST(BEAM_INVISIBILITY)
#define BEAM_DUPLICATE      GCONST(BEAM_DUPLICATE)
#define BEAM_LIGHTNING      GCONST(BEAM_LIGHTNING)
#define BEAM_DOOR_CREATION  GCONST(BEAM_DOOR_CREATION)
#define BEAM_ACID_RAIN      GCONST(BEAM_ACID_RAIN)
#define BEAM_NECROMANCY     GCONST(BEAM_NECROMANCY)

// WARNING! sync with "define.def"
#define BEAM_STYLES   (3)

#define PARTICLE_BEAM   GCONST(PARTICLE_BEAM)
#define LIGHTNING_BEAM  GCONST(LIGHTNING_BEAM)
#define SHIELD_BEAM     GCONST(SHIELD_BEAM)


#define RANDOM_COLOR  (0x10000)

/* Entry indices, not actual config defines */

//#define STAIRS_UP 100
//#define STAIRS_DOWN 200
//#define WAYPOINT_DEEPER 1100
//#define WAYPOINT_SHALLOWER 1200
//#define FOUNTAIN 0xFFFF  // in confdef.h

//#define NO_LIMIT  (0xFFFF)

#define ALL_ITEMS   (0xFFFF)

/* StateData flags */

#define NO_FLAGS            (0)
#define SECRET              (1)
#define DUR_TEMPORARY       (2)
#define DUR_PERMANENT       (4)
#define DUR_FLAGS           (DUR_TEMPORARY|DUR_PERMANENT)
#define SRC_FOUNTAIN        (8)
#define SRC_MUSHROOM        (16)
#define SRC_MAGIC_MUSHROOM  (32)
#define SRC_CONFUSE_READ    (64)
#define SRC_EVIL            (128)
#define SRC_GOOD            (256)
#define SRC_FLAGS (SRC_FOUNTAIN|SRC_MUSHROOM|SRC_MAGIC_MUSHROOM|SRC_CONFUSE_READ|SRC_EVIL|SRC_GOOD)
#define RANDOMIZABLE        (DUR_FLAGS|SRC_FLAGS)

#define MAP_HIDDEN                  (0)
#define SHOW_MAP_IN_TRUE_LIGHT      (1)
#define SHOW_MAP_IN_UNIFORM_LIGHT   (2)

#define DIM_LUMINANCE   (0x6E6E6E)

#define SUPER   (64)
#define BROKEN  (128)
#define WINDOW  (1024)

/* item flags */

#define CANNIBALIZED          (4)
#define SQUARE_POSITION_BITS  (16|32|64)
#define SQUARE_POSITION_SHIFT (4)

/* bodypart flags */

#define UNIQUE      (128)
#define BADLY_HURT  (256)
#define STUCK       (512)
#define BODYPART_SPARKLE_SHIFT  (9)

#define NO_BROKEN             (1)
#define IGNORE_BROKEN_PRICE   (2)

#define MAX_SQUARES_UNDER       (16)
#define MAX_NEIGHBOUR_SQUARES   (20)

#define N_LOCK_ID   GCONST(N_LOCK_ID)
#define S_LOCK_ID   GCONST(S_LOCK_ID)
#define LOCK_DELTA  GCONST(LOCK_DELTA)

#define LOCK_BITS   (0xFC00)

#define BROKEN_LOCK   GCONST(BROKEN_LOCK)

/* Normal lock types, which can be randomized */

#define ROUND_LOCK        GCONST(ROUND_LOCK)
#define SQUARE_LOCK       GCONST(SQUARE_LOCK)
#define TRIANGULAR_LOCK   GCONST(TRIANGULAR_LOCK)

/* Special lock types, which must be generated in the script */

#define HEXAGONAL_LOCK      GCONST(HEXAGONAL_LOCK)
#define OCTAGONAL_LOCK      GCONST(OCTAGONAL_LOCK)
#define HEART_SHAPED_LOCK   GCONST(HEART_SHAPED_LOCK)
#define PENTAGONAL_LOCK     GCONST(PENTAGONAL_LOCK)

#define DESERT            GCONST(DESERT)
#define JUNGLE            GCONST(JUNGLE)
#define STEPPE            GCONST(STEPPE)
#define LEAFY_FOREST      GCONST(LEAFY_FOREST)
#define EVERGREEN_FOREST  GCONST(EVERGREEN_FOREST)
#define TUNDRA            GCONST(TUNDRA)
#define GLACIER           GCONST(GLACIER)
#define OCEAN             GCONST(OCEAN)

#define NO_MOVE   GCONST(NO_MOVE)
#define WALK      GCONST(WALK)
#define SWIM      GCONST(SWIM)
#define FLY       GCONST(FLY)
#define ETHEREAL  GCONST(ETHEREAL)
#define ANY_MOVE  GCONST(ANY_MOVE)

/* unused
#define KEY_UP_INDEX      (1)
#define KEY_LEFT_INDEX    (3)
#define KEY_RIGHT_INDEX   (4)
#define KEY_DOWN_INDEX    (6)
*/

#define NO_ACTION   (0)
#define SUCCESS     (1)
#define BLOCKED     (2)

#define STACK_SLOT      (1)
#define CHARACTER_SLOT  (2)
#define GEAR_SLOT       (3)

#define NOT_RUSTED        GCONST(NOT_RUSTED)
#define SLIGHTLY_RUSTED   GCONST(SLIGHTLY_RUSTED)
#define RUSTED            GCONST(RUSTED)
#define VERY_RUSTED       GCONST(VERY_RUSTED)

#define HUMAN_BODY_ARMOR_PIXELS   (68)

#define ARMOR_OUTLINE_PRIORITY  ((7<<4)+7)
#define CLOAK_PRIORITY          ((8<<4)+7)

#define BODY_ARMOR_PARTS  (6)

#define SUMO_ROOM_POS   v2(25, 35)
#define SUMO_ARENA_POS  v2(19, 12)

#define MAX_RAIN_DROPS  (32)

#define WON           (0)
#define LOST          (1)
#define DISQUALIFIED  (2)

#define EMITTER_IDENTIFIER_BITS     (0xFFFF)
#define EMITTER_SQUARE_PART_BITS    (0xF000000)
#define EMITTER_SHADOW_BITS         (0xF0000000)
#define EMITTER_SQUARE_PART_SHIFT   (24)
#define EMITTER_SHADOW_SHIFT        (28)

#define RE_SUN_EMITATED   (0x200000)
#define ID_X_COORDINATE   (0x400000)
#define ID_BEGIN          (0x800000)

#define FORCE_ADD             (0x400000)
#define SECONDARY_SUN_LIGHT   (0x800000)

/* square & lsquare flags */

#define ALLOW_EMITATION_CONTINUE  (1)
#define FREEZED                   (2) /* also a stack flag */
#define INSIDE                    (4)
#define NEW_DRAW_REQUEST          (8)
#define STRONG_BIT                (16)
#define STRONG_NEW_DRAW_REQUEST   (NEW_DRAW_REQUEST|STRONG_BIT)
#define DESCRIPTION_CHANGE        (32)
#define MEMORIZED_UPDATE_REQUEST  (128)
#define IN_SQUARE_STACK           (256)
#define CHECK_SUN_LIGHT_NEEDED    (512)
#define IS_TRANSPARENT            (1024)
#define PERFECTLY_QUADRI_HANDLED  (2048)

#define CONFIG_TABLE_SIZE   (256)

#define SPARKLE_POS_X_ERROR   (128)

/*
#define SKIN_COLOR 1
#define CAP_COLOR 2
#define HAIR_COLOR 4
#define EYE_COLOR 8
#define TORSO_MAIN_COLOR 16
#define BELT_COLOR 32
#define BOOT_COLOR 64
#define TORSO_SPECIAL_COLOR 128
#define ARM_MAIN_COLOR 256
#define GAUNTLET_COLOR 512
#define ARM_SPECIAL_COLOR 1024
#define LEG_MAIN_COLOR 2048
#define LEG_SPECIAL_COLOR 4096
#define CLOTH_COLOR (CAP_COLOR\
        |TORSO_MAIN_COLOR\
        |ARM_MAIN_COLOR\
        |GAUNTLET_COLOR\
        |LEG_MAIN_COLOR)
*/


#define SKIN_COLOR            GCONST(SKIN_COLOR)
#define CAP_COLOR             GCONST(CAP_COLOR)
#define HAIR_COLOR            GCONST(HAIR_COLOR)
#define EYE_COLOR             GCONST(EYE_COLOR)
#define TORSO_MAIN_COLOR      GCONST(TORSO_MAIN_COLOR)
#define BELT_COLOR            GCONST(BELT_COLOR)
#define BOOT_COLOR            GCONST(BOOT_COLOR)
#define TORSO_SPECIAL_COLOR   GCONST(TORSO_SPECIAL_COLOR)
#define ARM_MAIN_COLOR        GCONST(ARM_MAIN_COLOR)
#define GAUNTLET_COLOR        GCONST(GAUNTLET_COLOR)
#define ARM_SPECIAL_COLOR     GCONST(ARM_SPECIAL_COLOR)
#define LEG_MAIN_COLOR        GCONST(LEG_MAIN_COLOR)
#define LEG_SPECIAL_COLOR     GCONST(LEG_SPECIAL_COLOR)
#define CLOTH_COLOR           GCONST(CLOTH_COLOR)

/* contentscript<character> flags */
#define IS_LEADER   (1)
#define IS_MASTER   (2)

/* stack flags */

/* If set, all items are always considered visible, so CanBeSeenBy calls
   become unneeded */

#define HIDDEN  (1)

/* All costly updates (like emitation's) are avoided if this is set.
   Allows much faster removing and adding items, but make sure the stack is
   returned to the original state (excluding item order) before switching
   this off. Note: also an lsquare flag */

#define FREEZED   (2)

/* End stack Flags */

#define SUN_BEAM_DIRECTIONS   (48)

/* Square part flags */

#define SP_TOP_LEFT       (1)
#define SP_TOP_RIGHT      (2)
#define SP_BOTTOM_LEFT    (4)
#define SP_BOTTOM_RIGHT   (8)
#define SP_TOP            (SP_TOP_LEFT|SP_TOP_RIGHT)
#define SP_LEFT           (SP_TOP_LEFT|SP_BOTTOM_LEFT)
#define SP_RIGHT          (SP_TOP_RIGHT|SP_BOTTOM_RIGHT)
#define SP_BOTTOM         (SP_BOTTOM_LEFT|SP_BOTTOM_RIGHT)

#define CONDITION_COLORS  (5)

#define NATURAL_MATERIAL_FORM   (0x7FFF)

#define EXP_DIVISOR     (2e-8)
#define EXP_MULTIPLIER  (5e+7)
#define MIN_EXP         (5e+7)
#define MAX_EXP         (5e+10)

#define HAS_BEEN_GENERATED  (1)
#define HAS_BEEN_SEEN       (2)

#define DEPENDS_ON_ATTRIBUTES   (0xFFFF)

/* Tiredness states */

#define FAINTING    (0)
#define EXHAUSTED   (1)
#define UNTIRED     (2)

/* sadly, cannot move to config */
#define DEFAULT_GENERATION_DANGER   (0.05)
#define ANGEL_GENERATION_DANGER     (0.10)

/* Duplication flags */

#define MIRROR_IMAGE          (1)
#define IGNORE_PROHIBITIONS   (2)
#define CHANGE_TEAM           (4)
#define LE_BASE_SHIFT         (3)
#define LE_BASE_RANGE         (0x7FFF)
#define LE_RAND_SHIFT         (18)
#define LE_RAND_RANGE         (0x3FFF)

/* action flags */

#define IN_DND_MODE   (1)
#define TERMINATING   (2)

/* fluid flags */

#define HAS_BODY_ARMOR_PICTURES   (1)
#define FLUID_INSIDE              (2)

#define COMMAND_FLAGS       4
#define ALL_COMMAND_FLAGS   (1|2|4|8)

#define FOLLOW_LEADER                   (1)
#define FLEE_FROM_ENEMIES               (2)
#define DONT_CHANGE_EQUIPMENT           (4)
#define DONT_CONSUME_ANYTHING_VALUABLE  (8)

#define CHAT_MENU_ENTRIES     (5)
#define ALL_MANAGEMENT_FLAGS  (1|2|4|8|16)

#define CHANGE_EQUIPMENT  (1)
#define TAKE_ITEMS        (2)
#define GIVE_ITEMS        (4)
#define ISSUE_COMMANDS    (8)
#define CHAT_IDLY         (16)

#define NO_PARAMETERS   (0xFF)

#define CURSOR_TYPES  (5)

#define DARK_CURSOR     (0)
#define RED_CURSOR      (1)
#define BLUE_CURSOR     (2)
#define YELLOW_CURSOR   (3)
#define GREEN_CURSOR    (4)

#define CURSOR_SHADE    (0x1000)
#define CURSOR_FLASH    (0x2000)
#define CURSOR_TARGET   (0x4000)
#define CURSOR_BIG      (0x8000)
#define CURSOR_FLAGS    (CURSOR_SHADE|CURSOR_BIG|CURSOR_FLASH|CURSOR_TARGET)

#define GRAY_FRACTAL    (0)
#define RED_FRACTAL     (1)
#define GREEN_FRACTAL   (2)
#define BLUE_FRACTAL    (3)
#define YELLOW_FRACTAL  (4)

#define DAMAGE_TYPES  (3)
/* not used in C++ code, only in scripts
#define BLUNT   GCONST(BLUNT)
#define SLASH   GCONST(SLASH)
#define PIERCE  GCONST(PIERCE)
*/

#define SILHOUETTE_TYPES  (2)

#define SILHOUETTE_NORMAL       (0)
#define SILHOUETTE_INTER_LACED  (1)

#define WARNED            (1)
#define HAS_CAUSED_PANIC  (2)

/* MaxHP calculation flags */

#define MAY_CHANGE_HPS    (1)
#define CHECK_USABILITY   (2)

#define ITEM_TRAP   (0x8000)
#define FLUID_TRAP  (0x10000)

/* Death flags */

#define FORCE_MSG               (1)
#define FORCE_DEATH             (2)
#define DISALLOW_CORPSE         (4)
#define DISALLOW_MSG            (8)
#define IGNORE_UNCONSCIOUSNESS  (16)
#define IGNORE_TRAPS            (32)
#define FORBID_REINCARNATION    (64)

/* character flags */

#define C_PLAYER                      (4)
#define C_INITIALIZING                (8)
#define C_POLYMORPHED                 (16)
#define C_IN_NO_MSG_MODE              (32)
#define C_PICTURE_UPDATES_FORBIDDEN   (64)

/*************************/
/* Common DataBase flags */
/*************************/

/* CommonFlags */
#define IS_ABSTRACT                   (1)
#define HAS_SECONDARY_MATERIAL        (2)
#define CREATE_DIVINE_CONFIGURATIONS  (4)
#define CAN_BE_WISHED                 (8)
#define CAN_BE_DESTROYED              (16)
#define IS_VALUABLE                   (32)
#define CAN_BE_MIRRORED               (64)
#define CAN_BE_DETECTED               (128)

/* NameFlags */
#define USE_AN            (1)
#define USE_ADJECTIVE_AN  (2)
#define NO_ARTICLE        (4) /* for instance "Petrus's wive number 4"; this is what `GetArticleMode()` can return */
#define FORCE_THE         (8) /* this is what `GetArticleMode()` can return */
#define SHOW_MATERIAL     (16) /* only works for terrains */

/* Spatial Flags (unused for now) */
#define MF_OBJECT   (256)
#define MF_FLOOR    (512)
#define MF_CEIL     (1024)
#define MF_WALL     (2048)
#define MF_ONFLOOR  (4096)
#define MF_LOWWALL  (8192)
#define MF_TABLE    (16384)
#define MF_WINDOW   (128)
#define MF_LARGE    (64)
#define MF_STAIR    (32)

/****************************/
/* Character DataBase flags */
/****************************/

/* CommonFlags */
/* NameFlags */

/* BodyFlags */
/*
#define HAS_HEAD
#define HAS_EYES
#define HAS_A_LEG
#define SPILLS_BLOOD
#define SWEATS
#define USES_NUTRITION
#define ALWAYS_USE_MATERIAL_ATTRIBUTES
#define IS_ENORMOUS
#define IS_EXTRA_FRAGILE
#define IS_PLANT
#define IS_ROOTED
*/

/* AbilityFlags */
/*
#define CAN_USE_EQUIPMENT
#define CAN_KICK
#define CAN_TALK
#define CAN_READ
#define CAN_OPEN
#define CAN_ZAP
#define CAN_THROW
#define CAN_APPLY
#define CAN_HEAR
*/

/* CopyrightFlags */
/*
#define IS_UNIQUE
#define CAN_BE_GENERATED
#define CAN_BE_NAMED
*/

/* EffectFlags; */
/*
#define BODY_PARTS_DISAPPEAR_WHEN_SEVERED
#define DESTROYS_WALLS
#define BITE_CAPTURES_BODY_PART
*/

/* ImmunityFlags */
/*
#define IMMUNITY_POLYMORPH
#define IMMUNITY_CHARM
#define IMMUNITY_CLONING
#define IMMUNITY_CONFUSE
#define IMMUNITY_LEPROSY
#define IMMUNITY_ITEM_TELEPORT
#define IMMUNITY_STICKINESS
#define IMMUNITY_CHOKING
#define IMMUNITY_UNCONSCIOUSNESS
*/

/* MiscFlags */
//#define CREATE_GOLEM_MATERIAL_CONFIGURATIONS  (2)
//#define IGNORE_DANGER                         (4)
//#define AUTOMATICALLY_SEEN                    (8)
//#define WILL_CARRY_ITEMS                      (16)
//#define IS_EXTRA_COWARD                       (32)

/***********************/
/* Item DataBase flags */
/***********************/

/* CommonFlags */
/* NameFlags */
/* AttributeAffectFlags */

/* GenerationFlags*/
//#define CREATE_LOCK_CONFIGURATIONS 2
//#define CAN_BE_AUTO_INITIALIZED 4 /* used only in WMode */
//#define CAN_BE_GENERATED_IN_CONTAINER 8
//#define CAN_BE_SPAWNED_BY_POLYMORPH 16

/* InteractionFlags */
/*
#define MATERIAL_CAN_BE_CHANGED
#define CAN_BE_POLYMORPHED
#define CAN_BE_CLONED
#define CAN_BE_ENCHANTED
#define CAN_BE_BROKEN
#define AFFECTS_CARRYING_CAPACITY
*/

/* CategoryFlags */
/*
#define IS_QUEST_ITEM
#define CAN_BE_USED_BY_SMITH
#define IS_KAMIKAZE_WEAPON
#define IS_TWO_HANDED
#define IS_GOOD_WITH_PLANTS
*/

/* MiscFlags */
/*
#define HANDLE_IN_PAIRS
#define PRICE_IS_PROPORTIONAL_TO_ENCHANTMENT
#define FLEXIBILITY_IS_ESSENTIAL
#define HAS_NORMAL_PICTURE_DIRECTION
#define CAN_BE_PILED
#define CAN_BE_PICKED_UP
#define ALLOW_EQUIP
*/

/**************************/
/* Terrain DataBase flags */
/**************************/

/* CommonFlags */
/* NameFlags */

/* OLTerrainFlags */
//#define CREATE_LOCK_CONFIGURATIONS 2
//#define CREATE_WINDOW_CONFIGURATIONS 4
/*
#define IS_UP_LINK
#define IS_WALL
#define USE_BORDER_TILES
#define IS_ALWAYS_TRANSPARENT
#define SHOW_THINGS_UNDER
#define IS_SAFE_TO_CREATE_DOOR
*/

/***************************/
/* Material DataBase flags */
/***************************/

/* CommonFlags */
/* NameFlags (only USE_AN) */

/* CategoryFlags */
#define IS_METAL            (1)
#define IS_BLOOD            (2)
#define CAN_BE_TAILORED     (4)
#define IS_SPARKLING        (8)
#define IS_SCARY            (16)
#define IS_GOLEM_MATERIAL   (32)
#define IS_BEVERAGE         (64)

/* BodyFlags */
#define IS_ALIVE                  (1)
#define IS_WARM                   (2)
#define CAN_HAVE_PARASITE         (4)
#define USE_MATERIAL_ATTRIBUTES   (8)
#define CAN_REGENERATE            (16)
#define IS_WARM_BLOODED           (32)

/* InteractionFlags */
#define CAN_BURN                    (1)
#define CAN_EXPLODE                 (2)
#define CAN_DISSOLVE                (4)
#define AFFECT_INSIDE               (8)
#define EFFECT_IS_GOOD              (16)
#define IS_AFFECTED_BY_MUSTARD_GAS  (32)
#define RISES_FROM_ASHES            (64)

/*************************/
/* End of DataBase flags */
/*************************/

#define TILE_SIZE (16)
cv2 TILE_V2(TILE_SIZE, TILE_SIZE);

#define SQUARE_INDEX_MASK   (0xFFFF)
#define ALLOW_ANIMATE       (0x10000)
#define ALLOW_ALPHA         (0x20000)

#define TALENTS   (4)

#define TALENT_STRONG           (0)
#define TALENT_FAST_N_ACCURATE  (1)
#define TALENT_HEALTHY          (2)
#define TALENT_CLEVER           (3)

#define BORDER_PARTNER_ANIMATED   (16<<24)

/* room flags */

//FIXME: use value from "define.def"!
#define NO_MONSTER_GENERATION   (1)

//FIXME: use value from "define.def"!
#define NO_TAMING   (-1)

//FIXME: use value from "define.def"!
#define SADIST_HIT  (1)

#define EQUIPMENT_DATAS   (13)

#define SPECIAL_CONFIGURATION_GENERATION_LEVELS   (2)

#endif
