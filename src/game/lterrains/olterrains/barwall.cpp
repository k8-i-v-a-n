#ifdef HEADER_PHASE
OLTERRAIN(barwall, olterrain)
{
public:
  virtual void Break () override;
  virtual truth IsBarWall () const override;
};


#else


void barwall::Break () {
  if (GetConfig() == BROKEN_BARWALL) {
    olterrain::Break();
  } else {
    barwall *Temp = barwall::Spawn(BROKEN_BARWALL, NO_MATERIALS);
    Temp->InitMaterials(GetMainMaterial()->SpawnMore());
    GetLSquareUnder()->ChangeOLTerrainAndUpdateLights(Temp);
  }
}


truth barwall::IsBarWall () const {
  return true;
}


#endif
