#ifdef HEADER_PHASE
OLTERRAIN(olterraincontainer, olterrain)
{
public:
  olterraincontainer ();
  virtual ~olterraincontainer ();

  virtual truth Open (character *) override;
  virtual truth CanBeOpened () const override;
  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual void SetItemsInside (const fearray<itemcontentscript> &, int) override;
  virtual void Break () override;
  virtual truth AllowContentEmitation () const override;
  virtual void PreProcessForBone () override;
  virtual void PostProcessForBone () override;
  virtual void FinalProcessForBone () override;

  virtual stack *GetContained () const;

protected:
  stack *Contained;
};


#else


truth olterraincontainer::CanBeOpened () const { return true; }
stack *olterraincontainer::GetContained () const { return Contained; }
truth olterraincontainer::AllowContentEmitation () const { return false; }


//==========================================================================
//
//  olterraincontainer::olterraincontainer
//
//==========================================================================
olterraincontainer::olterraincontainer () {
  Contained = new stack(0, this, HIDDEN);
}


//==========================================================================
//
//  olterraincontainer::~olterraincontainer
//
//==========================================================================
olterraincontainer::~olterraincontainer () {
  delete Contained;
}


//==========================================================================
//
//  olterraincontainer::Save
//
//==========================================================================
void olterraincontainer::Save (outputfile &SaveFile) const {
  olterrain::Save(SaveFile);
  Contained->Save(SaveFile);
}


//==========================================================================
//
//  olterraincontainer::Load
//
//==========================================================================
void olterraincontainer::Load (inputfile &SaveFile) {
  olterrain::Load(SaveFile);
  Contained->Load(SaveFile);
}


//==========================================================================
//
//  olterraincontainer::Open
//
//==========================================================================
truth olterraincontainer::Open (character *Opener) {
  if (!Opener->IsPlayer()) return false;
  truth Success = false;
  /*
  switch (game::KeyQuestion(CONST_S("Do you want to \1Gt\2ake something from or "
                                    "\1Gp\2ut something in this container?"),
                                    REQUIRES_ANSWER,
                                    'T', 'P', KEY_ESC, 0))
  {
    case 'T': Success = GetContained()->TakeSomethingFrom(Opener, GetName(DEFINITE)); break;
    case 'P': Success = GetContained()->PutSomethingIn(Opener, GetName(DEFINITE), GetStorageVolume(), 0); break;
    default: return false;
  }
  */
  if (!GetContained()->GetItems()) {
    ADD_MESSAGE("There is nothing in %s.", GetName(DEFINITE).CStr());
  }
  bool doTake = true;
  int selTake = -1, selPut = -1;
  for (;;) {
    int res;
    /*
    if (!GetContained()->GetItems()) {
      if (!Opener->GetStack()->GetItems()) {
        ADD_MESSAGE("You have nothing to take and nothing put in %s.", GetName(DEFINITE).CStr());
        break;
      }
      doTake = false;
    } else if (!Opener->GetStack()->GetItems()) {
      doTake = true;
    }
    */
    if (doTake) {
      res = GetContained()->TakeSomethingFrom(Opener, GetName(DEFINITE), Success, selTake);
    } else {
      res = GetContained()->PutSomethingIn(Opener, GetName(DEFINITE), GetStorageVolume(),
                                           0, Success, selPut);
    }
    if (res < 0) break;
    doTake = !doTake;
  }
  if (Success) {
    Opener->DexterityAction(Opener->OpenMultiplier() * 5);
  }
  return Success;
}


//==========================================================================
//
//  olterraincontainer::SetItemsInside
//
//==========================================================================
void olterraincontainer::SetItemsInside (const fearray<itemcontentscript> &ItemArray,
                                         int SpecialFlags)
{
  GetContained()->Clean();
  for (uInt c1 = 0; c1 < ItemArray.Size; ++c1) {
    if (ItemArray[c1].IsValid()) {
      const interval *TimesPtr = ItemArray[c1].GetTimes();
      int Times = (TimesPtr ? TimesPtr->Randomize() : 1);
      for (int c2 = 0; c2 < Times; ++c2) {
        item *Item = ItemArray[c1].Instantiate(SpecialFlags);
        if (Item) {
          Contained->AddItem(Item);
          Item->SpecialGenerationHandler();
        }
      }
    }
  }
}


//==========================================================================
//
//  olterraincontainer::Break
//
//==========================================================================
void olterraincontainer::Break () {
  GetContained()->MoveItemsTo(GetLSquareUnder()->GetStack());
  olterrain::Break();
}


//==========================================================================
//
//  olterraincontainer::PreProcessForBone
//
//==========================================================================
void olterraincontainer::PreProcessForBone () {
  olterrain::PreProcessForBone();
  Contained->PreProcessForBone();
}


//==========================================================================
//
//  olterraincontainer::PostProcessForBone
//
//==========================================================================
void olterraincontainer::PostProcessForBone () {
  olterrain::PostProcessForBone();
  Contained->PostProcessForBone();
}


//==========================================================================
//
//  olterraincontainer::FinalProcessForBone
//
//==========================================================================
void olterraincontainer::FinalProcessForBone () {
  olterrain::FinalProcessForBone();
  Contained->FinalProcessForBone();
}


#endif
