#ifdef HEADER_PHASE
OLTERRAIN(coffin, olterraincontainer)
{
public:
  coffin ();
  virtual ~coffin ();

  virtual truth Open (character *) override;
  virtual truth CanBeOpened () const override;
  virtual stack *GetContained () const override;
  virtual void Load (inputfile &) override;
  virtual void Save (outputfile &) const override;
  virtual void SetItemsInside (const fearray<itemcontentscript> &, int) override;
  virtual void Break () override;
  virtual truth AllowContentEmitation () const override;
  virtual void PreProcessForBone () override;
  virtual void PostProcessForBone () override;
  virtual void FinalProcessForBone () override;

protected:
  virtual void GenerateGhost (lsquare *);

protected:
  stack *Contained;
};


#else


class ghost;


truth coffin::CanBeOpened () const { return true; }
stack *coffin::GetContained () const { return Contained; }
truth coffin::AllowContentEmitation () const { return false; }


coffin::coffin () {
  Contained = new stack(0, this, HIDDEN);
}


coffin::~coffin () {
  delete Contained;
}


void coffin::Save (outputfile &SaveFile) const {
  olterrain::Save(SaveFile);
  Contained->Save(SaveFile);
}


void coffin::Load (inputfile &SaveFile) {
  olterrain::Load(SaveFile);
  Contained->Load(SaveFile);
}


void coffin::PreProcessForBone () {
  olterrain::PreProcessForBone();
  Contained->PreProcessForBone();
}


void coffin::PostProcessForBone () {
  olterrain::PostProcessForBone();
  Contained->PostProcessForBone();
}


void coffin::FinalProcessForBone () {
  olterrain::FinalProcessForBone();
  Contained->FinalProcessForBone();
}


truth coffin::Open (character *Opener) {
  if (!Opener->IsPlayer()) return false;
  if (!game::TruthQuestion(CONST_S("Disturbing the dead might not be wise... Continue?"))) return false;
  truth Success = olterraincontainer::Open(Opener);
  if (Success) {
    game::DoEvilDeed(25);
    for (int c = 0; c < RAND_N(10); ++c) {
      v2 Pos = GetLevel()->GetRandomSquare();
      if (Pos == ERROR_V2) break;
      GenerateGhost(GetLevel()->GetLSquare(Pos));
    }
  }
  return Success;
}


void coffin::Break () {
  for (int c = 0; c < 9; ++c) {
    lsquare *Neighbour = GetLSquareUnder()->GetNeighbourLSquare(c);
    if (!RAND_4 && Neighbour && Neighbour->IsFlyable()) GenerateGhost(Neighbour);
  }
  olterraincontainer::Break();
}


void coffin::GenerateGhost (lsquare *Square) {
  if ((!Square->GetRoomIndex() || !Square->GetRoom()->DontGenerateMonsters())) {
    v2 Pos = Square->GetPos();
    character *Char = ghost::Spawn();
    Char->SetTeam(game::GetTeam(MONSTER_TEAM));
    //k8:Char->PutTo(Pos);
    if (Char->PutToOrNear(Pos, true)) {
      Char->SignalGeneration();
      if (Char->CanBeSeenByPlayer()) ADD_MESSAGE("%s appears.", Char->CHAR_NAME(DEFINITE));
    } else {
      delete Char;
    }
  }
}


void coffin::SetItemsInside (const fearray<itemcontentscript> &ItemArray, int SpecialFlags) {
  GetContained()->Clean();
  for (size_t c1 = 0; c1 < ItemArray.Size; ++c1) {
    if (ItemArray[c1].IsValid()) {
      const interval *TimesPtr = ItemArray[c1].GetTimes();
      int Times = TimesPtr ? TimesPtr->Randomize() : 1;
      for (int c2 = 0; c2 < Times; ++c2) {
        item *Item = ItemArray[c1].Instantiate(SpecialFlags);
        if (Item) {
          Contained->AddItem(Item);
          Item->SpecialGenerationHandler();
        }
      }
    }
  }
}


#endif
