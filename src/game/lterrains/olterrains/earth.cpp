#ifdef HEADER_PHASE
OLTERRAIN(earth, olterrain)
{
public:
  earth ();

  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;

protected:
  virtual void PostConstruct () override;
  virtual v2 GetBitmapPos (int) const override;

protected:
  int PictureIndex;
};


#else


earth::earth () : PictureIndex(0) {}


void earth::PostConstruct () {
  PictureIndex = RAND_4;
}


void earth::Save (outputfile &SaveFile) const {
  olterrain::Save(SaveFile);
  SaveFile << PictureIndex;
}


void earth::Load (inputfile& SaveFile) {
  olterrain::Load(SaveFile);
  SaveFile >> PictureIndex;
}


v2 earth::GetBitmapPos (int I) const {
  return olterrain::GetBitmapPos(I) + v2(PictureIndex * 48, 0);
}


#endif
