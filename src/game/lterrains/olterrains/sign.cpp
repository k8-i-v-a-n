#ifdef HEADER_PHASE
OLTERRAIN(sign, olterrain)
{
public:
  sign ();

  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void SetText(cfestring &What) override;
  virtual festring GetText () const override;
  virtual void AddPostFix (festring &, int) const override;
  virtual void StepOn (character *) override;

protected:
  festring Text;
};


#else


sign::sign () : Text("") {}


void sign::SetText (cfestring &What) { Text = What; }
festring sign::GetText () const { return Text; }


void sign::AddPostFix (festring &String, int) const {
  String << " with text \"" << Text << '\"';
}


void sign::StepOn (character *Stepper) {
  if (Stepper->IsPlayer()) ADD_MESSAGE("There's a sign here saying: \"%s\"", Text.CStr());
}


void sign::Save (outputfile &SaveFile) const {
  olterrain::Save(SaveFile);
  SaveFile << Text;
}


void sign::Load (inputfile &SaveFile) {
  olterrain::Load(SaveFile);
  SaveFile >> Text;
}


#endif
