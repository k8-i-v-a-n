#ifdef HEADER_PHASE
MATERIAL(powder, liquid)
{
 public:
  powder ();

  virtual truth IsPowder () const override;
  virtual truth IsExplosive () const override;
  virtual void AddWetness (sLong What) override;
  virtual void Be (feuLong) override;
  virtual truth HasBe () const override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;

protected:
  sLong Wetness;
};


#else


truth powder::IsExplosive() const { return !Wetness && material::IsExplosive(); }
truth powder::IsPowder () const { return true; }
void powder::AddWetness (sLong What) { Wetness += What; }
truth powder::HasBe () const { return true; }


powder::powder () : Wetness(0) {}


void powder::Be (feuLong) {
  if (Wetness > 0) --Wetness;
}



void powder::Save (outputfile &SaveFile) const {
  material::Save(SaveFile);
  SaveFile << Wetness;
}


void powder::Load (inputfile &SaveFile) {
  material::Load(SaveFile);
  SaveFile >> Wetness;
}


#endif
