#ifdef HEADER_PHASE
MATERIAL(gas, material)
{
  virtual truth IsGaseous () const override;
};

#else

truth gas::IsGaseous () const { return true; }

#endif
