#ifdef HEADER_PHASE
MATERIAL(flesh, organic)
{
public:
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual col16 GetSkinColor () const override;
  virtual void SetSkinColor (int What) override;
  virtual truth SkinColorIsSparkling () const override;
  virtual void SetSkinColorIsSparkling (truth What) override;
  virtual truth IsFlesh () const override;
  virtual void SetIsInfectedByLeprosy (truth What) override;
  virtual truth IsInfectedByLeprosy () const override;

protected:
  virtual void PostConstruct () override;

protected:
  col16 SkinColor;
  truth SkinColorSparkling;
  truth InfectedByLeprosy;
};


#else


col16 flesh::GetSkinColor () const { return SkinColor; }
void flesh::SetSkinColor (int What) { SkinColor = What; }
truth flesh::SkinColorIsSparkling () const { return SkinColorSparkling; }
void flesh::SetSkinColorIsSparkling (truth What) { SkinColorSparkling = What; }
truth flesh::IsFlesh () const { return true; }
void flesh::SetIsInfectedByLeprosy (truth What) { InfectedByLeprosy = What; }
truth flesh::IsInfectedByLeprosy () const { return InfectedByLeprosy; }


void flesh::PostConstruct () {
  organic::PostConstruct();
  SkinColorSparkling = InfectedByLeprosy = false;
  SkinColor = GetColor();
}


void flesh::Save (outputfile &SaveFile) const {
  organic::Save(SaveFile);
  SaveFile << SkinColor << SkinColorSparkling << InfectedByLeprosy;
}


void flesh::Load (inputfile &SaveFile) {
  organic::Load(SaveFile);
  SaveFile >> SkinColor >> SkinColorSparkling >> InfectedByLeprosy;
}


#endif
