/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include <cstdarg>
#include <cctype>

#ifndef DISABLE_SOUND
# include "regex.h"
# include <SDL2/SDL_mixer.h>
#endif

#include "vwfileio.h"
#include "message.h"
#include "festring.h"
#include "felist.h"
#include "rawbit.h"
#include "hscore.h"
#include "game.h"
#include "graphics.h"
#include "fesave.h"
#include "feparse.h"
#include "bitmap.h"
#include "igraph.h"
#include "iconf.h"

#include "sq3support.h"

#ifdef SHITDOZE
// fuck you, shitdoze!
# ifdef PlaySound
#  undef PlaySound
# endif
#endif


felist msgsystem::MessageHistory(CONST_S("Message history"), WHITE, 128);
festring msgsystem::LastMessage;
festring msgsystem::BigMessage;
int msgsystem::Times;
v2 msgsystem::Begin, msgsystem::End;
truth msgsystem::Enabled = true;
truth msgsystem::BigMessageMode = false;
truth msgsystem::MessagesChanged = true;
bitmap* msgsystem::QuickDrawCache = 0;
int msgsystem::LastMessageLines;
bool msgsystem::TempSoundEnabled = true;

static bool logdbSkip = false;
static SQ3DB logdb;
static int currgameid = 0;


//==========================================================================
//
//  dblogOpen
//
//==========================================================================
static void dblogOpen (const char *fname, bool readWrite) {
  if (readWrite) {
    logdb.OpenRWCreate(fname);
  } else {
    logdb.OpenRO(fname);
  }
  if (!logdb.IsOpen()) return;
  if (readWrite) {
    if (ivanconfig::GetSaveLogToDB() == 1) {
      logdb.Exec("PRAGMA journal_mode=DELETE;"); // deactivate WAL mode
      logdb.Exec("PRAGMA journal_mode=MEMORY;");
    } else {
      logdb.Exec("PRAGMA journal_mode=WAL;");
    }
    logdb.Exec(
      "BEGIN TRANSACTION;\n"
      "\n"
      "CREATE TABLE IF NOT EXISTS games (\n"
      "    id INTEGER PRIMARY KEY\n"
      "  , time INTEGER NOT NULL\n"
      ") STRICT;\n"
      "\n"
      "CREATE TABLE IF NOT EXISTS messages (\n"
      "    id INTEGER PRIMARY KEY\n"
      "  , gameid INTEGER NOT NULL\n"
      "  , time INTEGER NOT NULL              /* unix epoch */\n"
      "  , count INTEGER NOT NULL DEFAULT 1   /* number of repetitions */\n"
      "  , message TEXT NOT NULL              /* without formatting chars */\n"
      ") STRICT;\n"
      "CREATE TABLE IF NOT EXISTS rawmessages (\n"
      "    msgid INTEGER PRIMARY KEY\n"
      "  , message TEXT NOT NULL  /* with formatting chars */\n"
      ") STRICT;\n"
      "\n"
      "COMMIT TRANSACTION;\n"
      "");
  }
}


//==========================================================================
//
//  dblogClose
//
//==========================================================================
static void dblogClose () {
  logdb.Close();
}


//==========================================================================
//
//  CreateNewGameId
//
//==========================================================================
static void CreateNewGameId () {
  currgameid = 0;
  if (logdb.IsOpen()) {
    SQ3Stmt stmt;
    if (logdb.Stmt(stmt,
          "INSERT INTO games "
          "        (time)\n"
          "  VALUES(unixepoch('now'))\n"
          "RETURNING id AS id\n"
          ""))
    {
      while (stmt.Step() > 0) {
        currgameid = stmt.GetInt("id");
      }
      stmt.Close();
    }
  }
}


//==========================================================================
//
//  OpenLogDatabase
//
//==========================================================================
static void OpenLogDatabase (int currid) {
  if (logdb.IsOpen()) dblogClose();

  festring path;
  #ifdef LOCAL_SAVES
  path << inputfile::GetMyDir();
  #else
  path << getenv("HOME");
  if (path.IsEmpty()) {
    path << inputfile::GetMyDir();
  } else {
    path << "/.k8ivan";
    outputfile::MakeDir(path);
  }
  #endif
  path << "/k8ivan-messages.sqlite";

  currgameid = 0;
  if (!ivanconfig::GetSaveLogToDB()) {
    return;
  }
  dblogOpen(path.CStr(), true);

  if (logdb.IsOpen()) {
    if (currid != 0) {
      // try to find the old game
      #if 0
      ConLogf("looking for the old game with id #%d", currid);
      #endif
      SQ3Stmt stmt;
      if (logdb.Stmt(stmt,
            "SELECT id AS id\n"
            "FROM games\n"
            "WHERE id=:gameid\n"
            "LIMIT 1"
            ""))
      {
        stmt.BindInt(":gameid", currid);
        while (stmt.Step() > 0) {
          currgameid = stmt.GetInt("id");
        }
        stmt.Close();
      }
    }
    if (currgameid == 0) {
      CreateNewGameId();
      #if 0
      ConLogf("created new game with id #%d", currgameid);
      #endif
    } else {
      #if 0
      ConLogf("using old game id #%d", currgameid);
      #endif
    }
  } else {
    logdbSkip = true;
  }
}


//==========================================================================
//
//  msgsystem::Init
//
//==========================================================================
void msgsystem::Init () {
  QuickDrawCache = new bitmap(v2((game::GetScreenXSize() << 4) + 6, 106));
  QuickDrawCache->ActivateFastFlag();
  game::SetStandardListAttributes(MessageHistory);
  MessageHistory.AddFlags(INVERSE_MODE);
}


//==========================================================================
//
//  msgsystem::Shutdown
//
//==========================================================================
void msgsystem::Shutdown () {
  dblogClose();
  currgameid = 0;
}


//==========================================================================
//
//  msgsystem::Format
//
//==========================================================================
void msgsystem::Format () {
  MessageHistory.Empty();
  LastMessage.Empty();
  MessagesChanged = true;
  BigMessageMode = false;
  dblogClose();
  currgameid = 0;
}


//==========================================================================
//
//  AppendLogMessage
//
//==========================================================================
static void AppendLogMessage (cfestring &msg, int count) {
  if (!msg.IsEmpty() && logdb.IsOpen() && currgameid) {
    SQ3Stmt stmt;
    if (count > 1) {
      // we should check if it is really the same, but... meh
      if (logdb.Stmt(stmt,
            "UPDATE messages\n"
            "SET count=count+1\n"
            "WHERE id IN (SELECT MAX(id) FROM messages WHERE gameid=:gameid)\n"
            ""))
      {
        stmt.BindInt(":gameid", currgameid);
        stmt.Exec();
        stmt.Close();
        return;
      }
    }
    // failed for some reason, try to instert new message
    if (logdb.Stmt(stmt,
          "INSERT INTO messages "
          "        ( gameid, message, count, time)\n"
          "  VALUES(:gameid,:message,:count, unixepoch('now'))\n"
          "RETURNING id AS id\n"
          ""))
    {
      stmt.BindInt(":gameid", currgameid);
      stmt.BindInt(":count", count);
      stmt.BindTextNormSpaces(":message", msg);
      int msgid = 0;
      while (stmt.Step() > 0) {
        msgid = stmt.GetInt("id");
      }
      stmt.Close();
      if (msgid != 0) {
        if (logdb.Stmt(stmt,
              "INSERT INTO rawmessages "
              "        ( msgid, message)\n"
              "  VALUES(:msgid,:message)\n"
              ""))
        {
          stmt.BindInt(":msgid", msgid);
          stmt.BindText(":message", msg);
          stmt.Exec();
          stmt.Close();
        }
      }
    }
  }
}


//==========================================================================
//
//  msgsystem::AddMessage
//
//==========================================================================
void msgsystem::AddMessage (cchar *Format, ...) {
  if (!Enabled) return;
  if (BigMessageMode && BigMessage.GetSize() >= 512) LeaveBigMessageMode();
  static char Message[8192];

  va_list AP;
  va_start(AP, Format);
  vsnprintf(Message, sizeof(Message)-1, Format, AP);
  va_end(AP);

  festring Buffer(Message);

  if (!Buffer.GetSize()) ABORT("Empty message request!");

  if (TempSoundEnabled) {
    soundsystem::PlaySound(Buffer);
  }

  Buffer.Capitalize();

  /* Comment the first line and uncomment the second before the release! */
  if (isalpha(Buffer[Buffer.GetSize()-1])) {
    //Buffer << " (this sentence isn't terminated correctly because Hex doesn't know grammar rules)";
    Buffer.AppendChar('.');
  }

  if (BigMessageMode) {
    if (BigMessage.GetSize()) BigMessage.AppendChar(' ');
    BigMessage << Buffer;
    return;
  }

  ivantime Time;
  game::GetTime(Time);

  if (Buffer == LastMessage) {
    for (int c = 0; c < LastMessageLines; ++c) MessageHistory.Pop();
    ++Times;
    End = v2(Time.Hour, Time.Min);
  } else {
    Times = 1;
    Begin = End = v2(Time.Hour, Time.Min);
    LastMessage = Buffer;
    LastMessage.EnsureUnique();
  }

  if (!logdbSkip && !logdb.IsOpen()) {
    OpenLogDatabase(0);
  }

  // write to database
  if (!Buffer.IsEmpty() && logdb.IsOpen() && currgameid) {
    AppendLogMessage(Buffer, Times);
  }

  festring Temp;
  Temp << Begin.X << ':';

  if (Begin.Y < 10) Temp << '0';

  Temp << Begin.Y;

  if (Begin != End) {
    Temp << '-' << End.X << ':';
    if (End.Y < 10) Temp << '0';
    Temp << End.Y;
  }

  if (Times != 1) Temp << " (" << Times << "x)";

  Temp << ' ';
  //int Marginal = Temp.GetSize();
  int Marginal = -FONT->TextWidth(Temp);
  Temp << Buffer;

  std::vector<festring> Chapter;
  FONT->WordWrap(Temp, Chapter, MessageHistory.GetWidth() - 26, Marginal);

  for (uInt c = 0; c < Chapter.size(); ++c) {
    MessageHistory.AddEntry(Chapter[c], WHITE);
  }

  MessageHistory.SetSelected(MessageHistory.GetLastEntryIndex());
  LastMessageLines = Chapter.size();
  MessagesChanged = true;
}


//==========================================================================
//
//  msgsystem::Draw
//
//==========================================================================
void msgsystem::Draw () {
  truth WasInBigMessageMode = BigMessageMode;
  LeaveBigMessageMode();

  if (MessagesChanged) {
    MessageHistory.QuickDraw(QuickDrawCache, 8);
    MessagesChanged = false;
  }

  v2 Size = QuickDrawCache->GetSize();
  int X = 13;
  //int Y = RES.Y - 122;
  int Y = RES.Y - 110;

  if (ivanconfig::GetStatusOnLeft()) {
    X += 96;
  }

  blitdata B = { DOUBLE_BUFFER,
     { 0, 0 },
     { X, Y },
     { Size.X, Size.Y },
     { 0 },
     0,
     0 };

  QuickDrawCache->NormalBlit(B);
  igraph::BlitBackGround(v2(X + 1, Y), v2(1, 1));
  igraph::BlitBackGround(v2(X + 0 + Size.X, Y), v2(1, 1));
  igraph::BlitBackGround(v2(X + 1, Y + Size.Y - 1), v2(1, 1));
  igraph::BlitBackGround(v2(X + 0 + Size.X, Y + Size.Y - 1), v2(1, 1));

  if (WasInBigMessageMode) {
    EnterBigMessageMode();
  }
}


//==========================================================================
//
//  msgsystem::DrawMessageHistory
//
//==========================================================================
void msgsystem::DrawMessageHistory () {
  MessageHistory.Draw();
}


//==========================================================================
//
//  msgsystem::Save
//
//==========================================================================
void msgsystem::Save (outputfile &SaveFile) {
  MessageHistory.Save(SaveFile);
  sLong version = 0;
  SaveFile << version;
  SaveFile << LastMessage << Times << Begin << End;
  SaveFile << currgameid;
}


//==========================================================================
//
//  msgsystem::Load
//
//==========================================================================
void msgsystem::Load (inputfile &SaveFile) {
  dblogClose(); currgameid = 0; // just in case
  MessageHistory.Load(SaveFile);
  sLong version = 666;
  SaveFile >> version;
  SaveFile >> LastMessage >> Times >> Begin >> End;
  SaveFile >> currgameid;
  if (!logdbSkip) {
    OpenLogDatabase(currgameid);
  }
}


//==========================================================================
//
//  msgsystem::ScrollDown
//
//==========================================================================
void msgsystem::ScrollDown () {
  if (MessageHistory.GetSelected() < MessageHistory.GetLastEntryIndex()) {
    MessageHistory.EditSelected(1);
    MessagesChanged = true;
  }
}


//==========================================================================
//
//  msgsystem::ScrollUp
//
//==========================================================================
void msgsystem::ScrollUp () {
  if (MessageHistory.GetSelected()) {
    MessageHistory.EditSelected(-1);
    MessagesChanged = true;
  }
}


//==========================================================================
//
//  msgsystem::LeaveBigMessageMode
//
//==========================================================================
void msgsystem::LeaveBigMessageMode () {
  BigMessageMode = false;
  if (BigMessage.GetSize()) {
    const bool ooo = TempSoundEnabled;
    TempSoundEnabled = false;
    AddMessage("%s", BigMessage.CStr());
    TempSoundEnabled = ooo;
    BigMessage.Empty();
  }
}


//==========================================================================
//
//  msgsystem::ThyMessagesAreNowOld
//
//==========================================================================
void msgsystem::ThyMessagesAreNowOld () {
  if (MessageHistory.GetColor(MessageHistory.GetLastEntryIndex()) == WHITE) {
    MessagesChanged = true;
  }

  for (uInt c = 0; c < MessageHistory.GetLength(); ++c) {
    MessageHistory.SetColor(c, LIGHT_GRAY);
  }
}


//==========================================================================
//
//  msgsystem::SaveLastMessages
//
//==========================================================================
void msgsystem::SaveLastMessages () {
  for (uInt c = 0; c < MessageHistory.GetLength(); ++c) {
    highscore::AddLastMessage(MessageHistory.GetEntry(c));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// SOUND SYSTEM
// ////////////////////////////////////////////////////////////////////////// //
#ifndef DISABLE_SOUND

// 8 is more that enough
#define SND_CHANNEL_COUNT  (8)

struct SoundFile {
  festring filename;
  Mix_Chunk *chunk;
  //Mix_Music *music;
  bool reported;
};


struct SoundInfo {
  SEE_RegExpr *re;
  festring retext;
  std::vector<int> sounds;
  uint32_t soundid; // never 0
};

int soundsystem::SoundState = 0;

std::vector<SoundFile> soundsystem::files;
std::vector<SoundInfo> soundsystem::patterns;


struct ChanInfo {
  uint32_t soundid; // 0: channel is free
  Uint32 startTime;
};

// so we could throw away the chunk which is oldest
static ChanInfo channels[SND_CHANNEL_COUNT];


//==========================================================================
//
//  soundsystem::AddFile
//
//==========================================================================
int soundsystem::AddFile (cfestring &filename) {
  for (int i = 0; i < (int)files.size(); i += 1) {
    if (files[i].filename == filename) return i;
  }
  //ConLogf("sound file: <%s>", filename.CStr());
  SoundFile p;
  p.filename = filename;
  p.chunk = NULL;
  p.reported = false;
  //p.music = NULL;
  files.push_back(p);
  return (int)files.size() - 1;
}


//==========================================================================
//
//  getstr
//
//==========================================================================
static festring getstr (VFile f) {
  festring s;
  bool eof = false;
  while (!eof && (s.IsEmpty() || s[0] == '#')) {
    s.Empty();
    char c = 0;
    while (!eof && c != 10) {
      if (vwread(f, &c, 1) != 1) eof = true;
      else if (c != 10 && c != 13) s.AppendChar(c);
    }
    s.TrimAll();
  }
  return s;
}


//==========================================================================
//
//  soundsystem::InitSound
//
//==========================================================================
void soundsystem::InitSound () {
  if (SoundState == 0) {
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 8000) != 0) {
      ConLogf("Unable to initialize audio: %s", Mix_GetError());
      SoundState = -1;
      return;
    }
    Mix_AllocateChannels(SND_CHANNEL_COUNT);
    SetVolume(ivanconfig::GetSoundVolume());
    SoundState = -2;
    festring cfgfile = game::GetGamePath() + "sound/config.rc";
    VFile f = vwopen(cfgfile.CStr());
    if (!f) {
      SoundState = -1;
    } else {
      uint32_t soundid = 0;
      festring Pattern, File;
      for (;;) {
        Pattern = getstr(f);
        if (Pattern.IsEmpty()) break;
        if (Pattern[0] != '^' && !Pattern.StartsWith(".*")) {
          Pattern = CONST_S(".*") + Pattern;
        }
        if (!Pattern.EndsWith(".*") && !Pattern.EndsWith("$")) {
          Pattern << ".*";
        }
        SoundInfo si;
        si.re = SEE_regex_parse(Pattern.CStr(), Pattern.GetSize(), SEERX_FLAG_IGNORECASE);
        if (!si.re) {
          ConLogf("Sound regexp compilation failed: %s", Pattern.CStr());
          Pattern = getstr(f);
        } else {
          si.retext = Pattern;
          Pattern = getstr(f);
          while (!Pattern.IsEmpty()) {
            festring::sizetype pos = 0;
            while (pos != Pattern.GetSize() && (Pattern[pos] < 0 || Pattern[pos] > 32)) {
              pos += 1;
            }
            festring fname = Pattern.LeftCopy((int)pos);
            if (!fname.IsEmpty()) {
              si.sounds.push_back(AddFile(fname));
            }
            Pattern.Erase(0, pos);
            Pattern.TrimAll();
          }
          if (si.sounds.size() != 0) {
            soundid += 1;
            si.soundid = soundid;
            patterns.push_back(si);
          }
        }
      }
      vwclose(f);
      if (patterns.size()) {
        SoundState = 1;
      } else {
        SoundState = -1;
      }
      //Mix_HookMusicFinished(changeMusic);
    }
    if (SoundState != 1) {
      Mix_CloseAudio();
    } else {
      for (int f = 0; f != SND_CHANNEL_COUNT; f += 1) {
        channels[f].soundid = 0;
        channels[f].startTime = 0;
      }
    }
  }
}


//==========================================================================
//
//  soundsystem::FindMatchingSound
//
//==========================================================================
SoundFile *soundsystem::FindMatchingSound (const festring &Buffer, uint32_t *soundid) {
  if (soundid) *soundid = 0;
  for (int f = (int)patterns.size() - 1; f >= 0; --f) {
    if (patterns[f].re) {
      if (SEE_regex_match(patterns[f].re, Buffer.CStr(), Buffer.GetSize(), 0, NULL)) {
        const int fidx = patterns[f].sounds[NG_RAND_N((int)patterns[f].sounds.size())];
        if (DEBUG_SOUND) {
          ConLogf("***SOUND [%s] for re [%s]; msg=<%s>",
                  files[fidx].filename.CStr(), patterns[f].retext.CStr(), Buffer.CStr());
        }
        if (soundid) *soundid = patterns[f].soundid;
        return &files[fidx];
      }
    }
  }
  return NULL;
}


//==========================================================================
//
//  soundsystem::SetVolume
//
//==========================================================================
void soundsystem::SetVolume (sLong vol) {
  if (SoundState == 1) {
    vol = Clamp(vol, 0, 128);
    for (int f = 0; f < SND_CHANNEL_COUNT; f += 1) {
      Mix_Volume(f, vol);
    }
  }
}


//==========================================================================
//
//  soundsystem::PlaySound
//
//==========================================================================
void soundsystem::PlaySound (const festring &Buffer) {
  if (!ivanconfig::GetPlaySounds()) return;
  InitSound();
  if (SoundState == 1) {
    uint32_t soundid;
    SoundFile *sf = FindMatchingSound(Buffer, &soundid);
    if (!sf) return;
    if (!sf->chunk) {
      //festring sndfile = game::GetGamePath() + "sound/" + sf->filename;
      festring sndfile = game::GetGamePath() + "sound/vorbis/" + sf->filename + ".ogg";
      VFile fl = vwopen(sndfile.CStr());
      if (fl) {
        #if 0
        ConLogf("loading sound: '%s'", sndfile.CStr());
        #endif
        SDL_RWops *rw = vwAllocSDLRW(fl);
        if (rw) {
          sf->chunk = Mix_LoadWAV_RW(rw, 0);
          vwFreeSDLRW(rw);
        }
        vwclose(fl);
      } else if (!sf->reported) {
        sf->reported = true;
        ConLogf("missing sound: '%s'", sf->filename.CStr());
      }
    }
    if (sf->chunk) {
      int latest = -1;
      // replace duplicated sound, if there is one
      for (int f = 0; latest == -1 && f != SND_CHANNEL_COUNT; f += 1) {
        if (!Mix_Playing(f)) {
          channels[f].soundid = 0;
        } else if (channels[f].soundid == soundid) {
          latest = f;
        }
      }
      // if no duplicate sound, replace the oldest one
      if (latest == -1) {
        for (int f = 0; f != SND_CHANNEL_COUNT; f += 1) {
          if (channels[f].soundid == 0 || !Mix_Playing(f)) {
            latest = f;
            break;
          } else if (latest < 0 || channels[f].startTime < channels[latest].startTime) {
            latest = f;
          }
        }
      }
      if (latest >= 0) {
        //ConLogf("starting sound: '%s'", sf->filename.CStr());
        Mix_HaltChannel(latest);
        Mix_Volume(latest, ivanconfig::GetSoundVolume());
        Mix_PlayChannel(latest, sf->chunk, 0);
        channels[latest].soundid = soundid;
        channels[latest].startTime = SDL_GetTicks();
      }
    }
  }
}

#else

void soundsystem::InitSound () {}
int soundsystem::AddFile (const festring &filename) { return 0; }
SoundFile *soundsystem::FindMatchingSound (const festring &Buffer) { return NULL; }
void soundsystem::PlaySound (const festring &Buffer) {}
void soundsystem::SetVolume (sLong vol) {}

#endif
