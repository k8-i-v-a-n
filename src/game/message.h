/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include <vector>
#include "v2.h"
#include "festring.h"

#ifdef SHITDOZE
// fuck you, shitdoze!
# ifdef PlaySound
#  undef PlaySound
# endif
#endif


#define ADD_MESSAGE  msgsystem::AddMessage


class felist;
class outputfile;
class inputfile;
class bitmap;


class msgsystem {
public:
  static void Init ();
  static void Shutdown ();

  static void LIKE_PRINTF(1, 2) AddMessage (cchar*, ...);
  static void Draw ();
  static void DrawMessageHistory ();
  static void Format (); // this clears everything
  static void Save (outputfile&);
  static void Load (inputfile&);
  static void ScrollDown ();
  static void ScrollUp ();
  static void EnableMessages () { Enabled = true; }
  static void DisableMessages () { Enabled = false; }
  static void EnterBigMessageMode () { BigMessageMode = true; }
  static void LeaveBigMessageMode ();
  static void ThyMessagesAreNowOld ();

  static void SaveLastMessages ();

  // used in "Look" command
  static void DisableSounds () { TempSoundEnabled = false; }
  static void EnableSounds () { TempSoundEnabled = true; }

private:
  static felist MessageHistory;
  static festring LastMessage;
  static festring BigMessage;
  static int Times;
  static v2 Begin, End;
  static truth Enabled;
  static truth BigMessageMode;
  static truth MessagesChanged;
  static bitmap* QuickDrawCache;
  static int LastMessageLines;
  static bool TempSoundEnabled;
};


class soundsystem {
public:
  static void PlaySound (const festring &Buffer);
  static void SetVolume (sLong vol); // 0..128

  static void InitSound ();

  static void DisableSound () { SoundState = -1; }

private:
  static int AddFile (const festring &filename);
  static struct SoundFile *FindMatchingSound (const festring &Buffer, uint32_t *soundid);

private:
  static int SoundState;
  static std::vector<struct SoundFile> files;
  static std::vector<struct SoundInfo> patterns;
};


#endif
