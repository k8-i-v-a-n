/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#include "object.h"
#include "materia.h"
#include "festring.h"
#include "whandler.h"
#include "rawbit.h"
#include "proto.h"
#include "game.h"
#include "bitmap.h"
#include "fesave.h"
#include "feparse.h"


v2 RightArmSparkleValidityArray[128];
v2 LeftArmSparkleValidityArray[128];
v2 GroinSparkleValidityArray[169];
v2 RightLegSparkleValidityArray[42];
v2 LeftLegSparkleValidityArray[45];
v2 NormalSparkleValidityArray[256];
v2 PossibleSparkleBuffer[256];


//==========================================================================
//
//  object::object
//
//==========================================================================
object::object () : entity(0), MainMaterial(0), VisualEffects(0) {}


//==========================================================================
//
//  object::object
//
//==========================================================================
object::object (const object& Object)
  : entity(Object)
  , id(Object)
  , VisualEffects(Object.VisualEffects)
{
  CopyMaterial(Object.MainMaterial, MainMaterial);
  mOnEvents = Object.mOnEvents;
}


//==========================================================================
//
//  object::~object
//
//==========================================================================
object::~object () {
  delete MainMaterial;
}


//==========================================================================
//
//  object::GetSpecialFlags
//
//==========================================================================
int object::GetSpecialFlags () const {
  return ST_NORMAL;
}


//==========================================================================
//
//  object::GetOutlineColor
//
//==========================================================================
col16 object::GetOutlineColor (int) const {
  return TRANSPARENT_COLOR;
}


//==========================================================================
//
//  object::GetPicture
//
//==========================================================================
cbitmap *const *object::GetPicture () const {
  return GraphicData.Picture;
}


//==========================================================================
//
//  object::CopyMaterial
//
//==========================================================================
void object::CopyMaterial (material *const &Source, material *&Dest) {
  if (Source) {
    Dest = Source->Duplicate();
    Dest->SetMotherEntity(this);
  } else {
    Dest = 0;
  }
}


//==========================================================================
//
//  object::Save
//
//==========================================================================
void object::Save (outputfile &SaveFile) const {
  SaveFile << GraphicData << (int)VisualEffects;
  SaveFile << MainMaterial;
}


//==========================================================================
//
//  object::Load
//
//==========================================================================
void object::Load (inputfile &SaveFile) {
  SaveFile >> GraphicData >> (int&)VisualEffects;
  LoadMaterial(SaveFile, MainMaterial);
}


//==========================================================================
//
//  object::ObjectInitMaterials
//
//==========================================================================
void object::ObjectInitMaterials (material *&FirstMaterial, material *FirstNewMaterial,
                                  sLong FirstDefaultVolume,
                                  material *&SecondMaterial, material *SecondNewMaterial,
                                  sLong SecondDefaultVolume, truth CallUpdatePictures)
{
  InitMaterial(FirstMaterial, FirstNewMaterial, FirstDefaultVolume);
  InitMaterial(SecondMaterial, SecondNewMaterial, SecondDefaultVolume);
  SignalVolumeAndWeightChange();
  if (CallUpdatePictures) UpdatePictures();
}


//==========================================================================
//
//  object::InitMaterial
//
//==========================================================================
void object::InitMaterial (material *&Material, material *NewMaterial, sLong DefaultVolume) {
  Material = NewMaterial;
  if (Material) {
    if (Material->HasBe()) Enable();
    if (DefaultVolume && !Material->GetVolume()) Material->SetVolume(DefaultVolume);
    Material->SetMotherEntity(this);
    SignalEmitationIncrease(Material->GetEmitation());
  }
}


//==========================================================================
//
//  object::ChangeMaterial
//
//==========================================================================
void object::ChangeMaterial (material *&Material, material *NewMaterial, sLong DefaultVolume,
                             int SpecialFlags)
{
  delete SetMaterial(Material, NewMaterial, DefaultVolume, SpecialFlags);
}


//==========================================================================
//
//  object::SetMaterial
//
//==========================================================================
material *object::SetMaterial (material *&Material, material *NewMaterial, sLong DefaultVolume,
                               int SpecialFlags)
{
  material *OldMaterial = Material;
  Material = NewMaterial;
  if ((!OldMaterial || !OldMaterial->HasBe()) && NewMaterial && NewMaterial->HasBe()) {
    Enable();
  } else if (OldMaterial && OldMaterial->HasBe() && (!NewMaterial || !NewMaterial->HasBe()) && !CalculateHasBe()) {
    Disable();
  }
  if (NewMaterial) {
    if (!NewMaterial->GetVolume()) {
      if (OldMaterial) NewMaterial->SetVolume(OldMaterial->GetVolume());
      else if (DefaultVolume) NewMaterial->SetVolume(DefaultVolume);
      else ABORT("Singularity spawn detected!");
    }
    NewMaterial->SetMotherEntity(this);
    if (!(SpecialFlags&NO_SIGNALS)) SignalEmitationIncrease(NewMaterial->GetEmitation());
  }
  if (!(SpecialFlags&NO_SIGNALS)) {
    if (OldMaterial) SignalEmitationDecrease(OldMaterial->GetEmitation());
    SignalVolumeAndWeightChange();
    SignalMaterialChange();
  }
  if (!(SpecialFlags&NO_PIC_UPDATE)) UpdatePictures();
  return OldMaterial;
}


//==========================================================================
//
//  object::UpdatePictures
//
//==========================================================================
void object::UpdatePictures () {
  const cv2 ZeroPos(0, 0);
  UpdatePictures(GraphicData, ZeroPos, VisualEffects|GetSpecialFlags(),
                 GetMaxAlpha(), GetGraphicsContainerIndex(),
                 &object::GetBitmapPos);
}


//==========================================================================
//
//  object::RandomizeSparklePos
//
//==========================================================================
truth object::RandomizeSparklePos (v2 &SparklePos, v2 BPos, int &SparkleTime, feuLong SeedBase,
                                   int SpecialFlags, int GraphicsContainerIndex) const
{
  static int SeedModifier = 1;
  v2 *ValidityArray;
  int ValidityArraySize;
  //auto saviour = femath::SeedSaviour();
  //femath::SaveSeed();
  //femath::SetSeed(SeedBase+SeedModifier);
  auto prng = ngprng;
  prng.SetSeed(SeedBase + SeedModifier);
  if (++SeedModifier > 0x10) SeedModifier = 1;

  if ((SpecialFlags&0x38) == ST_RIGHT_ARM) {
    ValidityArray = RightArmSparkleValidityArray;
    ValidityArraySize = 128;
  } else if ((SpecialFlags&0x38) == ST_LEFT_ARM) {
    ValidityArray = LeftArmSparkleValidityArray;
    ValidityArraySize = 128;
  } else if ((SpecialFlags&0x38) == ST_GROIN) {
    ValidityArray = GroinSparkleValidityArray;
    ValidityArraySize = 169;
  } else if ((SpecialFlags&0x38) == ST_RIGHT_LEG) {
    ValidityArray = RightLegSparkleValidityArray;
    ValidityArraySize = 42;
  } else if ((SpecialFlags&0x38) == ST_LEFT_LEG) {
    ValidityArray = LeftLegSparkleValidityArray;
    ValidityArraySize = 45;
  } else {
    ValidityArray = NormalSparkleValidityArray;
    ValidityArraySize = 256;
  }

  SparklePos = igraph::GetRawGraphic(GraphicsContainerIndex)->RandomizeSparklePos(
                                     prng, ValidityArray,
                                     PossibleSparkleBuffer, BPos, TILE_V2, ValidityArraySize,
                                     GetSparkleFlags());
  if (SparklePos != ERROR_V2) {
    SparkleTime = X_RAND_N(prng, 241);
    //femath::LoadSeed();
    return true;
  }
  //femath::LoadSeed();
  return false;
}


//==========================================================================
//
//  object::UpdatePictures
//
//==========================================================================
void object::UpdatePictures (graphicdata &GraphicData, v2 Position, int SpecialFlags,
                             alpha MaxAlpha, int GraphicsContainerIndex,
                             bposretriever BitmapPosRetriever) const
{
  int AnimationFrames = GetClassAnimationFrames();
  v2 SparklePos;
  int SparkleTime = 0;
  int Seed = 0;
  int FlyAmount = GetSpoilLevel();
  truth Sparkling = false, FrameNeeded = false, SeedNeeded = false;
  v2 BPos = (this->*BitmapPosRetriever)(0);
  alpha Alpha;

  if ((SpecialFlags & (ST_FLAMES | ST_LIGHTNING)) == 0) {
    if (AllowSparkling()) {
      int SparkleFlags = GetSparkleFlags();
      if (SparkleFlags && RandomizeSparklePos(SparklePos, BPos, SparkleTime,
                                              BPos.X + BPos.Y + GetMaterialColorA(0),
                                              SpecialFlags, GraphicsContainerIndex))
      {
        Sparkling = true;
        if (AnimationFrames <= 256) AnimationFrames = 256;
      }
    }
    if (FlyAmount) {
      SeedNeeded = true;
      FrameNeeded = true;
      if (AnimationFrames <= 32) AnimationFrames = 32;
    }
  } else if (SpecialFlags & ST_FLAMES) {
    SeedNeeded = true;
    FrameNeeded = true;
    if (AnimationFrames <= 16) AnimationFrames = 16;
  } else if (SpecialFlags & ST_LIGHTNING) {
    SeedNeeded = true;
    if (AnimationFrames <= 128) AnimationFrames = 128;
  }

  if (SeedNeeded) {
    static int SeedModifier = 1;
    Seed = BPos.X+BPos.Y+GetMaterialColorA(0)+SeedModifier+0x42;
    if (++SeedModifier > 0x10) SeedModifier = 1;
  }

  int WobbleMask = 0, WobbleData = GetWobbleData();
  if (WobbleData & WOBBLE) {
    int Speed = (WobbleData & WOBBLE_SPEED_RANGE) >> WOBBLE_SPEED_SHIFT;
    int Freq = (WobbleData & WOBBLE_FREQ_RANGE) >> WOBBLE_FREQ_SHIFT;
    int WobbleFrames = 512 >> (Freq + Speed);
    WobbleMask = 7 >> Freq << (6 - Speed);
    if (AnimationFrames <= WobbleFrames) AnimationFrames = WobbleFrames;
  }

  ModifyAnimationFrames(AnimationFrames);

  int OldAnimationFrames = GraphicData.AnimationFrames;
  for (int c = 0; c != OldAnimationFrames; c += 1) {
    igraph::RemoveUser(GraphicData.GraphicIterator[c]);
  }

  if (OldAnimationFrames != AnimationFrames) {
    if (OldAnimationFrames) {
      delete [] GraphicData.Picture;
      delete [] GraphicData.GraphicIterator;
    }
    GraphicData.Picture = new bitmap *[AnimationFrames];
    GraphicData.GraphicIterator = new GfxUID[AnimationFrames];
  }

  GraphicData.AnimationFrames = AnimationFrames;
  if (!AllowRegularColors()) SpecialFlags |= ST_DISALLOW_R_COLORS;

  if (GraphicsContainerIndex == GR_ITEM) {
    if ((SpecialFlags & ST_NO_OUTLINE_FLAGS) == 0 && AllowSimpleOutline()) {
      SpecialFlags |= ST_SIMPLE_ITEM_OUTLINE;
    } else {
      SpecialFlags &= ~ST_SIMPLE_ITEM_OUTLINE; // just in case
    }
  } else {
    SpecialFlags &= ~ST_SIMPLE_ITEM_OUTLINE; // just in case
  }

  graphicid GI;
  GI.BaseAlpha = MaxAlpha;
  GI.FileIndex = GraphicsContainerIndex;
  GI.SpecialFlags = SpecialFlags;
  GI.Seed = Seed;
  GI.FlyAmount = FlyAmount;
  GI.Position = Position;
  GI.RustData[0] = GetRustDataA();
  GI.RustData[1] = GetRustDataB();
  GI.RustData[2] = GetRustDataC();
  GI.RustData[3] = GetRustDataD();
  GI.WobbleData = WobbleData;

  for (int c = 0; c < AnimationFrames; ++c) {
    GI.Color[0] = GetMaterialColorA(c);
    GI.Color[1] = GetMaterialColorB(c);
    GI.Color[2] = GetMaterialColorC(c);
    GI.Color[3] = GetMaterialColorD(c);
    Alpha = GetAlphaA(c);
    GI.Alpha[0] = (Alpha < MaxAlpha ? Alpha : MaxAlpha);
    Alpha = GetAlphaB(c);
    GI.Alpha[1] = (Alpha < MaxAlpha ? Alpha : MaxAlpha);
    Alpha = GetAlphaC(c);
    GI.Alpha[2] = (Alpha < MaxAlpha ? Alpha : MaxAlpha);
    Alpha = GetAlphaD(c);
    GI.Alpha[3] = (Alpha < MaxAlpha ? Alpha : MaxAlpha);
    v2 BPos = (this->*BitmapPosRetriever)(c);
    GI.BitmapPosX = BPos.X;
    GI.BitmapPosY = BPos.Y;
    if (Sparkling && c > SparkleTime && c < SparkleTime+16) {
      GI.SparklePosX = SparklePos.X;
      GI.SparklePosY = SparklePos.Y;
      GI.SparkleFrame = c-SparkleTime;
    } else {
      GI.SparklePosX = SPARKLE_POS_X_ERROR;
      GI.SparklePosY = 0;
      GI.SparkleFrame = 0;
    }

    GI.Frame =
      !c || FrameNeeded ||
      ((SpecialFlags&ST_LIGHTNING) && !((c+1)&7)) ||
      ((WobbleData&WOBBLE) && !(c&WobbleMask)) ? c : 0;

    GI.OutlineColor = GetOutlineColor(c);
    GI.OutlineAlpha = GetOutlineAlpha(c);
    // if it is already outlined, no need to add another one
    if (GI.OutlineColor != TRANSPARENT_COLOR) {
      SpecialFlags &= ~ST_SIMPLE_ITEM_OUTLINE;
    }

    TileGfxInfo *gi = 0;
    GfxUID Iterator = igraph::AddUserEx(GI, gi);
    GraphicData.GraphicIterator[c] = Iterator;
    GraphicData.Picture[c] = gi->Bitmap;
  }
}


//==========================================================================
//
//  object::GetMaterialColorA
//
//==========================================================================
col16 object::GetMaterialColorA (int) const {
  return MainMaterial->GetColor();
}


//==========================================================================
//
//  object::AddRustLevelDescription
//
//==========================================================================
truth object::AddRustLevelDescription (festring &String, truth Articled) const {
  if (MainMaterial) {
    return MainMaterial->AddRustLevelDescription(String, Articled);
  }
  return false;
}


//==========================================================================
//
//  object::AddMaterialDescription
//
//==========================================================================
truth object::AddMaterialDescription (festring &String, truth Articled) const {
  //FIXME: gum solution
  if (IsBoneNameSingular()) {
    //FIXME: 'bone bone' removing
    festring s(MainMaterial->GetName(Articled));
    festring::sizetype pos = s.FindLast("bone");
    if (pos != festring::NPos && pos == s.GetSize() - 4) {
      while (pos > 0 && s[pos-1] == ' ') pos -= 1;
      s.Erase(pos, s.GetSize() - pos);
      if (s.GetSize() == 0) return true; // no name left
    }
    String << s;
  } else {
    MainMaterial->AddName(String, Articled);
  }
  String << ' ';
  return true;
}


//==========================================================================
//
//  object::AddContainerPostFix
//
//  FIXME: bottles may not be "full", fix this!
//
//==========================================================================
void object::AddContainerPostFix (festring &String, truth isFull) const {
  if (GetSecondaryMaterial()) {
    if (isFull) {
      GetSecondaryMaterial()->AddName(String << " full of ", false, false);
    } else {
      GetSecondaryMaterial()->AddName(String << " with some ", false, false);
    }
  }
}


//==========================================================================
//
//  object::AddContainerPostFix2
//
//==========================================================================
void object::AddContainerPostFix2 (festring &String, int volume, int maxvolume) const {
  if (GetSecondaryMaterial() && volume > 0) {
    if (volume >= maxvolume) {
      GetSecondaryMaterial()->AddName(String << " full of ", false, false);
    } else {
      const int prc = 100 * volume / maxvolume;
           if (prc > 80) GetSecondaryMaterial()->AddName(String << " full of ", false, false);
      else if (prc >= 60) GetSecondaryMaterial()->AddName(String << " almost full of ", false, false);
      else if (prc >= 30) GetSecondaryMaterial()->AddName(String << " with ", false, false);
      else GetSecondaryMaterial()->AddName(String << " with some ", false, false);
    }
  }
}


//==========================================================================
//
//  object::AddLumpyPostFix
//
//==========================================================================
void object::AddLumpyPostFix (festring &String) const {
  MainMaterial->AddName(String << " of ", false, false);
}


//==========================================================================
//
//  object::GetAlphaA
//
//==========================================================================
alpha object::GetAlphaA (int) const {
  return MainMaterial->GetAlpha();
}


//==========================================================================
//
//  object::RandomizeVisualEffects
//
//==========================================================================
void object::RandomizeVisualEffects () {
  int AcceptedFlags = GetOKVisualEffects();
  if (AcceptedFlags) {
    SetVisualEffects((RAND_8 & AcceptedFlags)|GetForcedVisualEffects());
  } else {
    SetVisualEffects(GetForcedVisualEffects());
  }
}


//==========================================================================
//
//  object::LoadMaterial
//
//==========================================================================
void object::LoadMaterial (inputfile &SaveFile, material *&Material) {
  SaveFile >> Material;
  if (Material) {
    if (Material->HasBe()) Enable();
    Material->SetMotherEntity(this);
    game::CombineLights(Emitation, Material->GetEmitation());
  }
}


//==========================================================================
//
//  object::RandomizeMaterialConfiguration
//
//==========================================================================
int object::RandomizeMaterialConfiguration () {
  const fearray<sLong>& MCC = GetMaterialConfigChances();
  return MCC.Size > 1 ? femath::WeightedRand(MCC.Data, GetMaterialConfigChanceSum()) : 0;
}


//==========================================================================
//
//  object::AddEmptyAdjective
//
//==========================================================================
truth object::AddEmptyAdjective (festring &String, truth Articled) const {
  if (GetSecondaryMaterial()) return false;
  String << (Articled ? "an empty " : "empty ");
  return true;
}


//==========================================================================
//
//  object::CalculateEmitation
//
//==========================================================================
void object::CalculateEmitation () {
  Emitation = GetBaseEmitation();
  if (MainMaterial) game::CombineLights(Emitation, MainMaterial->GetEmitation());
}


//==========================================================================
//
//  object::CalculateHasBe
//
//==========================================================================
truth object::CalculateHasBe () const {
  return MainMaterial && MainMaterial->HasBe();
}


//==========================================================================
//
//  object::GetSparkleFlags
//
//==========================================================================
int object::GetSparkleFlags () const {
  return MainMaterial->IsSparkling() ? SPARKLING_A : 0;
}


//==========================================================================
//
//  object::InitSparkleValidityArrays
//
//==========================================================================
void object::InitSparkleValidityArrays () {
  int Index = 0;

  for (int y = 0; y < 16; ++y)
    for (int x = 0; x < 8; ++x)
      RightArmSparkleValidityArray[Index++] = v2(x, y);

  Index = 0;
  for (int y = 0; y < 16; ++y)
    for (int x = 8; x < 16; ++x)
      LeftArmSparkleValidityArray[Index++] = v2(x, y);

  Index = 0;
  for (int y = 0; y < 10; ++y)
    for (int x = 0; x < 16; ++x)
      GroinSparkleValidityArray[Index++] = v2(x, y);
  for (int y = 10; y < 13; ++y)
    for (int x = y-5; x < 20-y; ++x)
      GroinSparkleValidityArray[Index++] = v2(x, y);

  Index = 0;
  for (int y = 10; y < 16; ++y)
    for (int x = 0; x < 8; ++x)
      if ((y != 10 || x < 5) && (y != 11 || x < 6) && (y != 12 || x < 7))
        RightLegSparkleValidityArray[Index++] = v2(x, y);

  Index = 0;
  for (int y = 10; y < 16; ++y)
    for (int x = 8; x < 16; ++x)
      if ((y != 10 || x > 9) && (y != 11 || x > 8))
        LeftLegSparkleValidityArray[Index++] = v2(x, y);

  Index = 0;
  for (int y = 0; y < 16; ++y)
    for (int x = 0; x < 16; ++x)
      NormalSparkleValidityArray[Index++] = v2(x, y);
}


//==========================================================================
//
//  object::GetRustDataA
//
//==========================================================================
int object::GetRustDataA () const {
  return MainMaterial->GetRustData();
}


//==========================================================================
//
//  object::DetectMaterial
//
//==========================================================================
truth object::DetectMaterial (cmaterial *Material) const {
  for (int c = 0; c < GetMaterials(); ++c) {
    if (GetMaterial(c) && GetMaterial(c)->IsSameAs(Material)) return true;
  }
  return false;
}
