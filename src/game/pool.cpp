/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through coreset.cpp */
#include <stdio.h>
#include <set>

#include "feerror.h"
#include "game.h"
#include "materia.h"
#include "action.h"


#if 0
# define xlogf(...)  do { ConPrintf(__VA_ARGS__); } while (0)
#else
# define xlogf(...)
#endif


// ////////////////////////////////////////////////////////////////////////// //
typedef std::unordered_set<action *> ActionSet;

static ActionSet deadActions;


//==========================================================================
//
//  pool::HellWithAction
//
//==========================================================================
void pool::HellWithAction (action *act) {
  IvanAssert(act);
  if (deadActions.find(act) == deadActions.end()) {
    #if 0
    ConLogf("addind dead action '%s'...", act->GetDescription());
    #endif
    deadActions.insert(act);
  } else {
    #if 0
    ConLogf("DOUBLE addind dead action '%s'...", act->GetDescription());
    #endif
  }
}


//==========================================================================
//
//  BurnActions
//
//==========================================================================
static void BurnActions () {
  ActionSet list;
  for (auto &act : deadActions) list.insert(act);
  deadActions.clear();
  for (auto &act : list) {
    #if 0
    ConLogf("removing action '%s'...", act->GetDescription());
    #endif
    delete act;
  }
}


////////////////////////////////////////////////////////////////////////////////
typedef std::unordered_set<material *> MaterialSet;

static MaterialSet matersAlive;
static MaterialSet matersToKill;


//==========================================================================
//
//  pool::RegisterMaterial
//
//==========================================================================
void pool::RegisterMaterial (material *mb) {
  if (!mb) return;
  if (matersAlive.find(mb) != matersAlive.end()) return;
  if (matersToKill.find(mb) != matersToKill.end()) ABORT("Cannot resurrect dead material");
  matersAlive.insert(mb);
}


//==========================================================================
//
//  pool::UnregisterMaterial
//
//==========================================================================
void pool::UnregisterMaterial (material *mb) {
  if (!mb) return;
  auto it = matersAlive.find(mb);
  if (it != matersAlive.end()) matersAlive.erase(it);
  it = matersToKill.find(mb);
  if (it != matersToKill.end()) matersToKill.erase(it);
}


//==========================================================================
//
//  pool::MaterToHell
//
//==========================================================================
void pool::MaterToHell (material *mb) {
  if (mb) {
    auto it = matersAlive.find(mb);
    if (it == matersAlive.end()) {
      it = matersToKill.find(mb);
      if (it == matersToKill.end()) ABORT("Cannot send to hell unregistered material");
      // already sent
      return;
    }
    matersAlive.erase(it);
    it = matersToKill.find(mb);
    if (it == matersToKill.end()) matersToKill.insert(mb);
  }
}


//==========================================================================
//
//  pool::BurnMaterHell
//
//==========================================================================
void pool::BurnMaterHell () {
  MaterialSet list;
  for (auto &mb : matersToKill) list.insert(mb);
  matersToKill.clear();
  for (auto &mb : list) delete mb;
}


//==========================================================================
//
//  pool::BurnAllMaterials
//
//==========================================================================
void pool::BurnAllMaterials () {
  BurnMaterHell();
  MaterialSet list = matersAlive;
  for (auto &mb : matersAlive) list.insert(mb);
  matersAlive.clear();
  for (auto &mb : list) delete mb;
}


////////////////////////////////////////////////////////////////////////////////
typedef std::unordered_set<entity *> EntitySet;


typedef struct EntityListItem {
  struct EntityListItem *next;
  entity *e;
} EntityListItem;


typedef struct EntityList {
  struct EntityListIterator *iterators;
  struct EntityListItem *head;
  struct EntityListItem *tail;
  EntitySet *items;
} EntityList;


typedef struct EntityListIterator {
  struct EntityListIterator *next;
  EntityList *owner;
  EntityListItem *nextitem;
} EntityListIterator;


//==========================================================================
//
//  elInitialize
//
//==========================================================================
static void elInitialize (EntityList *el) {
  memset(el, 0, sizeof(*el));
  el->items = new EntitySet();
}


//==========================================================================
//
//  elIsInList
//
//==========================================================================
static bool elIsInList (EntityList *el, entity *e) {
  EntitySet::const_iterator it = el->items->find(e);
  return (it != el->items->end());
}


//==========================================================================
//
//  elAddItem
//
//==========================================================================
static void elAddItem (EntityList *el, entity *e) {
  EntitySet::const_iterator it = el->items->find(e);
  if (it == el->items->end()) {
    EntityListItem *i = (EntityListItem *)calloc(1, sizeof(EntityListItem));
    el->items->insert(e);
    i->e = e;
    i->next = NULL;
    if (el->tail != NULL) el->tail->next = i; else el->head = i;
    el->tail = i;
  }
}


//==========================================================================
//
//  elClear
//
//==========================================================================
static void elClear (EntityList *el) {
  while (el->head != NULL) {
    EntityListItem *i = el->head;
    el->head = i->next;
    free(i);
  }
  el->tail = NULL;
  el->items->clear();
}


//==========================================================================
//
//  elRemoveItem
//
//==========================================================================
static void elRemoveItem (EntityList *el, entity *e) {
  EntitySet::iterator it = el->items->find(e);
  if (it != el->items->end()) {
    EntityListItem *p = NULL, *i;
    el->items->erase(it);
    for (i = el->head; i != NULL; p = i, i = i->next) if (i->e == e) break;
    if (i == NULL) {
      // memleak is better than crash (not sure)
      //ConLogf("WARNING: elRemoveItem() desync!");
      ABORT("FATAL: elRemoveItem() desync!");
    } else {
      for (EntityListIterator *eit = el->iterators; eit != NULL; eit = eit->next) {
        if (eit->nextitem == i) eit->nextitem = i->next;
      }
      if (p != NULL) p->next = i->next; else el->head = i->next;
      if (i->next == NULL) el->tail = p;
      free(i);
    }
  }
}


//==========================================================================
//
//  elInitIterator
//
//==========================================================================
static entity *elInitIterator (EntityList *el, EntityListIterator *it) {
  memset(it, 0, sizeof(*it));
  it->owner = el;
  it->nextitem = (el->head != NULL ? el->head->next : NULL);
  it->next = el->iterators;
  el->iterators = it;
  return (el->head != NULL ? el->head->e : NULL);
}


//==========================================================================
//
//  elRemoveIterator
//
//==========================================================================
static void elRemoveIterator (EntityListIterator *it) {
  if (it->owner != NULL) {
    if (it->owner->iterators == it) {
      it->owner->iterators = it->next;
    } else {
      EntityListIterator *p = NULL, *c;
      for (c = it->owner->iterators; c != NULL; p = c, c = c->next) if (c == it) break;
      if (c == NULL || p == NULL) ABORT("FATAL: elRemoveIterator() desync!");
      p->next = it->next;
    }
    memset(it, 0, sizeof(*it));
  }
}


//==========================================================================
//
//  elIteratorNext
//
//==========================================================================
static entity *elIteratorNext (EntityListIterator *it) {
  if (it->nextitem != NULL) {
    entity *e = it->nextitem->e;
    it->nextitem = it->nextitem->next;
    return e;
  } else {
    elRemoveIterator(it);
  }
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
static EntityList beList;
static EntityList hellList;


static bool burningHell = false; // is we burning lost souls?
static bool doBeing = false; // being process in progress?
static bool doRegister = true; // nothing %-)


////////////////////////////////////////////////////////////////////////////////
pool::pool () {
  elInitialize(&beList);
  elInitialize(&hellList);
}


//==========================================================================
//
//  pool::IsBurningHell
//
//==========================================================================
truth pool::IsBurningHell () {
  return burningHell;
}


//==========================================================================
//
//  pool::RegisterState
//
//==========================================================================
void pool::RegisterState (truth doreg) {
  //doRegister = doreg;
}


//==========================================================================
//
//  pool::Be
//
//  Calls the Be() function of each self-changeable entity during each tick,
//  thus allowing acting characters, spoiling food etc.
//
//==========================================================================
void pool::Be () {
  EntityListIterator it;
  entity *e;

  if (burningHell) { entity::DumpDeadSet(); ABORT("FATAL: started to being while burning souls!"); }
  if (doBeing) { entity::DumpDeadSet(); ABORT("FATAL: started to being while already being!"); }

  xlogf("Be: START\n");
  doBeing = true;
  try {
    for (e = elInitIterator(&beList, &it); e != NULL; e = elIteratorNext(&it)) {
      const uint32_t mmark = entity::GetUniqueMemoryMark(e);
      xlogf("Be: %p (%d)\n", e, mmark);
      e->Be();
      if (it.owner == NULL) break; // aborted
      if (elIsInList(&beList, e)) {
        if (entity::GetUniqueMemoryMark(e) != mmark) {
          entity::DumpDeadSet();
          ABORT("FATAL: entity polymorphed while being!");
        }
      }
    }
  #if 0
  } catch (FileError *e) {
    elRemoveIterator(&it);
    festring msg = e->msg;
    delete e;
    ConLogf("FILE ERROR: %s", msg.CStr());
    __builtin_trap();
  #endif
  } catch (...) {
    elRemoveIterator(&it);
    throw;
  }
  elRemoveIterator(&it);

  doBeing = false;
  xlogf("Be: DONE\n");
}


//==========================================================================
//
//  pool::AbortBe
//
//==========================================================================
void pool::AbortBe () {
  doBeing = false;
  for (EntityListIterator *eit = beList.iterators; eit != NULL; eit = eit->next) {
    eit->owner = NULL; eit->nextitem = NULL;
  }
  beList.iterators = NULL;
}


//==========================================================================
//
//  pool::BurnHell
//
//==========================================================================
void pool::BurnHell () {
  EntityListIterator it;
  entity *e;

  if (!game::IsRunning()) AbortBe();
  if (burningHell) { entity::DumpDeadSet(); ABORT("FATAL: started to burning souls while already burning souls!"); }
  if (doBeing) { entity::DumpDeadSet(); ABORT("FATAL: started to burning souls while being!"); }

  burningHell = true;
  xlogf("BurnHell: START\n");
  BurnActions();
  const bool verbose = (DEBUG_HELL_VERBOSE < 0);
  try {
    for (e = elInitIterator(&hellList, &it); e != NULL; e = elIteratorNext(&it)) {
      if (verbose) {
        festring nn = e->DebugGetName();
        // there are too many fluids leaking ;-)
        /*if (!nn.StartsWith("fluid:"))*/ {
          ConLogf("DEBUG: entity '%s' deleted.", nn.CStr());
        }
      }
      delete e;
      elRemoveItem(&hellList, e);
    }
  } catch (...) {
    elRemoveIterator(&it);
    throw;
  }
  elRemoveIterator(&it);

  BurnMaterHell();

  burningHell = false;
  xlogf("BurnHell: DONE\n");
}


//==========================================================================
//
//  pool::KillBees
//
//==========================================================================
void pool::KillBees () {
  /*
  xlogf("pool::KillBees()\n");
  beSets.clear();
  xnewBeSet.clear();
  notDoSet.clear();
  BurnHell();
  */
  // this leaks, but who cares...
  deadActions.clear();
  elInitialize(&beList);
  elInitialize(&hellList);
  burningHell = false;
  doBeing = false;
  doRegister = true;
}


//==========================================================================
//
//  pool::Add
//
//==========================================================================
void pool::Add (entity *e) {
  if (doRegister && e) {
    elRemoveItem(&hellList, e);
    if (!elIsInList(&beList, e)) {
      elAddItem(&beList, e);
      xlogf("Add: %p\n", e);
    }
  }
}


//==========================================================================
//
//  pool::Remove
//
//==========================================================================
void pool::Remove (entity *e) {
  if (doRegister && e) {
    if (elIsInList(&beList, e)) {
      xlogf("Remove: %p\n", e);
      elRemoveItem(&beList, e);
    }
  }
}


//==========================================================================
//
//  pool::AddToHell
//
//==========================================================================
void pool::AddToHell (entity *e) {
  if (doRegister && e) {
    Remove(e); // burn it with fire!
    if (!elIsInList(&hellList, e)) {
      xlogf("AddToHell: %p\n", e);
      elAddItem(&hellList, e);
    }
  }
}


//==========================================================================
//
//  pool::RemoveFromHell
//
//==========================================================================
void pool::RemoveFromHell (entity *e) {
  if (doRegister && e) {
    if (elIsInList(&hellList, e)) {
      xlogf("RemoveFromHell: %p\n", e);
      elRemoveItem(&hellList, e);
    }
  }
}


//==========================================================================
//
//  pool::IsInHell
//
//==========================================================================
bool pool::IsInHell (entity *e) {
  return (e && elIsInList(&hellList, e));
}


//==========================================================================
//
//  pool::RemoveEverything
//
//==========================================================================
void pool::RemoveEverything () {
  elClear(&beList);
  elClear(&hellList);
  //BurnMaterHell();
  matersAlive.clear();
  matersToKill.clear();
}


#undef xlogf


static pool poolsingleton;
