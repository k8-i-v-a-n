/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through dataset.cpp */

#include "confdef.h"
#include "level.h"


itemdatabase **protosystem::ItemConfigData;
int protosystem::ItemConfigDataSize;
itemdatabase **protosystem::ItemCategoryData[ITEM_CATEGORIES];
int protosystem::ItemCategorySize[ITEM_CATEGORIES];
sLong protosystem::ItemCategoryPossibility[ITEM_CATEGORIES];
sLong protosystem::TotalItemPossibility;


// slows down protosystem::BalancedCreateItem() but makes it produce more accurate results
// was `100`
#define BALANCED_CREATE_ITEM_ITERATIONS       GCONST(BALANCED_CREATE_ITEM_ITERATIONS)
#define BALANCED_CREATE_GOD_ITEM_ITERATIONS   GCONST(BALANCED_CREATE_GOD_ITEM_ITERATIONS)

/* was 100 */
#define BALANCED_MONSTER_SELECT_LIMIT  GCONST(BALANCED_MONSTER_SELECT_LIMIT)
/* was 25 */
#define BALANCED_MONSTER_TRY_LIMIT  GCONST(BALANCED_MONSTER_TRY_LIMIT)

/* was 25; used to find a suitable monster for polymorphing */
#define POLY_MONSTER_TRY_LIMIT  GCONST(POLY_MONSTER_TRY_LIMIT)

/* was 25 */
#define BALANCED_ITEM_TRY_LIMIT       GCONST(BALANCED_ITEM_TRY_LIMIT)
#define BALANCED_GOD_ITEM_TRY_LIMIT   GCONST(BALANCED_GOD_ITEM_TRY_LIMIT)


//==========================================================================
//
//  CheckAllowedDungeons
//
//==========================================================================
static bool CheckAllowedDungeons (const fearray<int> &dunglist) {
  bool res = false;
  if (dunglist.Size > 0) {
    const int dungnum = game::GetCurrentDungeonIndex();
    for (uInt f = 0; f != dunglist.Size && !res; f += 1) {
      res = (dunglist[f] == ALL_DUNGEONS || dunglist[f] == dungnum);
    }
  } else {
    res = true;
  }
  return res;
}


//==========================================================================
//
//  CheckLevelTags
//
//==========================================================================
static bool CheckLevelTags (const fearray<festring> &taglist, level *lvl) {
  bool res = false;
  if (lvl && taglist.Size > 0) {
    for (uInt f = 0; f != taglist.Size && !res; f += 1) {
      res = (taglist[f] == "*");
    }
    if (!res) {
      cfestring tag = *lvl->GetLevelScript()->GetTag();
      if (!tag.IsEmpty()) {
        // masks are "name*" and "*name" (yes, that limited)
        if (tag.GetSize() > 1 && tag[tag.GetSize() - 1] == '*') {
          festring tmask = tag;
          tmask.Left(tag.Length() - 1);
          for (uInt f = 0; f != taglist.Size && !res; f += 1) {
            res = taglist[f].StartsWithCI(tmask);
          }
        } else if (tag.GetSize() > 1 && tag[0] == '*') {
          festring tmask = tag;
          tmask.Erase(0, 1);
          for (uInt f = 0; f != taglist.Size && !res; f += 1) {
            res = taglist[f].EndsWithCI(tmask);
          }
        } else {
          for (uInt f = 0; f != taglist.Size && !res; f += 1) {
            res = taglist[f].EquCI(tag);
          }
        }
      }
    }
  } else {
    res = true;
  }
  return res;
}


//==========================================================================
//
//  protosystem::BalancedCreateMonster
//
//==========================================================================
character *protosystem::BalancedCreateMonster (level *lvl) {
  /* k8:
  this whole thing seems to be quite ineffective. we are choosing monsters
  time and time again, only to try some 25 of them. then we throwing away
  the whole list, and rebuilding almost the same list again. this repeats
  until selection limit is reached.

  i am *almost* sure that we only need to built the list twice. first time
  with "appropriate" monsters, and then with all monsters (which should be
  a rare occasion).

  but note that `game::GetMinDifficulty()` is randomised, so the lists will
  not be the same each time.
  */
  if (!lvl) lvl = game::GetCurrentLevel();
  const int selectlimit = Clamp(BALANCED_MONSTER_SELECT_LIMIT, 50, 8192);
  const int trylimit = Clamp(BALANCED_MONSTER_TRY_LIMIT, 15, 50);
  const int debugFlags = DEBUG_MONSTER_SPAWN;

  for (int selectcount = 0; ; selectcount += 1) {
    double MinDifficulty = game::GetMinDifficulty();
    double MaxDifficulty = MinDifficulty * 25;

    if (debugFlags & 0xff) {
      ConLogf("=== BalancedCreateMonster: try #%d; diff: %g : %g ===",
              selectcount, MinDifficulty, MaxDifficulty);
    }
    if (selectcount == selectlimit) {
      ConLogf("*** BalancedCreateMonster: normal selection FAILED!");
    }

    std::vector<configid> Possible;
    for (int Type = 1; Type < protocontainer<character>::GetSize(); ++Type) {
      const character::prototype *Proto = protocontainer<character>::GetProto(Type);
      if (!Proto) continue; // missing character

      const character::database *const *ConfigData = Proto->GetConfigData();
      int ConfigSize = Proto->GetConfigSize();
      for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
        const character::database *DataBase = ConfigData[cfgidx];
        if (!DataBase->IsAbstract && DataBase->CanBeGenerated) {
          // check for dungeon/level restrictions
          if (!CheckAllowedDungeons(DataBase->AllowedDungeons) ||
              !CheckLevelTags(DataBase->LevelTags, lvl))
          {
            continue;
          }

          if (DataBase->IsUnique && (DataBase->Flags & HAS_BEEN_GENERATED)) continue;

          truth IsCatacomb = *game::GetCurrentLevel()->GetLevelScript()->IsCatacomb();
          if ((IsCatacomb && !DataBase->IsCatacombCreature) ||
              (!IsCatacomb && DataBase->CanBeGeneratedOnlyInTheCatacombs))
          {
            continue;
          }

          configid ConfigID(Type, /*ConfigData[cfgidx]*/DataBase->Config);
          //k8: i believe that this should be `selectcount`, not `cfgidx`
          //k8: but this might be something for special configs instead...
          //k8: but this is `selectcount` below, so...
          if (selectcount >= selectlimit) { // this was `cfgidx`
            //if (debugFlags)
            {
              ConLogf("BalancedCreateMonster: selectlimit reached, pushing '%s : %s'",
                      Proto->GetClassID(), DataBase->CfgStrName.CStr());
            }
            Possible.push_back(ConfigID);
            continue;
          }

          const dangerid &DangerID = game::GetDangerMap().find(ConfigID)->second;
          if (!DataBase->IgnoreDanger) {
            double Danger = DangerID.EquippedDanger;
            if (Danger > 99.0 || Danger < 0.0011 || (DataBase->IsUnique && Danger < 3.5)) {
              if (debugFlags & DMS_SHOW_SKIPS) {
                ConLogf("  SKIP-00: '%s : %s' (Danger=%g)", Proto->GetClassID(),
                        DataBase->CfgStrName.CStr(), Danger);
              }
              continue;
            }
            if (DataBase->DangerModifier == 0) {
              ABORT("'%s' : '%s' -- zero `DangerModifier`!",
                    Proto->GetClassID(), DataBase->CfgStrName.CStr());
            }
            double DangerModifier =
              DataBase->DangerModifier == 100 ? Danger
                                              : Danger * 100 / DataBase->DangerModifier;
            if (DangerModifier < MinDifficulty || DangerModifier > MaxDifficulty) {
              if (debugFlags & DMS_SHOW_SKIPS) {
                ConLogf("  SKIP-01: '%s : %s' (Danger=%g; DMod=%g)", Proto->GetClassID(),
                        DataBase->CfgStrName.CStr(), Danger, DangerModifier);
              }
              continue;
            }
          }

          ivantime Time;
          game::GetTime(Time);

          if (!PLAYER) {
            ConLogf("WARNING: WTF?! (protosystem::BalancedCreateMonster)");
            //continue;
          }

          if ((PLAYER && PLAYER->GetMaxHP() < DataBase->HPRequirementForGeneration) &&
              Time.Day < DataBase->DayRequirementForGeneration)
          {
            if (debugFlags & DMS_SHOW_SKIPS) {
              ConLogf("  SKIP-02: '%s : %s' (hp=%d:%d; day=%d:%d)", Proto->GetClassID(),
                      DataBase->CfgStrName.CStr(),
                      PLAYER->GetMaxHP(), DataBase->HPRequirementForGeneration,
                      Time.Day, DataBase->DayRequirementForGeneration);
            }
            continue;
          }

          if (debugFlags & DMS_SHOW_CONSIDERS) {
            ConLogf(" *CONSIDER: '%s : %s'", Proto->GetClassID(), DataBase->CfgStrName.CStr());
          }
          Possible.push_back(ConfigID);
        }
      }
    }

    // if we reached selection limit, there will always be somebody to spawn.
    if (Possible.empty()) continue;

    for (int i = 0; i != trylimit; i += 1) {
      configid Chosen = Possible[RAND_GOOD((int)Possible.size())];
      const character::prototype *Proto = protocontainer<character>::GetProto(Chosen.Type);
      if (!Proto) continue; // missing character
      character *Monster = Proto->Spawn(Chosen.Config);
      if (selectcount >= selectlimit ||
          ((Monster->GetFrequency() /*==*/>= 10000 || Monster->GetFrequency() > RAND_GOOD(10000)) &&
           (Monster->IsUnique() || (Monster->GetTimeToKill(PLAYER, true) > 5000 &&
                                    PLAYER->GetTimeToKill(Monster, true) < 200000))))
      {
        if (debugFlags & DMS_SHOW_SPAWNS) {
          ConLogf(" ***SPAWN: '%s : %s' : pkill: %g; mkill: %g",
                  Monster->GetClassID(), Monster->GetConfigName().CStr(),
                  PLAYER->GetTimeToKill(Monster, true),
                  Monster->GetTimeToKill(PLAYER, true));
        }
        Monster->SetTeam(game::GetTeam(MONSTER_TEAM));
        return Monster;
      }
      delete Monster;
    }
  }

  /* This line is never reached, but it prevents warnings given by some (stupid) compilers. */
  return 0;
}


//==========================================================================
//
//  protosystem::BalancedCreateItem
//
//==========================================================================
item *protosystem::BalancedCreateItem (level *lvl, sLong MinPrice, sLong MaxPrice,
                                       sLong RequiredCategory, int SpecialFlags,
                                       int ConfigFlags, int RequiredGod, truth Polymorph)
{
  typedef item::database database;
  database **PossibleCategory[ITEM_CATEGORIES];
  int PossibleCategorySize[ITEM_CATEGORIES];
  sLong PartialCategoryPossibilitySum[ITEM_CATEGORIES];
  int PossibleCategories = 0;
  sLong TotalPossibility = 0;
  //k8: dafuck... this is pointer to `sLong` member. no, i haven't the slightest idea.
  sLong database::*PartialPossibilitySumPtr = 0;

  if (!lvl) lvl = game::GetCurrentLevel();
  const int debugFlags = DEBUG_ITEM_SPAWN;

  if (RequiredCategory == ANY_CATEGORY) {
    PartialPossibilitySumPtr = &database::PartialPossibilitySum;
    PossibleCategory[0] = ItemConfigData;
    PossibleCategorySize[0] = ItemConfigDataSize;
    TotalPossibility = TotalItemPossibility;
    PartialCategoryPossibilitySum[0] = TotalPossibility;
    PossibleCategories = 1;
    if (debugFlags & DIS_SHOW_CATSEARCH) {
      ConLogf("***: BalancedCreateItem: any category; TotPoss=%d; items=%d",
              TotalPossibility, ItemConfigDataSize);
    }
  } else {
    PartialPossibilitySumPtr = &database::PartialCategoryPossibilitySum;
    if (debugFlags & DIS_SHOW_CATSEARCH) {
      ConLogf("***: BalancedCreateItem: cats=0x%04x", (unsigned)RequiredCategory);
    }
    for (sLong CategoryIndex = 0, Category = 1; CategoryIndex < ITEM_CATEGORIES; ++CategoryIndex, Category <<= 1) {
      if (Category & RequiredCategory) {
        // there could be unknown items
        if (ItemCategoryData[CategoryIndex]) {
          PossibleCategory[PossibleCategories] = ItemCategoryData[CategoryIndex];
          PossibleCategorySize[PossibleCategories] = ItemCategorySize[CategoryIndex];
          TotalPossibility += ItemCategoryPossibility[CategoryIndex];
          PartialCategoryPossibilitySum[PossibleCategories] = TotalPossibility;
          if (debugFlags & DIS_SHOW_CATSEARCH) {
            ConLogf("   :   cat=%d; poss=%d; items=%d", CategoryIndex,
                    ItemCategoryPossibility[CategoryIndex], ItemCategorySize[CategoryIndex]);
          }
          ++PossibleCategories;
        }
      }
    }
    if (debugFlags & DIS_SHOW_CATSEARCH) {
      ConLogf("***: BalancedCreateItem: CatCount=%d; TotPoss=%d", PossibleCategories,
              TotalPossibility);
    }
  }

  const int trylimit = RequiredGod ? Clamp(BALANCED_GOD_ITEM_TRY_LIMIT, 25, 1024)
                                   : Clamp(BALANCED_ITEM_TRY_LIMIT, 25, 1024);
  for (int trycount = 0;; ++trycount) {
    if (debugFlags & 0xff) {
      ConLogf("=== BalancedCreateItem: try #%d; categories: %d ===",
              trycount, PossibleCategories);
    }
    const int iters = RequiredGod ? Clamp(BALANCED_CREATE_GOD_ITEM_ITERATIONS, 50, 8192)
                                  : Clamp(BALANCED_CREATE_ITEM_ITERATIONS, 50, 8192);
    for (int itercount = 0; itercount < iters; ++itercount) {
      sLong Rand = RAND_GOOD(TotalPossibility);
      const sLong RandOrig = Rand;
      int Category;
      if (RequiredCategory == ANY_CATEGORY) {
        Category = 0;
      } else {
        Category = 0; // k8: could it be undefined otherwise? i don' think so, but...
        for (int c2 = 0;; ++c2) {
          IvanAssert(c2 < PossibleCategories);
          if (PartialCategoryPossibilitySum[c2] > Rand) {
            Category = c2;
            break;
          }
        }
        if (Category) {
          Rand -= PartialCategoryPossibilitySum[Category-1];
        }
      }

      if (debugFlags & DIS_SHOW_CATSEARCH) {
        ConLogf("      RandOrig=%d; Category=%d; RandLeft=%d",
                RandOrig, Category, Rand);
      }

      const database *const *ChosenCategory = PossibleCategory[Category];
      const database *ChosenDataBase = 0;
      if (!ChosenCategory[0]) continue; // k8: just in case
      int CCNum = 0;
      if (ChosenCategory[0]->PartialCategoryPossibilitySum > Rand) {
        ChosenDataBase = ChosenCategory[0];
      } else {
        sLong A = 0;
        sLong B = PossibleCategorySize[Category] - 1;
        #if 0
        ConLogf("### looking for %d ###", Rand);
        for (sLong f = 0; f != B; f += 1) {
          ConLogf("  #%d: %d", f, ChosenCategory[f]->*PartialPossibilitySumPtr);
        }
        #endif
        // sanity check
        #if 1
        for (sLong f = 1; f < B; f += 1) {
          IvanAssert(ChosenCategory[f - 1]->*PartialPossibilitySumPtr <= ChosenCategory[f - 1]->*PartialPossibilitySumPtr);
        }
        #endif
        // k8: this is definitely binary search of... something.
        // k8: looks like weighted random, but done in some weird way.
        #if 0
        ConLogf("*** Category=%d; B=%d ***", Category, B);
        #endif
        for (;;) {
          sLong C = (A + B) >> 1;
          if (A != C) {
            #if 0
            ConLogf("...C=%d; ppp=%p", C, ChosenCategory[C]);
            #endif
            if (ChosenCategory[C]->*PartialPossibilitySumPtr > Rand) B = C; else A = C;
          } else {
            CCNum = B;
            ChosenDataBase = ChosenCategory[B];
            break;
          }
        }
      }

      // it should not end up here, but...
      if (ChosenDataBase->IsAbstract) {
        continue;
      }

      int Config = ChosenDataBase->Config;
      if ((!(ConfigFlags & NO_BROKEN) || !(Config & BROKEN)) &&
          (!Polymorph || ChosenDataBase->IsPolymorphSpawnable))
      {
        // check for dungeon/level restrictions
        if (!CheckAllowedDungeons(ChosenDataBase->AllowedDungeons) ||
            !CheckLevelTags(ChosenDataBase->LevelTags, lvl))
        {
          continue;
        }

        item *Item = ChosenDataBase->ProtoType->Spawn(Config, SpecialFlags);
        const truth GodOK = (!RequiredGod || Item->GetAttachedGod() == RequiredGod);

        if (debugFlags & DIS_SHOW_POSSIBLE) {
          ConLogf(" Item: '%s:%s' (CCNum=%d) GodOk:%d; price:%d (%d:%d)",
                  Item->GetClassID(), Item->GetConfigName().CStr(),
                  CCNum, (int)GodOK,
                  Item->GetTruePrice(), MinPrice, MaxPrice);
        }

        /* Optimization, GetTruePrice() may be rather slow */
        if (GodOK && ((MinPrice == 0 && MaxPrice == MAX_PRICE) ||
                      ((Config & BROKEN) && (ConfigFlags & IGNORE_BROKEN_PRICE))))
        {
          if (debugFlags & DIS_SHOW_ACCEPT) ConLogf(" ***ACCEPT");
          return Item;
        }

        sLong Price = Item->GetTruePrice();
        if (Item->HandleInPairs()) Price <<= 1;
        if (Price >= MinPrice && Price <= MaxPrice && GodOK) {
          if (debugFlags & DIS_SHOW_ACCEPT) ConLogf(" ***ACCEPT");
          return Item;
        }
        if (debugFlags & DIS_SHOW_REJECT) ConLogf(" *REJECT*");
        delete Item;
      }
    }

    if (trycount == trylimit && RequiredGod) {
      ConLogf("***: BalancedCreateItem: failed to create item for %s (iterlimit:%d)",
              game::GetGod(RequiredGod)->GetName(), iters);
      return 0;
    }

    const sLong omin = MinPrice;
    const sLong omax = MaxPrice;
    MinPrice = MinPrice * 7 >> 3;
    MaxPrice = MaxPrice * 9 >> 3;
    ConLogf("BalancedCreateItem: failed to create item (iterlimit:%d); "
            "MinPrice=%d/%d; MaxPrice=%d/%d", iters,
            omin, MinPrice, omax, MaxPrice);
  }
}


//==========================================================================
//
//  protosystem::CreateMonster
//
//  this is used for polymorphing
//
//==========================================================================
character *protosystem::CreateMonster (int MinDanger, int MaxDanger, int SpecialFlags) {
  std::vector<configid> Possible;
  character *Monster = 0;
  const int trylimit = Clamp(POLY_MONSTER_TRY_LIMIT, 25, 1024);

  for (int trycount = 0; !Monster; ++trycount) {
    for (int Type = 1; Type < protocontainer<character>::GetSize(); ++Type) {
      const character::prototype *Proto = protocontainer<character>::GetProto(Type);
      if (!Proto) continue; // missing character
      const character::database*const* ConfigData = Proto->GetConfigData();
      int ConfigSize = Proto->GetConfigSize();

      for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
        const character::database* DataBase = ConfigData[cfgidx];

        if (!DataBase->IsAbstract && DataBase->CanBeGenerated && DataBase->CanBeWished && !DataBase->IsUnique &&
            (DataBase->Frequency == 10000 || DataBase->Frequency > RAND_GOOD(10000)))
        {
          configid ConfigID(Type, DataBase->Config);

          //k8: i believe that this should be `trycount`, not `cfgidx`
          //k8: but this might be something for special configs instead...
          //k8: yet it was `100` above, so it doesn't look like hard-coded config,
          //k8: but rather like "generate at least something if we failed long enough".
          if ((MinDanger > 0 || MaxDanger < 1000000) && trycount < trylimit) { // was `cfgindex`
            const dangerid &DangerID = game::GetDangerMap().find(ConfigID)->second;
            double RawDanger = (SpecialFlags & NO_EQUIPMENT ? DangerID.NakedDanger
                                                            : DangerID.EquippedDanger);
            int Danger = int(DataBase->DangerModifier == 100
                                  ? RawDanger * 1000
                                  : RawDanger * 100000 / DataBase->DangerModifier);
            if (Danger < MinDanger || Danger > MaxDanger) continue;
          }

          Possible.push_back(ConfigID);
        }
      }
    }

    if (Possible.empty()) {
      MinDanger = (MinDanger > 0 ? Max(MinDanger * 3 >> 2, 1) : 0);
      MaxDanger = (MaxDanger < 1000000 ? Min(MaxDanger * 5 >> 2, 999999) : 1000000);
      continue;
    }

    configid Chosen = Possible[RAND_GOOD(Possible.size())];
    auto monsProto = protocontainer<character>::GetProto(Chosen.Type);
    if (!monsProto) ABORT("Cannot create monster of type %d without prototype!", Chosen.Type);
    Monster = monsProto->Spawn(Chosen.Config, SpecialFlags);
    Monster->SignalGeneration();
    Monster->SetTeam(game::GetTeam(MONSTER_TEAM));
  }

  return Monster;
}


//==========================================================================
//
//  CountCorrectNameLetters
//
//==========================================================================
template <class type>
std::pair<int, int> CountCorrectNameLetters (const typename type::database *DataBase,
                                             cfestring &Identifier)
{
  std::pair<int, int> Result(0, 0);

  if (!DataBase->NameSingular.IsEmpty()) ++Result.second;
  if (festring::FindCI(Identifier, CONST_S(" ") + DataBase->NameSingular + " ") != festring::NPos) Result.first += DataBase->NameSingular.GetSize();
  if (!DataBase->Adjective.IsEmpty()) ++Result.second;
  if (DataBase->Adjective.GetSize() && festring::FindCI(Identifier, CONST_S(" ") + DataBase->Adjective + " ") != festring::NPos) Result.first += DataBase->Adjective.GetSize();
  if (!DataBase->PostFix.IsEmpty()) ++Result.second;
  if (DataBase->PostFix.GetSize() && festring::FindCI(Identifier, CONST_S(" ") + DataBase->PostFix + " ") != festring::NPos) Result.first += DataBase->PostFix.GetSize();

  for (uInt c = 0; c < DataBase->Alias.Size; ++c) {
    if (festring::FindCI(Identifier, CONST_S(" ") + DataBase->Alias[c] + " ") != festring::NPos) {
      Result.first += DataBase->Alias[c].GetSize();
    }
  }

  return Result;
}


//==========================================================================
//
//  SearchForProto
//
//==========================================================================
template <class type>
std::pair<const typename type::prototype *, int> SearchForProto (cfestring &What,
                                                    truth Output, truth forScript=false)
{
  typedef typename type::prototype prototype;
  typedef typename type::database database;

  festring Identifier;
  Identifier << ' ' << What << ' ';
  truth Illegal = false, Conflict = false;
  truth BrokenRequested = (festring::FindCI(Identifier, CONST_S(" broken ")) != festring::NPos);
  std::pair<const prototype *, int> ID(0, 0);
  std::pair<int, int> Best(0, 0);

  for (int protocount = 1; protocount < protocontainer<type>::GetSize(); ++protocount) {
    const prototype *Proto = protocontainer<type>::GetProto(protocount);
    if (!Proto) continue; // missing something
    const database *const *ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();

    for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
      if (!ConfigData[cfgidx]->IsAbstract) {
        if (BrokenRequested == !(ConfigData[cfgidx]->Config & BROKEN)) continue;
        std::pair<int, int> Correct = CountCorrectNameLetters<type>(ConfigData[cfgidx], Identifier);
        if (Correct == Best) {
          if (forScript) {
            ConLogf("search (%s) conflict: cfg=%s%d; name=%s; adj=%s; post=%s",
                    What.CStr(),
                    (ConfigData[cfgidx]->Config & BROKEN ? "(broken)" : ""),
                    ConfigData[cfgidx]->Config % BROKEN,
                    ConfigData[cfgidx]->NameSingular.CStr(),
                    ConfigData[cfgidx]->Adjective.CStr(),
                    ConfigData[cfgidx]->PostFix.CStr());
          }
          Conflict = true;
        } else if (Correct.first > Best.first ||
                   (Correct.first == Best.first && Correct.second < Best.second))
        {
          if (forScript || ConfigData[cfgidx]->CanBeWished || game::WizardModeIsActive()) {
            ID.first = Proto;
            ID.second = ConfigData[cfgidx]->Config;
            Best = Correct;
            Conflict = false;
          } else {
            Illegal = true;
          }
        }
      }
    }
  }

  if (Output) {
    if (!Best.first) {
      if (Illegal) {
        ADD_MESSAGE("You hear a booming voice: \"No, mortal! This will not be done!\"");
      } else {
        ADD_MESSAGE("What a strange wish!");
      }
    } else if (Conflict) {
      ADD_MESSAGE("Be more precise!");
      return std::pair<const prototype*, int>(0, 0);
    }
  }

  return ID;
}


//==========================================================================
//
//  protosystem::CreateMonster
//
//==========================================================================
character *protosystem::CreateMonster (cfestring &What, int SpecialFlags, truth Output) {
  std::pair<const character::prototype*, int> ID = SearchForProto<character>(What, Output);
  if (ID.first) {
    character *Char = ID.first->Spawn(ID.second, SpecialFlags);
    if (!Char->HasBeenSeen() && !game::WizardModeIsActive()) {
      ADD_MESSAGE("You have no idea what this creature is like.");
      delete Char; //Char->SendToHell(); // k8:equipment
      return 0;
    }
    return Char;
  }
  return 0;
}


//==========================================================================
//
//  protosystem::CreateMonsterCheat
//
//==========================================================================
character *protosystem::CreateMonsterCheat (cfestring &What, int SpecialFlags) {
  std::pair<const character::prototype *, int> ID = SearchForProto<character>(What, false, true);
  if (ID.first) {
    character *Char = ID.first->Spawn(ID.second, SpecialFlags);
    return Char;
  }
  return 0;
}


//==========================================================================
//
//  protosystem::CreateItem
//
//==========================================================================
item *protosystem::CreateItem (cfestring &What, truth Output) {
  std::pair<const item::prototype*, int> ID = SearchForProto<item>(What, Output);
  if (ID.first) {
    item *Item = ID.first->Spawn(ID.second);
    // only Wizard can confirm wishes
    if (game::WizardModeIsActive()) {
      festring Q = CONST_S("Do you want to wish for \1Y");
      Item->AddName(Q, INDEFINITE | STRIPPED | NO_FLUIDS);
      Q << "\2?";
      if (!game::TruthQuestion(Q)) { delete Item; return 0; }
    }
    return Item;
  }
  return 0;
}


//==========================================================================
//
//  protosystem::CreateItemForScript
//
//==========================================================================
item *protosystem::CreateItemForScript (cfestring &What) {
  std::pair<const item::prototype*, int> ID = SearchForProto<item>(What, false, true);
  if (ID.first) {
    return ID.first->Spawn(ID.second);
  }
  return 0;
}


//==========================================================================
//
//  protosystem::CreateMaterial
//
//==========================================================================
material *protosystem::CreateMaterial (cfestring& What, sLong Volume, truth Output) {
  for (int c1 = 1; c1 < protocontainer<material>::GetSize(); ++c1) {
    const material::prototype* Proto = protocontainer<material>::GetProto(c1);
    if (!Proto) continue; // missing material
    const material::database*const* ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    for (int cfgidx = 1; cfgidx < ConfigSize; ++cfgidx) {
      if (ConfigData[cfgidx]->NameStem == What) {
        if (ConfigData[cfgidx]->CommonFlags & CAN_BE_WISHED || game::WizardModeIsActive()) {
          return ConfigData[cfgidx]->ProtoType->Spawn(ConfigData[cfgidx]->Config, Volume);
        }
        if (Output) {
          ADD_MESSAGE("You hear a booming voice: \"No, mortal! This will not be done!\"");
          return 0;
        }
      }
    }
  }
  if (Output) ADD_MESSAGE("There is no such material.");
  return 0;
}


//==========================================================================
//
//  protosystem::CreateEveryCharacter
//
//==========================================================================
void protosystem::CreateEveryCharacter (charactervector &Character) {
  for (int c1 = 1; c1 < protocontainer<character>::GetSize(); ++c1) {
    const character::prototype* Proto = protocontainer<character>::GetProto(c1);
    if (!Proto) continue; // missing character
    const character::database*const* ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
      if (!ConfigData[cfgidx]->IsAbstract) Character.push_back(Proto->Spawn(ConfigData[cfgidx]->Config));
    }
  }
}


//==========================================================================
//
//  protosystem::CreateEveryItem
//
//==========================================================================
void protosystem::CreateEveryItem (itemvectorvector &Item) {
  for (int protocount = 1; protocount < protocontainer<item>::GetSize(); ++protocount) {
    const item::prototype* Proto = protocontainer<item>::GetProto(protocount);
    if (!Proto) continue; // missing item
    const item::database*const* ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
      if (!ConfigData[cfgidx]->IsAbstract && ConfigData[cfgidx]->IsAutoInitializable) {
        Item.push_back(itemvector(1, Proto->Spawn(ConfigData[cfgidx]->Config)));
      }
    }
  }
}


//==========================================================================
//
//  protosystem::CreateEveryMaterial
//
//==========================================================================
void protosystem::CreateEveryMaterial (std::vector<material *> &Material) {
  for (int c1 = 1; c1 < protocontainer<material>::GetSize(); ++c1) {
    const material::prototype *Proto = protocontainer<material>::GetProto(c1);
    if (!Proto) continue; // missing material
    const material::database*const* ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    for (int cfgidx = 1; cfgidx < ConfigSize; ++cfgidx) {
      Material.push_back(Proto->Spawn(ConfigData[cfgidx]->Config));
    }
  }
}


//==========================================================================
//
//  protosystem::CreateMaterialByName
//
//==========================================================================
material *protosystem::CreateMaterialByName (const char *mname) {
  if (mname && mname[0]) {
    festring ss = CONST_S(mname).UndersToSpacesCopy();
    for (int c1 = 1; c1 < protocontainer<material>::GetSize(); ++c1) {
      const material::prototype *Proto = protocontainer<material>::GetProto(c1);
      if (!Proto) continue; // missing material
      const material::database *const *ConfigData = Proto->GetConfigData();
      int ConfigSize = Proto->GetConfigSize();
      for (int cfgidx = 1; cfgidx < ConfigSize; ++cfgidx) {
        if (ConfigData[cfgidx]->NameStem.EquCI(ss)) {
          material *m1 = Proto->Spawn(ConfigData[cfgidx]->Config);
          if (m1) return m1;
        }
      }
    }
  }
  return 0;
}


//==========================================================================
//
//  protosystem::GetMaterialNamesWithPrefix
//
//  will not clear `list`
//
//==========================================================================
void protosystem::GetMaterialNamesWithPrefix (std::vector<festring> &list, cfestring &pfx) {
  for (int c1 = 1; c1 < protocontainer<material>::GetSize(); ++c1) {
    const material::prototype *Proto = protocontainer<material>::GetProto(c1);
    if (!Proto) continue; // missing material
    const material::database *const *ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    festring pfxspc = pfx.UndersToSpacesCopy();
    for (int cfgidx = 1; cfgidx < ConfigSize; ++cfgidx) {
      if (pfx.IsEmpty() || ConfigData[cfgidx]->NameStem.StartsWithCI(pfxspc)) {
        festring ss = ConfigData[cfgidx]->NameStem.SpacesToUndersCopy();
        list.push_back(ss);
      }
    }
  }
}


//==========================================================================
//
//  protosystem::CreateEveryNormalEnemy
//
//  used in player ghost creation
//
//==========================================================================
void protosystem::CreateEveryNormalEnemy (charactervector &EnemyVector) {
  for (int protocount = 1; protocount < protocontainer<character>::GetSize(); ++protocount) {
    const character::prototype *Proto = protocontainer<character>::GetProto(protocount);
    if (!Proto) continue; // missing character
    const character::database*const* ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
      if (!ConfigData[cfgidx]->IsAbstract &&
          !ConfigData[cfgidx]->IsUnique &&
          ConfigData[cfgidx]->CanBeGenerated)
      {
        //festring ss = CONST_S(Proto->GetClassID());
        //if (ss != "golem" && ss != "skeleton" && ss != "zombie" && ss != "ghost")
        {
          EnemyVector.push_back(Proto->Spawn(ConfigData[cfgidx]->Config,
                                             NO_PIC_UPDATE|NO_EQUIPMENT_PIC_UPDATE|
                                             NO_SEVERED_LIMBS/*k8*/));
        }
      }
    }
  }
}


//==========================================================================
//
//  protosystem::Initialize
//
//==========================================================================
void protosystem::Initialize () {
  typedef item::prototype prototype;
  typedef item::database database;

  protocontainer<item>::Cleanup();

  for (int protocount = 1; protocount < protocontainer<item>::GetSize(); ++protocount) {
    const prototype *Proto = protocontainer<item>::GetProtoData()[protocount];
    if (!Proto) continue; // missing something
    if (!Proto->GetConfigData()) ABORT("Seems that database is missing <%s>!", Proto->GetClassID());
    ItemConfigDataSize += Proto->GetConfigSize();
    if (Proto->GetConfigData()[0]->IsAbstract) --ItemConfigDataSize;
  }

  ItemConfigData = new database*[ItemConfigDataSize];
  int Index = 0;

  for (int protocount = 1; protocount < protocontainer<item>::GetSize(); ++protocount) {
    const prototype *Proto = protocontainer<item>::GetProtoData()[protocount];
    if (!Proto) continue; // missing something
    const database* const *ProtoConfigData = Proto->GetConfigData();
    const database* MainDataBase = *ProtoConfigData;

    if (!MainDataBase->IsAbstract) {
      ItemConfigData[Index++] = const_cast<database*>(MainDataBase);
    }

    int ConfigSize = Proto->GetConfigSize();

    for (int cfgidx = 1; cfgidx < ConfigSize; ++cfgidx) {
      ItemConfigData[Index++] = const_cast<database *>(ProtoConfigData[cfgidx]);
    }
  }

  database **DataBaseBuffer = new database*[ItemConfigDataSize];

  for (int CategoryIndex = 0, Category = 1; CategoryIndex < ITEM_CATEGORIES; ++CategoryIndex, Category <<= 1) {
    sLong TotalPossibility = 0;
    int CSize = 0;

    for (int c = 0; c < ItemConfigDataSize; ++c) {
      database *DataBase = ItemConfigData[c];

      if (DataBase->Category == Category) {
        DataBaseBuffer[CSize++] = DataBase;
        TotalPossibility += DataBase->Possibility;
        DataBase->PartialCategoryPossibilitySum = TotalPossibility;
      }
    }

    ItemCategoryData[CategoryIndex] = new database*[CSize];
    ItemCategorySize[CategoryIndex] = CSize;
    ItemCategoryPossibility[CategoryIndex] = TotalPossibility;
    memmove(ItemCategoryData[CategoryIndex], DataBaseBuffer, CSize*sizeof(database*));
  }

  delete [] DataBaseBuffer;

  for (int c = 0; c < ItemConfigDataSize; ++c) {
    database *DataBase = ItemConfigData[c];
    TotalItemPossibility += DataBase->Possibility;
    DataBase->PartialPossibilitySum = TotalItemPossibility;
  }
}


//==========================================================================
//
//  protosystem::InitCharacterDataBaseFlags
//
//==========================================================================
void protosystem::InitCharacterDataBaseFlags () {
  for (int c1 = 1; c1 < protocontainer<character>::GetSize(); ++c1) {
    const character::prototype* Proto = protocontainer<character>::GetProto(c1);
    if (!Proto) continue; // missing character
    character::database** ConfigData = Proto->ConfigData;
    int ConfigSize = Proto->GetConfigSize();
    for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
      if (!ConfigData[cfgidx]->AutomaticallySeen) {
        ConfigData[cfgidx]->Flags = 0;
      } else {
        ConfigData[cfgidx]->Flags = HAS_BEEN_SEEN;
      }
    }
  }
}


//==========================================================================
//
//  protosystem::SaveCharacterDataBaseFlags
//
//==========================================================================
void protosystem::SaveCharacterDataBaseFlags (outputfile &SaveFile) {
  SaveFile << (int)protocontainer<character>::GetSize();
  for (int c1 = 1; c1 < protocontainer<character>::GetSize(); ++c1) {
    const character::prototype *Proto = protocontainer<character>::GetProto(c1);
    if (!Proto) continue; // missing character
    festring name(CONST_S(Proto->GetClassID()));
    SaveFile << name;
    const character::database* const *ConfigData = Proto->ConfigData;
    int ConfigSize = Proto->GetConfigSize();
    SaveFile << ConfigSize;
    for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
      SaveFile << ConfigData[cfgidx]->Config;
      SaveFile << ConfigData[cfgidx]->Flags;
    }
  }
}


//==========================================================================
//
//  protosystem::LoadCharacterDataBaseFlags
//
//==========================================================================
void protosystem::LoadCharacterDataBaseFlags (inputfile &SaveFile) {
  int ccount = -1;
  SaveFile >> ccount;
  if (ccount != protocontainer<character>::GetSize()) {
    throw new FileError(festring("invalid character flags db prototype count"));
  }
  for (int c1 = 1; c1 < protocontainer<character>::GetSize(); ++c1) {
    const character::prototype* Proto = protocontainer<character>::GetProto(c1);
    if (!Proto) continue; // missing character
    festring name;
    SaveFile >> name;
    if (name != Proto->GetClassID()) {
      throw new FileError(festring("invalid character flags db prototype classid"));
    }
    character::database **ConfigData = Proto->ConfigData;
    int ConfigSize = -1;
    SaveFile >> ConfigSize;
    if (ConfigSize != Proto->GetConfigSize()) {
      throw new FileError(festring("invalid character flags db prototype config size"));
    }
    for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
      int cfg = -1;
      SaveFile >> cfg;
      if (cfg != ConfigData[cfgidx]->Config) {
        throw new FileError(festring("invalid character flags db prototype config id"));
      }
      SaveFile >> ConfigData[cfgidx]->Flags;
    }
  }
}


//==========================================================================
//
//  protosystem::CreateEverySeenCharacter
//
//==========================================================================
void protosystem::CreateEverySeenCharacter (charactervector &Character) {
  for (int c1 = 1; c1 < protocontainer<character>::GetSize(); ++c1) {
    const character::prototype* Proto = protocontainer<character>::GetProto(c1);
    if (!Proto) continue; // missing character
    const character::database*const* ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    for (int cfgidx = 0; cfgidx < ConfigSize; ++cfgidx) {
      if (!ConfigData[cfgidx]->IsAbstract && ConfigData[cfgidx]->Flags & HAS_BEEN_SEEN) {
        character* Char = Proto->Spawn(ConfigData[cfgidx]->Config);
        Char->SetAssignedName(festring::EmptyStr());
        Character.push_back(Char);
      }
    }
  }
}


//==========================================================================
//
//  protosystem::CreateEveryMaterial
//
//==========================================================================
void protosystem::CreateEveryMaterial (std::vector<material*> &Material, const god *God,
                                       ccharacter *Char)
{
  for (int c1 = 1; c1 < protocontainer<material>::GetSize(); ++c1) {
    const material::prototype* Proto = protocontainer<material>::GetProto(c1);
    if (!Proto) continue; // missing material
    const material::database*const* ConfigData = Proto->GetConfigData();
    int ConfigSize = Proto->GetConfigSize();
    for (int cfgidx = 1; cfgidx < ConfigSize; ++cfgidx) {
      if (God->LikesMaterial(ConfigData[cfgidx], Char)) Material.push_back(Proto->Spawn(ConfigData[cfgidx]->Config));
    }
  }
}
