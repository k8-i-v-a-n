/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

/* Compiled through materset.cpp */

//==========================================================================
//
//  rain::rain
//
//==========================================================================
rain::rain (liquid *Liquid, lsquare *LSquareUnder, v2 Speed, int Team, truth OwnLiquid)
  : entity(OwnLiquid ? HAS_BE : 0)
  , Next(0)
  , Drop(0)
  , Liquid(Liquid)
  , LSquareUnder(LSquareUnder)
  , Speed(Speed)
  , SpeedAbs(sLong(sqrt(Speed.GetLengthSquare())))
  , Drops(0)
  , OwnLiquid(OwnLiquid)
  , Team(Team)
{
  Emitation = Liquid->GetEmitation();
  BeCounter = NG_RAND_N(50);
}


//==========================================================================
//
//  rain::~rain
//
//==========================================================================
rain::~rain () {
  delete [] Drop;
  if (OwnLiquid) delete Liquid;
}


//==========================================================================
//
//  rain::DebugGetName
//
//==========================================================================
festring rain::DebugGetName () {
  festring res;
  res << "rain:";
  if (Liquid) {
    res << Liquid->GetName(false, false);
  } else {
    res << "<wtf>";
  }
  return res;
}


//==========================================================================
//
//  rain::Draw
//
//==========================================================================
void rain::Draw (blitdata &BlitData) const {
  sLong Volume = Liquid->GetVolume();
  int Drops = this->Drops;

  if (!Volume && !Drops) return;

  int DropMax = Volume ? Limit<int>(Volume / 50, 1, MAX_RAIN_DROPS) : 0;
  int c;

  if (Drops < DropMax) {
    drop *OldDrop = Drop;
    Drop = new drop[DropMax];

    for (c = 0; c < Drops; c += 1) {
      if (OldDrop[c].MaxAge) {
        Drop[c] = OldDrop[c];
      } else {
        RandomizeDropPos(c);
      }
    }

    delete [] OldDrop;

    for (; Drops < DropMax; Drops += 1) {
      RandomizeDropPos(Drops);
    }
  } else {
    for (c = 0; c < DropMax; c += 1) {
      if (!Drop[c].MaxAge) {
        RandomizeDropPos(c);
      }
    }

    for (; Drops > DropMax && !Drop[Drops - 1].MaxAge; Drops -= 1) {}

    if (!Drops) {
      this->Drops = 0;
      delete [] Drop;
      Drop = 0;
      return;
    }
  }

  col16 Color = Liquid->GetRainColor();

  for (c = 0; c < Drops; c += 1) {
    if (Drop[c].MaxAge) {
      feuLong Age = uShort(GET_TICK()) - Drop[c].StartTick;

      if (Age > Drop[c].MaxAge) {
        Drop[c].MaxAge = 0;
        continue;
      }

      v2 DropPos = v2(Drop[c].StartPos) + (Speed * int(Age) >> 8);

      if (DropPos.X < 0 || DropPos.Y < 0 || DropPos.X >= 16 || DropPos.Y >= 16) {
        Drop[c].MaxAge = 0;
        continue;
      }

      BlitData.Bitmap->AlphaPutPixel(DropPos + BlitData.Dest, Color, BlitData.Luminance, 255);
    }
  }

  this->Drops = Drops;
}


//==========================================================================
//
//  rain::RandomizeDropPos
//
//==========================================================================
void rain::RandomizeDropPos (int I) const {
  Drop[I].StartTick = GET_TICK() - (NG_RAND_4);
  v2 Pos;

  if (Speed.X && (!Speed.Y || NG_RAND_2)) {
    Pos.X = (Speed.X > 0 ? 0 : 15);
    Pos.Y = NG_RAND_16;
  } else {
    Pos.X = NG_RAND_16;
    Pos.Y = (Speed.Y > 0 ? 0 : 15);
  }

  Drop[I].StartPos = Pos;
  int AgeModifier = 5000 / SpeedAbs;
  Drop[I].MaxAge = AgeModifier > 1 ? 1 + NG_RAND_N(AgeModifier) : 1;
}


//==========================================================================
//
//  rain::Be
//
//==========================================================================
void rain::Be () {
  if (++BeCounter < 50) return;
  BeCounter = 0;
  sLong Volume = Liquid->GetVolume();
  if (Volume && !Liquid->IsPowder()) { // gum
    sLong Rand = 5000000/(Volume*SpeedAbs);
    if (OwnLiquid) Rand >>= 3;
    if (Rand < 1 || !NG_RAND_N(Rand)) {
      sLong DropVolume = Min(Volume, 50);
      /* Gum */
      LSquareUnder->SpillFluid((Team == PLAYER_TEAM ? PLAYER : 0),
                               Liquid->SpawnMoreLiquid(DropVolume), true, OwnLiquid);
      if (OwnLiquid) {
        if (Volume == DropVolume) {
          LSquareUnder->RemoveRain(this);
          SendToHell();
        } else {
          Liquid->EditVolume(-DropVolume);
        }
      }
    }
  }
}


//==========================================================================
//
//  rain::Save
//
//==========================================================================
void rain::Save (outputfile &SaveFile) const {
  SaveFile << Liquid << Speed << (uChar)Team;
}


//==========================================================================
//
//  rain::Load
//
//==========================================================================
void rain::Load (inputfile &SaveFile) {
  OwnLiquid = 1;
  LSquareUnder = static_cast<lsquare *>(game::GetSquareInLoad());
  Liquid = static_cast<liquid *>(ReadType(material *, SaveFile));
  Liquid->SetMotherEntity(this);
  Emitation = Liquid->GetEmitation();
  SaveFile >> Speed;
  Team = ReadType(uChar, SaveFile);
  SpeedAbs = sLong(sqrt(Speed.GetLengthSquare()));
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const rain *Rain) {
  if (Rain->HasOwnLiquid()) {
    SaveFile.Put(true);
    Rain->Save(SaveFile);
  } else {
    SaveFile.Put(false);
  }
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, rain *&Rain) {
  if (SaveFile.Get()) {
    Rain = new rain;
    Rain->Load(SaveFile);
  } else {
    Rain = game::ConstructGlobalRain();
  }
  return SaveFile;
}
