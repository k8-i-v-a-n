/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

/* Compiled through roomset.cpp */

//==========================================================================
//
//  roomprototype::roomprototype
//
//==========================================================================
roomprototype::roomprototype (roomspawner Spawner, cchar *ClassID)
  : Spawner(Spawner)
  , ClassID(ClassID)
{
  Index = protocontainer<room>::Add(this);
}


//==========================================================================
//
//  room::Save
//
//==========================================================================
void room::Save (outputfile &SaveFile) const {
  SaveFile << Pos << Size << Index << DivineMaster << MasterID;
}


//==========================================================================
//
//  room::Load
//
//==========================================================================
void room::Load (inputfile &SaveFile) {
  SaveFile >> Pos >> Size >> Index >> DivineMaster >> MasterID;
}


//==========================================================================
//
//  room::FinalProcessForBone
//
//==========================================================================
void room::FinalProcessForBone () {
  if (MasterID) {
    boneidmap::iterator BI = game::GetBoneCharacterIDMap().find(MasterID);
    if (BI != game::GetBoneCharacterIDMap().end()) MasterID = BI->second; else MasterID = 0;
  }
}


//==========================================================================
//
//  roomprototype::SpawnAndLoad
//
//==========================================================================
room *roomprototype::SpawnAndLoad (inputfile &SaveFile) const {
  room *Room = Spawner();
  Room->Load(SaveFile);
  return Room;
}


//==========================================================================
//
//  room::DestroyTerrain
//
//==========================================================================
void room::DestroyTerrain (character *Who) {
  if (Who && MasterIsActive()) Who->Hostility(GetMaster());
  if (Who && Who->IsPlayer() && DivineMaster) {
    game::GetGod(DivineMaster)->AdjustRelation(GetGodRelationAdjustment());
  }
}


//==========================================================================
//
//  room::MasterIsActive
//
//==========================================================================
truth room::MasterIsActive () const {
  character *Master = GetMaster();
  return Master && Master->IsEnabled() && Master->IsConscious();
}


//==========================================================================
//
//  room::GetMaster
//
//==========================================================================
character *room::GetMaster () const {
  feuLong Tick = game::GetTick();
  if (LastMasterSearchTick == Tick) return Master;
  LastMasterSearchTick = Tick;
  Master = game::SearchCharacter(MasterID);
  return Master;
}


//==========================================================================
//
//  NeedToAsk
//
//==========================================================================
static bool NeedToAsk (ccharacter *Infidel) {
  if (!Infidel || !Infidel->IsEnabled()) return false;
  if (Infidel->IsPlayer()) return true;
  if (!PLAYER || !PLAYER->IsEnabled()) return false;
  if (Infidel->GetRelation(PLAYER) == FRIEND) return true;
  return false;
}


//==========================================================================
//
//  room::CheckDestroyTerrain
//
//  returns true if player agrees to continue
//
//==========================================================================
truth room::CheckDestroyTerrain (character *Infidel) {
  if (!MasterIsActive() || Infidel == GetMaster() ||
      GetMaster()->GetRelation(Infidel) == HOSTILE)
  {
    return true;
  }
  bool doit = true;
  if (NeedToAsk(Infidel)) {
    festring msg;
    msg << GetMaster()->CHAR_NAME(DEFINITE) << " might not like this.";
    ADD_MESSAGE("%s", msg.CStr());
    doit = game::TruthQuestion(msg + "\nAre you sure you want to do this?");
  }
  if (doit) {
    DestroyTerrain(Infidel);
    return true;
  }
  return false;
}


//==========================================================================
//
//  room::CheckKickSquare
//
//==========================================================================
truth room::CheckKickSquare (ccharacter *Kicker, const lsquare *LSquare) const {
  if (!AllowKick(Kicker, LSquare)) {
    if (NeedToAsk(Kicker)) {
      ADD_MESSAGE("That would be vandalism.");
      if (!game::TruthQuestion(CONST_S("That would be vandalism.\nDo you still want to do this?"))) return false;
    }
  }
  return true;
}


//==========================================================================
//
//  room::WardIsActive
//
//==========================================================================
truth room::WardIsActive () const {
  olterrain *PossibleWard = GetWard();
  if (!PossibleWard) return false;
  return PossibleWard->IsWard(); //if it is broken, then it will return zero hopefully
}


//==========================================================================
//
//  room::GetWard
//
//==========================================================================
olterrain *room::GetWard () const {
  feuLong Tick = game::GetTick();
  if (LastWardSearchTick == Tick) {
    return Ward;
  } else {
    LastWardSearchTick = Tick;
    std::vector<olterrain*> Found;
    olterrain *OLTerrain;
    v2 RoomPos = /*Room->*/GetPos();
    v2 RoomSize = /*Room->*/GetSize();
    for (int x = RoomPos.X; x < (RoomPos.X + RoomSize.X); ++x) {
      for (int y = RoomPos.Y; y < ( RoomPos.Y + RoomSize.Y); ++y) {
        OLTerrain = game::GetCurrentLevel()->GetLSquare(x,y)->GetOLTerrain();
        if (OLTerrain && OLTerrain->IsWard()) return OLTerrain;
      }
    }
    return 0;
  }
}


//==========================================================================
//
//  room::IsOKToTeleportInto
//
//==========================================================================
truth room::IsOKToTeleportInto () const {
  return !WardIsActive();
}


//==========================================================================
//
//  room::IsOKToDestroyWalls
//
//==========================================================================
truth room::IsOKToDestroyWalls (ccharacter *Infidel) const {
  return
    !MasterIsActive() ||
    Infidel == GetMaster() ||
    GetMaster()->GetRelation(Infidel) == HOSTILE;
}
