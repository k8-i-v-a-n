#ifdef HEADER_PHASE
ROOM(bananadroparea, room) {
public:
  virtual truth PickupItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual truth DropItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual void KickSquare (character *, lsquare *) override;
  virtual truth ConsumeItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual truth AllowDropGifts () const override;
  virtual void TeleportSquare (character *, lsquare *) override;
  virtual truth AllowKick (ccharacter *, const lsquare *) const override;
  virtual void HostileAction (character *) const override;
};


#else


truth bananadroparea::AllowDropGifts () const { return false; }


truth bananadroparea::PickupItem (character *Hungry, item *Item, int Amount, bool check) {
  if (game::GetTeam(NEW_ATTNAM_TEAM)->GetRelation(Hungry->GetTeam()) == HOSTILE) {
    return true;
  }

  if (Hungry->IsPlayer()) {
    if (!Item->IsBanana() && !Item->IsLanternOnWall()) return true;
    if (game::GetLiberator()) return true;
    if (!check) {
      ADD_MESSAGE("That would be stealing.");
      if (game::TruthQuestion(CONST_S("That would be stealing.\nDo you still want to do this?"))) {
        if (!Hungry->TryToStealFromShop(GetMaster(), Item)) {
          ADD_MESSAGE("You caught stealing %s.", Item->CHAR_NAME(UNARTICLED));
          Hungry->GetTeam()->Hostility(game::GetTeam(NEW_ATTNAM_TEAM));
        } else {
          ADD_MESSAGE("You stole %s.", Item->CHAR_NAME(UNARTICLED));
        }
        return true;
      }
    }
  }

  return false;
}


truth bananadroparea::DropItem (character *Dropper, item *Item, int Amount, bool check) {
  if (Dropper->IsPlayer() && Item->IsMangoSeedling() &&
      game::GetTeam(NEW_ATTNAM_TEAM)->GetRelation(Dropper->GetTeam()) != HOSTILE)
  {
    if (game::TruthQuestion(CONST_S("Do you wish to plant the mango seedling at this time?")) && game::TweraifIsFree()) {
      game::TextScreen(CONST_S(
        "You plant the seedling of the Holy Mango Tree in New Attnam.\n"
        "The people of your home village gather around you cheering! Tweraif is\n"
        "now restored to its former glory and you remain there as honourary\n"
        "spiritual leader and hero of the new republic. You ensure that free\n"
        "and fair elections quickly ensue.\n"
        "\n"
        "You are victorious!"));
      game::GetCurrentArea()->SendNewDrawRequest();
      game::DrawEverything();
      festring Msg = CONST_S("restored Tweraif to independence and continued to further adventures");
      PLAYER->AddPolymorphedText(Msg, "while");
      Dropper->AddScoreEntry(Msg, 1.1, false);
      PLAYER->ShowAdventureInfo(Msg);
      game::End(Msg);
    } else {
      if (Dropper->IsPlayer() && !game::TweraifIsFree()) {
        ADD_MESSAGE("You feel that the climate is not quite right for growing mangoes.");
      } else {
        ADD_MESSAGE("You choose not to plant the seedling.");
      }
      return false;
    }
  }

  return (game::GetTeam(NEW_ATTNAM_TEAM)->GetRelation(Dropper->GetTeam()) == HOSTILE ||
    (Dropper->IsPlayer() && ((!Item->IsBanana() && !Item->IsLanternOnWall()) ||
      game::TruthQuestion(CONST_S("Do you wish to donate this item to the town?")))));
}


void bananadroparea::KickSquare (character *Kicker, lsquare *Square) {
  if (game::GetLiberator()) return;
  if (!AllowKick(Kicker, Square) && Kicker->IsPlayer() &&
      game::GetTeam(NEW_ATTNAM_TEAM)->GetRelation(Kicker->GetTeam()) != HOSTILE)
  {
    for (stackiterator i = Square->GetStack()->GetBottom(); i.HasItem(); ++i) {
      if (i->IsBanana() || i->IsLanternOnWall()) {
        ADD_MESSAGE("You have harmed the property of the town!");
        Kicker->GetTeam()->Hostility(game::GetTeam(NEW_ATTNAM_TEAM));
        return;
      }
    }
  }
}


truth bananadroparea::ConsumeItem (character *HungryMan, item *Item, int Amount, bool check) {
  if (game::GetTeam(NEW_ATTNAM_TEAM)->GetRelation(HungryMan->GetTeam()) == HOSTILE) {
    return true;
  }

  if (HungryMan->IsPlayer()) {
    if (!Item->IsBanana() && !Item->IsLanternOnWall()) {
      return true;
    }
    if (game::GetLiberator()) {
      return true;
    }
    if (!check) {
      ADD_MESSAGE("Eating this is forbidden.");
      if (game::TruthQuestion(CONST_S("Eating this is forbidden.\nDo you still want to do this?"))) {
        HungryMan->GetTeam()->Hostility(game::GetTeam(NEW_ATTNAM_TEAM));
        return true;
      }
    }
    return false;
  }

  return HungryMan->IsSumoWrestler();
}


void bananadroparea::TeleportSquare (character *Infidel, lsquare *Square) {
  if (!Infidel || game::GetTeam(NEW_ATTNAM_TEAM)->GetRelation(Infidel->GetTeam()) == HOSTILE) return;
  if (game::GetLiberator()) return;
  for (stackiterator i = Square->GetStack()->GetBottom(); i.HasItem(); ++i) {
    if (i->IsBanana() || i->IsLanternOnWall()) {
      Infidel->GetTeam()->Hostility(game::GetTeam(NEW_ATTNAM_TEAM));
      return;
    }
  }
}


truth bananadroparea::AllowKick (ccharacter *Char, const lsquare *) const {
  return (!Char->IsPlayer() || (game::GetTeam(NEW_ATTNAM_TEAM)->GetRelation(Char->GetTeam()) == HOSTILE));
}


void bananadroparea::HostileAction (character *Guilty) const {
  if (Guilty) Guilty->GetTeam()->Hostility(game::GetTeam(NEW_ATTNAM_TEAM));
}


#endif
