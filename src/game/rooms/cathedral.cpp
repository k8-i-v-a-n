#ifdef HEADER_PHASE
ROOM(cathedral, room) {
public:
  cathedral ();

  virtual void Enter (character *) override;
  virtual truth PickupItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual truth DropItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual void KickSquare (character *, lsquare *) override;
  virtual truth ConsumeItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual truth AllowDropGifts () const override;
  virtual truth Drink (character *) const override;
  virtual truth HasDrinkHandler () const override;
  virtual truth Dip (character *) const override;
  virtual truth HasDipHandler () const override;
  virtual void TeleportSquare (character *, lsquare *) override;
  virtual truth AllowSpoil (citem *) const override;
  virtual int GetGodRelationAdjustment () const override;
  virtual truth AllowKick (ccharacter *, const lsquare *) const override;
  virtual void HostileAction (character *) const override;
  virtual truth AllowAltarPolymorph () const override;
  virtual truth AllowFoodSearch () const override;
  virtual void AddItemEffect (item *) override;

  void SetEntered (truth What);
  character *FindRandomExplosiveReceiver () const;

protected:
  truth Entered;
};


#else


void cathedral::SetEntered (truth What) { Entered = What; }
truth cathedral::AllowDropGifts () const { return false; }
truth cathedral::HasDrinkHandler () const { return true; }
truth cathedral::HasDipHandler () const { return true; }
truth cathedral::AllowSpoil (citem *) const { return false; }
int cathedral::GetGodRelationAdjustment () const { return -150; }
truth cathedral::AllowAltarPolymorph () const { return false; }
truth cathedral::AllowFoodSearch () const { return false; }


cathedral::cathedral () {
  SetEntered(false);
}


void cathedral::Save (outputfile &SaveFile) const {
  room::Save(SaveFile);
  SaveFile << Entered;
}


void cathedral::Load (inputfile &SaveFile) {
  room::Load(SaveFile);
  SaveFile >> Entered;
}


void cathedral::Enter (character *Visitor) {
  if (Visitor->IsPlayer() && !Entered) {
    ADD_MESSAGE("The majestetic Cathedral of Valpurus looms before you. You watch it with utter respect.");
    Entered = true;
  }
}


truth cathedral::PickupItem (character *Visitor, item *Item, int Amount, bool check) {
  if (game::GetStoryState() == 2 ||
      game::GetTeam(ATTNAM_TEAM)->GetRelation(Visitor->GetTeam()) == HOSTILE)
  {
    return true;
  }

  if (Visitor->IsPlayer()) {
    if (Item->IsHeadOfElpuri() || Item->IsGoldenEagleShirt() || Item->IsPetrussNut() ||
        !Item->GetTruePrice() || Item->IsEncryptedScroll())
    {
      return true;
    }
    if (!check) {
      ADD_MESSAGE("Picking up property of the Cathedral is prohibited.");
      if (game::TruthQuestion(CONST_S("Picking up property of the Cathedral is prohibited.\nDo you still want to do this?"))) {
        if (!Visitor->TryToStealFromShop(GetMaster(), Item)) {
          ADD_MESSAGE("You caught stealing %s from the Cathedral.", Item->CHAR_NAME(UNARTICLED));
          Visitor->GetTeam()->Hostility(game::GetTeam(ATTNAM_TEAM));
        } else {
          ADD_MESSAGE("You stole %s from the Cathedral.", Item->CHAR_NAME(UNARTICLED));
        }
        return true;
      }
    }
  }

  return false;
}


truth cathedral::DropItem (character *Visitor, item *Item, int Amount, bool check) {
  if (game::GetStoryState() == 2 ||
      game::GetTeam(ATTNAM_TEAM)->GetRelation(Visitor->GetTeam()) == HOSTILE)
  {
    return true;
  }

  if (Visitor->IsPlayer()) {
    if (Item->IsHeadOfElpuri() || Item->IsGoldenEagleShirt() || Item->IsPetrussNut() ||
        Item->IsTheAvatar() || Item->IsEncryptedScroll())
    {
      if (!check) {
        ADD_MESSAGE("Donating this to the Cathedral wouldn't be wise. You may still need it.");
      }
      return false;
    }
    if (check || game::TruthQuestion(CONST_S("Do you wish to donate this item to the Cathedral?"))) {
      return true;
    }
  }

  return false;
}


void cathedral::KickSquare (character *Kicker, lsquare *Square) {
  if (!AllowKick(Kicker, Square) && Kicker->IsPlayer() && game::GetStoryState() != 2 &&
      game::GetTeam(ATTNAM_TEAM)->GetRelation(Kicker->GetTeam()) != HOSTILE) {
    ADD_MESSAGE("You have harmed the property of the Cathedral!");
    Kicker->GetTeam()->Hostility(game::GetTeam(ATTNAM_TEAM));
  }
}


truth cathedral::ConsumeItem (character *HungryMan, item *, int Amount, bool check) {
  if (game::GetStoryState() == 2 ||
      (game::GetTeam(ATTNAM_TEAM)->GetRelation(HungryMan->GetTeam()) == HOSTILE))
  {
    return true;
  }

  if (HungryMan->IsPlayer()) {
    if (!check) {
      ADD_MESSAGE("Eating the property of the Cathedral is forbidden.");
      if (game::TruthQuestion(CONST_S("Eating the property of the Cathedral is forbidden.\nDo you still want to do this?"))) {
        HungryMan->GetTeam()->Hostility(game::GetTeam(ATTNAM_TEAM));
        return true;
      }
    }
  }

  return false;
}


truth cathedral::Drink (character *Thirsty) const {
  if (game::GetStoryState() == 2 ||
      game::GetTeam(ATTNAM_TEAM)->GetRelation(Thirsty->GetTeam()) == HOSTILE)
  {
    return game::TruthQuestion(CONST_S("Do you want to drink?"));
  }

  if (Thirsty->IsPlayer()) {
    ADD_MESSAGE("Drinking property of the Cathedral is prohibited.");
    if (game::TruthQuestion(CONST_S("Drinking property of the Cathedral is prohibited.\nDo you still want to do this?"))) {
      Thirsty->GetTeam()->Hostility(game::GetTeam(ATTNAM_TEAM));
      return true;
    }
  }

  return false;
}


void cathedral::TeleportSquare (character *Teleporter, lsquare *Square) {
  if (game::GetStoryState() == 2 || !Teleporter ||
      game::GetTeam(ATTNAM_TEAM)->GetRelation(Teleporter->GetTeam()) == HOSTILE)
  {
    return;
  }
  if (Teleporter->IsPlayer() && Square->GetStack()->GetItems()) {
    ADD_MESSAGE("You have done unnatural things to the property of the Cathedral!");
    Teleporter->GetTeam()->Hostility(game::GetTeam(ATTNAM_TEAM));
  }
}


truth cathedral::Dip (character *Thirsty) const {
  if (game::GetStoryState() == 2 || game::GetTeam(ATTNAM_TEAM)->GetRelation(Thirsty->GetTeam()) == HOSTILE) return true;
  if (Thirsty->IsPlayer()) {
    /*FIXME: What if it's not water? */
    ADD_MESSAGE("Stealing the precious water of the Cathedral is prohibited.");
    if (game::TruthQuestion(CONST_S("Stealing the precious water of the Cathedral is prohibited.\nAre you sure you want to dip?"))) {
      Thirsty->GetTeam()->Hostility(game::GetTeam(ATTNAM_TEAM));
      return true;
    }
  }
  return false;
}


truth cathedral::AllowKick (ccharacter *Char, const lsquare *LSquare) const {
  return (game::GetTeam(ATTNAM_TEAM)->GetRelation(Char->GetTeam()) == HOSTILE || !LSquare->GetStack()->GetItems());
}


void cathedral::HostileAction (character *Guilty) const {
  if (game::GetStoryState() != 2 && Guilty) Guilty->GetTeam()->Hostility(game::GetTeam(ATTNAM_TEAM));
}


void cathedral::AddItemEffect (item *Dropped) {
  truth SeenBeforeTeleport = Dropped->CanBeSeenByPlayer();
  character *KamikazeDwarf = FindRandomExplosiveReceiver();
  if (!Dropped->IsKamikazeWeapon(KamikazeDwarf)) return;
  if (KamikazeDwarf) {
    Dropped->MoveTo(KamikazeDwarf->GetStack());
    if (KamikazeDwarf->CanBeSeenByPlayer()) {
      if (SeenBeforeTeleport) {
        ADD_MESSAGE("%s disappears and reappears in %s's inventory.",
                    Dropped->GetName(DEFINITE).CStr(), KamikazeDwarf->GetName(DEFINITE).CStr());
      } else {
        ADD_MESSAGE("%s appears in %s's inventory.",
                    Dropped->GetName(DEFINITE).CStr(), KamikazeDwarf->GetName(DEFINITE).CStr());
      }
    } else if (SeenBeforeTeleport) {
      ADD_MESSAGE("%s disappears.", Dropped->GetName(DEFINITE).CStr());
    }
  } else {
    /* position is in kamikaze dwarf room */
    Dropped->RemoveFromSlot();
    game::GetCurrentLevel()->GetLSquare(18,21)->GetStack()->AddItem(Dropped, false);
    if (Dropped->CanBeSeenByPlayer()) {
      if (SeenBeforeTeleport) {
        ADD_MESSAGE("%s disappears and reappears in the kamikaze dwarf room.",
                    Dropped->GetName(DEFINITE).CStr());
      } else {
        ADD_MESSAGE("%s appears in the kamikaze dwarf room.",
                    Dropped->GetName(DEFINITE).CStr());
      }
    } else if (SeenBeforeTeleport) {
      ADD_MESSAGE("%s disappears.", Dropped->GetNameSingular().CStr());
    }
  }
}


character *cathedral::FindRandomExplosiveReceiver () const {
  std::vector<character *> ListOfDwarfs;
  for (std::list<character *>::const_iterator i = game::GetTeam(ATTNAM_TEAM)->GetMember().begin(); i != game::GetTeam(ATTNAM_TEAM)->GetMember().end(); ++i) {
    if ((*i)->IsEnabled() && (*i)->IsKamikazeDwarf()) ListOfDwarfs.push_back(*i);
  }
  if (ListOfDwarfs.empty()) return 0;
  return ListOfDwarfs[RAND_N(ListOfDwarfs.size())];
}


#endif
