#ifdef HEADER_PHASE
ROOM(guardedarea, room) {
public:
  guardedarea ();
  virtual void BeforeHit (character *Attacker, character *Victim) override;
};


#else

//#define RGRDA_DEBUG

//==========================================================================
//
//  guardedarea::guardedarea
//
//==========================================================================
guardedarea::guardedarea () {
  #ifdef RGRDA_DEBUG
  ConLogf(">>> guardedarea::guardedarea() <<<");
  #endif
}


//==========================================================================
//
//  guardedarea::BeforeHit
//
//==========================================================================
void guardedarea::BeforeHit (character *Attacker, character *Victim) {
  IvanAssert(Attacker);
  IvanAssert(Victim);
  IvanAssert(Attacker != Victim);
  #ifdef RGRDA_DEBUG
  ConLogf("guardedarea::BeforeHit: Attacker='%s'; Victim='%s'",
          Attacker->DebugLogName().CStr(), Victim->DebugLogName().CStr());
  #endif
  // the player is allowed to fight in a shop, but monsters aren't
  if (Attacker->IsPlayer()) return;
  if (Attacker->GetTeam() == PLAYER->GetTeam()) return;
  ccharacter *Master = GetMaster();
  if (!Master) {
    #ifdef RGRDA_DEBUG
    ConLogf("guardedarea::BeforeHit:   no master!");
    #endif
    return;
  }
  if (!MasterIsActive()) {
    #ifdef RGRDA_DEBUG
    ConLogf("guardedarea::BeforeHit:   master is inactive!");
    #endif
    return;
  }
  if (Attacker->GetTeam() != game::GetTeam(SHOP_PERPETRATOR_TEAM)) {
    if (Victim->GetTeam() != Master->GetTeam()) {
      // monster?
      if (Attacker->GetTeam() != game::GetTeam(MONSTER_TEAM)) {
        #ifdef RGRDA_DEBUG
        ConLogf("guardedarea::BeforeHit:   attacker is not a monster!");
        #endif
        return;
      }
      if (Master->GetRelation(Victim) == HOSTILE) {
        #ifdef RGRDA_DEBUG
        ConLogf("guardedarea::BeforeHit:   master is hostile to victim!");
        #endif
        return; // don't care
      }
      if (Master->GetTeam() == Attacker->GetTeam()) {
        #ifdef RGRDA_DEBUG
        ConLogf("guardedarea::BeforeHit:   master is in the same team as attacker!");
        #endif
        return; // don't care
      }
      #ifdef RGRDA_DEBUG
      ConLogf("guardedarea::BeforeHit: ...attack the attacker!");
      #endif
    } else {
      // the master and the victim are in the same team
    }
    // fighting in guarded areas is prohibited!
    Attacker->ChangeTeam(game::GetTeam(SHOP_PERPETRATOR_TEAM));
  }
}


#endif
