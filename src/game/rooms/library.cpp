#ifdef HEADER_PHASE
ROOM(library, room) {
public:
  virtual void Enter (character *) override;
  virtual truth PickupItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual truth DropItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual void KickSquare (character *, lsquare *) override;
  virtual truth ConsumeItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual truth AllowDropGifts () const override;
  virtual void TeleportSquare (character *, lsquare *) override;
  virtual truth AllowKick (ccharacter *, const lsquare *) const override;
  virtual void HostileAction (character *) const override;
  virtual truth IsShopActive (ccharacter *Customer) const override;
  virtual sLong GetItemBuyPrice (ccharacter *Customer, item *ForSale, int Amount=1) override;
  virtual sLong GetItemSellPrice (ccharacter *Customer, item *ForSale, int Amount=1) override;
  virtual truth CanBeSoldToShop (ccharacter *Customer, item *ForSale) override;
};


#else


//==========================================================================
//
//  library::AllowDropGifts
//
//==========================================================================
truth library::AllowDropGifts () const {
  return false;
}


//==========================================================================
//
//  library::IsShopActive
//
//==========================================================================
truth library::IsShopActive (ccharacter *Customer) const {
  ccharacter *Master = GetMaster();
  if (MasterIsActive() && Master && Customer != Master &&
      (!Customer || Master->GetRelation(Customer) != HOSTILE))
  {
    return true;
  }
  return false;
}


//==========================================================================
//
//  library::GetItemBuyPrice
//
//==========================================================================
sLong library::GetItemBuyPrice (ccharacter *Customer, item *ForSale, int Amount) {
  IvanAssert(Customer);
  IvanAssert(ForSale);
  IvanAssert(Amount > 0);
  sLong Price = 0;
  if (IsShopActive(Customer)) {
    Price = ForSale->GetTruePrice();
    if (Price) {
      Price = Price * Amount * 100 / (100 + Customer->GetAttribute(CHARISMA));
    }
  }
  return Price;
}


//==========================================================================
//
//  library::CanBeSoldToShop
//
//==========================================================================
truth library::CanBeSoldToShop (ccharacter *Customer, item *ForSale) {
  if (ForSale && IsShopActive(Customer)) {
    if (ForSale->IsHeadOfElpuri() || ForSale->IsGoldenEagleShirt() || ForSale->IsPetrussNut() ||
        ForSale->IsTheAvatar() || ForSale->IsEncryptedScroll())
    {
      return false;
    }
    if (ForSale->CanBeSoldInLibrary(GetMaster())) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  library::GetItemSellPrice
//
//==========================================================================
sLong library::GetItemSellPrice (ccharacter *Customer, item *ForSale, int Amount) {
  IvanAssert(Customer);
  IvanAssert(ForSale);
  IvanAssert(Amount > 0);
  sLong Price = 0;
  if (IsShopActive(Customer)) {
    Price = ForSale->GetTruePrice();
    if (Price) {
      Price = Price * Amount * (100 + Customer->GetAttribute(CHARISMA)) / 400;
    }
  }
  return Price;
}


//==========================================================================
//
//  library::Enter
//
//==========================================================================
void library::Enter (character *Customer) {
  if (Customer->IsPlayer()) {
    if (MasterIsActive()) {
      if (GetMaster()->GetRelation(Customer) != HOSTILE && Customer->CanBeSeenBy(GetMaster())) {
        if (GetMaster()->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s looks at you suspiciously. \"Feel free to open the shelves, "
                      "but be quiet in the library!\" %s whispers.",
                      GetMaster()->CHAR_NAME(DEFINITE),
                      GetMaster()->GetPersonalPronoun().CStr());
        } else {
          ADD_MESSAGE("You feel somebody staring at you.");
        }
      }
    } else {
      ADD_MESSAGE("The library appears to be deserted.");
    }
  }
}


//==========================================================================
//
//  library::PickupItem
//
//==========================================================================
truth library::PickupItem (character *Customer, item *ForSale, int Amount, bool check) {
  if (!MasterIsActive() || Customer == GetMaster() ||
      GetMaster()->GetRelation(Customer) == HOSTILE)
  {
    return true;
  }

  if (ForSale->IsLanternOnWall()) {
    if (!check) {
      ADD_MESSAGE("\"I'd appreciate it if you left my light sources alone, thank you!\"");
    }
    return false;
  }

  const sLong Price = GetItemBuyPrice(Customer, ForSale, Amount);

  if (!Customer->IsPlayer()) {
    // monsters can buy items too
    if (Customer->CanBeSeenBy(GetMaster()) && Customer->GetMoney() >= Price) {
      if (!check) {
        if (Customer->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s buys %s.", Customer->CHAR_NAME(DEFINITE),
                      ForSale->GetName(INDEFINITE, Amount).CStr());
        }
        Customer->EditMoney(-Price);
        GetMaster()->EditMoney(Price);
        Customer->EditDealExperience(Price);
      }
      return true;
    }
    return false;
  }

  // the customer is the player
  if (Customer->CanBeSeenBy(GetMaster())) {
    if (ForSale->IsHeadOfElpuri() || ForSale->IsGoldenEagleShirt() || ForSale->IsPetrussNut() ||
        ForSale->IsTheAvatar() || ForSale->IsEncryptedScroll())
    {
      if (!check) {
        ADD_MESSAGE("\"I think it is yours. Take it.\"");
      }
      return true;
    }
    if (!Price || !ForSale->CanBeSoldInLibrary(GetMaster())) {
      if (!check) {
        ADD_MESSAGE("\"Thank you for cleaning that junk out of my floor.\"");
      }
      return true;
    }

    if (Customer->GetMoney() >= Price) {
      if (check) return true;
      festring ss;
      if (Amount == 1) {
        ss << "That " << ForSale->CHAR_NAME(UNARTICLED);
        ss << " costs \1Y" << Price << "\2 gold pieces.";
      } else {
        ss << "Those " << Amount << " " << ForSale->CHAR_NAME(PLURAL);
        ss << " cost \1Y" << Price << "\2 gold pieces.";
      }
      ADD_MESSAGE("\"Ah! %s No haggling, please.\"", ss.CStr());
      if (game::TruthQuestion(CONST_S("\"") + ss + "\"\nDo you accept this deal?")) {
        Customer->EditMoney(-Price);
        GetMaster()->EditMoney(Price);
        Customer->EditDealExperience(Price);
        return true;
      }
      return false;
    } else {
      if (!check) {
        if (Amount == 1) {
          ADD_MESSAGE("\"Don't touch that %s, beggar! It is worth \1Y%d\2 gold pieces!\"",
                      ForSale->CHAR_NAME(UNARTICLED), Price);
        } else {
          ADD_MESSAGE("\"Don't touch those %s, beggar! They are worth \1Y%d\2 gold pieces!\"",
                      ForSale->CHAR_NAME(PLURAL), Price);
        }
      }
      return false;
    }
  }

  if (!check) {
    if (game::TruthQuestion(CONST_S("Are you sure you want to commit this thievery?"))) {
      if (!Customer->TryToStealFromShop(GetMaster(), ForSale)) {
        ADD_MESSAGE("You caught stealing %s from the library.", ForSale->CHAR_NAME(UNARTICLED));
        Customer->Hostility(GetMaster());
      } else {
        ADD_MESSAGE("You stole %s from the library.", ForSale->CHAR_NAME(UNARTICLED));
      }
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  library::DropItem
//
//==========================================================================
truth library::DropItem (character *Customer, item *ForSale, int Amount, bool check) {
  if (!MasterIsActive() || Customer == GetMaster() ||
      GetMaster()->GetRelation(Customer) == HOSTILE)
  {
    return true;
  }

  sLong Price = GetItemSellPrice(Customer, ForSale, Amount);

  if (!Customer->IsPlayer()) {
    if (Price && Customer->CanBeSeenBy(GetMaster()) && GetMaster()->GetMoney() >= Price) {
      if (!check) {
        if (Customer->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s sells %s.", Customer->CHAR_NAME(DEFINITE),
                      ForSale->GetName(INDEFINITE, Amount).CStr());
        }
        Customer->SetMoney(Customer->GetMoney() + Price);
        GetMaster()->SetMoney(GetMaster()->GetMoney() - Price);
        Customer->EditDealExperience(Price);
      }
      return true;
    }
    return false;
  }

  if (Customer->CanBeSeenBy(GetMaster())) {
    if (ForSale->IsHeadOfElpuri() || ForSale->IsGoldenEagleShirt() || ForSale->IsPetrussNut() ||
        ForSale->IsTheAvatar() || ForSale->IsEncryptedScroll())
    {
      if (!check) {
        ADD_MESSAGE("\"Oh no! You need it far more than I!\"");
      }
      return false;
    }

    if (!Price || !ForSale->CanBeSoldInLibrary(GetMaster())) {
      if (!check) {
        ADD_MESSAGE("\"Sorry, but I don't think %s into my collection.\"",
                    (Amount == 1 ? "that fits" : "those fit"));
      }
      return false;
    }

    if (GetMaster()->GetMoney()) {
      if (check) return true;
      festring ss;
      if (GetMaster()->GetMoney() < Price) {
        Price = GetMaster()->GetMoney();
      }
      if (Amount == 1) {
        ss << "\"What an interesting " << ForSale->CHAR_NAME(UNARTICLED);
      } else {
        ss << "\"What an interesting collection of " << Amount;
        ss << " " << ForSale->CHAR_NAME(PLURAL);
      }
      ss << ". I'll pay \1Y" << Price << "\2 gold pieces for it.\"";
      ADD_MESSAGE("%s", ss.CStr());
      if (game::TruthQuestion(ss + "\nDo you want to sell " + (Amount == 1 ? "this item" : "these items") + "?")) {
        Customer->EditMoney(Price);
        GetMaster()->EditMoney(-Price);
        Customer->EditDealExperience(Price);
        return true;
      }
      return false;
    } else {
      if (!check) {
        ADD_MESSAGE("\"I would pay you \1Y%d\2 gold pieces for %s, but I'm temporarily short of cash. Sorry.\"",
                    Price, Amount == 1 ? "it" : "them");
      }
      return false;
    }
  } else {
    if (!check) {
      ADD_MESSAGE("The librarian doesn't see you, so you cannot trade with him.");
      return game::TruthQuestion(CONST_S("Still drop ")+(Amount == 1 ? "this item" : "these items")+"?");
    } else {
      return true;
    }
  }
}


void library::KickSquare (character *Infidel, lsquare *Square) {
  if (!AllowKick(Infidel, Square)) {
    ADD_MESSAGE("\"You book vandal!\"");
    Infidel->Hostility(GetMaster());
  }
}


truth library::ConsumeItem (character *, item *, int, bool) {
  return true;
}


void library::TeleportSquare (character *Infidel, lsquare *Square) {
  if (Square->GetStack()->GetItems() && MasterIsActive() &&
      Infidel && Infidel != GetMaster() &&
      GetMaster()->GetRelation(Infidel) != HOSTILE &&
      Square->CanBeSeenBy(GetMaster()))
  {
    ADD_MESSAGE("\"You book hater!\"");
    Infidel->Hostility(GetMaster());
  }
}


truth library::AllowKick (ccharacter *Char, const lsquare *LSquare) const {
  return
    !LSquare->GetStack()->GetItems() ||
    !MasterIsActive() || Char == GetMaster() ||
    GetMaster()->GetRelation(Char) == HOSTILE ||
    !LSquare->CanBeSeenBy(GetMaster());
}


void library::HostileAction (character *Guilty) const {
  if (MasterIsActive() && Guilty && Guilty != GetMaster() &&
      GetMaster()->GetRelation(Guilty) != HOSTILE &&
      Guilty->CanBeSeenBy(GetMaster())) {
    ADD_MESSAGE("\"You infidel!\"");
    Guilty->Hostility(GetMaster());
  }
}


#endif
