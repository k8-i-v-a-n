#ifdef HEADER_PHASE
ROOM(ownedarea, room) {
public:
  ownedarea ();
  virtual truth PickupItem (character *Who, item *Item, int Amount, bool checkOnly=false);
  virtual truth DropItem (character *Who, item *Item, int Amount, bool checkOnly=false);
  virtual void KickSquare (character *, lsquare *);
  virtual truth ConsumeItem (character *Who, item *Item, int Amount, bool checkOnly=false);
  virtual truth AllowDropGifts () const;
  virtual truth Drink (character *) const;
  virtual truth HasDrinkHandler () const;
  virtual truth Dip (character *) const;
  virtual truth HasDipHandler () const;
  virtual void TeleportSquare (character *, lsquare *);
  virtual truth AllowSpoil (citem *) const;
  virtual truth AllowKick (ccharacter *, const lsquare *) const;
  virtual void HostileAction (character *) const;
  virtual truth AllowAltarPolymorph () const;
  virtual truth AllowFoodSearch () const;
  virtual void AddItemEffect (item *);

  void SetEntered (truth What);
  character *FindRandomExplosiveReceiver () const;

protected:
  truth Entered;
};


#else


//==========================================================================
//
//  ownedarea::ownedarea
//
//==========================================================================
ownedarea::ownedarea () {
  SetEntered(false);
}


void ownedarea::SetEntered (truth What) { Entered = What; }
truth ownedarea::AllowDropGifts () const { return false; }
truth ownedarea::HasDrinkHandler () const { return true; }
truth ownedarea::HasDipHandler () const { return true; }
truth ownedarea::AllowSpoil (citem *) const { return false; }
truth ownedarea::AllowAltarPolymorph () const { return false; }
truth ownedarea::AllowFoodSearch () const { return false; }


//==========================================================================
//
//  ownedarea::PickupItem
//
//==========================================================================
truth ownedarea::PickupItem (character *Visitor, item *Item, int Amount, bool check) {
  if (!MasterIsActive() || Visitor == GetMaster() ||
      GetMaster()->GetRelation(Visitor) == HOSTILE)
  {
    return true;
  }

  if (Visitor->IsPlayer()) {
    if (Item->IsQuestItem()) return true;
    if (!check) {
      ADD_MESSAGE("Picking up private property is prohibited.");
      if (game::TruthQuestion(CONST_S("Picking up private property is prohibited.\nDo you still want to do this?"))) {
        if (!Visitor->TryToStealFromShop(GetMaster(), Item)) {
          ADD_MESSAGE("You caught stealing %s.", Item->CHAR_NAME(UNARTICLED));
          Visitor->Hostility(GetMaster());
        } else {
          ADD_MESSAGE("You stole %s.", Item->CHAR_NAME(UNARTICLED));
        }
        return true;
      }
    }
  }

  return false;
}


//==========================================================================
//
//  ownedarea::DropItem
//
//==========================================================================
truth ownedarea::DropItem (character *Visitor, item *Item, int Amount, bool check) {
  if (!MasterIsActive() || Visitor == GetMaster() ||
      GetMaster()->GetRelation(Visitor) == HOSTILE)
  {
    return true;
  }

  if (Visitor->IsPlayer()) {
    if (Item->IsQuestItem()) {
      if (!check) {
        ADD_MESSAGE("Donating this item wouldn't be wise. You may still need it.");
      }
      return false;
    }

    if (check || game::TruthQuestion(CONST_S("Do you wish to donate this item?"))) {
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  ownedarea::KickSquare
//
//==========================================================================
void ownedarea::KickSquare (character *Kicker, lsquare *Square) {
  if (!AllowKick(Kicker, Square) && Kicker->IsPlayer() &&
      (GetMaster()->GetRelation(Kicker) != HOSTILE))
  {
    ADD_MESSAGE("You have harmed a private property!");
    Kicker->Hostility(GetMaster());
  }
}


//==========================================================================
//
//  ownedarea::ConsumeItem
//
//==========================================================================
truth ownedarea::ConsumeItem (character *HungryMan, item *Item, int Amount, bool check) {
  if (!MasterIsActive() || HungryMan == GetMaster() ||
      GetMaster()->GetRelation(HungryMan) == HOSTILE)
  {
    return true;
  }

  if (HungryMan->IsPlayer()) {
    if (!check) {
      ADD_MESSAGE("Eating private property is forbidden.");
      if (game::TruthQuestion(CONST_S("Eating private property is forbidden.\nDo you still want to do this?"))) {
        HungryMan->Hostility(GetMaster());
        return true;
      }
    }
  }

  return false;
}


//==========================================================================
//
//  ownedarea::Drink
//
//==========================================================================
truth ownedarea::Drink (character *Thirsty) const {
  if (!MasterIsActive() || Thirsty == GetMaster() ||
      GetMaster()->GetRelation(Thirsty) == HOSTILE)
  {
    return game::TruthQuestion(CONST_S("Do you want to drink?"));
  }

  if (Thirsty->IsPlayer()) {
    ADD_MESSAGE("Drinking private property is prohibited.");
    if (game::TruthQuestion(CONST_S("Drinking private property is prohibited.\nDo you still want to do this?"))) {
      Thirsty->Hostility(GetMaster());
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  ownedarea::TeleportSquare
//
//==========================================================================
void ownedarea::TeleportSquare (character *Infidel, lsquare *Square) {
  if (Square->GetStack()->GetItems() && MasterIsActive() &&
      Infidel && Infidel != GetMaster() &&
      GetMaster()->GetRelation(Infidel) != HOSTILE)
  {
    ADD_MESSAGE("\"Thief! Thief!\"");
    Infidel->Hostility(GetMaster());
  }
}


//==========================================================================
//
//  ownedarea::Dip
//
//==========================================================================
truth ownedarea::Dip (character *Thirsty) const {
  if (!MasterIsActive() || Thirsty == GetMaster() ||
      GetMaster()->GetRelation(Thirsty) == HOSTILE)
  {
    return true;
  }

  if (Thirsty->IsPlayer()) {
    /* What if it's not water? */
    ADD_MESSAGE("Stealing water is prohibited.");
    if (game::TruthQuestion(CONST_S("Stealing water is prohibited.\nAre you sure you want to dip?"))) {
      Thirsty->Hostility(GetMaster());
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  ownedarea::AllowKick
//
//==========================================================================
truth ownedarea::AllowKick (ccharacter *Char, const lsquare *LSquare) const {
  return (!LSquare->GetStack()->GetItems() || !MasterIsActive()
          || Char == GetMaster() || GetMaster()->GetRelation(Char) == HOSTILE
          || !LSquare->CanBeSeenBy(GetMaster()));
}


//==========================================================================
//
//  ownedarea::HostileAction
//
//==========================================================================
void ownedarea::HostileAction (character *Guilty) const {
  if (MasterIsActive() && Guilty && Guilty != GetMaster() &&
      GetMaster()->GetRelation(Guilty) != HOSTILE)
  {
    ADD_MESSAGE("\"You vandal!\"");
    Guilty->Hostility(GetMaster());
  }
}


//==========================================================================
//
//  ownedarea::AddItemEffect
//
//==========================================================================
void ownedarea::AddItemEffect (item *Dropped) {
  truth SeenBeforeTeleport = Dropped->CanBeSeenByPlayer();
  character *KamikazeDwarf = FindRandomExplosiveReceiver();

  if (!Dropped->IsKamikazeWeapon(KamikazeDwarf)) return;

  if (KamikazeDwarf) {
    Dropped->MoveTo(KamikazeDwarf->GetStack());

    if (KamikazeDwarf->CanBeSeenByPlayer()) {
      if (SeenBeforeTeleport) {
        ADD_MESSAGE("%s disappears and reappears in %s's inventory.",
                    Dropped->GetName(DEFINITE).CStr(),
                    KamikazeDwarf->GetName(DEFINITE).CStr());
      } else {
        ADD_MESSAGE("%s appears in %s's inventory.",
                    Dropped->GetName(DEFINITE).CStr(),
                    KamikazeDwarf->GetName(DEFINITE).CStr());
      }
    } else if (SeenBeforeTeleport) {
      ADD_MESSAGE("%s disappears.", Dropped->GetName(DEFINITE).CStr());
    }
  } else {
    if (SeenBeforeTeleport) {
      ADD_MESSAGE("%s flickers for a moment.", Dropped->GetNameSingular().CStr());
    }
  }
}


//==========================================================================
//
//  ownedarea::FindRandomExplosiveReceiver
//
//==========================================================================
character *ownedarea::FindRandomExplosiveReceiver () const {
  if (!MasterIsActive()) return 0;

  std::vector<character *> ListOfDwarfs;

  for (character *p : GetMaster()->GetTeam()->GetMember()) {
    if (p->IsEnabled() && p->IsKamikazeDwarf()) {
      ListOfDwarfs.push_back(p);
    }
  }

  if (ListOfDwarfs.empty()) {
    return 0;
  } else {
    return ListOfDwarfs[RAND_N(ListOfDwarfs.size())];
  }
}


#endif
