#ifdef HEADER_PHASE
ROOM(shop, room) {
public:
  virtual void Enter (character *) override;
  virtual truth PickupItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual truth DropItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual void KickSquare (character *, lsquare *) override;
  virtual truth ConsumeItem (character *Who, item *Item, int Amount, bool checkOnly=false) override;
  virtual truth AllowDropGifts () const override;
  virtual void TeleportSquare (character *, lsquare *) override;
  virtual truth AllowSpoil (citem *) const override;
  virtual truth AllowKick (ccharacter *, const lsquare *) const override;
  virtual void HostileAction (character *) const override;
  virtual truth AllowFoodSearch () const override;
  virtual void ReceiveVomit (character *) override;
  virtual truth IsShopActive (ccharacter *Customer) const override;
  virtual sLong GetItemBuyPrice (ccharacter *Customer, item *ForSale, int Amount=1) override;
  virtual sLong GetItemSellPrice (ccharacter *Customer, item *ForSale, int Amount=1) override;
  virtual truth CanBeSoldToShop (ccharacter *Customer, item *ForSale) override;
  virtual void BeforeHit (character *Attacker, character *Victim) override;
};


#else


//==========================================================================
//
//  shop::AllowDropGifts
//
//==========================================================================
truth shop::AllowDropGifts () const {
  return false;
}


//==========================================================================
//
//  shop::AllowFoodSearch
//
//==========================================================================
truth shop::AllowFoodSearch () const {
  return false;
}


//==========================================================================
//
//  shop::BeforeHit
//
//==========================================================================
void shop::BeforeHit (character *Attacker, character *Victim) {
  IvanAssert(Attacker);
  IvanAssert(Victim);
  IvanAssert(Attacker != Victim);
  // the player is allowed to fight in a shop, but monsters aren't
  if (Attacker->IsPlayer()) return;
  if (Attacker->GetTeam() == PLAYER->GetTeam()) return;
  ccharacter *Master = GetMaster();
  if (!Master) return;
  if (!MasterIsActive()) return;
  if (Master->GetRelation(Victim) == HOSTILE) return; // don't care
  // Merka?
  if (Master->GetConfig() != ELPURI_CAVE) return;
  // monster?
  if (Attacker->GetTeam() != game::GetTeam(SHOP_PERPETRATOR_TEAM)) {
    if (Victim->GetTeam() != Master->GetTeam()) {
      if (Attacker->GetTeam() != game::GetTeam(MONSTER_TEAM)) return;
    }
    // fighting in shops is prohibited!
    Attacker->ChangeTeam(game::GetTeam(SHOP_PERPETRATOR_TEAM));
  }
}


//==========================================================================
//
//  shop::IsShopActive
//
//==========================================================================
truth shop::IsShopActive (ccharacter *Customer) const {
  ccharacter *Master = GetMaster();
  if (MasterIsActive() && Master && Customer != Master &&
      (!Customer || Master->GetRelation(Customer) != HOSTILE))
  {
    return true;
  }
  return false;
}


//==========================================================================
//
//  shop::GetItemBuyPrice
//
//==========================================================================
sLong shop::GetItemBuyPrice (ccharacter *Customer, item *ForSale, int Amount) {
  IvanAssert(Customer);
  IvanAssert(ForSale);
  IvanAssert(Amount > 0);
  sLong Price = 0;
  if (IsShopActive(Customer)) {
    Price = ForSale->GetTruePrice();
    if (Price) {
      Price = Amount * (Price * 100 / (100 + Customer->GetAttribute(CHARISMA)) + 1);
      ccharacter *Master = GetMaster();
      if (Master) {
        if (Master->GetConfig() == NEW_ATTNAM) {
               if (ForSale->IsBanana()) Price = (Price>>2)+1;
          else if (ForSale->IsEatable(Master)) Price <<= 2;
          else Price = 0;
        } else if (Master->GetConfig() == BLACK_MARKET) {
          // it's a black market after all!
          Price *= 4;
        }
      }
    }
  }
  return Price;
}


//==========================================================================
//
//  shop::CanBeSoldToShop
//
//==========================================================================
truth shop::CanBeSoldToShop (ccharacter *Customer, item *ForSale) {
  if (ForSale && IsShopActive(Customer)) {
    if (ForSale->IsHeadOfElpuri() || ForSale->IsGoldenEagleShirt() || ForSale->IsPetrussNut() ||
        ForSale->IsTheAvatar() || ForSale->IsEncryptedScroll())
    {
      return false;
    }
    ccharacter *Master = GetMaster();
    if (Master) {
      if (Master->GetConfig() == NEW_ATTNAM) return false;
      if (Master->GetConfig() == REBEL_CAMP) return false;
    }
    return true;
  }
  return false;
}


//==========================================================================
//
//  shop::GetItemSellPrice
//
//==========================================================================
sLong shop::GetItemSellPrice (ccharacter *Customer, item *ForSale, int Amount) {
  IvanAssert(Customer);
  IvanAssert(ForSale);
  IvanAssert(Amount > 0);
  sLong Price = 0;
  if (IsShopActive(Customer)) {
    Price = ForSale->GetTruePrice();
    if (ForSale->IsMoneyBag()) {
      Price *= Amount;
    } else {
      Price = Price * Amount * (100 + Customer->GetAttribute(CHARISMA)) / 400;
      // Decrease the selling price of very expensive items sold in black market.
      ccharacter *Master = GetMaster();
      if (Master && Master->GetConfig() == BLACK_MARKET) {
        Price = (int)((double)Price * (100000.0 / (1000.0 + (double)Price)) / 100.0);
      }
    }
  }
  return Price;
}


//==========================================================================
//
//  shop::Enter
//
//==========================================================================
void shop::Enter (character *Customer) {
  if (Customer->IsPlayer()) {
    if (MasterIsActive()) {
      if (GetMaster()->GetRelation(Customer) != HOSTILE && Customer->CanBeSeenBy(GetMaster())) {
        if (GetMaster()->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s welcomes you warmly to the shop.",
                      GetMaster()->CHAR_NAME(DEFINITE));
        } else {
          ADD_MESSAGE("Something welcomes you warmly to the shop.");
        }
      }
    } else {
      ADD_MESSAGE("The shop appears to be deserted.");
    }
  }
}


//==========================================================================
//
//  shop::PickupItem
//
//==========================================================================
truth shop::PickupItem (character *Customer, item *ForSale, int Amount, bool check) {
  if (!MasterIsActive() || Customer == GetMaster() ||
      GetMaster()->GetRelation(Customer) == HOSTILE)
  {
    return true;
  }

  if (ForSale->IsLanternOnWall()) {
    if (!check) {
      ADD_MESSAGE("\"I'd appreciate it if you left my light sources alone, thank you!\"");
    }
    return false;
  }

  const sLong Price = GetItemBuyPrice(Customer, ForSale, Amount);

  if (!Customer->IsPlayer()) {
    if (Customer->CanBeSeenBy(GetMaster()) && Customer->GetMoney() >= Price) {
      if (!check) {
        if (Price) {
          if (Customer->CanBeSeenByPlayer()) {
            ADD_MESSAGE("%s buys %s.", Customer->CHAR_NAME(DEFINITE),
                        ForSale->GetName(INDEFINITE, Amount).CStr());
          }
          Customer->EditMoney(-Price);
          GetMaster()->EditMoney(Price);
          Customer->EditDealExperience(Price);
        }
      }
      return true;
    }
    return false;
  }

  // the customer is the player
  if (Customer->CanBeSeenBy(GetMaster())) {
    if (ForSale->IsHeadOfElpuri() || ForSale->IsGoldenEagleShirt() ||
        ForSale->IsPetrussNut() || ForSale->IsTheAvatar() || ForSale->IsEncryptedScroll() ||
        ForSale->IsMangoSeedling() || ForSale->IsQuestItem())
    {
      if (!check) {
        ADD_MESSAGE("\"I think it is yours. Take it.\"");
      }
      return true;
    }

    if (!Price) {
      if (!check) {
        ADD_MESSAGE("\"Thank you for cleaning that junk out of my floor.\"");
      }
      return true;
    }

    if (Customer->GetMoney() >= Price) {
      if (check) return true;
      festring ss;
      if (Amount == 1) {
        ss << "That \1Y" << ForSale->CHAR_NAME(UNARTICLED);
        ss << "\2 costs \1Y" << Price << "\2 gold pieces.";
      } else {
        ss << "Those \1Y" << Amount << " " << ForSale->CHAR_NAME(PLURAL);
        ss << "\2 cost \1Y" << Price << "\2 gold pieces.";
      }
      ADD_MESSAGE("\"Ah! %s No haggling, please.\"", ss.CStr());
      if (game::TruthQuestion(CONST_S("\"") + ss + "\"\nDo you accept this deal?")) {
        ADD_MESSAGE("You bought %s.", ForSale->GetName(INDEFINITE, Amount).CStr());
        Customer->EditMoney(-Price);
        GetMaster()->EditMoney(+Price);
        Customer->EditDealExperience(Price);
        return true;
      }
      return false;
    } else {
      if (!check) {
        if (Amount == 1) {
          ADD_MESSAGE("\"Don't touch that %s, beggar! It is worth \1Y%d\2 gold pieces!\"",
                      ForSale->CHAR_NAME(UNARTICLED), Price);
        } else {
          ADD_MESSAGE("\"Don't touch those %s, beggar! They are worth \1Y%d\2 gold pieces!\"",
                      ForSale->CHAR_NAME(PLURAL), Price);
        }
      }
      return false;
    }
  }

  if (!check) {
    if (game::TruthQuestion(CONST_S("Are you sure you want to commit this thievery?"))) {
      if (!Customer->TryToStealFromShop(GetMaster(), ForSale)) {
        ADD_MESSAGE("You caught stealing %s from the shop.", ForSale->CHAR_NAME(UNARTICLED));
        Customer->Hostility(GetMaster());
      } else {
        ADD_MESSAGE("You stole %s from the shop.", ForSale->CHAR_NAME(UNARTICLED));
      }
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  shop::DropItem
//
//==========================================================================
truth shop::DropItem (character *Customer, item *ForSale, int Amount, bool check) {
  if (!MasterIsActive() || Customer == GetMaster() ||
      GetMaster()->GetRelation(Customer) == HOSTILE)
  {
    return true;
  }

  if (GetMaster()->GetConfig() == NEW_ATTNAM) {
    if (!check) {
      ADD_MESSAGE("\"Sorry, I'm only allowed to buy from Decos Bananas Co. if I wish to stay here.\"");
    }
    return false;
  }

  if (GetMaster()->GetConfig() == REBEL_CAMP) {
    if (!check) {
      ADD_MESSAGE("\"I'm a quartermaster, not a merchant. Go sell your stuff somewhere else.\"");
    }
    return false;
  }

  sLong Price = GetItemSellPrice(Customer, ForSale, Amount);

  if (!Customer->IsPlayer()) {
    if (ForSale->IsMoneyBag()) {
      if (!check) {
        if (Customer->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s gives %s to the shopkeeper.", Customer->CHAR_NAME(DEFINITE),
                      ForSale->GetName(INDEFINITE, Amount).CStr());
        }
        GetMaster()->EditMoney(Price);
      }
      return true;
    }

    if (Price && GetMaster()->GetMoney() >= Price) {
      if (!check) {
        if (Customer->CanBeSeenByPlayer()) {
          ADD_MESSAGE("%s sells %s.", Customer->CHAR_NAME(DEFINITE),
                      ForSale->GetName(INDEFINITE, Amount).CStr());
        }
        Customer->EditMoney(Price);
        GetMaster()->EditMoney(-Price);
        Customer->EditDealExperience(Price);
      }
      return true;
    }
    return false;
  }

  if (Customer->CanBeSeenBy(GetMaster())) {
    if (ForSale->IsHeadOfElpuri() || ForSale->IsGoldenEagleShirt() ||
        ForSale->IsPetrussNut() || ForSale->IsTheAvatar() || ForSale->IsEncryptedScroll() ||
        ForSale->IsMangoSeedling() || ForSale->IsQuestItem())
    {
      if (!check) {
        ADD_MESSAGE("\"Oh no! You need it far more than I!\"");
      }
      return false;
    }

    if (ForSale->WillExplodeSoon()) {
      if (!check) {
        ADD_MESSAGE("\"Hey that %s is primed! Take it out! OUT, I SAY!\"",
                    ForSale->CHAR_NAME(UNARTICLED));
      }
      return false;
    }

    if (!Price) {
      if (!check) {
        ADD_MESSAGE("\"Hah! I wouldn't take %s even if you paid me for it!\"",
                    (Amount == 1 ? "that" : "those"));
      }
      return false;
    }

    if (ForSale->IsMoneyBag()) {
      if (!check) {
        if (!game::TruthQuestion(CONST_S("Do you want to give it to the shopkeeper?"))) return false;
        ADD_MESSAGE("You gives %s to the shopkeeper.", ForSale->GetName(INDEFINITE, Amount).CStr());
        GetMaster()->EditMoney(Price);
      }
      return true;
    }

    if (GetMaster()->GetMoney()) {
      if (check) return true;
      if (GetMaster()->GetMoney() < Price) {
        Price = GetMaster()->GetMoney();
      }
      festring ss;
      if (Amount == 1) {
        ss << "\"What a fine " << ForSale->CHAR_NAME(UNARTICLED) << ". I'll pay \1Y";
        ss << Price << "\2 gold pieces for it.\"";
      } else {
        ss << "\"What a fine pile of " << Amount << " " << ForSale->CHAR_NAME(PLURAL);
        ss << ". I'll pay \1Y" << Price << "\2 gold pieces for them.\"";
      }
      ADD_MESSAGE("%s", ss.CStr());
      ss.Empty();
      ss << "\"I'll pay \1Y" << Price << "\2 gold pieces for \1Y";
      if (Amount == 1) {
        ss << ForSale->CHAR_NAME(UNARTICLED);
      } else {
        ss << ForSale->CHAR_NAME(PLURAL);
      }
      if (game::TruthQuestion(ss + "\2.\"\nDo you accept this deal?")) {
        ADD_MESSAGE("You sold %s.",
                    (Amount == 1 ? ForSale->CHAR_NAME(UNARTICLED)
                                 : ForSale->CHAR_NAME(PLURAL)));
        Customer->SetMoney(Customer->GetMoney() + Price);
        GetMaster()->SetMoney(GetMaster()->GetMoney() - Price);
        Customer->EditDealExperience(Price);
        return true;
      }
      return false;
    }

    if (!check) {
      ADD_MESSAGE("\"I would pay you \1Y%d\2 gold pieces for %s, but "
                  "I'm temporarily short of cash. Sorry.\"",
                  Price, Amount == 1 ? "it" : "them");
    }
    return false;
  }

  if (!check) {
    ADD_MESSAGE("The shopkeeper doesn't see you, so you cannot trade with him.");
    return game::TruthQuestion(CONST_S("Still drop ") +
                               (Amount == 1 ? "this item" : "these items")+"?");
  } else {
    return true;
  }
}


//==========================================================================
//
//  shop::KickSquare
//
//==========================================================================
void shop::KickSquare (character *Infidel, lsquare *Square) {
  if (!AllowKick(Infidel, Square)) {
    ADD_MESSAGE("\"You infidel!\"");
    Infidel->Hostility(GetMaster());
  }
}


//==========================================================================
//
//  shop::ConsumeItem
//
//==========================================================================
truth shop::ConsumeItem (character *Customer, item *Item, int Amount, bool check) {
  if (!MasterIsActive() || GetMaster()->GetRelation(Customer) == HOSTILE) {
    return true;
  }

  if (!Customer->IsPlayer()) {
    return false;
  }

  if (Customer->CanBeSeenBy(GetMaster())) {
    if (!check) {
      ADD_MESSAGE("\"Buy that first, please.\"");
    }
    return false;
  }

  if (!check) {
    if (game::TruthQuestion(CONST_S("It's illegal to eat property of others. Are you sure you sure?"))) {
      Customer->Hostility(GetMaster());
      return true;
    }
  }

  return false;
}


//==========================================================================
//
//  shop::TeleportSquare
//
//==========================================================================
void shop::TeleportSquare (character *Infidel, lsquare *Square) {
  if (Square->GetStack()->GetItems() && MasterIsActive() &&
     Infidel && Infidel != GetMaster() &&
     GetMaster()->GetRelation(Infidel) != HOSTILE &&
     Square->CanBeSeenBy(GetMaster()))
  {
    ADD_MESSAGE("\"You infidel!\"");
    Infidel->Hostility(GetMaster());
  }
}


//==========================================================================
//
//  shop::AllowSpoil
//
//==========================================================================
truth shop::AllowSpoil (citem *Item) const {
  character *Master = GetMaster();
  return (!Master || !Master->IsEnabled() || !Item->HasPrice());
}


//==========================================================================
//
//  shop::AllowKick
//
//==========================================================================
truth shop::AllowKick (ccharacter *Char, const lsquare *LSquare) const {
  return (
    !LSquare->GetStack()->GetItems() || !MasterIsActive() ||
    Char == GetMaster() || GetMaster()->GetRelation(Char) == HOSTILE ||
    !LSquare->CanBeSeenBy(GetMaster()));
}


//==========================================================================
//
//  shop::HostileAction
//
//==========================================================================
void shop::HostileAction (character *Guilty) const {
  if (MasterIsActive() && Guilty && Guilty != GetMaster() &&
      GetMaster()->GetRelation(Guilty) != HOSTILE &&
      Guilty->CanBeSeenBy(GetMaster()))
  {
    ADD_MESSAGE("\"You infidel!\"");
    Guilty->Hostility(GetMaster());
  }
}


//==========================================================================
//
//  shop::ReceiveVomit
//
//==========================================================================
void shop::ReceiveVomit (character *Who) {
  if (MasterIsActive() &&
      Who->IsPlayer() &&
      Who->GetRelation(GetMaster()) != HOSTILE &&
      Who->CanBeSeenBy(GetMaster()))
  {
    ADD_MESSAGE("\"Unfortunately I accept no returns.\"");
  }
}


#endif
