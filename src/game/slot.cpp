/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

/* Compiled through slotset.cpp */

// ////////////////////////////////////////////////////////////////////////// //
// slot
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  slot::Save
//
//==========================================================================
void slot::Save (outputfile &SaveFile) const {
  SaveFile << Item;
}


//==========================================================================
//
//  slot::Load
//
//==========================================================================
void slot::Load (inputfile &SaveFile) {
  SaveFile >> Item;
  if (Item) {
    Item->SetMainSlot(this);
  }
}


//==========================================================================
//
//  slot::DonateTo
//
//==========================================================================
void slot::DonateTo (item *Item) {
  Empty();
  PutInItem(Item);
}


// ////////////////////////////////////////////////////////////////////////// //
// stackslot
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  stackslot::Load
//
//==========================================================================
void stackslot::Load (inputfile &SaveFile) {
  SaveFile >> Item;
  if (Item) {
    Item->SignalStackAdd(this, &stack::AddElement);
  }
}


//==========================================================================
//
//  stackslot::Empty
//
//==========================================================================
void stackslot::Empty () {
  GetMotherStack()->RemoveItem(this);
}


//==========================================================================
//
//  stackslot::AddFriendItem
//
//==========================================================================
void stackslot::AddFriendItem (item *Item) const {
  Item->RemoveFromSlot();
  GetMotherStack()->AddItem(Item);
}


//==========================================================================
//
//  stackslot::IsOnGround
//
//==========================================================================
truth stackslot::IsOnGround () const {
  return GetMotherStack()->IsOnGround();
}


//==========================================================================
//
//  stackslot::SignalVolumeAndWeightChange
//
//==========================================================================
void stackslot::SignalVolumeAndWeightChange () {
  GetMotherStack()->SignalVolumeAndWeightChange();
}


//==========================================================================
//
//  stackslot::PutInItem
//
//==========================================================================
void stackslot::PutInItem (item *What) {
  Item = What;
  if (Item) {
    Item->SignalStackAdd(this, &stack::AddItem);
    SignalVolumeAndWeightChange();
    SignalEmitationIncrease(Item->GetEmitation());
  }
}


//==========================================================================
//
//  stackslot::GetSquareUnder
//
//==========================================================================
square *stackslot::GetSquareUnder (int) const {
  return GetMotherStack()->GetSquareUnder();
}


//==========================================================================
//
//  stackslot::SignalEmitationIncrease
//
//==========================================================================
void stackslot::SignalEmitationIncrease (col24 Emitation) {
  GetMotherStack()->SignalEmitationIncrease(Item->GetSquarePosition(), Emitation);
}


//==========================================================================
//
//  stackslot::SignalEmitationDecrease
//
//==========================================================================
void stackslot::SignalEmitationDecrease (col24 Emitation) {
  GetMotherStack()->SignalEmitationDecrease(Item->GetSquarePosition(), Emitation);
}


//==========================================================================
//
//  stackslot::DonateTo
//
//  FIXME: could be optimized (k8: how?)
//
//==========================================================================
void stackslot::DonateTo (item *Item) {
  AddFriendItem(Item);
  Empty();
}


//==========================================================================
//
//  stackslot::CanBeSeenBy
//
//==========================================================================
truth stackslot::CanBeSeenBy (ccharacter *Viewer) const {
  return GetMotherStack()->CanBeSeenBy(Viewer, Item->GetSquarePosition());
}


//==========================================================================
//
//  stackslot::IsVisible
//
//==========================================================================
truth stackslot::IsVisible () const {
  return GetMotherStack()->IsVisible();
}


//==========================================================================
//
//  stackslot::FindCarrier
//
//==========================================================================
ccharacter *stackslot::FindCarrier () const {
  return GetMotherStack()->FindCarrier();
}


// ////////////////////////////////////////////////////////////////////////// //
// bodypartslot
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  bodypartslot::Load
//
//==========================================================================
void bodypartslot::Load (inputfile &SaveFile) {
  slot::Load(SaveFile);
  if (Item) {
    static_cast<bodypart *>(Item)->SetMaster(GetMaster());
  }
}


//==========================================================================
//
//  bodypartslot::Empty
//
//==========================================================================
void bodypartslot::Empty () {
  col24 Emitation = Item->GetEmitation();
  static_cast<bodypart*>(Item)->SetMaster(0);
  Item = 0;
  GetMaster()->CalculateEquipmentState();
  SignalVolumeAndWeightChange();
  SignalEmitationDecrease(Emitation);
  if (!GetMaster()->IsInitializing()) {
    GetMaster()->CalculateHP();
    GetMaster()->CalculateMaxHP();
  }
}


//==========================================================================
//
//  bodypartslot::AddFriendItem
//
//==========================================================================
void bodypartslot::AddFriendItem (item *Item) const {
  Item->RemoveFromSlot();
  if (!game::IsInWilderness()) {
    GetMaster()->GetStackUnder()->AddItem(Item);
  } else {
    GetMaster()->GetStack()->AddItem(Item);
  }
}


//==========================================================================
//
//  bodypartslot::SignalVolumeAndWeightChange
//
//==========================================================================
void bodypartslot::SignalVolumeAndWeightChange () {
  GetMaster()->SignalVolumeAndWeightChange();
  GetMaster()->SignalBodyPartVolumeAndWeightChange();
}


//==========================================================================
//
//  bodypartslot::PutInItem
//
//==========================================================================
void bodypartslot::PutInItem (item *What) {
  Item = What;
  if (Item) {
    Item->SetMainSlot(this);
    static_cast<bodypart *>(Item)->SetMaster(GetMaster());
    if (!GetMaster()->IsInitializing()) {
      SignalVolumeAndWeightChange();
      SignalEmitationIncrease(Item->GetEmitation());
      static_cast<bodypart *>(Item)->CalculateMaxHP(0);
      GetMaster()->CalculateHP();
      GetMaster()->CalculateMaxHP();
    }
  }
}


//==========================================================================
//
//  bodypartslot::GetSquareUnder
//
//==========================================================================
square *bodypartslot::GetSquareUnder (int I) const {
  return GetMaster()->GetSquareUnder(I);
}


//==========================================================================
//
//  bodypartslot::SignalEmitationIncrease
//
//==========================================================================
void bodypartslot::SignalEmitationIncrease (col24 Emitation) {
  GetMaster()->SignalEmitationIncrease(Emitation);
}


//==========================================================================
//
//  bodypartslot::SignalEmitationDecrease
//
//==========================================================================
void bodypartslot::SignalEmitationDecrease (col24 Emitation) {
  GetMaster()->SignalEmitationDecrease(Emitation);
}


//==========================================================================
//
//  bodypartslot::CanBeSeenBy
//
//==========================================================================
truth bodypartslot::CanBeSeenBy (ccharacter *Viewer) const {
  return GetMaster()->CanBeSeenBy(Viewer);
}


// ////////////////////////////////////////////////////////////////////////// //
// gearslot
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  gearslot::Init
//
//==========================================================================
void gearslot::Init (bodypart *BodyPart, int I) {
  SetBodyPart(BodyPart);
  SetEquipmentIndex(I);
}


//==========================================================================
//
//  gearslot::Empty
//
//==========================================================================
void gearslot::Empty () {
  citem *Old = Item;
  Item = 0;
  col24 Emitation = Old->GetEmitation();
  SignalVolumeAndWeightChange();
  GetBodyPart()->SignalEquipmentRemoval(this, Old);
  SignalEmitationDecrease(Emitation);
}


//==========================================================================
//
//  gearslot::AddFriendItem
//
//==========================================================================
void gearslot::AddFriendItem (item* Item) const {
  Item->RemoveFromSlot();
  if (!game::IsInWilderness()) {
    GetBodyPart()->GetLSquareUnder()->AddItem(Item);
  } else {
    GetBodyPart()->GetMaster()->GetStack()->AddItem(Item);
  }
}


//==========================================================================
//
//  gearslot::SignalVolumeAndWeightChange
//
//==========================================================================
void gearslot::SignalVolumeAndWeightChange () {
  GetBodyPart()->SignalVolumeAndWeightChange();
}


//==========================================================================
//
//  gearslot::PutInItem
//
//==========================================================================
void gearslot::PutInItem (item *What) {
  Item = What;
  if (Item) {
    Item->SetMainSlot(this);
    GetBodyPart()->SignalEquipmentAdd(this);
    SignalVolumeAndWeightChange();
    SignalEmitationIncrease(Item->GetEmitation());
  }
}


//==========================================================================
//
//  gearslot::GetSquareUnder
//
//==========================================================================
square *gearslot::GetSquareUnder (int) const {
  return GetBodyPart()->GetSquareUnder();
}


//==========================================================================
//
//  gearslot::SignalEmitationIncrease
//
//==========================================================================
void gearslot::SignalEmitationIncrease (col24 Emitation) {
  GetBodyPart()->SignalEmitationIncrease(Emitation);
}


//==========================================================================
//
//  gearslot::SignalEmitationDecrease
//
//==========================================================================
void gearslot::SignalEmitationDecrease (col24 Emitation) {
  GetBodyPart()->SignalEmitationDecrease(Emitation);
}


//==========================================================================
//
//  gearslot::CanBeSeenBy
//
//==========================================================================
truth gearslot::CanBeSeenBy (ccharacter *Viewer) const {
  return GetBodyPart()->CanBeSeenBy(Viewer);
}


//==========================================================================
//
//  gearslot::SignalEnchantmentChange
//
//==========================================================================
void gearslot::SignalEnchantmentChange () {
  GetBodyPart()->SignalEnchantmentChange();
}


//==========================================================================
//
//  gearslot::FindCarrier
//
//==========================================================================
ccharacter *gearslot::FindCarrier () const {
  return GetBodyPart()->FindCarrier();
}
