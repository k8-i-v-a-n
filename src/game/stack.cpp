/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through slotset.cpp */


/* If REMEMBER_SELECTED flag is used, DrawContents() will use this to determine the initial selected item */

int stack::Selected;


//==========================================================================
//
//  stack::stack
//
//==========================================================================
stack::stack (square *MotherSquare, entity* MotherEntity, feuLong Flags)
  : Bottom(0)
  , Top(0)
  , MotherSquare(MotherSquare)
  , MotherEntity(MotherEntity)
  , Volume(0)
  , Weight(0)
  , Emitation(0)
  , Flags(Flags)
  , Items(0)
{
}


//==========================================================================
//
//  stack::~stack
//
//==========================================================================
stack::~stack () {
  Clean(true);
}


//==========================================================================
//
//  stack::GetSquareUnder
//
//==========================================================================
square *stack::GetSquareUnder () const {
  return !MotherEntity ? MotherSquare : MotherEntity->GetSquareUnderEntity();
}


//==========================================================================
//
//  stack::Draw
//
//  Modifies the square index bits of BlitData.CustomData
//
//==========================================================================
void stack::Draw (ccharacter *Viewer, blitdata &BlitData, int RequiredSquarePosition) const {
  if (!Items) return;
  int VisibleItems = 0;
  v2 StackPos = GetPos();
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == RequiredSquarePosition &&
        (i->CanBeSeenBy(Viewer) || game::GetSeeWholeMapCheatMode()))
    {
      BlitData.CustomData |= i->GetSquareIndex(StackPos);
      i->Draw(BlitData);
      BlitData.CustomData &= ~SQUARE_INDEX_MASK;
      VisibleItems += 1;
    }
  }
  if (RequiredSquarePosition == CENTER) {
    const truth PlusSymbol = (VisibleItems > 1);
    const truth Dangerous = NeedDangerSymbol(Viewer);
    if (PlusSymbol || Dangerous) {
      col24 L = BlitData.Luminance;
      BlitData.Luminance = ivanconfig::GetContrastLuminance();
      BlitData.Src.Y = 16;
      if (PlusSymbol) igraph::GetSymbolGraphic()->LuminanceMaskedBlit(BlitData);
      if (Dangerous) {
        BlitData.Src.X = 160;
        igraph::GetSymbolGraphic()->LuminanceMaskedBlit(BlitData);
      }
      BlitData.Src.X = BlitData.Src.Y = 0; //TODO: check (k8: what?)
      BlitData.Luminance = L;
    }
  }
}


//==========================================================================
//
//  stack::AddItem
//
//==========================================================================
void stack::AddItem (item *ToBeAdded, truth RunRoomEffects) {
  if (!ToBeAdded) return;
  AddElement(ToBeAdded);
  if (Flags & HIDDEN) return;
  lsquare *SquareUnder = GetLSquareTrulyUnder(ToBeAdded->GetSquarePosition());
  if (!SquareUnder) return;
  if (ToBeAdded->IsAnimated()) {
    SquareUnder->IncStaticAnimatedEntities();
  }
  if (!game::IsGenerating()) {
    if (RunRoomEffects && GetLSquareUnder()->GetRoom()) {
      GetLSquareUnder()->GetRoom()->AddItemEffect(ToBeAdded);
    }
    SquareUnder->SendNewDrawRequest();
    SquareUnder->SendMemorizedUpdateRequest();
  }
}


//==========================================================================
//
//  stack::RemoveItem
//
//==========================================================================
void stack::RemoveItem (stackslot *Slot) {
  item *Item = Slot->GetItem();
  truth WasAnimated = Item->IsAnimated();
  col24 Emit = Item->GetEmitation();
  RemoveElement(Slot);
  SignalVolumeAndWeightChange();
  SignalEmitationDecrease(Item->GetSquarePosition(), Emit);
  if (Flags & HIDDEN) return;
  lsquare *SquareUnder = GetLSquareTrulyUnder(Item->GetSquarePosition());
  if (Item->GetSquarePosition() != CENTER) Item->SignalSquarePositionChange(CENTER);
  if (!SquareUnder) return;
  if (WasAnimated) SquareUnder->DecStaticAnimatedEntities();
  if (!game::IsGenerating()) {
    SquareUnder->SendNewDrawRequest();
    SquareUnder->SendMemorizedUpdateRequest();
  }
}


//==========================================================================
//
//  stack::Clean
//
//  Removes all items. `LastClean` should be true only if
//  the stack is being deleted (the default is false).
//
//==========================================================================
void stack::Clean (truth LastClean) {
  if (!Items) return;
  stackslot *Slot = Bottom;
  if (!LastClean) {
    Bottom = Top = 0;
    Volume = Weight = Items = 0;
    SignalVolumeAndWeightChange();
  }
  while (Slot) {
    item *Item = Slot->GetItem();
    if (!(Flags & HIDDEN) && Item->IsAnimated() && !LastClean) {
      lsquare *Square = GetLSquareTrulyUnder(Item->GetSquarePosition());
      if (Square) {
        Square->DecStaticAnimatedEntities();
      }
    }
    if (LastClean && Item->GetSquaresUnder() == 1) {
      delete Item;
    } else {
      Item->SendToHell();
    }
    stackslot *Rubbish = Slot;
    Slot = Slot->Next;
    delete Rubbish;
    if (!LastClean) {
      SignalEmitationDecrease(Item->GetSquarePosition(), Item->GetEmitation());
    }
  }
}


//==========================================================================
//
//  stack::Save
//
//==========================================================================
void stack::Save (outputfile &SaveFile) const {
  if (!Items) {
    SaveFile << uShort(0);
    return;
  }
  uShort SavedItems = 0;
  for (stackiterator i1 = GetBottom(); i1.HasItem(); ++i1) {
    if (i1->IsMainSlot(&i1.GetSlot())) {
      SavedItems += 1;
    }
  }
  SaveFile << SavedItems;
  /* Save multitiled items only to one stack */
  for (stackiterator i2 = GetBottom(); i2.HasItem(); ++i2) {
    if (i2->IsMainSlot(&i2.GetSlot())) {
      SaveFile << i2.GetSlot();
    }
  }
}


//==========================================================================
//
//  stack::Load
//
//==========================================================================
void stack::Load (inputfile &SaveFile) {
  uShort SavedItems = 0;
  SaveFile >> SavedItems;
  for (int c = 0; c < SavedItems; ++c) {
    if (!c && !Items) {
      Bottom = Top = new stackslot(this, 0);
    } else {
      Top = Top->Next = new stackslot(this, Top);
    }
    SaveFile >> *Top;
    Volume += (*Top)->GetVolume();
    Weight += (*Top)->GetWeight();
    if ((*Top)->GetSquarePosition() == CENTER) {
      Emitation = game::CombineConstLights(Emitation, (*Top)->GetEmitation());
    }
  }
  Items += SavedItems;
}


//==========================================================================
//
//  stack::GetPos
//
//==========================================================================
v2 stack::GetPos () const {
  return GetSquareUnder()->GetPos();
}


//==========================================================================
//
//  stack::SortedItems
//
//  Returns whether there are any items satisfying
//  the sorter or any visible items if it is zero
//
//==========================================================================
truth stack::SortedItems (ccharacter *Viewer, sorter SorterFunction) const {
  if (Items) {
    for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
      if ((SorterFunction == 0 || ((*i)->*SorterFunction)(Viewer)) &&
          ((Flags & HIDDEN) || i->CanBeSeenBy(Viewer)))
      {
        return true;
      }
    }
  }
  return false;
}


//==========================================================================
//
//  stack::SortedItemsCount
//
//==========================================================================
int stack::SortedItemsCount (ccharacter *Viewer, sorter SorterFunction) const {
  int res = 0;
  if (Items) {
    for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
      if ((SorterFunction == 0 || ((*i)->*SorterFunction)(Viewer)) &&
          ((Flags & HIDDEN) || i->CanBeSeenBy(Viewer)))
      {
        res += 1;
      }
    }
  }
  return res;
}


//==========================================================================
//
//  stack::SortedItemsRecentTime
//
//  0, 1 or most recent pickup time
//
//==========================================================================
feuLong stack::SortedItemsRecentTime (ccharacter *Viewer, sorter SorterFunction) const {
  feuLong highestTime = 0;
  if (Items) {
    for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
      if ((SorterFunction == 0 || ((*i)->*SorterFunction)(Viewer)) &&
          ((Flags & HIDDEN) || i->CanBeSeenBy(Viewer)))
      {
        feuLong pt = (*i)->pickupTime;
        if (pt == 0) pt = 1;
        if (pt > highestTime) highestTime = pt;
      }
    }
  }
  return highestTime;
}


//==========================================================================
//
//  stack::BeKicked
//
//==========================================================================
void stack::BeKicked (character *Kicker, int KickDamage, int Direction) {
  if (KickDamage) {
    ReceiveDamage(Kicker, KickDamage, PHYSICAL_DAMAGE, Direction);
    if (GetItems() && GetLSquareUnder()->IsFlyable()) {///&& SquarePosition == CENTER)
      item *Item1 = *GetTop();
      item *Item2 = RAND_2 && GetItems() > 1 ? *--GetTop() : 0;
      Item1->Fly(Kicker, Direction, KickDamage*3);
      if (Item2) {
        /*k8: if(!Item2->Exists() || Item2->GetPos() != GetPos()) int esko = esko = 2; */
        if (!(!Item2->Exists() || Item2->GetPos() != GetPos())) {
          Item2->Fly(Kicker, Direction, KickDamage * 3);
        }
      }
    }
  } else if (Kicker->IsPlayer() && GetNativeVisibleItems(Kicker)) {
    ADD_MESSAGE("Your weak kick has no effect.");
  }
}


//==========================================================================
//
//  stack::Polymorph
//
//==========================================================================
void stack::Polymorph (character *Polymorpher) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  int p = 0;
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->Exists() && ItemVector[c]->Polymorph(Polymorpher, this) && ++p == 5) {
      break;
    }
  }
}


//==========================================================================
//
//  stack::Alchemize
//
//==========================================================================
void stack::Alchemize (character *Midas) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  int p = 0;
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->Exists() && ItemVector[c]->Alchemize(Midas, this) && ++p == 5) {
      break;
    }
  }
}


//==========================================================================
//
//  stack::SoftenMaterial
//
//==========================================================================
void stack::SoftenMaterial (character *Jerk) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  int p = 0;
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->Exists() && ItemVector[c]->SoftenMaterial() && ++p == 5) {
      break;
    }
  }
}


//==========================================================================
//
//  stack::CheckForStepOnEffect
//
//==========================================================================
void stack::CheckForStepOnEffect (character *Stepper) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->Exists()) {
      ItemVector[c]->StepOnEffect(Stepper);
      if (!Stepper->IsEnabled()) return;
    }
  }
}


//==========================================================================
//
//  stack::GetLSquareTrulyUnder
//
//==========================================================================
lsquare *stack::GetLSquareTrulyUnder (int SquarePosition) const {
  if (SquarePosition == DOWN) {
    if (GetArea()->IsValidPos(GetPos()+v2(0, 1))) return GetNearLSquare(GetPos() + v2(0, 1));
    return 0;
  }
  if (SquarePosition == LEFT) {
    if (GetArea()->IsValidPos(GetPos()+v2(-1, 0))) return GetNearLSquare(GetPos() + v2(-1, 0));
    return 0;
  }
  if (SquarePosition == UP) {
    if (GetArea()->IsValidPos(GetPos()+v2(0, -1))) return GetNearLSquare(GetPos() + v2(0, -1));
    return 0;
  }
  if (SquarePosition == RIGHT) {
    if (GetArea()->IsValidPos(GetPos()+v2(1, 0))) return GetNearLSquare(GetPos() + v2(1, 0));
    return 0;
  }
  return GetLSquareUnder();
}


//==========================================================================
//
//  stack::ReceiveDamage
//
//==========================================================================
void stack::ReceiveDamage (character *Damager, int Damage, int Type, int Direction) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->Exists() && AllowDamage(Direction, ItemVector[c]->GetSquarePosition())) {
      ItemVector[c]->ReceiveDamage(Damager, Damage, Type);
    }
  }
}


//==========================================================================
//
//  stack::TeleportRandomly
//
//==========================================================================
void stack::TeleportRandomly (uInt Amount) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  for (uInt c = 0; c < ItemVector.size() && c < Amount; ++c) {
    if (ItemVector[c]->Exists()) {
      if (ItemVector[c]->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s disappears!", ItemVector[c]->GetExtendedDescription().CStr());
      }
      ItemVector[c]->TeleportRandomly();
    }
  }
}


//==========================================================================
//
//  stack::HasAnyItem
//
//==========================================================================
bool stack::HasAnyItem () const {
  stackiterator i = GetBottom();
  return i.HasItem();
}


//==========================================================================
//
//  stack::FillItemVector
//
//  ItemVector receives all items in the stack
//
//==========================================================================
void stack::FillItemVector (itemvector &ItemVector) const {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    ItemVector.push_back(*i);
  }
}


//==========================================================================
//
//  stack::GetItem
//
//  Don't use; this function is only for gum solutions
//
//==========================================================================
item *stack::GetItem (int I) const {
  int c = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i, ++c) {
    if (c == I) return *i;
  }
  return 0;
}


//==========================================================================
//
//  stack::SearchItem
//
//  Don't use; this function is only for gum solutions
//
//==========================================================================
int stack::SearchItem (item *ToBeSearched) const {
  int c = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i, ++c) {
    if (*i == ToBeSearched) return c;
  }
  return -1;
}


//==========================================================================
//
//  stack::DrawContents
//
//  Flags for all `DrawContents()` functions can be found in "ivandef.h".
//  Those returning int return `0` on success and a felist error
//  otherwise (see "felibdef.h").
//
//==========================================================================
item *stack::DrawContents (ccharacter *Viewer, cfestring &Topic, int Flags,
                           sorter SorterFunction, item *hiitem) const
{
  itemvector ReturnVector;
  DrawContents(ReturnVector, 0, Viewer, Topic, CONST_S(""), CONST_S(""), CONST_S(""), 0,
               Flags|NO_MULTI_SELECT, SorterFunction, hiitem);
  return ReturnVector.empty() ? 0 : ReturnVector[0];
}


//==========================================================================
//
//  stack::DrawContents
//
//==========================================================================
int stack::DrawContents (itemvector &ReturnVector, ccharacter *Viewer, cfestring &Topic,
                         int Flags, sorter SorterFunction, item *hiitem) const
{
  return DrawContents(ReturnVector, 0, Viewer, Topic, CONST_S(""), CONST_S(""), CONST_S(""), 0,
                      Flags, SorterFunction, hiitem);
}


//==========================================================================
//
//  stack::DrawContents
//
//  `MergeStack` is used for showing two stacks together.
//  Like when eating when there are items on the ground and
//  in the character's stack.
//
//==========================================================================
int stack::DrawContents (itemvector &ReturnVector, stack *MergeStack,
                         ccharacter *Viewer, cfestring &Topic,
                         cfestring &ThisDesc, cfestring &ThatDesc,
                         cfestring &SpecialDesc, col16 SpecialDescColor,
                         int Flags, sorter SorterFunction, item *hiitem) const
{
  felist Contents(Topic);
  lsquare *Square = GetLSquareUnder();
  stack *AdjacentStack[4] = { 0, 0, 0, 0 };

  // why not?
  if (MotherEntity && MotherEntity->IsCharacter()) {
    ccharacter *Char = (ccharacter *)MotherEntity;
    if (Char->IsPlayer()) {
      Flags |= STACK_ALLOW_NAMING;
    }
  }

  if ((this->Flags & HIDDEN) == 0) {
    for (int c = 0; c != 4; c += 1) {
      AdjacentStack[c] = Square->GetStackOfAdjacentSquare(c);
    }
  }

  if (!SpecialDesc.IsEmpty()) {
    Contents.AddDescription(CONST_S(""));
    Contents.AddDescription(SpecialDesc.CapitalizeCopy(), SpecialDescColor);
  }

  /*
  if (!(Flags & NO_SPECIAL_INFO)) {
    Contents.AddDescription(CONST_S(""));
    sLong Weight = GetWeight(Viewer, CENTER);
    if (MergeStack) Weight += MergeStack->GetWeight(Viewer, CENTER);
    for (c = 0; c < 4; ++c)
      if (AdjacentStack[c])
        Weight += AdjacentStack[c]->GetWeight(Viewer, 3 - c);
    Contents.AddDescription(CONST_S("Overall weight: ") + Weight + " grams");
    }
  */

  if (Flags & NONE_AS_CHOICE) {
    int ImageKey = game::AddToItemDrawVector(itemvector());
    Contents.AddEntry(CONST_S("none"), /*LIGHT_GRAY*/MakeRGB16(196, 196, 0), 0, ImageKey);
  }

  if (MergeStack) {
    MergeStack->AddContentsToList(Contents, Viewer, ThatDesc, Flags, CENTER, SorterFunction, hiitem);
  }
  AddContentsToList(Contents, Viewer, ThisDesc, Flags, CENTER, SorterFunction, hiitem);

  cchar *WallDescription[] = { "western", "southern", "nothern", "eastern" };
  for (int c = 0; c != 4; c += 1) {
    if (AdjacentStack[c]) {
      AdjacentStack[c]->AddContentsToList(Contents, Viewer,
                                          CONST_S("Items on the ")+WallDescription[c] + " wall:",
                                          Flags, 3-c, SorterFunction, hiitem);
    }
  }

  game::SetStandardListAttributes(Contents);
  Contents.SetPageLength(12);
  Contents.RemoveFlags(BLIT_AFTERWARDS);
  Contents.SetEntryDrawer(game::ItemEntryDrawer);

  if ((Flags & NO_SELECT) == 0) {
    Contents.AddFlags(SELECTABLE);
  }

  if (Flags & STACK_ALLOW_TAB) {
    Contents.AddFlags(FE_ALLOW_TAB);
  }

  if (Flags & STACK_ALLOW_NAMING) {
    if ((Flags & NO_SELECT) == 0) {
      Contents.AddFlags(FE_ALLOW_EXCLAM);
    }
  }

  // `Contents.Draw()` will fix invalid selections
  // k8: the following code is pure shit. i don't even understand it anymore.
  if ((Flags & NO_SELECT) == 0) {
    int selected = -1;
    truth foundSomething = false;

    if ((Flags & NONE_AS_CHOICE) && (Flags & SKIP_FIRST_IF_NO_OLD) && !hiitem) {
      if ((Flags & REMEMBER_SELECTED) == 0 || GetSelected() == 0) {
        selected = 1;
      }
    }

    if (Flags & SS_FIRST_ITEM) {
      selected = 0;
      foundSomething = true;
    } else if (Flags & SS_LAST_ITEM) {
      int cursel = -1;
      for (uInt c = 0; c < Contents.GetLength(); ++c) {
        if (!Contents.IsEntrySelectable(c)) continue;
        ++cursel;
        if (Flags & DONT_SELECT_CONTAINERS) {
          item *it = (item *)Contents.GetEntryUPtr(c);
          if (it && it->IsLockableContainer()) continue;
        }
        selected = cursel;
        foundSomething = true;
      }
    } else {
      if (Flags & SELECT_MOST_RECENT) {
        int cursel = -1;
        feuLong maxpt = 0;
        for (uInt c = 0; c < Contents.GetLength(); ++c) {
          if (!Contents.IsEntrySelectable(c)) continue;
          ++cursel;
          if (cursel < selected) continue;
          feuLong pt = Contents.GetEntryUData(c);
          if (pt <= 1 || game::GetTick() - pt > game::PickTimeout) continue;
          if (pt < maxpt) continue;
          if (Flags & DONT_SELECT_CONTAINERS) {
            item *it = (item *)Contents.GetEntryUPtr(c);
            if (it && it->IsLockableContainer()) continue;
          }
          maxpt = pt;
          selected = cursel;
          foundSomething = true;
        }
      }

      if (Flags & SELECT_ZEROPICK_FIRST) {
        int cursel = -1;
        for (uInt c = 0; c < Contents.GetLength(); ++c) {
          if (!Contents.IsEntrySelectable(c)) continue;
          ++cursel;
          if (Flags & DONT_SELECT_CONTAINERS) {
            item *it = (item *)Contents.GetEntryUPtr(c);
            if (it && it->IsLockableContainer()) continue;
          }
          feuLong pt = Contents.GetEntryUData(c);
          if (pt == 0) {
            selected = cursel;
            foundSomething = true;
            break;
          }
        }
      }
    }

    if (!foundSomething && Contents.GetLength() &&
        (Flags & (SS_STACK_TOP | SS_STACK_BOTTOM)) != 0)
    {
      int cursel = -1;
      int xpos = -1;
      for (uInt c = 0; c < Contents.GetLength(); c += 1) {
        if (!Contents.IsEntrySelectable(c)) continue;
        cursel += 1;
        item *it = (item *)Contents.GetEntryUPtr(c);
        if (it && ((Flags & DONT_SELECT_CONTAINERS) == 0 || !it->IsLockableContainer())) {
          const int cc = SearchItem(it);
          if (cc >= 0) {
            bool ok = false;
            if (Flags & SS_STACK_TOP) {
              ok = (cc > xpos);
            } else if (Flags & SS_STACK_BOTTOM) {
              ok = (cc < xpos);
            }
            if (xpos < 0 || ok) {
              xpos = cc;
              selected = cursel;
              foundSomething = true;
            }
          }
        }
      }
    }

    if (selected < 0 && !foundSomething && (Flags & REMEMBER_SELECTED)) {
      selected = GetSelected();
    }
    Contents.SetSelected(selected);
  }

  int Chosen;
  for (;;) {
    game::DrawEverythingNoBlit(); //doesn't prevent mirage puppies
    Chosen = Contents.Draw();
    if (Chosen == EXCLAM_PRESSED) {
      itemvector XVector;
      Selected = Contents.GetSelected();
      int XPos = 0;
      if (Flags & NONE_AS_CHOICE) {
        if (Selected == 0) continue;
        XPos += 1;
      }
      if (MergeStack) {
        XPos = MergeStack->SearchChosen(XVector, Viewer, XPos, Selected, Flags, CENTER,
                                        SorterFunction, false);
      }
      if (XVector.empty()) {
        XPos = SearchChosen(XVector, Viewer, XPos, Selected, Flags, CENTER,
                            SorterFunction, false);
      }
      /*
      if (XVector.empty()) {
        for (int c = 0; c != 4; c += 1) {
          if (AdjacentStack[c]) {
            AdjacentStack[c]->SearchChosen(XVector, Viewer, XPos, Selected, Flags, 3-c, SorterFunction);
            if (!XVector.empty()) break;
          }
        }
      }
      */
      if (!XVector.empty()) {
        festring title;
        title << "Note for \1Y";
        if (XVector.size() == 1) {
          title << XVector[0]->CHAR_NAME(DEFINITE);
        } else {
          title << XVector[0]->CHAR_NAME(DEFINITE | PLURAL);
        }
        title << "\2 (0-32 letters):";
        festring note = game::GetItemNote(XVector[0]);
        truth aborted = false;
        festring nn = game::StringQuestionEx(title, note, WHITE, 0, 32, /*AllowExit*/true, 0, &aborted);
        if (!aborted) {
          for (size_t f = 0; f != XVector.size(); f += 1) {
            game::SetItemNote(XVector[f], nn);
          }
          festring text = Contents.GetSelectableEntry(Selected);
          if (!text.IsEmpty()) {
            auto pos = text.Find(" \1#18eda4|(");
            if (pos != festring::NPos) text.Left((int)pos);
            if (!nn.IsEmpty()) {
              text << " \1#18eda4|(" << nn << ")\2";
            }
            Contents.UpdateSelectableEntry(Selected, text);
          }
        }
      }
      continue;
    }
    break;
  }

  game::ClearItemDrawVector();

  if (Chosen == TAB_PRESSED) {
    Selected = Contents.GetSelected();
    return Chosen;
  }

  /*
  if (Chosen == EXCLAM_PRESSED) {
    Selected = Contents.GetSelected();
    return Chosen;
  }
  */

  if (Chosen == LIST_WAS_EMPTY && (Flags & STACK_ALLOW_TAB) != 0) {
    Selected = 0;
    return TAB_PRESSED;
  }

  if (Chosen & FELIST_ERROR_BIT) {
    Selected = 0;
    return Chosen;
  }
  Selected = Chosen;

  int Pos = 0;

  if (Flags & NONE_AS_CHOICE) {
    if (!Selected) return 0;
    ++Pos;
  }

  if (MergeStack) {
    Pos = MergeStack->SearchChosen(ReturnVector, Viewer, Pos, Selected, Flags, CENTER,
                                   SorterFunction);
    if (!ReturnVector.empty()) return 0;
  }

  Pos = SearchChosen(ReturnVector, Viewer, Pos, Selected, Flags, CENTER, SorterFunction);

  if (!ReturnVector.empty()) return 0;

  for (int c = 0; c != 4; c += 1) {
    if (AdjacentStack[c]) {
      AdjacentStack[c]->SearchChosen(ReturnVector, Viewer, Pos, Selected, Flags, 3-c,
                                     SorterFunction);
      if (!ReturnVector.empty()) break;
    }
  }

  return 0;
}


//==========================================================================
//
//  stack::AddContentsToList
//
//  Internal function to fill Contents list
//
//==========================================================================
void stack::AddContentsToList (felist &Contents, ccharacter *Viewer,
                               cfestring &Desc, int Flags,
                               int RequiredSquarePosition, sorter SorterFunction,
                               item *hiitem) const
{
  itemvectorvector PileVector;
  Pile(PileVector, Viewer, RequiredSquarePosition, SorterFunction,
       false, !!(Flags & STACK_ALLOW_NAMING));
  truth DrawDesc = Desc.GetSize();
  sLong LastCategory = 0;
  festring Entry;

  room *Room = 0; // for sell prices
  #if 0
  if (Flags & SS_SHOW_SELL_PRICE) {
    if (!MotherEntity) {
      ConLogf("no mother entity!");
    } else if (!MotherEntity->IsCharacter()) {
      ConLogf("mother entity is not a character!");
    } else {
      ConLogf("prices!");
    }
  }
  #endif

  if (MotherEntity && MotherEntity->IsCharacter() && (Flags & SS_SHOW_SELL_PRICE)) {
    Room = ((character *)MotherEntity)->GetRoom();
    if (Room && !Room->IsShopActive((character *)MotherEntity)) {
      ConLogf("the shop is inactive!");
      Room = 0;
    } else if (!Room) {
      ConLogf("not a shop!");
    }
    #if 0
    else {
      ConLogf("in a shop.");
    }
    #endif
  }

  int gpwdt = 0;
  if (Room && (Flags & SS_SHOW_SELL_PRICE)) {
    for (uInt p = 0; p < PileVector.size(); ++p) {
      item *Item = PileVector[p].back();
      if (Room->CanBeSoldToShop((character *)MotherEntity, Item)) {
        sLong price = Room->GetItemSellPrice((character *)MotherEntity, Item);
        if (price > 0) {
          Entry = CONST_S("\1Y") + price + "\1#aa0|\x11gp";
        } else {
          Entry = CONST_S("\1#a00|0\x11gp\2");
        }
      } else {
        Entry = CONST_S("\1#a00|0\x11gp\2");
      }
      gpwdt = Max(gpwdt, FONT->TextWidth(Entry));
    }
  }

  for (size_t p = 0; p != PileVector.size(); p += 1) {
    if (DrawDesc) {
      if (!Contents.IsEmpty()) {
        Contents.AddEntry(CONST_S(""), WHITE, 0, NO_IMAGE, false);
      }
      Contents.AddEntry(Desc, WHITE, 0, NO_IMAGE, false);
      Contents.AddEntry(CONST_S(""), WHITE, 0, NO_IMAGE, false);
      DrawDesc = false;
    }

    item *Item = PileVector[p].back();
    if (Item->GetCategory() != LastCategory) {
      LastCategory = Item->GetCategory();
      Contents.AddEntry(CONST_S(item::GetItemCategoryName(LastCategory)),
                        /*LIGHT_GRAY*/ORANGE, 0, NO_IMAGE, false);
    }

    Entry.Empty();
    Item->AddInventoryEntry(Viewer, Entry, PileVector[p].size(), !(Flags & NO_SPECIAL_INFO));
    if (Flags & STACK_ALLOW_NAMING) {
      festring itname = game::GetItemNote(Item);
      if (!itname.IsEmpty()) {
        //Entry << " \1#ed9b18|(" << itname << ")\2";
        Entry << " \1#18eda4|(" << itname << ")\2";
      }
    }

    if (Room && (Flags & SS_SHOW_SELL_PRICE)) {
      festring prc;
      if (Room->CanBeSoldToShop((character *)MotherEntity, Item)) {
        sLong price = Room->GetItemSellPrice((character *)MotherEntity, Item);
        #if 0
        ConLogf("item [%s:%s]; price=%d",
                Item->GetClassID(), Item->GetConfigName().CStr(), price);
        #endif
        //Entry << "\n\1GPRICE\2: \1Y" << price << "\2";
        //Entry = CONST_S("\1Y[") + price + "\x11gp]\2 " + Entry;
        if (price > 0) {
          prc = CONST_S("\1Y") + price + "\1#aa0|\x11gp\2";
        } else {
          prc = CONST_S("\1#a00|0\x11gp\2");
        }
      } else {
        prc = CONST_S("\1#a00|0\x11gp\2");
      }
      FONT->LPadToPixWidth(prc, gpwdt, true);
      Entry = prc + NBSP + Entry;
    }

    int ImageKey = game::AddToItemDrawVector(PileVector[p]);
    Contents.AddEntry(Entry, (Item == hiitem ? WHITE : LIGHT_GRAY), 0,
                      ImageKey, true, Item->pickupTime, (void *)Item);
    Contents.AddLastEntryHelp(Item->GetDescriptiveInfo());
  }
}


//==========================================================================
//
//  stack::SearchChosen
//
//  Internal function which fills ReturnVector according to Chosen,
//  which is given by felist::Draw, and possibly the user's additional
//  input about item amount.
//
//==========================================================================
int stack::SearchChosen (itemvector &ReturnVector, ccharacter *Viewer, int Pos,
                         int Chosen, int Flags, int RequiredSquarePosition,
                         sorter SorterFunction, truth doAsk) const
{
  /* Not really efficient... :( */
  itemvectorvector PileVector;
  Pile(PileVector, Viewer, RequiredSquarePosition, SorterFunction,
       false, !!(Flags & STACK_ALLOW_NAMING));
  for (size_t p = 0; p != PileVector.size(); ++p) {
    if (Pos++ == Chosen) {
      if (!doAsk) {
        ReturnVector.assign(PileVector[p].begin(), PileVector[p].end());
        return -1;
      }
      if (Flags & NO_MULTI_SELECT) {
        int Amount = ((Flags & SELECT_PAIR) && PileVector[p][0]->HandleInPairs() &&
                      PileVector[p].size() >= 2 ? 2 : 1);
        ReturnVector.assign(PileVector[p].end() - Amount, PileVector[p].end());
        return -1;
      } else {
        int Amount = (int)PileVector[p].size();
        if (Amount > 1) {
          Amount = game::ScrollBarQuestion(CONST_S("How many ")+PileVector[p][0]->GetName(PLURAL)+'?',
            Amount, 1, 0, Amount, 0, WHITE,
            LIGHT_GRAY, DARK_GRAY);
        }
        ReturnVector.assign(PileVector[p].end() - Amount, PileVector[p].end());
        return -1;
      }
    }
  }
  return Pos;
}


//==========================================================================
//
//  stack::RaiseTheDead
//
//==========================================================================
truth stack::RaiseTheDead (character *Summoner) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  for (uInt c = 0; c < ItemVector.size(); ++c) if (ItemVector[c]->RaiseTheDead(Summoner)) return true;
  return false;
}


//==========================================================================
//
//  stack::KeyCanOpen
//
//==========================================================================
truth stack::KeyCanOpen (item* Key, character* Applier) const {
  if (!Applier->IsPlayer()) return false;
  //TODO: find item to apply?
  return false;
}


//==========================================================================
//
//  stack::TryKey
//
//  Returns false if the Applier didn't try to use the key
//
//==========================================================================
truth stack::TryKey (item *Key, character *Applier, bool silent) {
  if (!Applier->IsPlayer()) return false;
  item *ToBeOpened = DrawContents(Applier, CONST_S("Where do you wish to use the key?"), 0, &item::HasLock);
  if (!ToBeOpened) return false;
  return ToBeOpened->TryKey(Key, Applier, silent);
}


//==========================================================================
//
//  stack::Open
//
//  Returns false if the Applier didn't try to open anything
//
//==========================================================================
truth stack::Open (character *Opener) {
  if (!Opener->IsPlayer()) return false;
  item *ToBeOpened = DrawContents(Opener, CONST_S("What do you wish to open?"), 0, &item::IsOpenable);
  return (ToBeOpened ? ToBeOpened->Open(Opener) : false);
}


//==========================================================================
//
//  stack::GetSideItems
//
//==========================================================================
int stack::GetSideItems (int RequiredSquarePosition) const {
  int VisibleItems = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == RequiredSquarePosition) {
      VisibleItems += 1;
    }
  }
  return VisibleItems;
}


//==========================================================================
//
//  stack::GetVisibleItems
//
//==========================================================================
int stack::GetVisibleItems (ccharacter *Viewer) const {
  int VisibleItems = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == CENTER && i->CanBeSeenBy(Viewer)) {
      VisibleItems += 1;
    }
  }
  lsquare *Square = GetLSquareUnder();
  for (int c = 0; c < 4; ++c) {
    stack *Stack = Square->GetStackOfAdjacentSquare(c);
    if (Stack) {
      VisibleItems += Stack->GetVisibleSideItems(Viewer, 3-c);
    }
  }
  return VisibleItems;
}


//==========================================================================
//
//  stack::GetVisibleItemsV
//
//==========================================================================
void stack::GetVisibleItemsV (ccharacter *Viewer, std::vector<item *> &vi) {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == CENTER && i->CanBeSeenBy(Viewer)) {
      vi.push_back(*i);
    }
  }
  lsquare *Square = GetLSquareUnder();
  for (int c = 0; c < 4; ++c) {
    stack *Stack = Square->GetStackOfAdjacentSquare(c);
    if (Stack) {
      //VisibleItems += Stack->GetVisibleSideItems(Viewer, 3-c);
      for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
        if (i->GetSquarePosition() == (3-c) && i->CanBeSeenBy(Viewer)) {
          vi.push_back(*i);
        }
      }
    }
  }
}


//==========================================================================
//
//  stack::HasSomethingFunnyFor
//
//  used in "Go" command
//
//==========================================================================
truth stack::HasSomethingFunnyFor (ccharacter *Viewer) {
  IvanAssert(Viewer);
  /*
  std::vector<item *> vi;
  GetVisibleItemsV(Viewer, vi);
  for (size_t f = 0; f != vi.size(); f += 1) {
    if (Viewer->IsFunnyItem(vi[f])) return true;
  }
  return false;
  */

  // `GetVisibleItemsV()`, but without a vector

  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == CENTER && i->CanBeSeenBy(Viewer)) {
      if (Viewer->IsFunnyItem(*i)) return true;
    }
  }

  lsquare *Square = GetLSquareUnder();
  for (int c = 0; c < 4; ++c) {
    stack *Stack = Square->GetStackOfAdjacentSquare(c);
    if (Stack) {
      for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
        if (i->GetSquarePosition() == (3-c) && i->CanBeSeenBy(Viewer)) {
          if (Viewer->IsFunnyItem(*i)) return true;
        }
      }
    }
  }

  return false;
}


//==========================================================================
//
//  stack::SetSteppedOn
//
//==========================================================================
void stack::SetSteppedOn (truth v) {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == CENTER) {
      i->SetSteppedOn(v);
    }
  }
  lsquare *Square = GetLSquareUnder();
  for (int c = 0; c < 4; ++c) {
    stack *Stack = Square->GetStackOfAdjacentSquare(c);
    if (Stack) {
      for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
        if (i->GetSquarePosition() == 3 - c) {
          i->SetSteppedOn(v);
        }
      }
    }
  }
}


//==========================================================================
//
//  stack::GetNativeVisibleItems
//
//==========================================================================
int stack::GetNativeVisibleItems (ccharacter *Viewer) const {
  if (Flags & HIDDEN) return Items;
  int VisibleItems = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->CanBeSeenBy(Viewer)) {
      VisibleItems += 1;
    }
  }
  return VisibleItems;
}


//==========================================================================
//
//  stack::GetVisibleSideItems
//
//==========================================================================
int stack::GetVisibleSideItems (ccharacter *Viewer, int RequiredSquarePosition) const {
  int VisibleItems = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == RequiredSquarePosition && i->CanBeSeenBy(Viewer)) {
      VisibleItems += 1;
    }
  }
  return VisibleItems;
}


//==========================================================================
//
//  stack::GetBottomVisibleItem
//
//==========================================================================
item *stack::GetBottomVisibleItem (ccharacter *Viewer) const {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if ((Flags & HIDDEN) || i->CanBeSeenBy(Viewer)) {
      return *i;
    }
  }
  return 0;
}


//==========================================================================
//
//  stack::SignalVolumeAndWeightChange
//
//==========================================================================
void stack::SignalVolumeAndWeightChange () {
  if (!(Flags & FREEZED)) {
    CalculateVolumeAndWeight();
    if (MotherEntity) {
      MotherEntity->SignalVolumeAndWeightChange();
    }
  }
}


//==========================================================================
//
//  stack::CalculateVolumeAndWeight
//
//==========================================================================
void stack::CalculateVolumeAndWeight () {
  Volume = Weight = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    Volume += i->GetVolume();
    Weight += i->GetWeight();
  }
}


//==========================================================================
//
//  stack::SignalEmitationIncrease
//
//==========================================================================
void stack::SignalEmitationIncrease (int ItemSquarePosition, col24 EmitationUpdate) {
  if (ItemSquarePosition < CENTER) {
    stack *Stack = GetLSquareUnder()->GetStackOfAdjacentSquare(ItemSquarePosition);
    if (Stack) Stack->SignalEmitationIncrease(CENTER, EmitationUpdate);
    return;
  }
  if (!(Flags & FREEZED) && game::CompareLights(EmitationUpdate, Emitation) > 0) {
    Emitation = game::CombineConstLights(Emitation, EmitationUpdate);
    if (MotherEntity) {
      if (MotherEntity->AllowContentEmitation()) {
        MotherEntity->SignalEmitationIncrease(EmitationUpdate);
      }
    } else {
      GetLSquareUnder()->SignalEmitationIncrease(EmitationUpdate);
    }
  }
}


//==========================================================================
//
//  stack::SignalEmitationDecrease
//
//==========================================================================
void stack::SignalEmitationDecrease (int ItemSquarePosition, col24 EmitationUpdate) {
  if (ItemSquarePosition < CENTER) {
    stack *Stack = GetLSquareUnder()->GetStackOfAdjacentSquare(ItemSquarePosition);
    if (Stack) {
      Stack->SignalEmitationDecrease(CENTER, EmitationUpdate);
    }
    return;
  }
  if (!(Flags & FREEZED) && Emitation && game::CompareLights(EmitationUpdate, Emitation) >= 0) {
    col24 Backup = Emitation;
    CalculateEmitation();
    if (Backup != Emitation) {
      if (MotherEntity) {
        if (MotherEntity->AllowContentEmitation()) {
          MotherEntity->SignalEmitationDecrease(EmitationUpdate);
        }
      } else {
        GetLSquareUnder()->SignalEmitationDecrease(EmitationUpdate);
      }
    }
  }
}


//==========================================================================
//
//  stack::CalculateEmitation
//
//==========================================================================
void stack::CalculateEmitation () {
  Emitation = GetSideEmitation(CENTER);
}


//==========================================================================
//
//  stack::GetSideEmitation
//
//==========================================================================
col24 stack::GetSideEmitation (int RequiredSquarePosition) {
  col24 Emitation = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == RequiredSquarePosition) {
      game::CombineLights(Emitation, i->GetEmitation());
    }
  }
  return Emitation;
}


//==========================================================================
//
//  stack::CanBeSeenBy
//
//==========================================================================
truth stack::CanBeSeenBy (ccharacter *Viewer, int SquarePosition) const {
  if (MotherEntity) {
    return MotherEntity->ContentsCanBeSeenBy(Viewer);
  }
  lsquare *Square = GetLSquareTrulyUnder(SquarePosition);
  return (Viewer->IsOver(Square->GetPos()) || Square->CanBeSeenBy(Viewer));
}


//==========================================================================
//
//  stack::IsDangerous
//
//==========================================================================
truth stack::IsDangerous (ccharacter *Stepper) const {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->IsDangerous(Stepper) && i->CanBeSeenBy(Stepper)) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  stack::Duplicate
//
//  Returns true if something was duplicated.
//  Max is the cap of items to be affected.
//
//==========================================================================
truth stack::Duplicate (int Max, feuLong Flags) {
  if (!GetItems()) return false;
  itemvector ItemVector;
  FillItemVector(ItemVector);
  int p = 0;
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->Exists() && ItemVector[c]->DuplicateToStack(this, Flags) && ++p == Max) {
      break;
    }
  }
  return p > 0;
}


//==========================================================================
//
//  stack::AddElement
//
//  Adds the item without any external update requests
//
//==========================================================================
void stack::AddElement (item *Item, truth) {
  ++Items;
  /* "I love writing illegible code." - Guy who wrote this */
  (Top = (Bottom ? Top->Next : Bottom) = new stackslot(this, Top))->PutInItem(Item);
}


//==========================================================================
//
//  stack::RemoveElement
//
//  Removes the slot without any external update requests
//
//==========================================================================
void stack::RemoveElement (stackslot *Slot) {
  --Items;
  (Slot->Last ? Slot->Last->Next : Bottom) = Slot->Next;
  (Slot->Next ? Slot->Next->Last : Top) = Slot->Last;
  delete Slot;
}


//==========================================================================
//
//  stack::MoveItemsTo
//
//==========================================================================
void stack::MoveItemsTo (stack *Stack) {
  while (Items) {
    GetBottom()->MoveTo(Stack);
  }
}


//==========================================================================
//
//  stack::MoveItemsTo
//
//==========================================================================
void stack::MoveItemsTo (slot *Slot) {
  while (Items) {
    Slot->AddFriendItem(*GetBottom());
  }
}


//==========================================================================
//
//  stack::GetBottomItem
//
//==========================================================================
item *stack::GetBottomItem (ccharacter *Char, truth ForceIgnoreVisibility) const {
  if ((Flags & HIDDEN) || ForceIgnoreVisibility) {
    return Bottom ? **Bottom : 0;
  }
  return GetBottomVisibleItem(Char);
}


//==========================================================================
//
//  stack::GetBottomSideItem
//
//==========================================================================
item *stack::GetBottomSideItem (ccharacter *Char, int RequiredSquarePosition,
                                truth ForceIgnoreVisibility) const
{
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if ((i->GetSquarePosition() == RequiredSquarePosition && (Flags & HIDDEN)) ||
        ForceIgnoreVisibility || i->CanBeSeenBy(Char))
    {
      return *i;
    }
  }
  return 0;
}


//==========================================================================
//
//  CategorySorter
//
//==========================================================================
truth CategorySorter (const itemvector &V1, const itemvector &V2) {
  return (*V1.begin())->GetCategory() < (*V2.begin())->GetCategory();
}


//==========================================================================
//
//  stack::SetScrollInspected
//
//==========================================================================
void stack::SetScrollInspected () {
  for (stackiterator s = GetBottom(); s.HasItem(); ++s) {
    (*s)->SetScrollInspected();
  }
}


//==========================================================================
//
//  stack::Pile
//
//  Slow function which sorts the stack's contents to a vector of piles
//  (itemvectors) of which elements are similiar to each other, for instance
//  4 bananas
//
//==========================================================================
void stack::Pile (itemvectorvector &PileVector, ccharacter *Viewer,
                  int RequiredSquarePosition, sorter SorterFunction,
                  truth ForLook, truth itemNames) const
{
  if (!Items) return;

  std::list<item *> List;
  for (stackiterator s = GetBottom(); s.HasItem(); ++s) {
    if (s->GetSquarePosition() == RequiredSquarePosition &&
        (SorterFunction == 0 || ((*s)->*SorterFunction)(Viewer)) &&
        ((Flags & HIDDEN) || s->CanBeSeenBy(Viewer)))
    {
      List.push_back(*s);
    }
  }

  for (std::list<item *>::iterator i = List.begin(); i != List.end(); ++i) {
    PileVector.resize(PileVector.size() + 1);
    itemvector &Pile = PileVector.back();
    item *CurItem = *i;
    Pile.push_back(CurItem);
    if (ForLook && CurItem->IsGenericScrollDescr()) {
      // pile all generic scrolls, the player cannot distinguish them with "L"ook
      std::list<item *>::iterator j = i;
      for (++j; j != List.end(); ) {
        if ((*j)->IsGenericScrollDescr()) {
          Pile.push_back(*j);
          std::list<item *>::iterator Dirt = j++;
          List.erase(Dirt);
        } else {
          ++j;
        }
      }
    } else {
      bool canBePiled = CurItem->CanBePiled();
      /*
      if (canBePiled && itemNames && game::HasItemNote(CurItem)) {
        canBePiled = false;
      }
      */
      if (canBePiled) {
        festring iname;
        if (itemNames) iname = game::GetItemNote(CurItem);
        std::list<item *>::iterator j = i;
        for (++j; j != List.end(); ) {
          item *JItem = *j;
          bool canBePiled2nd = JItem->CanBePiled();
          if (canBePiled2nd && itemNames) {
            if (game::GetItemNote(JItem) != iname) {
              canBePiled2nd = false;
            }
          }
          if (canBePiled2nd && CurItem->CanBePiledWith(JItem, Viewer)) {
            Pile.push_back(JItem);
            std::list<item *>::iterator Dirt = j++;
            List.erase(Dirt);
          } else {
            ++j;
          }
        }
      }
    }
  }

  std::stable_sort(PileVector.begin(), PileVector.end(), CategorySorter);
}


//==========================================================================
//
//  stack::GetTruePrice
//
//  Total price of the stack
//
//==========================================================================
sLong stack::GetTruePrice () const {
  sLong Price = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    Price += i->GetTruePrice();
  }
  return Price;
}


//==========================================================================
//
//  ListOfNothing
//
//==========================================================================
static int ListOfNothing (cfestring &title) {
  felist List(title);
  int ImageKey = game::AddToItemDrawVector(itemvector());
  List.AddEntry(CONST_S("nothing"), /*LIGHT_GRAY*/MakeRGB16(196, 196, 0), 0, ImageKey);
  game::SetStandardListAttributes(List);
  List.SetPageLength(12);
  List.RemoveFlags(BLIT_AFTERWARDS);
  List.SetEntryDrawer(game::ItemEntryDrawer);
  List.AddFlags(FE_ALLOW_TAB);
  int res = -666;
  while (res < -1) {
    switch (List.Draw()) {
      case TAB_PRESSED: res = 0; break;
      case ESCAPED: res = -1; break;
    }
  }
  game::ClearItemDrawVector();
  return res;
}


//==========================================================================
//
//  stack::TakeSomethingFrom
//
//  GUI used for instance by chests and bookcases.
//
//  result: 0:tab; -1:esc
//
//==========================================================================
int stack::TakeSomethingFrom (character *Opener, cfestring &ContainerName, truth &WasAction,
                              int &XSelection)
{
  if (!GetItems()) {
    //ADD_MESSAGE("There is nothing in %s.", ContainerName.CStr());
    return ListOfNothing(CONST_S("There is nothing to \1Gtake\2 from ") + ContainerName + "! \1Y[Tab]\2");
  }
  int res = -1, choosen;
  truth Success = false;
  room *Room = GetLSquareUnder()->GetRoom();
  SetSelected(Max(0, XSelection));
  for (;;) {
    itemvector ToTake;
    game::DrawEverythingNoBlit();
    choosen = DrawContents(ToTake, Opener,
                           CONST_S("What do you want to \1Gtake\2 from ") + ContainerName +
                             "? \1Y[Tab]\2",
                           REMEMBER_SELECTED|DONT_SELECT_CONTAINERS|
                           STACK_ALLOW_TAB|STACK_ALLOW_NAMING);
    XSelection = Selected;
    if (choosen == TAB_PRESSED) { res = 0; break; }
    if (ToTake.empty()) break;
    if (!IsOnGround() || !Room || Room->PickupItem(Opener, ToTake[0], ToTake.size())) {
      for (size_t c = 0; c != ToTake.size(); c += 1) {
        ToTake[c]->MoveTo(Opener->GetStack(), -1);
      }
      ADD_MESSAGE("You take %s from %s.",
                  ToTake[0]->GetName(DEFINITE, ToTake.size()).CStr(), ContainerName.CStr());
      Success = true;
    }
  }
  WasAction = WasAction || Success;
  return res;
}


//==========================================================================
//
//  stack::PutSomethingIn
//
//  GUI used for instance by chests and bookcases (use ContainerID == 0 if
//  the container isn't an item).
//
//  result: 0:tab; -1:esc
//
//==========================================================================
int stack::PutSomethingIn (character *Opener, cfestring &ContainerName,
                           sLong StorageVolume, feuLong ContainerID,
                           truth &WasAction, int &XSelection)
{
  if (!Opener->GetStack()->GetItems()) {
    //ADD_MESSAGE("You have nothing to put in %s.", ContainerName.CStr());
    return ListOfNothing(CONST_S("There is nothing to \1Gput\2 in ") + ContainerName + "! \1Y[Tab]\2");
  }
  int res = -1, choosen;
  truth Success = false;
  room *Room = GetLSquareUnder()->GetRoom();
  SetSelected(Max(0, XSelection));
  for (;;) {
    itemvector ToPut;
    game::DrawEverythingNoBlit();
    choosen = Opener->GetStack()->DrawContents(ToPut, Opener,
                                    CONST_S("What do you want to \1Gput\2 in ") + ContainerName +
                                      "? \1Y[Tab]\2",
                                    REMEMBER_SELECTED|SELECT_MOST_RECENT|
                                    DONT_SELECT_CONTAINERS|STACK_ALLOW_TAB|
                                    STACK_ALLOW_NAMING);
    XSelection = Selected;
    if (choosen == TAB_PRESSED) { res = 0; break; }
    if (ToPut.empty()) break;
    if (ToPut[0]->GetID() == ContainerID) {
      ADD_MESSAGE("You can't put %s inside itself!", ContainerName.CStr());
      continue;
    }
    //size_t Amount = Min<size_t>((StorageVolume - GetVolume()) / ToPut[0]->GetVolume(), ToPut.size());
    size_t Amount;
    if (StorageVolume < GetVolume()) {
      Amount = 0;
    } else {
      Amount = Min<size_t>((StorageVolume - GetVolume()) / ToPut[0]->GetVolume(), ToPut.size());
    }
    if (!Amount) {
      if (ToPut.size() == 1) {
        ADD_MESSAGE("%s doesn't fit in %s.",
                    ToPut[0]->CHAR_NAME(DEFINITE), ContainerName.CStr());
      } else {
        ADD_MESSAGE("None of the %d %s fit in %s.",
                    int(ToPut.size()), ToPut[0]->CHAR_NAME(PLURAL), ContainerName.CStr());
      }
    } else {
      if (Amount != ToPut.size()) {
        ADD_MESSAGE("Only %d of the %d %s fit%s in %s.", Amount, int(ToPut.size()),
                    ToPut[0]->CHAR_NAME(PLURAL), Amount == 1 ? "s" : "", ContainerName.CStr());
      }
      if (!IsOnGround() || !Room || Room->DropItem(Opener, ToPut[0], Amount)) {
        for (size_t c = 0; c != Amount; ++c) {
          ToPut[c]->MoveTo(this);
        }
        ADD_MESSAGE("You put %s in %s.",
                    ToPut[0]->GetName(DEFINITE, Amount).CStr(), ContainerName.CStr());
        Success = true;
      }
    }
  }
  WasAction = WasAction || Success;
  return res;
}


//==========================================================================
//
//  stack::IsOnGround
//
//==========================================================================
truth stack::IsOnGround () const {
  return (!MotherEntity || MotherEntity->IsOnGround());
}


//==========================================================================
//
//  stack::GetSpoiledItems
//
//==========================================================================
int stack::GetSpoiledItems () const {
  int Counter = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    Counter += (i->GetSpoilLevel() > 0); // even though this is pretty unclear, it isn't mine but Hex's
  }
  return Counter;
}


//==========================================================================
//
//  stack::SortAllItems
//
//  Adds all items and recursively their contents
//  which satisfy the sorter to ItemVector
//
//==========================================================================
void stack::SortAllItems (const sortdata &SortData) const {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    i->SortAllItems(SortData);
  }
}


//==========================================================================
//
//  stack::Search
//
//  Search for traps and other secret items
//
//==========================================================================
void stack::Search (ccharacter *Char, int Perception) {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    i->Search(Char, Perception);
  }
}


//==========================================================================
//
//  stack::NeedDangerSymbol
//
//  Used to determine whether the danger symbol should be shown
//
//==========================================================================
truth stack::NeedDangerSymbol (ccharacter *Viewer) const {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->NeedDangerSymbol() && i->CanBeSeenBy(Viewer)) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  stack::PreProcessForBone
//
//==========================================================================
void stack::PreProcessForBone () {
  if (Items) {
    itemvector ItemVector;
    FillItemVector(ItemVector);
    for (uInt c = 0; c < ItemVector.size(); ++c) {
      ItemVector[c]->PreProcessForBone();
    }
  }
}


//==========================================================================
//
//  stack::PostProcessForBone
//
//==========================================================================
void stack::PostProcessForBone () {
  if (Items) {
    itemvector ItemVector;
    FillItemVector(ItemVector);
    for (uInt c = 0; c < ItemVector.size(); ++c) {
      ItemVector[c]->PostProcessForBone();
    }
  }
}


//==========================================================================
//
//  stack::FinalProcessForBone
//
//==========================================================================
void stack::FinalProcessForBone () {
  /* Items can't be removed during the final processing stage */
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    i->FinalProcessForBone();
  }
}


//==========================================================================
//
//  stack::SpillFluid
//
//  VolumeModifier increases the spilled liquid's volume.
//  Note that the original liquid isn't placed anywhere nor deleted,
//  but its volume is decreased (possibly to zero).
//
//==========================================================================
void stack::SpillFluid (character *Spiller, liquid *Liquid, sLong VolumeModifier) {
  if (!Items) return;
  if (!Liquid->GetVolume()) return;
  double ChanceMultiplier = 1.0/(300+sqrt(Volume));
  itemvector ItemVector;
  FillItemVector(ItemVector);
  for (int c = ItemVector.size()-1; c >= 0; --c) {
    if (ItemVector[c]->Exists() && ItemVector[c]->AllowFluids()) {
      sLong ItemVolume = ItemVector[c]->GetVolume();
      double Root = sqrt(ItemVolume);
      if (Root > RAND_N(200) || Root > RAND_N(200)) {
        sLong SpillVolume = sLong(VolumeModifier*Root*ChanceMultiplier);
        if (SpillVolume) {
          Liquid->EditVolume(-Max(SpillVolume, Liquid->GetVolume()));
          ItemVector[c]->SpillFluid(Spiller, Liquid->SpawnMoreLiquid(SpillVolume),
                                    ItemVector[c]->GetSquareIndex(GetPos()));
          if (!Liquid->GetVolume()) return;
        }
      }
    }
  }
}


//==========================================================================
//
//  stack::AddItems
//
//==========================================================================
void stack::AddItems (const itemvector &ItemVector) {
  for (uInt c = 0; c < ItemVector.size(); ++c) AddItem(ItemVector[c]);
}


//==========================================================================
//
//  stack::MoveItemsTo
//
//==========================================================================
void stack::MoveItemsTo (itemvector &ToVector, int RequiredSquarePosition) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->GetSquarePosition() == RequiredSquarePosition) {
      ItemVector[c]->RemoveFromSlot();
      ToVector.push_back(ItemVector[c]);
    }
  }
}


//==========================================================================
//
//  stack::DropSideItems
//
//==========================================================================
void stack::DropSideItems () {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    int SquarePosition = i->GetSquarePosition();
    if (SquarePosition != CENTER) {
      if (i->IsAnimated()) {
        lsquare *Square = GetLSquareTrulyUnder(SquarePosition);
        if (Square) {
          Square->DecStaticAnimatedEntities();
        }
        GetLSquareUnder()->IncStaticAnimatedEntities();
      }
      i->SignalSquarePositionChange(CENTER);
      SignalEmitationDecrease(SquarePosition, i->GetEmitation());
      SignalEmitationIncrease(CENTER, i->GetEmitation());
    }
  }
}


//==========================================================================
//
//  stack::AllowDamage
//
//==========================================================================
truth stack::AllowDamage (int Direction, int SquarePosition) {
  if (SquarePosition == CENTER) return true;
  switch (Direction) {
    case 0: return SquarePosition == DOWN || SquarePosition == RIGHT;
    case 1: return SquarePosition == DOWN;
    case 2: return SquarePosition == DOWN || SquarePosition == LEFT;
    case 3: return SquarePosition == RIGHT;
    case 4: return SquarePosition == LEFT;
    case 5: return SquarePosition == UP || SquarePosition == RIGHT;
    case 6: return SquarePosition == UP;
    case 7: return SquarePosition == UP || SquarePosition == LEFT;
  }
  return true;
}


//==========================================================================
//
//  stack::GetWeight
//
//==========================================================================
sLong stack::GetWeight (ccharacter *Viewer, int SquarePosition) const {
  sLong Weight = 0;
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->GetSquarePosition() == SquarePosition &&
        ((Flags & HIDDEN) || i->CanBeSeenBy(Viewer)))
    {
      Weight += i->GetWeight();
    }
  }
  return Weight;
}


//==========================================================================
//
//  stack::DetectMaterial
//
//==========================================================================
truth stack::DetectMaterial (cmaterial *Material) const {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    if (i->DetectMaterial(Material)) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  stack::SetLifeExpectancy
//
//==========================================================================
void stack::SetLifeExpectancy (int Base, int RandPlus) {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    i->SetLifeExpectancy(Base, RandPlus);
  }
}


//==========================================================================
//
//  stack::Necromancy
//
//==========================================================================
truth stack::Necromancy (character *Necromancer) {
  itemvector ItemVector;
  FillItemVector(ItemVector);
  for (uInt c = 0; c < ItemVector.size(); ++c) {
    if (ItemVector[c]->Necromancy(Necromancer)) {
      return true;
    }
  }
  return false;
}


//==========================================================================
//
//  stack::CalculateEnchantments
//
//==========================================================================
void stack::CalculateEnchantments () {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    i->CalculateEnchantment();
  }
}


//==========================================================================
//
//  stack::FindCarrier
//
//==========================================================================
ccharacter *stack::FindCarrier () const {
  return (MotherEntity ? MotherEntity->FindCarrier() : 0);
}


//==========================================================================
//
//  stack::Haste
//
//==========================================================================
void stack::Haste () {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    i->Haste();
  }
}


//==========================================================================
//
//  stack::Slow
//
//==========================================================================
void stack::Slow () {
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    i->Slow();
  }
}


//==========================================================================
//
//  stack::GeneralHasItem
//
//==========================================================================
truth stack::GeneralHasItem (const character *Char, truth (*chk) (item *i),
                             truth checkContainers) const
{
  for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
    item *it = *i;
    if (it && chk(it)) {
      return true;
    }
  }
  if (checkContainers) {
    for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
      item *it = *i;
      if (it && it->IsOpenable(Char)) {
        stack *cst = it->GetContained();
        if (cst && cst->GeneralHasItem(Char, chk, checkContainers)) {
          return true;
        }
      }
    }
  }
  return false;
}


//==========================================================================
//
//  stack::GeneralRemoveItem
//
//==========================================================================
int stack::GeneralRemoveItem (const character *Char, truth (*chk) (item *i),
                              truth allItems, truth checkContainers)
{
  int count = 0;
  for (;;) {
    bool done = true;
    for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
      item *it = *i;
      if (it && chk(it)) {
        it->RemoveFromSlot();
        it->SendToHell();
        ++count;
        if (!allItems) return count;
        done = false;
        break;
      }
      if (checkContainers && done) {
        for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
          item *it = *i;
          if (it && it->IsOpenable(Char)) {
            stack *cst = it->GetContained();
            if (cst) {
              int rcc = cst->GeneralRemoveItem(Char, chk, allItems, checkContainers);
              if (rcc) {
                count += rcc;
                if (!allItems) return count;
                done = false;
                break;
              }
            }
          }
        }
      }
    }
    if (done) break;
  }
  return count;
}


//==========================================================================
//
//  stack::GeneralRemoveItemTo
//
//==========================================================================
int stack::GeneralRemoveItemTo (const character *Char, truth (*chk) (item *i),
                                character *ToWhom, truth allItems, truth checkContainers)
{
  if (!ToWhom) return GeneralRemoveItem(Char, chk, allItems, checkContainers);
  int count = 0;
  for (;;) {
    bool done = true;
    for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
      item *it = *i;
      if (it && chk(it)) {
        it->RemoveFromSlot();
        ToWhom->ReceiveItemAsPresent(it);
        //it->SendToHell();
        ++count;
        if (!allItems) return count;
        done = false;
        break;
      }
      if (checkContainers && done) {
        for (stackiterator i = GetBottom(); i.HasItem(); ++i) {
          item *it = *i;
          if (it && it->IsOpenable(Char)) {
            stack *cst = it->GetContained();
            if (cst) {
              int rcc = cst->GeneralRemoveItemTo(Char, chk, ToWhom, allItems, checkContainers);
              if (rcc) {
                count += rcc;
                if (!allItems) return count;
                done = false;
                break;
              }
            }
          }
        }
      }
    }
    if (done) break;
  }
  return count;
}
