/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
/* Compiled through trapset.cpp */


//==========================================================================
//
//  trapprototype::trapprototype
//
//==========================================================================
trapprototype::trapprototype (trapspawner Spawner, cchar *ClassID)
  : Spawner(Spawner)
  , ClassID(ClassID)
{
  Index = protocontainer<trap>::Add(this);
}


//==========================================================================
//
//  trap::trap
//
//==========================================================================
trap::trap () : entity(HAS_BE), Next(0) {}


//==========================================================================
//
//  trap::~trap
//
//==========================================================================
trap::~trap () {
}


//==========================================================================
//
//  trap::DebugGetName
//
//==========================================================================
festring trap::DebugGetName () {
  festring res;
  res << "trap:";
  AddTrapName(res, INDEFINITE);
  return res;
}


//==========================================================================
//
//  trap::GetSquareUnderEntity
//
//==========================================================================
square *trap::GetSquareUnderEntity (int) const {
  return LSquareUnder;
}


//==========================================================================
//
//  trap::Save
//
//==========================================================================
void trap::Save (outputfile &SaveFile) const {
}


//==========================================================================
//
//  trap::Load
//
//==========================================================================
void trap::Load (inputfile &) {
  LSquareUnder = static_cast<lsquare *>(game::GetSquareInLoad());
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const trapdata *Data) {
  SaveFile << *Data;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, trapdata *&Data) {
  Data = new trapdata;
  SaveFile >> *Data;
  return SaveFile;
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const trapdata &Data) {
  SaveFile << Data.TrapID << Data.VictimID << Data.BodyParts;
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, trapdata &Data) {
  SaveFile >> Data.TrapID >> Data.VictimID >> Data.BodyParts;
  return SaveFile;
}


//==========================================================================
//
//  trapprototype::SpawnAndLoad
//
//==========================================================================
trap *trapprototype::SpawnAndLoad (inputfile &SaveFile) const {
  trap *Trap = Spawner();
  Trap->Load(SaveFile);
  return Trap;
}


//==========================================================================
//
//  itemtrapbase::Load
//
//==========================================================================
void itemtrapbase::Load (inputfile &SaveFile) {
  SaveFile >> Active >> Team >> DiscoveredByTeam;
}


//==========================================================================
//
//  itemtrapbase::Save
//
//==========================================================================
void itemtrapbase::Save (outputfile &SaveFile) const {
  SaveFile << Active << Team << DiscoveredByTeam;
}


//==========================================================================
//
//  itemtrapbase::CanBeSeenBy
//
//==========================================================================
truth itemtrapbase::CanBeSeenBy (ccharacter *Viewer) const {
  int ViewerTeam = Viewer->GetTeam()->GetID();
  return !Active || ViewerTeam == Team || DiscoveredByTeam.find(ViewerTeam) != DiscoveredByTeam.end();
}


//==========================================================================
//
//  itemtrapbase::Search
//
//==========================================================================
void itemtrapbase::Search (ccharacter *Char, int Perception) {
  int ViewerTeam = Char->GetTeam()->GetID();
  if (Active && ViewerTeam != Team &&
      DiscoveredByTeam.find(ViewerTeam) == DiscoveredByTeam.end() &&
      !RAND_N(200 / Perception))
  {
    DiscoveredByTeam.insert(ViewerTeam);
    SendNewDrawAndMemorizedUpdateRequest();
    if (Char->IsPlayer()) {
      // do not annoy player if he is levitating, as he cannot trigger the trap this way
      ADD_MESSAGE("You find %s.", CHAR_NAME(INDEFINITE));
      if (!Char->StateIsActivated(LEVITATION)) {
        game::AskForEscPress(CONST_S("Trap found!"));
        if (Char->GetAction() && Char->GetAction()->IsGo()) {
          Char->GetAction()->Terminate(false);
        }
      }
    }
  }
}


//==========================================================================
//
//  itemtrapbase::SetIsActive
//
//==========================================================================
void itemtrapbase::SetIsActive (truth What) {
  Active = What;
  UpdatePictures();
  DiscoveredByTeam.clear();
}


//==========================================================================
//
//  itemtrapbase::FinalProcessForBone
//
//==========================================================================
void itemtrapbase::FinalProcessForBone () {
  if (Team == PLAYER_TEAM) Team = MONSTER_TEAM;
  std::unordered_set<int>::iterator i = DiscoveredByTeam.find(PLAYER_TEAM);
  if (i != DiscoveredByTeam.end()) DiscoveredByTeam.erase(i);
}


//==========================================================================
//
//  itemtrapbase::TeleportRandomly
//
//==========================================================================
void itemtrapbase::TeleportRandomly () {
  Team = NO_TEAM;
  DiscoveredByTeam.clear();
}
