class bitmap;


#ifdef HEADER_PHASE
TRAP(web, trap)
{
public:
  web ();
  virtual ~web ();

  virtual void AddDescription (festring &) const override;
  virtual truth TryToUnStick (character *, v2) override;
  virtual void Save (outputfile &) const override;
  virtual void Load (inputfile &) override;
  virtual void StepOnEffect (character *Stepper) override;
  virtual int GetTrapType () const override;
  virtual feuLong GetTrapID () const override;
  virtual feuLong GetVictimID () const override;
  virtual void AddTrapName (festring &, int) const override;
  virtual void UnStick () override;
  virtual void UnStick (int I) override;
  virtual void Draw (blitdata &BlitData) const override;
  virtual void ReceiveDamage (character *, int, int, int) override;
  virtual truth IsDangerous (ccharacter *Char) const override;
  virtual void PreProcessForBone () override;
  virtual void PostProcessForBone () override;
  virtual void Untrap () override;

  virtual int GetTrapBaseModifier () const;
  virtual void Destroy ();
  truth CanBeSeenBy (ccharacter *Who) const;

  truth IsStuckToBodyPart (int) const;
  void SetStrength (int What);

  item *TryAutoBurn (character *Stepper);

protected:
  trapdata TrapData;
  bitmap *Picture;
  int Strength; /* must be more than 0 */
};


#else


int web::GetTrapBaseModifier () const { return Strength; }
int web::GetTrapType () const { return GetType(); }
feuLong web::GetTrapID () const { return TrapData.TrapID; }
feuLong web::GetVictimID () const { return TrapData.VictimID; }
void web::UnStick () { TrapData.VictimID = 0; }
void web::UnStick (int I) { TrapData.BodyParts &= ~(1 << I); }
void web::SetStrength (int What) { Strength = What; }
truth web::IsDangerous (ccharacter *Char) const { return CanBeSeenBy(Char); }


web::web () : TrapData(), Picture(0), Strength(1) {
  if (!game::IsLoading()) {
    TrapData.TrapID = game::CreateNewTrapID(this);
    TrapData.VictimID = 0;
    Picture = new bitmap(TILE_V2, TRANSPARENT_COLOR);
    bitmap Temp(TILE_V2, TRANSPARENT_COLOR);
    Temp.ActivateFastFlag();
    packcol16 Color = MakeRGB16(250, 250, 250);
    const rawbitmap *Effect = igraph::GetRawGraphic(GR_EFFECT);
    Effect->MaskedBlit(&Temp, v2(RAND_2 ? 64 : 80, 32), ZERO_V2, TILE_V2, &Color);
    Temp.NormalBlit(Picture, Flags);
  }
}


web::~web () {
  game::RemoveTrapID(TrapData.TrapID);
  delete Picture;
}


void web::Save (outputfile &SaveFile) const {
  trap::Save(SaveFile);
  SaveFile << TrapData << Strength << Picture;
}


void web::Load (inputfile &SaveFile) {
  trap::Load(SaveFile);
  SaveFile >> TrapData >> Strength >> Picture;
  game::AddTrapID(this, TrapData.TrapID);
}


void web::PreProcessForBone () {
  trap::PreProcessForBone();
  game::RemoveTrapID(TrapData.TrapID);
  TrapData.TrapID = 0;
}


void web::PostProcessForBone () {
  trap::PostProcessForBone();
  TrapData.TrapID = game::CreateNewTrapID(this);
}


truth web::TryToUnStick (character *Victim, v2) {
  feuLong TrapID = GetTrapID();

  int Modifier = 7*GetTrapBaseModifier()/Max(Victim->GetAttribute(DEXTERITY)+Victim->GetAttribute(ARM_STRENGTH), 1);
  if (RAND_N(Max(Modifier, 2)) == 0) {
    Victim->RemoveTrap(TrapID);
    TrapData.VictimID = 0;
    if (Victim->IsPlayer()) ADD_MESSAGE("You manage to free yourself from the web.");
    else if (Victim->CanBeSeenByPlayer()) ADD_MESSAGE("%s manages to free %sself from the web.", Victim->CHAR_NAME(DEFINITE), Victim->CHAR_OBJECT_PRONOUN);
    Victim->EditAP(-500);
    return true;
  }

  if (RAND_N(Max(Modifier << 1, 2)) == 0) {
    Victim->RemoveTrap(TrapID);
    TrapData.VictimID = 0;
    GetLSquareUnder()->RemoveTrap(this);
    SendToHell();
    if (Victim->IsPlayer()) ADD_MESSAGE("You tear the web down.");
    else if (Victim->CanBeSeenByPlayer()) ADD_MESSAGE("%s tears the web down.", Victim->CHAR_NAME(DEFINITE));
    Victim->EditAP(-500);
    return true;
  }

  Modifier = GetTrapBaseModifier()*(Victim->GetAttribute(DEXTERITY)+Victim->GetAttribute(ARM_STRENGTH))/75;
  if (Victim->CanChokeOnWeb(this) && RAND_N(Max(Modifier << 3, 2)) == 0) {
    if (Victim->IsPlayer()) ADD_MESSAGE("You manage to choke yourself on the web.");
    else if (Victim->CanBeSeenByPlayer()) ADD_MESSAGE("%s chokes %sself on the web.", Victim->CHAR_NAME(DEFINITE), Victim->CHAR_OBJECT_PRONOUN);
    Victim->LoseConsciousness(250 + RAND_N(250));
    Victim->EditAP(-1000);
    return true;
  }

  if (RAND_N(Max(Modifier, 2)) == 0) {
    int VictimBodyPart = Victim->GetRandomBodyPart(ALL_BODYPART_FLAGS&~TrapData.BodyParts);
    if (VictimBodyPart != NONE_INDEX) {
      TrapData.BodyParts |= 1 << VictimBodyPart;
      Victim->AddTrap(GetTrapID(), 1 << VictimBodyPart);
      if (Victim->IsPlayer()) {
        ADD_MESSAGE("You fail to free yourself from the web and your %s is stuck in it in the attempt.", Victim->GetBodyPartName(VictimBodyPart).CStr());
      } else if (Victim->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s tries to free %sself from the web but is stuck more tightly in it in the attempt.", Victim->CHAR_NAME(DEFINITE), Victim->CHAR_OBJECT_PRONOUN);
      }
      Victim->EditAP(-1000);
      return true;
    }
  }

  if (Victim->IsPlayer()) ADD_MESSAGE("You are unable to escape from the web.");
  Victim->EditAP(-1000);
  return false;
}


//==========================================================================
//
//  web:TryAutoBurn
//
//  if the player is wielding a burning weapon, automatically
//  burn the web (for some AP penalty)
//
//==========================================================================
item *web::TryAutoBurn (character *Stepper) {
  item *Item = 0;
  IvanAssert(Stepper);

  int eqcount = Stepper->GetEquipments();
  if (eqcount < Max(RIGHT_WIELDED_INDEX, LEFT_WIELDED_INDEX)) return 0; // no equipment in hands

  if (eqcount >= RIGHT_WIELDED_INDEX) {
    Item = Stepper->GetEquipment(RIGHT_WIELDED_INDEX);
    if (!Item || !Item->IsFlaming(Stepper)) Item = 0;
  }

  if (!Item && eqcount >= LEFT_WIELDED_INDEX) {
    Item = Stepper->GetEquipment(LEFT_WIELDED_INDEX);
    if (!Item || !Item->IsFlaming(Stepper)) Item = 0;
  }

  if (Item) {
    IvanAssert(Item);

    festring itname;
    if (Stepper->IsPlayer()) {
      Item->AddName(itname, UNARTICLED);
    } else {
      Item->AddName(itname, INDEFINITE);
    }

    // sometimes burning the web should fail
    //FIXME: i absolutely don't know what i am doing here!
    int Modifier = GetTrapBaseModifier() * Stepper->GetAttribute(AGILITY) / 7;
    if (RAND_N(Max(Modifier, 2)) == 0) {
      if (Stepper->IsPlayer()) {
        ADD_MESSAGE("You failed to burn the the web with your %s.", itname.CStr());
      } else if (Stepper->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s faield to burn the web with %s.", Stepper->CHAR_NAME(DEFINITE),
                    itname.CStr());
      }
      Stepper->EditAP(-1000); // failure penalty
      Item = 0;
    } else if (Item->Burn(Stepper, Stepper->GetPos(), -666)) {
      //FIXME: AP
      Stepper->EditExperience(AGILITY, 150, 1<<6);
      Stepper->EditNP(-10);
      Stepper->EditAP(-100000 / APBonus(Stepper->GetAttribute(AGILITY)));
      if (Stepper->IsPlayer()) {
        ADD_MESSAGE("You managed to burn the web with your %s.", itname.CStr());
      } else if (Stepper->CanBeSeenByPlayer()) {
        ADD_MESSAGE("%s burned the web with %s.", Stepper->CHAR_NAME(DEFINITE), itname.CStr());
      }
    } else {
      Item = 0; // oops
    }
  }

  return Item;
}


void web::StepOnEffect (character *Stepper) {
  if (Stepper->IsImmuneToStickiness()) return;
  int StepperBodyPart = Stepper->GetRandomBodyPart();
  if (StepperBodyPart == NONE_INDEX) return;
  if (!TryAutoBurn(Stepper)) {
    TrapData.VictimID = Stepper->GetID();
    TrapData.BodyParts = 1 << StepperBodyPart;
    Stepper->AddTrap(GetTrapID(), 1 << StepperBodyPart);
    if (Stepper->IsPlayer()) {
      ADD_MESSAGE("You try to step through the web but your %s sticks in it.",
                  Stepper->GetBodyPartName(StepperBodyPart).CStr());
    } else if (Stepper->CanBeSeenByPlayer()) {
      ADD_MESSAGE("%s gets stuck in the web.", Stepper->CHAR_NAME(DEFINITE));
    }
  }
}


void web::AddDescription (festring &Msg) const {
  Msg << ". A web envelops the square";
}


void web::AddTrapName (festring &String, int) const {
  String << "a spider web";
}


void web::Draw (blitdata &BlitData) const {
  Picture->LuminanceMaskedBlit(BlitData);
}


truth web::IsStuckToBodyPart (int I) const {
  return 1 << I & TrapData.BodyParts;
}


void web::ReceiveDamage (character *, int, int Type, int) {
  if (Type & (ACID|FIRE|ELECTRICITY|ENERGY)) Destroy();
}


void web::Destroy () {
  Untrap();
  GetLSquareUnder()->RemoveTrap(this);
  SendToHell();
}


truth web::CanBeSeenBy (ccharacter *Who) const {
  return
    Who->GetAttribute(WISDOM) > 4 &&
    GetLSquareUnder()->CanBeSeenBy(Who);
}


void web::Untrap () {
  character *Char = game::SearchCharacter(GetVictimID());
  if (Char) Char->RemoveTrap(GetTrapID());
  TrapData.VictimID = 0;
}


#endif
