/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */

/* Compiled through wmapset.cpp */

// ////////////////////////////////////////////////////////////////////////// //
/*
#define MAX_TEMPERATURE   27    //increase for a warmer world
#define LATITUDE_EFFECT   40    //increase for more effect
#define ALTITUDE_EFFECT   0.02

#define COLD    10
#define MEDIUM  12
#define WARM    17
#define HOT     19
*/

static int MAX_TEMPERATURE = 27;
static int LATITUDE_EFFECT = 40;
static float ALTITUDE_EFFECT = 0.02;
static int COLD = 10;
static int MEDIUM = 12;
static int WARM = 17;
static int HOT = 19;

static bool debugMessages = false;


// ////////////////////////////////////////////////////////////////////////// //
static const int DirX[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
static const int DirY[8] = { -1, 0, 1, -1, 1, -1, 0, 1 };


// ////////////////////////////////////////////////////////////////////////// //
// worldmap
// ////////////////////////////////////////////////////////////////////////// //

//==========================================================================
//
//  worldmap::worldmap
//
//==========================================================================
worldmap::worldmap () {
}


//==========================================================================
//
//  worldmap::worldmap
//
//==========================================================================
worldmap::worldmap (int XSize, int YSize) : area(XSize, YSize) {
  Map = reinterpret_cast<wsquare ***>(area::Map);
  for (int x = 0; x < XSize; ++x) {
    for (int y = 0; y < YSize; ++y) {
      //Map[x][y] = new wsquare(this, v2(x, y));
      //Map[x][y]->SetGWTerrain(ocean::Spawn());
      Map[x][y] = nullptr;
    }
  }
  TypeBuffer = nullptr;
  AltitudeBuffer = nullptr;
  ContinentBuffer = nullptr;
  continent::TypeBuffer = nullptr;
  continent::AltitudeBuffer = nullptr;
  continent::ContinentBuffer = nullptr;
}


//==========================================================================
//
//  worldmap::~worldmap
//
//==========================================================================
worldmap::~worldmap () {
  ResetItAll(false);
  delete [] TypeBuffer;
  delete [] AltitudeBuffer;
  delete [] ContinentBuffer;
  for (uInt c = 1; c < Continent.size(); ++c) delete Continent[c];
  for (uInt c = 0; c < PlayerGroup.size(); ++c) delete PlayerGroup[c];
  Continent.resize(1, 0);
  PlayerGroup.clear();
  continent::TypeBuffer = nullptr;
  continent::AltitudeBuffer = nullptr;
  continent::ContinentBuffer = nullptr;
}


//==========================================================================
//
//  worldmap::GetContinentUnder
//
//==========================================================================
continent *worldmap::GetContinentUnder (v2 Pos) const {
  return Continent[ContinentBuffer[Pos.X][Pos.Y]];
}


//==========================================================================
//
//  worldmap::GetEntryPos
//
//==========================================================================
v2 worldmap::GetEntryPos (ccharacter *, int I) const {
  //return EntryMap.find(I)->second;
  EntryInfo bestEntry;
  bestEntry.UseTick = -1; // impossible
  for (auto &&e : EntryMap) {
    if (e.Level == I && e.UseTick > bestEntry.UseTick) {
      bestEntry = e;
    }
  }
  if (bestEntry.UseTick >= 0) return bestEntry.Pos;
  ABORT("cannot find worldmap entry #%d!", I);
}


//==========================================================================
//
//  worldmap::GetContinent
//
//==========================================================================
continent *worldmap::GetContinent (int I) const {
  return Continent[I];
}


//==========================================================================
//
//  worldmap::GetAltitude
//
//==========================================================================
int worldmap::GetAltitude (v2 Pos) {
  return AltitudeBuffer[Pos.X][Pos.Y];
}


//==========================================================================
//
//  worldmap::GetPlayerGroup
//
//==========================================================================
charactervector &worldmap::GetPlayerGroup () {
  return PlayerGroup;
}


//==========================================================================
//
//  worldmap::GetPlayerGroupMember
//
//==========================================================================
character *worldmap::GetPlayerGroupMember (int c) {
  return PlayerGroup[c];
}


//==========================================================================
//
//  worldmap::ResetItAll
//
//==========================================================================
void worldmap::ResetItAll (truth recreate) {
  Map = reinterpret_cast<wsquare ***>(area::Map);
  for (int x = 0; x < XSize; ++x) {
    for (int y = 0; y < YSize; ++y) {
      if (Map[x][y]) delete Map[x][y];
      if (recreate) {
        Map[x][y] = new wsquare(this, v2(x, y));
        Map[x][y]->SetGWTerrain(GWSpawn(OCEAN));
      } else {
        Map[x][y] = nullptr;
      }
    }
  }
  ClearEntryPoints();
  if (recreate) {
    if (TypeBuffer) delete [] TypeBuffer;
    if (AltitudeBuffer) delete [] AltitudeBuffer;
    if (ContinentBuffer) delete [] ContinentBuffer;
    Alloc2D(TypeBuffer, XSize, YSize);
    Alloc2D(AltitudeBuffer, XSize, YSize);
    Alloc2D(ContinentBuffer, XSize, YSize);
    continent::TypeBuffer = TypeBuffer;
    continent::AltitudeBuffer = AltitudeBuffer;
    continent::ContinentBuffer = ContinentBuffer;
  }
}


//==========================================================================
//
//  worldmap::Save
//
//==========================================================================
void worldmap::Save (outputfile &SaveFile) const {
  area::Save(SaveFile);
  SaveFile.WriteBytes(reinterpret_cast<char*>(TypeBuffer[0]), XSizeTimesYSize*sizeof(uChar));
  SaveFile.WriteBytes(reinterpret_cast<char*>(AltitudeBuffer[0]), XSizeTimesYSize*sizeof(short));
  SaveFile.WriteBytes(reinterpret_cast<char*>(ContinentBuffer[0]), XSizeTimesYSize*sizeof(uChar));
  for (feuLong c = 0; c < XSizeTimesYSize; ++c) {
    Map[0][c]->Save(SaveFile);
  }
  SaveFile << Continent << PlayerGroup;
}


//==========================================================================
//
//  worldmap::Load
//
//==========================================================================
void worldmap::Load (inputfile &SaveFile) {
  area::Load(SaveFile);
  Map = reinterpret_cast<wsquare ***>(area::Map);
  Alloc2D(TypeBuffer, XSize, YSize);
  Alloc2D(AltitudeBuffer, XSize, YSize);
  Alloc2D(ContinentBuffer, XSize, YSize);
  SaveFile.ReadBytes(reinterpret_cast<char*>(TypeBuffer[0]), XSizeTimesYSize*sizeof(uChar));
  SaveFile.ReadBytes(reinterpret_cast<char*>(AltitudeBuffer[0]), XSizeTimesYSize*sizeof(short));
  SaveFile.ReadBytes(reinterpret_cast<char*>(ContinentBuffer[0]), XSizeTimesYSize*sizeof(uChar));
  continent::TypeBuffer = TypeBuffer;
  continent::AltitudeBuffer = AltitudeBuffer;
  continent::ContinentBuffer = ContinentBuffer;
  for (int x = 0; x < XSize; ++x) {
    for (int y = 0; y < YSize; ++y) {
      delete Map[x][y];
      Map[x][y] = new wsquare(this, v2(x, y));
    }
  }
  for (int x = 0; x < XSize; ++x) {
    for (int y = 0; y < YSize; ++y) {
      game::SetSquareInLoad(Map[x][y]);
      Map[x][y]->Load(SaveFile);
    }
  }
  CalculateNeighbourBitmapPoses();
  SaveFile >> Continent >> PlayerGroup;
}


//==========================================================================
//
//  FindWorldMapOptionsConfig
//
//==========================================================================
static const gwterraindatabase *FindWorldMapOptionsConfig () {
  auto xtype = protocontainer<gwterrain>::SearchCodeName(CONST_S("gwterrain"));
  if (!xtype) ABORT("Your worldmap is dull and empty.");
  auto proto = protocontainer<gwterrain>::GetProto(xtype);
  if (!proto) ABORT("wtf?!");
  auto configs = proto->GetConfigData();
  if (!configs) ABORT("wtf?!");
  int cfgcount = proto->GetConfigSize();
  // search for overriden config
  for (int f = 0; f < cfgcount; ++f) {
    auto cfg = configs[f];
    //if (cfg->Config == 0) continue;
    //ConLogf("<%s>", getCPPTypeName<decltype(cfg->Config)>().c_str());
    if (cfg->Config == WorldMapOptionsCfg()) return cfg;
  }
  // search for base config
  for (int f = 0; f < cfgcount; ++f) {
    auto cfg = configs[f];
    if (cfg->Config == 0) return cfg;
  }
  ABORT("Worldmap is without options!");
  return nullptr;
}


//==========================================================================
//
//  worldmap::ResetPOIs
//
//==========================================================================
void worldmap::ResetPOIs () {
  int maxConfig = 0;
  for (int f = 0; f < game::poiCount(); ++f) {
    owterrain *terra = game::poiByIndex(f);
    terra->SetRevealed(false);
    terra->SetPlaced(false);
    terra->SetGenerated(false);
    int cfg = terra->GetConfig();
    // fix obvious scripting bugs
    if (cfg == NEW_ATTNAM || cfg == UNDER_WATER_TUNNEL || cfg == UNDER_WATER_TUNNEL_EXIT || cfg == ELPURI_CAVE) {
      terra->MustBeSkipped = false;
    } else {
      //ConLogf("terra #%d prob is %d", terra->GetConfig(), terra->GetProbability());
      terra->MustBeSkipped = (terra->GetProbability() < RAND_N(100)+1);
      //if (terra->MustBeSkipped) ConLogf("worldmap::ResetPOIs: skipped POI with config #%d", terra->GetConfig());
    }
    maxConfig = Max(maxConfig, cfg);
  }
  if (maxConfig == 0) {
    ABORT("no POIs defined, are you nuts (not Petrus')?!");
  }

  poiContinents.clear();
  for (int f = 0; f <= maxConfig; f += 1) {
    poiContinents.push_back(0);
  }
}


//==========================================================================
//
//  worldmap::PlacePOIAtMap
//
//==========================================================================
void worldmap::PlacePOIAtMap (owterrain *terra, truth forceReveal) {
  if (!terra) ABORT("cannot place nothing on worldmap!");
  if (!terra->IsGenerated()) ABORT("cannot place ungenerated something on worldmap!");
  if (terra->MustBeSkipped) ABORT("cannot place skipped something on worldmap!");
  // place it, if it is not placed yet
  if (!terra->IsPlaced()) {
    terra->SetPlaced(true);
    GetWSquare(terra->GetPosition())->ChangeOWTerrain(terra->Clone());
    SetEntryPos(terra->GetConfig(), terra->GetPosition());
    //ConLogf("POI #%d placed at (%d,%d); attached dungeon is %d", terra->GetConfig(), terra->GetPosition().X, terra->GetPosition().Y, terra->GetAttachedDungeon());
  } else {
    //ConLogf("already placed...");
  }
  if (!IsValidPos(terra->GetPosition())) ABORT("cannot place something on invalid worldmap position!");
  if (forceReveal || (terra->RevealEnvironmentInitially() && !terra->IsRevealed())) {
    //if (terra->IsRevealed()) ConLogf("re-revealing...");
    terra->SetRevealed(true);
    RevealEnvironment(terra->GetPosition(), 1);
  }
}


//==========================================================================
//
//  CreateShuffledContinentList
//
//==========================================================================
static ContinentVector CreateShuffledContinentList (const ContinentVector &list) {
  ContinentVector res;
  for (auto &cont : list) res.push_back(cont);
  if (res.size() > 1) {
    for (uInt f = 0; f < res.size(); ++f) {
      uInt swp = (uInt)RAND_GOOD((int)res.size());
      if (swp != f) {
        continent *tmp = res[f];
        res[f] = res[swp];
        res[swp] = tmp;
      }
    }
  }
  return res;
}


/*
the algo is brute force. we will collect possible coords for all POIs, then
try to place each POI at each coord (choosen by random), then check if
constraints are ok. if not, try another coords. continue until all coords
are tried, or a solution found.

we definitely can do better, but meh...
*/


//==========================================================================
//
//  worldmap::CreateInfoList
//
//==========================================================================
void worldmap::CreateInfoList (std::vector<TerraInfo> &list, owterrain *contMain) {
  IvanAssert(contMain);
  IvanAssert(contMain->SeparateContinent() || contMain == attnam);
  IvanAssert(contMain != newattnam);
  IvanAssert(contMain != underwatertunnel);
  IvanAssert(contMain != underwatertunnelexit);
  list.clear();

  TerraInfo ti;
  ti.terra = contMain;
  if (ti.terra->IsGenerated()) {
    ti.pos = ti.terra->GetPosition();
  } else {
    ti.pos = ERROR_V2;
  }
  list.push_back(ti);

  for (int f = 0; f < game::poiCount(); ++f) {
    owterrain *terra = game::poiByIndex(f);
    if (!terra) ABORT("Somone stole our terrain!");
    if (terra == newattnam) continue;
    if (terra == underwatertunnel) continue;
    if (terra == underwatertunnelexit) continue;
    if (terra->MustBeSkipped) continue;
    if (terra != contMain && terra->GetWantContinentWith() == contMain->GetConfig()) {
      TerraInfo ti;
      ti.terra = terra;
      if (ti.terra->IsGenerated()) {
        ti.pos = ti.terra->GetPosition();
      } else {
        ti.pos = ERROR_V2;
      }
      list.push_back(ti);
    }
  }

  std::sort(list.begin(), list.end(),
    // `<`
    [&] (const TerraInfo &a, const TerraInfo &b) {
      if (a.terra == contMain) return (b.terra != contMain);
      if (b.terra == contMain) return false;

      int adpoi = a.terra->GetDistancePOI();
      if (adpoi == 0) adpoi = 0x7fffffff;

      int bdpoi = b.terra->GetDistancePOI();
      if (bdpoi == 0) bdpoi = 0x7fffffff;

      if (adpoi != bdpoi) return (adpoi < bdpoi);

      int aid = a.terra->GetMinimumDistanceTo();
      int bid = b.terra->GetMinimumDistanceTo();
      if (aid != bid) return (aid < bid);

      int axd = a.terra->GetMaximumDistanceTo();
      int bxd = b.terra->GetMaximumDistanceTo();
      if (axd != bxd) return (axd < bxd);

      return (a.terra->GetConfig() < b.terra->GetConfig());
    });

  if (debugMessages) {
    ConLogf("--------");
    for (size_t f = 0; f != list.size(); f += 1) {
      ConLogf("  %u: %s", (unsigned)f, list[f].terra->GetNameSingular().CStr());
    }
  }
}


//==========================================================================
//
//  IsGoodMinDistance
//
//  continent check should be done by caller
//
//==========================================================================
static bool IsGoodMinDistance (v2 mypos, v2 checkpos, owterrain *forTerra) {
  IvanAssert(forTerra);
  if (mypos == checkpos) return false; // just in case
  const int minDist = forTerra->GetMinimumDistanceTo();
  return (mypos.GetManhattanDistance(checkpos) > minDist);
}


//==========================================================================
//
//  IsGoodMaxDistance
//
//==========================================================================
static bool IsGoodMaxDistance (v2 mypos, v2 checkpos, owterrain *forTerra) {
  IvanAssert(forTerra);
  if (mypos == checkpos) return false; // just in case
  const int minDist = forTerra->GetMinimumDistanceTo();
  const int maxDist = forTerra->GetMaximumDistanceTo();
  if (maxDist < 1 || maxDist < minDist) return true;
  return (mypos.GetManhattanDistance(checkpos) <= maxDist);
}


//==========================================================================
//
//  NeedDistanceChecks
//
//==========================================================================
static FORCE_INLINE bool NeedDistanceChecks (owterrain *terra, owterrain *forTerra) {
  return (forTerra->GetDistancePOI() == 0 || forTerra->GetDistancePOI() == terra->GetConfig());
}


//==========================================================================
//
//  NeedDistanceChecksNew
//
//  is `terra` should be checked against `forTerra`?
//
//==========================================================================
static FORCE_INLINE bool NeedDistanceChecksNew (owterrain *terra, owterrain *forTerra) {
  return (terra->GetDistancePOI() == 0 || terra->GetDistancePOI() == forTerra->GetConfig());
}


//==========================================================================
//
//  worldmap::CheckListConstraints
//
//==========================================================================
bool worldmap::CheckListConstraints (std::vector<TerraInfo> &list) {
  //game::BusyAnimation();
  /*
  for (auto &ti : list) {
    for (auto &oti : list) {
      if (oti.terra == ti.terra) continue;
      // check `ti`
      if (NeedDistanceChecksNew(ti.terra, oti.terra)) {
        if (!IsGoodMinDistance(ti.pos, oti.pos, oti.terra)) return false;
        if (!IsGoodMaxDistance(ti.pos, oti.pos, oti.terra)) return false;
      }
    }
  }
  */
  return true;
}


static bool giveUp = false;
static time_t startTime = 0;
static int checkCounter = 0;


//==========================================================================
//
//  ShowGraphStats
//
//==========================================================================
static void ShowGraphStats (std::vector<worldmap::TerraInfo> &list, size_t listidx) {
  static int steps = 0;
  /*
  if (debugMessages) {
    ConPrintf("\r%2u:", (unsigned)listidx);
    for (size_t f = 0; f != list.size(); f += 1) {
      if (f < listidx) {
        ConPrintf(" [%4u/%4u] ", (unsigned)list[f].cpidx,
                (unsigned)list[f].cptotal);
      } else {
        ConPrintf(" [%4u/%4u] ", 0, 0);
      }
    }
    ConPrintf("\x1b[K");
    fflush(stderr);
  }
  */
  steps += 1;
  if (steps >= 1000) {
    steps = 0;
    checkCounter += 1;
    //ConLogf("cc=%d", checkCounter);
    if (checkCounter > 10) {
      checkCounter = 0;
      const time_t ett = time(NULL);
      if (ett - startTime > 10) {
        ConLogf("ABORT!");
        giveUp = true;
      } else {
        #if 0
        ConLogf("TIME: %u", (unsigned)(ett - startTime));
        #endif
        festring msg = CONST_S("still trying: [");
        msg << (int)list[0].cpidx + 1 << "/" << (int)list[0].cptotal << "]";
        game::SetBusyAnimationMessage(msg);
      }
    }
    game::BusyAnimation();
  }
}


//==========================================================================
//
//  worldmap::TryOnePoisition
//
//==========================================================================
bool worldmap::TryOnePoisition (std::vector<TerraInfo> &list, size_t listidx, continent *cont) {
  IvanAssert(cont);
  if (listidx == list.size()) {
    return CheckListConstraints(list);
  }
  if (giveUp) return false;
  TerraInfo &ti = list[listidx];
  if (ti.terra->IsGenerated()) {
    ti.cpidx = 0; ti.cptotal = 1;
    ti.pos = ti.terra->GetPosition();
    return TryOnePoisition(list, listidx + 1, cont);
  } else {
    std::vector<v2> poslist; // possible positions; this should be a list, but...
    cont->GetShuffledMembers(ti.terra->GetNativeGTerrainType(), poslist,
      [&] (const v2 pos) {
        bool seenTerra = false;
        bool seenGoodFar = false;
        for (size_t f = 0; f != listidx; f += 1) {
          const TerraInfo &xti = list[f];
          IvanAssert(xti.terra != ti.terra);
          IvanAssert(xti.pos != ERROR_V2);
          if (NeedDistanceChecksNew(ti.terra, xti.terra)) {
            seenTerra = true;
            if (!IsGoodMinDistance(pos, xti.pos, ti.terra)) return false;
            seenGoodFar = (seenGoodFar || IsGoodMaxDistance(pos, xti.pos, ti.terra));
          }
          /*
          if (NeedDistanceChecksNew(xti.terra, ti.terra)) {
            if (!IsGoodMinDistance(xti.pos, pos, ti.terra)) return false;
            if (!IsGoodMaxDistance(xti.pos, pos, ti.terra)) return false;
          }
          */
        }
        return (!seenTerra || seenGoodFar);
      });

    // if we want to be close to something, sort by distance
    if (ti.terra->GetCloseTo() != 0) {
      owterrain *closeTo = FindPOIByConfig(ti.terra->GetCloseTo());
      if (closeTo == ti.terra) {
        ABORT("Such intimacy is forbidden!");
      }
      if (!closeTo->IsGenerated()) {
        ABORT("Desperately want to be close to someone who's missing...\n"
              "POI (%s) wants to be close to POI (%s)\n",
              ti.terra->GetNameSingular().CStr(),
              closeTo->GetNameSingular().CStr());
      }

      std::sort(poslist.begin(), poslist.end(),
        [&] (const v2 &a, const v2 &b) {
          const int md1 = a.GetManhattanDistance(closeTo->GetPosition());
          const int md2 = b.GetManhattanDistance(closeTo->GetPosition());
          return (md1 < md2);
        });
    }

    if (poslist.empty()) {
      #if 0
      ConLogf("NO POSITIONS for POI %d '%s'...",
              (int)listidx, ti.terra->GetNameSingular().CStr());
      #endif
      return false;
    }

    #if 0
    ConLogf("GENERATED %u positions for POI %d '%s'...",
            (unsigned)poslist.size(), (int)listidx, ti.terra->GetNameSingular().CStr());
    #endif
    ti.cptotal = poslist.size();
    for (size_t pidx = 0; pidx != poslist.size(); pidx += 1) {
      ti.cpidx = pidx;
      ShowGraphStats(list, listidx + 1);
      ti.pos = poslist[pidx];
      if (TryOnePoisition(list, listidx + 1, cont)) return true;
    }
    #if 0
    ConLogf("FAILED ALL %u positions for POI %d '%s'...",
            (unsigned)poslist.size(), (int)listidx, ti.terra->GetNameSingular().CStr());
    #endif
  }
  return false;
}


//==========================================================================
//
//  worldmap::PlaceTheList
//
//==========================================================================
void worldmap::PlaceTheList (std::vector<TerraInfo> &list, continent *cont) {
  for (auto &ti : list) {
    if (!ti.terra->IsGenerated()) {
      IvanAssert(ti.pos != ERROR_V2);
      ti.terra->SetGenerated(true);
      ti.terra->SetPosition(ti.pos);
      if (ti.terra->PlaceInitially()) PlacePOIAtMap(ti.terra);
      poiContinents[ti.terra->GetConfig()] = cont;
    }
  }
}


//==========================================================================
//
//  worldmap::BruteForceSearch
//
//==========================================================================
bool worldmap::BruteForceSearch (owterrain *contMain) {
  IvanAssert(contMain);
  IvanAssert(contMain->SeparateContinent() || contMain == attnam);
  std::vector<TerraInfo> tlist;
  CreateInfoList(tlist, contMain);
  if (contMain->IsGenerated()) {
    continent *cont = FindPOIContinent(contMain);
    IvanAssert(cont);
    if (!TryOnePoisition(tlist, 0, cont)) return false;
    PlaceTheList(tlist, cont);
    return true;
  } else {
    // try all continents
    auto clist = CreateShuffledContinentList(Continent);
    for (auto &cc : clist) {
      if (IsFreeContinent(cc) && contMain->IsSuitableContinent(cc)) {
        if (TryOnePoisition(tlist, 0, cc)) {
          PlaceTheList(tlist, cc);
          return true;
        }
      }
    }
    return false;
  }
}


//==========================================================================
//
//  worldmap::FindPlacesForPOI
//
//==========================================================================
ContinentVector worldmap::FindPlacesForPOI (owterrain *terra, truth shuffle) {
  ContinentVector list;
  if (terra) {
    for (uInt c = 1; c < Continent.size(); ++c) {
      if (terra->MustBeSkipped) continue;
      //ConLogf(" trying continent %u for %d...", c, terra->GetConfig());
      if (terra->IsSuitableContinent(Continent[c])) {
        //ConLogf("  FOUND!");
        list.push_back(Continent[c]);
      }
    }
    // shuffle: https://en.wikipedia.org/wiki/Fisher-Yates_shuffle
    if (shuffle && list.size() > 1) {
      for (size_t f = (size_t)list.size() - 1; f != 0; f -= 1) {
        const size_t swp = (size_t)RAND_GOOD((int)f + 1);
        if (swp != f) {
          continent *tmp = list[f];
          list[f] = list[swp];
          list[swp] = tmp;
        }
      }
    }
  }
  //ConLogf(">>>%u continent(s) for %d...", list.size(), terra->GetConfig());
  return list;
}


//==========================================================================
//
//  worldmap::IsFreePlaceForPOI
//
//==========================================================================
truth worldmap::IsFreePlaceForPOI (continent *cont, owterrain *forTerra, v2 pos) {
  IvanAssert(cont);
  IvanAssert(forTerra);
  IvanAssert(!forTerra->IsGenerated());

  bool seenTerras = false;
  bool seenGoodMaxDist = false;

  for (int f = 0; f < game::poiCount(); ++f) {
    owterrain *terra = game::poiByIndex(f);
    if (!terra) ABORT("Somone stole our terrain!");
    if (terra == forTerra) continue;
    if (!terra->IsGenerated()) {
      if (forTerra->GetDistancePOI() == terra->GetConfig()) {
        ABORT("POI #%d (%s) wants to check the distance to ungenerated POI #%d (%s)!\n",
              forTerra->GetConfig(), forTerra->GetNameSingular().CStr(),
              terra->GetConfig(), terra->GetNameSingular().CStr());
      }
    }
    const v2 tpos = terra->GetPosition();
    if (tpos == pos) return false;

    if (forTerra && FindPOIContinent(terra) == cont) {
      // check distance to the current POI
      if (NeedDistanceChecks(terra, forTerra)) {
        seenTerras = true; // note that we've seen something interesting
        if (!IsGoodMinDistance(pos, tpos, forTerra)) {
          #if 0
          ConLogf("POI #%d (%s) failed min dist check to POI #%d (%s)!",
                 forTerra->GetConfig(), forTerra->GetNameSingular().CStr(),
                 terra->GetConfig(), terra->GetNameSingular().CStr());
          #endif
          return false; // alas
        }
        #if 0
        if (!IsGoodMaxDistance(pos, tpos, forTerra)) {
          ConLogf("POI #%d (%s) failed max dist check to POI #%d (%s)!",
                 forTerra->GetConfig(), forTerra->GetNameSingular().CStr(),
                 terra->GetConfig(), terra->GetNameSingular().CStr());
        }
        #endif
        seenGoodMaxDist = (seenGoodMaxDist || IsGoodMaxDistance(pos, tpos, forTerra));
      }

      // also, if the current POI has any requirements, they should be respected too
      if (NeedDistanceChecks(forTerra, terra)) {
        if (!IsGoodMinDistance(pos, tpos, terra)) return false; // alas
        // no need to check if too far: this is done when placing `terra` before
      }
    }
  }

  if (seenTerras && !seenGoodMaxDist) return false;
  return true;
}


//==========================================================================
//
//  worldmap::FindPOIByConfig
//
//==========================================================================
owterrain *worldmap::FindPOIByConfig (int config) {
  if (config < 1) return 0; // oops
  for (int f = 0; f < game::poiCount(); f += 1) {
    owterrain *terra = game::poiByIndex(f);
    if (terra->GetConfig() == config) {
      return terra;
    }
  }
  return 0;
}


//==========================================================================
//
//  worldmap::FindPlaceForAttnam
//
//  try to place all initial places, and check
//  if other places are (roughly) ok
//
//==========================================================================
continent *worldmap::FindPlaceForAttnam () {
  ContinentVector PerfectForAttnam = FindPlacesForPOI(attnam, true); // shuffle results
  if (PerfectForAttnam.size() == 0) {
    //ConLogf("no country for the old man...");
    return 0;
  }
  truth success = false;
  for (auto &cont : PerfectForAttnam) {
    //ConLogf("trying to place special pois...");
    if (!PlaceAttnamsAndUT(cont)) continue; // alas
    // WARNING! from here, we cannot continue checking continents on failure!
    //ConLogf("checking other pois...");
    success = true;
    for (int f = 0; success && f < game::poiCount(); ++f) {
      auto terra = game::poiByIndex(f);
      if (!terra->MustBeSkipped && !terra->IsGenerated() &&
          terra->GetWantContinentWith() == attnam->GetConfig())
      {
        //ConLogf(" f=%d; cidx=%d (%d)", f, terra->GetWantContinentWith(), attnam->GetConfig());
        if (!terra->IsSuitableContinent(cont)) {
          //ConLogf("  OOPS!(0)");
          if (!terra->CanBeSkipped()) {
            //ConLogf("   OOPS!(1)");
            success = false;
            //break;
          } else {
            terra->MustBeSkipped = true;
          }
        } else {
          //poiContinents[f] = cont;
        }
      }
    }
    // we can't continue looping here
    if (success) {
      return cont; // ok, this continent can be used for Petrus' needs
    }
  }
  return 0;
}


//==========================================================================
//
//  worldmap::PlaceAttnamsAndUT
//
//==========================================================================
truth worldmap::PlaceAttnamsAndUT (continent *PetrusLikes) {
  if (!PetrusLikes) return false; // oops
  if (PetrusLikes->GetSize() < 8) return false; // oops

  if (!attnam) ABORT("Who stole my Attnam?!");
  if (!newattnam) ABORT("Who stole my New Attnam?!");
  if (!underwatertunnel) ABORT("Who stole my UC Entry?!");
  if (!underwatertunnelexit) ABORT("Who stole my UC Exit?!");

  v2 newattnamPos = ERROR_V2, tunnelEntryPos = ERROR_V2, tunnelExitPos = ERROR_V2;

  // place tunnel exit
  truth Correct = false;
  for (int c1 = 0; c1 < 25; ++c1) {
    game::BusyAnimation();
    for (int c2 = 1; c2 < 50; ++c2) {
      tunnelExitPos = PetrusLikes->GetRandomMember(underwatertunnelexit->GetNativeGTerrainType(), &Correct);
      if (!Correct) return false; // no room, oops
      Correct = false;
      for (int d1 = 0; d1 < 8; ++d1) {
        v2 Pos = tunnelExitPos + game::GetMoveVector(d1);
        if (IsValidPos(Pos) && AltitudeBuffer[Pos.X][Pos.Y] <= 0) {
          int Distance = 3 + RAND_4;
          truth Error = false;
          int x, y;
          int Counter = 0;

          tunnelEntryPos = Pos;
          for (int c2 = 0; c2 < Distance; ++c2) {
            tunnelEntryPos += game::GetMoveVector(d1);
            if (!IsValidPos(tunnelEntryPos) || AltitudeBuffer[tunnelEntryPos.X][tunnelEntryPos.Y] > 0) { Error = true; break; }
          }
          if (Error) continue;

          for (x = tunnelEntryPos.X-3; x <= tunnelEntryPos.X+3; ++x) {
            for (y = tunnelEntryPos.Y-3; y <= tunnelEntryPos.Y+3; ++y, ++Counter) {
              if (Counter != 0 && Counter != 6 && Counter != 42 && Counter != 48 &&
                  (!IsValidPos(x, y) || AltitudeBuffer[x][y] > 0 || AltitudeBuffer[x][y] < -350))
              {
                Error = true;
                break;
              }
            }
            if (Error) break;
          }
          if (Error) continue;

          Error = true;
          for (x = 0; x < XSize; ++x) {
            if (TypeBuffer[x][tunnelEntryPos.Y] == JungleType()) {
              Error = false;
              break;
            }
          }
          if (Error) continue;

          Counter = 0;
          for (x = tunnelEntryPos.X - 2; x <= tunnelEntryPos.X+2; ++x) {
            for (y = tunnelEntryPos.Y - 2; y <= tunnelEntryPos.Y+2; ++y, ++Counter) {
              if (Counter != 0 && Counter != 4 && Counter != 20 && Counter != 24) {
                AltitudeBuffer[x][y] /= 2;
              }
            }
          }

          AltitudeBuffer[tunnelEntryPos.X][tunnelEntryPos.Y] = 1+RAND_N(50);
          TypeBuffer[tunnelEntryPos.X][tunnelEntryPos.Y] = JungleType();
          GetWSquare(tunnelEntryPos)->ChangeGWTerrain(GWSpawn(JUNGLE));

          int NewAttnamIndex;
          for (NewAttnamIndex = RAND_8; NewAttnamIndex == 7-d1; NewAttnamIndex = RAND_8) {}
          newattnamPos = tunnelEntryPos + game::GetMoveVector(NewAttnamIndex);

          static const int DiagonalDir[4] = { 0, 2, 5, 7 };
          static const int NotDiagonalDir[4] = { 1, 3, 4, 6 };
          static const int AdjacentDir[4][2] = { { 0, 1 }, { 0, 2 }, { 1, 3 }, { 2, 3 } };
          truth Raised[] = { false, false, false, false };

          for (int d2 = 0; d2 < 4; ++d2) {
            if (NotDiagonalDir[d2] != 7-d1 && (NotDiagonalDir[d2] == NewAttnamIndex || !RAND_2)) {
              v2 Pos = tunnelEntryPos+game::GetMoveVector(NotDiagonalDir[d2]);
              AltitudeBuffer[Pos.X][Pos.Y] = 1+RAND_N(50);
              TypeBuffer[Pos.X][Pos.Y] = JungleType();
              GetWSquare(Pos)->ChangeGWTerrain(GWSpawn(JUNGLE));
              Raised[d2] = true;
            }
          }

          for (int d2 = 0; d2 < 4; ++d2) {
            if (DiagonalDir[d2] != 7-d1 &&
                (DiagonalDir[d2] == NewAttnamIndex ||
                 (Raised[AdjacentDir[d2][0]] && Raised[AdjacentDir[d2][1]] && !RAND_2/*(RAND()&2)*/)))
            {
              v2 Pos = tunnelEntryPos+game::GetMoveVector(DiagonalDir[d2]);
              AltitudeBuffer[Pos.X][Pos.Y] = 1+RAND_N(50);
              TypeBuffer[Pos.X][Pos.Y] = JungleType();
              GetWSquare(Pos)->ChangeGWTerrain(GWSpawn(JUNGLE));
            }
          }
          Correct = true;
          break;
        }
      }
      if (Correct) break;
    }
    if (Correct) break;
  }

  if (!Correct) return false;

  if (newattnamPos == ERROR_V2 || tunnelEntryPos == ERROR_V2 || tunnelExitPos == ERROR_V2) {
    return false;
  }

  //ConLogf("UC and New Attnam were successfully placed...");
  // tunnel entry, tunnel exit and New Attnam are ok, find a place for Attnam
  game::BusyAnimation();

  int seen = 0;
  // spawn and reveal all special places
  for (int f = 0; f < game::poiCount(); ++f) {
    owterrain *terra = game::poiByIndex(f);
    bool placeIt = false;
    if (terra->GetConfig() == NEW_ATTNAM) {
      IvanAssert((seen & (1 << 0)) == 0); seen |= (1 << 0);
      placeIt = true;
      terra->SetPosition(newattnamPos);
    } else if (terra->GetConfig() == UNDER_WATER_TUNNEL) {
      IvanAssert((seen & (1 << 1)) == 0); seen |= (1 << 1);
      placeIt = true;
      terra->SetPosition(tunnelEntryPos);
    } else if (terra->GetConfig() == UNDER_WATER_TUNNEL_EXIT) {
      IvanAssert((seen & (1 << 2)) == 0); seen |= (1 << 2);
      placeIt = true;
      terra->SetPosition(tunnelExitPos);
    }
    if (placeIt) {
      terra->SetGenerated(true);
      PlacePOIAtMap(terra);
      poiContinents[terra->GetConfig()] = PetrusLikes;
    }
  }

  IvanAssert(seen == 7);

  // done
  return true;
}


//==========================================================================
//
//  worldmap::FindPOIContinent
//
//==========================================================================
continent *worldmap::FindPOIContinent (owterrain *terra) {
  IvanAssert(terra);
  IvanAssert(terra->GetConfig() > 0);
  return poiContinents[terra->GetConfig()];
}


//==========================================================================
//
//  worldmap::IsFreeContinent
//
//==========================================================================
bool worldmap::IsFreeContinent (continent *cont) {
  for (size_t f = 0; f != poiContinents.size(); f += 1) {
    if (poiContinents[f] == cont) return false;
  }
  return true;
}


//==========================================================================
//
//  worldmap::PlacePOIOnContinent
//
//==========================================================================
bool worldmap::PlacePOIOnContinent (owterrain *terra, continent *cont, bool allowSkip,
                                    bool doRealPlacement, v2 *foundPos)
{
  IvanAssert(terra);
  IvanAssert(cont);
  IvanAssert(terra->GetConfig() > 0);
  IvanAssert(!poiContinents[terra->GetConfig()]);
  IvanAssert(!terra->MustBeSkipped);
  IvanAssert(!terra->IsGenerated());
  IvanAssert(terra->IsSuitableContinent(cont));

  owterrain *closeTo = 0;
  if (terra->GetCloseTo() != 0) {
    closeTo = FindPOIByConfig(terra->GetCloseTo());
    if (closeTo == terra) {
      ABORT("Such intimacy is forbidden!");
    }
    if (!closeTo->IsGenerated()) {
      ABORT("Desperately want to be close to someone who's missing...");
    }
  }

  v2 poipos;
  game::BusyAnimation();

  int bestDist = 0x7fffffff;
  std::vector<v2> possiblePlaces;
  cont->GetShuffledMembers(terra->GetNativeGTerrainType(), possiblePlaces);

  bool success = false;
  for (auto &ppos : possiblePlaces) {
    if (IsFreePlaceForPOI(cont, terra, ppos)) {
      if (closeTo) {
        //const int sqdist = ppos.GetSquaredDistance(closeTo->GetPosition());
        const int sqdist = ppos.GetManhattanDistance(closeTo->GetPosition());
        if (sqdist < bestDist /*|| RAND_2*/) {
          success = true;
          poipos = ppos;
          bestDist = sqdist;
        }
      } else {
        poipos = ppos;
        success = true;
        break;
      }
    }
  }

  if (!success) {
    // if we can skip this dungeon, then skip it, otherwise signal failure
    if (!doRealPlacement) return false;
    if (!terra->CanBeSkipped()) return false; // oops
    if (allowSkip) {
      terra->MustBeSkipped = true;
    } else {
      return false;
    }
  } else {
    // ok, we can place it
    //ConLogf("place poicfg #%d at (%d,%d)...", terra->GetConfig(), poipos.X, poipos.Y);
    if (foundPos) *foundPos = poipos;
    if (doRealPlacement) {
      terra->SetGenerated(true);
      terra->SetPosition(poipos);
      if (terra->PlaceInitially()) PlacePOIAtMap(terra);
      poiContinents[terra->GetConfig()] = cont;
    }
  }

  return true;
}


//==========================================================================
//
//  worldmap::Generate
//
//==========================================================================
void worldmap::Generate () {
  Alloc2D(OldAltitudeBuffer, XSize, YSize);
  Alloc2D(OldTypeBuffer, XSize, YSize);

  debugMessages = (POI_PLACEMENT_DEBUG != 0);

  {
    auto cfg = FindWorldMapOptionsConfig();
    /*
    ConLogf("=== FOUND! ===");
    ConLogf("  max temperature: %d", cfg->MaxTemperature);
    ConLogf("  latitude effect: %d", cfg->LatitudeEffect);
    ConLogf("  altitude effect: %f", (double)cfg->AltitudeEffect);
    ConLogf("  cold  : %d", cfg->TemperatureCold);
    ConLogf("  medium: %d", cfg->TemperatureMedium);
    ConLogf("  warm  : %d", cfg->TemperatureWarm);
    ConLogf("  hot   : %d", cfg->TemperatureHot);
    */
    MAX_TEMPERATURE = cfg->MaxTemperature;
    LATITUDE_EFFECT = cfg->LatitudeEffect;
    ALTITUDE_EFFECT = cfg->AltitudeEffect;
    COLD = cfg->TemperatureCold;
    MEDIUM = cfg->TemperatureMedium;
    WARM = cfg->TemperatureWarm;
    HOT = cfg->TemperatureHot;
  }

  /*
  ConLogf("ocean survive: <%s>", GWSpawn(OceanType())->GetSurviveMessage().CStr());
  ConLogf("ocean survive: <%s>", GWSpawn(OceanType())->SurviveMessage());
  ConLogf("ocean walkability: 0x%04x (0x%08x)", GWSpawn(OceanType())->GetWalkability(), ANY_MOVE&~WALK);
  */

  for (;;) {
    if (debugMessages) {
      ConLogf("generating new planet...");
    }
    game::SetBusyAnimationMessage(CONST_S("Trying new world..."));
    game::BusyAnimation();

    ResetItAll(true); // recreate

    RandomizeAltitude();
    SmoothAltitude();
    GenerateClimate();
    SmoothClimate();
    CalculateContinents();

    if (Continent.size() < 2) ABORT("Strange things happens in Universe...");
    //ConLogf("%u continents generated...", Continent.size());

    //ConLogf("resetting POI info...");
    ResetPOIs(); // this also sets `owterrain::MustBeSkipped`, using spawn probabilities

    // find place for attnam
    if (!attnam) ABORT("Who stole my Attnam?!");

    game::BusyAnimation();

    if (debugMessages) {
      ConLogf("trying to find room for other Attnam and New Attnam...");
    }

    continent *PetrusLikes = FindPlaceForAttnam();
    if (!PetrusLikes) continue; // alas

    /* new brute-force search */
    // place Attnam
    giveUp = false;
    startTime = time(NULL);
    checkCounter = 0;
    if (true) {
      IvanAssert(!attnam->IsGenerated());
      if (!PlacePOIOnContinent(attnam, PetrusLikes, false)) {
        if (debugMessages) {
          ConLogf("OOPS! failed to place Attnam!");
        }
        continue;
      }
    }

    if (debugMessages) {
      ConLogf("================================================");
    }

    if (!BruteForceSearch(attnam)) continue;

    bool success = true;
    for (int f = 0; success && f < game::poiCount(); ++f) {
      owterrain *terra = game::poiByIndex(f);
      if (!terra) ABORT("Somone stole our terrain!");
      if (terra == newattnam) continue;
      if (terra == underwatertunnel) continue;
      if (terra == underwatertunnelexit) continue;
      if (terra->MustBeSkipped) continue;
      if (terra->SeparateContinent()) {
        if (debugMessages) {
          ConLogf("brute-forcing POI #%d (%s)...",
                  f, terra->GetNameSingular().CStr());
        }
        if (!BruteForceSearch(terra)) success = false;
      }
    }
    if (!success) continue; // generate new world

    // we may have POIs which doesn't care about continent.
    // FIXME: for now, it is not supported. it should be supported in the future.
    for (int f = 0; f < game::poiCount(); ++f) {
      owterrain *terra = game::poiByIndex(f);
      if (!terra) ABORT("Somone stole our terrain!");
      if (!terra->MustBeSkipped && !terra->IsGenerated()) {
        ABORT("POI #%d (%s) doesn't belong to any continent!",
              f, terra->GetNameSingular().CStr());
      }
    }

    // player just exited new attnam
    PLAYER->PutTo(newattnam->GetPosition());

    CalculateLuminances();
    CalculateNeighbourBitmapPoses();

    // break infinite loop, we're done
    break;
  }

  game::SetBusyAnimationMessage(festring::EmptyStr());

  // done
  delete [] OldAltitudeBuffer;
  delete [] OldTypeBuffer;
}


//==========================================================================
//
//  worldmap::GetFeelings
//
//  return "feeling" messages
//
//==========================================================================
festring worldmap::GetFeelings () const {
  festring res;
  for (int f = 0; f < game::poiCount(); ++f) {
    owterrain *terra = game::poiByIndex(f);
    if (!terra) ABORT("Somone stole our terrain!");
    if (!terra->MustBeSkipped && terra->IsGenerated()) {
      festring msg = terra->GetGenerationMessage();
      if (!msg.IsEmpty()) {
        //ADD_MESSAGE("%s", msg.CStr());
        if (!res.IsEmpty()) res << " ";
        res << msg;
      }
    }
  }
  return res;
}


//==========================================================================
//
//  worldmap::RandomizeAltitude
//
//==========================================================================
void worldmap::RandomizeAltitude () {
  game::BusyAnimation();
  for (int x = 0; x < XSize; ++x) {
    for (int y = 0; y < YSize; ++y) {
      AltitudeBuffer[x][y] = 4000-RAND_N(8000);
    }
  }
}


//==========================================================================
//
//  worldmap::SmoothAltitude
//
//==========================================================================
void worldmap::SmoothAltitude () {
  for (int c = 0; c < 10; ++c) {
    game::BusyAnimation();
    int x, y;
    for (y = 0; y < YSize; ++y) SafeSmooth(0, y);
    for (x = 1; x < XSize - 1; ++x) {
      SafeSmooth(x, 0);
      for (y = 1; y < YSize - 1; ++y) FastSmooth(x, y);
      SafeSmooth(x, YSize - 1);
    }
    for (y = 0; y < YSize; ++y) SafeSmooth(XSize - 1, y);
  }
}


//==========================================================================
//
//  worldmap::FastSmooth
//
//==========================================================================
void worldmap::FastSmooth (int x, int y) {
  sLong HeightNear = 0;
  int d;
  for (d = 0; d < 4; ++d) HeightNear += OldAltitudeBuffer[x + DirX[d]][y + DirY[d]];
  for (d = 4; d < 8; ++d) HeightNear += AltitudeBuffer[x + DirX[d]][y + DirY[d]];
  OldAltitudeBuffer[x][y] = AltitudeBuffer[x][y];
  AltitudeBuffer[x][y] = HeightNear >> 3;
}


//==========================================================================
//
//  worldmap::SafeSmooth
//
//==========================================================================
void worldmap::SafeSmooth (int x, int y) {
  sLong HeightNear = 0;
  int d, SquaresNear = 0;
  for (d = 0; d < 4; ++d) {
    int X = x + DirX[d];
    int Y = y + DirY[d];
    if (IsValidPos(X, Y)) {
      HeightNear += OldAltitudeBuffer[X][Y];
      ++SquaresNear;
    }
  }
  for (d = 4; d < 8; ++d) {
    int X = x + DirX[d];
    int Y = y + DirY[d];
    if (IsValidPos(X, Y)) {
      HeightNear += AltitudeBuffer[X][Y];
      ++SquaresNear;
    }
  }
  OldAltitudeBuffer[x][y] = AltitudeBuffer[x][y];
  AltitudeBuffer[x][y] = HeightNear / SquaresNear;
}


//==========================================================================
//
//  worldmap::GenerateClimate
//
//==========================================================================
void worldmap::GenerateClimate () {
  game::BusyAnimation();
  for (int y = 0; y < YSize; ++y) {
    double DistanceFromEquator = fabs(double(y) / YSize - 0.5);
    truth LatitudeRainy = DistanceFromEquator <= 0.05 || (DistanceFromEquator > 0.25 && DistanceFromEquator <= 0.45);
    for (int x = 0; x < XSize; ++x) {
      if (AltitudeBuffer[x][y] <= 0) {
        TypeBuffer[x][y] = OceanType();
        continue;
      }
      truth Rainy = LatitudeRainy;
      if (!Rainy) {
        for(int d = 0; d < 8; ++d) {
          v2 Pos = v2(x, y) + game::GetMoveVector(d);
          if (IsValidPos(Pos) && AltitudeBuffer[Pos.X][Pos.Y] <= 0) {
            Rainy = true;
            break;
          }
        }
      }
      int Temperature = int(MAX_TEMPERATURE-DistanceFromEquator*LATITUDE_EFFECT-AltitudeBuffer[x][y]*ALTITUDE_EFFECT);
      int Type = 0;
           if (Temperature <= COLD) Type = (Rainy ? SnowType() : GlacierType());
      else if (Temperature <= MEDIUM) Type = (Rainy ? EGForestType() : SnowType());
      else if (Temperature <= WARM) Type = (Rainy ? LForestType() : SteppeType());
      else if (Temperature <= HOT) Type = (Rainy ? LForestType() : DesertType());
      else Type = (Rainy ? JungleType() : DesertType());
      TypeBuffer[x][y] = Type;
    }
  }
}


//==========================================================================
//
//  worldmap::SmoothClimate
//
//==========================================================================
void worldmap::SmoothClimate () {
  for (int c = 0; c < 3; ++c) {
    game::BusyAnimation();
    for (int x = 0; x < XSize; ++x) {
      for (int y = 0; y < YSize; ++y) {
        if ((OldTypeBuffer[x][y] = TypeBuffer[x][y]) != OceanType()) {
          TypeBuffer[x][y] = WhatTerrainIsMostCommonAroundCurrentTerritorySquareIncludingTheSquareItself(x, y);
        }
      }
    }
  }
  game::BusyAnimation();
  for (int x = 0; x < XSize; ++x) {
    for (int y = 0; y < YSize; ++y) {
      //auto terraProto = protocontainer<gwterrain>::GetProto(TypeBuffer[x][y]);
      //if (!terraProto) ABORT("Oops! No gwterrain prototype for type #%d!\n", TypeBuffer[x][y]);
      //Map[x][y]->ChangeGWTerrain(terraProto->Spawn());
      Map[x][y]->ChangeGWTerrain(GWSpawn(TypeBuffer[x][y]));
    }
  }
}


/* Evil... */
#define ANALYZE_TYPE(type) {\
  int T = type;\
  for (c = 0; c < u; ++c) if (T == UsedType[c]) { ++TypeAmount[c]; break; }\
  if (c == u) { UsedType[u] = T; TypeAmount[u++] = 1; }\
}


//==========================================================================
//
//  worldmap::WhatTerrainIsMostCommonAroundCurrentTerritorySquareIncludingTheSquareItself
//
//  k8: WOW!
//
//==========================================================================
int worldmap::WhatTerrainIsMostCommonAroundCurrentTerritorySquareIncludingTheSquareItself (int x, int y) {
  int UsedType[9];
  int TypeAmount[9];
  int c, d, u = 1;
  UsedType[0] = TypeBuffer[x][y];
  TypeAmount[0] = 1;
  for (d = 0; d < 4; ++d) {
    int X = x+DirX[d];
    int Y = y+DirY[d];
    if (IsValidPos(X, Y)) ANALYZE_TYPE(OldTypeBuffer[X][Y]);
  }
  for (d = 4; d < 8; ++d) {
    int X = x+DirX[d];
    int Y = y+DirY[d];
    if (IsValidPos(X, Y)) ANALYZE_TYPE(TypeBuffer[X][Y]);
  }
  int MostCommon = 0;
  for (c = 1; c < u; ++c) if (TypeAmount[c] > TypeAmount[MostCommon] && UsedType[c] != OceanType()) MostCommon = c;
  return UsedType[MostCommon];
}


//==========================================================================
//
//  worldmap::CalculateContinents
//
//==========================================================================
void worldmap::CalculateContinents () {
  for (uInt c = 1; c < Continent.size(); ++c) delete Continent[c];
  Continent.resize(1, 0);
  memset(ContinentBuffer[0], 0, XSizeTimesYSize*sizeof(uChar));
  game::BusyAnimation();
  for (int x = 0; x < XSize; ++x) {
    for (int y = 0; y < YSize; ++y) {
      if (AltitudeBuffer[x][y] > 0) {
        truth Attached = false;
        for (int d = 0; d < 8; ++d) {
          v2 Pos = v2(x, y)+game::GetMoveVector(d);
          if (IsValidPos(Pos)) {
            cint NearCont = ContinentBuffer[Pos.X][Pos.Y];
            if (NearCont) {
              cint ThisCont = ContinentBuffer[x][y];
              if (ThisCont) {
                if (ThisCont != NearCont) {
                  if (Continent[ThisCont]->GetSize() < Continent[NearCont]->GetSize()) {
                    Continent[ThisCont]->AttachTo(Continent[NearCont]);
                  } else {
                    Continent[NearCont]->AttachTo(Continent[ThisCont]);
                  }
                }
              } else {
                Continent[NearCont]->Add(v2(x, y));
              }
              Attached = true;
            }
          }
        }
        if (!Attached) {
          if (Continent.size() == 255) {
            RemoveEmptyContinents();
            if (Continent.size() == 255) ABORT("Valpurus shall not carry more continents!");
          }
          continent *NewContinent = new continent(Continent.size());
          NewContinent->Add(v2(x, y));
          Continent.push_back(NewContinent);
        }
      }
    }
  }
  RemoveEmptyContinents();
  for (uInt c = 1; c < Continent.size(); ++c) Continent[c]->GenerateInfo();
}


//==========================================================================
//
//  worldmap::RemoveEmptyContinents
//
//==========================================================================
void worldmap::RemoveEmptyContinents () {
  for (uInt c = 1; c < Continent.size(); ++c) {
    if (!Continent[c]->GetSize()) {
      for (uInt i = Continent.size()-1; i >= c; --i) {
        if (Continent[i]->GetSize()) {
          Continent[i]->AttachTo(Continent[c]);
          delete Continent[i];
          Continent.pop_back();
          break;
        } else {
          delete Continent[i];
          Continent.pop_back();
        }
      }
    }
  }
}


//==========================================================================
//
//  worldmap::GetListOfKnownPOIs
//
//  FIXME: optimise this!
//
//==========================================================================
void worldmap::GetListOfKnownPOIs (std::vector<owterrain *> &list) {
  list.clear();
  for (int f = 0; f < game::poiCount(); ++f) {
    owterrain *terra = game::poiByIndex(f);
    if (terra && terra->IsGenerated() && terra->IsPlaced()) {
      const v2 pos = terra->GetPosition();
      wsquare **Square = &Map[pos.X][pos.Y];
      if ((*Square)->LastSeen) {
        list.push_back(terra);
      }
    }
  }
}


//==========================================================================
//
//  worldmap::Draw
//
//==========================================================================
void worldmap::Draw (truth) const {
  cint XMin = Max(game::GetCamera().X, 0);
  cint YMin = Max(game::GetCamera().Y, 0);
  cint XMax = Min(XSize, game::GetCamera().X+game::GetScreenXSize());
  cint YMax = Min(YSize, game::GetCamera().Y+game::GetScreenYSize());
  blitdata BlitData = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { 0, 0 },
    { TILE_SIZE, TILE_SIZE },
    { 0 },
    TRANSPARENT_COLOR,
    ALLOW_ANIMATE|ALLOW_ALPHA
  };
  if (!game::GetSeeWholeMapCheatMode()) {
    for (int x = XMin; x < XMax; ++x) {
      BlitData.Dest = game::CalculateScreenCoordinates(v2(x, YMin));
      wsquare **Square = &Map[x][YMin];
      for (int y = YMin; y < YMax; ++y, ++Square, BlitData.Dest.Y += TILE_SIZE) {
        if ((*Square)->LastSeen) (*Square)->Draw(BlitData);
      }
    }
  } else {
    for (int x = XMin; x < XMax; ++x) {
      BlitData.Dest = game::CalculateScreenCoordinates(v2(x, YMin));
      wsquare **Square = &Map[x][YMin];
      for (int y = YMin; y < YMax; ++y, ++Square, BlitData.Dest.Y += TILE_SIZE) (*Square)->Draw(BlitData);
    }
  }
}


//==========================================================================
//
//  worldmap::DrawMiniMap
//
//==========================================================================
void worldmap::DrawMiniMap (v2 XY0, v2 Size) const {
  bitmap *xbmp = game::PrepareMiniMapBitmap(Size * TILE_SIZE);

  cint XMin = Max(XY0.X, 0);
  cint YMin = Max(XY0.Y, 0);
  cint XMax = Min(XSize, XY0.X + Size.X);
  cint YMax = Min(YSize, XY0.Y + Size.Y);

  blitdata BlitData = {
    xbmp,
    { 0, 0 },
    { 0, 0 },
    { TILE_SIZE, TILE_SIZE },
    { 0 },
    TRANSPARENT_COLOR,
    ALLOW_ANIMATE|ALLOW_ALPHA
  };
  if (!game::GetSeeWholeMapCheatMode()) {
    for (int x = XMin; x < XMax; ++x) {
      //BlitData.Dest = game::CalculateScreenCoordinates(v2(x, YMin));
      BlitData.Dest = v2((x - XMin) * TILE_SIZE, 0);
      wsquare **Square = &Map[x][YMin];
      for (int y = YMin; y < YMax; ++y, ++Square, BlitData.Dest.Y += TILE_SIZE) {
        if ((*Square)->LastSeen) {
          (*Square)->SendStrongNewDrawRequest();
          (*Square)->Draw(BlitData);
        }
      }
    }
  } else {
    for (int x = XMin; x < XMax; ++x) {
      //BlitData.Dest = game::CalculateScreenCoordinates(v2(x, YMin));
      BlitData.Dest = v2((x - XMin) * TILE_SIZE, 0);
      wsquare **Square = &Map[x][YMin];
      for (int y = YMin; y < YMax; ++y, ++Square, BlitData.Dest.Y += TILE_SIZE) {
        (*Square)->Draw(BlitData);
      }
    }
  }
}


//==========================================================================
//
//  worldmap::CalculateLuminances
//
//==========================================================================
void worldmap::CalculateLuminances () {
  for (feuLong c = 0; c < XSizeTimesYSize; ++c) Map[0][c]->CalculateLuminance();
}


//==========================================================================
//
//  worldmap::CalculateNeighbourBitmapPoses
//
//==========================================================================
void worldmap::CalculateNeighbourBitmapPoses () {
  for (feuLong c = 0; c < XSizeTimesYSize; ++c) Map[0][c]->GetGWTerrain()->CalculateNeighbourBitmapPoses();
}


//==========================================================================
//
//  worldmap::GetNeighbourWSquare
//
//==========================================================================
wsquare *worldmap::GetNeighbourWSquare (v2 Pos, int I) const {
  Pos += game::GetMoveVector(I);
  if (Pos.X >= 0 && Pos.Y >= 0 && Pos.X < XSize && Pos.Y < YSize) return Map[Pos.X][Pos.Y];
  return 0;
}


//==========================================================================
//
//  worldmap::RevealEnvironment
//
//==========================================================================
void worldmap::RevealEnvironment (v2 Pos, int Radius) {
  rect Rect;
  femath::CalculateEnvironmentRectangle(Rect, Border, Pos, Radius);
  for (int x = Rect.X1; x <= Rect.X2; ++x) {
    for (int y = Rect.Y1; y <= Rect.Y2; ++y) {
      Map[x][y]->SignalSeen();
    }
  }
}


//==========================================================================
//
//  operator <<
//
//==========================================================================
outputfile &operator << (outputfile &SaveFile, const worldmap *WorldMap) {
  WorldMap->Save(SaveFile);
  return SaveFile;
}


//==========================================================================
//
//  operator >>
//
//==========================================================================
inputfile &operator >> (inputfile &SaveFile, worldmap *&WorldMap) {
  WorldMap = new worldmap;
  WorldMap->Load(SaveFile);
  return SaveFile;
}


//==========================================================================
//
//  worldmap::UpdateLOS
//
//==========================================================================
void worldmap::UpdateLOS () {
  game::RemoveLOSUpdateRequest();
  int Radius = PLAYER->GetLOSRange();
  sLong RadiusSquare = Radius*Radius;
  v2 Pos = PLAYER->GetPos();
  rect Rect;
  femath::CalculateEnvironmentRectangle(Rect, Border, Pos, Radius);
  for (int x = Rect.X1; x <= Rect.X2; ++x) {
    for (int y = Rect.Y1; y <= Rect.Y2; ++y) {
      if (sLong(HypotSquare(Pos.X-x, Pos.Y-y)) <= RadiusSquare) {
        Map[x][y]->SignalSeen();
      }
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct WMNode {
public:
  v2 pos;
  WMNode *parent;
  int Distance;
  int DistanceEstimate;
  int Diagonals;
  bool Sea;

public:
  WMNode (int x, int y, WMNode *aparent)
    : pos(x, y)
    , parent(aparent)
    , Distance(0)
    , DistanceEstimate(0)
    , Diagonals(0)
  {}
};


struct WMNodePtr {
public:
  WMNode *node;

public:
  WMNodePtr (WMNode *anode) : node(anode) {}
  bool operator < (const WMNodePtr &other) const;
};


//==========================================================================
//
//  WMNodePtr::operator <
//
//==========================================================================
bool WMNodePtr::operator < (const WMNodePtr &other) const {
  //if (node->Sea != other.node->Sea) return !node->Sea;
  if (node->DistanceEstimate != other.node->DistanceEstimate) {
    return (node->DistanceEstimate > other.node->DistanceEstimate);
  }
  return (node->Diagonals > other.node->Diagonals);

}


//==========================================================================
//
//  FreeNodes
//
//==========================================================================
static void FreeNodes (std::list<WMNode *> &nodes) {
  while (!nodes.empty()) {
    WMNode *node = nodes.front();
    nodes.pop_front();
    delete node;
  }
}


//==========================================================================
//
//  GetStep
//
//==========================================================================
static v2 GetStep (WMNode *node, const v2 from) {
  while (node) {
    #if 0
    ConLogf("...trace: node=(%d,%d); from=(%d,%d)",
            node->pos.X, node->pos.Y, from.X, from.Y);
    #endif
    if (abs(node->pos.X - from.X) <= 1 && abs(node->pos.Y - from.Y) <= 1) {
      return node->pos - from;
    }
    node = node->parent;
  }
  return ERROR_V2;
}


//==========================================================================
//
//  worldmap::CalculateStepDir
//
//==========================================================================
v2 worldmap::CalculateStepDir (ccharacter *Char, const v2 from, const v2 to) const
{
  const int TryOrder[8] = { 1, 3, 4, 6, 0, 2, 5, 7 };
  //std::list<WMNode *> queue;
  std::priority_queue<WMNodePtr> queue;
  std::list<WMNode *> nodes;
  std::unordered_set<v2> seen;

  if (from == to) return v2(0, 0);

  WMNode *node = new WMNode(from.X, from.Y, nullptr);
  const bool initialSea = !(Map[node->pos.X][node->pos.Y]->GetWalkability() & WALK);
  node->Sea = initialSea;
  queue.push(WMNodePtr(node));
  nodes.push_back(node);
  seen.insert(from);

  while (!queue.empty()) {
    WMNode *node = queue.top().node;
    queue.pop();
    if (node->pos == to) {
      const v2 res = GetStep(node, from);
      FreeNodes(nodes);
      return res;
    }
    for (int d = 0; d < 8; ++d) {
      v2 NodePos = node->pos + game::GetMoveVector(TryOrder[d]);
      if (NodePos.X >= 0 && NodePos.Y >= 0 &&
          NodePos.X < GetXSize() && NodePos.Y < GetYSize())
      {
        auto it = seen.find(NodePos);
        if (it == seen.end()) {
          //wsquare *osq = Map[node->pos.X][node->pos.Y];
          wsquare *wsq = Map[NodePos.X][NodePos.Y];
          if (Char->CanMoveOn(wsq)) {
            WMNode *nn = new WMNode(NodePos.X, NodePos.Y, node);
            nn->Sea = !(wsq->GetSquareWalkability() & WALK);
            //if (initialSea) nn->Sea = !nn->Sea;
            nn->Distance = node->Distance + 1;
            nn->Diagonals = node->Diagonals;
            if (d >= 4) nn->Diagonals += 1;
            int Remaining = to.X - NodePos.X;
            if (Remaining < NodePos.X - to.X) Remaining = NodePos.X - to.X;
            if (Remaining < NodePos.Y - to.Y) Remaining = NodePos.Y - to.Y;
            if (Remaining < to.Y - NodePos.Y) Remaining = to.Y - NodePos.Y;
            // moving into/out of the sea costs a lot
            #if 0
            if (nn->Sea != node->Sea) {
              nn->Distance += 100000;
            }
            #else
            if (nn->Sea != node->Sea) {
              Remaining += 100000;
            }
            #endif
            nn->DistanceEstimate = nn->Distance + Remaining;
            queue.push(nn);
            nodes.push_back(nn);
          }
          seen.insert(NodePos);
        }
      }
    }
  }

  FreeNodes(nodes);
  return ERROR_V2;
}
