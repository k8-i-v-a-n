/*
 *
 *  Iter Vehemens ad Necem (IVAN)
 *  Copyright (C) Timo Kiviluoto
 *  Released under the GNU General
 *  Public License
 *
 *  See LICENSING which should be included
 *  along with this file for more details
 *
 */
#ifndef __WORLDMAP_H__
#define __WORLDMAP_H__

#include <vector>

#include "area.h"


class wsquare;
class continent;
class owterrain;

typedef std::vector<character *> charactervector;
using ContinentVector = std::vector<continent *>;
typedef std::list<owterrain *> poilist;


typedef owterrain *(*PlaceSpawner) ();


class worldmap : public area {
public:
  worldmap (int, int);
  worldmap ();
  virtual ~worldmap ();

  void Generate ();
  void Save (outputfile &) const;
  void Load (inputfile &);
  wsquare *GetWSquare (v2 Pos) const { return Map[Pos.X][Pos.Y]; }
  wsquare *GetWSquare (int x, int y) const { return Map[x][y]; }
  void GenerateClimate ();
  int WhatTerrainIsMostCommonAroundCurrentTerritorySquareIncludingTheSquareItself (int, int);
  void CalculateContinents ();
  void SmoothAltitude ();
  void SmoothClimate ();
  void RandomizeAltitude ();
  continent *GetContinentUnder (v2) const;
  continent *GetContinent (int) const;
  void RemoveEmptyContinents ();
  int GetAltitude (v2);
  charactervector &GetPlayerGroup ();
  character *GetPlayerGroupMember (int);
  virtual void Draw (truth AnimationDraw) const override;
  virtual void DrawMiniMap (v2 XY0, v2 Size) const override;
  void CalculateLuminances ();
  void CalculateNeighbourBitmapPoses ();
  wsquare *GetNeighbourWSquare (v2, int) const;
  v2 GetEntryPos (ccharacter *, int) const;
  void RevealEnvironment (v2, int);
  void SafeSmooth (int, int);
  void FastSmooth (int, int);
  wsquare ***GetMap () const { return Map; }
  void UpdateLOS ();

  // return "feeling" messages
  festring GetFeelings () const;

  void GetListOfKnownPOIs (std::vector<owterrain *> &list);

  v2 CalculateStepDir (ccharacter *Char, const v2 from, const v2 to) const;

protected:
  // gum soluition!
  std::vector<continent *> poiContinents;

  // POI placement
  void ResetPOIs ();
  ContinentVector FindPlacesForPOI (owterrain *terra, truth shuffle);
  truth PlaceAttnamsAndUT (continent *PetrusLikes);
  truth IsFreePlaceForPOI (continent *cont, owterrain *forTerra, v2 pos);

  void ResetItAll (truth recreate);

  owterrain *FindPOIByConfig (int config);

  continent *FindPOIContinent (owterrain *terra);
  bool IsFreeContinent (continent *cont);

  // return `0` if cannot
  continent *FindPlaceForAttnam ();

  bool PlacePOIOnContinent (owterrain *terra, continent *cont, bool allowSkip,
                            bool doRealPlacement=true, v2 *foundPos=0);

public:
  void PlacePOIAtMap (owterrain *terra, truth forceReveal=false); // does map revealing if necessary

public:
  struct TerraInfo {
    owterrain *terra; // POI terrain
    v2 pos; // current position
    // for debug
    size_t cpidx, cptotal;

    FORCE_INLINE TerraInfo () : terra(0), pos(0, 0), cpidx(0), cptotal(0) {}
  };

protected:
  void PlaceTheList (std::vector<TerraInfo> &list, continent *cont);
  void CreateInfoList (std::vector<TerraInfo> &list, owterrain *contMain);
  bool CheckListConstraints (std::vector<TerraInfo> &list);
  bool TryOnePoisition (std::vector<TerraInfo> &list, size_t listidx, continent *cont);

  bool BruteForceSearch (owterrain *contMain);

protected:
  wsquare ***Map;
  ContinentVector Continent;
  uChar **TypeBuffer;
  uChar **OldTypeBuffer;
  short **AltitudeBuffer;
  short **OldAltitudeBuffer;
  uChar **ContinentBuffer;
  charactervector PlayerGroup;
};

outputfile &operator << (outputfile &, const worldmap *);
inputfile &operator >> (inputfile &, worldmap *&);


#endif
