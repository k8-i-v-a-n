#include <algorithm>
#include <iostream>
#include <fstream>
#include <cstdlib>

#include "graphics.h"
#include "bitmap.h"
#include "whandler.h"
#include "feerror.h"
#include "feio.h"
#include "rawbit.h"
#include "felist.h"
#include "fesave.h"


//==========================================================================
//
//  DumpFont
//
//==========================================================================
static void DumpFont (rawfont *font) {
  FILE *fo = fopen("zfont.txt", "w");
  fprintf(fo, "IVAN raster font v1\n");
  fprintf(fo, "width = %d;\n", font->FontWidth());
  fprintf(fo, "heght = %d;\n", font->FontHeight());
  fprintf(fo, "spaceWidth = %d;\n", font->CharWidth(32));
  fprintf(fo, "firstChar = 33;\n");
  fprintf(fo, "lastChar = 255;\n");
  for (int f = 33; f != 256; f += 1) {
    fprintf(fo, "\n[char%03d]\n", f);
    fprintf(fo, "offset = %d;\n", font->CharOfs(f));
    fprintf(fo, "width = %d;\n", font->CharWidth(f));
    fprintf(fo, "bitmap:\n");
    const uint32_t *bmp = font->GetCharBitmap(f);
    for (int y = 0; y != font->FontHeight(); y += 1) {
      uint32_t b = *bmp; bmp += 1;
      if (font->CharOfs(f)) b >>= font->CharOfs(f);
      b <<= 1;
      //fprintf(fo, "  ");
      for (int x = 0; x != font->CharWidth(f); x += 1) {
        fputc((b & 1 ? '#' : '.'), fo);
        b >>= 1;
      }
      fprintf(fo, "\n");
      IvanAssert(b == 0);
    }
  }
  fclose(fo);
}


//==========================================================================
//
//  Main
//
//==========================================================================
int Main (int argc, char **argv) {
  rawfont *font = new rawfont(CONST_S("graphics/Font.png"), 5, 8, 8, 16, 16, true);

  DumpFont(font);

  delete font;
  return 1;
}
