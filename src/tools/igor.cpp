#include <algorithm>
#include <iostream>
#include <fstream>
#include <cstdlib>

#include "graphics.h"
#include "bitmap.h"
#include "whandler.h"
#include "feerror.h"
#include "feio.h"
#include "rawbit.h"
#include "felist.h"
#include "fesave.h"


#define STRETCH 5
#define TILE_SIZE 16
cv2 TILE_V2(TILE_SIZE, TILE_SIZE);


static v2 Cursor(0, 0);
static std::vector<v2> DrawQueue;
static uShort Selected;

// 0 is k8, 1 is "another"
static rawbitmap *CBitmaps[2];
static int cbmIndex = 0;
static bool cbmChanged = false;
static bool secondBitmapEnabled = true;


static inline bool IsReadOnly () { return (cbmIndex != 0); }
static void SetChanged () { if (cbmIndex == 0) cbmChanged = true; }
static void SetUnchanged () { if (cbmIndex == 0) cbmChanged = false; }

#define CBitmap  (CBitmaps[cbmIndex])


static packcol16 Color[4] = {
  MakeRGB16(47, 131, 95),
  MakeRGB16(123, 0, 127),
  MakeRGB16(0, 131, 131),
  MakeRGB16(175, 131, 0)
};


//==========================================================================
//
//  DrawMainHelp
//
//==========================================================================
static void DrawMainHelp () {
  v2 helpPos = v2(400, 480 - 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, ORANGE, CONST_S(cbmIndex ? "COMM. FORK" : "k8 I.V.A.N.")); helpPos += v2(0, 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, CONST_S("Control cursor: \1Ywasd\2 and \1YWASD\2")); helpPos += v2(0, 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, CONST_S("Select m-col: \1Y1\2-\1Y4\2")); helpPos += v2(0, 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, CONST_S("Safely alter gradient: \1Y�\2")); helpPos += v2(0, 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, CONST_S("Power alter gradient: \1Y<>\2")); helpPos += v2(0, 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, CONST_S("Swap m-cols: \1Y=\2")); helpPos += v2(0, 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, CONST_S("Push to / pop from draw queue: \1Yp\2")); helpPos += v2(0, 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, CONST_S("Clear draw queue: \1Yc\2")); helpPos += v2(0, 10);
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, CONST_S("Roll picture: \1Yarrow keys\2")); helpPos += v2(0, 20);
  festring ss("MColor selected: \1Y"); ss << Selected + 1;
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, ss); helpPos += v2(0, 10);
  ss.Empty(); ss << "Current position: (\1Y" << Cursor.X << "\2, \1Y" << Cursor.Y << "\2)";
  FONT->PrintStr(DOUBLE_BUFFER, helpPos, WHITE, ss);
}


//==========================================================================
//
//  DrawSprQueue
//
//==========================================================================
static void DrawSprQueue (bitmap &CursorBitmap) {
  blitdata B2 = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { RES.X - STRETCH * 32 - 20, RES.Y - STRETCH * 16 - 10 },
    { TILE_SIZE, TILE_SIZE },
    { STRETCH }
  };
  blitdata B3 = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { 0, 0 },
    { TILE_SIZE, TILE_SIZE },
    { 0 },
    TRANSPARENT_COLOR
  };

  for (int c = 0; c < (int)DrawQueue.size(); ++c) {
    B2.Src = DrawQueue[c];
    DOUBLE_BUFFER->StretchBlit(B2);
    B3.Dest = DrawQueue[c];
    CursorBitmap.NormalMaskedBlit(B3);
  }
}


//==========================================================================
//
//  DrawMainScreen
//
//==========================================================================
static void DrawMainScreen (rawbitmap *cbm, bitmap &CursorBitmap) {
  blitdata B1 = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { RES.X - STRETCH * 16 - 10, RES.Y - STRETCH * 16 - 10 },
    { TILE_SIZE, TILE_SIZE },
    { STRETCH }
  };

  DOUBLE_BUFFER->ClearToColor(0);
  DOUBLE_BUFFER->Fill(0, 0, cbm->GetSize(), 0xF81F);
  cbm->MaskedBlit(DOUBLE_BUFFER, v2(0, 0), v2(0, 0), cbm->GetSize(), Color);
  DOUBLE_BUFFER->DrawRectangle(RES.X-STRETCH*16-12, RES.Y-STRETCH*16-12, RES.X-9, RES.Y-9, DARK_GRAY, true);
  DOUBLE_BUFFER->DrawRectangle(RES.X-STRETCH*32-22, RES.Y-STRETCH*16-12, RES.X-STRETCH*16-19, RES.Y-9, DARK_GRAY, true);
  B1.Src = Cursor;
  DOUBLE_BUFFER->StretchBlit(B1);

  DrawMainHelp();
  DrawSprQueue(CursorBitmap);

  blitdata B3 = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { 0, 0 },
    { TILE_SIZE, TILE_SIZE },
    { 0 },
    TRANSPARENT_COLOR
  };
  B3.Dest = Cursor;
  CursorBitmap.NormalMaskedBlit(B3);
}


//==========================================================================
//
//  EditTile
//
//==========================================================================
static void EditTile (rawbitmap *cbm, v2 pos, v2 size, bitmap &CursorBitmap, bool readOnly) {
  rawbitmap part(*cbm, pos, size);
  enum { MAG_SIZE = 16 };
  v2 curpos(0, 0);
  uChar curcidx = 192 + 15;
  for (;;) {
    DrawMainScreen(CBitmap, CursorBitmap);

    v2 gpos(400, 10);
    DOUBLE_BUFFER->DrawRectangle(gpos, gpos + v2(size.X * MAG_SIZE + 3, size.Y * MAG_SIZE + 3),
                                 LIGHT_GRAY, true);

    gpos += v2(2, 2);
    for (int y = 0; y != size.Y; y += 1) {
      for (int x = 0; x != size.X; x += 1) {
        v2 rpos = gpos + v2(x * MAG_SIZE, y * MAG_SIZE);
        packcol16 cc = part.GetRGB16Pixel(x, y);
        if (cc == TRANSPARENT_COLOR) {
          DOUBLE_BUFFER->Fill(rpos + v2(MAG_SIZE / 2 - 1, MAG_SIZE / 2 - 1), 3, 3, DARK_GRAY);
        } else {
          DOUBLE_BUFFER->Fill(rpos, MAG_SIZE, MAG_SIZE, cc);
        }
      }
    }

    for (int y = 1; y != size.Y; y += 1) {
      v2 rpos = gpos + v2(0, y * MAG_SIZE);
      DOUBLE_BUFFER->DrawHorizontalLine(rpos.X, rpos.X + MAG_SIZE * size.X - 1, rpos.Y, BLUE);
    }

    for (int x = 1; x != size.X; x += 1) {
      v2 rpos = gpos + v2(x * MAG_SIZE, 0);
      DOUBLE_BUFFER->DrawVerticalLine(rpos.X, rpos.Y, rpos.Y + MAG_SIZE * size.Y - 1, BLUE);
    }

    {
      v2 rpos = gpos + v2(curpos.X * MAG_SIZE, curpos.Y * MAG_SIZE);
      DOUBLE_BUFFER->DrawRectangle(rpos, rpos + v2(MAG_SIZE, MAG_SIZE),
                                   part.GetPalColor16(curcidx), true);

      uChar rawpx = part.GetRawPixel(curpos);
      packcol16 cpx = part.GetRGB16Pixel(curpos);
      const int r = GetRed16(cpx);
      const int g = GetGreen16(cpx);
      const int b = GetBlue16(cpx);
      const int r8 = part.GetPalColorRed8(rawpx);
      const int g8 = part.GetPalColorGreen8(rawpx);
      const int b8 = part.GetPalColorBlue8(rawpx);

      rpos = gpos + v2(8, (size.Y + 1) * MAG_SIZE);
      festring s;
      s << "Color: " << rawpx;
      FONT->PrintStr(DOUBLE_BUFFER, rpos, WHITE, s); rpos += v2(0, 16);
      char buf[128];
      snprintf(buf, sizeof(buf), "R:%3d  G:%3d  B:%3d", r, g, b);
      s.Empty(); s << buf;
      FONT->PrintStr(DOUBLE_BUFFER, rpos, WHITE, s); rpos += v2(0, 16);
      snprintf(buf, sizeof(buf), "#%02X%02X%02X   #%02X%02X%02X", r, g, b, r8, g8, b8);
      s.Empty(); s << buf;
      FONT->PrintStr(DOUBLE_BUFFER, rpos, WHITE, s); rpos += v2(0, 16);
    }

    // draw palette
    {
      const uChar crp = part.GetRawPixel(curpos);
      v2 rpos = gpos + v2(8, (size.Y + 4) * MAG_SIZE);
      for (int y = 0; y != 16; y += 1) {
        for (int x = 0; x != 16; x += 1) {
          int cidx = y * 16 + x;
          v2 ppos = rpos + v2(x * 8, y * 8);
          packcol16 cc = part.GetPalColor16(cidx);
          DOUBLE_BUFFER->Fill(ppos, 8, 8, cc);
          if (cidx == crp) {
            DOUBLE_BUFFER->Fill(ppos + v2(3, 3), 2, 2, cc ^ 0xFFFFu);
          }
        }
      }
      int ccx = curcidx % 16;
      int ccy = curcidx / 16;
      v2 ppos = rpos + v2(ccx * 8, ccy * 8);
      packcol16 cc = part.GetPalColor16(curcidx) ^ 0xFFFFu;
      DOUBLE_BUFFER->DrawRectangle(ppos - v2(1, 1), ppos + v2(8, 8), cc);
    }

    graphics::BlitDBToScreen();

    int k = GET_KEY();
    if (k == 0x1b) {
      break;
    }
    else if (k == KEY_UP) {
      curpos.Y = Max(0, curpos.Y - 1);
    }
    else if (k == KEY_DOWN) {
      curpos.Y = Min(size.Y - 1, curpos.Y + 1);
    }
    else if (k == KEY_RIGHT) {
      curpos.X = Min(size.X - 1, curpos.X + 1);
    }
    else if (k == KEY_LEFT) {
      curpos.X = Max(0, curpos.X - 1);
    }
    else if (k == KEY_MINUS) {
      if (curcidx != 0) curcidx -= 1;
    }
    else if (k == KEY_PLUS) {
      if (curcidx != 255) curcidx += 1;
    }
    else if (k == KEY_SPACE) {
      if (!readOnly) {
        part.SetRawPixel(curpos, curcidx);
      }
    }
    else if (KEY_EQU(k, "M-C")) {
      curcidx = part.GetRawPixel(curpos);
    }
    else if (k == 8) {
      if (!readOnly) {
        part.SetRawPixel(curpos, 191);
      }
    }
    else if (KEY_EQU(k, "S-1")) {
      if (!readOnly) {
        for (int y = 0; y != size.Y; y += 1) {
          for (int x = 0; x != size.X; x += 1) {
            cbm->SetRawPixel(pos + v2(x, y), part.GetRawPixel(x, y));
          }
        }
        SetChanged();
      }
    }
  }
}


//==========================================================================
//
//  Main
//
//==========================================================================
int Main (int argc, char **argv) {
  RawBmpLump clipboard;
  festring Directory;

  festring OldDirectory;
  std::ifstream IConfigFile(".igor.rc");
  if (IConfigFile.is_open()) {
    char ch;
    while (IConfigFile.get(ch)) OldDirectory << ch;
  }
  IConfigFile.close();

  if (OldDirectory.IsEmpty()) {
    std::cout << "Where is the graphics directory? ";
    if (OldDirectory.GetSize()) std::cout << '[' << OldDirectory.CStr() << "] ";

    char ch;
    while ((ch = getchar()) != '\n') Directory << ch;

    if (Directory.IsEmpty()) Directory = OldDirectory;
    if (!Directory.IsEmpty() && Directory[Directory.GetSize()-1] != '/') Directory << '/';

    std::ofstream OConfigFile(".igor.rc");
    OConfigFile << Directory.CStr();
    OConfigFile.close();
  } else {
    Directory = OldDirectory;
  }

  int startSelection = -1;
  for (int f = 1; f != argc; f += 1) {
    const char *aa = argv[f];
    if (aa && !aa[1] && aa[0] >= '0' && aa[0] <= '7') {
      startSelection = aa[0] - '0';
    }
  }

  graphics::Init();
  graphics::SetMode("IGOR 1.203", "", v2(800, 600), false, 7, true, true);
  graphics::LoadDefaultFont(Directory+"Font.png");
  graphics::LoadConsoleFont(Directory);
  DOUBLE_BUFFER->ClearToColor(0);
  graphics::EnableConsole();
  ConEnableStdErr();

  felist ListSaveQuit(CONST_S("Save/Quit?"));
  ListSaveQuit.SetPos(v2(300, 250));
  ListSaveQuit.SetWidth(200);
  ListSaveQuit.RemoveFlags(FADE);
  ListSaveQuit.AddFlags(DRAW_BACKGROUND_AFTERWARDS);
  ListSaveQuit.AddEntry(CONST_S("Save and quit"), LIGHT_GRAY);
  ListSaveQuit.AddEntry(CONST_S("Don't save and quit"), RED);

  felist ListSave(CONST_S("Save?"));
  ListSave.SetPos(v2(300, 250));
  ListSave.SetWidth(200);
  ListSave.RemoveFlags(FADE);
  ListSave.AddFlags(DRAW_BACKGROUND_AFTERWARDS);
  ListSave.AddEntry(CONST_S("Save"), LIGHT_GRAY);
  ListSave.AddEntry(CONST_S("Don't save"), RED);

  felist List(CONST_S("Choose file to edit:"));
  List.AddEntry(CONST_S("Item.png"), LIGHT_GRAY);
  //List.AddEntry(CONST_S("Item-Outlined.png"), LIGHT_GRAY);
  List.AddEntry(CONST_S("Char.png"), LIGHT_GRAY);
  List.AddEntry(CONST_S("Humanoid.png"), LIGHT_GRAY);
  List.AddEntry(CONST_S("Symbol.png"), LIGHT_GRAY);
  List.AddEntry(CONST_S("GLTerra.png"), LIGHT_GRAY);
  List.AddEntry(CONST_S("OLTerra.png"), LIGHT_GRAY);
  List.AddEntry(CONST_S("WTerra.png"), LIGHT_GRAY);
  festring FileName, FileName2;

  List.SetPos(v2(300, 250));
  List.SetWidth(200);
  if (startSelection >= 0 && startSelection <= 7) {
    Selected = startSelection;
  } else {
    while ((Selected = List.Draw()) & FELIST_ERROR_BIT) {
      if (Selected == ESCAPED) return 0;
    }
  }

  switch (Selected) {
    case 0: FileName = CONST_S("Item.png"); break;
    /*
    case 1: FileName = CONST_S("Item-outlined.png");
            FileName2 = CONST_S("Item.png");
            break;
    */
    case 1: FileName = CONST_S("Char.png"); break;
    case 2: FileName = CONST_S("Humanoid.png"); break;
    case 3: FileName = CONST_S("Symbol.png"); break;
    case 4: FileName = CONST_S("GLTerra.png"); break;
    case 5: FileName = CONST_S("OLTerra.png"); break;
    case 6: FileName = CONST_S("WTerra.png"); break;
  }

  CBitmaps[0] = new rawbitmap(Directory + FileName);

  if (FileName2.IsEmpty()) FileName2 << "_new/" << FileName;

  if (inputfile::fileExists(Directory + FileName2)) {
    CBitmaps[1] = new rawbitmap(Directory + FileName2);
    CBitmaps[1]->TranslateToPaletteOf(CBitmaps[0]);
    secondBitmapEnabled = true;
  } else {
    CBitmaps[1] = 0;
    secondBitmapEnabled = false;
  }

  bitmap CursorBitmap(Directory + "Cursor.png");
  CursorBitmap.ActivateFastFlag();

  bool moveByTile = true;

  int k = 0;
  Selected = 0;
  static uChar TempBuffer[65536];
  /*
  blitdata B1 = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { RES.X - STRETCH * 16 - 10, RES.Y - STRETCH * 16 - 10 },
    { TILE_SIZE, TILE_SIZE },
    { STRETCH }
  };
  blitdata B2 = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { RES.X - STRETCH * 32 - 20, RES.Y - STRETCH * 16 - 10 },
    { TILE_SIZE, TILE_SIZE },
    { STRETCH }
  };
  blitdata B3 = {
    DOUBLE_BUFFER,
    { 0, 0 },
    { 0, 0 },
    { TILE_SIZE, TILE_SIZE },
    { 0 },
    TRANSPARENT_COLOR
  };
  */

  for (;;) {
    #if 0
    static v2 MoveVector[] = { v2(0, -16), v2(-16, 0), v2(0, 16), v2(16, 0) };
    static int Key[] = { 'w', 'a', 's', 'd' };
    for (c = 0; c < 4; ++c) {
      if (Key[c] == k) {
        v2 NewPos = Cursor+MoveVector[c];
        if (NewPos.X >= 0 && NewPos.X <= CBitmap->GetSize().X-16 &&
            NewPos.Y >= 0 && NewPos.Y <= CBitmap->GetSize().Y-16) Cursor = NewPos;
        break;
      }
      if ((Key[c]&~0x20) == k) {
        v2 NewPos = Cursor+(MoveVector[c]<<2);
        if (NewPos.X >= 0 && NewPos.X <= CBitmap->GetSize().X-16 &&
            NewPos.Y >= 0 && NewPos.Y <= CBitmap->GetSize().Y-16) Cursor = NewPos;
        break;
      }
    }
    #else
    {
      v2 NewPos = Cursor;
      const int delta = (moveByTile ? TILE_SIZE : 1);
           if (k == KEY_UP) NewPos += v2(0, -delta);
      else if (k == KEY_DOWN) NewPos += v2(0, delta);
      else if (k == KEY_LEFT) NewPos += v2(-delta, 0);
      else if (k == KEY_RIGHT) NewPos += v2(delta, 0);
      if (NewPos.X >= 0 && NewPos.X <= CBitmap->GetSize().X-16 &&
          NewPos.Y >= 0 && NewPos.Y <= CBitmap->GetSize().Y-16)
      {
        Cursor = NewPos;
      }
    }
    #endif

    if (k >= 0x31 && k <= 0x34) {
      Selected = k-0x31;
    }
    else if (k == KEY_PLUS) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->AlterGradient(Cursor, TILE_V2, Selected, 1, false);
      }
    }
    else if (k == KEY_MINUS) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->AlterGradient(Cursor, TILE_V2, Selected, -1, false);
      }
    }
    else if (KEY_EQU(k, "S-.")) { //>
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->AlterGradient(Cursor, TILE_V2, Selected, 1, true);
      }
    }
    else if (KEY_EQU(k, "S-,")) { //<
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->AlterGradient(Cursor, TILE_V2, Selected, -1, true);
      }
    }
    else if (KEY_EQU(k, "S-E")) {
      EditTile(CBitmap, Cursor, TILE_V2, CursorBitmap, IsReadOnly());
    }
    /*
    else if (k == KEY_UP) { cbmChanged = true; CBitmap->Roll(Cursor, TILE_V2, v2(0, -1), TempBuffer); }
    else if (k == KEY_DOWN) { cbmChanged = true; CBitmap->Roll(Cursor, TILE_V2, v2(0, 1), TempBuffer); }
    else if (k == KEY_RIGHT) { cbmChanged = true; CBitmap->Roll(Cursor, TILE_V2, v2(1, 0), TempBuffer); }
    else if (k == KEY_LEFT) { cbmChanged = true; CBitmap->Roll(Cursor, TILE_V2, v2(-1, 0), TempBuffer); }
    */
    else if (KEY_EQU(k, "S-W")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Roll(Cursor, TILE_V2, v2(0, -1), TempBuffer);
      }
    }
    else if (KEY_EQU(k, "S-S")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Roll(Cursor, TILE_V2, v2(0, 1), TempBuffer);
      }
    }
    else if (KEY_EQU(k, "S-D")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Roll(Cursor, TILE_V2, v2(1, 0), TempBuffer);
      }
    }
    else if (KEY_EQU(k, "S-A")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Roll(Cursor, TILE_V2, v2(-1, 0), TempBuffer);
      }
    }
    else if (KEY_EQU(k, "S-I")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Roll(Cursor - v2(0, 1), v2(TILE_SIZE + 0, TILE_SIZE + 2), v2(0, -1), TempBuffer);
        Cursor.Y -= 1;
      }
    }
    else if (KEY_EQU(k, "S-K")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Roll(Cursor - v2(0, 1), v2(TILE_SIZE + 0, TILE_SIZE + 2), v2(0, 1), TempBuffer);
        Cursor.Y += 1;
      }
    }
    else if (KEY_EQU(k, "S-L")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Roll(Cursor - v2(1, 0), v2(TILE_SIZE + 2, TILE_SIZE + 0), v2(1, 0), TempBuffer);
        Cursor.X += 1;
      }
    }
    else if (KEY_EQU(k, "S-J")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Roll(Cursor - v2(1, 0), v2(TILE_SIZE + 2, TILE_SIZE + 0), v2(-1, 0), TempBuffer);
        Cursor.X -= 1;
      }
    }
    else if (k == '=') {
      if (!IsReadOnly()) {
        FONT->PrintStr(DOUBLE_BUFFER, v2(10, 460), RED,
                       CONST_S("Select col to swap with [\1G1\2-\1G4\2/\1YESC\2]"));
        graphics::BlitDBToScreen();
        for (k = GET_KEY(); k != 0x1B; k = GET_KEY()) {
          if (k >= 0x31 && k <= 0x34) {
            SetChanged();
            CBitmap->SwapColors(Cursor, TILE_V2, Selected, k - 0x31);
            break;
          }
        }
      }
    } else if (k == KEY_ESC || KEY_EQU(k, "S-S")) {
      const bool doExit = (k == KEY_ESC);
      //FONT->PrintStr(DOUBLE_BUFFER, v2(10, 460), RED, "Save? [y/n/c]");
      //graphics::BlitDBToScreen();

      bool doCancel = false;
      int sel;
      if (doExit) {
        if (cbmChanged) {
          while (!doCancel && ((sel = ListSaveQuit.Draw()) & FELIST_ERROR_BIT)) {
            if (sel == ESCAPED) doCancel = true;
          }
        } else {
          sel = 1;
        }
      } else {
        while (!doCancel && ((sel = ListSave.Draw()) & FELIST_ERROR_BIT)) {
          if (sel == ESCAPED) doCancel = true;
        }
      }

      if (!doCancel) {
        if (sel == 0 && cbmIndex != 0) {
          ConLogf("ERROR: cannot save new image!");
          doCancel = true;
        }
        if (sel == 0) {
          #if 1
          ConLogf("saving...");
          #endif
          CBitmap->SavePNG(Directory + FileName);
          SetUnchanged();
        }
        if (doExit) {
          delete CBitmaps[0];
          delete CBitmaps[1];
          return 0;
        }
      }
    } else if (KEY_EQU(k, "P")) {
      std::vector<v2>::iterator i = std::find(DrawQueue.begin(), DrawQueue.end(), Cursor);
      if (i == DrawQueue.end()) DrawQueue.push_back(Cursor); else DrawQueue.erase(i);
    } else if (KEY_EQU(k, "C")) {
      DrawQueue.clear();
    } else if (KEY_EQU(k, "S-1")) {
      if (secondBitmapEnabled) cbmIndex = 1 - cbmIndex;
    } else if (KEY_EQU(k, "S-C")) {
      clipboard = CBitmap->Copy(Cursor, v2(16, 16));
    } else if (KEY_EQU(k, "S-P")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->Paste(clipboard, Cursor);
      }
    } else if (KEY_EQU(k, "S-X")) {
      moveByTile = !moveByTile;
      if (moveByTile) {
        Cursor.X = Cursor.X / TILE_SIZE * TILE_SIZE;
        Cursor.Y = Cursor.Y / TILE_SIZE * TILE_SIZE;
      }
    } else if (KEY_EQU(k, "S-O")) {
      if (!IsReadOnly()) {
        SetChanged();
        CBitmap->OutlineRect(Cursor, v2(16, 16));
      }
    } else if (KEY_EQU(k, "F")) {
      if (!IsReadOnly()) {
        CBitmap->HFlipRect(Cursor, v2(16, 16));
        SetChanged();
      }
    } else if (KEY_EQU(k, "S-F")) {
      if (!IsReadOnly()) {
        CBitmap->VFlipRect(Cursor, v2(16, 16));
        SetChanged();
      }
    } else if (KEY_EQU(k, "S-/")) {
      ConLogf("BitmapPos = %d, %d;", Cursor.X, Cursor.Y);
    }

    DrawMainScreen(CBitmap, CursorBitmap);
    graphics::BlitDBToScreen();
    k = GET_KEY();
  }

  return 1;
}
